package net.teamplace.android.backend;

/**
 * Created by jobol on 02/06/15.
 *
 */
public interface BackendServers
{
    public static final String ACCOUNT = "https://cloudcentral-tp.cortado.com";
    public static final String WORKPLACE = "https://wppool-tp.cortado.com";
    public static final String TEAMDRIVE_API = "https://api-tp.cortado.com/teamdrive/v2";
    public static final String ACTIVITY_API = "https://api-tp.cortado.com/activity/v1";
    public static final String QUOTA_API = "https://api-tp.cortado.com/quota/v2";
    public static final String REGISTRATION = "https://account-tp.cortado.com";
    public static final String FORGOT_PASSWORD = "https://account-tp.cortado.com/web/forgotpassword";
    public static final String TUTORIAL = "http://teamplace.net/webview/tutorial-android";
    public static final String FAQ = "https://www.teamplace.net/en-us/Pricing#faq";
    public static final String PROFILE = "https://api-tp.cortado.com/profile/v1";
}
