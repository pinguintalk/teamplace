package net.teamplace.android.backend;

/**
 * Created by jobol on 02/06/15.
 *
 */
public interface BackendServers
{
    public static final String ACCOUNT = "https://manage.teamplace.net";
    public static final String WORKPLACE = "https://connect.teamplace.net";
    public static final String TEAMDRIVE_API = "https://api.teamplace.net/teamdrive/v2";
    public static final String ACTIVITY_API = "https://api.teamplace.net/activity/v1";
    public static final String QUOTA_API = "https://api.teamplace.net/quota/v2";
    public static final String REGISTRATION = "https://account.teamplace.net";
    public static final String FORGOT_PASSWORD = "https://account.teamplace.net/web/forgotpassword";
    public static final String TUTORIAL = "http://teamplace.net/webview/tutorial-android";
    public static final String FAQ = "https://www.teamplace.net/en-us/Pricing#faq";
    public static final String PROFILE = "https://api.teamplace.net/profile/v1";
}
