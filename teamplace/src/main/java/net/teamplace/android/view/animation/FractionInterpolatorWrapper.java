package net.teamplace.android.view.animation;

import android.view.animation.Interpolator;

/**
 * Interpolator that takes another interpolator and uses its interpolation function from startFrom (in time). More
 * precisely, let i be the other interpolator; then this interpolator function will be i.getInterpolation(f(t)) where
 * f(t) the linear function mapping [0..1] to [startFrom..1]
 * 
 * @author Gil Vegliach
 */
public class FractionInterpolatorWrapper implements Interpolator
{
	final Interpolator mInterpolator;
	final float mFraction;

	public FractionInterpolatorWrapper(Interpolator interpolator, float startFrom)
	{
		mInterpolator = interpolator;
		mFraction = startFrom;
	}

	@Override
	public float getInterpolation(float input)
	{
		return mInterpolator.getInterpolation((1.f - mFraction) * input + mFraction);
	};
}