package net.teamplace.android.view.animation;

import android.view.animation.Interpolator;

/**
 * Interpolator that mimics a spring pulling towards itself. The motion will start at null speed and accelerate towards
 * the end position. The acceleration will decrease as it depends on the displacement from the end position (spring
 * anchor point).
 * 
 * @author Gil Vegliach
 */
public class HarmonicInterpolator implements Interpolator
{
	@Override
	public float getInterpolation(float t)
	{
		return (float) (1.0 - Math.cos(Math.PI * t / 2.0));
	}
}