package net.teamplace.android.errorhandling;

public class AuthenticationEvent extends BasicEvent
{
	private final String TAG = getClass().getSimpleName();

	public AuthenticationEvent()
	{
	}

	public AuthenticationEvent(Exception exception)
	{
		super(exception);
	}
}
