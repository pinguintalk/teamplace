package net.teamplace.android.errorhandling;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cortado.android.R;

public class FeedbackToast extends Toast
{

	@SuppressLint("InflateParams")
	public FeedbackToast(Context context)
	{
		super(context);
		setView(LayoutInflater.from(context).inflate(R.layout.err_toast, null, false));
		int screenLayout = context.getResources().getConfiguration().screenLayout;
		boolean isPhoneSize = (screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_SMALL
				|| (screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL;
		int fillHorizontal = isPhoneSize ? Gravity.FILL_HORIZONTAL : 0;
		setGravity(fillHorizontal | Gravity.BOTTOM, 0, 0);
		setDuration(Toast.LENGTH_SHORT);
	}

	public FeedbackToast(Context context, int iconBgRes, int iconRes, String title, String msg)
	{
		this(context);
		setIconBgRes(iconBgRes);
		setIconRes(iconRes);
		setTitle(title);
		setMessage(msg);
	}

	public FeedbackToast(Context context, boolean positive, String msg)
	{
		this(context, positive ? R.drawable.brw_success_circle : R.drawable.brw_error_circle,
				positive ? R.drawable.checkmark : R.drawable.delete, context
						.getString(positive ? R.string.ntf_title_done : R.string.ntf_title_error), msg);
	}

	public static FeedbackToast show(Context ctx, boolean positive, String msg)
	{
		FeedbackToast feedbackToast = new FeedbackToast(ctx, positive, msg);
		feedbackToast.show();
		return feedbackToast;
	}

	public void setIconBgRes(int iconBgRes)
	{
		getIconView().setBackgroundResource(iconBgRes);
	}

	public void setIconRes(int iconRes)
	{
		getIconView().setImageResource(iconRes);
	}

	public void setTitle(String title)
	{
		((TextView) getView().findViewById(R.id.tv_title)).setText(title);
	}

	public void setMessage(String message)
	{
		((TextView) getView().findViewById(R.id.tv_details)).setText(message);
	}

	private ImageView getIconView()
	{
		return (ImageView) getView().findViewById(R.id.iv_icon);
	}

}
