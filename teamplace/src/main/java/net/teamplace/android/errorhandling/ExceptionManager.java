package net.teamplace.android.errorhandling;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.pkg;
import net.teamplace.android.application.App;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.initialization.InitializationLoader;
import net.teamplace.android.util.annotations.ThreadSafe;
import net.teamplace.android.utils.GenericUtils;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.cortado.android.R;

import de.greenrobot.event.EventBus;
import de.greenrobot.event.NoSubscriberEvent;

/**
 * Handles Exceptions propagating them to registered subscribers or showing a notification. The method
 * {@link #handleException(Exception)} be used every time an Exception is risen in the app.
 */
@ThreadSafe
final public class ExceptionManager
{
	private static final String PKG = pkg(ExceptionManager.class);
	private static final int ID_AUTHENTICATION_NOTIFICATION = 1449221014;

	public static final String EXTRA_EXCEPTION = PKG + ".extra_exception";

	private static class Holder
	{
		static final ExceptionManager INSTANCE = new ExceptionManager();
	}

	private ExceptionManager()
	{
		EventBus bus = EventBus.getDefault();
		bus.register(this);
	}

	@Override
	protected void finalize() throws Throwable
	{
		try
		{
			EventBus bus = EventBus.getDefault();
			bus.unregister(this);
		}
		finally
		{
			// Strictly speaking not needed, but good practice
			super.finalize();
		}
	}

	public static ExceptionManager getInstance()
	{
		return Holder.INSTANCE;
	}

	public void handleException(Exception exception)
	{
		checkNonNullArg(exception);
		if (exception instanceof LogonFailedException)
		{
			EventBus bus = EventBus.getDefault();
			bus.post(new AuthenticationEvent(exception));
		}
		else
		{
			// TODO: @jobol what is the current status of this?
			GenericUtils.laundryException(exception);
		}
	}

	public boolean handleGlobalException(Exception exception)
	{
		checkNonNullArg(exception);
		if (exception instanceof LogonFailedException)
		{
			EventBus bus = EventBus.getDefault();
			bus.post(new AuthenticationEvent(exception));
			return true;
		}
		return false;
	}

	public void onEventMainThread(NoSubscriberEvent event)
	{
		Context context = App.getContext();
		NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

		Object original = event.originalEvent;
		if (original instanceof AuthenticationEvent)
		{
			nm.notify(ID_AUTHENTICATION_NOTIFICATION,
					getAuthenticationNotification(context));
		}
	}

	private Notification getAuthenticationNotification(Context context)
	{
		Notification.Builder builder = new Notification.Builder(context);

		Intent intent = new Intent(context, InitializationActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

		intent.putExtra(InitializationLoader.OPTION_INVALIDATE_CACHED_TOKEN, true);

		builder.setContentIntent(pendingIntent);

		// TODO: icon not final and move strings in strings.xml
		builder.setSmallIcon(R.drawable.at);
		builder.setContentTitle("Please log in!");
		builder.setContentText("Authentication failed. Please log in again!");
		builder.setAutoCancel(true);
		return builder.build();
	}
}
