package net.teamplace.android.errorhandling;

public class BasicEvent
{
	private final String TAG = getClass().getSimpleName();

	private Exception exception;

	public BasicEvent()
	{
	}

	public BasicEvent(Exception exception)
	{
		this.exception = exception;
	}

	public Exception getException()
	{
		return exception;
	}

	public void setException(Exception exception)
	{
		this.exception = exception;
	}
}
