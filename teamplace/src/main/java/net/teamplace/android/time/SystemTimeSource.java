package net.teamplace.android.time;

public class SystemTimeSource implements TimeSource
{
	@Override
	public long currentTimeMillis()
	{
		return System.currentTimeMillis();
	}
}
