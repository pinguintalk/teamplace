package net.teamplace.android.time;

public interface TimeSource
{
	long currentTimeMillis();
}
