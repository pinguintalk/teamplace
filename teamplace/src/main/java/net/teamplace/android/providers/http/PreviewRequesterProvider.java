package net.teamplace.android.providers.http;

import net.teamplace.android.http.request.preview.PreviewRequester;
import android.content.Context;

public interface PreviewRequesterProvider
{
	PreviewRequester providePreviewRequester(Context context);
}