package net.teamplace.android.providers.http;

import net.teamplace.android.http.request.files.ExportRequester;
import android.content.Context;

public interface ExportRequesterProvider
{
	ExportRequester provideExportRequester(Context context);
}