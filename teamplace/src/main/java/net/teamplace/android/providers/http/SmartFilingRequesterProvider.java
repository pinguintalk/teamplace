package net.teamplace.android.providers.http;

import net.teamplace.android.http.request.smartfiling.SmartFilingRequester;
import android.content.Context;

public interface SmartFilingRequesterProvider
{
	SmartFilingRequester provideSmartFilingRequester(Context context);
}