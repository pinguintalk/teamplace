package net.teamplace.android.providers.http;

import net.teamplace.android.http.request.files.FileRequester;
import android.content.Context;

public interface FileRequesterProvider
{
	FileRequester provideFileRequester(Context context);
}