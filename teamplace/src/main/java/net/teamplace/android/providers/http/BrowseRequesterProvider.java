package net.teamplace.android.providers.http;

import net.teamplace.android.http.request.browse.BrowseRequester;
import android.content.Context;

public interface BrowseRequesterProvider
{
	BrowseRequester provideBrowseRequester(Context context);
}