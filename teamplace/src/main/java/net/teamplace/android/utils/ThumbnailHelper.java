package net.teamplace.android.utils;

/**
 * Created by nangu on 24.06.15.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

import net.teamplace.android.application.DefaultImageDownloader;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.thumbnail.ThumbnailRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class ThumbnailHelper implements DefaultImageDownloader.NetworkStreamResolver {

    @Override
    public InputStream fromUri(Context context, String uri) throws IOException {
        Session session = new SessionManager(context).getLastSession();
        ThumbnailRequester requester = new ThumbnailRequester(context, session, new TeamplaceBackend());
        try {
            return requester.getInputStream(Uri.parse(uri));
        } catch (XCBStatusException | HttpResponseException | ConnectFailedException | LogonFailedException
                | NotificationException | ServerLicenseException | RedirectException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void showThumbnail(FileInfo fileInfo, ImageView iv, ThumbnailRequester mRequester, DisplayImageOptions mImageOptions) {

        String uri = getThumbnailUri(fileInfo, mRequester);
        if (uri != null && !TextUtils.isEmpty(uri)) {
            ImageLoader.getInstance().displayImage(uri, iv, mImageOptions,
                    new ImageLoadingListener(fileInfo.getLastModifiedDate(),iv,mImageOptions));
        }

    }

    private static String getThumbnailUri(FileInfo fileInfo,ThumbnailRequester mRequester) {

        String uri = "";
        Path path = fileInfo.getPath();
        int mType = fileInfo.getType();

        String teamId = Path.isTeamDriveLocation(path.getLocation()) ? ((Path.TeamDrive)
                path).getTeamDriveId()
                : null;


        String pathName = PathUtils.remoteRequestString(path.getParentPath());

        if (Path.isLocalLocation(path.getLocation())) {
            String filePath = ((Path.Local) path).getAbsolutePathString();

            if (mType == FileInfo.IMAGE || mType == FileInfo.VIDEO) {
                uri = "file://" + filePath;
            }
        } else {

            try {
                uri = mRequester.getThumbnailUri(teamId, pathName, path.getName())
                        .toString();
            } catch (TeamDriveNotFoundException e) {
                e.printStackTrace();
            }
        }

        return uri;
    }

    public static void removeThumbnailFromCache(FileInfo fileInfo, ThumbnailRequester mRequester) {

            String imageUri = ThumbnailHelper.getThumbnailUri(fileInfo, mRequester);
            if (imageUri != null && !TextUtils.isEmpty(imageUri)) {
                MemoryCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getMemoryCache());
                DiskCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getDiskCache());
            }

    }

    public static void removeThumbnailsFromCache(Context context, ArrayList<FileInfo> fileInfos,ThumbnailRequester requester ) {

        for (int i = 0; i < fileInfos.size(); i++)
        {
            boolean isFolder = fileInfos.get(i).getType() == FileInfo.FOLDER;
            if(!isFolder)
                removeThumbnailFromCache(fileInfos.get(i),requester);
        }
    }

    private static class ImageLoadingListener extends SimpleImageLoadingListener
    {
        private boolean mDeleteCachedImg = false;

        private long mVersionTimestamp;
        private ImageView mIv;
        private DisplayImageOptions mImageOptions;

        public boolean isLoadComplete() {
            return isLoadComplete;
        }

        private boolean isLoadComplete = false;

        private ImageLoadingListener(long versionTimestamp, ImageView iv,
                                     DisplayImageOptions imageOptions)
        {
            mVersionTimestamp = versionTimestamp;
            mIv = iv;
            mImageOptions = imageOptions;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view)
        {
            File cachedImg = DiskCacheUtils.findInCache(imageUri, ImageLoader.getInstance().getDiskCache());
            if (cachedImg != null)
            {
                long currentTime = System.currentTimeMillis();
                long cachedImgTimestamp = cachedImg.lastModified();
                // remote file is newer than cached file OR unplausible local time
                mDeleteCachedImg = mVersionTimestamp > cachedImgTimestamp || mVersionTimestamp > currentTime;
            }
            mIv.setVisibility(View.INVISIBLE);
            super.onLoadingStarted(imageUri, view);
        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
        {

            if (mDeleteCachedImg)
            {
                MemoryCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getMemoryCache());
                DiskCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getDiskCache());
                mDeleteCachedImg = false;
                ImageLoader.getInstance().displayImage(imageUri, (ImageView) view, mImageOptions, this);
                return;
            }
            isLoadComplete = true;
            mIv.setVisibility(View.VISIBLE);
            super.onLoadingComplete(imageUri, view, loadedImage);
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason)
        {
            isLoadComplete = false;
            super.onLoadingFailed(imageUri, view, failReason);
        }
    }
}