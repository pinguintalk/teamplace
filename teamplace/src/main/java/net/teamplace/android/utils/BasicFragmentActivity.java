package net.teamplace.android.utils;

import net.teamplace.android.errorhandling.AuthenticationEvent;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.initialization.InitializationLoader;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;


import de.greenrobot.event.EventBus;

public class BasicFragmentActivity extends FragmentActivity
{
	@Override
	protected void onResume()
	{
		super.onResume();

		EventBus.getDefault().register(this);
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		EventBus.getDefault().unregister(this);
	}

	public void onEventMainThread(AuthenticationEvent event)
	{
		Intent intent = new Intent(this, InitializationActivity.class);

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

		intent.putExtra(InitializationLoader.OPTION_INVALIDATE_CACHED_TOKEN, true);

		startActivity(intent);
		finish();
	}
}
