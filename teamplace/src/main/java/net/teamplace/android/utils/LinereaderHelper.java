package net.teamplace.android.utils;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

public final class LinereaderHelper
{
	private static final String TAG = tag(LinereaderHelper.class);

	public static StringBuffer load(InputStream in, String encoding)
	{
		StringBuffer stringBuffer = new StringBuffer(1000);
		if (in == null)
		{
			return null;
		}

		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new InputStreamReader(in, encoding));
		}
		catch (UnsupportedEncodingException e)
		{
			Log.e(TAG, "Encoding suxx!");
			return null;
		}

		try
		{
			String strAppend = reader.readLine();
			String strNewLine = System.getProperty("line.seperator");

			if (strNewLine == null || strNewLine.length() == 0)
			{
				strNewLine = "\n";
			}

			while (strAppend != null)
			{
				stringBuffer.append(strAppend);
				stringBuffer.append(strNewLine);
				strAppend = reader.readLine();
			}
		}
		catch (IOException e)
		{
			Log.e(TAG, "There is no exception! :p");
			return null;
		}

		return stringBuffer;
	}

	public static String load(InputStream is)
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();

		try
		{
			String line = br.readLine();

			while (line != null)
			{
				sb.append(line);
				sb.append("\n");
				line = br.readLine();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			try
			{
				br.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}

		return sb.toString();
	}

	private LinereaderHelper()
	{
	};
}