package net.teamplace.android.utils;

import static net.teamplace.android.utils.GenericUtils.tag;
import android.os.Bundle;

/**
 * Simple Activity that logs all its life-cycle callbacks. Useful to reduce clutter
 * 
 * @author Gil Vegliach
 */
public class LoggingActivity extends BasicActivity
{
	// Not a constant, but functions similarly
	public String TAG = tag(LoggingActivity.class);

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart()
	{
		Log.d(TAG, "onStart()");
		super.onStart();
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		Log.d(TAG, "onRestoreInstanceState()");
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		Log.d(TAG, "onPostCreate()");
		super.onPostCreate(savedInstanceState);
	}

	@Override
	protected void onResume()
	{
		Log.d(TAG, "onResume()");
		super.onResume();
	}

	@Override
	public void onAttachedToWindow()
	{
		Log.d(TAG, "onAttachedToWindow()");
		super.onAttachedToWindow();
	}

	@Override
	public void onDetachedFromWindow()
	{
		Log.d(TAG, "onDetachedFromWindow()");
		super.onDetachedFromWindow();
	}

	@Override
	protected void onPause()
	{
		Log.d(TAG, "onPause()");
		super.onPause();
	}

	@Override
	protected void onStop()
	{
		Log.d(TAG, "onStop()");
		super.onStop();
	}

	@Override
	protected void onRestart()
	{
		Log.d(TAG, "onRestart()");
		super.onRestart();
	}

	@Override
	protected void onDestroy()
	{
		Log.d(TAG, "onDestroy()");
		super.onDestroy();
	}
}
