package net.teamplace.android.utils;

import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import net.teamplace.android.browsing.path.FileSystem;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.Local;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.cortado.android.R;

public class GenericUtils
{
	private static final String TAG = tag(GenericUtils.class);
	public static final String STACK_TRACK_HEADER = "StackTrace:\n";

	private GenericUtils()
	{
	}

	/** Calls {@link #checkNonNullArg(T, String)} with a standard error message. */
	public static <T> T checkNonNullArg(T arg)
	{
		return checkNonNullArg(arg, "Argument is null, was: " + arg);
	}

	/** Calls {@link #checkNonNullArg(T)} on the implicit array <code>args</code> and all its objects. */
	@SafeVarargs
	public static <T> void checkNonNullArgs(T... args)
	{
		checkNonNullArg(args);
		for (T arg : args)
		{
			checkNonNullArg(arg);
		}
	}

	/** Calls {@link #checkCondition(boolean)} with a standard error message. */
	public static void checkCondition(boolean condition)
	{
		checkCondition(condition, "Condition is false");
	}

	/**
	 * Checks if an argument is non-null and returns it in that case. Throws an IllegalArgumentException with the given
	 * error message if the argument is null.
	 * 
	 * @param arg
	 *            argument to check
	 * @param errMessage
	 *            exception detail message
	 */
	public static <T> T checkNonNullArg(T arg, String errMessage)
	{
		checkCondition(arg != null, errMessage);
		return arg;
	}

	/**
	 * Checks if a condition is true. Throws an IllegalArgumentException with the given
	 * error message if the condition is false.
	 * 
	 * @param condition
	 *            condition to check
	 * @param errMessage
	 *            exception detail message
	 */
	public static void checkCondition(boolean condition, String errMessage)
	{
		if (!condition)
		{
			IllegalArgumentException e;
			if (TextUtils.isEmpty(errMessage))
			{
				e = new IllegalArgumentException();
			}
			else
			{
				e = new IllegalArgumentException(errMessage);
			}
			throw e;
		}
	}

	/**
	 * Asserts that a condition is true. Throws a IllegalStateException otherwise. Similar to
	 * {@link #checkCondition(boolean, String))} but with an exception of another type.
	 * 
	 * @param condition
	 *            condition to assert
	 * @param errMessage
	 *            exception detail message
	 */
	public static void assertTrue(boolean condition, String errMessage)
	{
		if (!condition)
		{
			IllegalStateException e;
			if (TextUtils.isEmpty(errMessage))
			{
				e = new IllegalStateException();
			}
			else
			{
				e = new IllegalStateException(errMessage);
			}
			throw e;
		}
	}

	/**
	 * Checks if a given Exception e is a RuntimeException and if this is the case re-throws it. Useful to eat (only)
	 * all checked Exceptions in try-catch blocks.
	 * 
	 * @param e
	 *            the Exception to check
	 */
	public static void laundryException(Exception e)
	{
		if (e instanceof RuntimeException)
		{
			throw ((RuntimeException) e);
		}
	}

	/**
	 * Closes all the provided <code>closeables</code>, logging eventual failure with the default tag for this class.
	 * 
	 * @param closeables
	 */
	public static void closeQuietly(Closeable... closeables)
	{
		closeQuietly(TAG, closeables);
	}

	/**
	 * Closes all the provided <code>closeables</code>, logging eventual failure with the provided
	 * <code>tag</tag>. Does nothing if <code>closeables</code> is null.
	 * 
	 * @param tag
	 *            the tag used for logging
	 * @param closeables
	 *            vararg of closeable to close
	 */
	public static void closeQuietly(String tag, Closeable... closeables)
	{
		if (closeables == null)
		{
			return;
		}

		for (Closeable c : closeables)
		{
			if (c != null)
			{
				try
				{
					c.close();
				}
				catch (Exception e)
				{
					Log.d(tag, "Error in closing file", e);
				}
			}
		}
	}

	/**
	 * Returns an empty list instead of a null one. Useful in enhanced for loops.
	 * 
	 * @param list
	 * @return the same list if not null, otherwise an empty List
	 */
	public static <T> List<T> emptyIfNull(List<T> list)
	{
		return list == null ? Collections.<T> emptyList() : list;
	}

	/**
	 * Returns an empty set instead of a null one. Useful in enhanced for loops.
	 * 
	 * @param set
	 * @return the same set if not null, otherwise an empty Set
	 */
	public static <T> Set<T> emptyIfNull(Set<T> set)
	{
		return set == null ? Collections.<T> emptySet() : set;
	}

	/**
	 * Concatenates two generic arrays. Returns a copy of the result, or null if both arguments are nulls.
	 * 
	 * @return the two array concatenated in a new array, or null if both {@code arr1} and {@code arr2} are null
	 */
	public static <T> T[] concatArrays(T[] arr1, T[] arr2)
	{
		if (arr1 == null && arr2 == null)
		{
			return null;
		}

		Class<?> cls = null;
		int len1 = 0;
		if (arr1 != null)
		{
			len1 = arr1.length;
			cls = arr1.getClass();
		}

		int len2 = 0;
		if (arr2 != null)
		{
			len2 = arr2.length;
			cls = arr2.getClass();
		}

		@SuppressWarnings("unchecked")
		T[] result = (T[]) Array.newInstance(cls.getComponentType(), len1 + len2);

		for (int i = 0; i < len1; i++)
		{
			result[i] = arr1[i];
		}

		for (int i = 0; i < len2; i++)
		{
			result[len1 + i] = arr2[i];
		}

		return result;
	}

	/**
	 * Converts an InputStream to a byte array.
	 * 
	 * @param is
	 *            the InputStream
	 * @return the byte array or null if is is null or an error occurred
	 */
	public static byte[] inputStreamToByteArray(InputStream is)
	{
		if (is == null)
		{
			return null;
		}

		byte[] buff = new byte[8 * 1024]; // 8 kb
		int bytesRead = 0;
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try
		{
			while ((bytesRead = is.read(buff)) != -1)
			{
				os.write(buff, 0, bytesRead);
			}
			os.flush();
		}
		catch (IOException e)
		{
			Log.e(TAG, "Error converting input stream to byte array", e);
			return null;
		}

		return os.toByteArray();
	}

	/**
	 * Check whether the given email address is valid. Use an internal Pattern. Taken from SO,
	 * http://stackoverflow.com/a/7882950/913286 .
	 * 
	 * @param target
	 *            CharSequence containing the email address to verify
	 * @return
	 */
	public final static boolean isValidEmail(CharSequence target)
	{
		return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	/** General purpose Date formatter to be used across the client. Negative dates are supported. */
	public static String formatDate(Context context, long date)
	{
		checkNonNullArg(context);

		Date d = new Date(date);
		// The standard Java class already encompasses both date and time, but we go for the
		// Android's way :)
		// String formattedDate = java.text.DateFormat.getDateTimeInstance().format(date);
		String formattedDate = android.text.format.DateFormat.getDateFormat(context.getApplicationContext()).format(d);
		String formattedTime = android.text.format.DateFormat.getTimeFormat(context.getApplicationContext()).format(d);
		return formattedDate + " " + formattedTime;
	}

	/**
	 * General purpose file size formatter to be used across the client.
	 * 
	 * @param sizeInBytes
	 *            size to convert. Must be >= 0 or an IllegalArgumentException will be thrown
	 */
	public static String formatFileSize(Context context, long sizeInBytes)
	{
		checkCondition(sizeInBytes >= 0, "sizeInBytes must be >= 0");
		return android.text.format.Formatter.formatShortFileSize(context.getApplicationContext(), sizeInBytes);
	}

	/**
	 * Returns a string from the Throwable tr.
	 * 
	 * @param tr
	 *            generic Throwable, usually an Exception
	 * @return
	 */
	public static String getStackTraceStringFromException(Throwable tr)
	{
		if (tr == null)
		{
			return "";
		}

		PrintWriter pw = new PrintWriter(new StringWriter());
		tr.printStackTrace(pw);
		return STACK_TRACK_HEADER + pw.toString();
	}

	/** Returns a localized string showing the selected count. Used every time multi-select is used. */
	public static String getSelectedString(Context context, int checkedCount)
	{
		checkNonNullArg(context);
		checkCondition(checkedCount >= 0, "checkedCount must be >=0");

		Resources res = context.getResources();
		return res.getQuantityString(R.plurals.brw_selected, checkedCount, checkedCount);
	}

	/** Returns default TAG identifier for a class. Used especially for fragments and logging. */
	public static String tag(Class<?> clazz)
	{
		checkNonNullArg(clazz);
		return clazz.getSimpleName();
	}

	/** Returns default package string for a class. Used especially for keys in bundles and intents. */
	public static String pkg(Class<?> clazz)
	{
		checkNonNullArg(clazz);
		return clazz.getPackage().toString();
	}

	public static String getExtension(String filename)
	{
		checkNonNullArg(filename);
		String[] strArr = filename.split("\\.");
		if (strArr.length > 1)
		{
			return strArr[strArr.length - 1];
		}
		return "";
	}

	public static String getMimeType(String extension)
	{
		String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.toLowerCase(Locale.US));

		if (mimeType == null)
		{
			return "*/*";
		}

		return mimeType;
	}

	public static String stripExtension(String fileName)
	{
		checkNonNullArg(fileName);
		return fileName.replaceFirst("[.][^.]+$", "");
	}

	/**
	 * Finishes the given activity with the given data and the result set to {@link android.app.Activity#RESULT_OK
	 * Activity.RESULT_OK}
	 */
	public static void finishWithData(Activity activity, @Nullable Intent data)
	{
		checkNonNullArg(activity);
		activity.setResult(Activity.RESULT_OK, data);
		activity.finish();
	}

	// TODO: clean this
	public static String createCopyFileName(FileSystem fs, String originalName, Path.Local destFolder)
	{
		checkNonNullArgs(fs, originalName, destFolder);
		int copyCount = 0;
		String countStr = "(%1$d)";
		String noExt = stripExtension(originalName);
		Path.Local[] destContent = fs.listChildren(destFolder);
		String orgExtension = getExtension(originalName);
		
		lookuploop: while (true)
		{
			String toMatch = noExt + (copyCount == 0 ? "" : String.format(countStr, copyCount));
			copyCount++;
			for (Path.Local path : destContent)
			{	
				String currFileName = path.getName();
				if (getExtension(currFileName).equals(orgExtension))
				{
					if (stripExtension(currFileName).equals(toMatch))
					{
						continue lookuploop;
					}
				}
			}
			return toMatch + "." + orgExtension;
		}
	}

	/** Returns the maximum amount of free memory in bytes, after the heap has grown to its maximum. */
	public static long maxFreeMemory()
	{
		Runtime r = Runtime.getRuntime();
		return r.maxMemory() - usedMemory();
	}

	/** Returns the currently used memory in bytes. */
	public static long usedMemory()
	{
		Runtime r = Runtime.getRuntime();
		return r.totalMemory() - r.freeMemory();
	}

	public static File getExternalStorageAppDir()
	{
		return new File(Environment.getExternalStorageDirectory(), "Teamplace");
	}


	public static Path.Local getExternalStorageThumbnailPath()
	{
		return (Local) Path.valueOf(Path.localRoot(), getExternalStorageAppDir().getName() + "/Thumbnails");
	}


	public static Path.Local getExternalStorageAppPath()
	{
		return (Local) Path.valueOf(Path.localRoot(), getExternalStorageAppDir().getName());
	}

	public static long tomorrowStartsAt(long currentTime)
	{
		Calendar tomorrowStart = Calendar.getInstance();
		tomorrowStart.setTime(new Date(currentTime));
		tomorrowStart.set(Calendar.HOUR_OF_DAY, 0);
		tomorrowStart.set(Calendar.MINUTE, 0);
		tomorrowStart.set(Calendar.SECOND, 0);
		tomorrowStart.set(Calendar.MILLISECOND, 0);
		tomorrowStart.add(Calendar.DATE, 1);
		return tomorrowStart.getTimeInMillis();
	}

	public static boolean isConnectedToInternet(Context context){
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}
}
