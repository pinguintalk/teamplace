package net.teamplace.android.utils;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Workaround for https://github.com/chrisbanes/PhotoView/issues/4
 * 
 * @author jobol
 */
public class HackyViewPager extends ViewPager
{

	private boolean isLocked;

	public HackyViewPager(Context context)
	{
		super(context);
		isLocked = false;
	}

	public HackyViewPager(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		isLocked = false;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		if (!isLocked)
		{
			try
			{
				return super.onInterceptTouchEvent(ev);
			}
			catch (IllegalArgumentException e)
			{
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}

	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (!isLocked)
		{
			return super.onTouchEvent(event);
		}
		return false;
	}

	@Override
	protected boolean canScroll(View v, boolean checkV, int dx, int x, int y)
	{
		if (v instanceof ImageViewTouch)
		{
			return ((ImageViewTouch) v).canScroll(dx);
		}
		else
		{
			return super.canScroll(v, checkV, dx, x, y);
		}
	}

	public void toggleLock()
	{
		isLocked = !isLocked;
	}

	public void setLocked(boolean isLocked)
	{
		this.isLocked = isLocked;
	}

	public boolean isLocked()
	{
		return isLocked;
	}

}
