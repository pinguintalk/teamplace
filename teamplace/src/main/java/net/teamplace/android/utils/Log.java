package net.teamplace.android.utils;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.content.Context;

/**
 * <p>
 * Wrapper class for Logcat that logs also to a file. To use the class, subclass Application, declare the subclass name
 * in android:name attribute of <code>&lt;application&gt;</code> in the manifest file and add the following code: <code>
 * <pre>
 *    Context context = getApplicationContext();
 *    Log.setApplicationContext(context);
 * </pre> </code> Then you can enable/disable logging on a file with {@link #setLogOnFileEnabled(boolean)}, for instance
 * just after logging was started in Application subclass. The log file will be created in the private application
 * internal storage with the name specified by logFileName.
 * </p>
 * <p>
 * Logcat (console) logging can be enabled/disabled setting the LOG_ON_LOGCAT constant.
 * </p>
 * <p>
 * All methods work as the android.util.Log class.
 * </p>
 * 
 * @author Gil Vegliach
 */
public class Log
{
	private static final String TAG = tag(Log.class);

	/** Global log file name */
	public static final String logFileName = "Logfile.txt";

	/** Constant to enable or disable logging to console (through standard Logcat) */
	public static final boolean LOG_ON_LOGCAT = true;

	/** Constants */
	private static final int RETURN_VALUE_IF_NO_LOG_ON_LOGCAT = -1;
	private static final String LOG_LEVEL = "LVL: ";
	private static final String LOG_TIME = "TIME: ";
	private static final String LOG_PID = "PID: ";
	private static final String LOG_APP = "APP: ";
	private static final String LOG_TAG = "TAG: ";
	private static final String LOG_TEXT = "TEXT: ";
	private static final String LOG_SEPARATOR = "\t";
	private static final String LVL_V = "V";
	private static final String LVL_D = "D";
	private static final String LVL_I = "I";
	private static final String LVL_W = "W";
	private static final String LVL_E = "E";
	private static final String LVL_WTF = "WTF";

	/** Members */
	private static boolean sLogOnFile = false;
	private static FileOutputStream sFileOutputStream;
	private static Context sApplicationContext;
	private static String sPackageName;
	private static int sPid;

	/**
	 * We set a fixed locale in case there are multiple users with different locales. E.g. the administrator is German
	 * and the user are English and American (day and month swapped). This will be very confusing for the administrator.
	 * Nonetheless, if you want to use user's locale, set sDateFormat to: DateFormat.getDateTimeInstance();
	 */
	@SuppressLint("SimpleDateFormat")
	private static DateFormat sDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS Z");

	/** Sets application context used to retrieve info about the application such as pid and package name */
	public static void setApplicationContext(Context context)
	{
		sApplicationContext = context;
		sPackageName = sApplicationContext.getPackageName();

		// This could be set only once the Log class is loaded because the log class runs in the same process as the
		// application. Anyway, we set the pid here as it pairs up with the application (and
		// therefore with the application's context) itself, rather than to the log class
		sPid = android.os.Process.myPid();
	}

	/** Enables or disable logging */
	public static void setLogOnFileEnabled(boolean logging)
	{
		if (logging != sLogOnFile)
		{
			if (sApplicationContext == null)
			{
				throw new NullPointerException("Call setApplicationContext(Context) before enabling/disabling logging");
			}

			sLogOnFile = logging;

			if (sLogOnFile)
			{
				// Open file ...
				try
				{
					sFileOutputStream = sApplicationContext.openFileOutput(logFileName, Context.MODE_APPEND);
				}
				catch (FileNotFoundException e)
				{
					e.printStackTrace();
					sFileOutputStream = null;
					sLogOnFile = false;
				}
			}
			else
			{
				// Close file ...
				GenericUtils.closeQuietly(TAG, sFileOutputStream);
			}
		}
	}

	private static void writeToFileOutputStream(String level, String tag, String msg, Throwable tr)
	{
		try
		{
			StringBuilder sb = new StringBuilder();
			sb.append(LOG_LEVEL).append(level).append(LOG_SEPARATOR);
			sb.append(LOG_TIME).append(sDateFormat.format(System.currentTimeMillis())).append(LOG_SEPARATOR);
			sb.append(LOG_PID).append(sPid).append(LOG_SEPARATOR);
			sb.append(LOG_APP).append(sPackageName).append(LOG_SEPARATOR);
			sb.append(LOG_TAG).append(tag).append(LOG_SEPARATOR);
			sb.append(LOG_TEXT).append(msg).append("\n");
			sb.append(getStackTraceString(tr));

			sFileOutputStream.write(sb.toString().getBytes());
			sFileOutputStream.flush();
		}
		catch (IOException e)
		{
			e.printStackTrace();
			sFileOutputStream = null;
			sLogOnFile = false;
		}
	}

	private static void writeToFileOutputStream(String level, String tag, String msg)
	{
		writeToFileOutputStream(level, tag, msg, null);
	}

	public static int v(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.v(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_V, tag, msg);

		return result;
	}

	public static int v(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.v(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_V, tag, msg, tr);

		return result;
	}

	/** Wrappers */
	public static int d(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.d(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_D, tag, msg);

		return result;
	}

	public static int d(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.d(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_D, tag, msg, tr);

		return result;
	}

	public static int i(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.i(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_I, tag, msg);

		return result;
	}

	public static int i(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.i(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_I, tag, msg, tr);

		return result;
	}

	public static int e(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.d(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_E, tag, msg);

		return result;
	}

	public static int e(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.d(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_E, tag, msg, tr);

		return result;
	}

	public static int w(String tag, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.w(tag, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_W, tag, "", tr);

		return result;

	}

	public static int w(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.w(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_W, tag, msg, tr);

		return result;
	}

	public static int w(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.w(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_W, tag, msg);

		return result;
	}

	public static int wtf(String tag, String msg)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.wtf(tag, msg);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_WTF, tag, msg);

		return result;
	}

	public static int wtf(String tag, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.wtf(tag, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_WTF, tag, "", tr);

		return result;
	}

	public static int wtf(String tag, String msg, Throwable tr)
	{
		int result = RETURN_VALUE_IF_NO_LOG_ON_LOGCAT;

		if (LOG_ON_LOGCAT)
			result = android.util.Log.wtf(tag, msg, tr);

		if (sLogOnFile)
			writeToFileOutputStream(LVL_WTF, tag, msg, tr);

		return result;
	}

	public static boolean isLoggable(String tag, int level)
	{
		return android.util.Log.isLoggable(tag, level);
	}

	public static int println(int priority, String tag, String msg)
	{
		return android.util.Log.println(priority, tag, msg);
	}

	public static String getStackTraceString(Throwable tr)
	{
		return android.util.Log.getStackTraceString(tr);
	}
}
