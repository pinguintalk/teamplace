package net.teamplace.android.utils;

import net.teamplace.android.account.AccountConstants;
import net.teamplace.android.application.DatabaseLocker;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.database.util.DatabaseResetHelper;
import net.teamplace.android.initialization.InitializationActivity;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public final class ResetHelper
{
	private ResetHelper()
	{
	}

	public static void logout(Context context)
	{
		SessionManager sessionManager = new SessionManager(context);
		AccountManager accountManager = AccountManager.get(context);

		Account account = sessionManager.getLastSession().getAccount();
		sessionManager.clearAllSessions();

		accountManager.setPassword(account, null);

		context.startActivity(InitializationActivity.getIntent(context, true));
		((Activity) context).finish();
	}

	public static void resetAllCaches(Context context)
	{
		DatabaseLocker.get().lock();
		// kill session
		new SessionManager(context).killSession();

		// delete HttpLibrary DBs
		DatabaseResetHelper.killAllDatabases(context);
	}

	public static void killAccount(Context c)
	{
		AccountManager mgr = AccountManager.get(c);
		Account acc = mgr.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE)[0];

		mgr.invalidateAuthToken(AccountConstants.ACCOUNT_TYPE_WORKPLACE, null);
		mgr.removeAccount(acc, null, null);
	}

	public static void back2loginActivity(Context c)
	{
		if (c instanceof Activity)
		{
			Intent i = new Intent(c, InitializationActivity.class);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			Activity a = ((Activity) c).getParent();

			while (a != null && a.getParent() != null)
			{
				a = a.getParent();
				a.finish();
			}

			((Activity) c).startActivity(i);
			((Activity) c).finish();

		}
		else
		{
			System.out.println("# back2login: no activity");
		}
	}
}
