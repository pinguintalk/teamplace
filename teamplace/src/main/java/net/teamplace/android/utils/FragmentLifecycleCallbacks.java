package net.teamplace.android.utils;

import android.animation.Animator;
import android.app.Activity;
import android.content.ComponentCallbacks2;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * Interface for all Fragment lifecycle callbacks as of API level 21. Deprecated methods have not been added.
 * 
 * @author jobol
 */
public interface FragmentLifecycleCallbacks extends ComponentCallbacks2
{

	public void onActivityCreated(Bundle savedInstanceState);

	public void onActivityResult(int requestCode, int resultCode, Intent data);

	public void onAttach(Activity activity);

	public void onConfigurationChanged(Configuration newConfig);

	public boolean onContextItemSelected(MenuItem item);

	public void onCreate(Bundle savedInstanceState);

	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim);

	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo);

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater);

	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

	public void onDestroy();

	public void onDestroyOptionsMenu();

	public void onDestroyView();

	public void onDetach();

	public void onHiddenChanged(boolean hidden);

	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState);

	public void onLowMemory();

	public boolean onOptionsItemSelected(MenuItem item);

	public void onOptionsMenuClosed(Menu menu);

	public void onPause();

	public void onPrepareOptionsMenu(Menu menu);

	public void onResume();

	public void onSaveInstanceState(Bundle outState);

	public void onStart();

	public void onStop();

	public void onTrimMemory(int level);

	public void onViewCreated(View view, Bundle savedInstanceState);

	public void onViewStateRestored(Bundle savedInstanceState);

}
