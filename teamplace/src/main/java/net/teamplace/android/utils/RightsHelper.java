package net.teamplace.android.utils;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathOperation;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.configuration.UserRights;
import net.teamplace.android.http.teamdrive.CcsRights;
import net.teamplace.android.http.teamdrive.TeamDrive;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;

public class RightsHelper
{
	private final Map<String, TeamDrive> driveMap;
	private final Map<String, List<String>> shellMap;
	private final UserRights workplaceUserRights;

	public RightsHelper(Session session)
	{
		checkNonNullArg(session);
		this.driveMap = session.getTeamDriveMap();
		this.shellMap = session.getConfiguration().getShellObjectByType("export").getMapping();
		this.workplaceUserRights = session.getConfiguration().getUserRights();
	}

	public boolean canExportToPdf(List<FileInfo> files)
	{
		if (files.size() != 1)
		{
			return false;
		}

		if (files.get(0).isFolder())
		{
			return false;
		}

		return canExportTo(files.get(0), "pdf");
	}

	public boolean canExportToZip(List<FileInfo> files)
	{
		if (files.size() != 1)
		{
			return false;
		}

		return canExportTo(files.get(0), "zip");
	}

	public boolean canRename(List<FileInfo> files)
	{
		if (files.size() != 1)
		{
			return false;
		}

		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.RENAME);
		return files.get(0).getPath().execute(op);
	}

	public boolean canDelete(List<FileInfo> files)
	{
		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.DELETE)
		{
			@Override
			public Boolean execute(Path.Remote path)
			{
				return workplaceUserRights.isDeleteRemoteFilesAllowed();
			}
		};

		return files.get(0).getPath().execute(op);
	}

	public boolean canDelete(FileInfo file)
	{
		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.DELETE)
		{
			@Override
			public Boolean execute(Path.Remote path)
			{
				return workplaceUserRights.isDeleteRemoteFilesAllowed();
			}
		};

		return file.getPath().execute(op);
	}


	public boolean canMailAndShare(List<FileInfo> files)
	{
		if (files.size() > 1 || containsFolder(files))
		{
			return false;
		}

		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.MAIL_AND_SHARE)
		{
			@Override
			public Boolean execute(Path.Local path)
			{
				return false;
			}

			@Override
			public Boolean execute(Path.Remote path)
			{
				return workplaceUserRights.isSendByMailAllowed() || workplaceUserRights.isPrivateSharingEnabled(true)
						|| workplaceUserRights.isPublicSharingEnabled(true);
			}
		};

		return files.get(0).getPath().execute(op);
	}

	public boolean isTeamDriveTargetAllowed(List<Path> paths)
	{
		if (driveMap == null)
		{
			return false;
		}

		boolean possibleTargetsExist = false;
		for (TeamDrive drive : driveMap.values())
		{
			if (drive.getCcsRights().can(CcsRights.UPLOAD))
			{
				possibleTargetsExist = true;
				break;
			}
		}
		if (!possibleTargetsExist)
		{
			return false;
		}

		PathOperation<Boolean> op = new PathOperation<Boolean>()
		{
			@Override
			public Boolean execute(Path.Local path)
			{
				return true;
			}

			@Override
			public Boolean execute(Path.Remote path)
			{
				return workplaceUserRights.isDownloadAllowed();
			}

			@Override
			public Boolean execute(Path.TeamDrive path)
			{
				// source TeamDrive (still) exists in TeamDrive list
				return driveMap.get(path.getTeamDriveId()) != null;
			}
		};

		return paths.get(0).execute(op);
	}

	public boolean isWorkplaceTargetAllowed(List<Path> paths)
	{
		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.COPY)
		{
			@Override
			public Boolean execute(Path.Local path)
			{
				return workplaceUserRights.isUploadAllowed();
			}
		};

		return paths.get(0).execute(op);
	}

	public boolean isLocalTargetAllowed(List<Path> paths)
	{
		PathOperation<Boolean> op = new CanDoPathOperation(CcsRights.DOWNLOAD)
		{
			@Override
			public Boolean execute(Path.Remote path)
			{
				return workplaceUserRights.isDownloadAllowed();
			}
		};

		return paths.get(0).execute(op);
	}

	public TeamDrive[] filterTargetDrives(TeamDrive[] allDrives, List<Path> paths)
	{
		List<TeamDrive> targetDrives = new ArrayList<TeamDrive>();

		boolean isTeamDriveSource = paths.get(0) instanceof Path.TeamDrive;
		String sourceDriveId = isTeamDriveSource ? ((Path.TeamDrive) paths.get(0)).getTeamDriveId() : null;

		boolean teamDriveExpired;
		boolean sourceAndDestinationOnSameTeamDrive;
		boolean uploadAllowed;
		boolean copyAllowed;

		for (TeamDrive drive : allDrives)
		{
			sourceAndDestinationOnSameTeamDrive = isTeamDriveSource && drive.getId().equals(sourceDriveId);
			if (isTeamDriveSource && !sourceAndDestinationOnSameTeamDrive)
			{
				continue;
			}

			teamDriveExpired = new Date().getTime() > drive.getPayment().getPaidUntil();
			copyAllowed = drive.getCcsRights().can(CcsRights.COPY);
			uploadAllowed = drive.getCcsRights().can(CcsRights.UPLOAD);

			if (!teamDriveExpired &&
					((sourceAndDestinationOnSameTeamDrive && copyAllowed)
					|| (!sourceAndDestinationOnSameTeamDrive && uploadAllowed)))
			{
				targetDrives.add(drive);
			}
		}

		return targetDrives.toArray(new TeamDrive[targetDrives.size()]);
	}

	private boolean canExportTo(FileInfo file, final String format)
	{
		CanDoPathOperation op = new CanDoPathOperation(CcsRights.EXPORT)
		{
			@Override
			public Boolean execute(Path.Local path)
			{
				return false;
			}

			@Override
			public Boolean execute(Path.Remote path)
			{
				return isAllowedFiletype(path);
			}

			@Override
			public Boolean execute(net.teamplace.android.browsing.path.Path.TeamDrive path)
			{
				return super.execute(path) && isAllowedFiletype(path);
			}

			private boolean isAllowedFiletype(Path path)
			{
				String extension = GenericUtils.getExtension(path.getName());
				List<String> allFormats = shellMap.get("*");
				List<String> specificFormat = shellMap.get(extension);
				return (allFormats != null && allFormats.contains(format))
						|| (specificFormat != null && specificFormat.contains(format));
			}
		};

		return file.getPath().execute(op);
	}

	private boolean containsFolder(List<FileInfo> fileList)
	{
		for (FileInfo fileInfo : fileList)
		{
			if (fileInfo.isFolder())
			{
				return true;
			}
		}
		return false;
	}

	/* non-static */private class CanDoPathOperation implements PathOperation<Boolean>
	{
		private final int mFlags;

		private CanDoPathOperation(int ccsRightFlags)
		{
			mFlags = ccsRightFlags;
		}

		@Override
		public Boolean execute(Path.TeamDrive path)
		{
			TeamDrive drive = getHttpLibraryTeamDriveFromBrowsingTeamDrive(path);
			return drive != null ? drive.getCcsRights().can(mFlags) : false;
		}

		@Override
		public Boolean execute(Path.Local path)
		{
			return true;
		}

		@Override
		public Boolean execute(Path.Remote path)
		{
			return true;
		}

		private TeamDrive getHttpLibraryTeamDriveFromBrowsingTeamDrive(Path.TeamDrive path)
		{
			return driveMap != null ? driveMap.get(path.getTeamDriveId()) : null;
		}
	}
}
