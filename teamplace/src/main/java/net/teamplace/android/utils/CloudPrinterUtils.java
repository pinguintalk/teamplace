package net.teamplace.android.utils;

import java.io.File;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.openin.ContentHelper;
import net.teamplace.android.browsing.openin.DownloadFileDialogFragment;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.errorhandling.FeedbackToast;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.os.Environment;
import android.widget.Toast;

import com.cortado.android.R;

public class CloudPrinterUtils
{

	private static PackageInfo getCloudPrinterPackageInfo(Context context)
	{
		try
		{
			return context.getPackageManager().getPackageInfo(context.getString(R.string.app_cloud_printer_package),
					PackageManager.GET_ACTIVITIES);
		}
		catch (NameNotFoundException e)
		{
			return null;
		}
	}

	private static Intent buildCloudPrinterIntent(Context context, Uri localFileUri)
	{
		String cloudPrinterPackage = context.getString(R.string.app_cloud_printer_package);
		Intent i = new Intent(Intent.ACTION_SEND, localFileUri);
		i.setPackage(cloudPrinterPackage);
		return i;
	}

	private static Intent buildPlayStoreIntent(Context context, String packageName)
	{
		return new Intent(Intent.ACTION_VIEW, Uri.parse(context.getString(R.string.app_play_store_market_link) + packageName));
	}

	public static void print(Activity activity, Path toPrint)
	{
		if (getCloudPrinterPackageInfo(activity) == null)
		{

			 try {
					activity.startActivity(buildPlayStoreIntent(activity,
							activity.getString(R.string.app_cloud_printer_package)));

	            } catch (ActivityNotFoundException e) {

	                //the device hasn't installed Google Play
	                Toast.makeText(activity, activity.getString(R.string.ntf_error_googleplay_not_installed), 
	                		Toast.LENGTH_LONG).show();
	            } 
			 
		}
		else
		{
			boolean local = Path.isLocalLocation(toPrint.getLocation());
			Uri localUri = Uri.parse("file://" + (local ? ((Path.Local) toPrint).getAbsolutePathString() :
					new File(Environment.getExternalStoragePublicDirectory(
							Environment.DIRECTORY_DOWNLOADS), toPrint.getName())));
			Intent i = buildCloudPrinterIntent(activity, localUri);

			boolean fileTypeSupported = activity.getPackageManager().resolveActivity(i, 0) != null;
			if (!fileTypeSupported)
			{
				FeedbackToast.show(activity, false, activity.getString(R.string.brw_no_cloudpeinter_support));
				return;
			}

			if (local)
			{
				activity.startActivity(i);
			}
			else
			{
				DownloadFileDialogFragment dialog = DownloadFileDialogFragment.newInstance(new FileInfo(toPrint, 0, 0,
						false), i, ContentHelper.STRATEGY_DATA_URI);
				dialog.show(activity.getFragmentManager(), DownloadFileDialogFragment.TAG);
			}
		}
	}

}
