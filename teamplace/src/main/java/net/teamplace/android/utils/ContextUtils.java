package net.teamplace.android.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.WindowManager;

public class ContextUtils
{

	@SuppressLint("NewApi")
	public static DisplayMetrics getDisplayMetrics(Context ctx)
	{
		WindowManager wm = (WindowManager) ctx.getSystemService(Context.WINDOW_SERVICE);
		DisplayMetrics metrics = new DisplayMetrics();

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1)
		{
			wm.getDefaultDisplay().getRealMetrics(metrics);
		}
		else
		{
			wm.getDefaultDisplay().getMetrics(metrics);
		}
		return metrics;
	}

}
