package net.teamplace.android.utils;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.DisplayMetrics;
import android.widget.TextView;

/**
 * Utility methods for graphics and UI
 * 
 * @author Gil Vegliach
 */
public class GraphicsUtils
{
	private GraphicsUtils()
	{
	}

	/**
	 * Renders a Drawable on a Canvas backed by a Bitmap, then return this Bitmap. The Bitmap has the intrinsic size of
	 * the Drawable and density set to default (same as display's). Note: the method first
	 * {@link android.graphics.drawable.Drawable#setBounds() Drawable.setBounds()} on the Drawable then restores them
	 */
	public static Bitmap drawableToBitmap(Drawable drawable)
	{
		if (drawable instanceof BitmapDrawable)
			return ((BitmapDrawable) drawable).getBitmap();

		Rect oldBounds = drawable.copyBounds();

		Bitmap bitmap = Bitmap.createBitmap(oldBounds.width(), oldBounds.height(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);

		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);
		drawable.setBounds(oldBounds);

		return bitmap;
	}

	/**
	 * Transforms density-independent pixels to pixels, using the logical density of the screen. No rounding is
	 * performed and sub-px/dp measures are accepted
	 * 
	 * @param context
	 *            application Context from where retrieving the screen density
	 * @param dp
	 *            the amount of dp to convert
	 * @return the dp in px
	 */
	public static float dpToPx(Context context, float dp)
	{
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return dp * displayMetrics.density;
	}

	/**
	 * Sets the typeface of a given TextView from the given typeface stored in the assets folder
	 * 
	 * @param view
	 * @param typeface
	 */
	public static void setTypeface(TextView view, String typeface)
	{
		Typeface t = Typeface.createFromAsset(view.getContext().getAssets(), typeface);
		view.setTypeface(t);
	}

	/**
	 * Transforms pixels to density-independent pixels to, using the logical density of the screen. No rounding is
	 * performed and sub-px/dp measures are accepted
	 * 
	 * @param context
	 *            application Context from where retrieving the screen density
	 * @param px
	 *            the amount of px to convert
	 * @return the px in dp
	 */
	public static float pxToDp(Context context, float px)
	{
		DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
		return px / displayMetrics.density;
	}

	/**
	 * Calculates inSampleSize for BitmapFactory.Options respecting size and memory requirements
	 * 
	 * @param w
	 *            width of the bitmap
	 * @param h
	 *            height of the bitmap
	 * @param maxW
	 *            maximum width allowed
	 * @param maxH
	 *            maximum height allowed
	 * @param maxMem
	 *            maximum amount of memory allowed in bytes
	 * @return the value for inSampleSize satisfying size and memory constraints. Always greater or equal than 1
	 */
	public static int calculateInSampleSize(int w, int h, int maxW, int maxH, long maxMem)
	{
		// Both w and h will be divided by the same down-sampling factor. We need to ensure the
		// following inequalities are satisfied:
		// ... w / 2^x <= maxW ...
		// ... h / 2^x <= maxH ...
		// We can solve them independently and then take the maximum. We discard negative exponents
		double sampleSizeExpW = Math.log(((double) w) / maxW) / Math.log(2.d);
		double sampleSizeExpH = Math.log(((double) h) / maxH) / Math.log(2.d);
		int sampleSizeExpX = (int) Math.ceil(Math.max(sampleSizeExpW, Math.max(sampleSizeExpH, 0)));

		// Now sampleSizeExp is the exponent of a down-sampling factor that respects the given max
		// width and height, for instance of the largest drawable texture. We need to respect memory
		// requirements too. The inequality is:
		// ... mem / 2^2y <= maxMem ...
		// where we assume mem = w * h * 4, i.e. that the Bitmap.Config is ARGB_8888 and takes up 4
		// bytes per pixel. The solution is:
		// ... y >= log4( 4 * w * h / maxMem) = 1 + log4(w * h / maxMem) ...
		// The addition must be done before rounding because for large values of maxMem the quotient
		// (w * h) / mexMem will go to zero and thus the log to minus infinity.

		int sampleSizeExpY = (int) Math.ceil(1 + Math.log((w * h) / ((double) maxMem)) / Math.log(4.d));

		// What's left now, is only to respect both constraints and calculate the down-sampling
		// factor from its exponent
		int sampleSizeExp = Math.max(sampleSizeExpX, sampleSizeExpY);

		return (int) Math.pow(2.d, sampleSizeExp);
	}

	/**
	 * Posts a refresh on a {@code SwipeRefreshLayout}, instead of refreshing it directly. This is a known issue.
	 * <p>
	 * See: https://code.google.com/p/android/issues/detail?id=77712
	 * http://stackoverflow.com/questions/26858692/swiperefreshlayout-setrefreshing-not-showing-indicator-initially/
	 * 28217386#28217386
	 * </p>
	 */
	public static void setRefreshing(final SwipeRefreshLayout swipeRefreshLayout, final boolean isRefreshing)
	{
		checkNonNullArg(swipeRefreshLayout);
		swipeRefreshLayout.post(new Runnable()
		{
			@Override
			public void run()
			{
				swipeRefreshLayout.setRefreshing(isRefreshing);
			}
		});
	}
}
