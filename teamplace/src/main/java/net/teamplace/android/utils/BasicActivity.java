package net.teamplace.android.utils;

import net.teamplace.android.errorhandling.AuthenticationEvent;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.initialization.InitializationLoader;
import android.app.Activity;
import android.content.Intent;


import de.greenrobot.event.EventBus;

public class BasicActivity extends Activity
{
	@Override
	protected void onResume()
	{
		super.onResume();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onPause()
	{
		EventBus.getDefault().unregister(this);
		super.onPause();
	}

	public void onEventMainThread(AuthenticationEvent event)
	{
		Intent intent = new Intent(this, InitializationActivity.class);

		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

		intent.putExtra(InitializationLoader.OPTION_INVALIDATE_CACHED_TOKEN, true);

		startActivity(intent);
		finish();
	}
}
