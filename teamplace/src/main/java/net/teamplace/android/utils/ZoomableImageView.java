package net.teamplace.android.utils;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector.OnGestureListener;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.ScaleGestureDetector.OnScaleGestureListener;

/**
 * Fixes faulty swipe-to-zoom behaviour of superclass
 * 
 * @author jobol
 */
public class ZoomableImageView extends ImageViewTouch
{

	private boolean mHasScaled = false;

	public ZoomableImageView(Context context)
	{
		super(context);
	}

	public ZoomableImageView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public ZoomableImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	protected OnGestureListener getGestureListener()
	{
		return new GestureListener();
	}

	@Override
	protected OnScaleGestureListener getScaleListener()
	{
		return new ScaleListener();
	}

	private class GestureListener extends ImageViewTouch.GestureListener
	{
		@Override
		public boolean onDoubleTap(MotionEvent e)
		{
			return false;
		}

		@Override
		public boolean onDoubleTapEvent(MotionEvent e)
		{
			if (MotionEvent.ACTION_UP == e.getAction())
			{
				if (!mHasScaled)
				{
					super.onDoubleTap(e);
				}
				mHasScaled = false;
			}
			return super.onDoubleTapEvent(e);
		}
	}

	public class ScaleListener extends ImageViewTouch.ScaleListener
	{

		@Override
		public boolean onScale(ScaleGestureDetector detector)
		{
			if (mScaleEnabled)
			{
				mHasScaled = true;
			}
			return super.onScale(detector);
		}
	}

}
