package net.teamplace.android.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.teamplace.android.http.teamdrive.User;
import android.content.Context;
import android.os.SystemClock;
import android.text.TextUtils;

import com.cortado.android.R;

public class TeamDriveUtils
{
	private TeamDriveUtils()
	{
	}

	/**
	 * Formats member String in Teamdrive UI
	 * 
	 * @param context
	 *            must be non-null
	 * @param memberCount
	 *            must be >= 0
	 */
	public static String formatMemberString(Context context, int memberCount)
	{
		if (context == null)
		{
			throw new IllegalArgumentException("Context must be non-null");
		}
		if (memberCount < 0)
		{
			throw new IllegalArgumentException("memberCount must be >= 0");
		}

		if (memberCount == 0)
		{
			return "";
		}
		if (memberCount == 1)
		{
			return context.getString(R.string.tmd_member_singular);
		}

		return String.format(context.getString(R.string.tmd_member_plural), memberCount);
	}

	public static String getMemberRoleString(Context ctx, int role)
	{
		int id = 0;
		switch (role)
		{
			case User.ROLE_ADMIN:
				id = R.string.tmd_role_admin;
				break;
			case User.ROLE_MEMBER:
				id = R.string.tmd_role_member;
				break;
			case User.ROLE_OWNER:
				id = R.string.tmd_role_owner;
				break;
			case User.ROLE_READ_ONLY_MEMBER:
				id = R.string.tmd_role_readonly_member;
				break;
			case User.ROLE_MEMBER_INVITE:
				id = R.string.tmd_role_member_invite;
				break;
			default:
				return null;
		}
		return ctx.getString(id);
	}

	/**
	 * Parses a {@code CharSequence} from a Chips TextView (i.e. {@code RecipientEditTextView}) into an array of
	 * {@code User}'s
	 * 
	 * @throws InvalidEmailException
	 *             if format of contacts in cs is wrong
	 */
	public static User[] getUsersFromChipsText(CharSequence cs, int role) throws InvalidEmailException
	{
		if (TextUtils.isEmpty(cs))
		{
			return new User[0];
		}

		List<User> result = new ArrayList<User>();
		Pattern chipPattern = Pattern.compile(".*?<(.*?)>");
		String[] tokens = cs.toString().split(",");
		for (String token : tokens)
		{
			String trimmed = token.trim();
			if (TextUtils.isEmpty(trimmed))
			{
				continue;
			}

			String email = null;
			// We accept valid emails, "non-chipified" input, e.g. jan@cortado.com
			if (GenericUtils.isValidEmail(trimmed))
			{
				email = trimmed;
			}
			else
			{
				// If the text is not valid, try to extract the email from a chipified input,
				// e.g. "Johannes <johannes@cortado.com>"
				Matcher matcher = chipPattern.matcher(trimmed);
				if (matcher.find())
				{
					String candidate = matcher.group(1);
					if (GenericUtils.isValidEmail(candidate))
					{
						email = candidate;
					}
				}

				if (email == null)
				{
					// Otherwise fail
					throw new TeamDriveUtils.InvalidEmailException("Wrong format in Chips? Was: " + cs.toString());
				}
			}
			result.add(new User(email, role));

		}
		return result.toArray(new User[0]);
	}

	/**
	 * Thrown by {@link TeamDriveUtils#getUsersFromChipsText() getUsersFromChipsText()} if the format of the contact
	 * string is wrong
	 */
	public static class InvalidEmailException extends Exception
	{
		private static final long serialVersionUID = -8757871371982357233L;

		public InvalidEmailException()
		{
		}

		public InvalidEmailException(String msg)
		{
			super(msg);
		}
	}

	/**
	 * Generates a unique request code each time this method is called. At the moment the implementation only returns
	 * the current timestamp, but in the future this could change to a more robust implementation
	 */
	public static long generateRequestCode()
	{
		return SystemClock.elapsedRealtime();
	}

	public static User getOwner(User[] in)
	{
		return getUsersForRole(in, User.ROLE_OWNER)[0];
	}

	public static User[] getUsersForRole(User[] in, int role)
	{
		List<User> matches = new ArrayList<>();
		for (User user : in)
		{
			if (user.getRole() == role)
			{
				matches.add(user);
			}
		}
		return matches.toArray(new User[matches.size()]);
	}

	public static String getUserNameToDisplay(User user)
	{
		String name = user.getDisplayName();
		if (name == null || name.length() == 0)
		{
			name = user.getEmail();
		}
		return name;
	}
}
