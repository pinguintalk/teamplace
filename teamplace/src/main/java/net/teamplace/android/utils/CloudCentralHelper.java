package net.teamplace.android.utils;

import net.teamplace.android.backend.BackendServers;
import net.teamplace.android.http.session.SessionManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.cortado.android.R;

public class CloudCentralHelper
{
	public static void startTeamdriveUpgrade(Context context, String teamDriveId)
	{
		Uri.Builder builder = Uri.parse(BackendServers.ACCOUNT).buildUpon();
		builder.appendEncodedPath(String.format("r/Teamplace/Premium/%1$s", teamDriveId));
		builder.appendQueryParameter("username", getUserName(context));
		start(context, builder.build());
	}

	public static void startTeamdriveManagement(Context context, String teamDriveId)
	{
		Uri.Builder builder = Uri.parse(BackendServers.ACCOUNT).buildUpon();
		builder.appendEncodedPath(String.format("r/Teamplace/Manage/%1$s", teamDriveId));
		builder.appendQueryParameter("username", getUserName(context));
		start(context, builder.build());
	}

	public static void startMyPlaceUpgrade(Context context)
	{
		Uri.Builder builder = Uri.parse(BackendServers.ACCOUNT).buildUpon();
		builder.appendEncodedPath("r/Myplace/Premium");
		builder.appendQueryParameter("username", getUserName(context));
		start(context, builder.build());
	}

	private static void start(Context context, Uri uri)
	{
		context.startActivity(new Intent(Intent.ACTION_VIEW, uri));
	}

	private static String getUserName(Context context)
	{
		return new SessionManager(context).getLastSession().getCredentials().getUsername();
	}
}
