package net.teamplace.android.utils;

import static net.teamplace.android.utils.GenericUtils.tag;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;

/**
 * Simple Service that logs all its callbacks on {@link #TAG}. The TAG field is not final and can be changed in a
 * subclass static initializer or by {@link LoggingService#setTag(String)}. The TAG field can be retrieved by
 * {@link LoggingService#getTag()}
 * 
 * @author Gil Vegliach
 */
public class LoggingService extends Service
{
	/** Default tag on which to log. By default equal to "LoggingService" */
	public static String TAG = tag(LoggingService.class);

	@Override
	public void onCreate()
	{
		super.onCreate();
		Log.d(TAG, "onCreate()");
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId)
	{
		super.onStart(intent, startId);
		Log.d(TAG, "onStart()");
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		int result = super.onStartCommand(intent, flags, startId);
		Log.d(TAG, "onStartCommand()");
		return result;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		super.onConfigurationChanged(newConfig);
		Log.d(TAG, "onConfigurationChanged()");
	}

	@Override
	public void onLowMemory()
	{
		super.onLowMemory();
		Log.d(TAG, "onLowMemory()");
	}

	// On older Api this will not be used
	@SuppressLint("NewApi")
	@Override
	public void onTrimMemory(int level)
	{
		super.onTrimMemory(level);
		Log.d(TAG, "onTrimMemory()");
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		Log.d(TAG, "onBind()");
		return null;
	}

	@Override
	public boolean onUnbind(Intent intent)
	{
		boolean result = super.onUnbind(intent);
		Log.d(TAG, "onUnbind()");
		return result;
	}

	@Override
	public void onRebind(Intent intent)
	{
		super.onRebind(intent);
		Log.d(TAG, "onRebind()");
	}

	// On older Api this will not be used
	@SuppressLint("NewApi")
	@Override
	public void onTaskRemoved(Intent rootIntent)
	{
		super.onTaskRemoved(rootIntent);
		Log.d(TAG, "onTaskRemoved()");
	}

	public static String getTag()
	{
		return TAG;
	}

	public static void setTag(String tag)
	{
		TAG = tag;
	}
}
