package net.teamplace.android.application;

import static net.teamplace.android.utils.GenericUtils.tag;

import net.teamplace.android.account.AccountLibrary;
import net.teamplace.android.backend.ProductFlavor;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.http.session.database.SessionDatabaseActions;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;
import net.teamplace.android.http.util.DebugHelper;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;
import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Process;
import android.os.SystemClock;

import com.cortado.android.BuildConfig;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.InviteEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.crashlytics.android.core.CrashlyticsCore;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.decode.BaseImageDecoder;
import com.nostra13.universalimageloader.core.decode.ImageDecodingInfo;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import io.fabric.sdk.android.Fabric;

public class App extends Application implements Thread.UncaughtExceptionHandler
{
	public static final String TAG = tag(App.class);
	private static final int RESTART_DELAY_ON_UNCAUGHT_THROWABLE_MS = 1000;
	private static final int NO_REQUEST_CODE = -1;

	private static final int IMAGE_CACHE_SIZE = 15 * 1024 * 1024;

	private static Context sContext;
	private static volatile boolean sRootActivityHasSavedState = false;

	private static Thread.UncaughtExceptionHandler defaultExceptionHandler = Thread.getDefaultUncaughtExceptionHandler();

	@Override
	public void onCreate()
	{
		super.onCreate();
		synchronized (App.class)
		{
			sContext = this;
		}
		Log.setApplicationContext(this);
		Thread.setDefaultUncaughtExceptionHandler(this);

		initCrashReporting();
		initDatabases();
		initAccountLib();
		initUniversalImageLoader();
	}

	public synchronized static Context getContext()
	{
		if (sContext == null)
		{
			throw new IllegalStateException("App has not been initialized yet");
		}
		return sContext;
	}

	public static void setRootActivityHasSavedState(boolean hasSavedState)
	{
		sRootActivityHasSavedState = hasSavedState;
	}

	@Override
	public void uncaughtException(Thread thread, Throwable ex)
	{
		File exLog = new File(GenericUtils.getExternalStorageAppDir(), "exceptionlog.txt");
		if (exLog.exists()) {
			FileWriter writer = null;
			try {
				writer = new FileWriter(exLog, false);
				writer.write(ExceptionUtils.getStackTrace(ex));
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if (writer != null) {
					try {
						writer.close();
					} catch (IOException e) {
					}
				}
			}
		}
		defaultExceptionHandler.uncaughtException(thread, ex);
	}

	private void initAccountLib()
	{
		AccountLibrary.init(new TeamplaceBackend(), CustomAnswers.get());
	}

	private void initDatabases()
	{
		DatabaseLocker.init();
		TeamDriveDatabaseActions.init(getApplicationContext(), DatabaseLocker.get());
		SessionDatabaseActions.init(getApplicationContext(), DatabaseLocker.get());
	}

	private void initCrashReporting()
	{
		// only report in release versions
		boolean enable = ProductFlavor.ALLOW_CRASHREPORTING && !DebugHelper.isDebugVersion(this);
		CustomAnswers.init(enable);
		CrashlyticsCore crashlyticsCore =
				new CrashlyticsCore.Builder().disabled(!enable).build();
		Fabric.with(this, new Crashlytics.Builder()
				.core(crashlyticsCore).answers(CustomAnswers.get()).build());
		Crashlytics.setString("Environment", ProductFlavor.BUILD_VARIANT);
	}

	private void initUniversalImageLoader()
	{
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
				.imageDownloader(new DefaultImageDownloader(getApplicationContext())).diskCacheSize(IMAGE_CACHE_SIZE)
				.imageDecoder(new BaseImageDecoder(false) {
					@Override
					public Bitmap decode(ImageDecodingInfo decodingInfo) throws IOException {
						Bitmap bmp = super.decode(decodingInfo);
						BitmapFactory.Options opts = decodingInfo.getDecodingOptions();
						if (opts instanceof ProcessingOptions) {
							((ProcessingOptions) opts).getProcessor().process(bmp);
						}
						return bmp;
					}
				})
				.build();
		ImageLoader.getInstance().init(config);
	}
}
