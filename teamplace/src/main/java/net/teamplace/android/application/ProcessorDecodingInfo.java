package net.teamplace.android.application;

import android.graphics.BitmapFactory;

import com.nostra13.universalimageloader.core.process.BitmapProcessor;

/**
 * Created by jobol on 24/06/15.
 */
class ProcessingOptions extends BitmapFactory.Options {

    private BitmapProcessor mProcessor;

    public ProcessingOptions(BitmapProcessor processor) {
        mProcessor = processor;
    }

    public BitmapProcessor getProcessor() {
        return mProcessor;
    }
}
