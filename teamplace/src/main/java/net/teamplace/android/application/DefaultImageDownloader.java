package net.teamplace.android.application;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;

import com.nostra13.universalimageloader.core.download.BaseImageDownloader;

public class DefaultImageDownloader extends BaseImageDownloader
{

	public DefaultImageDownloader(Context context)
	{
		super(context);
	}

	public DefaultImageDownloader(Context context, int connectTimeout, int readTimeout)
	{
		super(context, connectTimeout, readTimeout);
	}

	@Override
	protected InputStream getStreamFromNetwork(String imageUri, Object extra) throws IOException
	{
		if (extra != null && extra instanceof NetworkStreamResolver)
		{
			return ((NetworkStreamResolver) extra).fromUri(context, imageUri);
		}
		else
		{
			return super.getStreamFromNetwork(imageUri, extra);
		}
	}

	public static interface NetworkStreamResolver
	{
		public InputStream fromUri(Context context, String uri) throws IOException;
	}

}
