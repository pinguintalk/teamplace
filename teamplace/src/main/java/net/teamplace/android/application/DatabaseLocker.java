package net.teamplace.android.application;

import net.teamplace.android.http.database.DatabaseAccessRestrictor;

/**
 * Created by jobol on 09/06/15.
 */
public class DatabaseLocker implements DatabaseAccessRestrictor {

    private static DatabaseLocker instance;

    private volatile boolean mIsLocked = false;

    private DatabaseLocker() {
    }

    static synchronized void init() {
        if (instance == null) {
            instance = new DatabaseLocker();
        }
        instance.unlock();
    }

    public static synchronized DatabaseLocker get() {
        if (instance == null) {
            throw new IllegalStateException("init() not called");
        }
        return instance;
    }

    public synchronized void lock() {
        mIsLocked = true;
    }

    public synchronized void unlock() {
        mIsLocked = false;
    }

    @Override
    public boolean isLocked() {
        return mIsLocked;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
}
