package net.teamplace.android.application;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.InviteEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;

/**
 * Created by jobol on 13/08/15.
 */
public class CustomAnswers extends Answers {

    private static CustomAnswers instance;

    private boolean mEnabled;

    private CustomAnswers(boolean enabled) {
        super();
        mEnabled = enabled;
    }

    public static synchronized void init(boolean enabled) {
        if (instance == null) {
            instance = new CustomAnswers(enabled);
        }
    }

    public static synchronized CustomAnswers get() {
        return instance;
    }

    @Override
    public void logInvite(InviteEvent event) {
        if (mEnabled) {
            super.logInvite(event);
        }
    }

    @Override
    public void logLogin(LoginEvent event) {
        if (mEnabled) {
            super.logLogin(event);
        }
    }

    @Override
    public void logCustom(CustomEvent event) {
        if (mEnabled) {
            super.logCustom(event);
        }
    }

    @Override
    public void logSignUp(SignUpEvent event) {
        if (mEnabled) {
            super.logSignUp(event);
        }
    }
}
