package net.teamplace.android.browsing.openin;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.utils.GenericUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is to pass Activity Resolving functionality dynamically into ResolvingActivityDialog. We can't pass
 * arbitrary objects (shouldn't use parameterized constructors in Fragments and arguments must implement Parcelable). So
 * get() instantiates different implementations based on identifier ints (which can be passed as args to the Fragment).
 * 
 * @author jobol
 */
public abstract class ActivityResolver
{

	public static ActivityResolver get(int type)
	{
		switch (type)
		{
			case ViewOrShareResolver.TYPE:
				return new ViewOrShareResolver();
			default:
				return null;
		}
	}

	protected abstract List<ResolveInfoForIntent> resolveActivities(Context context, FileInfo info);

	protected abstract void onResolveInfoSelected(Activity context, ResolveInfoForIntent resolveInfoForIntent,
			FileInfo info);

	/**
	 * Merges based on Intent action and package name
	 * 
	 * @param listsToMerge
	 *            the lower the index, the higher the priority
	 * @return
	 */
	protected List<ResolveInfoForIntent> mergeLists(List<ResolveInfoForIntent>[] listsToMerge)
	{
		List<ResolveInfoForIntent> mergedList = new ArrayList<ResolveInfoForIntent>();

		for (List<ResolveInfoForIntent> list : listsToMerge)
		{
			toAddLoop: for (ResolveInfoForIntent toAdd : list)
			{
				for (ResolveInfoForIntent info : mergedList)
				{
					if (info.getIntent().getAction().equals(toAdd.getIntent().getAction()) &&
							info.getResolveInfo().activityInfo.applicationInfo.packageName
									.equals(toAdd.getResolveInfo().activityInfo.applicationInfo.packageName))
					{
						continue toAddLoop;
					}
				}
				mergedList.add(toAdd);
			}
		}
		return mergedList;
	}

	protected Intent buildQueryIntent(String action, FileInfo info, int strategy)
	{
		Intent intent = new Intent(action);
		intent.setType(getMimeType(info));
		return ContentHelper.setFilePathContent(intent, info.getName(), strategy);
	}

	protected List<ResolveInfoForIntent> buildResolveInfoForIntentList(Context context, List<ResolveInfo> resolveList,
			Intent intent,
			int strategy)
	{
		List<ResolveInfoForIntent> resultList = new ArrayList<ResolveInfoForIntent>(resolveList.size());
		String appPkg = context.getApplicationContext().getPackageName();
		for (ResolveInfo resolveInfo : resolveList)
		{
			if (!resolveInfo.activityInfo.applicationInfo.packageName.equals(appPkg))
			{
				resultList.add(new ResolveInfoForIntent(intent, resolveInfo, strategy));
			}
		}
		return resultList;
	}

	protected String getMimeType(FileInfo info)
	{
		String extension = GenericUtils.getExtension(info.getName());
		return GenericUtils.getMimeType(extension);
	}

	// ++++++++++++++ Implementations: ++++++++++++++++++++++

	public static class ViewOrShareResolver extends ActivityResolver
	{
		public static final int TYPE = 0;

		@SuppressWarnings("unchecked")
		@Override
		protected List<ResolveInfoForIntent> resolveActivities(Context context, FileInfo info)
		{
			Intent intentViewActionContentUri = buildQueryIntent(Intent.ACTION_VIEW, info,
					ContentHelper.STRATEGY_DATA_URI);
			List<ResolveInfoForIntent> listViewActionContentUri = buildResolveInfoForIntentList(context, context
					.getPackageManager().queryIntentActivities(intentViewActionContentUri, 0),
					intentViewActionContentUri,
					ContentHelper.STRATEGY_DATA_URI);
			/*
			Intent intentViewActionStreamExtra = buildQueryIntent(Intent.ACTION_VIEW, info,
					ContentHelper.STRATEGY_STREAM_EXTRA);
			List<ResolveInfoForIntent> listViewActionStreamExtra = buildResolveInfoForIntentList(context, context
					.getPackageManager().queryIntentActivities(intentViewActionStreamExtra, 0),
					intentViewActionStreamExtra, ContentHelper.STRATEGY_STREAM_EXTRA); */

			Intent intentSendActionContentUri = buildQueryIntent(Intent.ACTION_SEND, info,
					ContentHelper.STRATEGY_DATA_URI);
			List<ResolveInfoForIntent> listSendActionContentUri = buildResolveInfoForIntentList(context, context
					.getPackageManager().queryIntentActivities(intentSendActionContentUri, 0),
					intentSendActionContentUri,
					ContentHelper.STRATEGY_DATA_URI);

			Intent intentSendActionStreamExtra = buildQueryIntent(Intent.ACTION_SEND, info,
					ContentHelper.STRATEGY_STREAM_EXTRA);
			List<ResolveInfoForIntent> listSendActionStreamExtra = buildResolveInfoForIntentList(context, context
					.getPackageManager().queryIntentActivities(intentSendActionStreamExtra, 0),
					intentSendActionStreamExtra,
					ContentHelper.STRATEGY_STREAM_EXTRA);

			return mergeLists((List<ResolveInfoForIntent>[]) new List[] {listViewActionContentUri,
					listSendActionStreamExtra, listSendActionContentUri});
		}

		@Override
		protected void onResolveInfoSelected(Activity context, ResolveInfoForIntent resolveInfoForIntent, FileInfo info)
		{
			Intent intent = resolveInfoForIntent.getIntent();
			ResolveInfo resolveInfo = resolveInfoForIntent.getResolveInfo();
			int contentStrategy = resolveInfoForIntent.getContentStrategy();
			intent.setComponent(new ComponentName(resolveInfo.activityInfo.applicationInfo.packageName,
					resolveInfo.activityInfo.name));

			if (info.getPath() instanceof Path.Local)
			{
				context.startActivity(
						ContentHelper.setFilePathContent(intent, ((Path.Local) info.getPath()).getAbsolutePathString(),
								contentStrategy));
			}
			else
			{
				DownloadFileDialogFragment dialog = DownloadFileDialogFragment.newInstance(info, intent,
						contentStrategy);
				dialog.show(context.getFragmentManager(), DownloadFileDialogFragment.TAG);
			}
		}

	}
}
