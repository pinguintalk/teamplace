package net.teamplace.android.browsing.path;

/** Thrown when a pathname given as a String cannot be converted to a Path.Local */
public class LocalPathNotRecognizedException extends Exception
{
	private static final long serialVersionUID = -264322782788437732L;

	public LocalPathNotRecognizedException()
	{
		super();
	}

	public LocalPathNotRecognizedException(String detailMessage, Throwable throwable)
	{
		super(detailMessage, throwable);
	}

	public LocalPathNotRecognizedException(String detailMessage)
	{
		super(detailMessage);
	}

	public LocalPathNotRecognizedException(Throwable throwable)
	{
		super(throwable);
	}
}
