package net.teamplace.android.browsing.dialog;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;

import com.cortado.android.R;
import com.forgottensystems.multiimagechooserV2.MultiImageChooserActivity;

import net.teamplace.android.browsing.path.Path;

import java.util.HashMap;

import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Lets the user choose among three actions: new folder, pick a photo, pick a file. The target to this fragment will be
 * passed on to the new folder and the file picker dialogs, and will be used for onActivityResult() in the case of the
 * image picker.
 * 
 * @author Gil Vegliach
 */
public class NewChooserDialogFragment extends DialogFragment
{
	public static final String TAG = tag(NewChooserDialogFragment.class);
	public static final int REQUEST_CODE_NEW_FOLDER = R.id.request_code_new_folder;
	public static final int REQUEST_CODE_IMAGE_PICKER = R.id.request_code_image_picker;
	public static final int REQUEST_CODE_FILE_PICKER = FilePickerDialogFragment.REQUEST_CODE;
	private static final String KEY_ILLEGAL_INPUTS = "illegalInputs";
	private HashMap<String, String> illegalInputsForFolder; // for folder only

	public static NewChooserDialogFragment newInstance(HashMap<String, String> illegalInputs)
	{
		NewChooserDialogFragment frag = new NewChooserDialogFragment();
		Bundle args = new Bundle();
		args.putSerializable(KEY_ILLEGAL_INPUTS, illegalInputs);
		frag.setArguments(args);
		return frag;
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Context ctx = getActivity();
		StyleableAlertDialog dlg = new StyleableAlertDialog(ctx);
		LayoutInflater inflater = LayoutInflater.from(ctx);
		View v = inflater.inflate(R.layout.flp_new_chooser_dialog, null);

		View newFolder = v.findViewById(R.id.new_folder);
		View imagePicker = v.findViewById(R.id.image_picker);
		View filePicker = v.findViewById(R.id.file_picker);

		Bundle args = getArguments();
		illegalInputsForFolder = (HashMap<String, String>) args.getSerializable(KEY_ILLEGAL_INPUTS);

		newFolder.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String title = getString(R.string.brw_action_new_folder);
				String text = getString(R.string.brw_new_folder_default_name);
				DialogFragment dlg = EditTextDialogFragment.newInstance(title, text, true, illegalInputsForFolder);
				dlg.setTargetFragment(getTargetFragment(), REQUEST_CODE_NEW_FOLDER);
				dlg.show(getFragmentManager(), EditTextDialogFragment.TAG);

				dismiss();
			}
		});

		imagePicker.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				// Will call onActivityResult() on target
				Fragment target = getTargetFragment();
				Intent i = new Intent(getActivity(), MultiImageChooserActivity.class);
				target.startActivityForResult(i, REQUEST_CODE_IMAGE_PICKER);

				dismiss();
			}
		});

		filePicker.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Fragment target = getTargetFragment();
				DialogFragment dlg = FilePickerDialogFragment.newInstance(Path.localRoot());
				dlg.setTargetFragment(target, FilePickerDialogFragment.REQUEST_CODE);
				dlg.show(getFragmentManager(), FilePickerDialogFragment.TAG);

				dismiss();
			}
		});

		dlg.setView(v);
		return dlg;
	}
}
