package net.teamplace.android.browsing;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Pair;
import android.widget.Toast;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.openin.DownloadFileDialogFragment;
import net.teamplace.android.browsing.path.FileSystem;
import net.teamplace.android.browsing.path.FileSystemImpl;
import net.teamplace.android.browsing.path.LocalPathNotRecognizedException;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.browsing.path.Path.Remote;
import net.teamplace.android.browsing.path.Path.TeamDrive;
import net.teamplace.android.browsing.path.PathOperation;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.sorting.SortManager;
import net.teamplace.android.browsing.ui.NotificationHelper;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.content.IntentBus;
import net.teamplace.android.content.IntentBusImpl;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.bookmark.Json;
import net.teamplace.android.http.bookmark.Json.EnumerateBookmarks.Bookmark;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.FileWasModifiedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.file.RemoteArchive;
import net.teamplace.android.http.file.RemoteFile;
import net.teamplace.android.http.file.RemoteFolder;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.request.browse.BrowseRequester;
import net.teamplace.android.http.request.faulttolerant.NetworkOperationCallback;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.download.DownloadRequester;
import net.teamplace.android.http.request.faulttolerant.upload.FaultTolerantUploadManager;
import net.teamplace.android.http.request.files.ExportRequester;
import net.teamplace.android.http.request.files.FileRequester;
import net.teamplace.android.http.request.smartfiling.SmartFilingLocation;
import net.teamplace.android.http.request.smartfiling.SmartFilingRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.providers.http.BrowseRequesterProvider;
import net.teamplace.android.providers.http.ExportRequesterProvider;
import net.teamplace.android.providers.http.FileRequesterProvider;
import net.teamplace.android.providers.http.SmartFilingRequesterProvider;
import net.teamplace.android.time.SystemTimeSource;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Performs network requests in the background. Inserts results in FileProvider when finished
 * 
 * @author Gil Vegliach
 */
public class BrowsingService extends IntentService
{
	private static final String PKG = pkg(BrowsingService.class);
	private static final String TAG = tag(BrowsingService.class);

	public static class Contract
	{

		public static Intent buildCheckRemoteFolderExists(Context ctx, Path filePath) {
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(REQUEST_CODE, REQUEST_CHECK_FOLDER_EXISTS);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, filePath);
			i.putExtra(KEY_TEAMDRIVE_ID, PathUtils.getTeamDriveIdOrNull(filePath));
			return i;
		}

		public static Intent buildBrowseRequestIntent(Context ctx, Path folder)
		{
			checkNonNullArgs(ctx, folder);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_BROWSE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, folder);
			return i;
		}

		public static Intent buildMkdirRequestIntent(Context ctx, Path folder)
		{
			checkNonNullArgs(ctx, folder);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_MKDIR);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, folder);
			return i;
		}

		public static Intent buildRenameRequestIntent(Context ctx, Path folder, String newName)
		{
			checkNonNullArgs(ctx, folder);
			checkCondition(!TextUtils.isEmpty(newName));

			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_RENAME);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, folder);
			i.putExtra(KEY_NEW_NAME, newName);
			return i;
		}

		public static Intent buildDeleteRequestIntent(Context ctx, Path[] paths)
		{
			checkNonNullArgs(ctx, paths);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_DELETE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATHS, paths);
			return i;
		}

		/** dest is the destination directory without the file name */
		public static Intent buildCopyRequestIntent(Context ctx, Path[] sources, Path dest)
		{
			checkNonNullArgs(ctx, sources, dest);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_COPY);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATHS, sources);
			i.putExtra(KEY_PATH_DEST, dest);
			return i;
		}

		public static Intent buildMoveRequestIntent(Context ctx, Path[] sources, Path dest)
		{
			checkNonNullArgs(ctx, sources, dest);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_MOVE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATHS, sources);
			i.putExtra(KEY_PATH_DEST, dest);
			return i;
		}

		public static Intent buildExportToPdfIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_EXPORT_TO_PDF);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		public static Intent buildExportToZipIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_EXPORT_TO_ZIP);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		public static Intent buildCheckUploadLocationsRequestIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_CHECK_UPLOAD_LOCATIONS);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		// TODO Unused - check if this can be removed
		public static Intent buildUploadPictureRequestIntent(Context ctx, ArrayList<String> imagePathList, Path dest)
		{
			checkNonNullArgs(ctx, imagePathList, dest);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_UPLOAD_PICTURE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH_DEST, dest);
			i.putExtra(KEY_PATHS, imagePathList);
			return i;
		}

		public static Intent buildAddFavoriteRequestIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_ADD_FAVORITE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		public static Intent buildDeleteFavoriteRequestIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_DELETE_FAVORITE);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		public static Intent buildEnumerateFavoriteSRequestIntent(Context ctx)
		{
			checkNonNullArg(ctx);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_ENUMERATE_FAVORITES);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			return i;
		}

		public static Intent buildSetSortRequestIntent(Context ctx, Path path, int category, int order)
		{
			checkNonNullArg(ctx);
			SortManager.checkCategory(category);
			SortManager.checkOrder(order);

			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_SET_SORT);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			i.putExtra(KEY_SORT_CATEGORY, category);
			i.putExtra(KEY_SORT_ORDER, order);
			return i;
		}

		public static Intent buildGetSortRequestIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_GET_SORT);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		public static Intent buildCancelCopyMoveIntent(Context ctx)
		{
			checkNonNullArgs(ctx);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_CANCEL_MOVE_COPY);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			return i;
		}

		public static Intent buildExtractVersionRequestIntent(Context ctx, Path versionPath, long versionDate)
		{
			checkNonNullArgs(ctx, versionPath);
			Intent i = new Intent(ctx, BrowsingService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_EXTRACT_VERSION);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, versionPath);
			i.putExtra(KEY_DATE, versionDate);
			return i;
		}

		/** Extracts the request id from a request or response given in form of an Intent */
		public static int extractRequestId(Intent intent)
		{
			if (intent == null)
			{
				return NO_ID;
			}

			int id = intent.getIntExtra(REQUEST_ID, NO_ID);
			if (id != NO_ID)
			{
				return id;
			}

			// This is a response, try to extract the data from RESPONSE_REQUEST_EXTRAS
			Bundle extras = intent.getBundleExtra(RESPONSE_REQUEST_EXTRAS);
			if (extras == null)
			{
				return NO_ID;
			}

			return extras.getInt(REQUEST_ID, NO_ID);
		}

		/** Extracts the parameter newName from a rename request or a response to such a request */
		public static String extractNewName(Intent intent)
		{
			if (intent == null)
			{
				return null;
			}

			String newName = intent.getStringExtra(KEY_NEW_NAME);
			if (newName != null)
			{
				return newName;
			}

			Bundle extras = intent.getBundleExtra(RESPONSE_REQUEST_EXTRAS);
			if (extras == null)
			{
				return null;
			}

			return extras.getString(KEY_NEW_NAME);
		}

		public static final String REQUEST_CODE = PKG + ".requestCode";
		public static final String REQUEST_ID = PKG + "." + "id";
		public static final int NO_ID = IdGenerator.NO_ID;

		public static final int REQUEST_BROWSE = 100;
		public static final int REQUEST_MKDIR = 101;
		public static final int REQUEST_DELETE = 102;
		public static final int REQUEST_COPY = 103;
		public static final int REQUEST_MOVE = 104;
		public static final int REQUEST_CHECK_UPLOAD_LOCATIONS = 105;
		public static final int REQUEST_RENAME = 106;
		public static final int REQUEST_EXPORT_TO_PDF = 107;
		public static final int REQUEST_EXPORT_TO_ZIP = 108;
		public static final int REQUEST_UPLOAD_PICTURE = 109; // TODO Unused - check if this can be removed
		public static final int REQUEST_ADD_FAVORITE = 110;
		public static final int REQUEST_DELETE_FAVORITE = 111;
		public static final int REQUEST_ENUMERATE_FAVORITES = 112;
		public static final int REQUEST_SET_SORT = 113;
		public static final int REQUEST_GET_SORT = 114;
		public static final int REQUEST_CANCEL_MOVE_COPY = 115;
		public static final int REQUEST_EXTRACT_VERSION = 116;
		public static final int REQUEST_UPLOAD = 117;
		public static final int REQUEST_CHECK_FOLDER_EXISTS = 119;

		public static final String ACTION_RESPONSE_BROWSE = PKG + ".ACTION_RESPONSE_BROWSE";
		public static final String ACTION_RESPONSE_MKDIR = PKG + ".ACTION_RESPONSE_MKDIR";
		public static final String ACTION_RESPONSE_DELETE = PKG + ".ACTION_RESPONSE_DELETE";
		public static final String ACTION_RESPONSE_COPY = PKG + ".ACTION_RESPONSE_COPY";
		public static final String ACTION_RESPONSE_MOVE = PKG + ".ACTION_RESPONSE_MOVE";
		public static final String ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS = PKG
				+ ".ACTION_RESPONSE_CHECK_UPLOAD_LOCATION";
		public static final String ACTION_RESPONSE_UPLOAD_PICTURE = PKG + ".ACTION_RESPONSE_UPLOAD_PICTURE";
		public static final String ACTION_RESPONSE_RENAME = PKG + ".ACTION_RESPONSE_RENAME";
		public static final String ACTION_RESPONSE_EXPORT_TO_PDF = PKG + ".ACTION_RESPONSE_EXPORT_TO_PDF";
		public static final String ACTION_RESPONSE_EXPORT_TO_ZIP = PKG + ".ACTION_RESPONSE_EXPORT_TO_ZIP";
		public static final String ACTION_RESPONSE_ADD_FAVORITE = PKG + ".ACTION_RESPONSE_ADD_FAVORITE";
		public static final String ACTION_RESPONSE_DELETE_FAVORITE = PKG + ".ACTION_RESPONSE_DELETE_FAVORITE";
		public static final String ACTION_RESPONSE_ENUMERATE_FAVORITES = PKG + ".ACTION_RESPONSE_ENUMERATE_FAVORITES";
		public static final String ACTION_RESPONSE_SET_SORT = PKG + ".ACTION_RESPONSE_SET_SORT";
		public static final String ACTION_RESPONSE_GET_SORT = PKG + ".ACTION_RESPONSE_GET_SORT";
		public static final String ACTION_RESPONSE_EXTRACT_VERSION = PKG + ".ACTION_RESPONSE_EXTRACT_VERSION";
		public static final String ACTION_RESPONSE_GET_THUMBNAIL = PKG + ".ACTION_RESPONSE_GET_THUMBNAIL";
		public static final String ACTION_RESPONSE_FETCH_FOLDER_CONTENT_IF_EXISTS = PKG + ".ACTION_RESPONSE_FETCH_FOLDER_CONTENT_IF_EXISTS";

		public static final String RESPONSE_REQUEST_EXTRAS = PKG + ".requestExtras";
		public static final String RESPONSE_BROWSE_FOLDER_CONTENT = PKG + ".folderContent";
		public static final String RESPONSE_FAVORITES = PKG + ".favorites";
		public static final String RESPONSE_SORT_CATEGORY = PKG + ".category";
		public static final String RESPONSE_SORT_ORDER = PKG + ".order";
		public static final String RESPONSE_FETCH_FOLDER_IF_EXISTS =  PKG + ".fetchRemoteFolderIfExists";;

		/**
		 * Targets are sorted, 'best first'. A target T is better than a target S iff T was downloaded on this device
		 * and S wasn't, or both T and S were downloaded on this device but T was downloaded after S, or both T and S
		 * were not downloaded on this device but T was downloaded (on another device) after S.
		 */
		public static final String RESPONSE_CHECK_UPLOAD_LOCATIONS_TARGETS = PKG + ".mTargets";
	}

	/** Local folder sizes are always 0, for speed reasons. In the UI they are not shown */
	private static final int DEFAULT_LOCAL_FOLDER_SIZE = 0;

	/** The depth of the tree the browse request returns */
	private static final int BROWSE_REQUEST_DEPTH = 1;

	/** Does not force overwrite during copy or move of a file */
	private static final int DONT_FORCE_OVERWRITE = ServerParams.OVERWRITE_DONT;

	/** Forces overwrite during copy or move of a file */
	private static final int FORCE_OVERWRITE = ServerParams.OVERWRITE_FORCE;

	/** Forces renaming a file during copy / move, if an equally named file exists */
	private static final int FORCE_NEW_NAME = ServerParams.OVERWRITE_NEW_NAME;

	/** Forces creating a new version during copy / move, if an equally named file exists */
	private static final int FORCE_NEW_VERSION = ServerParams.OVERWRITE_NEW_VERSION;

	private static final String WORKER = "worker";
	private static final String KEY_TEAMDRIVE_ID = PKG + ".teamdriveID";
	private static final String KEY_PATH = PKG + ".path";
	private static final String KEY_PATHS = PKG + ".paths";
	private static final String KEY_PATH_DEST = PKG + ".pathDest";
	private static final String KEY_NEW_NAME = PKG + ".newName";
	private static final String KEY_SORT_CATEGORY = PKG + ".category";
	private static final String KEY_SORT_ORDER = PKG + ".order";
	private static final String KEY_DATE = PKG + ".date";
	private static final String KEY_JOB_TIMESTAMP = PKG + ".jobId";
	private static final int PROGRESS_PERCENT_START = 0;
	private static final int PROGRESS_PERCENT_DONE = 100;
	private static final Path.Local.FolderFirstComparator LOCAL_PATH_FOLDER_FIRST_COMPARATOR = new Path.Local.FolderFirstComparator();

	private final static Comparator<SmartFilingLocation> BEST_SMART_TARGET_FIRST_COMPARATOR = new Comparator<SmartFilingLocation>()
	{
		@Override
		public int compare(SmartFilingLocation lhs, SmartFilingLocation rhs)
		{
			boolean lorigin = lhs.isOrigin();
			boolean rorigin = rhs.isOrigin();
			if (lorigin && !rorigin)
			{
				return -1;
			}
			if (!lorigin && rorigin)
			{
				return +1;
			}

			long ldate = lhs.getAccessed();
			long rdate = rhs.getAccessed();
			if (ldate == rdate)
			{
				return 0;
			}

			return ldate > rdate ? -1 : +1;
		}
	};

	// Memory issues are not a concern here
	@SuppressLint("UseSparseArrays")
	private final Map<Long, FileJobInfo> mJobQueue = new HashMap<Long, FileJobInfo>();
	// private final AtomicBoolean mCancelCopyOrMoveRequested = new AtomicBoolean(false);
	private final AtomicLong mLastCanceled = new AtomicLong(0l);
	private final AtomicLong mOngoingJobTimestamp = new AtomicLong(0l);

	private Dependencies mDependencies;

	public interface Dependencies extends BrowseRequesterProvider,
			FileRequesterProvider,
			SmartFilingRequesterProvider,
			ExportRequesterProvider
	{
		TimeSource provideTimeSource();

		FileSystem provideFileSystem();

		EventBus provideEventBus();

		IntentBus provideIntentBus(Context context);

		NotificationHelper provideNotificationHelper();
	}

	private synchronized Dependencies getDependencies()
	{
		if (mDependencies == null)
		{
			mDependencies = new DependenciesImpl();
		}
		return mDependencies;
	}

	/** @hide Used by tests to inject dependencies. Don't use in production */
	public synchronized void setDependencies(Dependencies dependencies)
	{
		mDependencies = dependencies;
	}

	public BrowsingService()
	{
		super(WORKER);
	}

	@Override
	public void onStart(Intent intent, int startId)
	{
		int requestCode = intent.getIntExtra(Contract.REQUEST_CODE, -1);
		if (requestCode == Contract.REQUEST_CANCEL_MOVE_COPY)
		{
			Toast.makeText(getApplicationContext(), "Canceled", Toast.LENGTH_SHORT).show();
			synchronized (this)
			{
				// mCancelCopyOrMoveRequested.set(true);
				mLastCanceled.set(System.currentTimeMillis());
				for (Iterator<Entry<Long, FileJobInfo>> iterator = mJobQueue.entrySet().iterator(); iterator.hasNext();)
				{
					if (iterator.next().getKey() != mOngoingJobTimestamp.get())
					{
						iterator.remove();
					}
				}
			}
			return;
		}
		else if (requestCode == Contract.REQUEST_MOVE || requestCode == Contract.REQUEST_COPY)
		{
			// mCancelCopyOrMoveRequested.set(false);
			Parcelable[] srcPaths = intent.getParcelableArrayExtra(KEY_PATHS);
			TimeSource timeSource = getDependencies().provideTimeSource();
			long created = timeSource.currentTimeMillis();
			synchronized (this)
			{
				Path destPath = (Path) intent.getParcelableExtra(KEY_PATH_DEST);
				Path[] paths = Arrays.copyOf(srcPaths, srcPaths.length, Path[].class);
				PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
						paths, FileJobInfo.STATE_QUEUED, PROGRESS_PERCENT_START);
				FileJobInfo jobInfo = new FileJobInfo(created, requestCode, pathStateInfo, destPath);
				mJobQueue.put(created, jobInfo);
			}
			intent.putExtra(KEY_JOB_TIMESTAMP, created);
		}
		super.onStart(intent, startId);
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		Bundle extras = intent.getExtras();
		int type = extras.getInt(Contract.REQUEST_CODE);
		if (type == Contract.REQUEST_BROWSE)
		{
			handleBrowse(extras);
		}
		else if (type == Contract.REQUEST_MKDIR)
		{
			handleMkdir(extras);
		}
		else if (type == Contract.REQUEST_RENAME)
		{
			handleRename(extras);
		}
		else if (type == Contract.REQUEST_DELETE)
		{
			handleDelete(extras);
		}
		else if (type == Contract.REQUEST_COPY)
		{
			handleCopy(extras);
		}
		else if (type == Contract.REQUEST_MOVE)
		{
			handleMove(extras);
		}
		else if (type == Contract.REQUEST_EXPORT_TO_PDF)
		{
			handleExportToPdf(extras);
		}
		else if (type == Contract.REQUEST_EXPORT_TO_ZIP)
		{
			handleExportToZip(extras);
		}
		else if (type == Contract.REQUEST_CHECK_UPLOAD_LOCATIONS)
		{
			handleCheckUploadLocations(extras);
		}
		else if (type == Contract.REQUEST_UPLOAD_PICTURE)
		{
			// TODO Unused - check if this can be removed
			handleUploadPicture(extras);
		}
		else if (type == Contract.REQUEST_ADD_FAVORITE)
		{
			handleAddFavorite(extras);
		}
		else if (type == Contract.REQUEST_DELETE_FAVORITE)
		{
			handleDeleteFavorite(extras);
		}
		else if (type == Contract.REQUEST_ENUMERATE_FAVORITES)
		{
			handleEnumerateFavorites(extras);
		}
		else if (type == Contract.REQUEST_SET_SORT)
		{
			handleSetSort(extras);
		}
		else if (type == Contract.REQUEST_GET_SORT)
		{
			handleGetSort(extras);
		}
		else if (type == Contract.REQUEST_EXTRACT_VERSION)
		{
			handleExtractVersion(extras);
		}
		else if (type == Contract.REQUEST_CHECK_FOLDER_EXISTS)
		{
			handleFetchFolderContentIfExists(extras);
		}
		else
		{
			throw new IllegalArgumentException("Request type not recognized");
		}
	}

	private void handleBrowse(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		Pair<Integer, Integer> p = SortManager.getSort(this, path);

		int category = p.first;
		int order = p.second;
		ArrayList<FileInfo> content = browse(path);
		SortManager.sort(content, category, order);

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_BROWSE, extras);
		i.putExtra(Contract.RESPONSE_SORT_CATEGORY, category);
		i.putExtra(Contract.RESPONSE_SORT_ORDER, order);
		i.putParcelableArrayListExtra(Contract.RESPONSE_BROWSE_FOLDER_CONTENT, content);
		sendLocalBroadcast(i);
	}


	private void handleMkdir(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		mkdir(path);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_MKDIR, extras);
		sendLocalBroadcast(i);
	}

	private void handleRename(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		String newName = extras.getString(KEY_NEW_NAME);
		rename(path, newName);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_RENAME, extras);
		sendLocalBroadcast(i);
	}

	private void handleDelete(Bundle extras)
	{
		Parcelable[] parcelables = extras.getParcelableArray(KEY_PATHS);
		// TODO: check this cast
		Path[] paths = Arrays.copyOf(parcelables, parcelables.length, Path[].class);
		if (paths == null || paths.length == 0)
		{
			return;
		}

		PathUtils.checkSameLocationOrThrow(paths);

		// TODO: @giveg is this even needed? The paths look like coming from the same location
		// Sieves local paths from remote paths
		int lcount = 0;
		int len = paths.length;
		for (int i = 0; i < len; i++)
		{
			if (paths[i] instanceof Path.Local)
			{
				lcount++;
			}
		}
		Path.Local[] lpaths = new Path.Local[lcount];
		Path[] rpaths = new Path[len - lcount];
		for (int i = 0, j = 0, k = 0; i < len; i++)
		{
			if (paths[i] instanceof Path.Local)
			{
				lpaths[j++] = (Path.Local) paths[i];
			}
			else
			{
				rpaths[k++] = paths[i];
			}
		}

		deleteRemote(rpaths);
		deleteLocal(lpaths);

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_DELETE, extras);
		sendLocalBroadcast(i);
	}

	private void handleCopy(Bundle extras)
	{
		handleCopyOrMove(extras, Contract.REQUEST_COPY);
	}

	private void handleMove(Bundle extras)
	{
		handleCopyOrMove(extras, Contract.REQUEST_MOVE);
	}

	private void handleExportToPdf(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		export(path, path.getName() + "." + ExportRequester.VALUE_EXPORT_FORMAT_PDF,
				ExportRequester.VALUE_EXPORT_FORMAT_PDF);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_EXPORT_TO_PDF, extras);
		sendLocalBroadcast(i);
	}

	private void handleExportToZip(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		export(path, path.getName() + "." + ExportRequester.VALUE_EXPORT_FORMAT_ZIP,
				ExportRequester.VALUE_EXPORT_FORMAT_ZIP);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_EXPORT_TO_ZIP, extras);
		sendLocalBroadcast(i);
	}

	private void handleCheckUploadLocations(Bundle extras)
	{
		// TODO: @giveg check this to-do, it might be obsolete. At the moment, the requester only works on the
		// workplace server
		ArrayList<Path> targets = new ArrayList<Path>();
		try
		{
			Path path = extras.getParcelable(KEY_PATH);
			SmartFilingRequester requester = getDependencies().provideSmartFilingRequester(this);
			List<SmartFilingLocation> locations = requester.requestSmartFilingLocations(path.getName());
			Collections.sort(locations, BEST_SMART_TARGET_FIRST_COMPARATOR);

			for (SmartFilingLocation location : GenericUtils.emptyIfNull(locations))
			{
				String teamDriveId = location.getTeamDriveId();
				String pathLocation = PathUtils.pathLocationFromTeamDriveId(teamDriveId);
				targets.add(PathUtils.buildPathfromServerResponseString(pathLocation, location.getPath()));
			}
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS, extras);
		i.putParcelableArrayListExtra(Contract.RESPONSE_CHECK_UPLOAD_LOCATIONS_TARGETS, targets);
		sendLocalBroadcast(i);
	}

	private void handleUploadPicture(Bundle extras)
	{
		Path dest = extras.getParcelable(KEY_PATH_DEST);
		checkCondition(!(dest instanceof Path.Local), "Can't upload picture to local path, was: " + dest);

		try
		{
			ArrayList<String> imagePathList = extras.getStringArrayList(KEY_PATHS);
			ArrayList<Path.Local> localPathList = new ArrayList<Path.Local>();

			for (String absolutePath : imagePathList)
			{
				localPathList.add(Path.Local.fromAbsolutePath(absolutePath));
			}

			copyOrMoveLocalRemote(localPathList.toArray(new Path.Local[localPathList.size()]), dest, false);
		}
		catch (LocalPathNotRecognizedException ignored)
		{
			// Try to upload as much as you can
		}

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_UPLOAD_PICTURE, extras);
		sendLocalBroadcast(i);
	}

	private void handleAddFavorite(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		checkCondition(!(path instanceof Path.Local), "Can't add a bookmark of a local path, was: " + path);
		addFavorite(path);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_ADD_FAVORITE, extras);
		sendLocalBroadcast(i);
	}

	private void handleDeleteFavorite(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		checkCondition(!(path instanceof Path.Local), "Can't delete a bookmark of a local path, was: " + path);
		deleteFavorite(path);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_DELETE_FAVORITE, extras);
		sendLocalBroadcast(i);
	}

	private void handleEnumerateFavorites(Bundle extras)
	{
		ArrayList<Json.EnumerateBookmarks.Bookmark> favorites = enumerateFavorites();
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_ENUMERATE_FAVORITES, extras);
		i.putParcelableArrayListExtra(Contract.RESPONSE_FAVORITES, favorites);
		sendLocalBroadcast(i);
	}

	private void handleSetSort(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		int category = extras.getInt(KEY_SORT_CATEGORY, SortManager.DEFAULT_CATEGORY);
		int order = extras.getInt(KEY_SORT_ORDER, SortManager.DEFAULT_ORDER);
		SortManager.setSort(this, path, category, order);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_SET_SORT, extras);
		sendLocalBroadcast(i);
	}

	private void handleGetSort(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		Pair<Integer, Integer> p = SortManager.getSort(this, path);
		int category = p.first;
		int order = p.second;

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_SET_SORT, extras);
		i.putExtra(Contract.RESPONSE_SORT_CATEGORY, category);
		i.putExtra(Contract.RESPONSE_SORT_ORDER, order);
		sendLocalBroadcast(i);
	}

	private void handleExtractVersion(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);
		long date = extras.getLong(KEY_DATE);
		extractVersion(path, date);
		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_EXTRACT_VERSION, extras);
		sendLocalBroadcast(i);
	}

	private void handleFetchFolderContentIfExists(Bundle extras)
	{
		Path path = extras.getParcelable(KEY_PATH);

		Intent i = buildResponseIntent(Contract.ACTION_RESPONSE_FETCH_FOLDER_CONTENT_IF_EXISTS, extras);
		RemoteFolder folder = fetchRemoteFolderIfExists(path);
		ArrayList<FileInfo> content = null;
		if(folder !=null)
			content =  remoteFolderToFileInfos(path, folder);

		i.putParcelableArrayListExtra(Contract.RESPONSE_FETCH_FOLDER_IF_EXISTS, content);
		sendLocalBroadcast(i);
	}


	private void handleCopyOrMove(Bundle extras, int type)
	{
		boolean isMove = type == Contract.REQUEST_MOVE;

		long created = extras.getLong(KEY_JOB_TIMESTAMP);
		if (mLastCanceled.get() < created)
		{
			mOngoingJobTimestamp.set(created);
			Parcelable[] parcelables = extras.getParcelableArray(KEY_PATHS);
			// TODO: check this cast
			Path[] sources = Arrays.copyOf(parcelables, parcelables.length, Path[].class);
			if (sources == null || sources.length == 0)
			{
				return;
			}

			PathUtils.checkSameLocationOrThrow(sources);

			Path dest = extras.getParcelable(KEY_PATH_DEST);
			boolean toLocal = dest instanceof Path.Local;
			boolean fromLocal = sources[0] instanceof Path.Local;

			if (!fromLocal && !toLocal)
			{
				copyOrMoveRemoteRemote(sources, dest, isMove);
			}
			else if (fromLocal && toLocal)
			{
				Path.Local[] lsources = Arrays.copyOf(sources, sources.length, Path.Local[].class);
				copyOrMoveLocalLocal(lsources, (Path.Local) dest, isMove);
			}
			else if (fromLocal && !toLocal)
			{
				Path.Local[] lsources = Arrays.copyOf(sources, sources.length, Path.Local[].class);
				copyOrMoveLocalRemote(lsources, dest, isMove);
			}
			else if (!fromLocal && toLocal)
			{
				String downloadFilePath = copyOrMoveRemoteLocal(sources, (Path.Local) dest, isMove);

				EventBus bus = getDependencies().provideEventBus();
				if (sources.length == 1
						&& !(sources[0] instanceof Path.Local)
						&& bus.hasSubscriberForEvent(DownloadFileDialogFragment.BlockingDownloadRequest.class))
				{
					DownloadFileDialogFragment.BlockingDownloadRequest downloadRequest =
							new DownloadFileDialogFragment.BlockingDownloadRequest(sources[0], downloadFilePath);
					bus.post(downloadRequest);
				}
			}
		}
		Intent i = buildResponseIntent(isMove
				? Contract.ACTION_RESPONSE_MOVE
				: Contract.ACTION_RESPONSE_COPY, extras);
		sendLocalBroadcast(i);
	}

	private static Intent buildResponseIntent(String action, Bundle requestExtras)
	{
		Intent i = new Intent(action);
		i.putExtra(Contract.RESPONSE_REQUEST_EXTRAS, requestExtras);
		return i;
	}

	private ArrayList<FileInfo> browse(Path path)
	{
		PathOperation<ArrayList<FileInfo>> browseOp = new PathOperation<ArrayList<FileInfo>>()
		{
			@Override
			public ArrayList<FileInfo> execute(Local path)
			{
				return browseLocal(path);
			}

			@Override
			public ArrayList<FileInfo> execute(Remote path)
			{
				return browseRemote(path);
			}

			@Override
			public ArrayList<FileInfo> execute(TeamDrive path)
			{
				return browseRemote(path);
			}
		};
		return path.execute(browseOp);
	}

	private ArrayList<FileInfo> browseLocal(Path.Local path)
	{
		FileSystem fs = getDependencies().provideFileSystem();
		Path.Local[] paths = fs.listChildren(path);
		checkNonNullArg(paths, "The file " + path.getAbsolutePathString() + " is not browsable");
		Arrays.sort(paths, LOCAL_PATH_FOLDER_FIRST_COMPARATOR);
		return localPathsToFileInfos(fs, paths);
	}

	private ArrayList<FileInfo> browseRemote(Path path)
	{
		RemoteFolder folder = fetchRemoteFolder(path);
		return remoteFolderToFileInfos(path, folder);
	}

	private ArrayList<FileInfo> mkdir(Path path)
	{
		PathOperation<ArrayList<FileInfo>> mkdirOp = new PathOperation<ArrayList<FileInfo>>()
		{
			@Override
			public ArrayList<FileInfo> execute(Local path)
			{
				mkdirLocal(path);
				return null;
			}

			@Override
			public ArrayList<FileInfo> execute(Remote path)
			{
				mkdirRemote(path);
				return null;
			}

			@Override
			public ArrayList<FileInfo> execute(TeamDrive path)
			{
				mkdirRemote(path);
				return null;
			}
		};
		return path.execute(mkdirOp);
	}

	private void rename(Path path, final String newName)
	{
		if (path.getName().equals(newName))
		{
			return;
		}

		PathOperation<Void> renameOp = new PathOperation<Void>()
		{
			@Override
			public Void execute(TeamDrive path)
			{
				renameRemote(path, newName);
				return null;
			}

			@Override
			public Void execute(Remote path)
			{
				renameRemote(path, newName);
				return null;
			}

			@Override
			public Void execute(Local path)
			{
				renameLocal(path, newName);
				return null;
			}
		};
		path.execute(renameOp);
	}

	private void mkdirLocal(Path.Local path)
	{
		FileSystem fs = getDependencies().provideFileSystem();
		if (!fs.mkdir(path))
		{
			postErrorFeedback(path, Contract.REQUEST_MKDIR, new IOException("local mkdir failed"));
		}
	}

	private void mkdirRemote(Path path)
	{
		try
		{
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			String parentFolder = PathUtils.remoteRequestString(path.getParentPath());
			FileRequester requester = getDependencies().provideFileRequester(this);
			requester.createFolder(teamDriveId, parentFolder, path.getName(), DONT_FORCE_OVERWRITE);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Error during mkdirRemote request", e);
			postErrorFeedback(path, Contract.REQUEST_MKDIR, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void renameRemote(Path path, String newName)
	{
		try
		{
			Path dest = Path.valueOf(path.getParentPath(), newName);
			String srcTd = PathUtils.getTeamDriveIdOrNull(path);
			String destTd = PathUtils.getTeamDriveIdOrNull(dest);
			FileRequester requester = getDependencies().provideFileRequester(getApplicationContext());
			requester.moveFile(srcTd, destTd, PathUtils.remoteRequestString(path.getParentPath()),
					new String[] {path.getName()}, PathUtils.remoteRequestString(path.getParentPath()),
					newName, FORCE_OVERWRITE);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Error during renameRemote request", e);
			postErrorFeedback(path, Contract.REQUEST_RENAME, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void renameLocal(Path.Local path, String newName)
	{
		FileSystem fs = getDependencies().provideFileSystem();
		if (!fs.rename(path, newName))
		{
			postErrorFeedback(path, Contract.REQUEST_RENAME, new IOException("local rename failed"));
		}
	}

	private boolean deleteLocal(Local[] paths)
	{
		if (paths == null || paths.length == 0)
		{
			return true;
		}

		FileSystem fs = getDependencies().provideFileSystem();
		for (Path.Local p : paths)
		{
			// Stop on first error
			if (!fs.delete(p, true))
			{
				postErrorFeedback(paths, Contract.REQUEST_DELETE, new IOException("local delete failed"));
				return false;
			}
		}
		return true;
	}

	private void deleteRemote(Path[] paths)
	{
		if (paths == null || paths.length == 0)
		{
			return;
		}

		try
		{
			Path parent = paths[0].getParentPath();
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(parent);
			String parentStr = PathUtils.remoteRequestString(parent);
			String[] names = extractPathNames(paths);
			FileRequester requester = getDependencies().provideFileRequester(this);
			requester.deleteFolderOrFile(teamDriveId, parentStr, names, FORCE_OVERWRITE);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Error during delete request", e);
			postErrorFeedback(paths, Contract.REQUEST_DELETE, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void copyOrMoveLocalRemote(Path.Local[] sources, Path dest, boolean delSrc)
	{
		final List<Pair<Path, Exception>> failed = Collections
				.synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

		String teamDriveId = PathUtils.getTeamDriveIdOrNull(dest);
		for (final Path.Local src : sources)
		{
			if (isCurrentJobCanceled())
			{
				NotificationHelper nh = getDependencies().provideNotificationHelper();
				nh.cancelProgressNotification(getApplicationContext());
				return;
			}
			final CountDownLatch done = new CountDownLatch(1);
			synchronized (this)
			{
				updateProgressNotification(PROGRESS_PERCENT_START, src);
			}
			final AnythingHolder<FaultTolerantUploadManager> mgrRef = new AnythingHolder<FaultTolerantUploadManager>();
			FaultTolerantUploadManager mgr = new FaultTolerantUploadManager(getApplicationContext(),
					new NetworkUpdateCallback()
					{
						@Override
						public void publishProgressInPercent(int percent)
						{
							if (isCurrentJobCanceled())
							{
								if (mgrRef.obj != null)
								{
									mgrRef.obj.stopUpload();
								}
								NotificationHelper nh = getDependencies().provideNotificationHelper();
								nh.cancelProgressNotification(getApplicationContext());
							}
							synchronized (BrowsingService.this)
							{
								updateProgressNotification(percent, src);
							}
						}

						@Override
						public void onSuccess(String filePath)
						{
							synchronized (BrowsingService.this)
							{
								updateProgressNotification(PROGRESS_PERCENT_DONE, src);
								updateDoneNotification(src);
							}
							done.countDown();
						}

						@Override
						public void onFailure(Exception e)
						{
							failed.add(new Pair<Path, Exception>(src, e));
							NotificationHelper nh = getDependencies().provideNotificationHelper();
							nh.cancelProgressNotification(getApplicationContext());
							done.countDown();
						}
					}, new TeamplaceBackend());
			mgrRef.obj = mgr;
			Path.Local parent = (Path.Local) src.getParentPath();
			mgr.uploadFile(teamDriveId, parent.getAbsolutePathString(), src.getName(),
					PathUtils.remoteRequestString(dest), src.getName(), teamDriveId == null ? FORCE_NEW_NAME
							: FORCE_NEW_VERSION);

			try
			{
				done.await();
			}
			catch (InterruptedException e)
			{
				Thread.currentThread().interrupt();
				Log.i(TAG, "Current thread has been interrupted during an upload", e);
				return;
			}
		}

		if (failed.isEmpty())
		{
			if (delSrc)
			{
				// Does not delete check this on errors!
				deleteLocal(sources);
			}
		}
		else if (!isCurrentJobCanceled())
		{
			// don't show error message if user cancel operation
			synchronized (this)
			{
				updateErrorNotification(failed);
			}
		}
	}

	private String downloadFilePath = null;

	private String copyOrMoveRemoteLocal(final Path[] sources, final Path.Local dest, boolean delSrc)
	{
		final List<Pair<Path, Exception>> failed = Collections
				.synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

		String teamDriveId = PathUtils.getTeamDriveIdOrNull(sources[0]);
		FileSystem fs = getDependencies().provideFileSystem();

		for (final Path src : sources)
		{
			if (isCurrentJobCanceled())
			{
				NotificationHelper nh = getDependencies().provideNotificationHelper();
				nh.cancelProgressNotification(getApplicationContext());
				return null;
			}

			DownloadRequester requester = new DownloadRequester(getApplicationContext(), new SessionManager(
					getApplicationContext()).getLastSession(), new TeamplaceBackend(), new NetworkUpdateCallback()
			{

				@Override
				public void publishProgressInPercent(int percent)
				{
					synchronized (BrowsingService.this)
					{
						updateProgressNotification(percent, src);
					}
					EventBus bus = getDependencies().provideEventBus();
					if (bus.hasSubscriberForEvent(DownloadFileDialogFragment.ProgressEvent.class))
					{
						if (sources[0] instanceof Path.Remote)
						{
							bus.post(new DownloadFileDialogFragment.ProgressEvent(sources[0], percent));
						}
						else if (sources[0] instanceof Path.TeamDrive)
						{
							bus.post(new DownloadFileDialogFragment.ProgressEvent(sources[0], percent));
						}
					}
				}

				@Override
				public void onSuccess(String filePath)
				{

					downloadFilePath = filePath;
					synchronized (BrowsingService.this)
					{
						updateProgressNotification(PROGRESS_PERCENT_DONE, src);
						updateDoneNotification(src);
					}
				}

				@Override
				public void onFailure(Exception e)
				{
					failed.add(new Pair<Path, Exception>(src, e));
					NotificationHelper nh = getDependencies().provideNotificationHelper();
					nh.cancelProgressNotification(getApplicationContext());
				}
			},
					new NetworkOperationCallback()
					{
						@Override
						public boolean stopNetworkOperation()
						{
							if (isCurrentJobCanceled())
							{
								NotificationHelper nh = getDependencies().provideNotificationHelper();
								nh.cancelProgressNotification(getApplicationContext());
								return true;
							}
							return false;
						}
					});
			try
			{
				requester.download(teamDriveId, PathUtils.remoteRequestString(src.getParentPath()), src.getName(),
						dest.getAbsolutePathString(), GenericUtils.createCopyFileName(fs, src.getName(), dest), null);
			}
			catch (XCBStatusException | HttpResponseException | TeamDriveNotFoundException | ConnectFailedException
					| LogonFailedException | NotificationException | ServerLicenseException | FileWasModifiedException
					| IOException | RedirectException e1)
			{
				failed.add(new Pair<Path, Exception>(src, e1));
				NotificationHelper nh = getDependencies().provideNotificationHelper();
				nh.cancelProgressNotification(getApplicationContext());
			}
		}

		String externalStorageAppPath = GenericUtils.getExternalStorageAppPath().getAbsolutePathString();

		if (externalStorageAppPath.equals(dest.getAbsolutePathString()))
		{
			scanDownloadDir();
		}

		if (failed.isEmpty())
		{
			if (delSrc)
			{
				// Does not delete check this on errors!
				deleteRemote(sources);
			}
		}
		else
		{
			synchronized (BrowsingService.this)
			{
				updateErrorNotification(failed);
			}
		}

		return downloadFilePath;
	}

	private void copyOrMoveRemoteRemote(Path[] sources, Path dest, boolean delSrc)
	{
		synchronized (this)
		{
			updateProgressNotification(PROGRESS_PERCENT_START, sources);
		}

		FileRequester requester = getDependencies().provideFileRequester(getApplicationContext());
		String srcTeamDriveId = PathUtils.getTeamDriveIdOrNull(sources[0]);
		String destTeamDriveId = PathUtils.getTeamDriveIdOrNull(dest);
		String[] pathNames = new String[sources.length];
		for (int i = 0; i < pathNames.length; i++)
		{
			pathNames[i] = sources[i].getName();
		}

		try
		{
			int flags = destTeamDriveId == null ? FORCE_NEW_NAME : FORCE_NEW_VERSION;
			String parentFolder = PathUtils.remoteRequestString(sources[0].getParentPath());
			String moveDstFolder = PathUtils.remoteRequestString(dest);
			if (delSrc)
			{
				requester
						.moveFile(destTeamDriveId, srcTeamDriveId, parentFolder, pathNames, moveDstFolder, null, flags);
			}
			else
			{
				requester
						.copyFile(destTeamDriveId, srcTeamDriveId, parentFolder, pathNames, moveDstFolder, null, flags);
			}
			synchronized (this)
			{
				updateProgressNotification(PROGRESS_PERCENT_DONE, sources);
				updateDoneNotification(sources);
			}

		}
		catch (Exception e)
		{
			Log.e(TAG, "Exception in copyOrMoveRemoteRemote", e);
			synchronized (this)
			{
				updateProgressNotification(PROGRESS_PERCENT_DONE, sources);
				updateErrorNotification(e, sources);
			}
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void copyOrMoveLocalLocal(Path.Local[] sources, Path.Local dest, boolean isMove)
	{
		if (isMove)
		{
			moveLocalLocal(sources, dest);
		}
		else
		{
			copyLocalLocal(sources, dest);
		}
	}

	private void copyLocalLocal(Path.Local[] sources, Path.Local dest)
	{
		try
		{
			synchronized (this)
			{
				upgradeProgressNotificationStart(sources);
			}
			FileSystem fs = getDependencies().provideFileSystem();
			fs.copy(sources, dest);
			synchronized (this)
			{
				updateProgressNotificationDone(sources);
			}
		}
		catch (IOException e)
		{
			synchronized (this)
			{
				updateProgressNotificationError(sources, e);
			}
		}
	}

	private void moveLocalLocal(Path.Local[] sources, Path.Local dest)
	{
		try
		{
			synchronized (this)
			{
				upgradeProgressNotificationStart(sources);
			}
			FileSystem fs = getDependencies().provideFileSystem();
			fs.move(sources, dest);
			synchronized (this)
			{
				updateProgressNotificationDone(sources);
			}
		}
		catch (IOException e)
		{
			synchronized (this)
			{
				updateProgressNotificationError(sources, e);
			}
		}
	}

	private void export(Path path, String exportedName, String exportFormat)
	{
		try
		{
			Path dest = Path.valueOf(path.getParentPath(), path.getName() + "." + exportFormat);
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			String srcParent = PathUtils.remoteRequestString(path.getParentPath());
			String destPath = PathUtils.remoteRequestString(dest);
			ExportRequester requester = getDependencies().provideExportRequester(this);
			requester.export(teamDriveId, srcParent, path.getName(), destPath, exportFormat, FORCE_NEW_NAME);
		}
		catch (Exception e)
		{
			Log.d(TAG, "Exception in export()", e);
			int requestType = exportFormatToRequestType(exportFormat);
			postErrorFeedback(path, requestType, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void addFavorite(Path path)
	{
		try
		{
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			String pathString = PathUtils.remoteRequestString(path);
			BrowseRequester requester = getDependencies().provideBrowseRequester(this);
			requester.addBookmarkRequest(teamDriveId, pathString);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Exception during addFavorite() for path: " + path, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private void deleteFavorite(Path path)
	{
		try
		{
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			String pathString = PathUtils.remoteRequestString(path);
			BrowseRequester requester = getDependencies().provideBrowseRequester(this);
			requester.deleteBookmarkRequest(teamDriveId, pathString);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Exception during deleteFavorite() for path: " + path, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private ArrayList<Bookmark> enumerateFavorites()
	{
		try
		{
			BrowseRequester requester = getDependencies().provideBrowseRequester(this);
			// TODO: why null here?
			return requester.enumerateBookmarksRequest(null);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Exception during enumerateFavorites()", e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
			return null;
		}
	}

	private void extractVersion(Path versionPath, long versionDate)
	{
		try
		{
			String srcName = versionPath.getName();
			String destName = extractedVersionName(versionPath, versionDate);
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(versionPath);
			String parentFolder = PathUtils.remoteRequestString(versionPath.getParentPath());
			FileRequester requester = getDependencies().provideFileRequester(getApplicationContext());
			requester.copyFile(teamDriveId, teamDriveId, parentFolder,
					new String[] {srcName}, parentFolder, destName, FORCE_OVERWRITE);
		}
		catch (Exception e)
		{
			Log.e(TAG, "Exception in extractVersion()", e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}
	}

	private RemoteFolder fetchRemoteFolderIfExists(Path folderPath)
	{
		try
		{
			Log.d(TAG, "fetchRemoteFolderIfExists, start: " + folderPath);
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(folderPath);
			String remoteRequestString = PathUtils.remoteRequestString(folderPath);
			BrowseRequester requester = getDependencies().provideBrowseRequester(this);
			RemoteFolder folder = requester.doBrowseRequest(teamDriveId, remoteRequestString,
					BROWSE_REQUEST_DEPTH, false).getBrowsedFolder();
			Log.d(TAG, "fetchRemoteFolderIfExists, done: " + folderPath);
			return folder;
		}
		catch (Exception e)
		{
			Log.e(TAG, "fetchRemoteFolderIfExists, exception: " + folderPath, e);
			// dont handle exception here
			return null;
		}

	}

	// Supports both path with and without version information. VersionPath must be non-null
	// This methods localize the given date (Epoch time) with the default locale of the user so an extracted version
	// can have a name that is out of sync with the last edit date of the version. For example the user can
	// extract a version (the name is generated using that timezone) then flight to another timezone and check
	// the last edit date of the version in the original file: they will be out of sync.
	private static String extractedVersionName(Path versionPath, long versionDate)
	{
		boolean hasVersion = false;
		String newName = versionPath.getName();
		String allName = versionPath.getName();
		int index = newName.lastIndexOf(':');
		if (index != -1)
		{
			newName = newName.substring(0, index);
		}
		// Post-condition: newName does not contains version information
		int versionNumber = 0;
		index = allName.lastIndexOf('_');
		if (index != -1)
		{
			String versionNr = allName.substring(index + 1);
			versionNumber = Integer.valueOf(versionNr);
			hasVersion = true;
		}

		String ext = "";
		index = newName.lastIndexOf('.');
		if (index != -1)
		{
			ext = newName.substring(index);
			newName = newName.substring(0, index);
		}
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd_hh-mm", Locale.getDefault());
		if (hasVersion)
			newName += "_" + dt.format(versionDate) + "_v" + versionNumber + ext;
		else
			newName += "_" + dt.format(versionDate) + ext;
		return newName;
	}

	private void scanDownloadDir()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				File basePath = GenericUtils.getExternalStorageAppDir();
				File[] folderContent = basePath.listFiles(new FileFilter()
				{
					@Override
					public boolean accept(File pathname)
					{
						return pathname.isFile();
					}
				});
				String[] filePaths = new String[folderContent.length];
				for (int i = 0; i < folderContent.length; i++)
				{
					filePaths[i] = folderContent[i].getAbsolutePath();
				}
				MediaScannerConnection.scanFile(getApplicationContext(), filePaths, null, null);
			}
		}).start();
	}

	private void sendLocalBroadcast(Intent i)
	{
		IntentBus bus = getDependencies().provideIntentBus(this);
		bus.sendBroadcast(i);
	}

	private void updateProgressNotification(int progressPercent, Path... sources)
	{

		if (updateFileJobInfoArray(mOngoingJobTimestamp.get(), progressPercent, sources))
		{
			NotificationHelper nh = getDependencies().provideNotificationHelper();
			nh.updateProgressNotification(getApplicationContext(),
					Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class));
		}
	}

	private void updateDoneNotification(Path... sources)
	{
		long ongoingJobTimestamp = mOngoingJobTimestamp.get();
		updateFileJobInfoArray(ongoingJobTimestamp, PROGRESS_PERCENT_DONE, sources);
		NotificationHelper nh = getDependencies().provideNotificationHelper();
		nh.updateDoneNotification(getApplicationContext(),
				Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class));
		deleteJobIfFinished(ongoingJobTimestamp);
	}

	private void updateErrorNotification(List<Pair<Path, Exception>> failed)
	{
		updateFileJobInfoArray(mOngoingJobTimestamp.get(), failed);
		NotificationHelper nh = getDependencies().provideNotificationHelper();
		nh.updateErrorNotification(getApplicationContext(),
				Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class));
		deleteJobIfFinished(mOngoingJobTimestamp.get());
	}

	private void updateErrorNotification(Exception exception, Path... sources)
	{
		updateFileJobInfoArray(mOngoingJobTimestamp.get(), exception, sources);
		NotificationHelper nh = getDependencies().provideNotificationHelper();
		nh.updateErrorNotification(getApplicationContext(),
				Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class));
		deleteJobIfFinished(mOngoingJobTimestamp.get());
	}

	private boolean updateFileJobInfoArray(long ongoingJobId, int progressPercent, Path... sources)
	{
		FileJobInfo current = mJobQueue.get(ongoingJobId);
		if (current != null)
		{
			current.updatePathStateInfo(sources,
					progressPercent < PROGRESS_PERCENT_DONE ? FileJobInfo.STATE_ACTIVE : FileJobInfo.STATE_FINISHED,
					progressPercent);
			return true;
		}
		return false;
	}

	private void updateFileJobInfoArray(long onoingJobId, List<Pair<Path, Exception>> failed)
	{
		FileJobInfo current = mJobQueue.get(mOngoingJobTimestamp.get());
		if (current != null)
		{
			for (Pair<Path, Exception> pathPair : failed)
			{
				current.updatePathStateInfo(new Path[] {pathPair.first}, FileJobInfo.STATE_ERROR,
						PROGRESS_PERCENT_DONE, pathPair.second);
			}
		}
	}

	private void updateFileJobInfoArray(long onoingJobId, Exception exception, Path... sources)
	{
		FileJobInfo current = mJobQueue.get(mOngoingJobTimestamp.get());
		if (current != null)
		{
			current.updatePathStateInfo(sources, FileJobInfo.STATE_ERROR, PROGRESS_PERCENT_DONE, exception);
		}
	}

	private void updateProgressNotificationError(Path.Local[] sources, IOException e)
	{
		updateProgressNotification(PROGRESS_PERCENT_DONE, sources);
		updateErrorNotification(e, sources);
	}

	private void updateProgressNotificationDone(Path.Local[] sources)
	{
		updateProgressNotification(PROGRESS_PERCENT_DONE, sources);
		updateDoneNotification(sources);
	}

	private void upgradeProgressNotificationStart(Path.Local[] sources)
	{
		updateProgressNotification(PROGRESS_PERCENT_START, sources);
	}

	private void deleteJobIfFinished(long ongoingJobId)
	{
		boolean allFinished = true;
		for (FileJobInfo.PathStateInfo pathInfo : mJobQueue.get(ongoingJobId).getSourceInfos())
		{
			if (pathInfo.getProgressPercent() < PROGRESS_PERCENT_DONE)
			{
				allFinished = false;
				break;
			}
		}
		if (allFinished)
		{
			mJobQueue.remove(ongoingJobId);
		}
	}

	private String[] extractPathNames(Path[] paths)
	{
		int len = paths.length;
		String[] names = new String[len];
		for (int i = 0; i < len; i++)
		{
			names[i] = paths[i].getName();
		}
		return names;
	}

	private RemoteFolder fetchRemoteFolder(Path folderPath)
	{
		try
		{
			Log.d(TAG, "fetchRemoteFolder, start: " + folderPath);
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(folderPath);
			String remoteRequestString = PathUtils.remoteRequestString(folderPath);
			BrowseRequester requester = getDependencies().provideBrowseRequester(this);
			RemoteFolder folder = requester.doBrowseRequest(teamDriveId, remoteRequestString,
					BROWSE_REQUEST_DEPTH, false).getBrowsedFolder();
			Log.d(TAG, "fetchRemoteFolder, done: " + folderPath);
			return folder;
		}
		catch (Exception e)
		{
			Log.e(TAG, "fetchRemoteFolder, exception: " + folderPath, e);
			postErrorFeedback(folderPath, Contract.REQUEST_BROWSE, e);
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
			return null;
		}
	}

	private ArrayList<FileInfo> remoteFolderToFileInfos(Path parent, RemoteFolder folder)
	{
		if (folder == null)
		{
			return new ArrayList<FileInfo>();
		}

		RemoteFolder[] folders = folder.getFolders();
		RemoteArchive[] archives = folder.getArchives();
		RemoteFile[] files = folder.getFiles();
		int lenFolders = folders == null ? 0 : folders.length;
		int lenArchives = archives == null ? 0 : archives.length;
		int lenFiles = files == null ? 0 : files.length;
		ArrayList<FileInfo> fileInfos = new ArrayList<FileInfo>();

		for (int i = 0; i < lenFolders; i++)
		{
			RemoteFolder child = folders[i];
			fileInfos.add(new FileInfo(Path.valueOf(parent, child.getName()), child.getDate() * 1000L, child.getSize(),
					child.isFavorite(), FileInfo.CTYPE_FOLDER));
		}

		for (int i = 0; i < lenArchives; i++)
		{
			RemoteArchive child = archives[i];
			fileInfos.add(new FileInfo(Path.valueOf(parent, child.getName()), child.getDate() * 1000L, child.getSize(),
					child.isFavorite(), FileInfo.CTYPE_ARCHIVE));
		}

		for (int i = 0; i < lenFiles; i++)
		{
			RemoteFile child = files[i];
			String name = child.getName();
			fileInfos.add(new FileInfo(Path.valueOf(parent, name), child.getDate() * 1000L, child.getSize(), child
					.isFavorite()));
		}
		return fileInfos;
	}

	private ArrayList<FileInfo> localPathsToFileInfos(FileSystem fs, Path.Local[] paths)
	{
		int len = paths.length;
		ArrayList<FileInfo> fileInfos = new ArrayList<FileInfo>();
		for (int i = 0; i < len; i++)
		{
			Path.Local p = paths[i];

			int ctype = FileInfo.CTYPE_FOLDER;
			long length = DEFAULT_LOCAL_FOLDER_SIZE;
			if (!fs.isDirectory(p))
			{
				ctype = FileInfo.CTYPE_OTHER;
			}

			length = fs.length(p);
			fileInfos.add(new FileInfo(p, fs.lastModified(p), length, false, ctype));
		}
		return fileInfos;
	}

	private int exportFormatToRequestType(String exportFormat)
	{
		switch (exportFormat)
		{
			case ExportRequester.FORMAT_PDF:
				return Contract.REQUEST_EXPORT_TO_PDF;
			case ExportRequester.FORMAT_ZIP:
				return Contract.REQUEST_EXPORT_TO_ZIP;
			default:
				return 0; // TODO: @jobol why zero?
		}
	}

	private void postErrorFeedback(Path path, int requestType, Exception e)
	{
		postErrorFeedback(new Path[] {path}, requestType, e);
	}

	private void postErrorFeedback(Path[] paths, int requestType, Exception e)
	{
		TimeSource timeSource = getDependencies().provideTimeSource();
		EventBus bus = getDependencies().provideEventBus();

		PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
				paths, FileJobInfo.STATE_ERROR, PROGRESS_PERCENT_DONE, e);

		long created = timeSource.currentTimeMillis();
		FileJobInfo jobInfo = new FileJobInfo(created, requestType, pathStateInfo, null);
		bus.post(new FileJobInfo[] {jobInfo});
	}

	private synchronized boolean isCurrentJobCanceled()
	{
		return mLastCanceled.get() >= mOngoingJobTimestamp.get();
	}

	private static class DependenciesImpl implements Dependencies
	{
		private final TimeSource mTimeSource = new SystemTimeSource();
		private final EventBus mEventBus = EventBus.getDefault();
		private final NotificationHelper mNotificationHelper = NotificationHelper.getInstance();
		private final FileSystem mFileSystem = new FileSystemImpl();
		private IntentBus mIntentBus;

		@Override
		public TimeSource provideTimeSource()
		{
			return mTimeSource;
		}

		@Override
		public EventBus provideEventBus()
		{
			return mEventBus;
		}

		@Override
		public IntentBus provideIntentBus(Context context)
		{
			if (mIntentBus == null)
			{
				mIntentBus = new IntentBusImpl(context);
			}

			return mIntentBus;
		}

		@Override
		public NotificationHelper provideNotificationHelper()
		{
			return mNotificationHelper;
		}

		@Override
		public FileSystem provideFileSystem()
		{
			return mFileSystem;
		}

		@Override
		public BrowseRequester provideBrowseRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new BrowseRequester(context, session, new TeamplaceBackend());
		}

		@Override
		public FileRequester provideFileRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new FileRequester(context, session, new TeamplaceBackend());
		}

		@Override
		public SmartFilingRequester provideSmartFilingRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new SmartFilingRequester(context, session, new TeamplaceBackend());
		}

		@Override
		public ExportRequester provideExportRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new ExportRequester(context, session, new TeamplaceBackend());
		}
	}

	private static class AnythingHolder<T>
	{
		private T obj;
	}
}
