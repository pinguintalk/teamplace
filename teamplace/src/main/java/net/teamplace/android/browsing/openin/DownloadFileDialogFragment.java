package net.teamplace.android.browsing.openin;

import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import net.teamplace.android.browsing.path.LocalPathNotRecognizedException;
import net.teamplace.android.browsing.path.Path;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cortado.android.R;

import de.greenrobot.event.EventBus;

public class DownloadFileDialogFragment extends DialogFragment
{
	public static final String TAG = tag(DownloadFileDialogFragment.class);

	public static final int STRATEGY_DATA_URI = 0;
	public static final int STRATEGY_STREAM_EXTRA = 1;

	// private static final String EXTRA_COMPONENT_NAME = "extraComponentName";
	private static final String EXTRA_INTENT = "intent";
	private static final String EXTRA_PROGRESS = "progress";
	private static final String EXTRA_CONTENT_STRATEGY = "contentStrategy";

	// private ComponentName componentName;
	private Intent intent;
	private FileInfo remoteFileInfo;
	private int contentStrategy;

	private ProgressBar progress;
	private TextView progressText;

	public static DownloadFileDialogFragment newInstance(FileInfo fileInfo, Intent intent, int contentStrategy)
	{
		Bundle arguments = new Bundle();
		// if (resolveInfo != null)
		// {
		// ActivityInfo activityInfo = resolveInfo.activityInfo;
		//
		// arguments.putParcelable(EXTRA_COMPONENT_NAME, new ComponentName(activityInfo.applicationInfo.packageName,
		// activityInfo.name));
		// }
		arguments.putParcelable(EXTRA_INTENT, intent);
		arguments.putParcelable(ResolvingActivityDialog.KEY_FILE_INFO, fileInfo);
		arguments.putInt(EXTRA_CONTENT_STRATEGY, contentStrategy);

		DownloadFileDialogFragment fragment = new DownloadFileDialogFragment();
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Activity context = getActivity();
		EventBus.getDefault().register(this);

		Bundle arguments = getArguments();
		// componentName = arguments.getParcelable(EXTRA_COMPONENT_NAME);
		intent = arguments.getParcelable(EXTRA_INTENT);
		remoteFileInfo = arguments.getParcelable(ResolvingActivityDialog.KEY_FILE_INFO);
		contentStrategy = arguments.getInt(EXTRA_CONTENT_STRATEGY);

		startDownload();

		LayoutInflater inflater = context.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.openin_blockingdownload_dialog, null);

		final StyleableAlertDialog dialog = new StyleableAlertDialog(context);
		dialog.setTitle(R.string.openin_download);
		dialog.setView(dialogView);

		TextView file = (TextView) dialogView.findViewById(R.id.brw_tv_open_in_file_name);
		file.setText(remoteFileInfo.getName());

		progressText = (TextView) dialogView.findViewById(R.id.brw_tv_open_in_progress_text);
		progress = (ProgressBar) dialogView.findViewById(R.id.brw_pb_download_open_in);

		if (savedInstanceState != null)
		{
			progress.setProgress(savedInstanceState.getInt(EXTRA_PROGRESS, 0));
		}

		return dialog;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putInt(EXTRA_PROGRESS, progress.getProgress());
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void show(FragmentManager manager, String tag)
	{
		if (manager.findFragmentByTag(tag) == null)
		{
			super.show(manager, tag);
		}
	}

	public void onEventMainThread(BlockingDownloadRequest response)
	{
		if (response.getRemoteFilePath().equals(remoteFileInfo.getPath()))
		{
			getActivity().startActivity(
					ContentHelper.setFilePathContent(intent, response.getDownloadFilePath(), contentStrategy));
			dismiss();
		}
	}

	public void onEventMainThread(ProgressEvent event)
	{
		if (event.getRemoteFilePath().equals(remoteFileInfo.getPath()))
		{
			int percent = event.getPercent();

			progress.setProgress(percent);
			progressText.setText(Integer.toString(percent) + "%");
		}
	}

	private void startDownload()
	{
		try
		{
			Path[] source = new Path[] {remoteFileInfo.getPath()};
			Path target = Path.Local.fromAbsolutePath(Environment.getExternalStoragePublicDirectory(
					Environment.DIRECTORY_DOWNLOADS).getAbsolutePath());

			Context context = getActivity();
			Intent i = Contract.buildCopyRequestIntent(context, source, target);
			context.startService(i);
		}
		catch (LocalPathNotRecognizedException e)
		{
			e.printStackTrace();
		}
	}

	public static class BlockingDownloadRequest
	{
		private final Path remoteFilePath;
		private final String downloadFilePath;

		public BlockingDownloadRequest(Path remoteFilePath, String downloadFilePath)
		{
			this.remoteFilePath = remoteFilePath;
			this.downloadFilePath = downloadFilePath;
		}

		public Path getRemoteFilePath()
		{
			return remoteFilePath;
		}

		public String getDownloadFilePath()
		{
			return downloadFilePath;
		}
	}

	public static class ProgressEvent
	{
		private final Path remoteFilePath;
		private final int percent;

		public ProgressEvent(Path remoteFilePath, int percent)
		{
			this.remoteFilePath = remoteFilePath;
			this.percent = percent;
		}

		public int getPercent()
		{
			return percent;
		}

		public Path getRemoteFilePath()
		{
			return remoteFilePath;
		}
	}
}