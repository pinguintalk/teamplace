package net.teamplace.android.browsing;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.bookmark.Json.EnumerateBookmarks.Bookmark;
import net.teamplace.android.http.file.RemoteFolder;
import android.os.Parcel;
import android.os.Parcelable;

import com.cortado.android.R;

/**
 * Contains information about a file
 * 
 * @author Gil Vegliach
 */
public class FileInfo implements Parcelable
{
	@SuppressWarnings("unused")
	private static final String TAG = tag(FileInfo.class);

	// Constructor types. Correspond to type attribute of a folder in a browse-request
	public static final int CTYPE_FOLDER = RemoteFolder.TYPE_STANDARD;
	public static final int CTYPE_DRIVE = RemoteFolder.TYPE_SHARE_POINT;
	public static final int CTYPE_ARCHIVE = RemoteFolder.TYPE_ZIP;
	public static final int CTYPE_OTHER = -1;

	public static final int NO_SIZE = -1;

	// File types. Do not confuse with CTYPEs
	public static final int UNKNOWN = -1;
	public static final int FOLDER = 10;
	public static final int DRIVE = 11;
	public static final int IMAGE = 20;
	public static final int TEXT = 21;
	public static final int PDF = 22;
	public static final int WORD = 23;
	public static final int SPREADSHEET = 24;
	public static final int PRESENTATION = 25;
	public static final int ARCHIVE = 26;
	public static final int WEB = 27;
	public static final int AUDIO = 28;
	public static final int VIDEO = 29;
	public static final int MAIL = 30;
	public static final int DATABASE = 31;
	public static final int TPM = 32;
	public static final int TPF = 33;
	public static final int FONT = 34;

	// For the mappings, refer to:
	// https://confluence.thinprint.de/display/TEAMPLACE/Supported+file+types
	private static final Map<String, Integer> EXT_TYPE_MAP;
	private static final Map<Integer, Integer> TYPE_RES_MAP;

	private static final String EMPTY_TAG = "folder";
	private static final String UNKNOWN_TAG = "unknown";

	private final Path mPath;
	private final long mLastModifiedDate;
	private final long mSize;
	private boolean mIsFavorite;
	private final int mType;

	/**
	 * Constructs an object representing a file. Equivalent to FileInfo(path, lastModifiedDate, size, isFavorite,
	 * CTYPE_OTHER)
	 **/
	public FileInfo(Path path, long lastModifiedDate, long size, boolean isFavorite)
	{
		this(path, lastModifiedDate, size, isFavorite, CTYPE_OTHER);
	}

	/** Construct an object representing a file or a folder. The parameter ctype must be one of the CTYPE_* constants */
	public FileInfo(Path path, long lastModifiedDate, long size, boolean isFavorite, int ctype)
	{
		mPath = path;
		mLastModifiedDate = lastModifiedDate;
		mSize = size;
		mIsFavorite = isFavorite;
		mType = getTypeFromCtypeAndName(ctype, path.getName());
	}

	/** Constructs an object representing a favorite file of folder. */
	public FileInfo(Bookmark bookmark)
	{
		String teamdriveId = bookmark.getTeam();
		String pathname = bookmark.getName();
		String location = PathUtils.pathLocationFromTeamDriveId(teamdriveId);
		mPath = PathUtils.buildPathfromServerResponseString(location, pathname);
		mLastModifiedDate = bookmark.getDate();
		mSize = bookmark.getSize();
		mIsFavorite = true;
		Integer type = bookmark.getType();
		mType = getTypeFromCtypeAndName(type == null ? FileInfo.CTYPE_OTHER : type, mPath.getName());
	}

	public String getName()
	{
		return mPath.getName();
	}

	public Path getPath()
	{
		return mPath;
	}

	public long getLastModifiedDate()
	{
		return mLastModifiedDate;
	}

	public long getSize()
	{
		return mSize;
	}

	public int getType()
	{
		return mType;
	}

	public boolean isFolder()
	{
		return mType == FOLDER;
	}

	public boolean isBrowsable()
	{
		return mType == FOLDER || mType == ARCHIVE;
	}

	public boolean isFavorite()
	{
		return mIsFavorite;
	}

	public void setFavorite(boolean isFavorite)
	{
		mIsFavorite = isFavorite;
	}

	public int getIconResId()
	{
		return getIconResIdFromType(mType);
	}

	/**
	 * Retrieves the correct icon to use given a file name (not path) and its ctype. This method uses the same logic as
	 * {@link FileInfo#getIconResId()} but avoids creating a FileInfo object.
	 */
	public static int getIconResIdFromCtypeAndName(int ctype, String name)
	{
		int type = getTypeFromCtypeAndName(ctype, name);
		return getIconResIdFromType(type);
	}

	/** Tests for equality of FileInfo objects. The result of {@link #isFavorite()} flag is taken into account */
	@Override
	public boolean equals(Object o)
	{
		if (o == null)
		{
			return false;
		}

		if (o == this)
		{
			return true;
		}

		if (!(o instanceof FileInfo))
		{
			return false;
		}

		FileInfo info = (FileInfo) o;
		return info.mPath.equals(mPath) &&
				info.mLastModifiedDate == mLastModifiedDate &&
				info.mSize == mSize &&
				info.mIsFavorite == mIsFavorite &&
				info.mType == mType;
	}

	@Override
	public int hashCode()
	{
		int isFavorite = mIsFavorite ? 1 : 0;
		return mPath.hashCode() ^ ((int) mLastModifiedDate) ^ ((int) mSize) ^ isFavorite ^ mType;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("FileInfo{")
				.append("path=" + mPath)
				.append(", lastModifiedDate=" + mLastModifiedDate)
				.append(", size=" + mSize)
				.append(", isFavorite=" + mIsFavorite)
				.append(", type=" + mType)
				.append("}");
		return sb.toString();
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	public static int getTypeFromExtension(String ext)
	{
		Integer type = EXT_TYPE_MAP.get(ext.toLowerCase(Locale.US));
		if (type == null)
		{
			return UNKNOWN;
		}

		return type;
	}

	private static String getExtensionFromName(String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name must be non-null");
		}

		int index = name.lastIndexOf(".");

		if (index == -1)
		{
			return EMPTY_TAG;
		}

		if (index < name.length())
		{
			return name.substring(index + 1).toLowerCase(Locale.US);
		}

		return UNKNOWN_TAG;
	}

	/** Merges ctype and type obtained from the extension */
	private static int getTypeFromCtypeAndName(int ctype, String name)
	{
		switch (ctype)
		{
			case CTYPE_FOLDER:
				return FOLDER;
			case CTYPE_DRIVE:
				return DRIVE;
			case CTYPE_ARCHIVE:
				return ARCHIVE;
			case CTYPE_OTHER:
				return getTypeFromExtension(getExtensionFromName(name));
			default:
				throw new IllegalArgumentException("ctype must be one of the CTYPE_* constants");
		}
	}

	private static int getIconResIdFromType(int type)
	{
		Integer resId = TYPE_RES_MAP.get(type);
		if (resId == null)
		{
			return R.drawable.brw_type_unknown;
		}

		return resId;
	}

	@Override
	public void writeToParcel(Parcel out, int flags)
	{
		out.writeParcelable(mPath, flags);
		out.writeLong(mLastModifiedDate);
		out.writeLong(mSize);
		out.writeInt(mIsFavorite ? 1 : 0);
		out.writeInt(mType);
	}

	public static final Parcelable.Creator<FileInfo> CREATOR = new Parcelable.Creator<FileInfo>()
	{
		@Override
		public FileInfo createFromParcel(Parcel in)
		{
			return new FileInfo(in);
		}

		@Override
		public FileInfo[] newArray(int size)
		{
			return new FileInfo[size];
		}
	};

	private FileInfo(Parcel in)
	{
		mPath = in.readParcelable(Path.class.getClassLoader());
		mLastModifiedDate = in.readLong();
		mSize = in.readLong();
		mIsFavorite = (in.readInt() == 1);
		mType = in.readInt();
	}

	static
	{
		HashMap<String, Integer> extensionType = new HashMap<>();
		extensionType.put("bmp", IMAGE);
		extensionType.put("gif", IMAGE);
		extensionType.put("jpeg", IMAGE);
		extensionType.put("jpg", IMAGE);
		extensionType.put("jpe", IMAGE);
		extensionType.put("jiff", IMAGE);
		extensionType.put("png", IMAGE);
		extensionType.put("tiff", IMAGE);
		extensionType.put("tif", IMAGE);
		extensionType.put("ico", IMAGE);

		extensionType.put("log", TEXT);
		extensionType.put("txt", TEXT);
		extensionType.put("wtx", TEXT);
		extensionType.put("xml", TEXT);

		extensionType.put("pdf", PDF);

		extensionType.put("doc", WORD);
		extensionType.put("docx", WORD);
		extensionType.put("docm", WORD);
		extensionType.put("dot", WORD);
		extensionType.put("dotx", WORD);
		extensionType.put("dotm", WORD);
		extensionType.put("rtf", WORD);
		extensionType.put("odt", WORD);

		extensionType.put("csv", SPREADSHEET);
		extensionType.put("xls", SPREADSHEET);
		extensionType.put("xlsx", SPREADSHEET);
		extensionType.put("xlsb", SPREADSHEET);
		extensionType.put("xlsm", SPREADSHEET);
		extensionType.put("xlt", SPREADSHEET);
		extensionType.put("xltm", SPREADSHEET);
		extensionType.put("xlt", SPREADSHEET);
		extensionType.put("xltx", SPREADSHEET);
		extensionType.put("xlw", SPREADSHEET);
		extensionType.put("ods", SPREADSHEET);

		extensionType.put("ppt", PRESENTATION);
		extensionType.put("pptx", PRESENTATION);
		extensionType.put("pptm", PRESENTATION);
		extensionType.put("pps", PRESENTATION);
		extensionType.put("ppsx", PRESENTATION);
		extensionType.put("ppsm", PRESENTATION);
		extensionType.put("pot", PRESENTATION);
		extensionType.put("potm", PRESENTATION);
		extensionType.put("ppam", PRESENTATION);
		extensionType.put("ppa", PRESENTATION);
		extensionType.put("odp", PRESENTATION);

		extensionType.put("zip", ARCHIVE);

		extensionType.put("mht", WEB);
		extensionType.put("mhtml", WEB);
		extensionType.put("htm", WEB);
		extensionType.put("html", WEB);
		extensionType.put("php", WEB);
		extensionType.put("xhtml", WEB);

		extensionType.put("mp3", AUDIO);
		extensionType.put("mpu", AUDIO);
		extensionType.put("m4a", AUDIO);

		extensionType.put("mp4", VIDEO);
		extensionType.put("avi", VIDEO);
		extensionType.put("mpeg", VIDEO);
		extensionType.put("wmv", VIDEO);
		extensionType.put("mov", VIDEO);

		extensionType.put("eml", MAIL);
		extensionType.put("msg", MAIL);

		extensionType.put("mdb", DATABASE);
		extensionType.put("accdb", DATABASE);
		extensionType.put("odb", DATABASE);

		extensionType.put("tpm", TPM);

		extensionType.put("tpf", TPF);

		extensionType.put("ttf", FONT);
		EXT_TYPE_MAP = Collections.unmodifiableMap(extensionType);

		HashMap<Integer, Integer> typeRes = new HashMap<>();
		typeRes.put(UNKNOWN, R.drawable.brw_type_unknown);
		typeRes.put(FOLDER, R.drawable.brw_type_folder);
		typeRes.put(DRIVE, R.drawable.brw_type_drive);
		typeRes.put(IMAGE, R.drawable.brw_type_image);
		typeRes.put(TEXT, R.drawable.brw_type_text);
		typeRes.put(PDF, R.drawable.brw_type_pdf);
		typeRes.put(WORD, R.drawable.brw_type_word);
		typeRes.put(SPREADSHEET, R.drawable.brw_type_spreadsheet);
		typeRes.put(PRESENTATION, R.drawable.brw_type_presentation);
		typeRes.put(ARCHIVE, R.drawable.brw_type_archive);
		typeRes.put(WEB, R.drawable.brw_type_web);
		typeRes.put(AUDIO, R.drawable.brw_type_audio);
		typeRes.put(VIDEO, R.drawable.brw_type_video);
		typeRes.put(MAIL, R.drawable.brw_type_mail);
		// DATABASE missing
		typeRes.put(TPM, R.drawable.brw_type_tpm);
		// TPF missing
		typeRes.put(FONT, R.drawable.brw_type_font);

		TYPE_RES_MAP = Collections.unmodifiableMap(typeRes);
	}
}
