package net.teamplace.android.browsing.openin;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.StyleableAlertDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.cortado.android.R;

public class ResolvingActivityDialog extends DialogFragment implements OnItemClickListener
{
	public static final String TAG = tag(ResolvingActivityDialog.class);

	public static final String KEY_FILE_INFO = "extraFileInfo";
	public static final String KEY_RESOLVER_TYPE = "keyResolverType";

	private FileInfo fileInfo;
	private ResolvingActivityAdapter adapter;
	private ActivityResolver activityResolver;

	public static ResolvingActivityDialog newInstance(FileInfo file, int activityResolverType)
	{
		Bundle arguments = new Bundle();
		arguments.putParcelable(KEY_FILE_INFO, file);
		arguments.putInt(KEY_RESOLVER_TYPE, activityResolverType);
		ResolvingActivityDialog fragment = new ResolvingActivityDialog();
		fragment.setArguments(arguments);
		return fragment;
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Activity context = getActivity();
		final StyleableAlertDialog dialog = new StyleableAlertDialog(context);

		fileInfo = getArguments().getParcelable(KEY_FILE_INFO);

		dialog.setTitle(getString(R.string.brw_open_in));

		LayoutInflater inflater = context.getLayoutInflater();
		ListView listView = (ListView) inflater.inflate(R.layout.brw_resolving_activity_list, null);

		activityResolver = ActivityResolver.get(getArguments().getInt(KEY_RESOLVER_TYPE));
		List<ResolveInfoForIntent> list = activityResolver.resolveActivities(getActivity(), fileInfo);

		if (list.size() == 0)
		{
			dismiss();
		}

		adapter = new ResolvingActivityAdapter(getActivity(), 0, list);

		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);

		dialog.setView(listView);
		return dialog;
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		ResolveInfoForIntent resolveInfoForAction = adapter.getItem(position);
		activityResolver.onResolveInfoSelected(getActivity(), resolveInfoForAction, fileInfo);
		dismiss();
	}

	@Override
	public void show(FragmentManager manager, String tag)
	{
		if (manager.findFragmentByTag(tag) == null)
		{
			super.show(manager, tag);
		}
	}
}
