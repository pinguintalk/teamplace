package net.teamplace.android.browsing.path;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;


public class FileSystemImpl implements FileSystem
{
	private static final String TAG = tag(FileSystemImpl.class);

	@Override
	public Path.Local[] listChildren(Path.Local path)
	{
		checkNonNullArg(path);
		return path.listPaths();
	}

	@Override
	public boolean mkdir(Path.Local path)
	{
		checkNonNullArg(path);
		return path.mkdir();
	}

	@Override
	public boolean mkdirs(Path.Local path)
	{
		checkNonNullArg(path);
		return path.mkdirs();
	}

	@Override
	public boolean delete(Path.Local path, boolean force)
	{
		checkNonNullArg(path);
		return path.delete(force);
	}

	@Override
	public boolean rename(Path.Local path, String newName)
	{
		checkNonNullArgs(path, newName);
		return path.rename(newName);
	}

	@Override
	public boolean isDirectory(Path.Local path)
	{
		checkNonNullArg(path);
		return path.isDirectory();
	}

	@Override
	public long lastModified(Path.Local path)
	{
		checkNonNullArg(path);
		return path.lastModified();
	}

	@Override
	public long length(Path.Local path)
	{
		checkNonNullArg(path);
		return path.length();
	}

	// TODO: Does this correctly handle nested directories? Probably not...
	@Override
	public void copy(Local[] sources, Local dest) throws IOException
	{
		checkNonNullDeep(sources, dest);
		File destFolder = new File(dest.getAbsolutePathString());
		for (Path.Local sourcePath : sources)
		{
			String destName = sourcePath.getName();
			Path destPath = Path.valueOf(dest, destName);
			if (sourcePath.equals(destPath))
			{
				destName = GenericUtils.createCopyFileName(this, sourcePath.getName(), dest);
			}
			File in = new File(sourcePath.getAbsolutePathString());
			File out = new File(destFolder, destName);
			FileChannel csrc = null;
			FileChannel cdest = null;
			try
			{
				csrc = new FileInputStream(in).getChannel();
				cdest = new FileOutputStream(out).getChannel();
				// TODO: what if this does not transfer the whole file? This method looks broken...
				cdest.transferFrom(csrc, 0L, csrc.size());
			}
			catch (IOException e)
			{
				Log.e(TAG, "Error copying the files, src: " + in + ", dest: " + destFolder, e);
				throw e; // Stop if something goes wrong
			}
			finally
			{
				GenericUtils.closeQuietly(TAG, csrc, cdest);
			}
		}
	}

	@Override
	public void move(Local[] sources, Local dest) throws IOException
	{
		checkNonNullDeep(sources, dest);
		copy(sources, dest);
		for (Path.Local source : sources)
		{
			delete(source, true);
		}
	}

	private static void checkNonNullDeep(Local[] sources, Local dest)
	{
		checkNonNullArgs(sources, dest);
		for (Path.Local sourcePath : sources)
		{
			checkNonNullArg(sourcePath);
		}
	}
}
