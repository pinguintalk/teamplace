package net.teamplace.android.browsing.filtering;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;

import java.util.List;
import java.util.Locale;

import android.widget.Adapter;

/**
 * Helps implement filtering in a generic Adapter. This class creates views of the dataset as follow: first an item in
 * the dataset is converted to a String by {@link ItemConverter}; then, if this string contains the filter string
 * (case-insensitive), the corresponding item is added to the view, otherwise it is excluded. The view is accessible
 * through the methods {@link #getItem(int)} and {@link #getCount()}.
 * <p>
 * To implement filtering in an adapter do the following:
 * <ol>
 * <li>Pass to the constructor an implementation of {@link ItemConverter} that converts specific items to strings.</li>
 * <li>Implement {@link Adapter#getCount()} and {@link Adapter#getItem(int)} delegating to {@link #getItem(int)} and
 * {@link #getCount()}.</li>
 * <li>Call {@link #setFilter(String)} and {@link #disableFilter()} as appropriate; follow those calls with {@link
 * Adapter.notifyDataSetChanged()}.</li>
 * <li><em>Optional:</em> use {@link #checkFilterNullOrThrow()} to throw a RuntimeException if filtering is enabled.</li>
 * </p>
 * 
 * @author Gil Vegliach
 */
public class FilterController<T>
{
	final private List<T> mDataset;
	final private ItemConverter<T> mConverter;
	private String mFilter;

	public FilterController(List<T> dataset, ItemConverter<T> converter)
	{
		checkNonNullArg(dataset);
		checkNonNullArg(converter);

		mDataset = dataset;
		mConverter = converter;
	}

	/** True iff filtering is enabled. */
	public boolean isFilterEnabled()
	{
		return mFilter != null;
	}

	/**
	 * Sets the filter string. Passing null or an empty String is equivalent to disabling filtering.
	 */
	public void setFilter(String filter)
	{
		mFilter = (!"".equals(filter)) ? filter : null;
	}

	/** Equivalent to {@link #setFilter(String) setFilter(null)} */
	public void disableFilter()
	{
		setFilter(null);
	}

	public int getCount()
	{
		int count = mDataset.size();
		if (mFilter == null)
			return count;

		int newCount = 0;
		for (int i = 0; i < count; i++)
		{
			T item = mDataset.get(i);
			String filterableString = mConverter.getFilterableString(item, i);
			if (containsCaseInsensitive(filterableString, mFilter))
				newCount++;
		}
		return newCount;
	}

	public Object getItem(int position)
	{
		if (mFilter == null)
			return mDataset.get(position);

		int count = mDataset.size();
		for (int i = 0; i < count; i++)
		{
			T item = mDataset.get(i);
			String filterableString = mConverter.getFilterableString(item, i);
			if (!containsCaseInsensitive(filterableString, mFilter))
				continue;

			if (position-- == 0)
				return item;
		}
		throw new IllegalStateException("Should not be reachable");
	}

	public void checkFilterDisabledOrThrow()
	{
		if (isFilterEnabled())
			throw new IllegalStateException("Selecting items is not supported while filtering");
	}

	/** Case insensitive version of String.contains() */
	private static boolean containsCaseInsensitive(String s, String prefix)
	{
		return s.toLowerCase(Locale.US).contains(prefix.toLowerCase(Locale.US));
	}

	public interface ItemConverter<E>
	{
		String getFilterableString(E item, int position);
	}
}
