package net.teamplace.android.browsing.dialog;

import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.utils.CloudCentralHelper;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.cortado.android.R;

public class TeamDriveExpiredDialogFragment extends DialogFragment
{
	public static final String TAG = tag(TeamDriveExpiredDialogFragment.class);

	public static final String EXTRA_TEAMDRIVE_ID = "teamDrive_id";

	public static TeamDriveExpiredDialogFragment newInstance(String teamDriveId)
	{
		Bundle arguments = new Bundle();

		arguments.putString(EXTRA_TEAMDRIVE_ID, teamDriveId);

		TeamDriveExpiredDialogFragment fragment = new TeamDriveExpiredDialogFragment();
		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Activity context = getActivity();

		final StyleableAlertDialog dialog = new StyleableAlertDialog(context);

		dialog.setTitle(R.string.brw_teamdrive_expired);

		LayoutInflater inflater = context.getLayoutInflater();
		View dialogView = inflater.inflate(R.layout.brw_teamdrive_expired_dialog, null);

		dialog.setView(dialogView);

		final String teamDriveId = getArguments().getString(EXTRA_TEAMDRIVE_ID);

		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.tmd_manage_manage),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						CloudCentralHelper.startTeamdriveManagement(getActivity(), teamDriveId);
					}
				});

		dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(R.string.tmd_manage_upgrade),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						CloudCentralHelper.startTeamdriveUpgrade(getActivity(), teamDriveId);
					}
				});

		return dialog;
	}

	@Override
	public void show(FragmentManager manager, String tag)
	{
		if (manager.findFragmentByTag(tag) == null)
		{
			super.show(manager, tag);
		}
	}
}
