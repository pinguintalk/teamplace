package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * ListView that manages a MultiSelectAdapter. Should be used everywhere multi-select is needed. The choice mode is
 * always set to none and can't be changed. This ListView can be used as a normal ListView without choice mode if need
 * be
 * 
 * @author Gil Vegliach
 */
public class MultiSelectListView extends ListView
{
	private final boolean mConstructed;

	public MultiSelectListView(Context context)
	{
		super(context);
		// Guarantees that adapter takes care of multi-select
		super.setChoiceMode(ListView.CHOICE_MODE_NONE);
		mConstructed = true;
	}

	public MultiSelectListView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		super.setChoiceMode(ListView.CHOICE_MODE_NONE);
		mConstructed = true;
	}

	public MultiSelectListView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		super.setChoiceMode(ListView.CHOICE_MODE_NONE);
		mConstructed = true;
	}

	@Override
	public void setChoiceMode(int choiceMode)
	{
		// To the maintainer: this method should not be used by external code, as it could mess up the state and the
		// interaction between MultiSelectListView and MultiSelectAdapter. Unfortunately this method is public and can't
		// be hidden, but it can be disabled at runtime throwing a RuntimeException. However, AbsListView
		// calls this method during construction so we disable the method only after construction. If the behavior of
		// AbsListView or any other parent class changes and they end up calling this method, then the maintainer must
		// change it. Removing the RuntimeExcetion should be seen a poor choice though, as it will let the called
		// perform illegal operation on this MultiSelectListView

		if (!mConstructed)
		{
			super.setChoiceMode(choiceMode);
		}
		else
		{
			throw new UnsupportedOperationException("The method setChoiceMode(int) is disabled for "
					+ MultiSelectListView.class.getSimpleName()
					+ " as it will mess up with multi-select. Check the docs");
		}
	}

	@Override
	public void setAdapter(ListAdapter adapter)
	{
		// Back-pointer (reference) pattern
		ListAdapter ladapter = getAdapter();
		if (ladapter instanceof MultiSelectAdapter)
		{
			MultiSelectAdapter msAdapter = (MultiSelectAdapter) ladapter;
			msAdapter.unbindMultiSelectListView();
		}

		super.setAdapter(adapter);

		if (adapter instanceof MultiSelectAdapter)
		{
			MultiSelectAdapter msAdapter = (MultiSelectAdapter) adapter;
			msAdapter.bindMultiSelectListView(this);
		}
	}
}
