package net.teamplace.android.browsing.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.widget.MultiSelectAdapter;
import net.teamplace.android.browsing.widget.MultiSelectAdapter.OnIconClickListener;
import net.teamplace.android.utils.GenericUtils;

import java.util.ArrayList;
import java.util.List;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Dialog that lets the user pick one or more files.
 * 
 * @author Gil Vegliach
 */
public class FilePickerDialogFragment extends DialogFragment
{
	private static final String PKG = pkg(FilePickerDialogFragment.class);

	public static final String TAG = tag(FilePickerDialogFragment.class);
	public static final int REQUEST_CODE = R.id.request_code_file_picker;
	public static final String KEY_FILES = PKG + ".files";

	private static final String KEY_FOLDER = PKG + ".folder";
	private static final String KEY_CHECKED = PKG + ".checked";
	private static final String KEY_LISTVIEW_STATE = PKG + ".listviewstate";
	private static final String KEY_LAST_POSTION = PKG + ".list_position";

	private static final int VIEW_SWITCHER_DEFAULT = 0;
	private static final int VIEW_SWITCHER_MULTISELECT = 1;
	private static final Path DEFAULT_FOLDER = Path.localRoot();

	private int mRequestId = Contract.NO_ID;
	private ViewAnimator mViewAnimator;

	private Path mCurrFolder;

	private MultiSelectAdapter mAdapter;
	private ViewSwitcher mMultiSelectViewSwitcher;
	private TextView mMultiSelectCounter;
	private View mMultiSelectDone;
	private ListView folderList;
	private Parcelable listViewState;
	private int mLastVisisblePosition;

	private Button mUploadBtn;

	// Used to restore the checked items. Non-null only when recreating the dialog before the first response
	private ArrayList<FileInfo> mChecked;

	public static FilePickerDialogFragment newInstance(Path folder)
	{
		FilePickerDialogFragment frag = new FilePickerDialogFragment();
		Bundle args = new Bundle();
		args.putParcelable(KEY_FOLDER, folder);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Context ctx = getActivity();
		StyleableAlertDialog dlg = new StyleableAlertDialog(ctx);
		dlg.setTitle(R.string.flp_file_picker_dlg_title);
		mViewAnimator = new ViewAnimator(ctx);
		dlg.setView(mViewAnimator);

		dlg.setButton(DialogInterface.BUTTON_NEGATIVE, ctx.getText(R.string.app_cancel),
				(DialogInterface.OnClickListener) null);
		dlg.setButton(DialogInterface.BUTTON_POSITIVE, ctx.getText(R.string.flp_file_picker_dlg_upload),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dismiss();

						Fragment frag = getTargetFragment();
						if (frag != null)
						{
							Intent i = new Intent();
							i.putExtra(KEY_FILES, mAdapter.getCheckedItems());
							frag.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, i);
						}
					}
				});

		dlg.setOnShowListener(new OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				mUploadBtn = ((AlertDialog) dialog).getButton(DialogInterface.BUTTON_POSITIVE);
				mUploadBtn.setEnabled(false);
			}
		});

		dlg.setOnKeyListener(mOnBackPressedListener);

		if (savedInstanceState != null)
		{
			mCurrFolder = savedInstanceState.getParcelable(KEY_FOLDER);
			mChecked = savedInstanceState.getParcelableArrayList(KEY_CHECKED);
		}
		else
		{
			mCurrFolder = getArguments().getParcelable(KEY_FOLDER);
		}

		if (mCurrFolder == null)
			mCurrFolder = DEFAULT_FOLDER;

		browseInto(mCurrFolder);

		return dlg;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		IntentFilter filter = new IntentFilter(Contract.ACTION_RESPONSE_BROWSE);
		LocalBroadcastManager.getInstance(activity).registerReceiver(mReceiver, filter);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putParcelable(KEY_FOLDER, mCurrFolder);

		if (folderList != null) {
			outState.putParcelable(KEY_LISTVIEW_STATE, folderList.onSaveInstanceState());
			outState.putInt(KEY_LAST_POSTION,folderList.getFirstVisiblePosition());
		}

		if (mAdapter == null)
			return;

		if (mAdapter.getCheckedCount() == 0)
			return;

		ArrayList<FileInfo> checked = mAdapter.getCheckedItems();
		outState.putParcelableArrayList(KEY_CHECKED, checked);

	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null) {
			mLastVisisblePosition = savedInstanceState.getInt(KEY_LAST_POSTION);
			listViewState = savedInstanceState.getParcelable(KEY_LISTVIEW_STATE);
		}
	}


	private void browseInto(Path path)
	{
		Context ctx = getActivity();

		if (folderList != null) {
			listViewState = folderList.onSaveInstanceState();
		}

		View pane = LayoutInflater.from(ctx).inflate(R.layout.flp_browsing_pane, mViewAnimator, false);
		TextView label = (TextView) pane.findViewById(R.id.label);
		label.setText(R.string.flp_dlg_browsing_select_files);
		TextView currentFolder = (TextView) pane.findViewById(R.id.current_folder);
		currentFolder.setText(PathUtils.shortDisplayablePath(path));

		// Hide new folder button
		pane.findViewById(R.id.new_folder).setVisibility(View.GONE);

		mMultiSelectViewSwitcher = (ViewSwitcher) pane.findViewById(R.id.header_view_switcher);
		mMultiSelectCounter = (TextView) mMultiSelectViewSwitcher.findViewById(R.id.counter);
		mMultiSelectDone = mMultiSelectViewSwitcher.findViewById(R.id.done);

		mViewAnimator.addView(pane);
		mViewAnimator.showNext();

		if (mViewAnimator.getChildCount() > 1)
			mViewAnimator.removeViewAt(0);

		mCurrFolder = path;

		Intent i = Contract.buildBrowseRequestIntent(ctx, path);
		mRequestId = Contract.extractRequestId(i);
		ctx.startService(i);
	}

	private void syncCheckedCount()
	{
		int count = mAdapter.getCheckedCount();
		mUploadBtn.setEnabled(count > 0);

		// We don't want to show 0 in the multi-select bar, but just fade out the bar itself
		if (count > 0)
			mMultiSelectCounter.setText(GenericUtils.getSelectedString(getActivity(), count));
	}

	private void uncheckAll()
	{
		mAdapter.uncheckAll();
		mMultiSelectViewSwitcher.setDisplayedChild(VIEW_SWITCHER_DEFAULT);
		syncCheckedCount();
	}

	private void handleClick(ViewParent parent, View view, int position, long id)
	{
		FileInfo fileInfo = mAdapter.getItem(position);
		if (fileInfo.isFolder())
		{
			uncheckAll();
			browseInto(fileInfo.getPath());
			return;
		}

		if (mAdapter.isViewAnimating(view))
			return;

		boolean wasMultiSelect = mAdapter.hasCheckedItems();
		mAdapter.toggle(position);
		boolean isMultiSelect = mAdapter.hasCheckedItems();

		if (isMultiSelect && !wasMultiSelect)
		{
			mMultiSelectViewSwitcher.setDisplayedChild(VIEW_SWITCHER_MULTISELECT);
		}
		else if (!isMultiSelect)
		{
			mMultiSelectViewSwitcher.setDisplayedChild(VIEW_SWITCHER_DEFAULT);
		}
		syncCheckedCount();
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int id = Contract.extractRequestId(intent);
			if (id == Contract.NO_ID || id != mRequestId)
				return;

			String action = intent.getAction();
			if (Contract.ACTION_RESPONSE_BROWSE.equals(action))
			{
				mRequestId = Contract.NO_ID;

				List<FileInfo> result = intent.getParcelableArrayListExtra(Contract.RESPONSE_BROWSE_FOLDER_CONTENT);
				if (result == null)
					return;

				// Check if we need to restore checked items
				List<Integer> checkedPositions = new ArrayList<Integer>();
				if (mChecked != null)
				{
					int size = result.size();
					for (int i = 0; i < size; i++)
					{
						if (mChecked.contains(result.get(i)))
							checkedPositions.add(i);
					}
				}
				mChecked = null;

				View pane = mViewAnimator.getCurrentView();
				folderList = (ListView) pane.findViewById(R.id.folder_list);
				folderList.setOnItemClickListener(mItemClickListener);
				folderList.setOnItemLongClickListener(mItemLongClickListener);

				Activity activity = getActivity();
				mAdapter = new MultiSelectAdapter(activity, result, false);
				mAdapter.setOnIconClickListener(mIconClickListener);
				mAdapter.check(checkedPositions);
				folderList.setAdapter(mAdapter);
				if (listViewState != null)
					folderList.onRestoreInstanceState(listViewState);


				mMultiSelectDone.setOnClickListener(new View.OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						uncheckAll();
					}
				});

				ViewSwitcher vs = (ViewSwitcher) pane.findViewById(R.id.content_view_switcher);
				vs.showNext();

				if (mAdapter.hasCheckedItems())
				{
					mMultiSelectViewSwitcher.setDisplayedChild(VIEW_SWITCHER_MULTISELECT);
					syncCheckedCount();
				}
			}
		}
	};
	private final OnIconClickListener mIconClickListener = new OnIconClickListener()
	{
		@Override
		public void onIconClick(View v, ViewGroup parent, int position, int id)
		{
			handleClick(parent.getParent(), parent, position, id);
		};
	};

	private final OnItemLongClickListener mItemLongClickListener = new OnItemLongClickListener()
	{
		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
		{
			handleClick(parent, view, position, id);
			return true;
		}
	};

	private final OnItemClickListener mItemClickListener = new OnItemClickListener()
	{
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
		{
			handleClick(parent, view, position, id);
		}
	};

	private final OnBackPressedListener mOnBackPressedListener = new OnBackPressedListener()
	{
		@Override
		protected boolean onBackPressed()
		{
			if (mAdapter != null && mAdapter.hasCheckedItems())
			{
				uncheckAll();
				return true;
			}
			if (!mCurrFolder.isRoot())
			{
				browseInto(mCurrFolder.getParentPath());
				return true;
			}
			return false;
		}
	};
}
