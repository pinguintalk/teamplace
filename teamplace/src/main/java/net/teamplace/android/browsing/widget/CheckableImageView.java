package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.ImageView;

public class CheckableImageView extends ImageView implements Checkable
{
	private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};
	private OnCheckedChangeListener mCheckedChangeListener;
	private boolean mChecked;

	public CheckableImageView(Context context)
	{
		super(context);
	}

	public CheckableImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

	}

	public CheckableImageView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	@Override
	public void setChecked(boolean checked)
	{
		if (checked == mChecked)
			return;

		mChecked = checked;
		refreshDrawableState();

		if (mCheckedChangeListener != null)
			mCheckedChangeListener.onCheckedChanged(this, checked);
	}

	@Override
	public boolean isChecked()
	{
		return mChecked;
	}

	@Override
	public void toggle()
	{
		setChecked(!mChecked);
	}

	public void setOnCheckedChangeListener(OnCheckedChangeListener listener)
	{
		mCheckedChangeListener = listener;
	}

	@Override
	public int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if (isChecked())
		{
			mergeDrawableStates(drawableState, CHECKED_STATE_SET);
		}
		return drawableState;
	}

	public interface OnCheckedChangeListener
	{
		void onCheckedChanged(CheckableImageView checkableImageView, boolean isChecked);
	}
}
