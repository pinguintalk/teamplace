package net.teamplace.android.browsing.sorting;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;


public class SortManager
{
	public static final int CATEGORY_NAME = 0;
	public static final int CATEGORY_SIZE = 1;
	public static final int CATEGORY_DATE = 2;

	public static final int ORDER_ASCENDING = 10;
	public static final int ORDER_DESCENDING = 11;

	public static final int DEFAULT_CATEGORY = CATEGORY_NAME;
	public static final int DEFAULT_ORDER = ORDER_ASCENDING;

	public static void checkCategory(int category)
	{
		if (!isCategoryConstant(category))
			throw new IllegalArgumentException("The constant is not a category, was: " + category);
	}

	public static void checkOrder(int order)
	{
		if (!isOrderConstant(order))
			throw new IllegalArgumentException("The constant is not a order, was: " + order);
	}

	public static Comparator<FileInfo> getComparator(int category, int order)
	{
		checkCategory(category);
		checkOrder(order);

		switch (category)
		{
			case CATEGORY_NAME:
			{
				if (order == ORDER_ASCENDING)
					return NAME_ASCENDING_ORDER;
				return NAME_DESCENDING_ORDER;
			}
			case CATEGORY_SIZE:
			{
				if (order == ORDER_ASCENDING)
					return SIZE_ASCENDING_ORDER;
				return SIZE_DESCENDING_ORDER;
			}
			case CATEGORY_DATE:
			{
				if (order == ORDER_ASCENDING)
					return DATE_ASCENDING_ORDER;
				return DATE_DESCENDING_ORDER;
			}
		}
		throw new RuntimeException("Should be unreachable");
	}

	public static void sort(List<FileInfo> list, int category, int order)
	{
		checkNonNullArg(list);
		checkCategory(category);
		checkOrder(order);

		// Running sort() twice on the same dataset with the same category and order
		// can produce different results: this is because the relative positions of
		// equal elements can differ depending on the category and order we start
		// from. To solve this problem we always sort the data with default settings
		// first and the sort it again with the desired settings
		Collections.sort(list, getComparator(DEFAULT_CATEGORY, DEFAULT_ORDER));
		Collections.sort(list, getComparator(category, order));
	}

	public static void setSort(Context context, Path path, int category, int order)
	{
		checkNonNullArg(path);
		checkCategory(category);
		checkOrder(order);

		SortDatabaseHelper helper = SortDatabaseHelper.getInstance(context);
		SQLiteDatabase db = helper.getWritableDatabase();
		SortTable.setSort(db, path, category, order);
	}

	public static Pair<Integer, Integer> getSort(Context context, Path path)
	{
		checkNonNullArg(path);

		SortDatabaseHelper helper = SortDatabaseHelper.getInstance(context);
		SQLiteDatabase db = helper.getReadableDatabase();
		return SortTable.getSort(db, path);
	}

	public static void resetSortPreferences(Context context)
	{
		checkNonNullArg(context);

		SortDatabaseHelper helper = SortDatabaseHelper.getInstance(context);
		SQLiteDatabase db = helper.getWritableDatabase();
		SortTable.purge(db);
	}

	public static boolean isCategoryConstant(int constant)
	{
		return constant == CATEGORY_NAME || constant == CATEGORY_SIZE
				|| constant == CATEGORY_DATE;
	}

	public static boolean isOrderConstant(int constant)
	{
		return constant == ORDER_ASCENDING || constant == ORDER_DESCENDING;
	}

	public static Comparator<FileInfo> FOLDERS_FIRST_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			if (lhs == null && rhs == null)
				return 0;

			// Nulls are smaller than everything
			if (lhs == null && rhs != null)
				return -1;
			if (lhs != null && rhs == null)
				return 1;

			int ltype = lhs.getType();
			int rtype = rhs.getType();
			if (ltype == FileInfo.FOLDER && rtype != FileInfo.FOLDER)
				return -1;

			if (ltype != FileInfo.FOLDER && rtype == FileInfo.FOLDER)
				return 1;

			return 0;
		}
	};
	public static Comparator<FileInfo> NAME_ASCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			String left = lhs.getName();
			String right = rhs.getName();
			return String.CASE_INSENSITIVE_ORDER.compare(left, right);
		}
	};
	public static Comparator<FileInfo> NAME_DESCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			String left = lhs.getName();
			String right = rhs.getName();
			return String.CASE_INSENSITIVE_ORDER.compare(right, left);
		}
	};
	public static Comparator<FileInfo> SIZE_ASCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			long left = lhs.getSize();
			long right = rhs.getSize();
			return left < right ? -1 : (left == right ? 0 : 1);
		}
	};
	public static Comparator<FileInfo> SIZE_DESCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			long left = lhs.getSize();
			long right = rhs.getSize();
			return left < right ? 1 : (left == right ? 0 : -1);
		}
	};
	public static Comparator<FileInfo> DATE_ASCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			long left = lhs.getLastModifiedDate();
			long right = rhs.getLastModifiedDate();
			return left < right ? -1 : (left == right ? 0 : 1);
		}
	};
	public static Comparator<FileInfo> DATE_DESCENDING_ORDER = new Comparator<FileInfo>()
	{
		@Override
		public int compare(FileInfo lhs, FileInfo rhs)
		{
			int result = FOLDERS_FIRST_ORDER.compare(lhs, rhs);
			if (result != 0)
				return result;

			long left = lhs.getLastModifiedDate();
			long right = rhs.getLastModifiedDate();
			return left < right ? 1 : (left == right ? 0 : -1);
		}
	};
}
