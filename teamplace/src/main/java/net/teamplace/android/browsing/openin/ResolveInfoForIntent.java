package net.teamplace.android.browsing.openin;

import static net.teamplace.android.utils.GenericUtils.tag;
import android.content.Intent;
import android.content.pm.ResolveInfo;

public class ResolveInfoForIntent
{
	private final String TAG = tag(ResolveInfoForIntent.class);

	private Intent intent;
	private ResolveInfo resolveInfo;
	private int contentStrategy;

	public ResolveInfoForIntent(Intent intent, ResolveInfo resolveInfo, int contentStrategy)
	{
		this.intent = intent;
		this.resolveInfo = resolveInfo;
		this.contentStrategy = contentStrategy;
	}

	public Intent getIntent()
	{
		return intent;
	}

	public void setIntent(Intent intent)
	{
		this.intent = intent;
	}

	public ResolveInfo getResolveInfo()
	{
		return resolveInfo;
	}

	public void setResolveInfo(ResolveInfo resolveInfo)
	{
		this.resolveInfo = resolveInfo;
	}

	public int getContentStrategy()
	{
		return contentStrategy;
	}

	public void setContentStrategy(int contentStrategy)
	{
		this.contentStrategy = contentStrategy;
	}

	@Override
	public String toString()
	{
		return "Action: " + intent + ", Resolve info: " + resolveInfo;
	}
}
