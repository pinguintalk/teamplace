package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Checkable;
import android.widget.RelativeLayout;

/**
 * RelativeLayout that supports the Checkable interface
 * 
 * @author Gil Vegliach
 */
public class CheckableRelativeLayout extends RelativeLayout implements Checkable
{
	private boolean mChecked = false;

	private static final int[] CHECKED_STATE_SET = {android.R.attr.state_checked};

	private OnAttachStateChangeListener mListener;

	public CheckableRelativeLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
	}

	public CheckableRelativeLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public CheckableRelativeLayout(Context context)
	{
		super(context);
	}

	@Override
	public boolean isChecked()
	{
		return mChecked;
	}

	@Override
	public void setChecked(boolean checked)
	{
		mChecked = checked;
		refreshDrawableState();
	}

	@Override
	public void toggle()
	{
		setChecked(!mChecked);
	}

	public void setOnAttachStateChangeListener(OnAttachStateChangeListener listener)
	{
		mListener = listener;
	}

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		final int[] drawableState = super.onCreateDrawableState(extraSpace + 1);
		if (isChecked())
		{
			mergeDrawableStates(drawableState, CHECKED_STATE_SET);
		}
		return drawableState;
	}

	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();
		if (mListener != null)
			mListener.onViewAttachedToWindow(this);

	}

	@Override
	protected void onDetachedFromWindow()
	{
		super.onDetachedFromWindow();

		if (mListener != null)
			mListener.onViewDetachedFromWindow(this);
	}

	public static interface OnAttachStateChangeListener
	{
		void onViewAttachedToWindow(View v);

		void onViewDetachedFromWindow(View v);
	}
}
