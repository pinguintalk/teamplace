package net.teamplace.android.browsing.path;

/**
 * Visitor pattern for a Path.
 * 
 * @author Gil Vegliach
 */
public interface PathOperation<T>
{
	T execute(Path.Local path);

	T execute(Path.Remote path);

	T execute(Path.TeamDrive path);
}
