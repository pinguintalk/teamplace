package net.teamplace.android.browsing;

import android.util.Pair;

import net.teamplace.android.browsing.path.Path;

import java.util.ArrayList;
import java.util.List;


/**
 * Represents a file operation call to BrowsingService with possibly multiple source paths and one destination path.
 * Each source path is represented by a {@link PathStateInfo} object. Each source path stores its own state.
 * 
 * @author jobol
 */
public final class FileJobInfo
{
	public static final int STATE_ACTIVE = 0;
	public static final int STATE_QUEUED = 1;
	public static final int STATE_PAUSED = 2;
	public static final int STATE_FINISHED = 3;
	public static final int STATE_ERROR = 4;
	public static final int STATE_CANCEL = 5;
	
	private final long created;
	private final int action;
	private final Path dest;
	private final PathStateInfo[] pathStateInfos;

	public FileJobInfo(long created, int action, PathStateInfo[] sourceInfos, Path dest)
	{
		this.created = created;
		this.action = action;
		this.dest = dest;
		this.pathStateInfos = sourceInfos;
	}

	public long getCreated()
	{
		return created;
	}

	public int getAction()
	{
		return action;
	}

	public PathStateInfo[] getSourceInfos()
	{
		return pathStateInfos;
	}

	public Path getDest()
	{
		return dest;
	}

	private List<Pair<PathStateInfo, Long>> getPathStateInfoIdPairListForState(int state)
	{
		List<Pair<PathStateInfo, Long>> resultList = new ArrayList<>();
		for (PathStateInfo info : pathStateInfos)
		{
			if (info.state == state)
			{
				resultList.add(new Pair<FileJobInfo.PathStateInfo, Long>(info, created));
			}
		}
		return resultList;
	}

	public boolean allFinishedOrFailed()
	{
		for (PathStateInfo info : pathStateInfos)
		{
			if (info.state != STATE_FINISHED && info.state != STATE_ERROR)
			{
				return false;
			}
		}
		return true;
	}

	public boolean isStillActive()
	{
		for (PathStateInfo info : pathStateInfos)
		{
			if (info.state == STATE_ACTIVE)
			{
				return true;
			}
		}
		return false;
	}

	public static List<Pair<PathStateInfo, Long>> getPathStateInfoIdPairListForState(int state,
			FileJobInfo... jobsToEvaluate)
	{
		List<Pair<PathStateInfo, Long>> resultList = new ArrayList<>();
		for (FileJobInfo job : jobsToEvaluate)
		{
			resultList.addAll(job.getPathStateInfoIdPairListForState(state));
		}
		return resultList;
	}

	public void updatePathStateInfo(Path[] paths, int state, int progressPercent)
	{
		updatePathStateInfo(paths, state, progressPercent, null);
	}

	public void updatePathStateInfo(Path[] paths, int state, int progressPercent, Exception exception)
	{
		for (Path path : paths)
		{
			PathStateInfo toUpdate = null;
			for (PathStateInfo infoInArray : pathStateInfos)
			{
				if (infoInArray.getPath().toString().equals(path.toString()))
				{
					toUpdate = infoInArray;
					break;
				}
			}
			if (toUpdate == null)
			{
				// throw new IllegalArgumentException("PathStateInfo object not contained");
				return;
			}
			toUpdate.progressPercent = progressPercent;
			toUpdate.state = state;
			toUpdate.exception = exception;
		}
	}

	public static PathStateInfo[] buildPathStateInfoArrayWithSingleState(Path[] paths, int state, int progressPercent)
	{
		return buildPathStateInfoArrayWithSingleState(paths, state, progressPercent, null);
	}

	public static PathStateInfo[] buildPathStateInfoArrayWithSingleState(Path[] paths, int state, int progressPercent,
			Exception e)
	{
		PathStateInfo[] stateArr = new PathStateInfo[paths.length];
		for (int i = 0; i < paths.length; i++)
		{
			stateArr[i] = new PathStateInfo(paths[i], state, progressPercent, e);
		}
		return stateArr;
	}

	/**
	 * Represents a source path within a {@link FileJobInfo} object.
	 * 
	 * @author jobol
	 */
	public static class PathStateInfo
	{
		private final Path path;
		private int state;
		private int progressPercent;
		private Exception exception;

		public PathStateInfo(Path path, int state, int progressPercent, Exception exception)
		{
			this.path = path;
			this.state = state;
			this.progressPercent = progressPercent;
			this.exception = exception;
		}

		public PathStateInfo(Path path, int state, int progressPercent)
		{
			this(path, state, progressPercent, null);
		}

		public Path getPath()
		{
			return path;
		}

		public int getState()
		{
			return state;
		}

		public void setState(int state)
		{
			this.state = state;
		}

		public int getProgressPercent()
		{
			return progressPercent;
		}

		public void setProgressPercent(int progressPercent)
		{
			this.progressPercent = progressPercent;
		}

		public Exception getException()
		{
			return exception;
		}

		public void setException(Exception exception)
		{
			this.exception = exception;
		}
	}

}
