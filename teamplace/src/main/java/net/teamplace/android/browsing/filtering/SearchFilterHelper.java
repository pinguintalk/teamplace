package net.teamplace.android.browsing.filtering;

import android.content.res.Resources;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.SearchView;

import com.cortado.android.R;

/**
 * Helps coordinating a SearchView in the action bar with a {@link FilterableAdapter}.
 * <p>
 * The purpose of this class is to help in the implementation of the complex relationship between SearchView and
 * FilterableAdapter, providing a common reusable base. Once an object of this class is created, a filterable adapter
 * should be bound with {@link #bindFilterableAdapter(FilterableAdapter)}. If the adapter dies, it should be unbound
 * calling <code>bindFilterableAdapter(null)<code>. 
 * In {@link android.app.Activity#onCreateOptionsMenu(android.view.Menu)} call
 * {@link #bindSearchMenuItem(MenuItem)} passing a MenuItem containing a SearchView. Then every time you need to
 * collapse the SearchView and disable filtering, you can call {@link #collapseSearchView()}.
 * </p>
 * 
 * @author Gil Vegliach
 */
public class SearchFilterHelper
{
	private MenuItem mSearchItem;
	private FilterableAdapter mAdapter;
	private String mCurrFilter;

	public String getCurrFilterString() {
		return mCurrFilter;
	}

	public void setCurrFilterString(String currFilter) {
		this.mCurrFilter = currFilter;
	}

	public void bindFilterableAdapter(FilterableAdapter adapter)
	{
		mAdapter = adapter;
	}

	public void bindSearchMenuItem(MenuItem searchMenuItem)
	{
		mSearchItem = searchMenuItem;

		if (mSearchItem != null)
			setUpSearchView();
	}

	public void collapseSearchView()
	{
		if (mSearchItem != null) {
			mSearchItem.collapseActionView();
		}

		if (mAdapter.isFilterEnabled())
			mAdapter.disableFilter();
	}

	private SearchView setUpSearchView()
	{
		View actionView = mSearchItem.getActionView();
		if (!(actionView instanceof SearchView))
		{
			throw new IllegalArgumentException(
					"You must bind a MenuItem containing a valid android.widget.SearchView");
		}

		final SearchView sv = (SearchView) actionView;
		styleSearchView(sv);

		mSearchItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener()
		{
			@Override
			public boolean onMenuItemActionExpand(MenuItem item)
			{
				iconifySearchView(sv,mCurrFilter);

				if (mAdapter != null)
					mAdapter.disableFilter();

				return true;
			}

			@Override
			public boolean onMenuItemActionCollapse(MenuItem item)
			{
				iconifySearchView(sv,"");

				if (mAdapter != null)
					mAdapter.disableFilter();

				return true;
			}
		});
		sv.setOnQueryTextListener(new SearchView.OnQueryTextListener()
		{
			@Override
			public boolean onQueryTextSubmit(String query)
			{
				mSearchItem.collapseActionView();
				return true;
			}

			@Override
			public boolean onQueryTextChange(String newText)
			{
				if (mAdapter != null) {
					mAdapter.setFilter(newText);
					mCurrFilter = newText;
				}

				return true;
			}
		});
		sv.setImeOptions(EditorInfo.IME_ACTION_DONE);
		return sv;
	}

	private void styleSearchView(SearchView searchView)
	{
		// Taken from:
		// http://stackoverflow.com/questions/11085308/changing-the-background-drawable-of-the-searchview-widget
		Resources res = searchView.getContext().getResources();
		int searchPlateId = res.getIdentifier("android:id/search_plate", null, null);
		View searchPlate = searchView.findViewById(searchPlateId);
		searchPlate.setBackgroundResource(R.drawable.brw_search_bg);
	}

	private static void iconifySearchView(SearchView sv,String currFilter)
	{
		sv.setQuery("", false);
		sv.setIconified(true);
	}
}