package net.teamplace.android.browsing.widget;

import static net.teamplace.android.utils.GenericUtils.tag;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;

import com.cortado.android.R;

/**
 * Subclass of FrameLayout that holds two Views (or Viewgroups) and has methods to flip between them with an animation
 * 
 * @author Gil Vegliach
 */
public class FlipLayout extends FrameLayout
{
	public static final String TAG = tag(FlipLayout.class);

	private static final FrontAnimationInterpolator sFrontAnimaInterp = new FrontAnimationInterpolator();
	private static final BackAnimationInterpolator sBackAnimInterp = new BackAnimationInterpolator();

	private View mFront;
	private View mBack;

	private boolean mIsAnimating;
	private boolean mIsFrontShowing = true;

	private OnFlipListener mListener;

	public FlipLayout(Context context)
	{
		super(context);
	}

	public FlipLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initFromAttrs(attrs);
	}

	public FlipLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		initFromAttrs(attrs);
	}

	private void initFromAttrs(AttributeSet attrs)
	{
		Context context = getContext();
		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.FlipLayout, 0, 0);

		try
		{
			mIsFrontShowing = a.getInt(R.styleable.FlipLayout_display, 1) == 1 ? true : false;
		}
		finally
		{
			a.recycle();
		}
	}

	/**
	 * Sort of initialization. It cannot be done before because the children hadn't been added to the layout yet,
	 * especially during inflating (getChildCount() == 0)
	 */
	@Override
	protected void onAttachedToWindow()
	{
		super.onAttachedToWindow();

		int count = getChildCount();

		if (count > 0)
			mFront = getChildAt(0);

		if (count > 1)
			mBack = getChildAt(1);

		if (count > 2)
			throw new IllegalStateException("Only two children allowed in a FlipLayout");

		mIsAnimating = false;

		displayView(mIsFrontShowing);
	}

	public void setFrontView(View v)
	{
		mFront = v;
		addView(mFront, 0);
		invalidate();
	}

	public void setBackView(View v)
	{
		mBack = v;
		addView(mBack, 1);
		invalidate();
	}

	public View getFrontView()
	{
		return mFront;
	}

	public View getBackView()
	{
		return mBack;
	}

	/**
	 * Displays front view is front is true, otherwise displays back view. This method does not use animations and
	 * automatically cancels animations in progress
	 */
	public void displayView(boolean front)
	{
		if (front)
		{
			displayFrontView();
		}
		else
		{
			displayBackView();
		}
	}

	/** Forces back view to be displayed and cancel animations in progress */
	public void displayBackView()
	{
		// Clears animations
		if (mIsAnimating)
			clearFlipAnimation();

		if (mFront != null)
			mFront.setVisibility(View.INVISIBLE);

		if (mBack != null)
			mBack.setVisibility(View.VISIBLE);

		mIsFrontShowing = false;
		invalidate();
	}

	/** Forces front view to be displayed and cancel animations in progress */
	public void displayFrontView()
	{
		// Clears animations
		if (mIsAnimating)
			clearFlipAnimation();

		if (mFront != null)
			mFront.setVisibility(View.VISIBLE);

		if (mBack != null)
			mBack.setVisibility(View.INVISIBLE);

		mIsFrontShowing = true;
		invalidate();
	}

	/**
	 * Clears all animations, back and front view included. This method behaves as the animation had never taken place
	 */
	public void clearFlipAnimation()
	{
		// Sets this before clearing because the callback's behavious depends on this flag
		mIsAnimating = false;

		if (mFront != null)
			mFront.clearAnimation();

		if (mBack != null)
			mBack.clearAnimation();

	}

	/** Flips the two view with the animation */
	public void flip()
	{
		flip(true);
	}

	public void flip(boolean withAnimation)
	{
		if (mFront == null || mBack == null)
			return;

		if (!withAnimation)
		{
			displayView(!mIsFrontShowing);
			return;
		}

		// Flip with animation
		if (!mIsAnimating)
		{
			mIsAnimating = true;

			final View current, next;
			if (mIsFrontShowing)
			{
				current = mFront;
				next = mBack;
			}
			else
			{
				current = mBack;
				next = mFront;
			}

			current.setVisibility(View.VISIBLE);
			next.setVisibility(View.INVISIBLE);

			final Animation frontAnim = AnimationUtils.loadAnimation(getContext(), R.anim.new_flip_out);
			final Animation backAnim = AnimationUtils.loadAnimation(getContext(), R.anim.new_flip_in);

			final int duration = getContext().getResources().getInteger(R.integer.flip_time_half);
			frontAnim.setDuration(duration);
			frontAnim.setInterpolator(sFrontAnimaInterp);

			backAnim.setDuration(duration);
			backAnim.setInterpolator(sBackAnimInterp);

			frontAnim.setAnimationListener(new Animation.AnimationListener()
			{
				@Override
				public void onAnimationEnd(Animation animation)
				{
					if (mIsAnimating)
					{
						next.setVisibility(View.VISIBLE);
						current.setVisibility(View.INVISIBLE);
						next.startAnimation(backAnim);
					}
					else
					{
						// Synchronizes graphics and state
						displayView(mIsFrontShowing);

						if (mListener != null)
							mListener.onFlipCanceled(FlipLayout.this, next, current);
					}
				}

				@Override
				public void onAnimationRepeat(Animation animation)
				{
				}

				@Override
				public void onAnimationStart(Animation animation)
				{
					if (mListener != null)
						mListener.onFlipStarted(FlipLayout.this, next, current);
				}
			});

			backAnim.setAnimationListener(new AnimationListener()
			{
				@Override
				public void onAnimationStart(Animation animation)
				{
				}

				@Override
				public void onAnimationRepeat(Animation animation)
				{
				}

				@Override
				public void onAnimationEnd(Animation animation)
				{
					if (mIsAnimating)
					{
						mIsAnimating = false;
						mIsFrontShowing = !mIsFrontShowing;

						if (mListener != null)
							mListener.onFlipFinished(FlipLayout.this, next, current);
					}
					else
					{
						displayView(mIsFrontShowing);

						if (mListener != null)
							mListener.onFlipCanceled(FlipLayout.this, next, current);
					}
				}
			});

			current.startAnimation(frontAnim);
		}

	}

	public void setOnFlipListener(OnFlipListener listener)
	{
		mListener = listener;
	}

	@Override
	protected Parcelable onSaveInstanceState()
	{
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.isFrontShowing = mIsFrontShowing;

		return ss;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state)
	{
		if (!(state instanceof SavedState))
		{
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());

		mIsFrontShowing = ss.isFrontShowing;
	}

	private static class SavedState extends BaseSavedState
	{
		boolean isFrontShowing;

		public SavedState(Parcelable superState)
		{
			super(superState);
		}

		public SavedState(Parcel in)
		{
			super(in);
			isFrontShowing = in.readInt() == 1 ? true : false;
		}

		@Override
		public void writeToParcel(Parcel out, int flags)
		{
			super.writeToParcel(out, flags);
			out.writeInt(isFrontShowing ? 1 : 0);
		}

		@SuppressWarnings("unused")
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>()
		{
			@Override
			public SavedState createFromParcel(Parcel in)
			{
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size)
			{
				return new SavedState[size];
			}
		};
	}

	private static class FrontAnimationInterpolator implements Interpolator
	{
		@Override
		public float getInterpolation(float t)
		{
			return (float) (1.0 - Math.cos(t * Math.PI / 2.0));
		}
	}

	private static class BackAnimationInterpolator implements Interpolator
	{
		@Override
		public float getInterpolation(float t)
		{
			return (float) Math.sin(t * Math.PI / 2.0);
		}
	}

	public interface OnFlipListener
	{
		void onFlipStarted(View container, View entering, View exiting);

		void onFlipFinished(View container, View entering, View exiting);

		void onFlipCanceled(View container, View entering, View exiting);
	}
}
