package net.teamplace.android.browsing.ui;

import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

/** Wraps an ActionMode.Callback object and delegates to it */
public class ActionModeCallbackWrapper implements ActionMode.Callback
{
	private final ActionMode.Callback mDelegate;

	public ActionModeCallbackWrapper(ActionMode.Callback delegate)
	{
		mDelegate = delegate;
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu)
	{
		return mDelegate.onCreateActionMode(mode, menu);
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu)
	{
		return mDelegate.onPrepareActionMode(mode, menu);
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item)
	{
		return mDelegate.onActionItemClicked(mode, item);
	}

	@Override
	public void onDestroyActionMode(ActionMode mode)
	{
		mDelegate.onDestroyActionMode(mode);
	}
}