package net.teamplace.android.browsing.navigation;

import android.animation.LayoutTransition;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Checkable;

import com.cortado.android.R;

import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.browsing.widget.RemoteDrawerItemView;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.teamdrive.quota.AccountQuotaModel;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.utils.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.assertTrue;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

public class NavigationFragment extends Fragment implements Navigation, RemoteDrawerItemView.QuotaExpandListener {
	private static final String PKG = pkg(NavigationFragment.class);
	public static final String TAG = tag(NavigationFragment.class);


	private static final int NO_CHECKED_POSITION = -1;

	/**
	 * Non-null iff the Drawer displays labeled views, not only ImageViews
	 */
	private RemoteDrawerItemView mRemoteDrawerItemView;
	private Navigation.Callback mCallback;

	/**
	 * Contains Checkable children of the ViewGroup with id R.id.container
	 */
	private final List<Checkable> mCheckables = new ArrayList<Checkable>();
	private final View.OnClickListener mClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View view) {
			clickView(view);
		}
	};

	private State mState = new State();
	private StateResolver mStateResolver = new StateResolver();

	private static class State {
		long teamDriveServiceReuestCode = -1l;
		AccountQuotaModel quota;
		boolean quotaLoaded = false;
		boolean quotaViewCollapsed = true;
		int checkedPosition = NO_CHECKED_POSITION;
	}

	private class StateResolver
	{
		public void onEventMainThread(State event)
		{
			mState = event;
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		EventBus.getDefault().registerSticky(mStateResolver);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d(TAG, "onCreateView()");
		View root = inflater.inflate(R.layout.brw_drawer, container, false);
		ViewGroup itemContainer = (ViewGroup) root.findViewById(R.id.navigation_container);
		LayoutTransition lt = new LayoutTransition();
		lt.enableTransitionType(LayoutTransition.CHANGING);
		lt.enableTransitionType(LayoutTransition.CHANGE_APPEARING);
		lt.enableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);
		itemContainer.setLayoutTransition(lt);
		setupItems(itemContainer);
		return root;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (mRemoteDrawerItemView != null)
			mRemoteDrawerItemView.setmExpandListener(this);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		Log.d(TAG, "onActivityCreated()");

		if (savedInstanceState != null)
		{
			restoreState(savedInstanceState);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		EventBus.getDefault().unregister(mStateResolver);
		if (hasLabeledViews())
		{
			mState.quotaViewCollapsed = mRemoteDrawerItemView.isCollapsed();
			EventBus.getDefault().postSticky(mState);
		}
	}

	@Override
	public void onPause() {
		EventBus.getDefault().unregister(this);
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
	}

	@Override
	public void setNavigationCallback(Navigation.Callback callback)
	{
		mCallback = callback;
	}

	@Override
	public void select(TopLevel topLevel)
	{
		View drawerItem = findViewByTopLevel(topLevel);
		clickView(drawerItem);
	}

	private boolean hasLabeledViews()
	{
		return mRemoteDrawerItemView != null;
	}

	private void clickView(View view)
	{
		if (!(view instanceof Checkable))
			return;

		int newCheckedPosition = mCheckables.indexOf(view);
		if (newCheckedPosition == NO_CHECKED_POSITION)
			return;

		if (mState.checkedPosition != newCheckedPosition) {
			if (mState.checkedPosition != NO_CHECKED_POSITION)
				mCheckables.get(mState.checkedPosition).setChecked(false);
		}
		
		mCheckables.get(newCheckedPosition).setChecked(true);

		mState.checkedPosition = newCheckedPosition;

		if (hasLabeledViews())
			mRemoteDrawerItemView.collapseQuotaInfo();

		hideKeyboard(getActivity(),view);
		
		notifyFromId(view.getId());
	}


	public static void hideKeyboard(Context context, View view) {
		    InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
		    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
	
	private View findViewByTopLevel(TopLevel topLevel)
	{
		View root = getView();
		View view = null;
		switch (topLevel)
		{
			case FAVORITES:
				view = root.findViewById(R.id.favorites);
				break;
			case REMOTE:
				view = findRemoteItemView();
				break;
			case TEAMDRIVES:
				view = root.findViewById(R.id.teamdrives);
				break;
			case LOCAL:
				view = root.findViewById(R.id.local);
				break;
			case SETTINGS:
				view = root.findViewById(R.id.settings);
				break;
		}
		assertTrue(view != null,
				"Check that NavigationFragment be in sync with the navigation items in its layout");
		return view;
	}

	private View findRemoteItemView()
	{
		View root = getView();
		View remoteItem = root.findViewById(R.id.remote_main_item);
		if (remoteItem == null)
			remoteItem = root.findViewById(R.id.remote);
		return remoteItem;
	}

	private void notifyFromId(int id)
	{
		if (mCallback == null)
			return;

		if (id == R.id.favorites)
		{
			mCallback.onNavigationTopLevelSelected(TopLevel.FAVORITES);
		}
		else if (id == R.id.remote || id == R.id.remote_main_item)
		{
			mCallback.onNavigationTopLevelSelected(TopLevel.REMOTE);
		}
		else if (id == R.id.teamdrives)
		{
			mCallback.onNavigationTopLevelSelected(TopLevel.TEAMDRIVES);
		}
		else if (id == R.id.local)
		{
			mCallback.onNavigationTopLevelSelected(TopLevel.LOCAL);
		}
		else if (id == R.id.settings)
		{
			mCallback.onNavigationTopLevelSelected(TopLevel.SETTINGS);
		}
	}

	private void restoreState(Bundle savedInstanceState)
	{
		if (mState.checkedPosition != NO_CHECKED_POSITION) {
			mCheckables.get(mState.checkedPosition).setChecked(true);
		}

		if (!mState.quotaViewCollapsed && hasLabeledViews())
		{
			mRemoteDrawerItemView.expandQuotaInfo();
		}
	}

	private void setupItems(ViewGroup itemContainer)
	{
		int count = itemContainer.getChildCount();
		for (int i = 0; i < count; i++)
		{
			View child = itemContainer.getChildAt(i);
			int id = child.getId();
			if (id == R.id.remote)
			{
				View view = child.findViewById(R.id.remote_main_item);
				if (view != null)
				{
					mRemoteDrawerItemView = (RemoteDrawerItemView) child;
					child = view;
				}
				child.setOnClickListener(mClickListener);
			}
			else if (id > 0)
			{
				child.setOnClickListener(mClickListener);
			}

			if (child instanceof Checkable)
			{
				mCheckables.add((Checkable) child);
			}
		}
	}

	@Override
	public void onExpandStateChanged(int state) {
		switch (state)
		{
			case RemoteDrawerItemView.QuotaExpandListener.EXPANDING:
				if (!mState.quotaLoaded)
				{
					mState.teamDriveServiceReuestCode = IdGenerator.getId();
					TeamDriveService.getAccountQuota(getActivity().getApplicationContext(), mState.teamDriveServiceReuestCode);
				}
				else
				{
					mRemoteDrawerItemView.setQuota(mState.quota);
					mRemoteDrawerItemView.refreshQuota();
				}
				break;
			case RemoteDrawerItemView.QuotaExpandListener.COLLAPSING:
				mState.quotaLoaded = false;
				break;
			default:
				break;
		}
	}

	public void onEventMainThread(TeamDriveService.TeamDriveServiceResponse event) {
		if (event.getRequestCode() == mState.teamDriveServiceReuestCode) {
			try {
				if (event.getResponseData() != null) {
					mState.quota = (AccountQuotaModel) event.getResponseData();
					mRemoteDrawerItemView.setQuota(mState.quota);
					mRemoteDrawerItemView.refreshQuota();
					mState.quotaLoaded = true;
				}
			} catch (CortadoException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
