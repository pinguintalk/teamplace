package net.teamplace.android.browsing.path;

import java.io.IOException;

public interface FileSystem
{
	Path.Local[] listChildren(Path.Local path);

	boolean mkdir(Path.Local path);

	boolean mkdirs(Path.Local path);

	boolean delete(Path.Local path, boolean force);

	boolean rename(Path.Local path, String newName);

	boolean isDirectory(Path.Local path);

	long lastModified(Path.Local path);

	long length(Path.Local path);

	void copy(Path.Local[] sources, Path.Local dest) throws IOException;

	void move(Path.Local[] sources, Path.Local dest) throws IOException;
}