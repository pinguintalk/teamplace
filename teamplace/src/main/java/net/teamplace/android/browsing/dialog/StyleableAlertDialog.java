package net.teamplace.android.browsing.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.cortado.android.R;

/**
 * Allows basic styling of an AlertDialog, i.e. changing title's color, divider's color, divider's thickness and button
 * text's color. By default the attributes from R.style.StyleableAlertDialogDefault are loaded. This class relies on
 * some hard-coded ids in the Android platform, so it can break in the future. If that happens, the class will continue
 * to work but the styling will be lost. In that case, change the ids in {@link #bindView()}. Setting a custom title
 * View will override the title's color
 * 
 * @author Gil Vegliach
 */
public class StyleableAlertDialog extends AlertDialog
{
	private boolean mStarted = false;

	// This is an ImageView in the xml but findViewById() returns only a View for some obscure reason
	private TextView mTitle;
	private TextView mMessage;
	private View mTitleDivider;

	private int mTitleColor = -1;
	private int mMessageColor = -1;
	private int mDividerColor = -1;
	private int mDividerThickness = -1;

	private Button mPositiveButton;
	private Button mNegativeButton;
	private Button mNeutralButton;

	// ColorStateList will be preferred over color in lazyStyle()
	private ColorStateList mPositiveButtonTextColorList;
	private ColorStateList mNegativeButtonTextColorList;
	private ColorStateList mNeutralButtonTextColorList;

	private int mPositiveButtonTextColor = -1;
	private int mNegativeButtonTextColor = -1;
	private int mNeutralButtonTextColor = -1;

	public StyleableAlertDialog(Context context)
	{
		this(context, AlertDialog.THEME_HOLO_LIGHT);
	}

	/**
	 * Construct a StyleableAlertDialog. The parameter theme is not to load the divider and title styles. It is rather
	 * the theme parameter in the protect constructor {@link android.app.AlertDialog#AlertDialog(Context, int)
	 * AlertDialog(Context, int)}
	 */
	public StyleableAlertDialog(Context context, int theme)
	{
		super(context, theme);

		TypedArray a = getContext().getTheme().obtainStyledAttributes(null, R.styleable.StyleableAlertDialog, 0,
				R.style.StyleableAlertDialogDefault);
		try
		{
			setTitleColor(a.getColor(R.styleable.StyleableAlertDialog_titleColor, -1));
			setMessageColor(a.getColor(R.styleable.StyleableAlertDialog_messageColor, -1));
			setDividerColor(a.getColor(R.styleable.StyleableAlertDialog_dividerColor, -1));
			setDividerThickness(a.getDimensionPixelSize(R.styleable.StyleableAlertDialog_dividerThickness, -1));

			setPositiveButtonTextColor(a.getColorStateList(R.styleable.StyleableAlertDialog_positiveButtonTextColor));
			setNegativeButtonTextColor(a.getColorStateList(R.styleable.StyleableAlertDialog_negativeButtonTextColor));
			setNeutralButtonTextColor(a.getColorStateList(R.styleable.StyleableAlertDialog_neutralButtonTextColor));
		}
		finally
		{
			a.recycle();
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
	}

	@Override
	protected void onStart()
	{
		super.onStart();
		mStarted = true;
		bindView();
		lazyStyle();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
		mStarted = false;
	}
	

	public void setDividerColor(int color)
	{
		if (!mStarted)
		{
			mDividerColor = color;
			return;
		}

		if (mTitleDivider == null)
			return;

		mTitleDivider.setBackgroundColor(color);
	}

	public void setDividerThickness(int thicknessPx)
	{
		if (!mStarted)
		{
			mDividerThickness = thicknessPx;
			return;
		}

		if (mTitleDivider == null)
			return;

		ViewGroup.LayoutParams lp = mTitleDivider.getLayoutParams();
		lp.height = thicknessPx;
		mTitleDivider.setLayoutParams(lp);
	}

	public void setTitleColor(int color)
	{
		if (!mStarted)
		{
			mTitleColor = color;
			return;
		}

		if (mTitle == null)
			return;

		mTitle.setTextColor(color);
	}

	public void setMessageColor(int color)
	{
		if (!mStarted)
		{
			mMessageColor = color;
			return;
		}

		if (mMessage == null)
			return;

		mMessage.setTextColor(color);
	}

	public void setPositiveButtonTextColor(ColorStateList cslist)
	{
		if (!mStarted)
		{
			mPositiveButtonTextColorList = cslist;
			return;
		}

		if (mPositiveButton == null)
			return;

		mPositiveButton.setTextColor(cslist);
	}

	public void setPositiveButtonTextColor(int color)
	{
		if (!mStarted)
		{
			mPositiveButtonTextColor = color;
			return;
		}

		if (mPositiveButton == null)
			return;

		mPositiveButton.setTextColor(color);
	}

	public void setNegativeButtonTextColor(ColorStateList cslist)
	{
		if (!mStarted)
		{
			mNegativeButtonTextColorList = cslist;
			return;
		}

		if (mNegativeButton == null)
			return;

		mNegativeButton.setTextColor(cslist);
	}

	public void setNegativeButtonTextColor(int color)
	{
		if (!mStarted)
		{
			mNegativeButtonTextColor = color;
			return;
		}

		if (mNegativeButton == null)
			return;

		mNegativeButton.setTextColor(color);
	}

	public void setNeutralButtonTextColor(ColorStateList cslist)
	{
		if (!mStarted)
		{
			mNeutralButtonTextColorList = cslist;
			return;
		}

		if (mNeutralButton == null)
			return;

		mNeutralButton.setTextColor(cslist);
	}

	public void setNeutralButtonTextColor(int color)
	{
		if (!mStarted)
		{
			mNeutralButtonTextColor = color;
			return;
		}

		if (mNeutralButton == null)
			return;

		mNeutralButton.setTextColor(color);
	}

	private void bindView()
	{
		Resources res = getContext().getResources();

		int id = res.getIdentifier("android:id/titleDivider", null, null);
		mTitleDivider = findViewById(id);

		id = res.getIdentifier("android:id/alertTitle", null, null);
		View v = findViewById(id);
		if (v instanceof TextView)
		{
			mTitle = (TextView) v;
		}

		id = res.getIdentifier("android:id/message", null, null);
		v = findViewById(id);
		if (v instanceof TextView)
		{
			mMessage = (TextView) v;
		}

		id = res.getIdentifier("android:id/button1", null, null);
		v = findViewById(id);
		if (v instanceof Button)
		{
			mPositiveButton = (Button) v;
		}

		id = res.getIdentifier("android:id/button2", null, null);
		v = findViewById(id);
		if (v instanceof Button)
		{
			mNegativeButton = (Button) v;
		}

		id = res.getIdentifier("android:id/button3", null, null);
		v = findViewById(id);
		if (v instanceof Button)
		{
			mNeutralButton = (Button) v;
		}
	}

	private void lazyStyle()
	{
		if (mTitleColor != -1)
			setTitleColor(mTitleColor);

		if (mMessageColor != -1)
			setMessageColor(mMessageColor);

		if (mDividerColor != -1)
			setDividerColor(mDividerColor);

		if (mDividerThickness != -1)
			setDividerThickness(mDividerThickness);

		if (mPositiveButtonTextColorList != null)
		{
			setPositiveButtonTextColor(mPositiveButtonTextColorList);
		}
		else if (mPositiveButtonTextColor != -1)
		{
			setPositiveButtonTextColor(mPositiveButtonTextColor);
		}

		if (mNegativeButtonTextColorList != null)
		{
			setNegativeButtonTextColor(mNegativeButtonTextColorList);
		}
		else if (mNegativeButtonTextColor != -1)
		{
			setNegativeButtonTextColor(mNegativeButtonTextColor);
		}

		if (mNeutralButtonTextColorList != null)
		{
			setNeutralButtonTextColor(mNeutralButtonTextColorList);
		}
		else if (mNeutralButtonTextColor != -1)
		{
			setNegativeButtonTextColor(mNeutralButtonTextColor);
		}
	}
}
