package net.teamplace.android.browsing.share;

import static net.teamplace.android.utils.GenericUtils.tag;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.request.share.ShareRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.TeamDriveUtils.InvalidEmailException;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.text.format.Formatter;
import android.text.util.Rfc822Tokenizer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.cortado.android.R;

import de.greenrobot.event.EventBus;

public class ShareAttachmentFragment extends Fragment
{
	private final String TAG = tag(this.getClass());

	public static final String ARG_SHARE_FILE = "argShareFile";
	public static final String ARG_FORBIDDEN = "argForbidden";
	public static final String ARG_FORBIDDEN_FILE_TYPE = "argForbiddenFileType";

	private FileInfo shareFileInfo;
	private boolean forbiddenFile;
	private boolean forbiddenFileType;

	private RecipientEditTextView shareTo;
	private EditText subject;
	private EditText mailText;

	public static Fragment newInstance(FileInfo shareFile, boolean forbidden, boolean forbiddenFileType)
	{
		ShareAttachmentFragment fragment = new ShareAttachmentFragment();
		Bundle arguments = new Bundle();

		arguments.putParcelable(ARG_SHARE_FILE, shareFile);
		arguments.putBoolean(ARG_FORBIDDEN, forbidden);
		arguments.putBoolean(ARG_FORBIDDEN_FILE_TYPE, forbiddenFileType);

		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		shareFileInfo = getArguments().getParcelable(ARG_SHARE_FILE);
		forbiddenFile = getArguments().getBoolean(ARG_FORBIDDEN, false);
		forbiddenFileType = getArguments().getBoolean(ARG_FORBIDDEN_FILE_TYPE, false);

		String date = DateFormat.getDateFormat(getActivity()).format(shareFileInfo.getLastModifiedDate());
		String size = Formatter.formatShortFileSize(getActivity(), shareFileInfo.getSize());

		View root = inflater.inflate(R.layout.shr_attachment, container, false);

		((TextView) root.findViewById(R.id.tv_share_description)).setText(R.string.shr_description_attachment);

		shareTo = (RecipientEditTextView) root.findViewById(R.id.et_share_to);

		shareTo.setTokenizer(new Rfc822Tokenizer());
		shareTo.setAdapter(new BaseRecipientAdapter(getActivity())
		{
		});

		subject = (EditText) root.findViewById(R.id.et_share_subject);
		mailText = (EditText) root.findViewById(R.id.et_share_write_text);

		TextView tvFileText = (TextView) root.findViewById(R.id.tv_share_file_name);
		tvFileText.setText(shareFileInfo.getName());

		TextView tvFileDate = (TextView) root.findViewById(R.id.tv_share_file_date);
		tvFileDate.setText(date);

		TextView tvFileSize = (TextView) root.findViewById(R.id.tv_share_file_size);
		tvFileSize.setText(size);

		ImageView tvIcon = (ImageView) root.findViewById(R.id.iv_share_file_icon);
		tvIcon.setImageResource(shareFileInfo.getIconResId());

		if (forbiddenFile)
		{
			shareTo.setEnabled(false);
			shareTo.setHintTextColor(0xffb2b2b2);

			subject.setEnabled(false);
			subject.setHintTextColor(0xffb2b2b2);

			mailText.setEnabled(false);
			mailText.setHintTextColor(0xffb2b2b2);

			TextView shareForbidden = (TextView) root.findViewById(R.id.tv_share_forbidden);
			shareForbidden.setText(R.string.shr_forbidden_size);
			shareForbidden.setVisibility(View.VISIBLE);

			if (forbiddenFileType)
			{
				shareForbidden.setText(R.string.shr_forbidden_extension);
			}
		}

		return root;
	}

	public void share()
	{
		if (TextUtils.isEmpty(shareTo.getText()))
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.shr_empty_recipients));
		}
		else
		{
			((ShareActivity) getActivity()).enableActionbarProgress();

			new AsyncTask<Void, Void, Void>()
			{
				private String message;
				private String error;
				private Context context;

				@Override
				protected Void doInBackground(Void... params)
				{
					context = getActivity().getApplicationContext();

					String remoteFilePath = PathUtils.remoteRequestString(shareFileInfo.getPath());

					try
					{
						Session session = new SessionManager(context).getLastSession();
						String teamDriveId = PathUtils.getTeamDriveIdOrNull(shareFileInfo.getPath());
						String recipients = ((ShareActivity) getActivity()).getEmailAdressesFromChipsText(shareTo
								.getText());

						new ShareRequester(context, session, new TeamplaceBackend()).shareAsMailAttachment(teamDriveId, remoteFilePath,
								recipients, subject.getText().toString(), mailText.getText().toString());

						message = shareFileInfo.getName();

					}
					catch (InvalidEmailException iee)
					{
						error = context.getString(R.string.shr_invalid_email_address);
					}
					catch (Exception e)
					{
						error = String.format(context.getString(R.string.shr_sharing_failed), shareFileInfo.getPath()
								.getName());
						ExceptionManager em = ExceptionManager.getInstance();
						em.handleException(e);
					}
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					if (message != null)
					{
						FeedbackToast.show(context, true, message);
					}
					if (error != null)
					{
						FeedbackToast.show(context, false, error);
					}

					EventBus.getDefault().postSticky(new ShareActivity.ShareActionCompleteEvent());
				};
			}.execute();
		}
	}
}
