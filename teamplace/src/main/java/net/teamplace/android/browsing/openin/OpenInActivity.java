package net.teamplace.android.browsing.openin;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;

import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.LoggingActivity;

public class OpenInActivity extends LoggingActivity
{
	public static String OPEN_IN_EXTRA = "OpenInExtra";

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		SessionManager manager = new SessionManager(this);

		if (manager.getLastSession() == null)
		{
			Intent initIntent = new Intent(this, InitializationActivity.class);
			initIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			initIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			initIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			initIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

			startActivity(initIntent);
			finish();
			return;
		}

		Intent intent = getIntent();
		Uri fileURI = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
		if (fileURI == null)
			 fileURI = intent.getData();

		String strFilePath = getPathfromUri(fileURI);
		Log.i("OpenIn", "OpenIn File: " + strFilePath);

		intent = new Intent(this, BrowsingActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(OPEN_IN_EXTRA, strFilePath);

		startActivity(intent);
		finish();
	}

	private String getPathfromUri(Uri uri)
	{
		if (uri.toString().startsWith("file://"))
		{
			return uri.getPath();
		}

		String[] projection = {MediaStore.Images.Media.DATA};
		Cursor cursor = managedQuery(uri, projection, null, null, null);
		startManagingCursor(cursor);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		String path = cursor.getString(column_index);
		// cursor.close();

		return path;
	}
}
