package net.teamplace.android.browsing.dialog;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cortado.android.R;

/*package*/class FolderListAdapter extends ArrayAdapter<FileInfo> implements View.OnClickListener
{
	private final OnFolderClickedListener mListener;

	public FolderListAdapter(Context context, List<FileInfo> files)
	{
		this(context, files, null);
	}

	/** Filters out non-folder files from dataset */
	public FolderListAdapter(Context context, List<FileInfo> files, OnFolderClickedListener listener)
	{
		super(context, 0, filter(files));
		mListener = listener;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if (v == null)
		{
			LayoutInflater li = LayoutInflater.from(getContext());
			v = li.inflate(R.layout.flp_browsing_item, parent, false);
			v.setOnClickListener(this);
		}

		v.setTag(position);
		FileInfo file = getItem(position);
		TextView name = (TextView) v.findViewById(R.id.name);
		TextView info = (TextView) v.findViewById(R.id.info);
		name.setText(file.getName());
		info.setText(PathUtils.shortDisplayablePath(file.getPath().getParentPath()));
		return v;
	}

	@Override
	public void onClick(View v)
	{
		if (mListener == null)
			return;

		Object tag = v.getTag();
		if (!(tag instanceof Integer))
			return;

		int position = (Integer) tag;
		Path folder = getItem(position).getPath();
		mListener.onFolderClicked(folder);
	}

	/** Sieves a file and folder list and returns a list with only folders */
	static List<FileInfo> filter(List<FileInfo> files)
	{
		List<FileInfo> result = new ArrayList<FileInfo>();
		for (FileInfo file : files)
		{
			if (file.isFolder())
			{
				result.add(file);
			}
		}
		return result;
	}

	interface OnFolderClickedListener
	{
		void onFolderClicked(Path folder);
	}
}