package net.teamplace.android.browsing.dialog;

import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;

/**
 * Utility class that listens only for back presses and delegates the rest to the system.
 * 
 * @author Gil Vegliach
 */
public abstract class OnBackPressedListener implements OnKeyListener
{
	@Override
	public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event)
	{
		if (keyCode != KeyEvent.KEYCODE_BACK || event.getAction() != KeyEvent.ACTION_UP)
			return false;

		return onBackPressed();
	}

	/** Returns true if back press was handled, otherwise false */
	abstract protected boolean onBackPressed();
}
