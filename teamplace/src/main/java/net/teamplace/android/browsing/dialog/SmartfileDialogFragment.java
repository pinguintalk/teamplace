package net.teamplace.android.browsing.dialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.ViewAnimator;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.FolderListAdapter.OnFolderClickedListener;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.teamdrive.ui.TeamDriveComparator;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.RightsHelper;
import net.teamplace.android.utils.TeamDriveUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse.NO_REQUEST_CODE;
import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Dialog fragment to perform file operations and smart filing.
 * 
 * @author Gil Vegliach
 */
public class SmartfileDialogFragment extends DialogFragment implements FolderListAdapter.OnFolderClickedListener
{
	public static final String TAG = tag(SmartfileDialogFragment.class);
	private static final String PKG = pkg(SmartfileDialogFragment.class);

	public static final int NO_ACTION = 0;
	public static final int ACTION_MOVE = 1;
	public static final int ACTION_COPY = 2;

	public static final int REQUEST_CODE = R.id.request_code_smartfile;
	public static final String KEY_TARGET = PKG + "." + TAG + ".target";
	public static final String KEY_ACTION = PKG + "." + TAG + ".action";

	// Types of dialog, up/download, smart filing, etc.
	private static final int TYPE_UNKNOWN_UPLOAD = 0;
	private static final int TYPE_QUICK_UPLOAD = 1;
	private static final int TYPE_SMART_UPLOAD = 2;
	private static final int TYPE_QUICK_DOWNLOAD = 3;

	// State that this dialog can be in. Used also as view indices
	private static final int STATE_QUICK_TARGET = 0;
	private static final int STATE_SELECT_LOCATION = 1;
	private static final int STATE_BROWSING = 2;

	private static final String KEY_TYPE = PKG + "." + TAG + ".type";
	private static final String KEY_STATE = PKG + ".state";
	private static final String KEY_PATHS = PKG + "." + TAG + ".paths";
	private static final String KEY_CURR_FOLDER = PKG + ".currFolder";

	// TODO: what is the correct way to retrieve the upload folder?
	private static final Path QUICK_UPLOAD_PATH = Path.valueOf(Path.remoteRoot(), "upload");
	private static final Path QUICK_DOWNLOAD_PATH;
	static
	{
		// File f = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		File quickDir = GenericUtils.getExternalStorageAppDir();
		// Ensure that download folder exists
		quickDir.mkdirs();
		QUICK_DOWNLOAD_PATH = GenericUtils.getExternalStorageAppPath();
	}

	// Conventions used by all ViewSwitcher in this dialog's ui. The first view is a progress bar, the second is the
	// content
	private static final int VIEW_SWITCHER_PB = 0;
	private static final int VIEW_SWITCHER_CONTENT = 1;

	/** Current folder. Non-null if mState == STATE_BROWSING && we are not in the select teamdrive pane */
	private Path mCurrFolder;

	private int mType;
	private ArrayList<Path> mPaths;
	private int mState = STATE_QUICK_TARGET;

	/** Browse request id that is in progress or NO_ID */
	private int mRequestId = Contract.NO_ID;

	/** Quick download, quick upload or smartfile upload target */
	private Path mQuickTarget;

	private ViewAnimator mViewAnimator;
	private ViewSwitcher mQtPane;
	private View mQtButton;

	private View mBtnPanel;
	private Button mBtnCancel;
	private Button mBtnMove;
	private Button mBtnCopy;

	private RightsHelper mRightsHelper;
	private boolean mCanDeleteSrc = false;

	public static SmartfileDialogFragment newInstance(ArrayList<Path> paths)
	{
		checkCondition(paths != null && paths.size() > 0, "paths must contain at least one path");
		int type = typeFromPaths(paths);
		Bundle args = new Bundle();
		args.putInt(KEY_TYPE, type);
		args.putParcelableArrayList(KEY_PATHS, paths);
		SmartfileDialogFragment frag = new SmartfileDialogFragment();
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		checkNonNullArg(args, "Construct this fragment with newInstace(ArrayList<Path>)");

		mType = args.getInt(KEY_TYPE, -1);
		checkCondition(mType != -1, "type must be one of the TYPE_* constants");

		mPaths = args.getParcelableArrayList(KEY_PATHS);
		checkNonNullArg(mPaths, "paths must be non-null");

		// TODO: refactor this line
		mCanDeleteSrc = mRightsHelper.canDelete(new ArrayList<FileInfo>(Arrays.asList(new FileInfo(mPaths.get(0), 0, 0,
				false))));
	}

	// Dialog does not support parent params
	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Context ctx = getActivity();
		View root = LayoutInflater.from(ctx).inflate(R.layout.flp_dialog, null);
		bindViews(root);
		setUpButtons();
		setUpQuickTargetPane();
		setUpSelectLocationPane();
		return createDialog(root);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null)
		{
			restoreInstanceState(savedInstanceState);
		}

		if (mType == TYPE_UNKNOWN_UPLOAD)
		{
			checkUploadLocations();
		}
		else
		{
			// type is known and content is ready, show it
			mQtPane.setDisplayedChild(VIEW_SWITCHER_CONTENT);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_STATE, mState);
		outState.putInt(KEY_TYPE, mType);
		outState.putParcelable(KEY_TARGET, mQuickTarget);
		if (mState == STATE_BROWSING)
		{
			outState.putParcelable(KEY_CURR_FOLDER, mCurrFolder);
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		mRightsHelper = new RightsHelper(new SessionManager(activity).getLastSession());
		IntentFilter filter = new IntentFilter(Contract.ACTION_RESPONSE_BROWSE);
		filter.addAction(Contract.ACTION_RESPONSE_MKDIR);
		filter.addAction(Contract.ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS);
		LocalBroadcastManager.getInstance(activity).registerReceiver(mReceiver, filter);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode != R.id.request_code_new_folder || mState != STATE_BROWSING)
		{
			return;
		}

		// Mkdir request
		ViewSwitcher vs = (ViewSwitcher) mViewAnimator.getCurrentView().findViewById(R.id.content_view_switcher);
		vs.setDisplayedChild(VIEW_SWITCHER_PB);

		Context ctx = getActivity();
		CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
		Path folder = Path.valueOf(mCurrFolder, cs.toString());
		Intent i = Contract.buildMkdirRequestIntent(ctx, folder);
		mRequestId = Contract.extractRequestId(i);
		ctx.startService(i);
	}

	private static int typeFromPaths(ArrayList<Path> paths)
	{
		int type;
		if (paths.size() == 1)
		{
			type = (paths.get(0) instanceof Path.Local) ? TYPE_UNKNOWN_UPLOAD : TYPE_QUICK_DOWNLOAD;
		}
		else
		{
			PathUtils.checkSameLocationOrThrow(paths.toArray(new Path[0]));
			type = (paths.get(0) instanceof Path.Local) ? TYPE_QUICK_UPLOAD : TYPE_QUICK_DOWNLOAD;
		}
		return type;
	}

	private void bindViews(View root)
	{
		mViewAnimator = (ViewAnimator) root.findViewById(R.id.view_animator);
		mBtnPanel = root.findViewById(R.id.button_panel);
		mQtPane = (ViewSwitcher) mViewAnimator.findViewById(R.id.quick_target_pane);
		mQtButton = mQtPane.findViewById(R.id.quick_target);
	}

	private StyleableAlertDialog createDialog(View rootView)
	{
		Context ctx = getActivity();
		StyleableAlertDialog dlg = new StyleableAlertDialog(ctx, 0);
		dlg.setTitle(R.string.flp_dlg_title);
		dlg.setView(rootView);
		dlg.setOnKeyListener(mOnBackPressedListener);
		return dlg;
	}

	private void restoreInstanceState(Bundle instanceState)
	{
		int state = instanceState.getInt(KEY_STATE, STATE_SELECT_LOCATION);
		if (state == STATE_BROWSING)
		{
			mCurrFolder = instanceState.getParcelable(KEY_CURR_FOLDER);
			// Will set mState = STATE_BROWSING
			if (mCurrFolder == null)
			{
				browseTeamdrives();
			}
			else
			{
				browseInto(mCurrFolder);
			}
		}
		else
		{
			mState = state;
			syncButtonsFromState();
			mViewAnimator.setDisplayedChild(state);
		}

		mType = instanceState.getInt(KEY_TYPE);
		mQuickTarget = instanceState.getParcelable(KEY_TARGET);
		setUpQtButtonFromTypeAndTarget();
	}

	private void browseTeamdrives()
	{
		Context ctx = getActivity();
		View pane = LayoutInflater.from(ctx).inflate(R.layout.flp_teamdrives_pane, mViewAnimator, false);

		updateBrowsingUiAndState(null, pane);

		// Start the TeamDriveService and get all the team drives
		mRequestId = IdGenerator.getId();
		// TODO: theoretically speaking, we want the same behavior as in the browsing, so we should skip the cached
		// response and force the refresh
		TeamDriveService.getAllTeamDrivesSkipCacheQuery(ctx, mRequestId);
	}

	private void browseInto(Path path)
	{
		Context ctx = getActivity();
		View pane = LayoutInflater.from(ctx).inflate(R.layout.flp_browsing_pane, mViewAnimator, false);
		TextView label = (TextView) pane.findViewById(R.id.label);
		label.setText(R.string.flp_dlg_browsing_copy_or_move);
		TextView currentFolder = (TextView) pane.findViewById(R.id.current_folder);
		currentFolder.setText(PathUtils.shortDisplayablePath(path));
		ImageView newFolder = (ImageView) pane.findViewById(R.id.new_folder);
		newFolder.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				CharSequence title = getString(R.string.brw_action_new_folder);
				CharSequence text = getString(R.string.brw_new_folder_default_name);
				DialogFragment dlg = EditTextDialogFragment.newInstance(title, text, true);
				dlg.setTargetFragment(SmartfileDialogFragment.this, R.id.request_code_new_folder);
				dlg.show(getFragmentManager(), EditTextDialogFragment.TAG);
			}
		});

		updateBrowsingUiAndState(path, pane);

		Intent i = Contract.buildBrowseRequestIntent(ctx, path);
		mRequestId = Contract.extractRequestId(i);
		ctx.startService(i);
	}

	private void updateBrowsingUiAndState(Path path, View pane)
	{
		int oldState = mState;
		mState = STATE_BROWSING;
		mCurrFolder = path;

		int index = mViewAnimator.getDisplayedChild();
		mViewAnimator.addView(pane);
		syncButtonsFromState();

		if (oldState == STATE_BROWSING)
		{
			mViewAnimator.showNext();
			mViewAnimator.removeViewAt(index);
		}
		else
		{
			mViewAnimator.setDisplayedChild(STATE_BROWSING);
		}
	}

	private void syncButtonsFromState()
	{
		switch (mState)
		{
			case STATE_QUICK_TARGET:
			{
				mBtnPanel.setVisibility(View.GONE);
				break;
			}
			case STATE_SELECT_LOCATION:
			{
				mBtnPanel.setVisibility(View.VISIBLE);
				mBtnCancel.setEnabled(true);
				mBtnMove.setEnabled(false);
				mBtnCopy.setEnabled(false);
				break;
			}
			case STATE_BROWSING:
			{
				// If we are selecting a teamdrive, disable all the buttons but cancel
				if (mCurrFolder == null)
				{
					mBtnPanel.setVisibility(View.VISIBLE);
					mBtnCancel.setEnabled(true);
					mBtnMove.setEnabled(false);
					mBtnCopy.setEnabled(false);
				}
				else
				{
					mBtnPanel.setVisibility(View.VISIBLE);
					mBtnCancel.setEnabled(true);
					mBtnMove.setEnabled(mCanDeleteSrc);
					mBtnCopy.setEnabled(true);
				}
				break;
			}
		}
	}

	private void setUpButtons()
	{
		mBtnCancel = (Button) mBtnPanel.findViewById(R.id.button_cancel);
		mBtnMove = (Button) mBtnPanel.findViewById(R.id.button_move);
		mBtnCopy = (Button) mBtnPanel.findViewById(R.id.button_copy);

		mBtnCancel.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dismiss();
			}
		});
		mBtnMove.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mState != STATE_BROWSING) {
					throw new IllegalStateException("Move button should be disabled in state: " + mState);
				}

				dismiss();

				Fragment frag = getTargetFragment();
				if (frag != null) {
					Intent i = new Intent();
					i.putExtra(KEY_TARGET, mCurrFolder);
					i.putExtra(KEY_ACTION, ACTION_MOVE);
					frag.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, i);
				}
			}
		});
		mBtnCopy.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mState != STATE_BROWSING) {
					throw new IllegalStateException("Copy button should be disabled in state: " + mState);
				}

				dismiss();

				Fragment frag = getTargetFragment();
				if (frag != null) {
					Intent i = new Intent();

					i.putExtra(KEY_TARGET, mCurrFolder);
					i.putExtra(KEY_ACTION, ACTION_COPY);
					frag.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, i);
				}
			}
		});
	}

	private void setUpSelectLocationPane()
	{
		View pane = mViewAnimator.findViewById(R.id.select_location_pane);
		View remote = pane.findViewById(R.id.remote);
		remote.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				if(GenericUtils.isConnectedToInternet(getActivity()))
					browseInto(Path.remoteRoot());
				else
					FeedbackToast.show(getActivity(), false,
							getResources().getString(R.string.tmd_err_no_internet_connection));
			}
		});

		View teamdrives = pane.findViewById(R.id.teamdrives);
		teamdrives.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(GenericUtils.isConnectedToInternet(getActivity()))
					browseTeamdrives();
				else
					FeedbackToast.show(getActivity(), false,
							getResources().getString(R.string.tmd_err_no_internet_connection));

			}
		});

		View local = pane.findViewById(R.id.local);
		local.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				browseInto(Path.localRoot());
			}
		});

		remote.setVisibility(mRightsHelper.isWorkplaceTargetAllowed(mPaths) ? View.VISIBLE : View.GONE);
		teamdrives.setVisibility(mRightsHelper.isTeamDriveTargetAllowed(mPaths) ? View.VISIBLE : View.GONE);
		local.setVisibility(mRightsHelper.isLocalTargetAllowed(mPaths) ? View.VISIBLE : View.GONE);
	}

	private void setUpQuickTargetPane()
	{
		if (mType == TYPE_QUICK_UPLOAD)
		{
			mQuickTarget = QUICK_UPLOAD_PATH;
		}
		else if (mType == TYPE_QUICK_DOWNLOAD)
		{
			mQuickTarget = QUICK_DOWNLOAD_PATH;
		}
		setUpQtButtonFromTypeAndTarget();

		mQtButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				dismiss();

				Fragment frag = getTargetFragment();
				if (frag != null)
				{
					Intent i = new Intent();
					// Quick targets are copy actions
					i.putExtra(KEY_ACTION, ACTION_COPY);
					if (mQuickTarget != null)
					{
						i.putExtra(KEY_TARGET, mQuickTarget);
						frag.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, i);
					}
					else
					{
						frag.onActivityResult(REQUEST_CODE, Activity.RESULT_CANCELED, i);
					}

				}

			}
		});

		View browse = mQtPane.findViewById(R.id.browse);
		browse.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mState = STATE_SELECT_LOCATION;
				syncButtonsFromState();
				mViewAnimator.showNext();
			}
		});
	}

	private void checkUploadLocations()
	{
		// Smart file only if we have only one path in mPaths
		Context ctx = getActivity();
		Intent i = Contract.buildCheckUploadLocationsRequestIntent(ctx, mPaths.get(0));
		mRequestId = Contract.extractRequestId(i);
		ctx.startService(i);

		// Progress bar ui of quick target pane
		mQtPane.setDisplayedChild(VIEW_SWITCHER_PB);
	}

	// Call this after mType or mQuickTarget change
	private void setUpQtButtonFromTypeAndTarget()
	{
		TextView title = (TextView) mQtButton.findViewById(R.id.quick_target_title);
		TextView subtitle = (TextView) mQtButton.findViewById(R.id.quick_target_subtitle);
		ImageView icon = (ImageView) mQtButton.findViewById(R.id.quick_target_icon);

		boolean enableQtButton = false;

		switch (mType)
		{
			case TYPE_SMART_UPLOAD:
			{
				String to = getActivity().getString(R.string.flp_dlg_select_target_to);
				String targetStr = to + PathUtils.shortDisplayablePath(mQuickTarget);
				title.setText(R.string.flp_dlg_save_back);
				subtitle.setText(targetStr);
				icon.setImageResource(R.drawable.flp_upload);
				enableQtButton = true;
				break;
			}
			case TYPE_QUICK_UPLOAD:
			case TYPE_UNKNOWN_UPLOAD:
			{
				title.setText(R.string.flp_dlg_quick_upload);
				subtitle.setText(R.string.flp_dlg_quick_upload_folder);
				icon.setImageResource(R.drawable.flp_upload);
				enableQtButton = mRightsHelper.isWorkplaceTargetAllowed(mPaths);
				break;
			}
			case TYPE_QUICK_DOWNLOAD:
			{
				title.setText(R.string.flp_dlg_quick_download);
				subtitle.setText(R.string.flp_dlg_quick_download_folder);
				icon.setImageResource(R.drawable.flp_download);
				enableQtButton = mRightsHelper.isLocalTargetAllowed(mPaths);
				break;
			}
		}
		if (!enableQtButton)
		{
			mQtButton.setVisibility(View.GONE);
		}
	}

	@Override
	public void onFolderClicked(Path folder)
	{
		if (mState != STATE_BROWSING)
		{
			throw new IllegalStateException("Tried to browse from a state != STATE_BROWSING");
		}

		browseInto(folder);
	}

	// Assumes Contract.NO_ID == IdGenerator.NO_ID
	public void onEventMainThread(TeamDriveServiceResponse response)
	{
		// If this is a response from a request we haven't done, just ignore
		long id = response.getRequestCode();
		if (id == NO_REQUEST_CODE || id == Contract.NO_ID || id != mRequestId)
		{
			return;
		}

		mRequestId = Contract.NO_ID;

		Object data;
		try
		{
			data = response.getResponseData();
			if (!(data instanceof TeamDrive[]))
			{
				return;
			}

			// The top View and the ListView are the same in the current implementation
			View pane = mViewAnimator.getCurrentView();
			ListView teamDrivesList = (ListView) pane.findViewById(R.id.teamdrives_list);

			TeamDrive[] teamDrives = (TeamDrive[]) data;
			teamDrivesList.setAdapter(new TeamDrivesAdapter(getActivity(), mRightsHelper.filterTargetDrives(teamDrives,
					mPaths), this));

			ViewSwitcher vs = (ViewSwitcher) pane.findViewById(R.id.view_switcher);
			vs.setDisplayedChild(VIEW_SWITCHER_CONTENT);
		}
		catch (Exception e)
		{
			if (e instanceof RuntimeException)
			{
				throw (RuntimeException) e;
			}
		}
	}

	private final OnBackPressedListener mOnBackPressedListener = new OnBackPressedListener()
	{
		@Override
		protected boolean onBackPressed()
		{
			if (mState == STATE_QUICK_TARGET)
			{
				return false;
			}

			if (mState == STATE_SELECT_LOCATION)
			{
				mViewAnimator.showPrevious();
				mState = STATE_QUICK_TARGET;
				syncButtonsFromState();
				return true;
			}

			// Post-condition: mState == STATE_BROWSING
			if (mCurrFolder instanceof Path.TeamDrive && mCurrFolder.isRoot())
			{
				browseTeamdrives();
				return true;
			}

			// If we are at the select teamdrive pane or at a non-teamdrive root, go back to select location pane
			if (mCurrFolder == null || mCurrFolder.isRoot())
			{
				mViewAnimator.showPrevious();
				mViewAnimator.removeViewAt(STATE_BROWSING);
				mState = STATE_SELECT_LOCATION;
				mCurrFolder = null;
				syncButtonsFromState();
				return true;
			}

			// Otherwise navigate back
			browseInto(mCurrFolder.getParentPath());
			return true;
		}
	};

	private final BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			int id = Contract.extractRequestId(intent);
			if (id == Contract.NO_ID || id != mRequestId)
			{
				return;
			}

			String action = intent.getAction();
			if (Contract.ACTION_RESPONSE_BROWSE.equals(action) && mState == STATE_BROWSING)
			{
				mRequestId = Contract.NO_ID;

				List<FileInfo> result = intent.getParcelableArrayListExtra(Contract.RESPONSE_BROWSE_FOLDER_CONTENT);
				if (result == null)
				{
					return;
				}

				View pane = mViewAnimator.getCurrentView();
				ListView folderList = (ListView) pane.findViewById(R.id.folder_list);
				if (folderList == null)
				{
					return;
				}

				// Use Activity context to apply the correct theme
				folderList.setAdapter(new FolderListAdapter(getActivity(), result, SmartfileDialogFragment.this));
				ViewSwitcher vs = (ViewSwitcher) pane.findViewById(R.id.content_view_switcher);
				vs.setDisplayedChild(VIEW_SWITCHER_CONTENT);
			}
			else if (Contract.ACTION_RESPONSE_MKDIR.equals(action) && mState == STATE_BROWSING)
			{
				// Mkdir done, refresh ui with a browse request
				Intent i = Contract.buildBrowseRequestIntent(context, mCurrFolder);
				mRequestId = Contract.extractRequestId(i);
				context.startService(i);
			}
			else if (Contract.ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS.equals(action) && mState == STATE_QUICK_TARGET
					&& mType == TYPE_UNKNOWN_UPLOAD)
			{
				mRequestId = Contract.NO_ID;
				mQtPane.setDisplayedChild(VIEW_SWITCHER_CONTENT);

				// If we have some smartfile targets, use the first one for the quick target button listener. Otherwise
				// fall back on the default quick upload target
				ArrayList<Path> result = intent
						.getParcelableArrayListExtra(Contract.RESPONSE_CHECK_UPLOAD_LOCATIONS_TARGETS);
				if (result != null && result.size() > 0)
				{
					mType = TYPE_SMART_UPLOAD;
					mQuickTarget = result.get(0);
					setUpQtButtonFromTypeAndTarget();
				}
				else
				{
					mType = TYPE_QUICK_UPLOAD;
				}
			}
		}
	};
}

class TeamDrivesAdapter extends ArrayAdapter<TeamDrive> implements View.OnClickListener
{
	private static final Comparator<Object> COMPARATOR = new TeamDriveComparator();
	private final OnFolderClickedListener mListener;

	public TeamDrivesAdapter(Context context, TeamDrive[] objects, OnFolderClickedListener listener)
	{
		super(context, 0, objects);
		mListener = listener;
		sort(COMPARATOR);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if (v == null)
		{
			LayoutInflater li = LayoutInflater.from(getContext());
			v = li.inflate(R.layout.flp_teamdrives_item, parent, false);
			v.setOnClickListener(this);
		}

		v.setTag(position);
		TeamDrive drive = getItem(position);
		int users = drive.getUsers().length;
		TextView name = (TextView) v.findViewById(R.id.name);
		TextView members = (TextView) v.findViewById(R.id.members);
		name.setText(drive.getName());
		members.setText(TeamDriveUtils.formatMemberString(getContext(), users));
		return v;
	}

	@Override
	public void onClick(View v)
	{
		if (mListener == null)
		{
			return;
		}

		Object tag = v.getTag();
		if (!(tag instanceof Integer))
		{
			return;
		}

		int position = (Integer) tag;
		TeamDrive drive = getItem(position);
		mListener.onFolderClicked(Path.teamDriveRoot(drive.getId()));
	}
}