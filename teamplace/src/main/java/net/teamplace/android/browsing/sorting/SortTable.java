package net.teamplace.android.browsing.sorting;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.utils.Log;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Pair;


/**
 * Represents a table in the database for remembering the sorting preferences of each folder.
 * 
 * @author Gil Vegliach
 */
public class SortTable
{
	public static final String TAG = tag(SortTable.class);

	public static final String TABLE = "sort";

	public static final String PATH = "path";
	public static final String CATEGORY = "sort_category";
	// If we chose "order" then we'd have to surround it always with []
	public static final String ORDER = "sort_order";

	// without rowid prevents the database from storing an additional rowid column
	private static final String TABLE_CREATE =
			"create table " + TABLE + "("
					+ PATH + " text primary key on conflict replace, "
					+ CATEGORY + " integer not null, "
					+ ORDER + " integer not null "
					+ ");";

	public static void create(SQLiteDatabase database)
	{
		checkNonNullArg(database);
		database.execSQL(TABLE_CREATE);
	}

	public static void upgrade(SQLiteDatabase database, int oldVersion, int newVersion)
	{
		checkNonNullArg(database);
		purge(database);
	}

	/** Removes all sorting preferences from the database */
	public static void purge(SQLiteDatabase database)
	{
		checkNonNullArg(database);
		Log.i(TAG, "Purging " + TABLE + " from " + database);
		try
		{
			database.delete(TABLE, null, null);
		}
		catch (SQLiteException e)
		{
			Log.e(TAG, "SQLiteException during purging suppressed. The table may not be created yet.", e);
		}
	}

	/**
	 * Returns category and order constants for the given path, or default category and order if the
	 * path is not in the database.
	 */
	public static Pair<Integer, Integer> getSort(SQLiteDatabase database, Path path)
	{
		checkNonNullArg(database);
		checkNonNullArg(path);

		int category = SortManager.DEFAULT_CATEGORY;
		int order = SortManager.DEFAULT_ORDER;

		String[] projection = new String[] {CATEGORY, ORDER};
		String selection = PATH + " = ?";
		String[] selectionArgs = new String[] {PathUtils.databaseString(path)};
		Cursor cursor = database.query(TABLE, projection, selection, selectionArgs, null, null, null);
		if (cursor.moveToFirst())
		{
			int index = cursor.getColumnIndexOrThrow(CATEGORY);
			category = cursor.getInt(index);
			index = cursor.getColumnIndexOrThrow(ORDER);
			order = cursor.getInt(index);
		}
		cursor.close();
		return new Pair<Integer, Integer>(category, order);
	}

	/**
	 * Stores category and order information for the given path in the database, eventually overwriting the
	 * previous entry.
	 */
	public static void setSort(SQLiteDatabase database, Path path, int category, int order)
	{
		checkNonNullArg(database);
		checkNonNullArg(path);
		SortManager.checkCategory(category);
		SortManager.checkOrder(order);

		ContentValues vals = new ContentValues();
		vals.put(PATH, PathUtils.databaseString(path));
		vals.put(CATEGORY, category);
		vals.put(ORDER, order);
		database.insert(TABLE, null, vals);
	}
}
