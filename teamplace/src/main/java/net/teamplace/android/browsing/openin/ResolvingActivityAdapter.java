package net.teamplace.android.browsing.openin;

import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;

public class ResolvingActivityAdapter extends ArrayAdapter<ResolveInfoForIntent>
{

	public ResolvingActivityAdapter(Context context, int resource, List<ResolveInfoForIntent> objects)
	{
		super(context, resource, objects);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		if (convertView == null)
		{
			LayoutInflater inflater = LayoutInflater.from(getContext());
			convertView = inflater.inflate(R.layout.brw_resolving_activity_item, parent, false);
		}

		ImageView activityIcon = (ImageView) convertView.findViewById(R.id.iv_activity_icon);
		activityIcon.setImageDrawable(getActivityIcon(position));

		TextView activityName = (TextView) convertView.findViewById(R.id.tv_activity_name);
		activityName.setText(getActivityLabel(position));

		return convertView;
	}

	private String getActivityLabel(int position)
	{
		return getItem(position).getResolveInfo().activityInfo.loadLabel(getContext().getPackageManager()).toString();
	}

	private Drawable getActivityIcon(int position)
	{
		return getItem(position).getResolveInfo().activityInfo.applicationInfo.loadIcon(getContext()
				.getPackageManager());
	}
}
