package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.filtering.FilterController;
import net.teamplace.android.browsing.filtering.FilterableAdapter;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.sorting.SortManager;
import net.teamplace.android.browsing.widget.CheckableRelativeLayout.OnAttachStateChangeListener;
import net.teamplace.android.browsing.widget.FlipLayout.OnFlipListener;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.request.thumbnail.ThumbnailRequester;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.ThumbnailHelper;

import java.util.ArrayList;
import java.util.List;

import static net.teamplace.android.utils.GenericUtils.tag;


/**
 * ArrayAdapter supporting multiselect
 * 
 * @author Gil Vegliach
 */
public class MultiSelectAdapter extends BaseAdapter implements FilterableAdapter
{
	private static final String TAG = tag(MultiSelectAdapter.class);

	private final Context mContext;

	// Package-private is slightly faster than private when accessed by AnimationListener. This
	// is because the compiler unravel inner classes into stand-alone classes that thus cannot
	// access the private members anymore. So the compiler needs to generate package-private getters
	// that slow down the access. Setting the visibility to package-private, the compiler does not
	// need to generate the getters
	/* package */SparseBooleanArray mChecked;

	private OnIconClickListener mIconClickListener;
	private final AnimationListener mAnimationListener = new AnimationListener();
	private final boolean mHasFavorites;
	private MultiSelectAdapter.OnFavoriteCheckedChangeListener mFavListener;
	private CheckTeamDriveCallback checkTeamDriveCallback;
	private ThumbnailRequester mRequester;
	private DisplayImageOptions mImageOptions;
	/**
	 * Filter string, non-null iff filtering is enabled. When filtering is enabled all operations related to checked
	 * elements, except {@link #hasCheckedItems()} , will throw a runtime exception
	 */
	// private String mFilter;
	private final FilterController<FileInfo> mFilterController;

	// This reference is bound by MultiSelectListView.setAdapter()
	private MultiSelectListView mListView;

	// TODO: check this
	final List<FileInfo> mObjects;

	public MultiSelectAdapter(Context context, List<FileInfo> objects, boolean hasFavorites)
	{
		mContext = context;
		mObjects = objects;
		mChecked = new SparseBooleanArray(objects.size());
		mHasFavorites = hasFavorites;
		mFilterController = new FilterController<FileInfo>(objects,
				new FilterController.ItemConverter<FileInfo>()
				{
					@Override
					public String getFilterableString(FileInfo item, int position)
					{
						return item.getName();
					}
				});

		mRequester = new ThumbnailRequester(context, new SessionManager(context).getLastSession(), new TeamplaceBackend());

		if (objects.size() > 0 && Path.isLocalLocation(objects.get(0).getPath().getLocation())) {
			mImageOptions = new DisplayImageOptions.Builder().extraForDownloader(new ThumbnailHelper()).build();
		} else
			mImageOptions = new DisplayImageOptions.Builder().extraForDownloader(new ThumbnailHelper()).cacheOnDisk(true)
					.cacheInMemory(true).build();

	}

	public MultiSelectAdapter(Context context, List<FileInfo> objects, boolean hasFavorites,
			OnIconClickListener clickListener, OnFavoriteCheckedChangeListener favListener,
			CheckTeamDriveCallback checkTeamDriveCallback)
	{
		this(context, objects, hasFavorites);
		setOnIconClickListener(clickListener);
		setOnFavoriteCheckedChangeListener(favListener);
		this.checkTeamDriveCallback = checkTeamDriveCallback;
	}

	@Override
	public boolean hasStableIds()
	{
		return !isFilterEnabled();
	}

	/**
	 * Returns just the position
	 */
	@Override
	public long getItemId(int position)
	{
		return position;
	}

	public ArrayList<FileInfo> getItems()
	{
		ArrayList<FileInfo> items = new ArrayList<FileInfo>();
		for (int i = 0; i < getCount(); i++)
		{
			items.add(getItem(i));
		}

		return items;
	}

	@Override
	public int getCount()
	{
		return mFilterController.getCount();
	}

	@Override
	public FileInfo getItem(int position)
	{
		return (FileInfo) mFilterController.getItem(position);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final CheckableRelativeLayout crl;
		final int id = (int) getItemId(position);
		final boolean checked = mChecked.get(id);
		final FileInfo item = getItem(position);

		final TextView name;
		final TextView size;
		final TextView lastModifiedDate;
		final FlipLayout fl;
		final ImageView frontView;
		final CheckableImageView favorite;
		final ViewHolder holder;
		// Log.d(TAG, "getView() for item: " + getItem(position));

		if (convertView != null)
		{
			crl = (CheckableRelativeLayout) convertView;

			// Updates id, position, and isAnimating
			holder = (ViewHolder) crl.getTag();
			holder.id = id;
			holder.position = position;
			holder.isAnimating = false;

			// Binds view references
			name = holder.name;
			size = holder.size;
			lastModifiedDate = holder.lastModifiedDate;
			fl = holder.flipLayout;
			frontView = holder.frontView;
			favorite = holder.favorite;
			//frontView.setBackgroundResource(item.getIconResId());
			//frontView.setImageDrawable(mContext.getResources().getDrawable(item.getIconResId()));
			// Clears the favorite listener so it won't trigger on the wrong item when state is restored later
			favorite.setOnCheckedChangeListener(null);
		}
		else
		{
			LayoutInflater inflater = LayoutInflater.from(mContext);
			crl = (CheckableRelativeLayout) inflater.inflate(R.layout.brw_default_item, parent, false);

			// Binds view references
			name = (TextView) crl.findViewById(R.id.name);
			size = (TextView) crl.findViewById(R.id.size);
			lastModifiedDate = (TextView) crl.findViewById(R.id.last_modified_date);
			fl = (FlipLayout) crl.findViewById(R.id.flip_layout);
			frontView = (ImageView) fl.findViewById(R.id.front_view);
			favorite = (CheckableImageView) crl.findViewById(R.id.favorite);

			// Handles start and stop of animations
			crl.setOnAttachStateChangeListener(mAnimationListener);
			fl.setOnFlipListener(mAnimationListener);

			// New holder, with position, id and non-animating
			holder = new ViewHolder(position, id, false);

			// Saves view references
			holder.name = name;
			holder.size = size;
			holder.lastModifiedDate = lastModifiedDate;
			holder.flipLayout = fl;
			holder.frontView = frontView;
			holder.favorite = favorite;
			crl.setTag(holder);
		}

		holder.frontView.setImageDrawable(mContext.getResources().getDrawable(item.getIconResId()));
		if(!item.isFolder())
			ThumbnailHelper.showThumbnail(item, holder.frontView, mRequester, mImageOptions);

		// Synchronizes background and text with state. The flip layout will be synchronized in the
		// next if-block
		crl.setChecked(checked);

		if (mHasFavorites)
		{
			favorite.setVisibility(View.VISIBLE);
			favorite.setChecked(item.isFavorite());

			if (checkTeamDriveCallback != null && !checkTeamDriveCallback.isTeamDriveExpired())
			{
				favorite.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						favorite.toggle();
					}
				});
				favorite.setOnCheckedChangeListener(new CheckableImageView.OnCheckedChangeListener()
				{
					@Override
					public void onCheckedChanged(CheckableImageView buttonView, boolean isChecked)
					{
						Log.d(TAG, "onCheckedChanged(), is: " + isChecked);
						boolean wasChecked = item.isFavorite();
						if (wasChecked != isChecked)
						{
							item.setFavorite(isChecked);
							mFavListener.onFavoriteCheckedChange(item, isChecked);
						}
					}
				});
			}
		}
		else
		{
			favorite.setVisibility(View.GONE);
		}

		String fileName = item.getName();
		long date = item.getLastModifiedDate();
		long fileSize = item.getSize();
		name.setText(fileName);
		lastModifiedDate.setText(GenericUtils.formatDate(mContext, date));
		size.setText(GenericUtils.formatFileSize(mContext, fileSize));
		size.setVisibility(item.isFolder() ? View.GONE : View.VISIBLE);

		// TODO: this method should be renamed and moved to browsing fragment
		if (checkTeamDriveCallback != null && checkTeamDriveCallback.isTeamDriveExpired())
		{
			if (crl != null)
			{
				crl.setAlpha(0.5f);
			}
		}

		// Updates FlipLayout to its current value, deleting eventual animations in progress
		fl.displayView(!checked);

		// Adds a click listener on the flip layout and passes over the callback to
		// OnIconClickListener
		fl.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				handleIconClick(v);
			}
		});

		return crl;
	}



	/** Returns whether a View returned by {@link #getView(int, View, ViewGroup)} is animating */
	public boolean isViewAnimating(View v)
	{
		if (!(v instanceof CheckableRelativeLayout))
		{
			throw new IllegalArgumentException("v must be a View returned by getView()");
		}

		ViewHolder holder = (ViewHolder) v.getTag();
		if (holder == null)
		{
			throw new IllegalArgumentException("v must be a View returned by getView(). Do not remove its tag");
		}

		return holder.isAnimating;
	}

	public void setOnIconClickListener(OnIconClickListener listener)
	{
		mIconClickListener = listener;
	}

	public void setOnFavoriteCheckedChangeListener(MultiSelectAdapter.OnFavoriteCheckedChangeListener listener)
	{
		mFavListener = listener;
	}

	@Override
	public boolean isFilterEnabled()
	{
		return mFilterController.isFilterEnabled();
	}

	public void sort(int category, int order)
	{
		// TODO: check this
		if (hasCheckedItems())
		{
			// throw new IllegalStateException("Sorting is not supported during multi-select");
			FeedbackToast.show(mContext, false, mContext.getString(R.string.ntf_error_sorting_not_supported));
			return;
		}

		// mFilterController.checkFilterDisabledOrThrow();
		SortManager.sort(mObjects, category, order);
		notifyDataSetChanged();
	}

	@Override
	public void setFilter(String filter)
	{

		// TODO: this check has to be removed; multi-select should be supporting during filtering
		if (hasCheckedItems() && !TextUtils.isEmpty(filter))
		{
			// because element cant marked after filtering right now
			return;
			// throw new IllegalStateException("Filtering is not supported during multi-select");
		}

		mFilterController.setFilter(filter);
		notifyDataSetChanged();
	}

	@Override
	public void disableFilter()
	{
		mFilterController.disableFilter();
		notifyDataSetChanged();
	}

	public int getCheckedCount()
	{
		mFilterController.checkFilterDisabledOrThrow();
		return getCheckedPositions().size();
	}

	public ArrayList<Integer> getCheckedPositions()
	{
		mFilterController.checkFilterDisabledOrThrow();

		ArrayList<Integer> positions = new ArrayList<Integer>();
		for (int i = 0; i < getCount(); i++)
		{
			if (mChecked.valueAt(i))
			{
				positions.add(mChecked.keyAt(i));
			}
		}

		return positions;
	}

	public ArrayList<FileInfo> getCheckedItems()
	{
		mFilterController.checkFilterDisabledOrThrow();

		ArrayList<Integer> checkedPositions = getCheckedPositions();
		ArrayList<FileInfo> checkedItems = new ArrayList<FileInfo>();
		for (int i = 0; i < getCount(); i++)
		{
			if (checkedPositions.contains(i))
			{
				checkedItems.add(getItem(i));
			}
		}

		return checkedItems;
	}

	public boolean isChecked(int position)
	{
		mFilterController.checkFilterDisabledOrThrow();
		return mChecked.get(position);
	}

	public boolean hasCheckedItems()
	{
		for (int i = 0; i < mChecked.size(); i++)
		{
			if (mChecked.valueAt(i))
			{
				return true;
			}
		}
		return false;
	}

	/** Toggles the element with specified id and returns its new checked state */
	public boolean toggle(int position)
	{
		mFilterController.checkFilterDisabledOrThrow();

		if (position < 0 || position > getCount() - 1)
		{
			throw new IllegalArgumentException("id must be a valid position in the dataset");
		}

		boolean newChecked = !isChecked(position);
		mChecked.put(position, newChecked);
		toggleInListView(position);
		return newChecked;
	}

	/** Checks items in the given positions in the dataset without an animation */
	public void check(List<Integer> positions)
	{
		mFilterController.checkFilterDisabledOrThrow();

		if (positions == null)
		{
			throw new IllegalArgumentException("ids must be non-null");
		}

		clearChecked();
		setChecked(positions);
		checkInListView(positions);
	}

	public void uncheckAll()
	{
		mFilterController.checkFilterDisabledOrThrow();

		List<Integer> positions = getCheckedPositions();
		for (Integer position : positions)
		{
			toggle(position);
		}
	}

	// Must be called only by MultiSelectListView
	/* package */void bindMultiSelectListView(MultiSelectListView lv)
	{
		mListView = lv;
	}

	// Must be called only by MultiSelectListView
	/* package */void unbindMultiSelectListView()
	{
		mListView = null;
	}

	private void toggleInListView(int position)
	{
		if (mListView == null)
		{
			return;
		}

		int index = position - mListView.getFirstVisiblePosition();
		if (index < 0 || index > mListView.getChildCount() - 1)
		{
			return;
		}

		CheckableRelativeLayout child = (CheckableRelativeLayout) mListView.getChildAt(index);
		boolean newChecked = !child.isChecked();
		child.setChecked(newChecked);

		ViewHolder holder = (ViewHolder) child.getTag();
		holder.flipLayout.flip();
	}

	private void checkInListView(List<Integer> positions)
	{
		if (mListView == null)
		{
			return;
		}

		int firstVisible = mListView.getFirstVisiblePosition();
		int count = mListView.getChildCount();

		for (int i = 0; i < count; i++)
		{
			int position = firstVisible + i;
			if (positions.contains(position))
			{
				CheckableRelativeLayout child = (CheckableRelativeLayout) mListView.getChildAt(i);
				child.setChecked(true);
			}
		}
	}

	/** Checks all ids in idsToCheck */
	private void setChecked(List<Integer> positions)
	{
		for (Integer position : positions)
		{
			mChecked.put(position, true);
		}
	}

	// mChecked.clear() does not remove the true entries
	private void clearChecked()
	{
		for (int i = 0; i < mChecked.size(); i++)
		{
			if (mChecked.valueAt(i))
			{
				int key = mChecked.keyAt(i);
				mChecked.put(key, false);
			}
		}
	}

	private void handleIconClick(View v)
	{
		if (mIconClickListener != null)
		{
			CheckableRelativeLayout cll = (CheckableRelativeLayout) v.getParent();
			ViewHolder holder = (ViewHolder) cll.getTag();
			mIconClickListener.onIconClick(v, cll, holder.position, holder.id);
		}
	}

	public void removeThumbnailsFromCache(ArrayList<FileInfo> fileInfos) {
		ThumbnailHelper.removeThumbnailsFromCache(mContext, fileInfos, mRequester);
	}

	public void removeThumbnailFromCache(FileInfo fileInfo) {
		ThumbnailHelper.removeThumbnailFromCache(fileInfo, mRequester);
	}

	class AnimationListener implements OnAttachStateChangeListener, OnFlipListener
	{
		@Override
		public void onViewAttachedToWindow(View v)
		{
			// Does nothing
		}

		@Override
		public void onViewDetachedFromWindow(View v)
		{
			updateViewOnStopAnimation(v);
		}

		@Override
		public void onFlipStarted(View container, View entering, View exiting)
		{
			updateViewOnStartAnimation((View) container.getParent());
		}

		@Override
		public void onFlipFinished(View container, View entering, View exiting)
		{
			updateViewOnStopAnimation((View) container.getParent());
		}

		@Override
		public void onFlipCanceled(View container, View entering, View exiting)
		{
			updateViewOnStopAnimation((View) container.getParent());
		}

		private void updateViewOnStartAnimation(View v)
		{
			ViewHolder holder = (ViewHolder) v.getTag();
			holder.isAnimating = true;
		}

		private void updateViewOnStopAnimation(View v)
		{
			ViewHolder holder = (ViewHolder) v.getTag();
			holder.isAnimating = false;

			boolean checked = mChecked.get(holder.id);
			holder.flipLayout.displayView(!checked);
		}
	}


	private static class ViewHolder
	{
		// Used for canceling the transient state of view
		boolean isAnimating = false;
		int id;
		int position;

		// Used for speed optimizations
		TextView name;
		TextView size;
		TextView lastModifiedDate;
		FlipLayout flipLayout;
		ImageView frontView;
		CheckableImageView favorite;

		ViewHolder(int position, int id, boolean isAnimating)
		{
			this.position = position;
			this.id = id;
			this.isAnimating = isAnimating;
		}
	}

	public interface OnIconClickListener
	{
		void onIconClick(View v, ViewGroup parent, int position, int id);
	}

	public interface OnFavoriteCheckedChangeListener
	{
		public void onFavoriteCheckedChange(FileInfo file, boolean checked);
	}

	public interface CheckTeamDriveCallback
	{
		public boolean isTeamDriveExpired();
	}

}