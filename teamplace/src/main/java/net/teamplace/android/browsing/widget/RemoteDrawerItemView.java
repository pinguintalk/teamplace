package net.teamplace.android.browsing.widget;

import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.session.configuration.WorkplaceInformation;
import net.teamplace.android.http.teamdrive.quota.AccountQuotaModel;
import net.teamplace.android.teamdrive.widget.QuotaView;
import net.teamplace.android.utils.CloudCentralHelper;
import android.animation.LayoutTransition;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.LinearLayout;

import com.cortado.android.R;

public class RemoteDrawerItemView extends LinearLayout implements Checkable {
	private final CheckableRelativeLayout mMainItem;
	private final LinearLayout mInfoItem;
	private final QuotaView mQuotaView;
	private AccountQuotaModel mQuota;
	private QuotaExpandListener mExpandListener;

	private final Button mUpgradeBtn;
	private final View.OnClickListener mInfoBtnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			expandOrCollapseQuotaInfo();
		}
	};

	private final View.OnClickListener mUpgradeBtnClickListener = new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			CloudCentralHelper.startMyPlaceUpgrade(getContext());
		}
	};

	public RemoteDrawerItemView(Context context) {
		this(context, null);
	}

	public RemoteDrawerItemView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public RemoteDrawerItemView(final Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setOrientation(LinearLayout.VERTICAL);
		setBackgroundColor(context.getResources().getColor(R.color.brw_drawer_item_bg_dark));

		LayoutTransition lt = new LayoutTransition();
		lt.enableTransitionType(LayoutTransition.APPEARING);
		lt.enableTransitionType(LayoutTransition.DISAPPEARING);
		setLayoutTransition(lt);

		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.brw_drawer_item_remote, this, true);
		mMainItem = (CheckableRelativeLayout) v.findViewById(R.id.remote_main_item);
		mInfoItem = (LinearLayout) v.findViewById(R.id.info_item);
		mQuotaView = (QuotaView) mInfoItem.findViewById(R.id.quota);

		View infoBtn = mMainItem.findViewById(R.id.info);
		infoBtn.setOnClickListener(mInfoBtnClickListener);

		mUpgradeBtn = (Button) mInfoItem.findViewById(R.id.upgrade);
		mUpgradeBtn.setOnClickListener(mUpgradeBtnClickListener);
	}

	public void setQuota(AccountQuotaModel quota) {
		mQuota = quota;
	}

	public boolean isCollapsed() {
		return mInfoItem.getVisibility() == View.GONE;
	}

	public void expandOrCollapseQuotaInfo() {
		if (isCollapsed()) {
			expandQuotaInfo();
		} else {
			collapseQuotaInfo();
		}
	}

	public void expandQuotaInfo() {
		if (isCollapsed()) {
			if (mExpandListener != null)
			{
				mExpandListener.onExpandStateChanged(QuotaExpandListener.EXPANDING);
			}
			mInfoItem.setVisibility(View.VISIBLE);
			refreshQuota();
		}
	}

	public void collapseQuotaInfo() {
		if (!isCollapsed()) {
			if (mExpandListener != null)
			{
				mExpandListener.onExpandStateChanged(QuotaExpandListener.COLLAPSING);
			}
			mInfoItem.setVisibility(View.GONE);
		}
	}

	@Override
	public void setChecked(boolean checked) {
		mMainItem.setChecked(checked);
	}

	@Override
	public boolean isChecked() {
		return mMainItem.isChecked();
	}

	@Override
	public void toggle() {
		mMainItem.toggle();
	}

	public void refreshQuota() {
		if (mQuota == null) {
			mQuotaView.setQuotaKnown(false);
			return;
		}
		boolean isUnlimited = mQuota.limit == 0;
		mQuotaView.setUnlimited(isUnlimited);
		if (!isUnlimited) {
			mQuotaView.setMaxBytes(((long) mQuota.remainingMb + (long) mQuota.usedMb) * 1024l * 1024l);
		}
		mQuotaView.setQuota((float )mQuota.usedPercent / 100.f);
		mQuotaView.setQuotaKnown(true);
	}

	public void setmExpandListener(QuotaExpandListener listener)
	{
		mExpandListener = listener;
	}

	public interface QuotaExpandListener
	{
		int EXPANDING = 0;
		int COLLAPSING = 1;
		void onExpandStateChanged(int state);
	}
}
