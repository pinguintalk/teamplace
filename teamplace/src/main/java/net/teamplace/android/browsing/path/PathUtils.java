package net.teamplace.android.browsing.path;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.browsing.path.Path.Remote;
import net.teamplace.android.browsing.path.Path.TeamDrive;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import android.content.Context;
import android.text.TextUtils;

import com.cortado.android.R;

/**
 * Utility methods for paths
 * 
 * @author Gil Vegliach
 */
public class PathUtils
{
	private PathUtils()
	{
	}

	/**
	 * Checks whether all the paths in a Path array have the same type and are from the same teamdrive, in case they are
	 * Path.TeamDrive
	 */
	public static void checkSameLocationOrThrow(Path[] paths)
	{
		if (paths == null || paths.length == 0)
		{
			return;
		}

		// Type check
		Class<?> type = paths[0].getClass();
		for (int i = 0; i < paths.length; i++)
		{
			if (paths[i] == null || !paths[i].getClass().equals(type))
			{
				throw new IllegalArgumentException("Paths must be non-null and all from the same location");
			}
		}

		if (!type.equals(Path.TeamDrive.class))
		{
			return;
		}

		// Teamdrives check: paths must be from the same teamdrive
		String teamDriveId = ((Path.TeamDrive) paths[0]).getTeamDriveId();
		for (int i = 0; i < paths.length; i++)
		{
			String id = ((Path.TeamDrive) paths[i]).getTeamDriveId();
			if (TextUtils.isEmpty(id) || !id.equals(teamDriveId))
			{
				throw new IllegalArgumentException("Paths must from the same teamdrive");
			}
		}
	}

	/**
	 * Short user friendly path String representation. This representation does not include the location of a path
	 * and should be used only where that is obvious from the context. This method is used for example as the subtitle
	 * of the action bar during browsing.
	 */
	public static String shortDisplayablePath(Path path)
	{
		checkNonNullArg(path);
		return "/" + path.getPathFile().getPath();
	}

	/**
	 * Complete user friendly path string representation. Differences from {@link Path#toString()}, include a
	 * different string for local paths (phones or tablets), a product-aware string for remote path (now MyPlace),
	 * and team drive names instead of team ids.
	 */
	public static String displayablePath(Context context, Path path)
	{
		checkNonNullArgs(context, path);
		final String location = PathUtils.actionBarPathSite(context, path);
		final String shortPath = shortDisplayablePath(path);
		final Context ctx = context;

		PathOperation<String> op = new PathOperation<String>()
		{
			@Override
			public String execute(TeamDrive path)
			{
				SessionManager sm = new SessionManager(ctx);
				Session session = sm.getLastSession();
				net.teamplace.android.http.teamdrive.TeamDrive teamdrive = session.getTeamDriveMap().get(
						path.getTeamDriveId());
				String prefix = teamdrive == null ? location : teamdrive.getName();
				return prefix + Path.LOCATION_PATH_SEPARATOR + shortPath;
			}

			@Override
			public String execute(Remote path)
			{
				return location + Path.LOCATION_PATH_SEPARATOR + shortPath;
			}

			@Override
			public String execute(Local path)
			{
				return location + Path.LOCATION_PATH_SEPARATOR + shortPath;
			}
		};
		return path.execute(op);
	}

	/**
	 * Returns a String that represent where the path "lives", in a user-friendly way. A local path lives in the
	 * site "Phone" or "Tablet", a remote path lives in "MyPlace" and a teamplace path lives in "Teamplaces". This
	 * method is used for the action bar title during browsing.
	 */
	public static String actionBarPathSite(Context context, Path path)
	{
		checkNonNullArgs(context, path);
		final Context ctx = context;
		PathOperation<String> op = new PathOperation<String>()
		{
			@Override
			public String execute(Local path)
			{
				return ctx.getString(R.string.brw_drawer_local);
			}

			@Override
			public String execute(Remote path)
			{
				return ctx.getString(R.string.brw_drawer_remote);
			}

			@Override
			public String execute(TeamDrive path)
			{
				return ctx.getString(R.string.abt_teamdrives);
			}
		};
		return path.execute(op);
	}

	/**
	 * String representation of a Path suitable for storing it in a database
	 */
	public static String databaseString(Path path)
	{
		checkNonNullArg(path);
		return path.toString();
	}

	/**
	 * Returns Path.REMOTE_LOCATION or a teamdrive location from a teamDriveId obtained from a server request. In
	 * particular, on a null or empty teamDriveId the location is Path.REMOTE_LOCATION, otherwise is an appropriate
	 * location representing the team drive.
	 * 
	 * @see #buildPathfromServerResponseString(String, String)
	 */
	public static String pathLocationFromTeamDriveId(String teamDriveId)
	{
		if (TextUtils.isEmpty(teamDriveId))
		{
			return Path.REMOTE_LOCATION;
		}

		if (".".equals(teamDriveId))
		{
			return Path.REMOTE_LOCATION;
		}

		return Path.teamDriveLocation(teamDriveId);
	}

	/**
	 * Returns a Path object from parameters returned by the server.This method handles slashes correctly
	 * and should be preferred to do-yourself ways.
	 * 
	 * @see #pathLocationFromTeamDriveId(String)
	 */
	public static Path buildPathfromServerResponseString(String location, String pathname)
	{
		checkNonNullArgs(location, pathname);
		pathname = pathname.replaceAll("\\\\", "/").substring(1);
		return Path.valueOf(location, pathname);
	}

	/**
	 * Returns a String representing a Path suitable for a server request. This method handles slashes correctly
	 * and should be preferred to do-yourself ways.
	 */
	public static String remoteRequestString(Path path)
	{
		checkNonNullArg(path);
		// '\\\\' represent a single backslash '\'. It is escaped twice: the first time because of the docs of
		// String.replaceAll() (it should yield the same result as Matcher.replaceAll() which in turn lets use
		// backslashes to escape characters in the replacement String), and the second time because of Java syntax
		String serverRelativePath = path.getPathFile().getPath().replaceAll("/", "\\\\");
		return "\\" + serverRelativePath;
	}

	/** Builds a new path that has the same parent as <code>path</code> but has <code>newName</code> as name */
	public static Path buildRenamedPath(Path path, String newName)
	{
		checkNonNullArgs(path, newName);
		return Path.valueOf(path.getParentPath(), newName);
	}

	/**
	 * Retrieves the teamdrive id of path if path is of type {@link Path.TeamDrive}, otherwise returns null. Throws
	 * an exception if path is null.
	 */
	public static String getTeamDriveIdOrNull(Path path)
	{
		checkNonNullArg(path);
		return (path instanceof Path.TeamDrive)
				? ((Path.TeamDrive) path).getTeamDriveId()
				: null;
	}
}
