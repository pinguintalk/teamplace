package net.teamplace.android.browsing.navigation;

/**
 * Models top level navigation. Implemented by {@link NavigationFragment}
 * 
 * @author Gil Vegliach
 */
public interface Navigation
{
	static enum TopLevel
	{
		FAVORITES, REMOTE, TEAMDRIVES, LOCAL, SETTINGS
	}

	void setNavigationCallback(Callback callback);

	void select(TopLevel level);

	interface Callback
	{
		void onNavigationTopLevelSelected(TopLevel level);
	}
}
