package net.teamplace.android.browsing.filtering;

/**
 * Represents an Adapter that implements filtering. The Adapter can implement filtering with a {@link FilterController}.
 * 
 * @author Gil Vegliach
 */
public interface FilterableAdapter
{
	/** True iff the filter is enabled at the moment */
	boolean isFilterEnabled();

	/** Applies the filter given by <code>filter</code>. */
	void setFilter(String filter);

	/** Should be equivalent to {@link #setFilter(String) setFilter(null)}. */
	void disableFilter();
}
