package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cortado.android.R;

public class DrawerItemView extends CheckableLinearLayout
{
	public final static int REGULAR = 0;
	public final static int THIN = 1;

	private final static Typeface REGULAR_TYPEFACE = Typeface.create("sans-serif-medium", Typeface.NORMAL);
	private final static float REGULAR_TEXT_SIZE = 20.f;

	private final static Typeface THIN_TYPEFACE = Typeface.create("sans-serif-thin", Typeface.NORMAL);
	private final static float THIN_TEXT_SIZE = 18.f;

	private final TextView mLabel;
	private final ImageView mIcon;

	public DrawerItemView(Context context)
	{
		this(context, null);

	}

	public DrawerItemView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);

	}

	public DrawerItemView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		setOrientation(LinearLayout.HORIZONTAL);
		setGravity(Gravity.CENTER_VERTICAL);
		setClickable(true);

		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.brw_drawer_item, this, true);
		mLabel = (TextView) v.findViewById(R.id.label);
		mIcon = (ImageView) v.findViewById(R.id.icon);

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DrawerItemView, 0, 0);
		try
		{
			setLabel(a.getString(R.styleable.DrawerItemView_label));
			setIcon(a.getDrawable(R.styleable.DrawerItemView_icon));
			int type = a.getInt(R.styleable.DrawerItemView_appearance, REGULAR);
			setAppearance(type);
		}
		finally
		{
			a.recycle();
		}
	}

	public void setIcon(Drawable d)
	{
		mIcon.setImageDrawable(d);
	}

	public void setLabel(String text)
	{
		mLabel.setText(text);
	}

	/** Type must be either {@link #THIN} or {@link #REGULAR} */
	public void setAppearance(int type)
	{
		if (type == THIN)
		{
			mLabel.setTypeface(THIN_TYPEFACE);
			mLabel.setTextSize(THIN_TEXT_SIZE);
		}
		else if (type == REGULAR)
		{
			mLabel.setTypeface(REGULAR_TYPEFACE);
			mLabel.setTextSize(REGULAR_TEXT_SIZE);
		}
		else
		{
			throw new IllegalArgumentException("type must be either THIN or REGULAR");
		}
	}
}
