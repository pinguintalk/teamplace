package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;

/**
 * A SwipeRefreshLayout that cannot be scrolled up. Use this layout only for maintaining consistency in the progress
 * animations.
 */
final public class LockedSwipeRefreshLayout extends SwipeRefreshLayout
{
	public LockedSwipeRefreshLayout(Context context)
	{
		super(context);
	}

	public LockedSwipeRefreshLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	@Override
	public boolean canChildScrollUp()
	{
		// Disable the swipe-to-refresh gesture permanently
		return true;
	}
}
