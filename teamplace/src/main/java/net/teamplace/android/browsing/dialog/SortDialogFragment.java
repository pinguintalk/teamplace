package net.teamplace.android.browsing.dialog;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.browsing.sorting.SortManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Checkable;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;

public class SortDialogFragment extends DialogFragment implements View.OnClickListener
{
	public static final String TAG = tag(SortDialogFragment.class);
	private static final String PKG = pkg(SortDialogFragment.class);

	public static final int REQUEST_CODE = R.id.request_code_sort;

	public static final int CATEGORY_NAME = SortManager.CATEGORY_NAME;
	public static final int CATEGORY_SIZE = SortManager.CATEGORY_SIZE;
	public static final int CATEGORY_DATE = SortManager.CATEGORY_DATE;

	public static final int ORDER_ASCENDING = SortManager.ORDER_ASCENDING;
	public static final int ORDER_DESCENDING = SortManager.ORDER_DESCENDING;

	public static final int DEFAULT_CATEGORY = SortManager.DEFAULT_CATEGORY;
	public static final int DEFAULT_ORDER = SortManager.DEFAULT_ORDER;

	public static final String KEY_CATEGORY = PKG + ".category";
	public static final String KEY_ORDER = PKG + ".order";

	private static final ItemModel[] DATA_MODEL = new ItemModel[] {
			new ItemModel(R.id.option_name, R.drawable.brw_sort_dlg_name,
					R.string.brw_sort_dlg_option_name, CATEGORY_NAME),

			new ItemModel(R.id.option_size, R.drawable.brw_sort_dlg_size,
					R.string.brw_sort_dlg_option_size, CATEGORY_SIZE),

			new ItemModel(R.id.option_date, R.drawable.brw_sort_dlg_date,
					R.string.brw_sort_dlg_option_date, CATEGORY_DATE),

			new ItemModel(R.id.option_ascending, R.drawable.brw_sort_dlg_asc,
					R.string.brw_sort_dlg_option_ascending, ORDER_ASCENDING),

			new ItemModel(R.id.option_descending, R.drawable.brw_sort_dlg_desc,
					R.string.brw_sort_dlg_option_descending, ORDER_DESCENDING),
	};

	private ViewGroup mContainer;

	private int mCheckedCategory;
	private int mCheckedOrder;

	public static SortDialogFragment newInstance(int checkedCategory, int checkedOrder)
	{
		checkCondition(0 <= checkedCategory && checkedCategory <= 2,
				"checkedCategory must be one of the CATEGORY_* constants");
		checkCondition(10 <= checkedOrder && checkedOrder <= 11,
				"checkedOrder must be one of the ORDER_* constants");

		SortDialogFragment f = new SortDialogFragment();
		Bundle args = new Bundle(2);
		args.putInt(KEY_CATEGORY, checkedCategory);
		args.putInt(KEY_ORDER, checkedOrder);
		f.setArguments(args);
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		if (savedInstanceState == null)
		{
			Bundle args = getArguments();
			mCheckedCategory = args.getInt(KEY_CATEGORY, -1);
			mCheckedOrder = args.getInt(KEY_ORDER, -1);
		}
		else
		{
			mCheckedCategory = savedInstanceState.getInt(KEY_CATEGORY, -1);
			mCheckedOrder = savedInstanceState.getInt(KEY_ORDER, -1);
		}

		if (mCheckedCategory == -1 || mCheckedOrder == -1)
		{
			throw new IllegalStateException("Category and/or order not checked. Category: " + mCheckedCategory
					+ ", order: " + mCheckedOrder);
		}
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Context ctx = getActivity();
		View v = LayoutInflater.from(ctx).inflate(R.layout.brw_sort_dialog, null);
		mContainer = (ViewGroup) v.findViewById(R.id.container);

		TextView label = (TextView) v.findViewById(R.id.label);
		label.setText(R.string.brw_sort_dlg_label);

		for (ItemModel itemModel : DATA_MODEL)
		{
			View item = mContainer.findViewById(itemModel.viewId);
			ImageView icon = (ImageView) item.findViewById(R.id.icon);
			label = (TextView) item.findViewById(R.id.label);
			icon.setBackgroundResource(itemModel.iconDrawableId);
			label.setText(itemModel.labelStringId);
			item.setTag(itemModel);
			item.setOnClickListener(this);
		}

		StyleableAlertDialog dlg = new StyleableAlertDialog(ctx, 0);
		dlg.setTitle(R.string.brw_sort_dlg_title);
		dlg.setView(v);
		dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.app_cancel),
				(DialogInterface.OnClickListener) null);
		dlg.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.brw_sort_dlg_sort_btn),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						Fragment target = getTargetFragment();
						if (target != null)
						{
							Intent data = new Intent();
							data.putExtra(KEY_CATEGORY, mCheckedCategory);
							data.putExtra(KEY_ORDER, mCheckedOrder);
							target.onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
						}
						dismiss();
					}
				});
		return dlg;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		refreshViewsState();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_CATEGORY, mCheckedCategory);
		outState.putInt(KEY_ORDER, mCheckedOrder);
	}

	@Override
	public void onClick(View v)
	{
		ItemModel item = (ItemModel) v.getTag();
		if (SortManager.isCategoryConstant(item.constant))
		{
			mCheckedCategory = item.constant;
		}
		else
		{
			mCheckedOrder = item.constant;
		}
		refreshViewsState();
	}

	private void refreshViewsState()
	{
		int count = mContainer.getChildCount();
		for (int i = 0; i < count; i++)
		{
			View v = mContainer.getChildAt(i);
			if (v instanceof Checkable)
			{
				ItemModel item = (ItemModel) v.getTag();

				// Assumes CATEGORY_* and ORDER_* are disjoint sets
				boolean checked = mCheckedCategory == item.constant ||
						mCheckedOrder == item.constant;
				((Checkable) v).setChecked(checked);
			}
		}
	}

	static class ItemModel
	{
		final int viewId;
		final int iconDrawableId;
		final int labelStringId;
		final int constant;

		ItemModel(int viewId, int iconDrawableId, int labelStringId, int constant)
		{
			this.viewId = viewId;
			this.iconDrawableId = iconDrawableId;
			this.labelStringId = labelStringId;
			this.constant = constant;
		}
	}
}
