package net.teamplace.android.browsing.dialog;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import net.teamplace.android.utils.Log;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cortado.android.R;

/**
 * Dialog Fragment that manages an EditText and returns the text when done. The cursor will be always positioned at the
 * end of the text.
 * 
 * @author Gil Vegliach
 */
public class EditTextDialogFragment extends DialogFragment implements TextWatcher
{
	private static final String PKG = pkg(EditTextDialogFragment.class);

	public static final String TAG = tag(EditTextDialogFragment.class);
	public static final String KEY_TEXT = PKG + ".text";

	private static final String KEY_TITLE = PKG + ".title";
	private static final String KEY_INITIAL_TEXT = PKG + ".initialText";
	private static final String KEY_IS_FOLDER = "isFolder";
	private static final String KEY_ILLEGAL_INPUTS = "illegalInputs";

	private static final int ID_EDIT_TEXT = 123456;
	private static final int MAX_STRING_LENGTH = 255;
	private static final int INPUT_VALID = 0;
	private static final int INPUT_EMPTY = 1;
	private static final int INPUT_STARTS_INVALID = -1;
	private static final int INPUT_INVALID_CHARACTERS = -2;
	private static final int INPUT_TOO_LONG = -3;
	private static final int INPUT_ILLEGAL = -4;
	
	private EditText editText;
	private TextView errorTv;

	private HashMap<String, String> illegalInputs;

	public static EditTextDialogFragment newInstance(CharSequence title, CharSequence initialText, boolean isFolder)
	{
		return newInstance(title, initialText, isFolder, new HashMap<String, String>(0));
	}

	public static EditTextDialogFragment newInstance(CharSequence title, CharSequence initialText, boolean isFolder,
			HashMap<String, String> illegalInputs)
	{
		EditTextDialogFragment frag = new EditTextDialogFragment();
		Bundle args = new Bundle();
		args.putCharSequence(KEY_TITLE, title);
		args.putCharSequence(KEY_INITIAL_TEXT, initialText);
		args.putBoolean(KEY_IS_FOLDER, isFolder);
		args.putSerializable(KEY_ILLEGAL_INPUTS, illegalInputs);
		frag.setArguments(args);
		return frag;
	}

	@SuppressWarnings("unchecked")
	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Bundle args = getArguments();
		illegalInputs = (HashMap<String, String>) args.getSerializable(KEY_ILLEGAL_INPUTS);
		CharSequence title = args.getCharSequence(KEY_TITLE);

		Context context = getActivity();
		final StyleableAlertDialog dlg = new StyleableAlertDialog(context);
		dlg.setTitle(title);

		// Dialogs do not support margins, so we wrap the EditText in a FrameLayout

		ViewGroup.LayoutParams frameLayoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT);

		FrameLayout frameLayout = new FrameLayout(context);
		frameLayout.setLayoutParams(frameLayoutParams);

		float density = context.getResources().getDisplayMetrics().density;

		FrameLayout.LayoutParams relativeLayoutParams = new FrameLayout.LayoutParams(
				FrameLayout.LayoutParams.MATCH_PARENT,
				FrameLayout.LayoutParams.MATCH_PARENT);

		relativeLayoutParams.topMargin = (int) (8 * density + .5f);
		relativeLayoutParams.leftMargin = (int) (16 * density + .5f);
		relativeLayoutParams.rightMargin = (int) (16 * density + .5f);
		relativeLayoutParams.bottomMargin = (int) (8 * density + .5f);

		// no bottomMargin, space already put in by the dialog

		RelativeLayout relativeLayout = new RelativeLayout(context);
		relativeLayout.setLayoutParams(relativeLayoutParams);

		frameLayout.addView(relativeLayout);

		RelativeLayout.LayoutParams editTextParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		editText = new EditText(context);
		editText.setId(ID_EDIT_TEXT);
		editText.setLayoutParams(editTextParams);
		editText.setMaxLines(1);
		editText.setSingleLine(true);
		
		editText.setPadding((int) (8 * density + .5f), (int) (8 * density + .5f), (int) (8 * density + .5f),
				(int) (8 * density + .5f));
		
		relativeLayout.addView(editText);

		// Set text
		CharSequence text;
		if (savedInstanceState == null)
		{
			text = args.getString(KEY_INITIAL_TEXT);
			editText.setText(text);
		}
		else
		{
			text = savedInstanceState.getString(KEY_TEXT);
			editText.setText(text);
		}
		setSelection(getArguments().getBoolean(KEY_IS_FOLDER));
		editText.addTextChangedListener(this);

		RelativeLayout.LayoutParams textViewParams = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.MATCH_PARENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);

		textViewParams.addRule(RelativeLayout.BELOW, ID_EDIT_TEXT);

		errorTv = new TextView(context);
		errorTv.setLayoutParams(textViewParams);
		errorTv.setTextColor(Color.RED);
		errorTv.setGravity(Gravity.CENTER_HORIZONTAL);
		errorTv.setVisibility(View.GONE);

		errorTv.setPadding((int) (8 * density + .5f), (int) (8 * density + .5f), (int) (8 * density + .5f),
				(int) (8 * density + .5f));

		relativeLayout.addView(errorTv);

		dlg.setView(frameLayout);
		dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.app_cancel),
				(DialogInterface.OnClickListener) null);
		
		dlg.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.app_ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						if (validateInput() != INPUT_VALID)
							return;

						Intent data = new Intent();
						data.putExtra(KEY_TEXT, editText.getText().toString());
						getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, data);
						dismiss();
					}
			    });
 
		// TODO: check this when user stack this dialog on top of the smartfile dialog and press ok
		// Show the soft keyboard when the dialog pops up
		dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		// Set appropriately the OK button depending on the input as soon as the dialog is displayed
		dlg.setOnShowListener(new DialogInterface.OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				showInputFeedback();
			}
		});

		return dlg;
	}


	 
	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
	}

	@Override
	public void afterTextChanged(Editable editable)
	{
		showInputFeedback();
	}

	@Override
	public void onSaveInstanceState(Bundle out)
	{
		out.putString(KEY_TEXT, editText.getText().toString());
	}

	private void showInputFeedback()
	{
		Dialog dlg = getDialog();
		if (dlg instanceof AlertDialog)
		{
			AlertDialog alertDlg = (AlertDialog) dlg;
			Button button = alertDlg.getButton(AlertDialog.BUTTON_POSITIVE);
			int inputCode = validateInput();

			if (inputCode == INPUT_VALID)
			{
				button.setEnabled(true);
				errorTv.setVisibility(View.GONE);
			}
			else if (inputCode == INPUT_EMPTY)
			{
				button.setEnabled(false);
				errorTv.setVisibility(View.GONE);
			}
			else
			{
				button.setEnabled(false);

				if (inputCode < INPUT_VALID)
				{
					errorTv.setVisibility(View.VISIBLE);

					if (inputCode == INPUT_TOO_LONG)
					{
						errorTv.setText(R.string.brw_input_too_long);
					}
					else if (inputCode == INPUT_STARTS_INVALID)
					{
						errorTv.setText(R.string.brw_input_starts_invalid);
					}
					else if (inputCode == INPUT_ILLEGAL)
					{
						errorTv.setText(illegalInputs.get(editText.getText().toString().toLowerCase(Locale.getDefault())));
					}
					else
					{
						errorTv.setText(R.string.brw_input_invalid_characters);
					}
				}
			}
		}
	}

	/**
	 * Returns an input code based on the text in editTexto
	 */
	private int validateInput()
	{
		String input = editText.getText().toString();

		if (TextUtils.isEmpty(input))
		{
			return INPUT_EMPTY;
		}
		else if (exceedMaxStringLength(input))
		{
			return INPUT_TOO_LONG;
		}
		else if (startsWithInvalidCharacter(input))
		{
			return INPUT_STARTS_INVALID;
		}
		else if (containsInvalidCharacters(input))
		{
			return INPUT_INVALID_CHARACTERS;
		}
		else if (isIllegalInput(input))
		{
			return INPUT_ILLEGAL;
		}
		
		return INPUT_VALID;
	}


	private boolean containsInvalidCharacters(String input)
	{
		String pattern = "[/\\\\|<>?:\"*]";
		return Pattern.compile(pattern).matcher(input).find();
	}

	private boolean startsWithInvalidCharacter(String input)
	{
		return input.startsWith(" ") || input.startsWith(".");
	}

	private boolean exceedMaxStringLength(String input)
	{
		return input.length() > MAX_STRING_LENGTH;
	}

	private boolean isIllegalInput(String input)
	{
		Set<String> keys = illegalInputs.keySet();
		for (String toCheck : keys)
		{
			if (toCheck.equalsIgnoreCase(input))
			{
				return true;
			}
		}
		return false;
	}

	private void setSelection(boolean includeExtension)
	{
		String itemName = editText.getText().toString();
		int start = 0, stop = itemName.length();
		if (!includeExtension)
		{
			int index = itemName.lastIndexOf(".");
			if (index != -1)
				stop = index;
		}
		editText.setSelection(start, stop);
	}
}
