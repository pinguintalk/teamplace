package net.teamplace.android.browsing.ui;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.initialization.InitializationActivity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Pair;
import android.view.View;
import android.widget.RemoteViews;

import com.cortado.android.R;

import de.greenrobot.event.EventBus;

public class NotificationHelper
{
	private static final String TAG = "net.teamplace.android.browsing.ui.NotificationHelper";
	private static final int ID_PROGRESS = 0;
	private static final int ID_DONE = 1;
	private static final int ID_FAILED = 2;

	private static NotificationHelper instance;

	List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
	List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
	List<Pair<FileJobInfo.PathStateInfo, Long>> finishedOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
	List<Pair<FileJobInfo.PathStateInfo, Long>> failedOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();

	private final Map<Long, FileJobInfo> jobinfos = new HashMap<Long, FileJobInfo>();

	private NotificationHelper()
	{

	}

	public static synchronized NotificationHelper getInstance()
	{
		if (instance == null)
		{
			instance = new NotificationHelper();
		}
		return instance;
	}

	public synchronized void updateProgressNotification(Context ctx, FileJobInfo[] jobs)
	{
		boolean multipleOngoingNoProgress = false;
		extractOperationLists(jobs);
		multipleOngoingNoProgress = ongoingOperations.size() > 1;

		NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification.Builder builder = new Notification.Builder(ctx);
		builder.setOngoing(true);
		builder.setPriority(Notification.PRIORITY_MAX);

		if (ongoingOperations.size() > 0)
		{
			builder.setTicker(getActionString(ctx, ongoingOperations.get(0).second));
			buildSmallNotification(ctx, builder, ongoingOperations, activeOperations, multipleOngoingNoProgress);
			Notification notification = builder.build();
			buildLargeNotification(ctx, notification, ongoingOperations, activeOperations, multipleOngoingNoProgress);
			nm.notify(TAG, ID_PROGRESS, notification);
		}
		else
		{
			if (activeOperations.size() < 1)
			{
				nm.cancel(TAG, ID_PROGRESS);
			}
		}
	}

	public synchronized void cancelProgressNotification(Context ctx)
	{
		((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(TAG, ID_PROGRESS);
	}

	public synchronized void updateDoneNotification(Context ctx, FileJobInfo[] jobs)
	{
		extractOperationLists(jobs);

		if (finishedOperations.size() == 0)
		{
			return;
		}

		if (EventBus.getDefault().hasSubscriberForEvent(FileJobInfo[].class))
		{
			EventBus.getDefault().post(jobs);
			return;
		}

		Notification.Builder builder = new Notification.Builder(ctx);
		builder.setSmallIcon(R.drawable.notification_icon);
		builder.setContentTitle(ctx.getString(R.string.ntf_title_done));
		builder.setContentText(finishedOperations.size() == 1 ? finishedOperations.get(0).first.getPath().getName()
				: String.format(ctx.getString(R.string.ntf_number_of_files), finishedOperations.size()));
		builder.setContentIntent(buildOpenIntent(ctx));
		builder.setAutoCancel(true);
		NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(TAG, ID_DONE, builder.build());
	}

	public static ArrayList<Pair<PathStateInfo, Long>> getStateIdList(FileJobInfo[] jobs, int action, int state)
	{
		Map<Long, Integer> idActionMap = new HashMap<>();
		for (FileJobInfo job : jobs)
		{
			idActionMap.put(job.getCreated(), job.getAction());
		}
		List<Pair<PathStateInfo, Long>> stateIdList = new ArrayList<>();
		extractOperationListForState(jobs, state, stateIdList);

		ArrayList<Pair<PathStateInfo, Long>> files = new ArrayList<>();
		for (Pair<PathStateInfo, Long> stateId : stateIdList)
		{
			long id = stateId.second;
			if (idActionMap.get(id) == action)
			{
				files.add(stateId);
			}
		}
		return files;
	}

	public static boolean isUploadJob(FileJobInfo[] jobs, Pair<PathStateInfo, Long> stateId)
	{		
		for (FileJobInfo job : jobs)
		{
			if (stateId.second == job.getCreated()) {

				if (job.getAction() == BrowsingService.Contract.REQUEST_COPY) {
					Path dest = job.getDest();
					Path src = stateId.first.getPath();
					if ((src instanceof Path.Local) && !(dest instanceof Path.Local)) {
						// this is a upload job
						return true;
					}
				}
			}
		}

		return false;
	}
	
	public synchronized void updateErrorNotification(Context ctx, FileJobInfo[] jobs)
	{
		extractOperationLists(jobs);

		if (failedOperations.size() == 0)
		{
			return;
		}

		if (EventBus.getDefault().hasSubscriberForEvent(FileJobInfo[].class))
		{
			EventBus.getDefault().post(jobs);
			return;
		}

		Notification.Builder builder = new Notification.Builder(ctx);
		builder.setSmallIcon(R.drawable.notification_icon);
		builder.setContentTitle(ctx.getString(R.string.ntf_title_error));
		Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
		bigTextStyle.setBigContentTitle(ctx.getString(R.string.ntf_title_error));
		// StringBuilder copyStrBuilder = null;
		// StringBuilder moveStrBuilder = null;

		// for (Pair<PathStateInfo, Long> infoPair : failedOperations)
		// {
		// switch (jobinfos.get(infoPair.second).getAction())
		// {
		// case BrowsingService.Contract.REQUEST_COPY:
		// if (copyStrBuilder == null)
		// {
		// copyStrBuilder = new StringBuilder();
		// }
		// copyStrBuilder.append(infoPair.first.getPath().getName())
		// .append(", ");
		// break;
		// case BrowsingService.Contract.REQUEST_MOVE:
		// if (moveStrBuilder == null)
		// {
		// moveStrBuilder = new StringBuilder();
		// }
		// moveStrBuilder.append(infoPair.first.getPath().getName())
		// .append(", ");
		//
		// break;
		// default:
		// break;
		// }
		// }

		String copyFiles = buildFileList(failedOperations, BrowsingService.Contract.REQUEST_COPY);

		if (copyFiles != null)
		{
			buildErrorNotificationForAction(ctx, builder, bigTextStyle, BrowsingService.Contract.REQUEST_COPY,
					R.string.ntf_copying_failed, copyFiles);
		}

		String moveFiles = buildFileList(failedOperations, BrowsingService.Contract.REQUEST_MOVE);

		if (moveFiles != null)
		{
			buildErrorNotificationForAction(ctx, builder, bigTextStyle, BrowsingService.Contract.REQUEST_MOVE,
					R.string.ntf_moving_failed, moveFiles);
		}
	}

	public synchronized String buildFileList(List<Pair<PathStateInfo, Long>> toExtractFrom, int action)
	{
		StringBuilder builder = null;
		for (Pair<PathStateInfo, Long> infoPair : failedOperations)
		{
			if (jobinfos.get(infoPair.second).getAction() == action)
			{
				if (builder == null)
				{
					builder = new StringBuilder();
				}
				builder.append(infoPair.first.getPath().getName()).append(", ");
			}
		}
		return builder != null ? builder.substring(0, builder.length() - 2) : null;
	}

	private void buildErrorNotificationForAction(Context ctx, Notification.Builder builder,
			Notification.BigTextStyle bigTextStyle, int action, int actionStrRes, String fileList)
	{
		String msg = String.format(ctx.getString(actionStrRes), fileList);
		builder.setContentText(msg);
		builder.setAutoCancel(true);
		NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
		bigTextStyle.bigText(msg);
		builder.setStyle(bigTextStyle);
		nm.notify(buildFailedTag(action), ID_FAILED, builder.build());
	}

	private void buildSmallNotification(Context ctx, Notification.Builder builder,
			List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations,
			List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations,
			boolean multipleOngoingNoProgress)
	{
		builder.setSmallIcon(R.drawable.notification_icon);
		builder.setContentTitle(getActionString(ctx, ongoingOperations.get(0).second));
		builder.setContentText(ongoingOperations.size() == 1 ? ongoingOperations.get(0).first.getPath().getName()
				: String.format(ctx.getString(R.string.ntf_number_of_files), ongoingOperations.size()));
		builder.setContentInfo(activeOperations.size() + " pending");

		PathStateInfo infoToEvaluate = ongoingOperations.get(0).first;
		int percent = infoToEvaluate.getProgressPercent();
		builder.setProgress(100, percent,
				percent == 0 || multipleOngoingNoProgress);
	}

	private void buildLargeNotification(Context ctx, Notification notification,
			List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations,
			List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations,
			boolean multipleOngoingNoProgress)
	{
		RemoteViews rv = new RemoteViews(ctx.getPackageName(), R.layout.prg_big_notification);

		rv.setTextViewText(R.id.txt_action, getActionString(ctx, ongoingOperations.get(0).second));

		PathStateInfo infoToEvaluate = ongoingOperations.get(0).first;
		int percent = infoToEvaluate.getProgressPercent();
		rv.setProgressBar(R.id.prg_progress, 100, ongoingOperations.get(0).first.getProgressPercent(),
				percent == 0 || multipleOngoingNoProgress);
		rv.setTextViewText(R.id.txt_percent, percent > 0 ? percent + "%" : "");

		rv.setTextViewText(R.id.txt_ongoing, ongoingOperations.size() == 1 ? ongoingOperations.get(0).first.getPath()
				.getName()
				: ongoingOperations.size() + " files");

		int[] containerIds = new int[] {R.id.rl_pending0, R.id.rl_pending1, R.id.rl_pending2, R.id.rl_pending3,
				R.id.rl_pending4};
		int[] tvIds = new int[] {R.id.txt_pending0, R.id.txt_pending1, R.id.txt_pending2, R.id.txt_pending3,
				R.id.txt_pending4};
		for (int i = 0; i < tvIds.length; i++)
		{
			if (activeOperations.size() > i)
			{
				rv.setViewVisibility(containerIds[i], View.VISIBLE);
				rv.setTextViewText(tvIds[i], activeOperations.get(i).first.getPath().getName());
			}
			else
			{
				rv.setViewVisibility(containerIds[i], View.GONE);
			}
		}
		int cantBeDisplayed = activeOperations.size() - 5;
		if (cantBeDisplayed > 0)
		{
			rv.setViewVisibility(R.id.txt_pendingN, View.VISIBLE);
			rv.setTextViewText(R.id.txt_pendingN,
					String.format(ctx.getString(R.string.ntf_more_files), cantBeDisplayed));
		}
		else
		{
			rv.setViewVisibility(R.id.txt_pendingN, View.GONE);
		}

		rv.setOnClickPendingIntent(R.id.btn_cancel, buildCancelIntent(ctx));

		notification.bigContentView = rv;
	}

	private void extractOperationLists(FileJobInfo[] jobs)
	{
		jobinfos.clear();
		for (FileJobInfo info : jobs)
		{
			jobinfos.put(info.getCreated(), info);
		}
		extractOperationListForState(jobs, FileJobInfo.STATE_ACTIVE, ongoingOperations);
		extractOperationListForState(jobs, FileJobInfo.STATE_QUEUED, activeOperations);
		extractOperationListForState(jobs, FileJobInfo.STATE_FINISHED, finishedOperations);
		extractOperationListForState(jobs, FileJobInfo.STATE_ERROR, failedOperations);
	}

	private static void extractOperationListForState(FileJobInfo[] jobs, int state,
			List<Pair<PathStateInfo, Long>> toPopulate)
	{
		toPopulate.clear();
		toPopulate.addAll(FileJobInfo.getPathStateInfoIdPairListForState(state, jobs));
	}

	private String getActionString(Context ctx, long jobId)
	{
		switch (jobinfos.get(jobId).getAction())
		{
			case BrowsingService.Contract.REQUEST_COPY:
				return ctx.getString(R.string.ntf_copying);
			case BrowsingService.Contract.REQUEST_MOVE:
				return ctx.getString(R.string.ntf_moving);
			default:
				return "";
		}
	}

	private PendingIntent buildCancelIntent(Context ctx)
	{
		return PendingIntent.getService(ctx, 0, BrowsingService.Contract.buildCancelCopyMoveIntent(ctx), 0);
	}

	private PendingIntent buildOpenIntent(Context ctx)
	{
		Intent intent = new Intent(ctx, InitializationActivity.class);

		boolean singleDest = true;
		Iterator<Pair<PathStateInfo, Long>> iterator = finishedOperations.iterator();
		String pathStr = null;
		String nextPathStr;

		while (iterator.hasNext() && singleDest)
		{
			nextPathStr = jobinfos.get(iterator.next().second).getDest().toString();
			singleDest = pathStr == null || pathStr.equals(nextPathStr);
			pathStr = nextPathStr;
		}

		if (singleDest)
		{
			intent.setAction(InitializationActivity.ACTION_OPEN_PATH);
			Path dest = jobinfos.get(finishedOperations.get(0).second).getDest();
			intent.putExtra(InitializationActivity.EXTRA_PATH, dest);
		}
		else
		{
			intent.addCategory(Intent.CATEGORY_LAUNCHER);
			intent.setAction(Intent.ACTION_MAIN);
		}
		return PendingIntent.getActivity(ctx, 0, intent, Intent.FLAG_ACTIVITY_CLEAR_TOP, null);
	}

	private String buildFailedTag(int action)
	{
		return TAG + action;
	}

}
