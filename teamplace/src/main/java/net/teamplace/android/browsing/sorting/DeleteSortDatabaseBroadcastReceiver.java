package net.teamplace.android.browsing.sorting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

public class DeleteSortDatabaseBroadcastReceiver extends BroadcastReceiver
{
	public static final String ACTION_DELETE_SORT_DATABASE = "net.teamplace.android.ACTION_DELETE_SORT_DATABASE";

	@Override
	public void onReceive(Context context, Intent intent)
	{
		String action = intent.getAction();

		if (action.equals(ACTION_DELETE_SORT_DATABASE))
		{
			// Forces closing the database, so that we don't delete a database in use
			SortDatabaseHelper databaseHelper = SortDatabaseHelper.getInstance(context);
			forceCloseDatabase(databaseHelper);
			context.deleteDatabase(databaseHelper.getDatabaseName());
		}
	}

	private void forceCloseDatabase(SortDatabaseHelper databaseHelper)
	{
		SQLiteDatabase database = databaseHelper.getWritableDatabase();
		database.close();
	}
}
