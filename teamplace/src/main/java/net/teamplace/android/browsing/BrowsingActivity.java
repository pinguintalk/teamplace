package net.teamplace.android.browsing;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Pair;
import android.view.ActionMode;
import android.view.ActionMode.Callback;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;

import com.cortado.android.R;

import net.teamplace.android.application.App;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.navigation.Navigation;
import net.teamplace.android.browsing.navigation.NavigationFragment;
import net.teamplace.android.browsing.openin.OpenInActivity;
import net.teamplace.android.browsing.path.LocalPathNotRecognizedException;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.browsing.path.Path.Remote;
import net.teamplace.android.browsing.path.Path.TeamDrive;
import net.teamplace.android.browsing.path.PathOperation;
import net.teamplace.android.browsing.ui.ActionModeCallbackWrapper;
import net.teamplace.android.browsing.ui.BrowsingFragment;
import net.teamplace.android.browsing.ui.FavoritesFragment;
import net.teamplace.android.browsing.ui.NotificationHelper;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.util.GenericUtils;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.settings.SettingsFragment;
import net.teamplace.android.teamdrive.ui.TeamDriveSelectFragment;
import net.teamplace.android.utils.LoggingActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Main activity for browsing file. Takes care of the ActionBar, the NavigationDrawer, and loading animation
 *
 * @author Gil Vegliach
 */
public class BrowsingActivity extends LoggingActivity implements
		DrawerLayout.DrawerListener,
		Navigation.Callback
{
	private static final String PKG = pkg(BrowsingActivity.class);

	{
		TAG = tag(BrowsingActivity.class);
	}

	public static final String EXTRA_BROWSE_FROM_PATH = PKG + ".browseFromPath";

	private static final String KEY_IS_DRAWER_ACTIVE = PKG + ".isDrawerActive";
	private static final String KEY_SAVED_AB_TITLE = PKG + ".savedAbTitle";
	private static final String KEY_SAVED_AB_SUBTITLE = PKG + ".savedAbSubtitle";
	private static final String KEY_MESSAGES = PKG + ".shownMessages";

	/**
	 * True iff between DrawerOpened() and DrawerClosed(), retained through configuration changes
	 */
	private boolean mIsDrawerActive = false;

	// Used to preserve state of ActionBar when the navigation drawer slides in and out
	private String mSavedAbTitle;
	private String mSavedAbSubtitle;

	/** True iff this layout has the NavigationDrawer */
	private boolean mHasDrawer;
	private DrawerLayout.DrawerListener mDrawerObserver;
	private ActionBarDrawerToggle mDrawerToggle;
	private DrawerLayout mDrawerLayout;

	private ActionMode mActionMode;
	private final ArrayList<String> shownMessages = new ArrayList<String>();
	private static final Map<String, FeedbackToast> feedbackToasts = new HashMap<String, FeedbackToast>();

	@Override
	protected void onCreate(Bundle state)
	{
		super.onCreate(state);
		setContentView(R.layout.brw_activity);
		setUpDrawer();
		setUpActionBar();
		setUpContentAndNavigation(state);
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState)
	{
		super.onPostCreate(savedInstanceState);
		if (mDrawerToggle != null)
		{
			mDrawerToggle.syncState();
		}
	}

	@Override
	protected void onResume()
	{
		super.onResume();
		//TODO TEMPORARY workaround for TEP-1305. Remove ASAP
		if (new SessionManager(this).getLastSession() == null) {
			Intent intent = new Intent(this, InitializationActivity.class);
			intent.setAction(Intent.ACTION_DEFAULT);
			AlarmManager alMgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			alMgr.set(AlarmManager.RTC, 100,
					PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
			System.exit(0);
		}
		
		App.setRootActivityHasSavedState(false);
	}


	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putBoolean(KEY_IS_DRAWER_ACTIVE, mIsDrawerActive);
		outState.putString(KEY_SAVED_AB_TITLE, mSavedAbTitle);
		outState.putString(KEY_SAVED_AB_SUBTITLE, mSavedAbSubtitle);
		outState.putStringArrayList(KEY_MESSAGES, shownMessages);
		App.setRootActivityHasSavedState(true);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState)
	{
		super.onRestoreInstanceState(savedInstanceState);

		if (savedInstanceState == null)
		{
			return;
		}

		mIsDrawerActive = savedInstanceState.getBoolean(KEY_IS_DRAWER_ACTIVE);
		mSavedAbTitle = savedInstanceState.getString(KEY_SAVED_AB_TITLE);
		mSavedAbSubtitle = savedInstanceState.getString(KEY_SAVED_AB_SUBTITLE);

		shownMessages.addAll(savedInstanceState.getStringArrayList(KEY_MESSAGES));

		if (mSavedAbTitle != null || mSavedAbSubtitle != null)
		{
			if (mIsDrawerActive)
			{
				ActionBar ab = getActionBar();
				ab.setTitle(R.string.app_name);
				ab.setSubtitle(null);
			}
			else
			{
				restoreActionBarStyle();
			}
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		if (mDrawerToggle != null) {
			mDrawerToggle.onConfigurationChanged(newConfig);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		// If the navigation drawer is open, hide action items related to the content view
		if (mHasDrawer)
		{
			DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawer_layout);
			View drawerList = findViewById(R.id.navigation);
			boolean drawerOpen = dl.isDrawerOpen(drawerList);

			for (int sz = menu.size(), i = 0; i < sz; i++)
			{
				menu.getItem(i).setVisible(!drawerOpen);
			}
		}
		FragmentManager fm = getFragmentManager();
		Fragment fragment = fm.findFragmentById(R.id.container);

		MenuItem mi = menu.findItem(R.id.action_search);
		if (mi != null) {

			SearchView searchView = (SearchView) mi.getActionView();
			if (searchView != null) {
				String currSearchString = "";
				if(fragment instanceof BrowsingFragment) {
					BrowsingFragment  browsingFragment = (BrowsingFragment) fragment;
					currSearchString = browsingFragment.getCurrFilterString();
				} else if (fragment instanceof FavoritesFragment) {
					FavoritesFragment  favoritesFragment = (FavoritesFragment) fragment;
					currSearchString = favoritesFragment.getmCurrentFilterText();
				}
				searchView.setQuery(currSearchString, false);
			}
		}
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (mDrawerToggle != null && mDrawerToggle.onOptionsItemSelected(item))
		{
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDrawerOpened(View drawerView)
	{
		if (mDrawerToggle != null)
		{
			mDrawerToggle.onDrawerOpened(drawerView);
		}

		mIsDrawerActive = true;

		hideKeyboard(this, drawerView);

		invalidateOptionsMenu();

		if (mDrawerObserver != null)
		{
			mDrawerObserver.onDrawerOpened(drawerView);
		}

		saveAndSetAppActionBarStyle();
	}

	@Override
	public void onDrawerClosed(View drawerView)
	{
		if (mDrawerToggle != null)
		{
			mDrawerToggle.onDrawerClosed(drawerView);
		}

		mIsDrawerActive = false;

		invalidateOptionsMenu();

		restoreActionBarStyle();

		if (mDrawerObserver != null)
		{
			mDrawerObserver.onDrawerClosed(drawerView);
		}
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset)
	{
		if (mDrawerToggle != null)
		{
			mDrawerToggle.onDrawerSlide(drawerView, slideOffset);
		}

		if (mDrawerObserver != null)
		{
			mDrawerObserver.onDrawerSlide(drawerView, slideOffset);
		}
	}

	@Override
	public void onDrawerStateChanged(int newState)
	{
		if (mDrawerToggle != null)
		{
			mDrawerToggle.onDrawerStateChanged(newState);
		}

		if (mDrawerObserver != null)
		{
			mDrawerObserver.onDrawerStateChanged(newState);
		}
	}

	@Override
	public void onNavigationTopLevelSelected(Navigation.TopLevel level)
	{
		Fragment frag = buildFragmentFromNavigationTopLevel(level);
		setContentFragment(frag);
		finishActionModeIfNeedBe();
		closeDrawerIfNeedBe();
	}

	// TODO @giveg: change name to this and make it non-static...
	public static void browseAndPush(FragmentManager fm, Path browsable)
	{
		checkNonNullArgs(fm, browsable);
		Fragment frag = BrowsingFragment.newInstance(browsable);
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container, frag);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE);
		ft.addToBackStack(null);
		ft.commit();
	}

	/** Register an observer to receive callbacks when Drawer events happen. Call it onResume() */
	public void registerDrawerObserver(DrawerLayout.DrawerListener listener)
	{
		mDrawerObserver = listener;
	}

	/** Unregister the previous registered observer. Call it onPause() */
	public void unregisterDrawerObserver()
	{
		mDrawerObserver = null;
	}

	public void setActionBarTitle(int resId)
	{
		setActionBarTitle(getString(resId));
	}

	public void setActionBarSubtitle(int resId)
	{
		setActionBarSubtitle(getString(resId));
	}

	public void setActionBarTitle(String title)
	{
		if (mIsDrawerActive)
		{
			mSavedAbTitle = title;
		}
		else
		{
			ActionBar ab = getActionBar();
			ab.setTitle(title);
		}
	}

	public void setActionBarSubtitle(String subtitle)
	{
		if (mIsDrawerActive)
		{
			mSavedAbSubtitle = subtitle;
		}
		else
		{
			ActionBar ab = getActionBar();
			ab.setSubtitle(subtitle);
		}
	}

	@Override
	public ActionMode startActionMode(Callback callback)
	{
		// Watches ActionMode callbacks in order to manage a reference to the ActionMode and finish it if
		// appropriate (e.g. most of the ActionMode handling is done inside BrowsingFragment, but onViewSelected()
		// needs to finish it from the outside when the user changes perspective, e.g. local to remote browsing)
		ActionMode.Callback wrapper = new ActionModeCallbackWrapper(callback)
		{
			@Override
			public void onDestroyActionMode(ActionMode mode)
			{
				super.onDestroyActionMode(mode);
				mActionMode = null;
			}

		};
		mActionMode = super.startActionMode(wrapper);
		return mActionMode;
	}

	private void setUpDrawer()
	{
		mHasDrawer = findViewById(R.id.drawer_layout) != null;
		if (!mHasDrawer)
		{
			return;
		}

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.brw_ab_drawer, R.string.brw_drawer_open,
				R.string.brw_drawer_close);
		mDrawerLayout.setDrawerListener(this);
	}

	private void setUpActionBar()
	{
		ActionBar ab = getActionBar();
		// On API 2.3 the logo is not shown automatically
		ab.setLogo(R.drawable.app_logo);
		ab.setDisplayHomeAsUpEnabled(mHasDrawer);
		ab.setHomeButtonEnabled(mHasDrawer);
		ab.setDisplayShowTitleEnabled(true);
	}

	private void setUpContentAndNavigation(Bundle state)
	{
		if (state == null)
		{
			initContentAndNavigation();
		}

		wireContentAndNavigation();
	}

	private void wireContentAndNavigation()
	{
		FragmentManager fm = getFragmentManager();
		Navigation navigation = (NavigationFragment) fm.findFragmentById(R.id.navigation);
		navigation.setNavigationCallback(this);
	}

	private void initContentAndNavigation()
	{
		Path startingPath = getIntent().getParcelableExtra(EXTRA_BROWSE_FROM_PATH);
		String strOpenInPath = getIntent().getStringExtra(OpenInActivity.OPEN_IN_EXTRA);
		if (strOpenInPath != null)
		{
			// Start BrowsingFragment which contains the open-in logic.
				try
				{
					startingPath = Path.Local.fromAbsolutePath(strOpenInPath).getParentPath();
				}
				catch (LocalPathNotRecognizedException e)
				{
					startingPath = Path.Local.localRoot();
				}
		}
		if (startingPath != null)
		{
			initBrowsingUiFromPath(startingPath);
		}
		else
		{
			initTeamplacesUi();
		}
	}

	private void initTeamplacesUi()
	{
		Fragment frag = TeamDriveSelectFragment.newInstance();
		setContentFragment(frag);
		selectNavigationTopLevel(Navigation.TopLevel.TEAMDRIVES);
	}

	private void initBrowsingUiFromPath(Path path)
	{
		FragmentManager fm = getFragmentManager();
		browseFromRoot(fm, path);
		Navigation.TopLevel topLevel = topLevelFromPath(path);
		selectNavigationTopLevel(topLevel);
	}

	private void setContentFragment(Fragment frag)
	{
		// Pops the whole back stack leaving only the root fragment
		FragmentManager fm = getFragmentManager();
		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

		// New root fragment
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container, frag);
		ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		ft.commit();
	}

	private void selectNavigationTopLevel(Navigation.TopLevel topLevel)
	{
		FragmentManager fm = getFragmentManager();
		Navigation navigation = (NavigationFragment) fm.findFragmentById(R.id.navigation);
		navigation.select(topLevel);
	}

	public static void browseFromRoot(FragmentManager fm, Path browsable)
	{
		// Unroll path on a stack
		Path curr = browsable;
		Stack<Path> stack = new Stack<Path>();
		while (!curr.isRoot())
		{
			stack.push(curr);
			curr = curr.getParentPath();
		}

		// Clear back stack, push root on it without saving the transaction; similar to setContentFragment, but no
		// animation
//		fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//		navigateToEntryPoint(fm);

		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container, BrowsingFragment.newInstance(curr));
		ft.addToBackStack(null);
		ft.commit();

		// Push the rest
		while (!stack.isEmpty())
		{
			curr = stack.pop();
			ft = fm.beginTransaction();
			ft.replace(R.id.container, BrowsingFragment.newInstance(curr));
			ft.addToBackStack(null);
			ft.commit();
		}
	}

	public static void navigateFromEntryPoint(FragmentManager fm, Fragment toPush)
	{
		navigateToEntryPoint(fm);
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container, toPush);
		ft.addToBackStack(null);
		ft.commit();
	}

	public static void navigateToEntryPoint(FragmentManager fm) {
		if (fm.getBackStackEntryCount() > 0){
			int rootFragId = fm.getBackStackEntryAt(0).getId();
			fm.popBackStack(rootFragId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
	}

	private static Navigation.TopLevel topLevelFromPath(Path path)
	{
		PathOperation<Navigation.TopLevel> op = new PathOperation<Navigation.TopLevel>()
		{
			@Override
			public Navigation.TopLevel execute(Local path)
			{
				return Navigation.TopLevel.LOCAL;
			}

			@Override
			public Navigation.TopLevel execute(Remote path)
			{
				return Navigation.TopLevel.REMOTE;
			}

			@Override
			public Navigation.TopLevel execute(TeamDrive path)
			{
				return Navigation.TopLevel.TEAMDRIVES;
			}
		};
		return path.execute(op);
	}

	private static Fragment buildFragmentFromNavigationTopLevel(Navigation.TopLevel topLevel)
	{
		switch (topLevel)
		{
			case FAVORITES:
				return FavoritesFragment.newInstance();
			case REMOTE:
				return BrowsingFragment.newInstance(Path.remoteRoot());
			case TEAMDRIVES:
				return TeamDriveSelectFragment.newInstance();
			case LOCAL:
				return BrowsingFragment.newInstance(Path.localRoot());
			case SETTINGS:
				return SettingsFragment.newInstance();
			default:
				throw new IllegalArgumentException("Not implemented: " + topLevel);
		}
	}

	private void saveAndSetAppActionBarStyle()
	{
		ActionBar ab = getActionBar();
		CharSequence temp = ab.getTitle();
		mSavedAbTitle = temp == null ? null : temp.toString();

		temp = ab.getSubtitle();
		mSavedAbSubtitle = temp == null ? null : temp.toString();
		ab.setTitle(R.string.app_name);
		ab.setSubtitle(null);
	}

	private void restoreActionBarStyle()
	{
		ActionBar ab = getActionBar();
		ab.setTitle(mSavedAbTitle);
		ab.setSubtitle(mSavedAbSubtitle);
		mSavedAbTitle = null;
		mSavedAbSubtitle = null;
	}

	private void finishActionModeIfNeedBe()
	{
		if (mActionMode != null)
		{
			mActionMode.finish();
		}
	}

	public void onEventMainThread(FileJobInfo[] event)
	{
		List<FileJobInfo> doneJobs = new ArrayList<FileJobInfo>();

		for (FileJobInfo info : event)
		{
			if (info.allFinishedOrFailed())
			{
				doneJobs.add(info);
			}
		}
		FileJobInfo[] doneJobArr = Arrays.copyOf(doneJobs.toArray(), doneJobs.size(), FileJobInfo[].class);

		if (doneJobArr.length > 0) {
			// failed copy operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_copying_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_COPY, FileJobInfo.STATE_ERROR);

			// failed move operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_moving_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_MOVE, FileJobInfo.STATE_ERROR);

			// failed delete operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_delete_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_DELETE, FileJobInfo.STATE_ERROR);

			// failed mkdir operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_create_folder_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_MKDIR, FileJobInfo.STATE_ERROR);

			// failed rename operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_rename_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_RENAME, FileJobInfo.STATE_ERROR);

			// failed export to ZIP operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_zip_export_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_EXPORT_TO_ZIP, FileJobInfo.STATE_ERROR);

			// failed export to PDF operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_pdf_export_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_EXPORT_TO_PDF, FileJobInfo.STATE_ERROR);

			// failed to browse operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.delete, R.drawable.brw_error_circle,
					getString(R.string.ntf_browse_failed),
					getString(R.string.ntf_title_error),
					BrowsingService.Contract.REQUEST_BROWSE, FileJobInfo.STATE_ERROR);

			// successful copy operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.checkmark, R.drawable.brw_success_circle,
					"%1$s",
					getString(R.string.ntf_title_done),
					BrowsingService.Contract.REQUEST_COPY, FileJobInfo.STATE_FINISHED);

			// successful move operations
			showFeedbackToastIfNeeded(doneJobArr, R.drawable.checkmark, R.drawable.brw_success_circle,
					"%1$s",
					getString(R.string.ntf_title_done),
					BrowsingService.Contract.REQUEST_MOVE, FileJobInfo.STATE_FINISHED);

			showFeedbackToastIfNeeded(doneJobArr, R.drawable.checkmark, R.drawable.brw_success_circle,
					getString(R.string.ntf_pdf_export_still_processing),
					getString(R.string.ntf_title_export_pdf),
					BrowsingService.Contract.REQUEST_EXPORT_TO_PDF, FileJobInfo.STATE_FINISHED);
		}

	}

	private void showFeedbackToastIfNeeded(FileJobInfo[] jobs, int iconRes, int iconBg, String msgTemplate,
			String title, int action, int state)
	{
		List<Pair<PathStateInfo, Long>> stateIdList = NotificationHelper.getStateIdList(jobs, action, state);
		List<Pair<PathStateInfo, Long>> notShownStateIdList = extractNotShownPairs(stateIdList);

		if (notShownStateIdList.size() == 0)
		{
			return;
		}

		addToShownPairs(notShownStateIdList);

		String msg = buildToastMessage(jobs, msgTemplate, notShownStateIdList, state);
		FeedbackToast feedbackToast = retrieveOrCreateFeedbackToast(action, state);
		feedbackToast.setIconBgRes(iconBg);
		feedbackToast.setIconRes(iconRes);
		feedbackToast.setTitle(title);
		feedbackToast.setMessage(msg);
		feedbackToast.show();
	}

	// this method is extended to show correct messages while canceling file upload process
	// on the next release new STATE_CANCEL will added
	private String buildToastMessage(FileJobInfo[] jobs, String msgTemplate,
			List<Pair<PathStateInfo, Long>> stateIdList, int state)
	{
		boolean isUploadJob = false;
		try
		{
			StringBuilder sb = new StringBuilder();
			int size = stateIdList.size();
			for (int i = 0; i < size; i++)
			{
				Pair<PathStateInfo, Long> stateId = stateIdList.get(i);
				PathStateInfo pathState = stateId.first;
				String pathName = pathState.getPath().getName();
				sb.append(pathName);

				if (i < size - 1)
				{
					sb.append(", ");
				}

				if (NotificationHelper.isUploadJob(jobs, stateId))
				{
					isUploadJob = true;
				}
			}

			String fileList = sb.toString();
			if (isUploadJob && (state == FileJobInfo.STATE_ERROR))
			{
				msgTemplate = getString(R.string.ntf_uploading_failed);
			}
			return String.format(msgTemplate, fileList);

		}
		catch (IllegalFormatException e)
		{
			// if template String has no placeholder, return template String
			return msgTemplate;
		}
	}

	private List<Pair<PathStateInfo, Long>> extractNotShownPairs(List<Pair<PathStateInfo, Long>> stateIdList)
	{
		ArrayList<Pair<PathStateInfo, Long>> result = new ArrayList<>();
		for (Pair<PathStateInfo, Long> stateId : GenericUtils.emptyIfNull(stateIdList))
		{
			String id = messageIdString(stateId);
			if (!shownMessages.contains(id))
			{
				result.add(stateId);
			}
		}
		return result;
	}

	private void addToShownPairs(List<Pair<PathStateInfo, Long>> stateIdList)
	{
		for (Pair<PathStateInfo, Long> stateId : GenericUtils.emptyIfNull(stateIdList))
		{
			String id = messageIdString(stateId);
			shownMessages.add(id);
		}
	}

	private FeedbackToast retrieveOrCreateFeedbackToast(int action, int state)
	{
		String key = action + "" + state;
		FeedbackToast feedbackToast = feedbackToasts.get(key);
		if (feedbackToast == null)
		{
			feedbackToast = new FeedbackToast(this);
			feedbackToasts.put(key, feedbackToast);
		}
		return feedbackToast;
	}

	// TODO: @jobol Pair<PathStateInfo, Long> should be made a class and this method moved to it
	private static String messageIdString(Pair<PathStateInfo, Long> pair)
	{
		PathStateInfo pathStateInfo = pair.first;
		long fileJobInfoId = pair.second;
		return pathStateInfo.getPath().toString() + "\\|" + fileJobInfoId + "\\|" + pathStateInfo.getState();
	}

	// Closes the drawer. The action bar title will be refreshed once the drawer is fully closed
	private void closeDrawerIfNeedBe()
	{
		if (mHasDrawer && mIsDrawerActive)
		{
			DrawerLayout dl = (DrawerLayout) findViewById(R.id.drawer_layout);
			dl.closeDrawers();
		}
	}

	public static void hideKeyboard(Context context, View view)
	{
		InputMethodManager inputMethodManager = (InputMethodManager) context
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}
}
