package net.teamplace.android.browsing.openin;

import android.content.Intent;
import android.net.Uri;

public class ContentHelper
{
	public static final int STRATEGY_DATA_URI = 0;
	public static final int STRATEGY_STREAM_EXTRA = 1;

	public static Intent setFilePathContent(Intent in, String filePath, int contentStrategy)
	{
		switch (contentStrategy)
		{
			case STRATEGY_DATA_URI:
				in.setDataAndType(Uri.parse("file://" + filePath), in.getType());
				break;
			case STRATEGY_STREAM_EXTRA:
				in.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + filePath));
				break;
			default:
				throw new IllegalArgumentException("contentStrategy " + contentStrategy + " does not exist.");
		}
		return in;
	}

}
