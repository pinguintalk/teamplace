package net.teamplace.android.browsing.share;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.cortado.android.R;

public class SharePagerAdapter extends FragmentPagerAdapter
{
	private final String TAG = tag(this.getClass());

	private Context context;
	private List<Fragment> fragments = new ArrayList<Fragment>();

	public SharePagerAdapter(Context context, FragmentManager fm, FileInfo shareFile, boolean forbiddenFile,
			boolean forbiddenFileType)
	{
		super(fm);

		this.context = context;

		fragments.add(ShareAttachmentFragment.newInstance(shareFile, forbiddenFile, forbiddenFileType));
		fragments.add(ShareLinkFragment.newInstance(shareFile, false));
		fragments.add(ShareLinkFragment.newInstance(shareFile, true));
		fragments.add(ShareShowLinkFragment.newInstance(shareFile));
	}

	@Override
	public Fragment getItem(int position)
	{
		return fragments.get(position);
	}

	@Override
	public int getCount()
	{
		return fragments.size();
	}

	@Override
	public CharSequence getPageTitle(int position)
	{
		switch (position)
		{
			case 0:
				return context.getString(R.string.shr_tab_attachment);
			case 1:
				return context.getString(R.string.shr_tab_secure_link);
			case 2:
				return context.getString(R.string.shr_tab_public_link);
			case 3:
				return context.getString(R.string.shr_tab_show_link);
			default:
				return null;
		}
	}
}
