package net.teamplace.android.browsing.path;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.File;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.Queue;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.teamplace.android.utils.Log;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;


/**
 * Abstract representation of a pathname, similar to java.io.File.File but cannot create files or directories. The root
 * folder path has name "" and parent "", so mRoot.getParent() returns "" and mRoot.getParentPath() returns mRoot again.
 * Differently from File, the method getParent() will be never null.
 * 
 * @author Gil Vegliach
 */
abstract public class Path implements Parcelable
{
	private static final String TAG = tag(Path.class);

	public static final String LOCAL_LOCATION = "local";
	public static final String REMOTE_LOCATION = "remote";
	public static final String TEAMDRIVE_LOCATION_PREFIX = "teamdrive";

	public static final String SEPARATOR = File.separator;
	public static final String LOCATION_PATH_SEPARATOR = ":";
	public static final String TEAMDRIVE_ID_SEPARATOR = LOCATION_PATH_SEPARATOR;

	private static final Path.Local LOCAL_ROOT = new Local("");
	private static final Path.Remote REMOTE_ROOT = new Remote("");
	private static final String ROOT_NAME = "";
	private static final Pattern TEAMDRIVE_ID_PATTERN = Pattern.compile("[a-f\\d]{8}(-[a-f\\d]{4}){3}-[a-f\\d]{12}",
			Pattern.CASE_INSENSITIVE);

	private final String mLocation;
	private final File mPathname;

	public static Path.Local localRoot()
	{
		return LOCAL_ROOT;
	}

	public static Path.Remote remoteRoot()
	{
		return REMOTE_ROOT;
	}

	public static Path.TeamDrive teamDriveRoot(String teamDriveId)
	{
		checkTeamDriveId(teamDriveId);
		return new TeamDrive(teamDriveId, "");
	}

	public static String teamDriveLocation(String teamDriveId)
	{
		checkTeamDriveId(teamDriveId);
		return teamDriveRoot(teamDriveId).getLocation();
	}

	public static Path valueOf(Path parent, String name)
	{
		checkNonNullArg(parent);
		checkValidName(name);

		String pathname;
		if (!"".equals(parent.mPathname.getPath()))
		{
			pathname = parent.mPathname.getPath() + File.separator + name;
		}
		else
		{
			pathname = name;
		}

		String loc = parent.getLocation();
		return Path.valueOf(loc, pathname);
	}

	public static boolean isTeamDriveLocation(String location)
	{
		return location != null && location.startsWith(TEAMDRIVE_LOCATION_PREFIX);
	}

	public static boolean isLocalLocation(String location)
	{
		return LOCAL_LOCATION.equals(location);
	}

	public static boolean isRemoteLocation(String location)
	{
		return REMOTE_LOCATION.equals(location);
	}

	public String getName()
	{
		return mPathname.getName();
	}

	public String getLocation()
	{
		return mLocation;
	}

	// TODO : change this if you change getParentPath()
	public boolean isRoot()
	{
		return getParentPath().equals(this);
	}

	// TODO: should return null on roots
	public Path getParentPath()
	{
		return valueOf(mLocation, getParentPathString());
	}

	/** Execute a PathOperation on this Path */
	abstract public <T> T execute(PathOperation<? extends T> op);

	/**
	 * Returns a {@link Builder} object to simplify Path construction
	 */
	public Builder buildUpon()
	{
		return new Builder(this);
	}

	/**
	 * Simplifies Path construction
	 */
	public static class Builder
	{
		private final Path mRoot;
		private final Queue<String> mSegments = new ArrayDeque<String>();

		private Builder(Path root)
		{
			mRoot = root;
		}

		public Builder addSegment(String segment)
		{
			checkValidName(segment);
			mSegments.offer(segment);
			return this;
		}

		public Path build()
		{
			Path p = mRoot;
			while (!mSegments.isEmpty())
			{
				p = Path.valueOf(p, mSegments.poll());
			}
			return p;
		}
	}

	// Used only by subclasses. Bear in mind that this class is abstract and can
	// be instantiated only through a subclass
	private Path(String location, String pathname)
	{
		if (pathname == null)
		{
			pathname = "";
		}

		mLocation = location;
		mPathname = new File(pathname);
	}

	// TODO: @giveg add check if name contains slashes!!!
	private static void checkValidName(String name)
	{
		checkNonNullArg(name);
		// slash check goes here! What else can go wrong?
	}

	private static void checkTeamDriveId(String teamDriveId)
	{
		checkNonNullArg(teamDriveId);
		Matcher m = TEAMDRIVE_ID_PATTERN.matcher(teamDriveId);
		if (!m.matches())
		{
			throw new IllegalArgumentException("Wrong format for teamDriveId, was: " + teamDriveId);
		}
	}

	// Assumes location is well formatted
	private static String getTeamDriveIdFromLocation(String location)
	{
		return location.substring(TEAMDRIVE_LOCATION_PREFIX.length() + 1);
	}

	private String getParentPathString()
	{
		String parent = mPathname.getParent();
		if (parent == null)
		{
			return ROOT_NAME;
		}

		return parent;
	}

	/* package */File getPathFile()
	{
		return mPathname;
	}

	/* package */static Path valueOf(String location, String pathname)
	{
		if (location == null || pathname == null)
		{
			throw new IllegalArgumentException("location and pathname must not be null");
		}

		if (LOCAL_LOCATION.equals(location))
		{
			return new Local(pathname);
		}

		if (REMOTE_LOCATION.equals(location))
		{
			return new Remote(pathname);
		}

		if (isTeamDriveLocation(location))
		{
			String teamDriveId = getTeamDriveIdFromLocation(location);
			checkTeamDriveId(teamDriveId);
			return new TeamDrive(teamDriveId, pathname);
		}

		throw new IllegalArgumentException("location not recognized. Use constants in Path. Was: " + location);
	}

	@Override
	public boolean equals(Object o)
	{
		if (o == null)
		{
			return false;
		}

		if (o == this)
		{
			return true;
		}

		if (!(o instanceof Path))
		{
			return false;
		}

		Path path = (Path) o;
		return path.getName().equals(getName()) && path.getParentPathString().equals(getParentPathString())
				&& path.mLocation.equals(mLocation);
	}

	@Override
	public int hashCode()
	{
		return (mLocation + ":" + getParentPathString() + File.separator + getName()).hashCode();
	}

	/**
	 * Returns a human-readable representation of a Path. The format follows these examples: <code>
	 <pre>
		local:/
		local:/Untitled folder
		local:/myfolder/myfile.jpg
		remote:/Folder/img.png
		teamdrive:919f0f79-5949-47f7-8740-a165429fe5fe:/photos
	  </pre>
	  </code>
	 */
	@Override
	public String toString()
	{
		// E.g. remote:/Folder/img.jpg
		return mLocation + LOCATION_PATH_SEPARATOR + SEPARATOR + mPathname.getPath();
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(mLocation);
		dest.writeString(mPathname.getPath());
	}

	// TODO: @giveg: what about this code? Aidl won't work otherwise...
	// public static final Parcelable.Creator<Path> CREATOR = new Parcelable.Creator<Path>()
	// {
	// @Override
	// public Path createFromParcel(Parcel in)
	// {
	// String location = in.readString();
	// String pathname = in.readString();
	// Path path = valueOf(location, pathname);
	// return path;
	// }
	//
	// @Override
	// public Path[] newArray(int size)
	// {
	// return new Path[size];
	// }
	// };

	private Path(Parcel in)
	{
		this(in.readString(), in.readString());
	}

	final public static class Remote extends Path
	{
		private Remote(String pathname)
		{
			super(REMOTE_LOCATION, pathname);
		}

		private Remote(Parcel in)
		{
			super(in);
		}

		@Override
		public <T> T execute(PathOperation<? extends T> op)
		{
			return op.execute(this);
		}

		public static final Parcelable.Creator<Remote> CREATOR = new Parcelable.Creator<Remote>()
		{
			@Override
			public Remote createFromParcel(Parcel in)
			{
				return new Remote(in);
			}

			@Override
			public Remote[] newArray(int size)
			{
				return new Remote[size];
			}
		};
	}

	final public static class TeamDrive extends Path
	{
		final private String mTeamDriveId;

		private TeamDrive(String teamDriveId, String pathname)
		{
			super(TEAMDRIVE_LOCATION_PREFIX + ":" + teamDriveId, pathname);
			mTeamDriveId = teamDriveId;
		}

		private TeamDrive(Parcel in)
		{
			super(in);
			mTeamDriveId = in.readString();
		}

		public String getTeamDriveId()
		{
			return mTeamDriveId;
		}

		@Override
		public <T> T execute(PathOperation<? extends T> op)
		{
			checkNonNullArg(op);
			return op.execute(this);
		}

		@Override
		public void writeToParcel(Parcel out, int flags)
		{
			super.writeToParcel(out, flags);
			out.writeString(mTeamDriveId);
		}

		public static final Parcelable.Creator<TeamDrive> CREATOR = new Parcelable.Creator<TeamDrive>()
		{
			@Override
			public TeamDrive createFromParcel(Parcel in)
			{
				return new TeamDrive(in);
			}

			@Override
			public TeamDrive[] newArray(int size)
			{
				return new TeamDrive[size];
			}
		};
	}

	// Warning related to LOCAL_ROOT_ALIASES, but it is not possible to suppress warning in a static initializer
	@SuppressLint("SdCardPath")
	final public static class Local extends Path
	{
		private static final String LOCAL_ROOT_STRING = Environment.getExternalStorageDirectory().getAbsolutePath()
				+ "/";

		// This is a workaround for the open-in feature. Files from external directories are shared and the external
		// storage root might be incorrectly qualified. For example, a path can start with "/sdcard/" instead of
		// "/storage/emulated/0". This code should be considered as a hacky workaround.
		private static final Set<String> LOCAL_ROOT_ALIASES;
		static
		{
			LinkedHashSet<String> s = new LinkedHashSet<String>();
			s.add("/sdcard/");
			s.add("/storage/sdcard0/");
			LOCAL_ROOT_ALIASES = Collections.unmodifiableSet(s);
		}

		private Local(String pathname)
		{
			super(LOCAL_LOCATION, pathname);
		}

		private Local(Parcel in)
		{
			super(in);
		}

		/**
		 * Creates a Path.Local from a pathname String. The file must be in the tree starting from {@link
		 * localRoot()}.
		 * 
		 * @throws LocalPathNotRecognizedException
		 *             when pathname does not start with {@link localRoot()}'s pathname or one of its aliases.
		 */
		public static Local fromAbsolutePath(String pathname) throws LocalPathNotRecognizedException
		{
			checkNonNullArg(pathname);

			if (pathname.startsWith(LOCAL_ROOT_STRING))
			{
				return new Local(pathname.substring(LOCAL_ROOT_STRING.length()));
			}

			// Hacky solution for 'open-in'-ing a file with a file name not properly qualified
			for (String alias : LOCAL_ROOT_ALIASES)
			{
				if (pathname.startsWith(alias))
				{
					Log.w(TAG, "Local path starting with \"" + alias + "\", was: " + pathname);
					return new Local(pathname.substring(alias.length()));
				}
			}
			throw new LocalPathNotRecognizedException("pathname must be in the external storage directory, was: "
					+ pathname);
		}

		public String getAbsolutePathString()
		{
			return LOCAL_ROOT_STRING + getPathFile().getPath();
		}

		Local[] listPaths()
		{
			String root = LOCAL_ROOT_STRING;
			String path = root + getPathFile().getPath();
			File folder = new File(path);

			File[] files = listFiles(folder);
			if (files == null)
			{
				return null;
			}

			Local[] paths = new Local[files.length];
			for (int i = 0; i < files.length; i++)
			{
				// Removes the root from the path
				path = files[i].getAbsolutePath().substring(root.length());
				paths[i] = new Local(path);
			}

			return paths;
		}

		boolean mkdir()
		{
			return new File(getAbsolutePathString()).mkdir();
		}

		boolean mkdirs()
		{
			return new File(getAbsolutePathString()).mkdirs();
		}

		/**
		 * Deletes the file or folder corresponding to this Path.
		 * 
		 * @param force
		 *            flag to force deletion of a non-empty folder
		 * @return true if Path has been deleted, false otherwise
		 */
		boolean delete(boolean force)
		{
			File curr = new File(getAbsolutePathString());
			if (curr.isFile() || !force)
			{
				return curr.delete();
			}

			int depth = 0;
			outer: while (depth >= 0)
			{
				File[] files = listFiles(curr);
				for (File f : files)
				{
					if (f.isDirectory())
					{
						curr = f;
						depth++;
						continue outer;
					}
					else
					{
						if (!f.delete())
						{
							return false;
						}
					}
				}

				// Post-condition: all files and subdirectories have been deleted;
				// delete curr and go up one level
				File next = curr.getParentFile();
				if (!curr.delete())
				{
					return false;
				}

				curr = next;
				depth--;
			}
			return true;
		}

		boolean rename(String newName)
		{
			checkNonNullArg(newName);

			File f = new File(getAbsolutePathString());
			if (f.exists())
			{
				return f.renameTo(new File(f.getParentFile(), newName));
			}
			return false;
		}

		boolean isDirectory()
		{
			return new File(getAbsolutePathString()).isDirectory();
		}

		long lastModified()
		{
			return new File(getAbsolutePathString()).lastModified();
		}

		long length()
		{
			File f = new File(getAbsolutePathString());
			if (f.isFile())
			{
				return f.length();
			}

			long len = 0L;
			Queue<File> queue = new ArrayDeque<File>();
			queue.offer(f);
			while (!queue.isEmpty())
			{
				f = queue.poll();
				if (f.isDirectory())
				{
					File[] children = listFiles(f);
					for (File child : children)
					{
						queue.offer(child);
					}
				}
				else
				{
					len += f.length();
				}
			}
			return len;
		}

		long getFolderSize(File f) {
			long size = 0;
			if (f.isDirectory()) {
				for (File file : f.listFiles()) {
					size += getFolderSize(file);
				}
			} else {
				size=f.length();
			}
			return size;
		}

		/**
		 * Returns an array of files contained in the directory represented by {@code f}. The result is null if
		 * {@code f} is not a directory. If {@code f} is a directory and an I/O error occurs, the empty list is
		 * returned.
		 * <p>
		 * The purpose of this method is to have an implementation of {@link File#listFiles()} that works 100% of the
		 * time. The android documentation does not specify what should happen on an I/O error so we decide here to
		 * check it and return always an empty array. This is different from what the Oracles documentation says.
		 * </p>
		 * 
		 * @see https://jira.thinprint.de/browse/BUG-26637?filter=17789
		 */
		private File[] listFiles(File f)
		{
			if (!f.isDirectory())
			{
				return null;
			}

			File[] files = f.listFiles();
			if (files == null)
			{
				RuntimeException e = new RuntimeException();
				Log.w(TAG, "listFiles() was null at path: " + f.getAbsolutePath(), e);
				files = new File[0];
			}
			return files;
		}

		@Override
		public <T> T execute(PathOperation<? extends T> op)
		{
			return op.execute(this);
		}

		public static final Parcelable.Creator<Local> CREATOR = new Parcelable.Creator<Local>()
		{

			@Override
			public Local createFromParcel(Parcel in)
			{
				return new Local(in);
			}

			@Override
			public Local[] newArray(int size)
			{
				return new Local[size];
			}
		};

		/** Comparators that sorts folder first in a local query */
		public static class FolderFirstComparator implements Comparator<Local>
		{
			@Override
			public int compare(Local lhs, Local rhs)
			{
				if (lhs == null && rhs == null)
				{
					return 0;
				}

				// Nulls are after everything
				if (lhs == null && rhs != null)
				{
					return +1;
				}

				if (lhs != null && rhs == null)
				{
					return -1;
				}

				// lhs, rhs are not null
				boolean isLhsDirectory = lhs.isDirectory();
				boolean isRhsDirectory = rhs.isDirectory();

				if ((isLhsDirectory && isRhsDirectory) || (!isLhsDirectory && !isRhsDirectory))
				{
					return lhs.getName().compareTo(rhs.getName());
				}

				if (isLhsDirectory)
				{
					return -1;
				}
				else
				{
					return +1;
				}

			}
		}
	}
}
