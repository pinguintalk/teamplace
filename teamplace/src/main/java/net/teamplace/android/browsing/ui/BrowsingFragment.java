package net.teamplace.android.browsing.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import com.cortado.android.R;
import com.forgottensystems.multiimagechooserV2.MultiImageChooserActivity;

import net.teamplace.android.action.Action;
import net.teamplace.android.action.ActionFactory;
import net.teamplace.android.action.BaseActionFactory;
import net.teamplace.android.action.MailAction;
import net.teamplace.android.action.NewChooserAction;
import net.teamplace.android.action.NewFolderAction;
import net.teamplace.android.action.PrintAction;
import net.teamplace.android.action.RenameAction;
import net.teamplace.android.action.ShareAction;
import net.teamplace.android.action.SmarfileAction;
import net.teamplace.android.action.SortAction;
import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.EditTextDialogFragment;
import net.teamplace.android.browsing.dialog.FilePickerDialogFragment;
import net.teamplace.android.browsing.dialog.NewChooserDialogFragment;
import net.teamplace.android.browsing.dialog.SmartfileDialogFragment;
import net.teamplace.android.browsing.dialog.SortDialogFragment;
import net.teamplace.android.browsing.dialog.TeamDriveExpiredDialogFragment;
import net.teamplace.android.browsing.filtering.SearchFilterHelper;
import net.teamplace.android.browsing.openin.OpenInActivity;
import net.teamplace.android.browsing.path.LocalPathNotRecognizedException;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.sorting.SortManager;
import net.teamplace.android.browsing.widget.ListViewSwipeRefreshLayout;
import net.teamplace.android.browsing.widget.MultiSelectAdapter;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.services.IntentFactory;
import net.teamplace.android.teamdrive.ui.TeamDriveASFragment;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.RightsHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static net.teamplace.android.utils.GenericUtils.laundryException;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Handles main browsing logic, such as content loading, displaying, and updating, clicks and request for action mode
 * 
 * @author Gil Vegliach
 */
public class BrowsingFragment extends Fragment implements
		AdapterView.OnItemClickListener,
		AdapterView.OnItemLongClickListener,
		SwipeRefreshLayout.OnRefreshListener,
		ActionMode.Callback,
		DrawerLayout.DrawerListener,
		MultiSelectAdapter.OnIconClickListener,
		MultiSelectAdapter.OnFavoriteCheckedChangeListener,
		MultiSelectAdapter.CheckTeamDriveCallback
{
	public static final String TAG = tag(BrowsingFragment.class);

	private static final String PKG = pkg(BrowsingFragment.class);
	private static final String KEY_IS_DRAWER_ACTIVE = PKG + ".isDrawerActive";
	private static final String KEY_ITEMS = PKG + ".items";
	private static final String KEY_SORT_CATEGORY = PKG + ".sortCategory";
	private static final String KEY_SORT_ORDER = PKG + ".sortOrder";
	private static final String KEY_PATH = PKG + ".path";
	private static final String KEY_CHECKED_IDS = PKG + ".checkedIds";
	private static final String KEY_CURR_REQUESTS = PKG + ".currRequests";
	private static final String KEY_RENAMING_PATH = PKG + ".renamingPath";
	private static final String KEY_HAS_DATA = PKG + ".hasData";
	private static final String KEY_CURR_FILTER_TEXT = PKG + ".currFilterText";

	// TODO: @thkru refactor this
	private static final String KEY_EXPIRATION_DIALOG_ALREADY_SHOWN = PKG + ".expirationDialogAlreadyShown";

	/** Length of each frame in the loading animation */
	private final static long LOADING_ANIMATION_TICK = 300; // ms

	/** Current Path that we are browsing */
	private Path mCurrFolder;

	/** Ids of requests in progress */
	private ArrayList<Integer> mCurrRequests;

	// Features that can enabled or disabled switching the relative flags
	private boolean mHasFavorites;
	private boolean mHasNewChooser;
	/** Sort category of the current folder */
	private int mSortCategory;

	/** Sort order of the current folder */
	private int mSortOrder;
	/** Curent search text*/
	private String mCurrentFilterText;
	/**
	 * Non-null iff between {@link onCreateActionMode()} and {@link onDestroyActionMode()}
	 */
	private ActionMode mActionMode;

	/** Acts as a data source. Never null after init(), recreated on updates */
	private MultiSelectAdapter mAdapter;
	private ListViewSwipeRefreshLayout mSwipeRefreshLayout;
	private ListView mListView;
	private ViewGroup mEmptyStateLayout;

	/**
	 * Whether this fragment has some data. This mean we received at least one successful response from the server, it
	 * does not matter whether it is empty or not. Retained
	 */
	private boolean mHasData;

	/**
	 * Used only to pass file information to a favorite/unfavorite action. Always non-null outside
	 * onFavoriteCheckedChange()
	 */
	private Path mFavoritingPath;

	/**
	 * Path that we are renaming (using the dialog) or null. Retained across configuration changes. Non-null iff between
	 * when we show the dialog and when we get its callback back. Retained
	 */
	private Path mRenamingPath;

	/**
	 * True iff between DrawerOpened() and DrawerClosed(). Retained
	 */
	private boolean mIsDrawerActive;

	final private SearchFilterHelper mSearchFilterHelper = new SearchFilterHelper();

	// TODO: @thkru this feature should be re-implemented with a single control point. At the moment it will not
	// disable the ui if there are no files to be shown
	private boolean isExpirationDialogAlreadyShown;

	/** 'Loading' text in the actionbar subtitle */
	private String mSubtitleLabel;

	/** Animates {@link #mSubtitleLabel} */
	private final ValueAnimator mSubtitleLabelAnimator = ValueAnimator.ofInt(0, 1, 2, 3);
	{
		mSubtitleLabelAnimator.setDuration(LOADING_ANIMATION_TICK * 3);
		mSubtitleLabelAnimator.setRepeatCount(ValueAnimator.INFINITE);
		mSubtitleLabelAnimator.setInterpolator(new LinearInterpolator());
		mSubtitleLabelAnimator.addUpdateListener(new AnimatorUpdateListener()
		{
			@Override
			public void onAnimationUpdate(ValueAnimator animation)
			{
				// 1 <= dotNumber <= 3
				int dotNumber = 1 + ((Integer) animation.getAnimatedValue()) % 3;
				String dots = new String(new char[dotNumber]).replace("\0", ".");

				BrowsingActivity act = (BrowsingActivity) getActivity();
				if (act == null)
				{
					return;
				}
				act.setActionBarSubtitle(mSubtitleLabel + dots);
			}
		});
		mSubtitleLabelAnimator.addListener(new AnimatorListenerAdapter()
		{
			@Override
			public void onAnimationEnd(Animator animation)
			{
				syncActionBarStyle();
			}
		});
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	private final View.OnClickListener mEmptyStateClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if (isTeamDriveExpired())
			{
				showTeamDriveExpiredDialog();
			}
			else
			{
				mActionFactory.build(R.id.action_new).execute();
			}
		}
	};

	private final ActionFactory mActionFactory = new BrowsingActionFactory();

	public static BrowsingFragment newInstance(Path path)
	{
		BrowsingFragment frag = new BrowsingFragment();
		Bundle args = new Bundle();
		args.putParcelable(KEY_PATH, path);
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreate(Bundle state)
	{
		super.onCreate(state);
		init(state);
		setHasOptionsMenu(true);
		Log.d(TAG, "BrowsingFragment.onCreate(): " + mCurrFolder);
	}

	@Override
	public void onDestroy()
	{
		Log.d(TAG, "BrowsingFragment.onDestroy(): " + mCurrFolder);
		super.onDestroy();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle state)
	{
		Log.d(TAG, "BrowsingFragment.onCreateView(): " + mCurrFolder);
		View root = inflater.inflate(R.layout.brw_fragment, container, false);
		setupViews(root);

		// TODO: what about list state as position and top? They seem automatically restored (at least on lollipop)
		if (mHasData)
		{
			bindListViewToData();
		}
		return root;
	}

	@Override
	public void onActivityCreated(Bundle state)
	{
		Log.d(TAG, "BrowsingFragment.onActivityCreated(): " + mCurrFolder + ", state: " + state);
		super.onActivityCreated(state);
		styleActionBarLogo();
		if (mAdapter.hasCheckedItems() && !mIsDrawerActive)
		{
			getActivity().startActionMode(this);
		}

		if (state != null) {
			restoreInstanceState(state);
		}
		// TODO: @thkru: check this
		handleIntentForOpenIn();
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "BrowsingFragment.onResume(): " + mCurrFolder);
		super.onResume();
		registerDynamicListeners();

		if (!mHasData && !isLoading()) {
			refreshContent();
		}

		syncLoadingAnimation();
	}

	public String getCurrFilterString()
	{
		return mCurrentFilterText;
	}


	@Override
	public void onPause()
	{
		Log.d(TAG, "BrowsingFragment.onPause(): " + mCurrFolder);
		super.onPause();
		//clearVolatileInstanceState();
		unregisterDynamicListeners();
		syncLoadingAnimation();
	}

	@Override
	public void onStart()
	{
		Log.d(TAG, "BrowsingFragment.onStart(): " + mCurrFolder);
		super.onStart();
	}

	@Override
	public void onStop()
	{
		Log.d(TAG, "BrowsingFragment.onStop(): " + mCurrFolder);
		super.onStop();
	}

	@Override
	public void onRefresh()
	{
		Log.d(TAG, "BrowsingFragment.onRefresh(): " + mCurrFolder);
		refreshContent();
	}

	public void onResponseReceived(Intent intent)
	{
		Log.d(TAG, "BrowsingFragment.onResponseReceived(): " + mCurrFolder
				+ ", id=" + Contract.extractRequestId(intent)
				+ ", action=" + intent.getAction()
				+ ", pending=" + mCurrRequests);


		String action = intent.getAction();
		int id = Contract.extractRequestId(intent);

		if (IntentFactory.ACTION_RESPONSE_DOWNLOAD.equals(action)) {
			// we don't need to refresh browse here
			id = IntentFactory.extractRequestId(intent);
			mCurrRequests.remove((Integer) id);
			return;
		}
		else if (IntentFactory.ACTION_RESPONSE_UPLOAD.equals(action)
				|| IntentFactory.ACTION_RESPONSE_MOVE_LOCAL_TO_REMOTE.equals(action)
				|| IntentFactory.ACTION_RESPONSE_MOVE_REMOTE_TO_LOCAL.equals(action)
				|| IntentFactory.ACTION_RESPONSE_EXPORT_TO_PDF.equals(action)) {

				id = IntentFactory.extractRequestId(intent);
				mCurrRequests.remove((Integer) id);
				refreshContent();
				return;
		}

		if (id == Contract.NO_ID || !mCurrRequests.remove((Integer) id))
		{
			syncLoadingAnimation();
			return;
		}

		if (BrowsingService.Contract.ACTION_RESPONSE_BROWSE.equals(action))
		{
			onResponseBrowse(intent);
		}
		else if (Contract.ACTION_RESPONSE_UPLOAD_PICTURE.equals(action))
		{
			refreshContent();
			FeedbackToast.show(getActivity(), true, getString(R.string.flp_image_picker_picture_uploaded));
		}
		else if (Contract.ACTION_RESPONSE_ADD_FAVORITE.equals(action)
				|| Contract.ACTION_RESPONSE_DELETE_FAVORITE.equals(action))
		{
			// Nothing, UI already up-to-date
		}
		else if (Contract.ACTION_RESPONSE_MKDIR.equals(action)
				|| Contract.ACTION_RESPONSE_DELETE.equals(action)
				|| Contract.ACTION_RESPONSE_MOVE.equals(action)
				|| Contract.ACTION_RESPONSE_RENAME.equals(action)
				|| Contract.ACTION_RESPONSE_COPY.equals(action)
				|| Contract.ACTION_RESPONSE_SET_SORT.equals(action)
				|| Contract.ACTION_RESPONSE_EXPORT_TO_ZIP.equals(action)
				|| Contract.ACTION_RESPONSE_EXPORT_TO_PDF.equals(action)
				|| Contract.ACTION_RESPONSE_EXTRACT_VERSION.equals(action))
		{
			refreshContent();
		}

		syncLoadingAnimation();
	}

	@Override
	public void onDrawerOpened(View drawerView)
	{
		mIsDrawerActive = true;
		finishActionModeIfNeedBe();
		syncActionBarStyle();
	}

	@Override
	public void onDrawerClosed(View drawerView)
	{
		mIsDrawerActive = false;

		if (mAdapter.hasCheckedItems())
		{
			getActivity().startActionMode(this);
		}
		syncActionBarStyle();
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset)
	{
	}

	@Override
	public void onDrawerStateChanged(int newState)
	{
		if (newState == DrawerLayout.STATE_DRAGGING)
		{
			mSearchFilterHelper.collapseSearchView();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		FileInfo item = mAdapter.getItem(position);
		if (isTeamDriveExpired() && !item.isFolder())
		{
			showTeamDriveExpiredDialog();
			return;
		}

		// A tap on an item during multiselect stops it
		if (mAdapter.hasCheckedItems())
		{
			mActionMode.finish();
		}

		if (item.isFolder()) {
			BrowsingActivity.browseAndPush(getFragmentManager(), item.getPath());
		}
		else
		{
			onPreviewableItemClick(item);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
	{
		// TODO: @thkru refactor this call
		if (isTeamDriveExpired())
		{
			showTeamDriveExpiredDialog();
			return true;
		}

		if (mAdapter.isViewAnimating(view))
		{
			return true;
		}

		// TODO: multi select cannot be started while filtering. This decision was made because of a lack of time, not
		// because it is desirable. See: https://jira.thinprint.de/browse/BUG-26628?filter=-1
		if (mAdapter.isFilterEnabled())
		{
			return true;
		}

		boolean wasMultiSelect = mAdapter.hasCheckedItems();
		mAdapter.toggle(position);
		boolean isMultiSelect = mAdapter.hasCheckedItems();

		if (!wasMultiSelect && isMultiSelect)
		{
			getActivity().startActionMode(this);
		}
		else if (wasMultiSelect && !isMultiSelect)
		{
			mActionMode.finish();
		}
		else if (wasMultiSelect && isMultiSelect)
		{
			mActionMode.invalidate();
			syncCabCheckedCount();
		}

		return true;
	}

	@Override
	public void onIconClick(View v, ViewGroup parent, int position, int id)
	{
		onItemLongClick(mListView, parent, position, id);
	}

	public void onPreviewableItemClick(FileInfo previewable)
	{
		if(!Path.isLocalLocation(previewable.getPath().getLocation())) {
			if(!GenericUtils.isConnectedToInternet(getActivity())) {
				FeedbackToast.show(getActivity(),false,
						getActivity().getResources().getString(R.string.tmd_err_no_internet_connection));
				return;
			}
		}
		Intent i = PreviewActivity.buildIntent(getActivity(), previewable,
				new PreviewConfiguration().folderContent(mAdapter.getItems()));
		startActivityForResult(i, PreviewActivity.REQUEST_CODE);

	}

	@Override
	public void onFavoriteCheckedChange(FileInfo file, boolean checked) {
		Log.d(TAG, "BrowsingFragment.onFavoriteCheckedChange(). Updading favorite status for: " + file.getPath()
				+ ", status: " + checked);

		mFavoritingPath = file.getPath();
		int id = checked ? R.id.action_favorite : R.id.action_unfavorite;
		mActionFactory.build(id).execute();
		mFavoritingPath = null;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "BrowsingFragment.onSaveInstanceState(): this: " + mCurrFolder);

		super.onSaveInstanceState(outState);
		clearVolatileInstanceState();
		syncLoadingAnimation();
		saveInstanceState(outState);
	}

	protected void restoreInstanceState(Bundle outState)
	{
		if (outState == null)
		{
			return;
		}

		//ArrayList<FileInfo> items = mAdapter.getItems();
		//ArrayList<Integer> checkedIds = mAdapter.getCheckedPositions();
		//outState.putParcelableArrayList(KEY_ITEMS, items);

		// outState.putIntegerArrayList(KEY_CHECKED_IDS, checkedIds);

		mSortCategory = outState.getInt(KEY_SORT_CATEGORY);
		mSortOrder = outState.getInt(KEY_SORT_ORDER);
		mCurrRequests = outState.getIntegerArrayList(KEY_CURR_REQUESTS);
		mHasData = outState.getBoolean(KEY_HAS_DATA, false);
		mIsDrawerActive = outState.getBoolean(KEY_IS_DRAWER_ACTIVE, false);
		mRenamingPath = outState.getParcelable(KEY_RENAMING_PATH);
		mCurrentFilterText = outState.getString(KEY_CURR_FILTER_TEXT);

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu) {
		//VISIBILITY of menuItems will handle standard in BrowsingActivity first
		super.onPrepareOptionsMenu(menu);
		MenuItem activityStreamMenuItem = menu.findItem(R.id.action_activity_stream);
		if(mIsDrawerActive)
			activityStreamMenuItem.setVisible(false);
		else
			activityStreamMenuItem.setVisible(Path.isTeamDriveLocation(mCurrFolder.getLocation()));

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.brw_main_menu, menu);
		MenuItem searchItem = menu.findItem(R.id.action_search);
		mSearchFilterHelper.bindSearchMenuItem(searchItem);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (isTeamDriveExpired())
		{
			showTeamDriveExpiredDialog();
			return true;
		}

		// Let search be handled by SearchView
		int id = item.getItemId();
		if (id == R.id.action_search)
		{
			return false;
		}

		mActionFactory.build(id).execute();
		return true;
	}

	private void showErrorToastMessage(Context ctx, String msg)
	{
		FeedbackToast feedbackToast = new FeedbackToast(ctx);
		feedbackToast.setIconBgRes(R.drawable.brw_error_circle);
		feedbackToast.setIconRes(R.drawable.delete);
		feedbackToast.setTitle(getString(R.string.ntf_title_error));
		feedbackToast.setMessage(msg);
		feedbackToast.show();
	}

	// Used to get results back from dialogs and from the preview component
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO: @thkru: why this code?
		if (mOpenInPathname != null)
		{
			getActivity().getIntent().removeExtra(OpenInActivity.OPEN_IN_EXTRA);
		}

		if (resultCode != Activity.RESULT_OK)
		{
			if(!GenericUtils.isConnectedToInternet(getActivity()))
				showErrorToastMessage(this.getActivity(), getString(R.string.tmd_err_no_internet_connection));
			else
				showErrorToastMessage(this.getActivity(), getString(R.string.ntf_msg_unknown_error));
			return;
		}

		if (requestCode == PreviewActivity.REQUEST_CODE)
		{
			handlePreviewResult(data);
		}
		else if (requestCode == R.id.request_code_new_folder
				|| requestCode == NewChooserDialogFragment.REQUEST_CODE_NEW_FOLDER)
		{
			handleNewFolderResult(data);
		}
		else if (requestCode == R.id.request_code_rename)
		{
			handleRenameResult(data);
		}
		else if (requestCode == SmartfileDialogFragment.REQUEST_CODE)
		{
			handleSmartfileResult(data);
		}
		else if (requestCode == NewChooserDialogFragment.REQUEST_CODE_IMAGE_PICKER)
		{
			handleImagePickerResult(data);
		}
		else if (requestCode == FilePickerDialogFragment.REQUEST_CODE)
		{
			handleFilePickerResult(data);
		}
		else if (requestCode == SortDialogFragment.REQUEST_CODE)
		{
			handleSortResult(data);
		}
	}

	@Override
	public boolean onCreateActionMode(ActionMode mode, Menu menu)
	{
		mActionMode = mode;
		MenuInflater inflater = mode.getMenuInflater();
		inflater.inflate(R.menu.brw_context_menu, menu);
		syncCabCheckedCount();
		syncCabCloseButtonLanguage();
		syncFolderModeActions();

		return true;
	}

	@Override
	public boolean onPrepareActionMode(ActionMode mode, Menu menu)
	{
		syncActionModeActions();
		return true;
	}

	@Override
	public boolean onActionItemClicked(ActionMode mode, MenuItem item)
	{
		if (isTeamDriveExpired())
		{
			showTeamDriveExpiredDialog();
			return true;
		}

		// Experimental code for file actions
		int id = item.getItemId();
		mActionFactory.build(id).execute();
		return true;
	}

	// Not posted on UI thread, called inside finish() (usually)
	@Override
	public void onDestroyActionMode(ActionMode mode)
	{
		mActionMode = null;
		if (!mIsDrawerActive)
		{
			mAdapter.uncheckAll();
		}
	}

	private void onResponseBrowse(Intent intent)
	{
		List<FileInfo> result = intent.getParcelableArrayListExtra(Contract.RESPONSE_BROWSE_FOLDER_CONTENT);
		mSortCategory = intent.getIntExtra(Contract.RESPONSE_SORT_CATEGORY, SortManager.DEFAULT_CATEGORY);
		mSortOrder = intent.getIntExtra(Contract.RESPONSE_SORT_ORDER, SortManager.DEFAULT_ORDER);

		if (result != null)
		{
			updateFromResult(result);
		}
	}

	private void init(Bundle state)
	{
		Bundle args = getArguments();
		if (args != null)
		{
			mCurrFolder = args.getParcelable(KEY_PATH);
			mHasFavorites = !(mCurrFolder instanceof Path.Local);
			mHasNewChooser = mHasFavorites;
		}

		if (state != null)
		{
			List<FileInfo> items = state.getParcelableArrayList(KEY_ITEMS);
			mAdapter = new MultiSelectAdapter(getActivity(), items, mHasFavorites, this, this, this);
			mSortCategory = state.getInt(KEY_SORT_CATEGORY, SortManager.DEFAULT_CATEGORY);
			mSortOrder = state.getInt(KEY_SORT_ORDER, SortManager.DEFAULT_ORDER);
			mCurrRequests = state.getIntegerArrayList(KEY_CURR_REQUESTS);
			mHasData = state.getBoolean(KEY_HAS_DATA);
			mIsDrawerActive = state.getBoolean(KEY_IS_DRAWER_ACTIVE);
			mCurrentFilterText = state.getString(KEY_CURR_FILTER_TEXT);

			ArrayList<Integer> checkedIds = state.getIntegerArrayList(KEY_CHECKED_IDS);
			if (checkedIds != null && checkedIds.size() > 0)
			{
				mAdapter.check(checkedIds);
			}
		}
		else
		{
			mAdapter = new MultiSelectAdapter(getActivity(), new ArrayList<FileInfo>(), mHasFavorites, this, this, this);
			mSortCategory = SortManager.DEFAULT_CATEGORY;
			mSortOrder = SortManager.DEFAULT_ORDER;
			mCurrRequests = new ArrayList<Integer>();
			mHasData = false;
			mIsDrawerActive = false;
		}

		mSearchFilterHelper.bindFilterableAdapter(mAdapter);
		mSubtitleLabel = getResources().getString(R.string.brw_loading_animation_label);


		// TODO: @thkru: refactor
		isExpirationDialogAlreadyShown = state != null
				? state.getBoolean(KEY_EXPIRATION_DIALOG_ALREADY_SHOWN, false)
				: false;
	}

	private void setupViews(View root)
	{
		setupSwipeRefreshLayout(root);
		setupListView(root);
		setupEmptyStateLayout(root);
	}

	private void setupSwipeRefreshLayout(View root)
	{
		mSwipeRefreshLayout = (ListViewSwipeRefreshLayout) root.findViewById(R.id.swiperefresh);
		mSwipeRefreshLayout.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
		mSwipeRefreshLayout.setOnRefreshListener(this);
	}

	private void setupListView(View root)
	{
		mListView = (ListView) root.findViewById(R.id.browser_list);
		mListView.setOnItemLongClickListener(this);
		mListView.setOnItemClickListener(this);
		mListView.setItemsCanFocus(false);
	}

	private void setupEmptyStateLayout(View root)
	{
		mEmptyStateLayout = (LinearLayout) root.findViewById(R.id.empty_state_layout);
		mEmptyStateLayout.setOnClickListener(mEmptyStateClickListener);

		// The text below the empty state graphic is to be show only for myplace and teamplaces. No text is
		// to be shown during local browsing (according to Ursula).
		View label = mEmptyStateLayout.findViewById(R.id.empty_state_label);
		label.setVisibility(mCurrFolder instanceof Path.Local
				? View.GONE
				: View.VISIBLE);
	}

	/**
	 * Sets in the adapter hiding the ListView and showing the label if there is the folder is empty. Assumes the
	 * adapter is initialized and has data. In addition if a teamdrive is shown and it is expired, an expiration dialog
	 * will be shown only on the root element of the teamdrive
	 */
	private void bindListViewToData()
	{
		mListView.setAdapter(mAdapter);
		syncEmptyFolderLabel();
	}

	private boolean isLoading()
	{
		return !mCurrRequests.isEmpty();
	}

	private void showTeamDriveExpiredDialog()
	{
		String teamId = ((Path.TeamDrive) mCurrFolder).getTeamDriveId();
		TeamDriveExpiredDialogFragment dlg = TeamDriveExpiredDialogFragment.newInstance(teamId);
		dlg.show(getFragmentManager(), TeamDriveExpiredDialogFragment.TAG);
	}

	private void registerDynamicListeners()
	{
		BrowsingActivity act = (BrowsingActivity) getActivity();
		act.registerDrawerObserver(this);

		IntentFilter filter = new IntentFilter(Contract.ACTION_RESPONSE_BROWSE);
		filter.addAction(Contract.ACTION_RESPONSE_MKDIR);
		filter.addAction(Contract.ACTION_RESPONSE_COPY);
		filter.addAction(Contract.ACTION_RESPONSE_MOVE);
		filter.addAction(Contract.ACTION_RESPONSE_DELETE);
		filter.addAction(Contract.ACTION_RESPONSE_RENAME);
		filter.addAction(Contract.ACTION_RESPONSE_EXPORT_TO_PDF);
		filter.addAction(Contract.ACTION_RESPONSE_EXPORT_TO_ZIP);
		filter.addAction(Contract.ACTION_RESPONSE_UPLOAD_PICTURE);
		filter.addAction(Contract.ACTION_RESPONSE_ADD_FAVORITE);
		filter.addAction(Contract.ACTION_RESPONSE_DELETE_FAVORITE);
		filter.addAction(Contract.ACTION_RESPONSE_SET_SORT);
		filter.addAction(Contract.ACTION_RESPONSE_EXTRACT_VERSION);

		// for background actions
		filter.addAction(IntentFactory.ACTION_RESPONSE_UPLOAD);
		filter.addAction(IntentFactory.ACTION_RESPONSE_DOWNLOAD);
		filter.addAction(IntentFactory.ACTION_RESPONSE_MOVE_LOCAL_TO_REMOTE);
		filter.addAction(IntentFactory.ACTION_RESPONSE_MOVE_REMOTE_TO_LOCAL);
		filter.addAction(IntentFactory.ACTION_RESPONSE_EXPORT_TO_PDF);
		filter.addAction(IntentFactory.ACTION_RESPONSE_EXPORT_TO_ZIP);


		LocalBroadcastManager.getInstance(act).registerReceiver(mReceiver, filter);
	}

	private void unregisterDynamicListeners()
	{
		BrowsingActivity act = (BrowsingActivity) getActivity();
		act.unregisterDrawerObserver();
		LocalBroadcastManager.getInstance(act).unregisterReceiver(mReceiver);
	}

	// Should be called in onPause() and onSaveInstanceState() quite early, before actually dealing with the instance
	// state. The reason is that onPause() can be called before or after onSavedInstanceState(). This method should
	// also be idempotent.
	private void clearVolatileInstanceState()
	{
		mCurrentFilterText = mSearchFilterHelper.getCurrFilterString();
		// Remove filtering
		mSearchFilterHelper.collapseSearchView();

		// The Activity is partially obscured by another Activity, going to the background, going into the back-stack or
		// simply being destroyed: stop listening for the current requests as we could drop a response
		if (!getActivity().isChangingConfigurations())
		{
			mCurrRequests.clear();
		}
	}

	private void saveInstanceState(Bundle outState)
	{
		ArrayList<FileInfo> items = mAdapter.getItems();
		ArrayList<Integer> checkedIds = mAdapter.getCheckedPositions();
		outState.putParcelableArrayList(KEY_ITEMS, items);
		outState.putInt(KEY_SORT_CATEGORY, mSortCategory);
		outState.putInt(KEY_SORT_ORDER, mSortOrder);
		outState.putIntegerArrayList(KEY_CHECKED_IDS, checkedIds);
		outState.putIntegerArrayList(KEY_CURR_REQUESTS, mCurrRequests);
		outState.putBoolean(KEY_HAS_DATA, mHasData);
		outState.putBoolean(KEY_IS_DRAWER_ACTIVE, mIsDrawerActive);
		outState.putParcelable(KEY_RENAMING_PATH, mRenamingPath);
		outState.putString(KEY_CURR_FILTER_TEXT, mCurrentFilterText);

		// TODO: @thkru refactor this
		outState.putBoolean(KEY_EXPIRATION_DIALOG_ALREADY_SHOWN, isExpirationDialogAlreadyShown);
	}


	private void updateFromResult(List<FileInfo> result)
	{
		// Saves state before updating the adapter
		ArrayList<Integer> checkedIds = null;
		if (mAdapter.hasCheckedItems())
		{
			checkedIds = mAdapter.getCheckedPositions();
		}

		// Save and restore the current position of the list
		View view = mListView.getChildAt(0);
		int pos = mListView.getFirstVisiblePosition();
		int top = (view == null) ? 0 : view.getTop();

		// Updates the adapter
		mAdapter = new MultiSelectAdapter(getActivity(), result, mHasFavorites, this, this, this);
		bindListViewToData();
		mSearchFilterHelper.bindFilterableAdapter(mAdapter);

		// Restores state
		mListView.setSelectionFromTop(pos, top);

		if (checkedIds != null && checkedIds.size() > 0)
		{
			mAdapter.check(checkedIds);
		}

		// Restores search results
		String currFilter = mSearchFilterHelper.getCurrFilterString();
		if (!TextUtils.isEmpty(currFilter))
		{
			mAdapter.setFilter(currFilter);
			mCurrentFilterText = currFilter;
		}
		// We have data now
		mHasData = true;

	}

	private void finishActionModeIfNeedBe()
	{
		if (mActionMode != null)
		{
			mActionMode.finish();
		}
	}

	private static ArrayList<Path> extractPathsArrayList(ArrayList<FileInfo> fileInfos)
	{
		ArrayList<Path> result = new ArrayList<Path>();
		for (FileInfo info : GenericUtils.emptyIfNull(fileInfos))
		{
			result.add(info.getPath());
		}
		return result;
	}

	private static Path[] extractPathsArray(List<FileInfo> fileInfos)
	{
		int size = fileInfos.size();
		Path[] paths = new Path[size];
		for (int i = 0; i < size; i++)
		{
			paths[i] = fileInfos.get(i).getPath();
		}
		return paths;
	}

	private HashMap<String, String> buildIllegalInput(CharSequence toRename)
	{
		HashMap<String, String> illegalInputs = new HashMap<>();
		String alreadyExists = getString(R.string.brw_input_already_exists);
		for (FileInfo info : mAdapter.getItems())
		{
			if (!info.getName().equals(toRename))
			{
				illegalInputs.put(info.getName().toLowerCase(Locale.getDefault()), alreadyExists);
			}
		}
		return illegalInputs;
	}

	private HashMap<String, String> buildIllegalInputForNewFolder()
	{
		HashMap<String, String> illegalInputs = new HashMap<>();
		String alreadyExists = getString(R.string.brw_folder_already_exists);
		for (FileInfo info : mAdapter.getItems())
		{
			illegalInputs.put(info.getName().toLowerCase(Locale.getDefault()), alreadyExists);
		}
		return illegalInputs;
	}

	private void syncFolderModeActions()
	{
		List<FileInfo> checkedItems = mAdapter.getCheckedItems();
		boolean containsFolder = false;
		for (FileInfo i : checkedItems)
		{
			containsFolder |= i.isFolder();
		}
		Menu menu = mActionMode.getMenu();
		menu.findItem(R.id.action_smartfile).setVisible(!containsFolder);// only show smartfile when no folder is
		menu.findItem(R.id.action_share).setVisible(!containsFolder);// only show share when no folder is
	}

	private void syncActionModeActions()
	{
		boolean oneChecked = mAdapter.getCheckedCount() == 1;
		List<FileInfo> checkedItems = mAdapter.getCheckedItems();

		// TODO: @jobol this method potentially perform a database operation on the UI thread and should be avoided.
		Session session = new SessionManager(getActivity().getApplicationContext()).getLastSession();
		RightsHelper rightsHelper = new RightsHelper(session);

		Menu menu = mActionMode.getMenu();
		menu.findItem(R.id.action_rename).setVisible(oneChecked && rightsHelper.canRename(checkedItems));
		menu.findItem(R.id.action_delete).setVisible(rightsHelper.canDelete(checkedItems));
		menu.findItem(R.id.action_export_to_pdf).setVisible(oneChecked && rightsHelper.canExportToPdf(checkedItems));
		menu.findItem(R.id.action_zip).setVisible(oneChecked && rightsHelper.canExportToZip(checkedItems));

		menu.findItem(R.id.action_share).setVisible(oneChecked && !checkedItems.get(0).isFolder());
		menu.findItem(R.id.action_mail).setVisible(rightsHelper.canMailAndShare(checkedItems));
		menu.findItem(R.id.action_print).setVisible(oneChecked && !checkedItems.get(0).isFolder());

		boolean containsFolder = false;
		for (FileInfo i : checkedItems)
		{
			containsFolder |= i.isFolder();
		}
		menu.findItem(R.id.action_smartfile).setVisible(!containsFolder);// only show smartfile when no folder is
	}

	// TODO: this should depend on mHasData, isLoading() and mAdapter.isEmpty()
	// hasData && adapter.empty() -> empty state
	// hasData && !adapter.empty() -> show listview
	// !hasData && isLoading -> show nothing (white), avoids flickering
	// !hasData && !isLoading -> error? should never happen
	private void syncEmptyFolderLabel()
	{
		boolean empty = mAdapter.isEmpty();
		mListView.setVisibility(empty ? View.GONE : View.VISIBLE);
		mEmptyStateLayout.setVisibility(empty ? View.VISIBLE : View.GONE);
	}

	private void syncLoadingAnimation()
	{
		if (mSwipeRefreshLayout != null)
		{
			GraphicsUtils.setRefreshing(mSwipeRefreshLayout, isLoading());
		}
		syncActionBarStyle();
	}

	private void syncActionBarStyle()
	{
		if (isLoading())
		{
			if (!mIsDrawerActive)
			{ // drawer closed
				if (!mSubtitleLabelAnimator.isStarted())
				{
					mSubtitleLabelAnimator.start();
				}
			}
			else
			{ // drawer open
				mSubtitleLabelAnimator.cancel();
			}
		}
		else
		{ // not loading
			mSubtitleLabelAnimator.cancel();

			if (!mIsDrawerActive)
			{ // drawer closed
				BrowsingActivity act = ((BrowsingActivity) getActivity());
				if (act != null)
				{
					if (act != null)
					{
						if (mCurrFolder.isRoot() && Path.isTeamDriveLocation(mCurrFolder.toString())) {
							SessionManager sm = new SessionManager(this.getActivity());
							Session lastSession = sm.getLastSession();
							if(lastSession !=null) {
								// TODO TEP-1683 we have to fix here ! There are no teamdrivers at first start !
								Map<String, TeamDrive> teamDriveMap = lastSession.getTeamDriveMap();
								if (teamDriveMap !=null) {
									TeamDrive td = teamDriveMap.get(((Path.TeamDrive) mCurrFolder).getTeamDriveId());
									act.setActionBarTitle(td.getName());

									act.setActionBarSubtitle("/" + mCurrFolder.getName());
								}
							}
						} else if (Path.isRemoteLocation(mCurrFolder.getLocation())) {
							act.setActionBarTitle(getResources().getString(R.string.abt_remote));
							act.setActionBarSubtitle(PathUtils.shortDisplayablePath(mCurrFolder));
						}
						else
							act.setActionBarSubtitle(PathUtils.shortDisplayablePath(mCurrFolder));
					}
				}
			}
		}
	}

	private void syncCabCheckedCount() {
		int checkedCount = mAdapter.getCheckedCount();
		mActionMode.setTitle(GenericUtils.getSelectedString(getActivity(), checkedCount));
	}

	private void syncCabCloseButtonLanguage()
	{
		// The layout of the view action_context_bar does not contain the action_mode_close_button during creation.
		// So we post a message on the next UI loop. This is a hacky solution, see also comments in the
		// styleActionModeCloseButton() method
		new Handler().post(new Runnable() {
			@Override
			public void run() {
				styleCabCloseButtonLanguage();
			}
		});
	}

	private void styleActionBarLogo()
	{
		// Sets the actionbar icon to the logo, especially when SearchView is expanded. Taken from:
		// http://stackoverflow.com/questions/16544048/search-view-uses-app-icon-instead-of-logo
		Activity act = getActivity();
		act.getActionBar().setIcon(R.drawable.app_logo);
	}

	private void styleCabCloseButtonLanguage()
	{
		try
		{
			Resources res = getResources();
			// This can be set in the them with the attribute android:actionMenuTextColor. However the same
			// color is applied also to the text of the menu items on the non-contextual action bar. So we
			// have opted to use the attribute for the regular action bar and style manually the 'done'
			// button here
			int textColor = res.getColor(R.color.brw_cab_font);
			int buttonId = res.getIdentifier("action_mode_close_button", "id", "android");
			LinearLayout actionModeCloseButton = (LinearLayout) getActivity().findViewById(buttonId);
			TextView tvDoneLabel = (TextView) actionModeCloseButton.getChildAt(1);
			tvDoneLabel.setText(R.string.app_done);
			tvDoneLabel.setTextColor(textColor);
		}
		catch (RuntimeException e)
		{
			// Catching all RuntimeException dodges crashes. This method is only meant to change the
			// language of the 'Done' label on non-English locales, so that it is in sync with the
			// language of the rest of the UI. Unfortunately, the attribute for such text is not public
			// so here there is a hacky workaround. Should this fail, we surely don't want to crash the
			// app
			Log.e(TAG, "Changing text of 'done' button in cab failed, did its layout change in the android sdk?",
					e);
		}
	}

	private void handleSortResult(Intent data)
	{
		int category = data.getIntExtra(SortDialogFragment.KEY_CATEGORY, SortDialogFragment.DEFAULT_CATEGORY);
		int order = data.getIntExtra(SortDialogFragment.KEY_ORDER, SortDialogFragment.DEFAULT_ORDER);
		mSortCategory = category;
		mSortOrder = order;
		mAdapter.sort(category, order);
		setSortRequest(category, order);
	}

	private void handleFilePickerResult(Intent data)
	{
		ArrayList<FileInfo> files = data.getParcelableArrayListExtra(FilePickerDialogFragment.KEY_FILES);
		int size = files.size();
		Path[] sources = new Path[size];
		for (int i = 0; i < size; i++)
		{
			sources[i] = files.get(i).getPath();
		}

		copyRequest(sources, mCurrFolder);
	}

	private void handleImagePickerResult(Intent data) {
		ArrayList<String> imagePathList = data
				.getStringArrayListExtra(MultiImageChooserActivity.EXTRA_IMAGE_PATH_LIST);

		if (imagePathList == null) {
			return;
		}

		ArrayList<Path.Local> localPathList = new ArrayList<Path.Local>();

		try
		{
			for (String absolutePath : imagePathList)
			{
				localPathList.add(Path.Local.fromAbsolutePath(absolutePath));
			}
		}
		catch (LocalPathNotRecognizedException ignored)
		{
			Log.e(TAG, ignored.toString());
		}

		uploadPictureRequest(localPathList.toArray(new Path.Local[localPathList.size()]));
	}

	private void handleSmartfileResult(Intent data)
	{
		Path target = data.getParcelableExtra(SmartfileDialogFragment.KEY_TARGET);
		List<FileInfo> list = mAdapter.getCheckedItems();

		Path[] sources;
		// TODO: @thkru: this if is really obscure and it should be refactored
		if (mOpenInPathname != null)
		{
			sources = new Path[1];
			try
			{
				sources[0] = Path.Local.fromAbsolutePath(mOpenInPathname);
			}
			catch (LocalPathNotRecognizedException e)
			{
				throw new RuntimeException("mOpenInPathname not recognized, was: " + mOpenInPathname, e);
			}
			finally
			{
				mOpenInPathname = null;
			}
		}
		else
		{
			int len = list.size();
			sources = new Path[len];
			for (int i = 0; i < len; i++)
			{
				sources[i] = list.get(i).getPath();
			}
		}

		int action = data.getIntExtra(SmartfileDialogFragment.KEY_ACTION, SmartfileDialogFragment.NO_ACTION);
		if (action == SmartfileDialogFragment.ACTION_COPY)
		{
			copyRequest(sources, target);
		}
		else if (action == SmartfileDialogFragment.ACTION_MOVE)
		{
			moveRequest(sources, target);
		}
		else
		{
			throw new IllegalArgumentException("Action non recognized");
		}
		finishActionModeIfNeedBe();
	}

	private void handleRenameResult(Intent data)
	{
		CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
		renameRequest(mRenamingPath, cs.toString());
		finishActionModeIfNeedBe();
	}

	private void handleNewFolderResult(Intent data)
	{
		CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
		Path folder = Path.valueOf(mCurrFolder, cs.toString());
		mkdirRequest(folder);
		finishActionModeIfNeedBe();
	}

	private void handlePreviewResult(Intent data)
	{
		// PreviewActivity can return either no data or some request. In the first case
		// just refresh the UI (e.g. the file could have been renamed), otherwise
		// execute the request
		if (data == null)
		{
			refreshContent();
		}
		else
		{
			executeRequest(data);
		}
	}

	private void showErrorToast(int messageRes)
	{
		FeedbackToast.show(getActivity(), false, getString(messageRes));
	}

	private void refreshContent()
	{
		if (!Path.isLocalLocation(mCurrFolder.getLocation())) {
			if (!GenericUtils.isConnectedToInternet(getActivity())) {
				showErrorToast(R.string.tmd_err_no_internet_connection);
				return;
			}
		}
		browseRequest();
	}

	private void executeRequest(Intent i)
	{
		Activity act = getActivity();
		act.startService(i);
		mCurrRequests.add(Contract.extractRequestId(i));
		syncLoadingAnimation();
	}

	private void executeRequestInBackground(Intent i) {
		Activity act = getActivity();
		act.startService(i);
		int requestId = IntentFactory.extractRequestId(i);
		mCurrRequests.add(requestId);
	}

	private void browseRequest()
	{
		Intent i = Contract.buildBrowseRequestIntent(getActivity(), mCurrFolder);
		executeRequest(i);
	}

	private void mkdirRequest(Path folder)
	{
		Intent i = Contract.buildMkdirRequestIntent(getActivity(), folder);
		executeRequest(i);
	}

	private void renameRequest(Path path, String newName)
	{
		Intent i = Contract.buildRenameRequestIntent(getActivity(), path, newName);
		executeRequest(i);
	}

	private void copyRequest(Path[] sources, Path target)
	{
			Intent i = Contract.buildCopyRequestIntent(getActivity(), sources, target);
			executeRequest(i);

	}

	/* for future use
        private void copyRequest(Path[] sources, Path target) {

            int subRequestType = IntentFactory.getDetailCopyRequestType(sources, target);

            if (subRequestType == IntentFactory.REQUEST_UPLOAD) {
                Intent i = IntentFactory.buildUploadRequestIntent(getActivity(), sources, target, false);
                executeRequestInBackground(i);

            } else if (subRequestType == IntentFactory.REQUEST_DOWNLOAD) {
                Intent i = IntentFactory.buildDownloadRequestIntent(getActivity(), sources, target, false);
                executeRequestInBackground(i);
            }
            else {
                Intent i = Contract.buildCopyRequestIntent(getActivity(), sources, target);
                executeRequest(i);
            }
        }
    */


        private void moveRequest(Path[] sources, Path target) {
			mAdapter.removeThumbnailsFromCache(mAdapter.getCheckedItems());
            Intent i = Contract.buildMoveRequestIntent(getActivity(), sources, target);
            executeRequest(i);
        }

        private void deleteRequest(Path[] paths)
        {
            Intent i = Contract.buildDeleteRequestIntent(getActivity(), paths);
            executeRequest(i);
        }

        private void addFavoriteRequest(Path path)
        {
            Intent i = Contract.buildAddFavoriteRequestIntent(getActivity(), path);
            executeRequest(i);
        }

        private void deleteFavoriteRequest(Path path)
        {
            Intent i = Contract.buildDeleteFavoriteRequestIntent(getActivity(), path);
            executeRequest(i);
        }

        private void exportToPdfRequest(Path path)
        {
            Intent i = IntentFactory.buildExportToPdfIntent(getActivity(), path);
            executeRequestInBackground(i);
        }


        private void getSyncFileRequest(Path src, String fileName)
        {
            Intent i = IntentFactory.buildGetSyncFileIntent(getActivity(), src, fileName);
            executeRequestInBackground(i);

        }

        private void zipRequest(Path path)
        {
            Intent i = Contract.buildExportToZipIntent(getActivity(), path);
            executeRequest(i);
        }

        private void uploadPictureRequest(Path.Local[] localPathList)
        {
            Intent i = Contract.buildCopyRequestIntent(getActivity(), localPathList, mCurrFolder);
            executeRequest(i);
        }

        private void setSortRequest(int category, int order)
        {
            Intent i = Contract.buildSetSortRequestIntent(getActivity(), mCurrFolder, category, order);
            executeRequest(i);
        }

        /* non-static */private class BrowsingActionFactory extends BaseActionFactory
	{
		@Override
		public Action buildNewAction()
		{
			if (mHasNewChooser)
			{
				return new NewChooserAction(BrowsingFragment.this,buildIllegalInputForNewFolder());
			}

			return new NewFolderAction(BrowsingFragment.this,buildIllegalInputForNewFolder());
		}

		@Override
		public Action buildSortAction()
		{
			return new SortAction(BrowsingFragment.this, mSortCategory, mSortOrder);
		}

		@Override
		public Action buildShareAction() {
			FileInfo fileInfo = mAdapter.getCheckedItems().get(0);
			return new ShareAction(getFragmentManager(), fileInfo);
		}

		@Override
		public Action buildSmartfileAction() {
			ArrayList<FileInfo> checked = mAdapter.getCheckedItems();
			return new SmarfileAction(BrowsingFragment.this, extractPathsArrayList(checked));
		}

		@Override
		public Action buildMailAction()
		{
			FileInfo fileInfo = mAdapter.getCheckedItems().get(0);
			return new MailAction(getActivity(), fileInfo)
			{
				@Override
				public void execute()
				{
					super.execute();
					finishActionModeIfNeedBe();
				}
			};
		}

		@Override
		public Action buildZipAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					FileInfo fileInfo = mAdapter.getCheckedItems().get(0);
					zipRequest(fileInfo.getPath());
					finishActionModeIfNeedBe();
				}
			};
		}

		@Override
		public Action buildExportToPdfAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					FileInfo fileInfo = mAdapter.getCheckedItems().get(0);
					exportToPdfRequest(fileInfo.getPath());
					finishActionModeIfNeedBe();
				}
			};
		}

		@Override
		public Action buildRenameAction()
		{
			FileInfo fileInfo = mAdapter.getCheckedItems().get(0);
			mRenamingPath = fileInfo.getPath();

			String toRename = mRenamingPath.getName();
			boolean isFolder = fileInfo.getType() == FileInfo.FOLDER;
			HashMap<String, String> illegalInputs = buildIllegalInput(toRename);

			if(!isFolder) {
				//int position = mAdapter.getCheckedPositions().get(0);
				mAdapter.removeThumbnailFromCache(fileInfo);
			}

			return new RenameAction(BrowsingFragment.this, toRename, isFolder, illegalInputs);
		}

		@Override
		public Action buildDeleteAction()
		{
			mAdapter.removeThumbnailsFromCache(mAdapter.getCheckedItems());

			return new Action()
			{
				@Override
				public void execute()
				{
					Path[] checked = extractPathsArray(mAdapter.getCheckedItems());
					deleteRequest(checked);
					finishActionModeIfNeedBe();
				}
			};
		}

		@Override
		public Action buildPrintAction()
		{
			return new PrintAction(getActivity(), mAdapter.getCheckedItems().get(0).getPath());
		}

		@Override
		public Action buildFavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					addFavoriteRequest(mFavoritingPath);
				}
			};
		}

		@Override
		public Action buildUnfavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					deleteFavoriteRequest(mFavoritingPath);
				}
			};
		}

		@Override
		protected Action buildActivityStreamAction() {
			return new Action() {
				@Override
				public void execute() {
					String teamId = ((Path.TeamDrive) mCurrFolder).getTeamDriveId();
					getFragmentManager().beginTransaction().replace(R.id.container,
							TeamDriveASFragment.createInstance(teamId)).setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
							.addToBackStack(null).commit();
				}
			};
		}
	}

	// //////////////////////////////////////////////////////////////////////////////////////////////////
	// Begin thkru's code

	// TODO: @thkru refactor
	private String mOpenInPathname;

	// TODO: @thkru refactor
	private void handleIntentForOpenIn()
	{
		try
		{
			Intent i = getActivity().getIntent();
			mOpenInPathname = i.getStringExtra(OpenInActivity.OPEN_IN_EXTRA);

			if (mOpenInPathname != null)
			{
				Log.i(TAG, "Handle OpenInDlg: " + mOpenInPathname);
				i.removeExtra(OpenInActivity.OPEN_IN_EXTRA);

				ArrayList<Path> openInPath = new ArrayList<Path>(1);
				openInPath.add(Path.Local.fromAbsolutePath(mOpenInPathname));

				SmartfileDialogFragment dlg = SmartfileDialogFragment.newInstance(openInPath);
				dlg.setTargetFragment(this, SmartfileDialogFragment.REQUEST_CODE);
				dlg.show(getFragmentManager(), SmartfileDialogFragment.TAG);
				i.removeExtra(OpenInActivity.OPEN_IN_EXTRA);
			}
		}
		catch (LocalPathNotRecognizedException e)
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.flp_err_source_not_supported));
		}
		catch (Exception e)
		{
			laundryException(e);
		}
	}

	// TODO: @thkru refactor
	@Override
	public boolean isTeamDriveExpired()
	{
		if (mCurrFolder instanceof Path.TeamDrive)
		{
			// TODO: @thkru here there can be a crash:
			// FATAL EXCEPTION: main
			// Process: net.teamplace.android.workplace, PID: 25326
			// java.lang.NullPointerException: Attempt to invoke virtual method
			// 'net.teamplace.android.httplibrary.teamdrive.Payment
			// net.teamplace.android.httplibrary.teamdrive.TeamDrive.getPayment()' on a null object reference
			// at net.teamplace.android.browsing.ui.BrowsingFragment.isTeamDriveExpired(BrowsingFragment.java:1282)
			// at net.teamplace.android.browsing.ui.BrowsingFragment.bindListViewToData(BrowsingFragment.java:846)
			// at net.teamplace.android.browsing.ui.BrowsingFragment.updateFromResult(BrowsingFragment.java:1003)
			// at net.teamplace.android.browsing.ui.BrowsingFragment.onResponseReceived(BrowsingFragment.java:370)
			// at net.teamplace.android.browsing.ui.BrowsingFragment$3.onReceive(BrowsingFragment.java:199)
			// at
			// android.support.v4.content.LocalBroadcastManager.executePendingBroadcasts(LocalBroadcastManager.java:297)
			// at android.support.v4.content.LocalBroadcastManager.access$000(LocalBroadcastManager.java:46)
			// at android.support.v4.content.LocalBroadcastManager$1.handleMessage(LocalBroadcastManager.java:116)
			// at android.os.Handler.dispatchMessage(Handler.java:102)
			// at android.os.Looper.loop(Looper.java:135)
			// at android.app.ActivityThread.main(ActivityThread.java:5221)
			// at java.lang.reflect.Method.invoke(Native Method)
			// at java.lang.reflect.Method.invoke(Method.java:372)
			// at com.android.internal.os.ZygoteInit$MethodAndArgsCaller.run(ZygoteInit.java:899)
			// at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:694)
			String teamDriveId = ((Path.TeamDrive) mCurrFolder).getTeamDriveId();
			Map<String, TeamDrive> teamDriveMap = new SessionManager(getActivity()).getLastSession().getTeamDriveMap();
			TeamDrive teamDrive = teamDriveMap.get(teamDriveId);
			long paidUntil = teamDrive.getPayment().getPaidUntil();
			long now = new Date().getTime();
			if (now > paidUntil)
			{
				return true;
			}
		}
		return false;
	}

	// End thkru's code
	// //////////////////////////////////////////////////////////////////////////////////////////////////

}
