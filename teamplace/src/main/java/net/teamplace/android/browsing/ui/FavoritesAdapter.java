package net.teamplace.android.browsing.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.TeamDriveExpiredDialogFragment;
import net.teamplace.android.browsing.filtering.FilterController;
import net.teamplace.android.browsing.filtering.FilterableAdapter;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.bookmark.Json.EnumerateBookmarks.Bookmark;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.request.thumbnail.ThumbnailRequester;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.ThumbnailHelper;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import static net.teamplace.android.utils.GenericUtils.assertTrue;

class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.ViewHolder> implements FilterableAdapter
{
	private static final String TAG = "FavoritesAdapter";

	private final Context mContext;
	private final ArrayList<Bookmark> mFavorites;
	private final FilterController<Bookmark> mFilterController;
	private OnMenuClickedListener mListener;
	private Map<String, TeamDrive> mTeamDrives;
	private ThumbnailRequester mRequester;
	private DisplayImageOptions mImageOptions;

	FavoritesAdapter(Context context, ArrayList<Bookmark> favorites)
	{
		this(context, favorites, null);
	}

	FavoritesAdapter(Context context, ArrayList<Bookmark> favorites, OnMenuClickedListener listener)
	{
		mContext = context;
		mTeamDrives = new SessionManager(mContext).getLastSession().getTeamDriveMap();
		mFavorites = favorites;
		mFilterController = new FilterController<Bookmark>(favorites,
				new FilterController.ItemConverter<Bookmark>()
				{
					@Override
					public String getFilterableString(Bookmark item, int position)
					{
						return item.getName();
					}
				});

		mRequester = new ThumbnailRequester(context, new SessionManager(context).getLastSession(), new TeamplaceBackend());
		mImageOptions = new DisplayImageOptions.Builder().extraForDownloader(new ThumbnailHelper()).cacheOnDisk(true)
				.cacheInMemory(true).build();

		setOnMenuClicked(listener);
	}

	void setOnMenuClicked(OnMenuClickedListener listener)
	{
		mListener = listener;
	}

	ArrayList<Bookmark> getFavorites()
	{
		return mFavorites;
	}

	@Override
	public boolean isFilterEnabled()
	{
		return false;
	}

	@Override
	public void setFilter(String filter)
	{
		mFilterController.setFilter(filter);
		notifyDataSetChanged();
	}

	@Override
	public void disableFilter()
	{
		mFilterController.disableFilter();
		notifyDataSetChanged();
	}


	// Create new views (invoked by the layout manager)
	@Override
	public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
		// Create a new view.
		View v = LayoutInflater.from(viewGroup.getContext())
				.inflate(R.layout.fav_item, viewGroup, false);

		return new ViewHolder(v);
	}

	// Replace the contents of a view (invoked by the layout manager)
	@Override
	public void onBindViewHolder(ViewHolder viewHolder, int position) {
		Log.d(TAG, "Element " + position + " set.");

		final Bookmark bookmark = (Bookmark) getItem(position);
		final FileInfo fileInfo = new FileInfo(bookmark);
		final Path path = fileInfo.getPath();

		TeamDrive drive = mTeamDrives.get(bookmark.getTeam());
		final boolean isExpired = drive != null && drive.getPayment().getPaidUntil() < new Date().getTime();
		viewHolder.getMenuImage().setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mListener != null) {
					mListener.onMenuClicked(v, bookmark, path);
				}
			}
		});

		// Define click listener for the ItemViewHolder's View.
		viewHolder.getParentView().setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO If we depend on BrowsingActivity, we should use BrowsingActivity as argument in the constructor
				// instead of a generic Context and doing a lot of casting. Of course, it would be better to use an
				// interface here.
				Context ctx = mContext;
				if (fileInfo.isFolder()) {
					assertTrue(ctx instanceof BrowsingActivity,
							"FavoritesAdapter must be used together with BrowsingActivity");
					BrowsingActivity act = (BrowsingActivity) ctx;
					BrowsingActivity.browseAndPush(act.getFragmentManager(), path);
				} else {
					if (isExpired) {
						assertTrue(ctx instanceof BrowsingActivity,
								"FavoritesAdapter must be used together with BrowsingActivity");
						TeamDriveExpiredDialogFragment dlg = TeamDriveExpiredDialogFragment.newInstance(bookmark
								.getTeam());
						dlg.show(((BrowsingActivity) ctx).getFragmentManager(), TeamDriveExpiredDialogFragment.TAG);
					} else {
						if (mListener instanceof FavoritesFragment) {
							((FavoritesFragment) mListener).showPreview(fileInfo, getFileInfoItems());
						}
						// TODO To solve TEP-1076, the appropriate component (BrowsingActivity or FavoritesFragment)

					}
				}
			}
		});

		String name = path.getName();
		viewHolder.getNameTV().setText(name);
		viewHolder.getParentPathTV().setText(PathUtils.displayablePath(mContext, path.getParentPath()));
		viewHolder.getIconImage().setImageDrawable(mContext.getResources().getDrawable(fileInfo.getIconResId()));

		if (!fileInfo.isFolder()) {
			ThumbnailHelper.showThumbnail(fileInfo, viewHolder.getIconImage(), mRequester, mImageOptions);
		}
		viewHolder.getIconImage().setVisibility(View.VISIBLE);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	@Override
	public int getItemCount() {
		return mFilterController.getCount();
	}

	public Object getItem(int position) {
		return mFilterController.getItem(position);
	}

	private ArrayList<FileInfo> getFileInfoItems()
	{
		ArrayList<FileInfo> items = new ArrayList<FileInfo>();
		for (int i = 0; i < getItemCount(); i++)
		{
			items.add(new FileInfo((Bookmark) getItem(i)));
		}

		return items;
	}

	static Path fromBookmark(Bookmark bookmark)
	{
		String teamdriveId = bookmark.getTeam();
		String pathname = bookmark.getName();
		String location = PathUtils.pathLocationFromTeamDriveId(teamdriveId);
		return PathUtils.buildPathfromServerResponseString(location, pathname);
	}

	interface OnMenuClickedListener
	{
		void onMenuClicked(View anchor, Bookmark item, Path bookmarkPath);
	}

	/**
	 * Provide a reference to the type of views that you are using (custom ItemViewHolder)
	 */
	public static class ViewHolder extends RecyclerView.ViewHolder {

		private final ImageView icon;
		private final TextView nameTv;
		private final TextView parentPath;
		private final ImageView menu;
		private final View view;

		public ViewHolder(View v) {
			super(v);
			view = v;

			icon = (ImageView) v.findViewById(R.id.icon);
			nameTv = (TextView) v.findViewById(R.id.name);
			parentPath = (TextView) v.findViewById(R.id.parent);
			menu = (ImageView) v.findViewById(R.id.menu);

		}

		public View getParentView() {
			return view;
		}

		public ImageView getIconImage() {
			return icon;
		}
		public ImageView getMenuImage() {
			return menu;
		}
		public TextView getNameTV() {
			return nameTv;
		}

		public TextView getParentPathTV() {
			return parentPath;
		}
	}
}