package net.teamplace.android.browsing.share;

import static net.teamplace.android.utils.GenericUtils.tag;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.request.share.ShareRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.TeamDriveUtils.InvalidEmailException;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.text.util.Rfc822Tokenizer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.cortado.android.R;

import de.greenrobot.event.EventBus;

public class ShareLinkFragment extends Fragment
{
	private final String TAG = tag(this.getClass());

	public static final String ARG_SHARE_FILE = "argShareFile";
	public static final String ARG_IS_PUBLIC = "argIsPublic";

	private FileInfo shareFileInfo;
	private boolean isPublic;

	private RecipientEditTextView shareTo;
	private EditText subject;
	private EditText mailText;
	private CheckBox preview;

	public static Fragment newInstance(FileInfo shareFile, boolean isPublic)
	{
		ShareLinkFragment fragment = new ShareLinkFragment();
		Bundle arguments = new Bundle();

		arguments.putParcelable(ARG_SHARE_FILE, shareFile);
		arguments.putBoolean(ARG_IS_PUBLIC, isPublic);

		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		shareFileInfo = getArguments().getParcelable(ARG_SHARE_FILE);
		isPublic = getArguments().getBoolean(ARG_IS_PUBLIC);

		View root = inflater.inflate(R.layout.shr_link, container, false);

		int description = isPublic ? R.string.shr_description_public_link : R.string.shr_description_secure_link;
		((TextView) root.findViewById(R.id.tv_share_description)).setText(description);

		shareTo = (RecipientEditTextView) root.findViewById(R.id.et_share_to);
		shareTo.setTokenizer(new Rfc822Tokenizer());
		shareTo.setAdapter(new BaseRecipientAdapter(getActivity())
		{
		});

		subject = (EditText) root.findViewById(R.id.et_share_subject);
		mailText = (EditText) root.findViewById(R.id.et_share_write_text);
		preview = (CheckBox) root.findViewById(R.id.cb_preview);

		return root;
	}

	public void share()
	{
		if (TextUtils.isEmpty(shareTo.getText()))
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.shr_empty_recipients));
		}
		else
		{
			((ShareActivity) getActivity()).enableActionbarProgress();

			new AsyncTask<Void, Void, Void>()
			{
				private String message;
				private String error;
				private Context context;

				@Override
				protected Void doInBackground(Void... params)
				{
					context = getActivity().getApplicationContext();

					String remoteFilePath = PathUtils.remoteRequestString(shareFileInfo.getPath());

					try
					{
						Session session = new SessionManager(context).getLastSession();
						String teamDriveId = PathUtils.getTeamDriveIdOrNull(shareFileInfo.getPath());
						String recipients = ((ShareActivity) getActivity())
								.getEmailAdressesFromChipsText(shareTo.getText());

						new ShareRequester(context, session, new TeamplaceBackend()).shareFileViaJSON(teamDriveId, remoteFilePath,
								recipients, preview.isChecked(), true, isPublic, null, mailText.getText().toString(),
								subject.getText().toString(), 0);

						message = shareFileInfo.getName();

					}
					catch (InvalidEmailException iee)
					{
						error = context.getString(R.string.shr_invalid_email_address);
					}
					catch (Exception e)
					{
						error = String.format(context.getString(R.string.shr_sharing_failed), shareFileInfo.getPath()
								.getName());
						ExceptionManager em = ExceptionManager.getInstance();
						em.handleException(e);
					}
					return null;
				}

				@Override
				protected void onPostExecute(Void result)
				{
					if (message != null)
					{
						FeedbackToast.show(context, true, message);
					}
					if (error != null)
					{
						FeedbackToast.show(context, false, error);
					}

					EventBus.getDefault().postSticky(new ShareActivity.ShareActionCompleteEvent());
				};
			}.execute();
		}
	}
}
