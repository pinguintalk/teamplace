package net.teamplace.android.browsing.utils;

import net.teamplace.android.util.annotations.ThreadSafe;

/**
 * Generates strictly increasing longs and resets to 1 at MAX_VALUE. Useful to generate unique ids
 * 
 * @author Gil Vegliach
 */
@ThreadSafe
public class IdGenerator
{
	public static final int NO_ID = -1;
	private static int sNextId = 1;

	public synchronized static int getId()
	{
		int nextId = sNextId;
		sNextId = (sNextId < Integer.MAX_VALUE) ? sNextId + 1 : 1;
		return nextId;
	}
}