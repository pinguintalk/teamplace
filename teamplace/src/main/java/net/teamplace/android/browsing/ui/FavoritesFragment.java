package net.teamplace.android.browsing.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.filtering.SearchFilterHelper;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.widget.DividerItemDecoration;
import net.teamplace.android.browsing.widget.ListViewSwipeRefreshLayout;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.bookmark.Json.EnumerateBookmarks.Bookmark;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.GraphicsUtils;

import java.util.ArrayList;

import static net.teamplace.android.utils.GenericUtils.pkg;

public class FavoritesFragment extends Fragment implements
		ListViewSwipeRefreshLayout.OnRefreshListener,
		FavoritesAdapter.OnMenuClickedListener,
		DrawerLayout.DrawerListener
{
	private static final String PKG = pkg(FavoritesFragment.class);
	private static final String KEY_FAVORITES = PKG + ".favorites";
	private static final String KEY_CURR_REQUESTS = PKG + ".currRequests";
	private static final String KEY_CURR_FILTER_TEXT = PKG + ".currFilterText";
	private static final String KEY_LAYOUT_MANAGER = "layoutManager";
	private static final int SPAN_COUNT = 2;
	private static final int DATASET_COUNT = 60;

	private enum LayoutManagerType {
		GRID_LAYOUT_MANAGER,
		LINEAR_LAYOUT_MANAGER
	}

	private final ArrayList<Integer> mCurrRequests = new ArrayList<Integer>();
	protected RecyclerView.LayoutManager mLayoutManager;
	protected LayoutManagerType mCurrentLayoutManagerType;
	private RecyclerView mRecyclerView;
	private FavoritesAdapter mAdapter;
	private ListViewSwipeRefreshLayout mSwipeRefreshLayout;
	private TextView mNoFavoritesLabel;
	private final SearchFilterHelper mSearchFilterHelper = new SearchFilterHelper();
	private PopupMenu mPopup;

	/** Curent search text*/
	private String mCurrentFilterText;
	public static FavoritesFragment newInstance()
	{
		return new FavoritesFragment();
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		IntentFilter filter = new IntentFilter(Contract.ACTION_RESPONSE_DELETE_FAVORITE);
		filter.addAction(Contract.ACTION_RESPONSE_ENUMERATE_FAVORITES);
		filter.addAction(Contract.ACTION_RESPONSE_DELETE);
		filter.addAction(Contract.ACTION_RESPONSE_RENAME);
		filter.addAction(Contract.ACTION_RESPONSE_MOVE);
		filter.addAction(Contract.ACTION_RESPONSE_COPY);
		filter.addAction(Contract.ACTION_RESPONSE_EXPORT_TO_ZIP);
		LocalBroadcastManager.getInstance(activity).registerReceiver(mReceiver, filter);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mReceiver);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.fav_fragment, container, false);
		mSwipeRefreshLayout = (ListViewSwipeRefreshLayout) v.findViewById(R.id.swiperefresh);

		// LinearLayoutManager is used here, this will layout the elements in a similar fashion
		// to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
		// elements are laid out.
		mLayoutManager = new LinearLayoutManager(getActivity());

		mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
		mRecyclerView = (RecyclerView) v.findViewById(R.id.recyclerView);
		mRecyclerView.addItemDecoration(
				new DividerItemDecoration(getActivity(), R.drawable.list_divider, LinearLayoutManager.VERTICAL));

		mNoFavoritesLabel = (TextView) v.findViewById(R.id.no_favorites_label);

		mSwipeRefreshLayout.setOnRefreshListener(this);
		mSwipeRefreshLayout.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
		mSwipeRefreshLayout.setSwipeableListViewId(R.id.recyclerView);

		Activity act = getActivity();
		if (savedInstanceState == null)
		{
			mAdapter = new FavoritesAdapter(act, new ArrayList<Bookmark>(), this);
			enumerateFavoritesRequest();
		}
		else
		{
			ArrayList<Bookmark> favorites = savedInstanceState.getParcelableArrayList(KEY_FAVORITES);
			mAdapter = new FavoritesAdapter(act, favorites, this);
			syncNoFavoritesLabel();

			ArrayList<Integer> requestIds = savedInstanceState.getIntegerArrayList(KEY_CURR_REQUESTS);
			mCurrRequests.addAll(requestIds);

			mCurrentFilterText = savedInstanceState.getString(KEY_CURR_FILTER_TEXT);
		}

		setRecyclerViewLayoutManager(mCurrentLayoutManagerType);
		mRecyclerView.setAdapter(mAdapter);
		mSearchFilterHelper.bindFilterableAdapter(mAdapter);

		return v;
	}

	@Override
	public void onPause()
	{
		super.onPause();
		if (mPopup != null)
		{
			mPopup.dismiss();
		}
//		mSearchFilterHelper.collapseSearchView();
		BrowsingActivity act = (BrowsingActivity) getActivity();
		act.unregisterDrawerObserver();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		BrowsingActivity act = (BrowsingActivity) getActivity();
		act.registerDrawerObserver(this);
		Context ctx = act.getApplicationContext();
		if(!GenericUtils.isConnectedToInternet(ctx))
			FeedbackToast.show(ctx,false,ctx.getResources().getString(R.string.tmd_err_no_internet_connection));
		else
			syncProgressBar();
		syncActionBar();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		mCurrentFilterText = mSearchFilterHelper.getCurrFilterString();
		mSearchFilterHelper.collapseSearchView();
		outState.putParcelableArrayList(KEY_FAVORITES, mAdapter.getFavorites());
		outState.putIntegerArrayList(KEY_CURR_REQUESTS, mCurrRequests);
		outState.putString(KEY_CURR_FILTER_TEXT, mCurrentFilterText);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.fav_main_menu, menu);

		MenuItem searchItem = menu.findItem(R.id.action_search);
		mSearchFilterHelper.bindSearchMenuItem(searchItem);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub
		// super.onActivityResult(requestCode, resultCode, data);
		int code = requestCode;
		if (resultCode == Activity.RESULT_CANCELED)
		{
			// show nothing here
			return;
		}
		if (resultCode != Activity.RESULT_OK)
		{
			FeedbackToast feedbackToast = new FeedbackToast(getActivity());
			feedbackToast.setIconBgRes(R.drawable.brw_error_circle);
			feedbackToast.setIconRes(R.drawable.delete);
			feedbackToast.setTitle(getString(R.string.ntf_title_error));
			feedbackToast.setMessage(getString(R.string.ntf_msg_unknown_error));
			feedbackToast.show();
			return;
		}

		if (requestCode == PreviewActivity.REQUEST_CODE)
		{
			handlePreviewResult(data);
		}

	}

	private void handlePreviewResult(Intent data)
	{
		// PreviewActivity can return either no data or some request. In the first case
		// just refresh the UI (e.g. the file could have been renamed), otherwise
		// execute the request
		if (data != null)
		{
			executeRequest(data);
		}
		else
			enumerateFavoritesRequest();
	}


	private void executeRequest(Intent i)
	{
		Activity act = getActivity();
		act.startService(i);
		mCurrRequests.add(Contract.extractRequestId(i));
		syncProgressBar();
	}

	@Override
	public void onRefresh()
	{
		if(!GenericUtils.isConnectedToInternet(getActivity()))
			FeedbackToast.show(getActivity(),false,getActivity().getResources().getString(R.string.tmd_err_no_internet_connection));
		else
			enumerateFavoritesRequest();
	}

	@Override
	public void onMenuClicked(View anchor, Bookmark item, final Path bookmarkPath)
	{
		if (mPopup != null)
			mPopup.dismiss();

		mPopup = new PopupMenu(getActivity(), anchor);
		mPopup.inflate(R.menu.fav_item_menu);
		mPopup.show();
		mPopup.setOnDismissListener(new PopupMenu.OnDismissListener()
		{
			@Override
			public void onDismiss(PopupMenu menu)
			{
				mPopup = null;
			}
		});

		mPopup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			@Override
			public boolean onMenuItemClick(MenuItem item) {
				int id = item.getItemId();
				if (id == R.id.action_unfavorite) {
					deleteFavoriteRequest(bookmarkPath);
					return true;
				} else if (id == R.id.action_go_to_parent) {
					Path parent = bookmarkPath.getParentPath();
					FragmentManager fm = getFragmentManager();
					/* TEP-1516
					// select item in NavigationDrawer
					Navigation navigation = (NavigationFragment) fm.findFragmentById(R.id.navigation);
					if (navigation != null)
					{
						if (Path.isRemoteLocation(bookmarkPath.getLocation()))
						{
							navigation.select(TopLevel.REMOTE);
						}
						else if (Path.isLocalLocation(bookmarkPath.getLocation()))
						{
							navigation.select(TopLevel.LOCAL);
						}
						else if (Path.isTeamDriveLocation(bookmarkPath.getLocation()))
						{
							navigation.select(TopLevel.TEAMDRIVES);
						}
					}
					 */

					BrowsingActivity.browseAndPush(fm, parent);
					return true;
				}
				return false;
			}
		});
	}

	/**
	 * Set RecyclerView's LayoutManager to the one given.
	 *
	 * @param layoutManagerType Type of layout manager to switch to.
	 */
	public void setRecyclerViewLayoutManager(LayoutManagerType layoutManagerType) {
		int scrollPosition = 0;

		// If a layout manager has already been set, get current scroll position.
		if (mRecyclerView.getLayoutManager() != null) {
			scrollPosition = ((LinearLayoutManager) mRecyclerView.getLayoutManager())
					.findFirstCompletelyVisibleItemPosition();
		}

		switch (layoutManagerType) {
			case GRID_LAYOUT_MANAGER:
				mLayoutManager = new GridLayoutManager(getActivity(), SPAN_COUNT);
				mCurrentLayoutManagerType = LayoutManagerType.GRID_LAYOUT_MANAGER;
				break;
			case LINEAR_LAYOUT_MANAGER:
				mLayoutManager = new LinearLayoutManager(getActivity());
				mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
				break;
			default:
				mLayoutManager = new LinearLayoutManager(getActivity());
				mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
		}

		mRecyclerView.setLayoutManager(mLayoutManager);
		mRecyclerView.scrollToPosition(scrollPosition);
	}
	@Override
	public void onDrawerStateChanged(int newState)
	{
		if (newState == DrawerLayout.STATE_DRAGGING)
		{
			mSearchFilterHelper.collapseSearchView();
		}
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset)
	{
		// Nothing
	}

	@Override
	public void onDrawerOpened(View drawerView)
	{
		// Nothing
	}

	@Override
	public void onDrawerClosed(View drawerView)
	{
		// Nothing
	}

	private void enumerateFavoritesRequest()
	{
		Activity act = getActivity();
		Intent i = Contract.buildEnumerateFavoriteSRequestIntent(act);
		mCurrRequests.add(Contract.extractRequestId(i));
		act.startService(i);
		syncProgressBar();
	}

	private void deleteFavoriteRequest(Path path)
	{
		Activity act = getActivity();
		Intent i = Contract.buildDeleteFavoriteRequestIntent(act, path);
		mCurrRequests.add(Contract.extractRequestId(i));
		act.startService(i);
		syncProgressBar();
	}

	private void syncActionBar()
	{
		Activity act = getActivity();
		if (!(act instanceof BrowsingActivity))
		{
			return;
		}

		BrowsingActivity browsingActivity = (BrowsingActivity) act;
		browsingActivity.setActionBarTitle(R.string.abt_favorites);
		browsingActivity.setActionBarSubtitle(null);
	}

	private void syncProgressBar()
	{
		GraphicsUtils.setRefreshing(mSwipeRefreshLayout, !mCurrRequests.isEmpty());
	}

	private void syncNoFavoritesLabel()
	{
		if (mAdapter.getItemCount() < 1)
		{
			mRecyclerView.setVisibility(View.GONE);
			mNoFavoritesLabel.setVisibility(View.VISIBLE);
		}
		else
		{
			mRecyclerView.setVisibility(View.VISIBLE);
			mNoFavoritesLabel.setVisibility(View.GONE);
		}
	}

	private void updateBookmarksFromResult(ArrayList<Bookmark> bookmarks)
	{
		// Updates the adapter
		mAdapter = new FavoritesAdapter(getActivity(), new ArrayList<Bookmark>(bookmarks), FavoritesFragment.this);
		mRecyclerView.setAdapter(mAdapter);
		mSearchFilterHelper.bindFilterableAdapter(mAdapter);

		// Restores search results
		String currFilter = mSearchFilterHelper.getCurrFilterString();
		if (!TextUtils.isEmpty(currFilter))
		{
			mAdapter.setFilter(currFilter);
			mCurrentFilterText = currFilter;
		}
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			// TODO: between onAttach() and onCreate(), mCurrRequests is empty and does not reflect the real state

			int id = Contract.extractRequestId(intent);
			if (id == Contract.NO_ID || !mCurrRequests.remove((Integer) id))
			{
				syncProgressBar();
				return;
			}

			String action = intent.getAction();
			if (Contract.ACTION_RESPONSE_ENUMERATE_FAVORITES.equals(action))
			{
				ArrayList<Bookmark> bookmarks = intent.getParcelableArrayListExtra(Contract.RESPONSE_FAVORITES);
				if (bookmarks != null)
				{
					updateBookmarksFromResult(bookmarks);
					syncNoFavoritesLabel();
					syncProgressBar();
				}

			}
			else if (Contract.ACTION_RESPONSE_DELETE_FAVORITE.equals(action)
					|| Contract.ACTION_RESPONSE_DELETE.equals(action)
					|| Contract.ACTION_RESPONSE_RENAME.equals(action)
					|| Contract.ACTION_RESPONSE_MOVE.equals(action)
					|| Contract.ACTION_RESPONSE_COPY.equals(action))
			{
				enumerateFavoritesRequest();
			}

		}
	};

	public void showPreview(FileInfo fileInfo, ArrayList<FileInfo> itemList) {
		// must be started with startActivityForResult().
		Intent i = PreviewActivity.buildIntent(this.getActivity(), fileInfo,
				new PreviewConfiguration().folderContent(itemList));
		startActivityForResult(i, PreviewActivity.REQUEST_CODE);
	}


	public String getmCurrentFilterText() {
		return mCurrentFilterText;
	}



}
