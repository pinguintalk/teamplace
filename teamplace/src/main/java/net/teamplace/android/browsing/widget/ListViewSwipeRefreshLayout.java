package net.teamplace.android.browsing.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ListView;

import com.cortado.android.R;

/**
 * <p>
 * A SwipeRefreshLayout that contains a ListView, possibly nested in child layouts. The swipe-to-refresh logic is based
 * on the ListView, so there is no functionality if the ListView is not set. This can be done in code with
 * {@link #setSwipeableListViewId(int)} or in XML with the attribute {@code swipeableListViewId}.
 * <p/>
 * <p>
 * Bear in mind that when you set {@code swipeableListViewId} the id must exist or be created. Therefore you are likely
 * to write something like {@code <ListViewSwipeRefreshLayout custom:swipeableListViewId=@+id/some_list_view ...>} in
 * ListViewSwipeRefreshLayout and {@code <ListView android:id="@id/some_list_view ...>} in some child ListView (notice
 * the '+')
 * </p>
 */
final public class ListViewSwipeRefreshLayout extends SwipeRefreshLayout
{
	private View mTarget;
	private ListView mListView;
	private int mId;

	public ListViewSwipeRefreshLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.ListViewSwipeRefreshLayout, 0, 0);

		try
		{
			mId = a.getResourceId(R.styleable.ListViewSwipeRefreshLayout_swipeableListViewId, 0);
		}
		finally
		{
			a.recycle();
		}
	}

	public ListViewSwipeRefreshLayout(Context context)
	{
		super(context);
	}

	@Override
	protected void onFinishInflate()
	{
		super.onFinishInflate();

		// findViewById() will fail if called too early, so this waits until xml inflation is finished
		if (mId != 0)
			setSwipeableListViewId(mId);
	}

	public void setSwipeableListViewId(int id)
	{
		mId = id;
		// mListView = (ListView) findViewById(id);
		mTarget = findViewById(id);
	}

	/*
	@Override
	public boolean canChildScrollUp()
	{
		if (mListView == null || !mListView.isShown() || !mListView.canScrollVertically(-1))
			return false;

		return true;
	}
	*/

	// allow scroll up and down of RecycleView
	//http://stackoverflow.com/questions/27641359/scrolling-swiperefreshlayout-with-recyclerview-refresh-anywhere-in-android-2-2
	@Override
	public boolean canChildScrollUp() {
		if (mTarget instanceof ListView) {
			mListView = (ListView) mTarget;
			if (mListView == null || !mListView.isShown() || !mListView.canScrollVertically(-1))
				return false;

			return true;
		}
		else if (mTarget instanceof RecyclerView) {
			final RecyclerView recyclerView = (RecyclerView) mTarget;
			RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
			if (layoutManager instanceof LinearLayoutManager) {
				int position = ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition();
				return position != 0;
			} else if (layoutManager instanceof StaggeredGridLayoutManager) {
				int[] positions = ((StaggeredGridLayoutManager) layoutManager).findFirstCompletelyVisibleItemPositions(null);
				for (int i = 0; i < positions.length; i++) {
					if (positions[i] == 0) {
						return false;
					}
				}
			}
			return true;
		} else if (android.os.Build.VERSION.SDK_INT < 14) {
			if (mTarget instanceof AbsListView) {
				final AbsListView absListView = (AbsListView) mTarget;
				return absListView.getChildCount() > 0
						&& (absListView.getFirstVisiblePosition() > 0 || absListView.getChildAt(0)
						.getTop() < absListView.getPaddingTop());
			} else {
				return mTarget.getScrollY() > 0;
			}
		} else {
			return ViewCompat.canScrollVertically(mTarget, -1);
		}
	}
}
