package net.teamplace.android.browsing.share;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.utils.BasicFragmentActivity;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.TeamDriveUtils;
import net.teamplace.android.utils.TeamDriveUtils.InvalidEmailException;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.cortado.android.R;

import de.greenrobot.event.EventBus;

@SuppressWarnings("deprecation")
public class ShareActivity extends BasicFragmentActivity implements ActionBar.TabListener
{
	private static final String TAG = tag(ShareActivity.class);
	private static final String PKG = pkg(ShareActivity.class);
	private static final String KEY_SHARE_ITEM = PKG + ".shareItem";

	public static final String STATE_IN_PROGRESS = "state_in_progress";

	private final int MAX_FILE_SIZE = 5 * 1024 * 1024;

	private FileInfo fileInfo;
	private boolean actionInProgress = false;

	private SharePagerAdapter adapter;
	private ViewPager viewPager;

	private static boolean removeStickyEvents;

	private static final String[] forbiddenFileExtensions = new String[] {
			".ade", ".adp", ".app", ".asp", ".bas", ".bat", ".cer", ".chm", ".cmd", ".com", ".cpl", ".crt",
			".csh", ".der", ".exe", ".fxp", ".gadget", ".hlp", ".hta", ".inf", ".ins", ".isp", ".its",
			".js", ".jse", ".ksh", ".lib", ".lnk", ".mad", ".maf", ".mag", ".mam", ".maq", ".mar",
			".mas", ".mat", ".mau", ".mav", ".maw", ".mda", ".mdb", ".mde", ".mdt", ".mdw", ".mdz",
			".msc", ".msh", ".msh1", ".msh2", ".mshxml", ".msh1xml", ".msh2xml", ".msi", ".msp",
			".mst", ".ops", ".pcd", ".pif", ".plg", ".prf", ".prg", ".reg", ".scf", ".scr", ".sct",
			".shb", ".shs", ".sys", ".ps1", ".ps1xml", ".ps2", ".ps2xml", ".psc1", ".psc2", ".tmp",
			".url", ".vb", ".vbe", ".vbs", ".vps", ".vsmacros", ".vss", ".vst", ".vsw", ".vxd", ".ws",
			".wsc", ".wsf", ".wsh", ".xnk"
	};

	public static Intent buildIntent(Context context, FileInfo fileInfo)
	{
		checkNonNullArgs(context, fileInfo);
		Intent i = new Intent(context, ShareActivity.class);
		i.putExtra(ShareActivity.KEY_SHARE_ITEM, fileInfo);
		removeStickyEvents = true;
		return i;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.shr_activity);

		if (removeStickyEvents)
			removeStickyEvents();

		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(getString(R.string.shr_title));
		actionBar.setStackedBackgroundDrawable(new ColorDrawable(0xff2d3858));

		if (savedInstanceState != null)
		{
			actionInProgress = savedInstanceState.getBoolean(STATE_IN_PROGRESS, false);

			if (actionInProgress)
			{
				enableActionbarProgress();
			}
		}

		fileInfo = (FileInfo) getIntent().getParcelableExtra(KEY_SHARE_ITEM);

		adapter = new SharePagerAdapter(this, getSupportFragmentManager(), fileInfo, forbiddenFile(fileInfo),
				forbiddenFileType(fileInfo));

		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(adapter);
		viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener()
		{
			@Override
			public void onPageSelected(int position)
			{
				actionBar.setSelectedNavigationItem(position);
				invalidateOptionsMenu();
			}
		});

		for (int i = 0; i < adapter.getCount(); i++)
		{
			actionBar.addTab(actionBar.newTab().setText(adapter.getPageTitle(i)).setTabListener(this));
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		outState.putBoolean(STATE_IN_PROGRESS, actionInProgress);
		super.onSaveInstanceState(outState);
	}

	private void removeStickyEvents()
	{
		removeStickyEvents = false;

		EventBus.getDefault().removeStickyEvent(ShareActionCompleteEvent.class);
		EventBus.getDefault().removeStickyEvent(ShareShowLinkFragment.LinkGenerationCompleteEvent.class);
	}

	public void onEventMainThread(ShareActionCompleteEvent event)
	{
		EventBus.getDefault().removeStickyEvent(event);
		disableActionbarProgress();
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction)
	{
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		getMenuInflater().inflate(R.menu.shr_context_menu, menu);
		return true;
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu)
	{
		if (actionInProgress)
		{
			disableActionShare(menu);
		}

		int currentItem = viewPager.getCurrentItem();

		if (currentItem == 0 && forbiddenFile(fileInfo))
		{
			disableActionShare(menu);
		}

		if (currentItem == 3)
		{
			disableActionShare(menu);
		}

		return true;
	}

	private void disableActionShare(Menu menu)
	{
		menu.findItem(R.id.action_share).setEnabled(false);
		menu.findItem(R.id.action_share).setVisible(false);
	}

	private String getFragmentTag(int viewPagerId, int fragmentPosition)
	{
		return "android:switcher:" + viewPagerId + ":" + fragmentPosition;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.action_share)
		{
			int currentItem = viewPager.getCurrentItem();
			// TODO hacky solution:
			// http://stackoverflow.com/questions/14035090/how-to-get-existing-fragments-when-using-fragmentpageradapter
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(getFragmentTag(R.id.pager, currentItem));

			switch (currentItem)
			{
				case 0:
					((ShareAttachmentFragment) fragment).share();
					break;
				case 1:
					((ShareLinkFragment) fragment).share();
					break;
				case 2:
					((ShareLinkFragment) fragment).share();
					break;
				default:
					break;
			}
		}
		return true;
	}

	/**
	 * Parses a {@code CharSequence} from a Chips TextView (i.e. {@code RecipientEditTextView}) into an array of
	 * {@code String}'s
	 * 
	 * @throws InvalidEmailException
	 *             if format of contacts in cs is wrong
	 */
	public String getEmailAdressesFromChipsText(CharSequence cs) throws InvalidEmailException
	{
		if (TextUtils.isEmpty(cs))
			return null;

		StringBuilder builder = new StringBuilder();

		Pattern chipPattern = Pattern.compile(".*?<(.*?)>");
		String[] tokens = cs.toString().split(",");

		for (String token : tokens)
		{
			String trimmed = token.trim();
			if (TextUtils.isEmpty(trimmed))
				continue;

			String email = null;
			// We accept valid emails, "non-chipified" input, e.g. jan@cortado.com
			if (GenericUtils.isValidEmail(trimmed))
			{
				email = trimmed;
			}
			else
			{
				// If the text is not valid, try to extract the email from a chipified input,
				// e.g. "Johannes <johannes@cortado.com>"
				Matcher matcher = chipPattern.matcher(trimmed);
				if (matcher.find())
				{
					String candidate = matcher.group(1);
					if (GenericUtils.isValidEmail(candidate))
					{
						email = candidate;
					}
				}

				if (email == null)
				{
					// Otherwise fail
					throw new TeamDriveUtils.InvalidEmailException("Wrong format in Chips? Was: " + cs.toString());
				}
			}

			builder.append(email + ";");
		}

		String emails = builder.toString();

		return emails.substring(0, emails.length() - 1);
	}

	private boolean forbiddenFile(FileInfo fileInfo)
	{
		if (forbiddenFileSize(fileInfo) || forbiddenFileType(fileInfo))
			return true;

		return false;
	}

	private boolean forbiddenFileSize(FileInfo fileInfo)
	{
		if (fileInfo.getSize() > MAX_FILE_SIZE)
			return true;

		return false;
	}

	private boolean forbiddenFileType(FileInfo fileInfo)
	{
		if (Arrays.asList(forbiddenFileExtensions).contains(getFileExtension(fileInfo)))
			return true;

		return false;
	}

	private String getFileExtension(FileInfo fileInfo)
	{
		String strName = fileInfo.getName();
		String extension = "";

		if (strName.contains("."))
		{
			extension = strName.substring(strName.lastIndexOf("."));
		}

		return extension;
	}

	public void enableActionbarProgress()
	{
		actionInProgress = true;
		invalidateOptionsMenu();
		setProgressBarIndeterminate(true);
		setProgressBarIndeterminateVisibility(true);
	}

	public void disableActionbarProgress()
	{
		actionInProgress = false;
		invalidateOptionsMenu();
		setProgressBarIndeterminate(false);
		setProgressBarIndeterminateVisibility(false);
	}

	public static class ShareActionCompleteEvent
	{
	}
}
