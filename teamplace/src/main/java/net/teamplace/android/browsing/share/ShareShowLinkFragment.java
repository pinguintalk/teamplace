package net.teamplace.android.browsing.share;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.TeamDrive;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.request.share.ShareRequester;
import net.teamplace.android.http.request.share.ShareResponse;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.Log;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

public class ShareShowLinkFragment extends Fragment
{
	private final String TAG = tag(this.getClass());

	public static final String ARG_SHARE_FILE = "argShareFile";

	private FileInfo shareFileInfo;
	private boolean inProgress = false;
	private Button btnGenerateLink;
	private EditText etGeneratedLink;
	private ProgressBar progress;

	public static Fragment newInstance(FileInfo shareFile)
	{
		ShareShowLinkFragment fragment = new ShareShowLinkFragment();
		Bundle arguments = new Bundle();

		arguments.putParcelable(ARG_SHARE_FILE, shareFile);

		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void onResume()
	{
		EventBus.getDefault().registerSticky(this);
		super.onResume();
	}

	@Override
	public void onPause()
	{
		EventBus.getDefault().unregister(this);
		super.onPause();
	}

	public void onEventMainThread(LinkGenerationCompleteEvent event)
	{
		EventBus.getDefault().removeStickyEvent(event);
		stopLinkGeneration(event.getLink());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		shareFileInfo = getArguments().getParcelable(ARG_SHARE_FILE);

		if (savedInstanceState != null)
		{
			inProgress = savedInstanceState.getBoolean(ShareActivity.STATE_IN_PROGRESS, false);
		}

		final View root = inflater.inflate(R.layout.shr_show_link, container, false);

		((TextView) root.findViewById(R.id.tv_share_description)).setText(R.string.shr_description_show_link);

		final CheckBox preview = (CheckBox) root.findViewById(R.id.cb_preview);
		btnGenerateLink = (Button) root.findViewById(R.id.btn_gen_link);
		etGeneratedLink = (EditText) root.findViewById(R.id.et_generated_link);
		progress = (ProgressBar) root.findViewById(R.id.pb_progress);

		if (inProgress)
		{
			progress.setVisibility(View.VISIBLE);
			etGeneratedLink.setText("");
			etGeneratedLink.setHint("");
		}

		btnGenerateLink.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				final String copied = getString(R.string.shr_copied_to_clipboard);
				final String error = getString(R.string.shr_error);

				etGeneratedLink.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{

						if (etGeneratedLink.getText().toString().equals(error))
						{
							Log.i(TAG, "Nothin' to copy");
							return;
						}

						copyToClipboard(getActivity(), etGeneratedLink.getText().toString());

						FeedbackToast.show(getActivity(), true, copied);
					}
				});

				startLinkGeneration();

				new AsyncTask<Void, Void, String>()
				{
					private String exception;
					private String message;
					private Context context;

					@Override
					protected String doInBackground(Void... params)
					{
						context = getActivity().getApplicationContext();

						Path path = shareFileInfo.getPath();
						String remoteFilePath = PathUtils.remoteRequestString(path);
						String teamId = Path.isTeamDriveLocation(path.getLocation()) ? ((TeamDrive) path)
								.getTeamDriveId() : null;
						String strRetMe = error;

						try
						{
							Session session = new SessionManager(context).getLastSession();
							ShareResponse[] response = new ShareRequester(context, session, new TeamplaceBackend()).shareFileViaJSON(
									teamId, remoteFilePath, "", preview.isChecked(), false, false, null, "", "", 0);

							strRetMe = response[0].getShareLink();
							message = copied;
						}
						catch (Exception e)
						{
							this.exception = String.format(context.getString(R.string.shr_sharing_failed),
									shareFileInfo.getPath().getName());
							ExceptionManager em = ExceptionManager.getInstance();
							em.handleException(e);
						}

						return strRetMe;
					}

					@Override
					protected void onPostExecute(String result)
					{
						if (exception != null)
						{
							FeedbackToast.show(context, false, exception);
						}
						if (message != null)
						{
							copyToClipboard(context, result);
							FeedbackToast.show(context, true, message);
						}

						EventBus.getDefault().postSticky(new LinkGenerationCompleteEvent(result));
					}

				}.execute();
			}
		});

		return root;
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putBoolean(ShareActivity.STATE_IN_PROGRESS, inProgress);
		super.onSaveInstanceState(outState);
	}

	private void startLinkGeneration()
	{
		inProgress = true;
		progress.setVisibility(View.VISIBLE);
		etGeneratedLink.setText("");
		etGeneratedLink.setHint("");
		btnGenerateLink.setEnabled(false);
		btnGenerateLink.setAlpha(0.5f);
	}

	private void stopLinkGeneration(String result)
	{
		inProgress = false;
		progress.setVisibility(View.INVISIBLE);
		etGeneratedLink.setText(result);
		btnGenerateLink.setEnabled(true);
		btnGenerateLink.setAlpha(1.f);
	}

	private void copyToClipboard(Context context, String text)
	{
		ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText(context.getString(R.string.shr_copied_text), text);
		clipboard.setPrimaryClip(clip);
	}

	public class LinkGenerationCompleteEvent
	{
		private String link;

		public LinkGenerationCompleteEvent(String link)
		{
			this.link = link;
		}

		public String getLink()
		{
			return link;
		}
	}
}
