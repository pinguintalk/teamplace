package net.teamplace.android.browsing.sorting;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SortDatabaseHelper extends SQLiteOpenHelper
{
	private static final String DATABASE_NAME = "sort_preferences.db";
	private static final int DATABASE_VERSION = 1;

	private static SortDatabaseHelper sInstance;

	public synchronized static SortDatabaseHelper getInstance(Context context)
	{
		if (sInstance == null)
			sInstance = new SortDatabaseHelper(context);

		return sInstance;
	}

	private SortDatabaseHelper(Context context)
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database)
	{
		SortTable.create(database);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
	{
		SortTable.upgrade(database, oldVersion, newVersion);
	}
}
