package net.teamplace.android.teamdrive.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.widget.ListViewSwipeRefreshLayout;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.NoConnectivityException;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.Rights;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.teamdrive.ui.TeamDriveSelectAdapter.OnTeamActionsMenuClickListener;
import net.teamplace.android.utils.CloudCentralHelper;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.TeamDriveUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

public class TeamDriveSelectFragment extends Fragment implements
		OnRefreshListener, OnItemClickListener
{
	public static final String TAG = tag(TeamDriveSelectFragment.class);

	private static final int REQ_CODE_INVITATION = 984165135;

	private TeamDriveSelectAdapter mAdapter;
	private ListViewSwipeRefreshLayout mSwipeRefreshLayout;
	private ListView mListView;
	private ViewSwitcher mViewSwitcher;
	private Menu mMenu;
	private State mState = new State();
	private StateResolver mStateResolver = new StateResolver();

	private static class State
	{
		private boolean isAddAllowed = false;
		private int freeDrivesUsed, freeDrivesAllowed;
		private long latestRequestCode = System.currentTimeMillis();
		private GlobalConfiguration globalConfig;
		private long teamDriveCacheRequestCode;
		private long teamDriveNetworkRequestCode;
		private long invitationsCacheRequestCode;
		private long invitationsNetworkRequestCode;
		private long globalConfigNetworkRequestCode;
		private long globalConfigCacheRequestCode;
		private boolean waitingForTeamDrivesFromCache = false;
		private boolean waitingForTeamDrivesFromNetwork = false;
		private boolean waitingForInvitationsFromCache = false;
		private boolean waitingForInvitationsFromNetwork = false;
		private boolean waitingForGlobalConfigFromNetwork = false;
		private boolean waitinfForGlobalConfigFromCache = false;
		private boolean waitingForInvitationFeedbackResponse = false;
		private List<Object> objects;
	}

	private class StateResolver
	{
		public void onEventMainThread(State event)
		{
			mState = event;
		}
	}

	public static TeamDriveSelectFragment newInstance()
	{
		return new TeamDriveSelectFragment();
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		EventBus.getDefault().registerSticky(mStateResolver);
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.tmd_select_fragment, container, false);
		mSwipeRefreshLayout = (ListViewSwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
		mSwipeRefreshLayout.setSwipeableListViewId(R.id.browser_list);
		mSwipeRefreshLayout.setOnRefreshListener(this);
		mSwipeRefreshLayout.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
		mViewSwitcher = (ViewSwitcher) v.findViewById(R.id.tmd_select_vs);
		mViewSwitcher.findViewById(R.id.empty_view).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				createTeamDriveIfAllowed();
			}
		});
		mListView = (ListView) v.findViewById(R.id.browser_list);
		mListView.setOnItemClickListener(this);
		return v;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		registerToEventBus();

		mAdapter = new TeamDriveSelectAdapter(getActivity(),
				mState.objects != null ? mState.objects
						: new ArrayList<>());
		mAdapter.setOnTeamActionsMenuClickedListener(new OnTeamActionsMenuClickListener() {
			@Override
			public void onActionsMenuClicked(Object drive, View anchor, View driveView) {
				Fragment frag = TeamDriveManageFragment.newInstance(((TeamDrive) drive).getId());
				FragmentManager fm = getFragmentManager();
				fm.beginTransaction().replace(R.id.container, frag)
						.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE).addToBackStack(null).commit();
			}
		});
		mListView.setAdapter(mAdapter);

		if (savedInstanceState == null || (savedInstanceState != null && mState.objects.size() == 0))
		{
			Log.d(TAG, "Starting TeamDriveService");


			if (!GenericUtils.isConnectedToInternet(getActivity()))
				showErrorToast(R.string.tmd_err_no_internet_connection);
			else {
				GraphicsUtils.setRefreshing(mSwipeRefreshLayout, true);
				requestGlobalConfigFromCache();
				requestGlobalConfigFromServer();
				requestTeamDrivesFromCache();
				requestTeamDrivesFromServer();
				requestInvitationsFromCache();
				requestInvitationsFromServer();
			}

		}
		syncViewSwitcher();

		// TEP-1058 avoid calling onPrepareOptionsMenu twice : here and in BrowserActivity
		if (mMenu != null) {
			MenuItem menuItem = mMenu.findItem(R.id.action_new_drive);
			if (menuItem != null)
				mMenu.findItem(R.id.action_new_drive).setVisible(false).setEnabled(false);
			// TEP-1534 disable it first before request global configuration
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		setUpActionBarTitle();
	}

	@Override
	public void onResume()
	{
		super.onResume();
		registerToEventBus();
	}

	@Override
	public void onPause()
	{
		EventBus.getDefault().unregister(this);
		super.onPause();
	}

	@Override
	public void onRefresh()
	{
		if (!GenericUtils.isConnectedToInternet(getActivity()))
			showErrorToast(R.string.tmd_err_no_internet_connection);
		else {
			requestGlobalConfigFromCache();
			requestGlobalConfigFromServer();
			requestTeamDrivesFromCache();
			requestTeamDrivesFromServer();
			requestInvitationsFromCache();
			requestInvitationsFromServer();
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQ_CODE_INVITATION)
		{
			switch (resultCode)
			{
				case TeamDriveInvitationActivity.RESULT_ACCEPT_REQUESTED:
				case TeamDriveInvitationActivity.RESULT_REJECT_REQUESTED:
					mState.waitingForInvitationFeedbackResponse = true;
					setRefreshingStatus();
					break;
				default:
					break;
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.tmd_select_fragment_menu, menu);
		mMenu = menu;
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == R.id.action_new_drive)
		{
			createTeamDriveIfAllowed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		int count = mAdapter == null ? 0 : mAdapter.getCount();
		Object[] outObjects = new Object[count];
		for (int i = 0; i < count; i++)
		{
			outObjects[i] = mAdapter.getItem(i);
		}
		mState.objects = new ArrayList<>(Arrays.asList(outObjects));
		EventBus.getDefault().unregister(mStateResolver);
		EventBus.getDefault().postSticky(mState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id)
	{
		Object item = mAdapter.getItem(position);
		if (item instanceof TeamDrive)
		{
			TeamDrive drive = (TeamDrive) item;
			Path.TeamDrive path = Path.teamDriveRoot(drive.getId());
			BrowsingActivity.browseAndPush(getFragmentManager(), path);
		}
		else if (item instanceof IncomingInvitation)
		{
			IncomingInvitation invitation = (IncomingInvitation) item;
			TeamDriveInvitationActivity.startForResult(this, invitation, REQ_CODE_INVITATION);
		}
	}

	private void createTeamDriveIfAllowed()
	{
		//GlobalConfiguration globalConfig = new SessionManager(getActivity()).getLastSession()
		//		.getTeamDriveGlobalConfig();
		//TEP-1534
		if (mState.globalConfig == null)  return;

		if (!mState.globalConfig.getRights().can(Rights.CREATE_TEAMDRIVE))
		{
			if (mState.globalConfig.getPotentialRights().can(Rights.CREATE_TEAMDRIVE))
			{
				showUnverifiedDialog();
			}
		}
		else if (mState.freeDrivesAllowed - mState.freeDrivesUsed <= 0)
		{
			showMaxFreeTeamDrivesReachedDialog(mState.freeDrivesAllowed);
		}
		else
		{
			boolean canAddMembers = mState.globalConfig.getRights().can(Rights.ADD_USER);
			Fragment frag = TeamDriveNewDriveFragment.newInstance(canAddMembers);
			FragmentManager fm = getFragmentManager();
			fm.beginTransaction()
					.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
					.replace(R.id.container, frag)
					.addToBackStack(null)
					.commit();
		}
	}

	private void requestTeamDrivesFromCache()
	{
		mState.teamDriveCacheRequestCode = generateRequestCode();
		mState.waitingForTeamDrivesFromCache = true;
		TeamDriveService.getAllTeamDrivesFromCache(getActivity(), mState.teamDriveCacheRequestCode);
	}

	private void requestTeamDrivesFromServer()
	{
		mState.teamDriveNetworkRequestCode = generateRequestCode();
		// waitingForTeamDrivesFromNetwork = true;
		// we get response only if something changed
		TeamDriveService.getAllTeamDrivesSkipCacheQuery(getActivity(), mState.teamDriveNetworkRequestCode);
	}

	private void requestInvitationsFromCache()
	{
		mState.invitationsCacheRequestCode = generateRequestCode();
		mState.waitingForInvitationsFromCache = true;
		TeamDriveService.getInvitationsFromCache(getActivity(), mState.invitationsCacheRequestCode);
	}

	private void requestInvitationsFromServer()
	{
		mState.invitationsNetworkRequestCode = generateRequestCode();
		mState.waitingForInvitationsFromNetwork = true;
		TeamDriveService.getInvitationsFromServer(getActivity(), mState.invitationsNetworkRequestCode);
	}

	private void requestGlobalConfigFromCache()
	{
		mState.globalConfigCacheRequestCode = generateRequestCode();
		mState.waitinfForGlobalConfigFromCache = true;
		TeamDriveService.getGlobalConfigurationFromCache(getActivity(), mState.globalConfigCacheRequestCode);
	}

	private void requestGlobalConfigFromServer()
	{
		mState.globalConfigNetworkRequestCode = generateRequestCode();
		mState.waitingForGlobalConfigFromNetwork = true;
		TeamDriveService.getGlobalConfigurationFromServer(getActivity(), mState.globalConfigNetworkRequestCode);
	}

	private long generateRequestCode()
	{
		return ++mState.latestRequestCode;
	}

	public void onEventMainThread(TeamDriveServiceResponse event)
	{
		long responseCode = event.getRequestCode();
		if (responseCode == 0) return;

		if (responseCode == mState.globalConfigCacheRequestCode)
		{
			handleGlobalConfigCacheResponse(event);
		}
		else if (responseCode == mState.globalConfigNetworkRequestCode)
		{
			handleGlobalConfigNetworkResponse(event);
		}
		else if (responseCode == mState.teamDriveCacheRequestCode)
		{
			handleTeamDriveCacheResponse(event);
		}
		else if (responseCode == mState.teamDriveNetworkRequestCode)
		{
			handleTeamDriveNetworkResponse(event);
		}
		else if (responseCode == mState.invitationsCacheRequestCode)
		{
			handleInvitationsCacheResponse(event);
		}
		else if (responseCode == mState.invitationsNetworkRequestCode)
		{
			handleInvitationsNetworkResponse(event);
		}
		else if (responseCode == TeamDriveInvitationActivity.REQ_ACCEPT_INVITATION
				|| responseCode == TeamDriveInvitationActivity.REQ_REJECT_INVITATION)
		{
			handleInvitationFeedbackRespose(event);
		}
	}

	private void handleGlobalConfigCacheResponse(TeamDriveServiceResponse event)
	{
		mState.waitinfForGlobalConfigFromCache = false;
		updateGlobalConfig(event);
		setRefreshingStatus();
	}

	private void handleGlobalConfigNetworkResponse(TeamDriveServiceResponse event)
	{
		mState.waitingForGlobalConfigFromNetwork = false;
		updateGlobalConfig(event);
		setRefreshingStatus();
	}

	private void updateGlobalConfig(TeamDriveServiceResponse event)
	{
		try
		{
			mState.globalConfig = (GlobalConfiguration) event.getResponseData();
			mState.isAddAllowed = mState.globalConfig.getPotentialRights().can(
					Rights.CREATE_TEAMDRIVE);
			mState.freeDrivesAllowed = mState.globalConfig.getMaxFreeDrives();
			// TEP-1058
			if (mMenu != null)
				mMenu.findItem(R.id.action_new_drive).setVisible(mState.isAddAllowed).setEnabled(mState.isAddAllowed);
			getActivity().invalidateOptionsMenu();
		}
		catch (NoConnectivityException e){
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_no_internet_connection);
		}
		catch (ConnectFailedException e){
			e.printStackTrace();
			showErrorToast(R.string.ini_connect_to_server_failed);
		}
		catch (SQLException | CortadoException | IOException e)
		{
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_loading_configuration_failed);
		}
	}

	private void handleTeamDriveCacheResponse(TeamDriveServiceResponse event)
	{
		mState.waitingForTeamDrivesFromCache = false;
		updateTeamDrives(event);
		setRefreshingStatus();
	}

	private void handleTeamDriveNetworkResponse(TeamDriveServiceResponse event)
	{
		mState.waitingForTeamDrivesFromNetwork = false;
		updateTeamDrives(event);
		setRefreshingStatus();
	}

	private void updateTeamDrives(TeamDriveServiceResponse event)
	{
		Object responseData = null;
		try
		{
			responseData = event.getResponseData();
			mAdapter.clearTeamDrives();
			mAdapter.addAll(responseData != null ? Arrays.asList((TeamDrive[]) responseData) : new ArrayList<Parcelable>(0));
			syncViewSwitcher();
			mAdapter.notifyDataSetChanged();
			mAdapter.sort();
			mState.freeDrivesUsed = countFreeDrives((TeamDrive[]) responseData);
		}
		catch (NoConnectivityException e) {
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_no_internet_connection);
		}
		catch (ConnectFailedException e){
			e.printStackTrace();
			showErrorToast(R.string.ini_connect_to_server_failed);
		}
		catch (SQLException | CortadoException | IOException e)
		{
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_loading_teamdrives_failed);
		}
	}

	private void handleInvitationsCacheResponse(TeamDriveServiceResponse event)
	{
		mState.waitingForInvitationsFromCache = false;
		updateInvitations(event);
		setRefreshingStatus();
	}

	private void handleInvitationsNetworkResponse(TeamDriveServiceResponse event)
	{
		mState.waitingForInvitationsFromNetwork = false;
		updateInvitations(event);
		setRefreshingStatus();
	}

	private void updateInvitations(TeamDriveServiceResponse event)
	{
		Object responseData;
		try
		{
			responseData = event.getResponseData();
			mAdapter.clearIncomingInvitations();
			mAdapter.addAll(responseData != null ? Arrays.asList((IncomingInvitation[]) responseData) : new ArrayList<>(0));
			syncViewSwitcher();
			mAdapter.notifyDataSetChanged();
			mAdapter.sort();
		}
		catch (NoConnectivityException e) {
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_no_internet_connection);
		}
		catch (ConnectFailedException e){
			showErrorToast(R.string.ini_connect_to_server_failed);
		}
		catch (SQLException | CortadoException | IOException e)
		{
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_loading_invitations_failed);
		}
	}

	private void handleInvitationFeedbackRespose(TeamDriveServiceResponse event)
	{
		mState.waitingForInvitationFeedbackResponse = false;
		try
		{
			event.getResponseData();
			requestInvitationsFromServer();
			requestTeamDrivesFromServer();
		}
		catch (NoConnectivityException e) {
			e.printStackTrace();
			showErrorToast(R.string.tmd_err_no_internet_connection);
		}
		catch (SQLException | CortadoException | IOException e)
		{
			e.printStackTrace();
			FeedbackToast.show(getActivity().getApplicationContext(), false, "");
		}
		setRefreshingStatus();
	}

	private void setRefreshingStatus()
	{
		mSwipeRefreshLayout.setRefreshing(isWaiting());
	}

	private boolean isWaiting()
	{
		return mState.waitinfForGlobalConfigFromCache || mState.waitingForInvitationFeedbackResponse
				|| mState.waitingForInvitationsFromCache || mState.waitingForInvitationsFromNetwork
				|| mState.waitingForTeamDrivesFromNetwork || mState.waitingForTeamDrivesFromCache;
	}

	private void syncViewSwitcher()
	{
		int indexEmpty = mViewSwitcher.indexOfChild(mViewSwitcher.findViewById(R.id.empty_view));
		int indexList = mViewSwitcher.indexOfChild(mViewSwitcher.findViewById(R.id.browser_list));

		if (mAdapter.getCount() == 0 && !isWaiting())
		{
			mViewSwitcher.setDisplayedChild(indexEmpty);
		}
		else
		{
			mViewSwitcher.setDisplayedChild(indexList);
		}
	}

	private void registerToEventBus()
	{
		EventBus bus = EventBus.getDefault();
		if (!bus.isRegistered(this))
		{
			bus.register(this);
		}
	}

	private void setUpActionBarTitle()
	{
		BrowsingActivity act = ((BrowsingActivity) getActivity());
		act.setActionBarTitle(R.string.abt_teamdrives);
		act.setActionBarSubtitle(null);
	}

	private void showUnverifiedDialog()
	{
		StyleableAlertDialog dialog = new StyleableAlertDialog(getActivity(), 0);
		dialog.setTitle(getString(R.string.tmd_creation_not_allowed_title));
		dialog.setMessage(getString(R.string.tmd_creation_not_allowed_message));
		dialog.setButton(DialogInterface.BUTTON_NEUTRAL, getString(R.string.app_ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dialog.dismiss();
					}
				});
		dialog.show();
	}

	private void showErrorToast(int messageRes)
	{
		FeedbackToast.show(getActivity(), false, getString(messageRes));
	}

	private void showMaxFreeTeamDrivesReachedDialog(int maxDrives)
	{
		String title = getString(R.string.tmd_max_free_teamdrives_reached_title);
		String message = String.format(getString(R.string.tmd_max_free_teamdrives_reached_message), maxDrives);
		StyleableAlertDialog dlg = new StyleableAlertDialog(getActivity());
		dlg.setTitle(title);
		dlg.setMessage(message);
		dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.app_close),
				(DialogInterface.OnClickListener) null);
		dlg.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.tmd_manage_upgrade),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						CloudCentralHelper.startTeamdriveUpgrade(getActivity(), "");
						dialog.dismiss();
					}
				});
		dlg.show();
	}

	private int countFreeDrives(TeamDrive[] teamDrives)
	{
		String currentUser = new SessionManager(getActivity()).getLastSession().getCredentials().getUsername();
		int currentUsersFreeDrives = 0;
		for (TeamDrive drive : teamDrives)
		{
			if (currentUser.equals(TeamDriveUtils.getOwner(drive.getUsers()).getEmail()) && drive.getPayment().isFree())
			{
				currentUsersFreeDrives++;
			}
		}
		return currentUsersFreeDrives;
	}
}
