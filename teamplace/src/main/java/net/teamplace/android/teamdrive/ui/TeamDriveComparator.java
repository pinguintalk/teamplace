package net.teamplace.android.teamdrive.ui;

import java.util.Comparator;
import java.util.Locale;

import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.TeamDrive;
import android.os.Parcelable;

/** Standard comparator to order teamdrives */
public final class TeamDriveComparator implements Comparator<Object>
{
	@Override
	public int compare(Object lhs, Object rhs)
	{
		if (lhs instanceof TeamDrive && rhs instanceof TeamDrive)
		{
			return compareNames(((TeamDrive) lhs).getName(), ((TeamDrive) rhs).getName());
		}
		else if (lhs instanceof IncomingInvitation && rhs instanceof IncomingInvitation)
		{
			return compareNames(((IncomingInvitation) lhs).getTeamDriveName(),
					((IncomingInvitation) rhs).getTeamDriveName());
		}
		else if (lhs instanceof IncomingInvitation && rhs instanceof TeamDrive)
		{
			return -1;
		}
		else if (rhs instanceof IncomingInvitation && lhs instanceof TeamDrive)
		{
			return 1;
		}
		return 0;
	}

	private int compareNames(String lhsName, String rhsName)
	{
		if (lhsName == null && rhsName != null)
		{
			return -1;
		}
		else if (lhsName != null && rhsName == null)
		{
			return 1;
		}
		else if (lhsName == null && rhsName == null)
		{
			return 0;
		}
		return (lhsName.toUpperCase(Locale.US).compareTo(rhsName.toUpperCase(Locale.US)));
	}
}