package net.teamplace.android.teamdrive;

import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Pair;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NoConnectivityException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.OperationFailedException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.teamdrive.ActivityRequester;
import net.teamplace.android.http.request.teamdrive.QuotaRequester;
import net.teamplace.android.http.request.teamdrive.TeamdriveRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.DriveCreation;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.UserListInvitation;
import net.teamplace.android.http.teamdrive.activity.ActivityResponse;
import net.teamplace.android.http.teamdrive.database.TableInvitationEtags;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;
import net.teamplace.android.http.teamdrive.quota.AccountQuotaModel;
import net.teamplace.android.http.teamdrive.quota.TeamdriveQuotaModel;
import net.teamplace.android.utils.GenericUtils;

import org.apache.http.HttpStatus;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

import de.greenrobot.event.EventBus;

public class TeamDriveService extends MultiThreadIntentService
{

	public static final String NAME = "TeamDriveService";

	private static final String ACTION_GET_ALL_TEAMDRIVES_SKIP_CACHE_QUERY = "get all teamdrives, skip cache query";
	private static final String ACTION_GET_ALL_TEAMDRIVES_FROM_CACHE = "get all teamdrives from cache";
	private static final String ACTION_GET_TEAMDRIVE = "get teamdrive";
	private static final String ACTION_CREATE_TEAMDRIVE = "create teamdrive";
	private static final String ACTION_GET_INVITATIONS_FROM_CACHE = "get invitations from cache";
	private static final String ACTION_GET_INVITATIONS_FROM_SERVER = "get invitations from server";
	private static final String ACTION_INVITE_USERS = "invite users";
	private static final String ACTION_ACCEPT_INVITATION = "accept invitation";
	private static final String ACTION_REJECT_INVITATION = "reject invitation";
	private static final String ACTION_GET_USER_IMAGE = "get user image";
	private static final String ACTION_GET_USER_IMAGE_FROM_CACHE = "get user image from cache";
	private static final String ACTION_GET_GLOBAL_CONFIGURATION_FROM_SERVER = "get global configuration from server";
	private static final String ACTION_GET_GLOBAL_CONFIGURATION_FROM_CACHE = "get global configuration from cache";
	private static final String ACTION_GET_ACTIVITIES_FOR_TEAMDRIVE = "get activities for teamdrive";
	private static final String ACTION_GET_MORE_ACTIVITIES = "get more activities";
	private static final String ACTION_GET_TEAMDRIVE_QUOTA = "get teamdrive quota";
	private static final String ACTION_GET_ACOUNT_QUOTA = "get account quota";


	private static final String EXTRA_TEAMDRIVE_ID = "teamdrive id";
	private static final String EXTRA_TEAMDRIVE_IDS = "teamdrive ids";
	private static final String EXTRA_TEAMDRIVE = "teamdrive";
	private static final String EXTRA_OUTGOING_INVITATION = "outgoing invitation";
	private static final String EXTRA_INVITATION_ID = "invitation id";
	private static final String EXTRA_REQUEST_CODE = "request code";
	private static final String EXTRA_USER = "user";
	private static final String EXTRA_PATH = "path";
	private static final String EXTRA_IMAGE_SIZE = "image size";
	private static final String EXTRA_REQUEST_ID = "request id";
	private static final String EXTRA_NEXT_ACTIVITIES_LINK = "next activities link";

	private Worker teamDriveWorker;
	private Worker userImgDownloadWorker;
	private Worker globalConfigWorker;
	private Worker teamDriveCacheWorker;
	private Worker invitationCacheWorker;
	private Worker globalConfigCacheWorker;
	private Worker activityWorker;
	private Worker quotaWorker;

	private TeamDriveDatabaseActions dbActions;

	private final ReentrantLock imgFileLock = new ReentrantLock(true);

	public TeamDriveService()
	{
		super(NAME);
		dbActions = TeamDriveDatabaseActions.getInstance();
	}

	private class GenericWorker extends Worker
	{
		public GenericWorker(String name)
		{
			super(name);
		}

		@Override
		void onHandleIntent(Intent intent)
		{
			doTheWork(intent);
		}
	}

	@Override
	protected void instantiateWorkers()
	{
		teamDriveWorker = new GenericWorker("teamDriveWorker");
		teamDriveCacheWorker = new GenericWorker("teamDriveCacheWorker");
		userImgDownloadWorker = new GenericWorker("userImgDownloadWorker");
		globalConfigWorker = new GenericWorker("globalConfigWorker");
		invitationCacheWorker = new GenericWorker("invitationCacheWorker");
		globalConfigCacheWorker = new GenericWorker("globalConfigCacheWorker");
		activityWorker = new GenericWorker("activityWorker");
		quotaWorker = new GenericWorker("quotaWorker");
	}

	@Override
	protected Worker getWorkerForIntent(Intent intent)
	{
		switch (intent.getAction())
		{
			case ACTION_GET_USER_IMAGE:
				return userImgDownloadWorker;
			case ACTION_GET_ALL_TEAMDRIVES_FROM_CACHE:
				return teamDriveCacheWorker;
			case ACTION_GET_GLOBAL_CONFIGURATION_FROM_SERVER:
				return globalConfigWorker;
			case ACTION_GET_GLOBAL_CONFIGURATION_FROM_CACHE:
				return globalConfigCacheWorker;
			case ACTION_GET_INVITATIONS_FROM_CACHE:
				return invitationCacheWorker;
			case ACTION_GET_ACTIVITIES_FOR_TEAMDRIVE:
			case ACTION_GET_MORE_ACTIVITIES:
				return activityWorker;
			case ACTION_GET_ACOUNT_QUOTA:
			case ACTION_GET_TEAMDRIVE_QUOTA:
				return quotaWorker;
			default:
				return teamDriveWorker;
		}
	}

	@Override
	protected Worker[] getAllWorkers()
	{
		return new Worker[] {teamDriveWorker, teamDriveCacheWorker, userImgDownloadWorker, globalConfigWorker,
				invitationCacheWorker, globalConfigCacheWorker, activityWorker, quotaWorker};
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		return super.onStartCommand(intent, flags, startId);
	}

	// TODO former IntentService.onHandleIntent(). Split this up into specific Worker.onHandleIntent() methods.
	private void doTheWork(Intent intent)
	{
		Exception exc = null;
		Object result = null;

		long requestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, TeamDriveServiceResponse.NO_REQUEST_CODE);
		Map<String, Object> requestData = getRequestData(requestCode);

		if(!GenericUtils.isConnectedToInternet(getApplicationContext())) {
			exc = new NoConnectivityException();
			postEvent(requestCode, exc, result);
			return;
		}

		Session session = new SessionManager(getApplicationContext()).getLastSession();
		// TODO This is a temporary fix avoiding crashes due to a null Session. Session handling should
		// rather be refactored.
		if (session == null) {
			return;
		}
		TeamdriveRequester requester = new TeamdriveRequester(getApplicationContext(), session, new TeamplaceBackend());

		try
		{
			String action = intent.getAction();
			switch (action)
			{
				case ACTION_GET_GLOBAL_CONFIGURATION_FROM_SERVER:
					result = getGlobalConfiguration(requester);
					break;
				case ACTION_GET_GLOBAL_CONFIGURATION_FROM_CACHE:
					Pair<GlobalConfiguration, String> dbResult = dbActions.getGlobalConfigurationAndEtag();
					result = (dbResult != null) ? dbResult.first : null;
					break;
				case ACTION_GET_ALL_TEAMDRIVES_SKIP_CACHE_QUERY:
					String oldEtag = dbActions.getEtagForAllTeamDrives();
					result = getAllTeamDrives(requester,oldEtag);
					String currEtag = dbActions.getEtagForAllTeamDrives();
					if (currEtag == null || currEtag.equals(oldEtag)) {
						// nothing change, don't post
						return;
					}
					break;
				case ACTION_GET_ALL_TEAMDRIVES_FROM_CACHE:
					result = dbActions.getAllTeamDrives();
					break;
				case ACTION_CREATE_TEAMDRIVE:
					result = createTeamDrive((DriveCreation) requestData.get(EXTRA_TEAMDRIVE), requester);
					break;
				case ACTION_GET_TEAMDRIVE:
					TeamDrive td = getTeamDriveFromServer((String) requestData.get(EXTRA_TEAMDRIVE_ID), requester);
					result = new TeamDrive[] {(td != null) ? td : null};
					break;
				case ACTION_GET_INVITATIONS_FROM_CACHE:
					result = dbActions.getInvitations();
					break;
				case ACTION_GET_INVITATIONS_FROM_SERVER:
					result = getInvitations(requester);
					break;
				case ACTION_INVITE_USERS:
					inviteUsers(requester, (String) requestData.get(EXTRA_TEAMDRIVE_ID),
							(UserListInvitation) requestData.get(EXTRA_OUTGOING_INVITATION));
					break;
				case ACTION_ACCEPT_INVITATION:
					acceptInvitation(requester, (String) requestData.get(EXTRA_INVITATION_ID));
					break;
				case ACTION_REJECT_INVITATION:
					rejectInvitation(requester, (String) requestData.get(EXTRA_INVITATION_ID));
					break;
				case ACTION_GET_USER_IMAGE:
				{
					User user = (User) requestData.get(EXTRA_USER);
					long requestId = (long) requestData.get(EXTRA_REQUEST_ID);
					result = downloadUserImage(requester, user, (Integer) requestData.get(EXTRA_IMAGE_SIZE), requestId);
				}
					break;
				case ACTION_GET_USER_IMAGE_FROM_CACHE:
				{
					User user = (User) requestData.get(EXTRA_USER);
					long requestId = (long) requestData.get(EXTRA_REQUEST_ID);
					result = new Parcelable[]{new UserImageItem(readImage(getUserImageFile(user, false)),
							user.getUserId(), false, requestId)};
				}
					break;
				case ACTION_GET_ACTIVITIES_FOR_TEAMDRIVE:
					result = getActivities(new ActivityRequester(getApplicationContext(), session, new TeamplaceBackend()),
							(String) requestData.get(EXTRA_TEAMDRIVE_ID));
					break;
				case ACTION_GET_MORE_ACTIVITIES:
					result = getMoreActivities(new ActivityRequester(getApplicationContext(), session, new TeamplaceBackend()),
							(String) requestData.get(EXTRA_NEXT_ACTIVITIES_LINK));
					break;
				case ACTION_GET_ACOUNT_QUOTA:
					result = getAccountQuota(new QuotaRequester(getApplicationContext(), session, new TeamplaceBackend()));
					break;
				case ACTION_GET_TEAMDRIVE_QUOTA:
					result = getTeamDriveQuota(new QuotaRequester(getApplicationContext(), session, new TeamplaceBackend()),
							(String[]) requestData.get(EXTRA_TEAMDRIVE_IDS));
					break;
				default:
					break;
			}
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
			exc = e;
		}
		finally
		{
			postEvent(requestCode, exc, result);
		}
	}

	private GlobalConfiguration getGlobalConfiguration(TeamdriveRequester requester)
			throws JsonParseException, JsonMappingException, XCBStatusException, HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		GlobalConfiguration result = null;
		Pair<GlobalConfiguration, String> cache = dbActions.getGlobalConfigurationAndEtag();
		String etag = cache != null ? cache.second : null;
		try
		{
			Pair<GlobalConfiguration, String> responseData = requester.getGlobalConfiguration(etag);
			dbActions.insertOrUpdateGlobalConfiguration(responseData.first, responseData.second);
			result = responseData.first;
		}
		catch (HttpResponseException e)
		{
			if (e.getResponseCode() == HttpStatus.SC_NOT_MODIFIED)
			{
				result = cache.first;
			}
			else
			{
				throw e;
			}
		}
		return result;
	}

	private TeamDrive[] getAllTeamDrives(TeamdriveRequester requester, String etag) throws JsonParseException,
			JsonMappingException, XCBStatusException, MalformedURLException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException,
			HttpResponseException, RedirectException {
		TeamDrive[] result = null;
		try
		{

			Pair<TeamDrive[], String> responseData = requester.getTeamDrives(etag,
					TeamdriveRequester.RequestFilters.ALL_BUT_IMGDATA);

			dbActions.overwriteTeamDriveDb(responseData.first == null ? new TeamDrive[0] : responseData.first);
			dbActions.insertAllTeamDrivesEtag(responseData.second);
			// TeamDriveDatabaseActions.matchTeamDrives(db, responseData.first == null ? new TeamDrive[0] : responseData.first);
			result = responseData.first;
		}
		catch (HttpResponseException e)
		{
			if (e.getResponseCode() == HttpStatus.SC_NOT_MODIFIED)
			{
				result = dbActions.getAllTeamDrives();
			}
			else
			{
				throw e;
			}

		}
		updateSessionWithTeamDrives(result, getGlobalConfiguration(requester));
		return result;
	}

	private TeamDrive getTeamDriveFromServer(String teamDrieveId, TeamdriveRequester requester)
			throws JsonParseException, JsonMappingException,
			XCBStatusException, MalformedURLException, ConnectFailedException,
			LogonFailedException, NotificationException,
			ServerLicenseException, IOException, HttpResponseException, RedirectException {
		TeamDrive result = null;
		try
		{
			// TODO have to add to server
			String etag = dbActions.getEtagForTeamDrive(teamDrieveId);
			Pair<TeamDrive, String> responseData = requester.getTeamDrive(teamDrieveId, etag, null);
			result = responseData.first;
			dbActions.insertOrUpdateTeamDrives(result);
			updateTeamDriveInSession(result);
		}
		catch (HttpResponseException e)
		{
			throw e;

		}

		return result;
	}

	private TeamDrive[] createTeamDrive(DriveCreation toCreate, TeamdriveRequester requester)
			throws JsonParseException, JsonMappingException, XCBStatusException,
			HttpResponseException, MalformedURLException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException, UnsupportedEncodingException,
			JsonProcessingException, IOException, OperationFailedException, RedirectException {
		TeamDrive[] result = null;
		TeamDrive created = requester.createTeamDrive(toCreate);
		if (created != null)
		{
			result = new TeamDrive[] {created};
			dbActions.insertOrUpdateTeamDrives(created);
			String etag = dbActions.getEtagForAllTeamDrives();
			// TODO: @jobol Forces to update the session with the newly created teamdrive. This method should be
			// optimized to not execute another network request. See blocker: https://jira.thinprint.de/browse/TEP-358
			getAllTeamDrives(requester,etag);
		}
		else
		{
			throw new OperationFailedException("Team Drive could not be created.");
		}
		return result;
	}

	private IncomingInvitation[] getInvitations(TeamdriveRequester requester) throws XCBStatusException,
			HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		IncomingInvitation[] result = null;
		try
		{
			String etag = dbActions.getEtagForAllInvitations();
			Pair<IncomingInvitation[], String> responseData = requester.getInvitations(etag);
			result = responseData.first;
			dbActions.overwriteInvitationsTable(responseData.first);
			dbActions.insertOrUpdateInvitationEtag(TableInvitationEtags.ID_ETAG_ALL_INVITATIONS, etag);
		}
		catch (HttpResponseException e)
		{
			if (e.getResponseCode() == HttpStatus.SC_NOT_MODIFIED)
			{
				result = dbActions.getInvitations();
			}
			else
			{
				throw e;
			}
		}
		return result;
	}

	private void inviteUsers(TeamdriveRequester requester, String driveId, UserListInvitation invitations)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
			UnsupportedEncodingException, JsonProcessingException, IOException, RedirectException {
		requester.inviteUsersToTeamDrive(driveId, invitations);
	}

	private void acceptInvitation(TeamdriveRequester requester, String invitationId)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		requester.acceptInvitation(invitationId);
	}

	private void rejectInvitation(TeamdriveRequester requester, String invitationId)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		requester.rejectInvitation(invitationId);
	}

	private ActivityResponse getActivities(ActivityRequester requester, String teamDriveId) throws HttpResponseException,
			ServerLicenseException, XCBStatusException, IOException, ConnectFailedException, LogonFailedException,
			NotificationException, RedirectException {
		return requester.getActivities(teamDriveId);
	}

	private ActivityResponse getMoreActivities(ActivityRequester requester, String nextLink) throws HttpResponseException,
			ServerLicenseException, IOException, XCBStatusException, LogonFailedException, ConnectFailedException,
			NotificationException, RedirectException {
		return requester.getMoreActivities(nextLink);
	}

	private TeamdriveQuotaModel[] getTeamDriveQuota(QuotaRequester requester, String... teamDriveIds) throws HttpResponseException,
			XCBStatusException, IOException, ConnectFailedException, ServerLicenseException, LogonFailedException,
			RedirectException, NotificationException {
		return requester.getTeamDriveQuota(teamDriveIds);
	}

	private AccountQuotaModel getAccountQuota(QuotaRequester requester) throws HttpResponseException, ServerLicenseException,
			XCBStatusException, IOException, ConnectFailedException, LogonFailedException, RedirectException, NotificationException {
		return requester.getAccountQuota();
	}

	private Parcelable[] downloadUserImage(TeamdriveRequester requester, User user, int imgSize,
			long requestId) throws XCBStatusException, HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		if (user.getImgPath() == null)
		{
			return null;
		}
		String etag = dbActions.getUserImageEtag(user.getUserId());
		File tempFile = getUserImageFile(user, true);
		File outFile = getUserImageFile(user, false);
		Pair<Boolean, String> result = requester.getUserImage(user, tempFile, imgSize, etag);
		dbActions.updateUserImageEtag(user.getUserId(), result.second);
		Parcelable[] imgResult = new Parcelable[] {new UserImageItem(cropToCircle(tempFile, outFile), user.getUserId(),
				true, requestId)};
		tempFile.delete();
		return imgResult;
	}

	private File getUserImageFile(User user, boolean tempFile)
	{
		File dir = getFilesDir();
		String namePrefix = tempFile ? "." : "";
		String name = namePrefix + user.getUserId();
		return new File(dir, name);
	}

	private Bitmap readImage(File imgFile)
	{
		if (imgFile.exists())
		{
			imgFileLock.lock();
			try
			{
				return BitmapFactory.decodeFile(imgFile.getAbsolutePath());
			}
			finally
			{
				imgFileLock.unlock();
			}
		}
		return null;
	}

	private Bitmap cropToCircle(File in, File out)
	{
		Bitmap toCrop = BitmapFactory.decodeFile(in.getAbsolutePath());
		Bitmap output = Bitmap.createBitmap(toCrop.getWidth(), toCrop.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, toCrop.getWidth(), toCrop.getHeight());

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		canvas.drawCircle(toCrop.getWidth() / 2, toCrop.getHeight() / 2, toCrop.getWidth() / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(toCrop, rect, rect, paint);

		OutputStream os = null;
		imgFileLock.lock();
		try
		{
			os = new FileOutputStream(out);
			output.compress(Bitmap.CompressFormat.PNG, 95, os);
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		finally
		{
			if (os != null)
			{
				try
				{
					os.close();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
			imgFileLock.unlock();
		}

		return output;
	}

	private void updateSessionWithTeamDrives(TeamDrive[] teamDrives, GlobalConfiguration config)
	{
		new SessionManager(getApplicationContext()).updateCachedSessionWithTeamDrives(teamDrives, config);
	}

	private void updateTeamDriveInSession(TeamDrive drive)
	{
		new SessionManager(getApplicationContext()).updateTeamDriveInCachedSession(drive);
	}

	private void postEvent(long requestCode, Exception e, Object responseObj)
	{
		EventBus.getDefault().post(new TeamDriveServiceResponse(requestCode, e, responseObj));
	}

	/**
	 * Same as {@link TeamDriveService#getAllTeamDrives(Context, String, long)}, but will skip the cache query. EventBus
	 * will return a {@link TeamDriveServiceResponse} with {@link TeamDrive}[] payload
	 * 
	 * @param ctx
	 * @param requestCode
	 */
	public static void getAllTeamDrivesSkipCacheQuery(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_ALL_TEAMDRIVES_SKIP_CACHE_QUERY, requestCode, null, null);
	}

	/**
	 * Same as {@link TeamDriveService#getAllTeamDrives(Context, String, long)}, but will skip the server query.
	 * EventBus will return a {@link TeamDriveServiceResponse} with {@link TeamDrive}[]
	 * payload
	 * 
	 * @param ctx
	 * @param requestCode
	 */
	public static void getAllTeamDrivesFromCache(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_ALL_TEAMDRIVES_FROM_CACHE, requestCode, null, null);
	}

	/**
	 * EventBus will return a {@link TeamDriveServiceResponse} with {@link TeamDrive} payload
	 * 
	 * @param ctx
	 * @param teamDriveId
	 * @param requestCode
	 */
	public static void getTeamDrive(Context ctx, String teamDriveId, long requestCode)
	{
		startMe(ctx, ACTION_GET_TEAMDRIVE, requestCode,
				new String[]{EXTRA_TEAMDRIVE_ID}, new Object[]{teamDriveId});
	}

	/**
	 * EventBus will return a {@link TeamDriveServiceResponse} with {@link TeamDrive} payload
	 * 
	 * @param ctx
	 * @param toCreate
	 * @param requestCode
	 */
	public static void createTeamDrive(Context ctx, DriveCreation toCreate, long requestCode)
	{
		startMe(ctx, ACTION_CREATE_TEAMDRIVE, requestCode,
				new String[]{EXTRA_TEAMDRIVE}, new Object[]{toCreate});
	}

	/**
	 * EventBus will return two {@link TeamDriveServiceResponse}s with {@link IncomingInvitation}[] payload: The first
	 * from the cache query, the second from the server data.
	 * 
	 * @param ctx
	 * @param server
	 * @param requestCode
	 */
	public static void getInvitationsFromServer(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_INVITATIONS_FROM_SERVER, requestCode, null, null);
	}

	/**
	 * EventBus will return two {@link TeamDriveServiceResponse}s with {@link IncomingInvitation}[] payload: The first
	 * from the cache query, the second from the server data.
	 * 
	 * @param ctx
	 * @param server
	 * @param requestCode
	 */
	public static void getInvitationsFromCache(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_INVITATIONS_FROM_CACHE, requestCode, null, null);
	}

	/**
	 * EventBus will return a {@link TeamDriveServiceResponse} with a null payload
	 * 
	 * @param ctx
	 * @param teamDriveId
	 * @param toInvite
	 * @param requestCode
	 */
	public static void inviteUsers(Context ctx, String teamDriveId, UserListInvitation toInvite, long requestCode)
	{
		startMe(ctx, ACTION_INVITE_USERS, requestCode,
				new String[]{EXTRA_OUTGOING_INVITATION, EXTRA_TEAMDRIVE_ID}, new Object[]{toInvite, teamDriveId});
	}

	/**
	 * EventBus will return a {@link TeamDriveServiceResponse} with a null payload
	 * 
	 * @param ctx
	 * @param invitationId
	 * @param requestCode
	 */
	public static void acceptInvitation(Context ctx, String invitationId, long requestCode)
	{
		startMe(ctx, ACTION_ACCEPT_INVITATION, requestCode,
				new String[]{EXTRA_INVITATION_ID}, new Object[]{invitationId});
	}

	/**
	 * EventBus will return a {@link TeamDriveServiceResponse} with a null payload
	 * 
	 * @param ctx
	 * @param invitationId
	 * @param requestCode
	 */
	public static void rejectInvitation(Context ctx, String invitationId, long requestCode)
	{
		startMe(ctx, ACTION_REJECT_INVITATION, requestCode,
				new String[]{EXTRA_INVITATION_ID}, new Object[]{invitationId});
	}

	/**
	 * EventBus will return two {@link TeamDriveServiceResponse}s with a {@link UserImageItem} payload: The first one
	 * frome the cache query, the second one from the server. {@link TeamDriveServiceResponse#getResponseData()} will
	 * throw a {@link HttpResponseException} with error code 304, if the server data hasn't changed.
	 * 
	 * @param ctx
	 * @param user
	 * @param imgSize
	 * @param requestCode
	 */
	public static void getUserImage(Context ctx, User user, int imgSize, long requestCode, long requestId)
	{
		startMe(ctx, ACTION_GET_USER_IMAGE, requestCode,
				new String[]{EXTRA_USER, EXTRA_IMAGE_SIZE, EXTRA_REQUEST_ID}, new Object[]{user, imgSize, requestId});
	}

	public static void getUserImageFromCache(Context ctx, User user, int imgSize, long requestCode, long requestId)
	{
		startMe(ctx, ACTION_GET_USER_IMAGE_FROM_CACHE, requestCode,
				new String[]{EXTRA_USER, EXTRA_IMAGE_SIZE, EXTRA_REQUEST_ID}, new Object[]{user, imgSize, requestId});
	}

	public static void getGlobalConfigurationFromServer(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_GLOBAL_CONFIGURATION_FROM_SERVER, requestCode, null, null);
	}

	public static void getGlobalConfigurationFromCache(Context ctx, long requestCode)
	{
		startMe(ctx, ACTION_GET_GLOBAL_CONFIGURATION_FROM_CACHE, requestCode, null, null);
	}

	public static void getActivitiesForTeamDrive(Context ctx, String teamDriveId, long requestCode)
	{
		startMe(ctx, ACTION_GET_ACTIVITIES_FOR_TEAMDRIVE, requestCode, null, null);
	}

	public static void getMoreActivities(Context ctx, String nextLink, long requestCode) {
		startMe(ctx, ACTION_GET_MORE_ACTIVITIES, requestCode,
				new String[]{EXTRA_NEXT_ACTIVITIES_LINK}, new Object[]{nextLink});
	}

	public static void getTeamDriveQuota(Context ctx, long requestCode, String... teamDriveIds) {
		startMe(ctx, ACTION_GET_TEAMDRIVE_QUOTA, requestCode,
				new String[]{EXTRA_TEAMDRIVE_IDS}, new Object[]{teamDriveIds});
	}

	public static void getAccountQuota(Context ctx, long requestCode) {
		startMe(ctx, ACTION_GET_ACOUNT_QUOTA, requestCode, null, null);
	}

	private static Intent buildBasicIntent(Context ctx, String action, long requestCode)
	{
		Intent i = new Intent(ctx, TeamDriveService.class);
		i.setAction(action);
		i.putExtra(EXTRA_REQUEST_CODE, requestCode);
		return i;
	}

	public static long getRandomRequestId()
	{
		return new Random().nextLong();
	}

	public static class TeamDriveServiceResponse
	{
		public final static int NO_REQUEST_CODE = -1;

		private final Object responseData;
		private final Exception exception;

		/** Allows to identify who this response come from */
		private final long requestCode;

		public TeamDriveServiceResponse(long requestCode, Exception e, Object data)
		{
			this.requestCode = requestCode;
			this.responseData = data;
			this.exception = e;
		}

		public final long getRequestCode()
		{
			return this.requestCode;
		}

		public Object getResponseData() throws JsonProcessingException, CortadoException, SQLException,
				IOException
		{
			if (exception == null)
			{
				return responseData;
			}

			// Re-throw exception
			if (exception instanceof CortadoException)
			{
				throw (CortadoException) exception;
			}
			else if (exception instanceof SQLException)
			{
				throw (SQLException) exception;
			}
			else if (exception instanceof IOException)
			{
				throw (IOException) exception;
			}
			else if (exception instanceof ConnectFailedException)
			{
				throw (ConnectFailedException) exception;
			}
			else
			{
				throw new RuntimeException("Exception Type not supported: " + exception.getClass().getName(), exception);
			}
		}
	}

	private static final Map<Long, Map<String, Object>> REQUEST_DATA_MAP = Collections.synchronizedMap(new HashMap<Long, Map<String, Object>>());

	private static synchronized void putRequestData(long requestId, Map<String, Object> requestData)
	{
		REQUEST_DATA_MAP.put(requestId, requestData);
	}

	private static synchronized Map<String, Object> getRequestData(long key)
	{
		Map<String, Object> requestData = REQUEST_DATA_MAP.get(key);
		if (requestData != null)
		{
			REQUEST_DATA_MAP.remove(key);
		}
		return requestData;
	}

	private static Map<String, Object> buildRequestData(String[] keys, Object[] values)
	{
		if (keys == null)
		{
			return new HashMap<>(0);
		}
		if (keys.length != values.length)
		{
			throw new RuntimeException("differing number of keys and values");
		}
		Map<String, Object> requestData = new HashMap<>(keys.length);
		for (int i = 0; i < keys.length; i++)
		{
			requestData.put(keys[i], values[i]);
		}
		return requestData;
	}

	private static void startMe(Context ctx, String action, long requestCode, String[] dataKeys, Object[] dataValues)
	{
		Intent i = buildBasicIntent(ctx, action, requestCode);
		putRequestData(requestCode, buildRequestData(dataKeys, dataValues));
		ctx.startService(i);
	}

	public static class UserImageItem implements Parcelable
	{
		private final Bitmap bmp;
		private final String userId;
		private final boolean fromNetwork;
		private final long requestId;

		private UserImageItem(Bitmap bmp, String userId, boolean fromNetwork, long requestId)
		{
			this.bmp = bmp;
			this.userId = userId;
			this.fromNetwork = fromNetwork;
			this.requestId = requestId;
		}

		public Bitmap getBitmap()
		{
			return bmp;
		}

		public String getUserId()
		{
			return userId;
		}

		public boolean isFromNetwork()
		{
			return fromNetwork;
		}

		public long getRequestId()
		{
			return requestId;
		}

		public static Parcelable.Creator<UserImageItem> CREATOR = new Creator<TeamDriveService.UserImageItem>()
		{

			@Override
			public UserImageItem[] newArray(int size)
			{
				return new UserImageItem[size];
			}

			@Override
			public UserImageItem createFromParcel(Parcel source)
			{
				return new UserImageItem(source);
			}
		};

		public UserImageItem(Parcel in)
		{
			bmp = in.readParcelable(getClass().getClassLoader());
			userId = in.readString();
			fromNetwork = in.readInt() == 1;
			requestId = in.readLong();
		}

		@Override
		public int describeContents()
		{
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			dest.writeParcelable(bmp, flags);
			dest.writeString(userId);
			dest.writeInt(fromNetwork ? 1 : 0);
			dest.writeLong(requestId);
		}

	}
}
