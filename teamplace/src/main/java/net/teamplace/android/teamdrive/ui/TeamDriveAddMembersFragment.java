package net.teamplace.android.teamdrive.ui;

import static net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse.NO_REQUEST_CODE;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.pkg;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.application.CustomAnswers;
import net.teamplace.android.browsing.widget.LockedSwipeRefreshLayout;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.Rights;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.UserListInvitation;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.TeamDriveUtils;
import net.teamplace.android.utils.TeamDriveUtils.InvalidEmailException;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.util.Rfc822Tokenizer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.cortado.android.R;
import com.crashlytics.android.answers.InviteEvent;

import de.greenrobot.event.EventBus;

public class TeamDriveAddMembersFragment extends Fragment implements OnFocusChangeListener
{
	private static final String PKG = pkg(TeamDriveAddMembersFragment.class);
	private static final String KEY_DRIVE_ID = PKG + ".drive";
	private static final String KEY_INST_IS_ENABLED = PKG + ".isEnabled";
	private static final String KEY_REQUEST_CODE = PKG + ".requestCode";
	private static final int ADD_BUTTON_INDEX = 0;
	private static final String KEY_INVITED_USER_EMAILS = PKG + ".mInvitedUserEmails";

	private TeamDrive mDrive;
	private boolean mCanInviteUser;

	private boolean mIsUIEnabled = true;
	private long mRequestCode = NO_REQUEST_CODE;

	private LockedSwipeRefreshLayout mProgressBar;

	private List<View> mViews;
	private RecipientEditTextView mContactsTv;
	private EditText mInvitationTv;
	private CheckBox mInvitationCb;
	private String[] mInvitedUserEmails;

	public static TeamDriveAddMembersFragment newInstance(String driveId)
	{
		Bundle args = new Bundle(1);
		args.putString(KEY_DRIVE_ID, driveId);
		TeamDriveAddMembersFragment frag = new TeamDriveAddMembersFragment();
		frag.setArguments(args);
		return frag;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		initInstanceState();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.tmd_add_members_fragment, container, false);
		mProgressBar = setupProgressBar(root);
		TextView driveNameTv = setupDriveNameTv(root);
		TextView driveMemberTv = setupDriveMemberTv(root);
		mInvitationCb = setupInvitationCb(root);
		mInvitationTv = setupInvitationTv(root);
		mContactsTv = setupContactsTv(root);
		saveViews(driveNameTv, driveMemberTv, mInvitationTv, mContactsTv, mInvitationCb);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		restoreInstanceState(savedInstanceState);

		if (!mIsUIEnabled)
		{
			disableUI();
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.tmd_add_members_menu, menu);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		menu.getItem(ADD_BUTTON_INDEX).setVisible(mIsUIEnabled);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putBoolean(KEY_INST_IS_ENABLED, mIsUIEnabled);
		outState.putLong(KEY_REQUEST_CODE, mRequestCode);
		outState.putStringArray(KEY_INVITED_USER_EMAILS, mInvitedUserEmails);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		if (id == R.id.action_add)
		{
			addMember();
			return true;
		}
		return false;
	}

	// Event bus callback from TeamDriveService
	public void onEventMainThread(TeamDriveServiceResponse response)
	{
		// If this is a response from a request we haven't done, just ignore
		long requestCode = response.getRequestCode();
		if (requestCode == NO_REQUEST_CODE || requestCode != mRequestCode)
		{
			return;
		}

		try
		{
			// Throws an Exception in case of error
			response.getResponseData();
			FeedbackToast.show(getActivity(), true, getString(R.string.tmd_add_members_toast_ok));
			for (int i = 0; i < mInvitedUserEmails.length; i++)
			{
				CustomAnswers.get().logInvite(new InviteEvent());
			}
			getFragmentManager().popBackStack();
		}
		catch (Exception e)
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_add_members_toast_err));
			enableUI();
			mRequestCode = NO_REQUEST_CODE;
		}
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus)
	{
		if (hasFocus && !mCanInviteUser)
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_err_user_not_activated));
			v.clearFocus();
			((EditText) v).setCursorVisible(false);
		}
	}

	private void initInstanceState()
	{
		Bundle args = getArguments();
		checkNonNullArg(args, "Create this fragment with TeamDriveAddMembersFragment.newInstance()");
		mDrive = new SessionManager(getActivity()).getLastSession().getTeamDriveMap().get(args.getString(KEY_DRIVE_ID));

		Session session = new SessionManager(getActivity()).getLastSession();
		mCanInviteUser = session.getTeamDriveGlobalConfig().getRights().can(Rights.ADD_USER);
	}

	private LockedSwipeRefreshLayout setupProgressBar(View root)
	{
		LockedSwipeRefreshLayout progressBar = (LockedSwipeRefreshLayout) root;
		progressBar.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
		return progressBar;
	}

	private RecipientEditTextView setupContactsTv(View root)
	{
		RecipientEditTextView contactsTv = (RecipientEditTextView) root.findViewById(R.id.ret_member_contacts);
		contactsTv.setTokenizer(new Rfc822Tokenizer());
		contactsTv.setAdapter(new BaseRecipientAdapter(getActivity()));
		contactsTv.setOnFocusChangeListener(this);
		return contactsTv;
	}

	private TextView setupDriveMemberTv(View root)
	{
		User[] users = mDrive.getUsers();
		int count = users == null ? 0 : users.length;
		TextView driveMemberTv = (TextView) root.findViewById(R.id.tv_drive_members);
		driveMemberTv.setText(TeamDriveUtils.formatMemberString(getActivity(), count));
		return driveMemberTv;
	}

	private EditText setupInvitationTv(View root)
	{
		EditText invitationTv = (EditText) root.findViewById(R.id.et_invitation);
		invitationTv.setOnFocusChangeListener(this);
		return invitationTv;
	}

	private CheckBox setupInvitationCb(View root)
	{
		return (CheckBox) root.findViewById(R.id.cb_invitation);
	}

	private TextView setupDriveNameTv(View root)
	{
		TextView driveNameTv = (TextView) root.findViewById(R.id.tv_drive_name);
		driveNameTv.setText(mDrive.getName());
		return driveNameTv;
	}

	private void saveViews(View... views)
	{
		mViews = new ArrayList<View>();
		for (View view : views)
		{
			mViews.add(view);
		}
	}

	private void disableUI()
	{
		setUIEnabled(false);
	}

	private void enableUI()
	{
		setUIEnabled(true);
	}

	private void setUIEnabled(boolean isEnabled)
	{
		mIsUIEnabled = isEnabled;
		getFragmentManager().invalidateOptionsMenu();
		GraphicsUtils.setRefreshing(mProgressBar, !isEnabled);
		for (View view : mViews)
		{
			view.setEnabled(isEnabled);
		}
	}

	private String getInvitationText()
	{
		Editable ed = mInvitationTv.getText();
		return ed == null ? "" : ed.toString();
	}

	private void restoreInstanceState(Bundle savedInstanceState)
	{
		if (savedInstanceState == null)
		{
			return;
		}

		mRequestCode = savedInstanceState.getLong(KEY_REQUEST_CODE);
		mIsUIEnabled = savedInstanceState.getBoolean(KEY_INST_IS_ENABLED);
		mInvitedUserEmails = savedInstanceState.getStringArray(KEY_INVITED_USER_EMAILS);
	}

	private void addMember()
	{
		if (!mCanInviteUser)
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_err_user_not_activated));
			return;
		}

		CharSequence cs = mContactsTv.getText();
		if (TextUtils.isEmpty(cs))
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_new_drive_toast_contacts_err));
			return;
		}

		try
		{
			disableUI();
			mRequestCode = TeamDriveUtils.generateRequestCode();
			inviteUserRequest();
		}
		catch (InvalidEmailException e)
		{
			enableUI();
			mRequestCode = NO_REQUEST_CODE;
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_new_drive_toast_contacts_err));
		}
	}

	private void inviteUserRequest() throws InvalidEmailException
	{
		CharSequence chipText = mContactsTv.getText();
		int role = mInvitationCb.isChecked() ? User.ROLE_MEMBER_INVITE : User.ROLE_MEMBER;
		User[] users = TeamDriveUtils.getUsersFromChipsText(chipText, role);
		mInvitedUserEmails = new String[users.length];
		for (int i = 0; i < users.length; i++) {
			mInvitedUserEmails[i] = users[i].getEmail();
		}
		String message = getInvitationText();
		UserListInvitation invitation = (users != null && users.length > 0) ? new UserListInvitation(message,
				users) : null;
		TeamDriveService.inviteUsers(getActivity(), mDrive.getId(), invitation, mRequestCode);
	}
}
