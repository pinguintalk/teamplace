package net.teamplace.android.teamdrive;

import java.lang.ref.WeakReference;

import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.teamdrive.TeamDriveService.UserImageItem;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;

public class ImageResultCallback
{
	private final String TAG = getClass().getSimpleName();

	private WeakReference<ImageView> viewRef;

	public ImageResultCallback(ImageView iv)
	{
		this.viewRef = new WeakReference<ImageView>(iv);
	}

	public void onResult(UserImageItem item)
	{
		ImageView iv = viewRef.get();
		Bitmap bmp = item.getBitmap();
		if (iv != null && iv.getVisibility() == View.VISIBLE && bmp != null
				&& item.getUserId().equals(((User) iv.getTag()).getUserId()))
		{
			iv.setImageBitmap(bmp);
		}
	}
}