package net.teamplace.android.teamdrive.ui;

import android.animation.ValueAnimator;
import android.app.Fragment;
import android.content.Context;
import android.database.SQLException;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cortado.android.R;
import com.fasterxml.jackson.core.JsonProcessingException;

import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.browsing.widget.DividerItemDecoration;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.Payment;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.quota.TeamdriveQuotaModel;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.Log;

import java.io.IOException;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

public class TeamDriveManageFragment extends Fragment implements
		SwipeRefreshLayout.OnRefreshListener
{
	public static final String TAG = tag(TeamDriveManageFragment.class);
	public static final String KEY_DRIVE_ID = TAG + ".driveId";

	private enum LayoutManagerType {
		GRID_LAYOUT_MANAGER,
		LINEAR_LAYOUT_MANAGER
	}
	private SwipeRefreshLayout mSwipeRefreshLayout;
	protected RecyclerView.LayoutManager mLayoutManager;
	protected LayoutManagerType mCurrentLayoutManagerType;
	private RecyclerView mRecyclerView;
	private TeamDriveManageAdapter mAdapter;
	private LinearLayout mManagerLayout;
	private ValueAnimator anim;

	private State mState = new State();
	private StateResolver mStateResolver = new StateResolver();

	private static class State {
		TeamDrive toManage;
		TeamdriveQuotaModel quota;
		int teamdriveRequestId = -1;
		int quotaRequestId = -1;
		boolean hasAnimationRun = false;
	}

	private class StateResolver {
		public void onEventMainThread(State event) {
			mState = event;
		}
	}

	public static TeamDriveManageFragment newInstance(String driveId)
	{
		TeamDriveManageFragment instance = new TeamDriveManageFragment();
		Bundle args = new Bundle();
		args.putString(KEY_DRIVE_ID, driveId);
		instance.setArguments(args);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		Log.d(TAG, "onCreate()");
		super.onCreate(savedInstanceState);
		mState.toManage = new SessionManager(getActivity()).getLastSession().getTeamDriveMap()
				.get(getArguments().getString(KEY_DRIVE_ID));
		EventBus.getDefault().registerSticky(mStateResolver);
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		Log.d(TAG, "onCreateView()");
		View v = inflater.inflate(R.layout.tmd_manage_fragment, container, false);
		mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swiperefresh);
		mSwipeRefreshLayout.setOnRefreshListener(this);
		mSwipeRefreshLayout.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);

		mManagerLayout = (LinearLayout) v.findViewById(R.id.teamdrive_title_layout);
		setHeaderView(mManagerLayout);

		// LinearLayoutManager is used here, this will layout the elements in a similar fashion
		// to the way ListView would layout elements. The RecyclerView.LayoutManager defines how
		// elements are laid out.
		mLayoutManager = new LinearLayoutManager(getActivity());
		mCurrentLayoutManagerType = LayoutManagerType.LINEAR_LAYOUT_MANAGER;
		mRecyclerView = (RecyclerView) v.findViewById(R.id.manage_list);
		mRecyclerView.addItemDecoration(
				new DividerItemDecoration(getActivity(), R.drawable.list_divider, LinearLayoutManager.VERTICAL));

		mRecyclerView.setLayoutManager(mLayoutManager);

		mAdapter = new TeamDriveManageAdapter(getActivity(), mState.toManage);
		mRecyclerView.setAdapter(mAdapter);

		return v;
	}

	@Override
	public void onViewCreated(final View view, Bundle savedInstanceState)
	{
		Log.d(TAG, "onViewCreated()");
		super.onViewCreated(view, savedInstanceState);
		if (savedInstanceState == null)
		{
			mAdapter.setQuota(null, true);
			refreshList();
			return;
		}
		if (mState.quota != null)
		{
			mAdapter.setQuota(mState.quota, !mState.hasAnimationRun);
		}

	}

	@Override
	public void onPause()
	{
		Log.d(TAG, "onPause()");
		super.onPause();
		EventBus.getDefault().unregister(this);
		if (anim != null && anim.isStarted())
		{
			anim.end();
		}
	}

	@Override
	public void onResume()
	{
		Log.d(TAG, "onResume()");
		super.onResume();
		EventBus.getDefault().register(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		Log.d(TAG, "onSaveInstanceState()");
		EventBus.getDefault().unregister(mStateResolver);
		EventBus.getDefault().postSticky(mState);
		super.onSaveInstanceState(outState);
	}

	@Override
	public void onRefresh()
	{
//		getCurrentQuotaView().setQuotaKnown(false);
		mAdapter.setQuota(null, false);
		mState.hasAnimationRun = false;
		refreshList();
	}

	private void  refreshList() {
		if(!GenericUtils.isConnectedToInternet(getActivity()))
			FeedbackToast.show(getActivity(),false,getActivity().getResources().getString(R.string.tmd_err_no_internet_connection));
		else {
			GraphicsUtils.setRefreshing(mSwipeRefreshLayout, true);
			mState.teamdriveRequestId = IdGenerator.getId();
			mState.quotaRequestId = IdGenerator.getId();
			TeamDriveService.getTeamDrive(this.getActivity(), mState.toManage.getId(), mState.teamdriveRequestId);
			mState.hasAnimationRun = false;
		}
	}

	public void onEventMainThread(TeamDriveServiceResponse event)
	{
		Object responseData = null;
		try
		{
			responseData = event.getResponseData();
		}
		catch (JsonProcessingException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (CortadoException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		long requestCode = event.getRequestCode();

		if (requestCode == mState.teamdriveRequestId)
		{
			TeamDrive[] drives = (TeamDrive[]) responseData;
			if (responseData != null && drives.length > 0)
			{
				mState.toManage = drives[0];
				mAdapter.setTeamDrive(mState.toManage);
				mAdapter.notifyDataSetChanged();
				TeamDriveService.getTeamDriveQuota(this.getActivity(), mState.quotaRequestId, mState.toManage.getId());
			}
		}
		else if (requestCode == mState.quotaRequestId)
		{
			if (responseData != null)
			{
				TeamdriveQuotaModel[] quotaModels = (TeamdriveQuotaModel[]) responseData;
				if (quotaModels.length > 0)
				{
					GraphicsUtils.setRefreshing(mSwipeRefreshLayout, false);
					mState.quota = quotaModels[0];
					mAdapter.setQuota(mState.quota, !mState.hasAnimationRun);
					mState.hasAnimationRun = true;
				}
			}
		}
	}



	View setHeaderView(View root)
	{
		setUpNameView(root);
		setUpExpirationViews(root);
		return root;
	}

	private void setUpNameView(View root)
	{
		TextView tvName = (TextView) root.findViewById(R.id.name);
		tvName.setText(mState.toManage.getName());
	}

	private void setUpExpirationViews(View root)
	{
		Payment payment = mState.toManage.getPayment();
		Long validuntil = payment != null ? payment.getPaidUntil() : null;
		Context context = getActivity();
		TextView tvExpires = (TextView) root.findViewById(R.id.expires);
		ImageView ivExpirationWarning = (ImageView) root.findViewById(R.id.img_expiraion_warning);
		if (validuntil == null)
		{
			tvExpires.setVisibility(View.INVISIBLE);
			ivExpirationWarning.setVisibility(View.GONE);
			return;
		}

		String text = null;
		int imgVisibility = View.VISIBLE;
		int textColor = context.getResources().getColor(R.color.tmd_expired_warning);

		long currentTime = System.currentTimeMillis();
		long timeToExpire = validuntil - currentTime;
		long timeFromTomorrow = validuntil - GenericUtils.tomorrowStartsAt(currentTime);
		long aDay = 1000 * 60 * 60 * 24;

		if (timeToExpire < 0)
		{
			text = context.getString(R.string.tmd_expired);
		}
		else if (timeFromTomorrow < 0)
		{
			text = context.getString(R.string.tmd_expires_today);
		}
		else if (timeFromTomorrow < aDay)
		{
			text = String.format(context.getString(R.string.tmd_expires_tomorrow));
		}
		else if (timeFromTomorrow < aDay * 2)
		{
			text = String.format(context.getString(R.string.tmd_expires_in_1_day));
		}
		else if (timeFromTomorrow < aDay * 6)
		{
			text = String.format(context.getString(R.string.tmd_expires_in_x_days), (timeFromTomorrow / aDay));
		}
		else
		{
			text = String.format(context.getString(R.string.tmd_expires_on),
					DateFormat.getDateFormat(context).format(validuntil));
			textColor = context.getResources().getColor(R.color.brw_default_item_file_info_font);
			imgVisibility = View.GONE;
		}

		tvExpires.setVisibility(View.VISIBLE);
		tvExpires.setText(text);
		tvExpires.setTextColor(textColor);
		ivExpirationWarning.setVisibility(imgVisibility);
	}

}
