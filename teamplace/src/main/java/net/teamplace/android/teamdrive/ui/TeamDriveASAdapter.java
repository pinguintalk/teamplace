package net.teamplace.android.teamdrive.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.activity.ActivityBase;
import net.teamplace.android.teamdrive.UserImageLoader;
import net.teamplace.android.utils.TeamDriveUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by jobol on 17/06/15.
 */
public class TeamDriveASAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_UNKNOWN = -1;
    private static final int TYPE_COMMENT_CREATED = 1;
    private static final int TYPE_FILE_UPLOADED = 2;
    private static final int TYPE_MULTI_FILE_UPLOADED = 3;
    private static final int TYPE_FILE_VERSION_CREATED = 4;
    private static final int TYPE_TEAMDRIVE_CREATED = 5;
    private static final int TYPE_USER_JOINED = 6;

    private static final int TYPE_LOAD_MORE_PROGRESS_ITEM = 7;

    private List<ActivityBase> mDisplayableItems = new ArrayList<>();
    private List<ActivityBase> mSubActivities = new ArrayList<>();
    private Context mContext;
    private TeamDrive mTeamDrive;
    private boolean mMoreToLoad;
    private UserImageLoader.CacheValidator mCacheValidator = new UserImageLoader.CacheValidator() {

        private List<User> mLoadedUsers = new ArrayList<>();

        @Override
        public boolean useCachedVersionIfAvailable(User user) {
            return mLoadedUsers.contains(user);
        }

        @Override
        public void onImageCached(User user) {
            mLoadedUsers.add(user);
        }
    };
    private ActionCallback mActionCallback;

    public interface ActionCallback {
        void onPreviewRequested(Path file, String versionId, String commentId);
        void onBrowseRequested(Path path);
    }

    public TeamDriveASAdapter(Context context, TeamDrive teamDrive, ActionCallback actionCallback) {
        mContext = context.getApplicationContext();
        mTeamDrive = teamDrive;
        mActionCallback = actionCallback;
    }

    public void setItems(ActivityBase[] items, boolean moreToLoad) {
        mDisplayableItems.clear();
        mSubActivities.clear();
        filterIncomingItems(Arrays.asList(items));
        mMoreToLoad = moreToLoad;
        notifyDataSetChanged();
    }

    public void addItems(ActivityBase[] items, boolean moreToLoad) {
        int listSize = mDisplayableItems.size();
        if (!moreToLoad && mMoreToLoad) {
            notifyItemRemoved(listSize);
        }
        mMoreToLoad = moreToLoad;
        filterIncomingItems(Arrays.asList(items));
        notifyItemRangeInserted(listSize, mDisplayableItems.size() - listSize);
    }

    public ActivityBase[] getItems() {
        ArrayList<ActivityBase> completeList = new ArrayList<>(mDisplayableItems);
        completeList.addAll(mSubActivities);
        return completeList.toArray(new ActivityBase[completeList.size()]);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        TypeViewConnector binder = null;
        switch (viewType) {
            case TYPE_COMMENT_CREATED:
                binder = mCommentCreatedViewConnector;
                break;
            case TYPE_FILE_UPLOADED:
                binder = mFileUploadedConnector;
                break;
            case TYPE_MULTI_FILE_UPLOADED:
                binder = mMultiFileUploadedConnector;
                break;
            case TYPE_FILE_VERSION_CREATED:
                binder = mVersionCreatedViewConnector;
                break;
            case TYPE_TEAMDRIVE_CREATED:
                binder = mTeamdriveCreatedViewConnector;
                break;
            case TYPE_USER_JOINED:
                binder = mMemberJoinedViewConnector;
                break;
            case TYPE_LOAD_MORE_PROGRESS_ITEM:
                View prgView = LayoutInflater.from(parent.getContext()).inflate(R.layout.ast_load_more_item,
                        parent, false);
                return new LoadMoreViewHolder(prgView);
            default:
                break;
        }
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ast_item, parent, false);
        return new ActivityBaseViewHolder(v, binder);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < mDisplayableItems.size()) {
            ((ActivityBaseViewHolder) holder).bindViews(mDisplayableItems.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mDisplayableItems.size() + (mMoreToLoad ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == mDisplayableItems.size()) {
            return TYPE_LOAD_MORE_PROGRESS_ITEM;
        }

        ActivityBase item = mDisplayableItems.get(position);
        if (item == null) {
            return TYPE_UNKNOWN;
        }
        if (item instanceof ActivityBase.CommentCreated) {
            return TYPE_COMMENT_CREATED;
        } else if (item instanceof ActivityBase.FileUploaded) {
            return hasSubActivities(item) ? TYPE_MULTI_FILE_UPLOADED : TYPE_FILE_UPLOADED;
        } else if (item instanceof ActivityBase.FileVersionCreated) {
            return TYPE_FILE_VERSION_CREATED;
        } else if (item instanceof ActivityBase.TeamDriveCreated) {
            return TYPE_TEAMDRIVE_CREATED;
        } else if (item instanceof ActivityBase.UserJoined) {
            return TYPE_USER_JOINED;
        } else {
            return TYPE_UNKNOWN;
        }
    }

    public class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        public LoadMoreViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ActivityBaseViewHolder extends RecyclerView.ViewHolder {
        
        private TypeViewConnector mTypeViewBinder;
        private View mContainerView;
        private ImageView mIvUser;
        private TextView mTvUserDisplayname;
        private TextView mTvTimestamp;
        private TextView mTvAction;
        private ImageView mIvAction;

        public ActivityBaseViewHolder(View itemView, TypeViewConnector binder) {
            super(itemView);
            mTypeViewBinder = binder;
            mContainerView = itemView;
            mIvUser = (ImageView) itemView.findViewById(R.id.iv_user_image);
            mTvUserDisplayname = (TextView) itemView.findViewById(R.id.tv_user_displayname);
            mTvTimestamp = (TextView) itemView.findViewById(R.id.tv_timestamp);
            mTvAction = (TextView) itemView.findViewById(R.id.tv_action_name);
            mIvAction = (ImageView) itemView.findViewById(R.id.iv_action);
        }

        void bindViews(final ActivityBase obj) {
            if (mTypeViewBinder != null) {
                mTypeViewBinder.bindUserImageView(mIvUser, obj, mCacheValidator);
                mTypeViewBinder.bindUserDisplaynameTextView(mTvUserDisplayname, obj);
                mTypeViewBinder.bindTimestampTextView(mTvTimestamp, obj);
                mTypeViewBinder.bindActionTextView(mTvAction, obj);
                mTypeViewBinder.bindActionImageView(mIvAction, obj);
                mTypeViewBinder.bindContainerView(mContainerView, obj);
                mContainerView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mTypeViewBinder.onClickAction(obj);
                    }
                });
            }
        }
    }

    private interface TypeViewConnector {
        void bindUserImageView(ImageView iv, ActivityBase obj, UserImageLoader.CacheValidator cacheValidator);
        void bindUserDisplaynameTextView(TextView tv, ActivityBase obj);
        void bindTimestampTextView(TextView tv, ActivityBase obj);
        void bindActionTextView(TextView tv, ActivityBase obj);
        void bindActionImageView(ImageView iv, ActivityBase obj);
        void bindContainerView(View view, ActivityBase obj);
        void onClickAction(ActivityBase obj);
    }

    private abstract class BaseTypeViewConnector implements TypeViewConnector {
        @Override
        public void bindUserImageView(ImageView iv, ActivityBase obj, UserImageLoader.CacheValidator cacheValidator) {
            iv.setImageResource(R.drawable.prv_account);
            User user = getUser(obj.userId);
            if (user != null)
                UserImageLoader.downloadUserImage(iv,user , cacheValidator);
            else
                iv.setBackgroundResource(R.drawable.tmd_default_icon);
        }

        @Override
        public void bindUserDisplaynameTextView(TextView tv, ActivityBase obj) {
            User user = getUser(obj.userId);
            if (user != null)
                tv.setText(TeamDriveUtils.getUserNameToDisplay(user));
            else
                tv.setText(mContext.getResources().getString(R.string.as_unknown_user));
        }

        @Override
        public void bindTimestampTextView(TextView tv, ActivityBase obj) {
            String timeStr = DateUtils.formatDateTime(mContext, obj.timeStamp * 1000l,
                    DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_SHOW_TIME);
            tv.setText(timeStr);
        }

        @Override
        public void bindContainerView(View view, ActivityBase obj) {
            view.setBackgroundResource(R.drawable.ast_item_sel);
        }
    }


    private BaseTypeViewConnector mFileUploadedConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
            tv.setText(Html.fromHtml(String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_file_upload),
                    "<b>" + obj.relatedFile.name + "</b>", getActionPathName(obj))));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_upload);
        }

        @Override
        public void onClickAction(ActivityBase obj) {
            mActionCallback.onPreviewRequested(getFilePath(obj), null, null);
        }
    };

    private BaseTypeViewConnector mMultiFileUploadedConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
           tv.setText(getMultiUploadedText(obj));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_upload);
        }

        @Override
        public void onClickAction(ActivityBase obj) {
            mActionCallback.onBrowseRequested(getFilePath(obj).getParentPath());
        }
    };

    private BaseTypeViewConnector mVersionCreatedViewConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
            tv.setText(Html.fromHtml(String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_version_created),
                    "<b>" + obj.relatedFile.name + "</b>", getActionPathName(obj))));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_version);
        }

        @Override
        public void onClickAction(ActivityBase obj) {
            mActionCallback.onPreviewRequested(getFilePath(obj),
                    ((ActivityBase.FileVersionCreated) obj).fileVersionId, null);
        }
    };

    private BaseTypeViewConnector mCommentCreatedViewConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
            tv.setText(Html.fromHtml(String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_comment_created),
                    "<b>" + obj.relatedFile.name + "</b>", getActionPathName(obj))));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_comment);
        }

        @Override
        public void onClickAction(ActivityBase obj) {
            mActionCallback.onPreviewRequested(getFilePath(obj), null,
                    ((ActivityBase.CommentCreated) obj).commentId);
        }
    };

    private BaseTypeViewConnector mTeamdriveCreatedViewConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
            tv.setText(String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_teamdrive_created),
                    mTeamDrive.getName() + "."));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_create);
        }

        @Override
        public void bindContainerView(View view, ActivityBase obj) {
            view.setBackgroundResource(R.drawable.ast_item_sel);
        }

        @Override
        public void onClickAction(ActivityBase obj) {

        }
    };

    private BaseTypeViewConnector mMemberJoinedViewConnector = new BaseTypeViewConnector() {
        @Override
        public void bindActionTextView(TextView tv, ActivityBase obj) {
            tv.setText(String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_member_joined),
                    mTeamDrive.getName() + "."));
        }

        @Override
        public void bindActionImageView(ImageView iv, ActivityBase obj) {
            iv.setImageResource(R.drawable.ast_action_join);
        }

        @Override
        public void bindContainerView(View view, ActivityBase obj) {
            view.setBackgroundResource(R.drawable.ast_item_sel);
        }

        @Override
        public void onClickAction(ActivityBase obj) {

        }
    };

    private void filterIncomingItems(List<ActivityBase> from) {
        for (ActivityBase act : from) {
            // only show activities that are no subactivities
            if (act.parentActivityId == null || act.parentActivityId.length() == 0) {
                mDisplayableItems.add(act);
            } else {
                mSubActivities.add(act);
            }
        }
    }

    private User getUser(String userId) {
        for (User user : mTeamDrive.getUsers()) {
            if (user.getUserId().equals(userId)) {
                return user;
            }
        }
        return null;
    }

    private Spanned getMultiUploadedText(ActivityBase mainItem) {
        String toDisplay;
        int childCount = mainItem.subActivityCount + 1;
        String sizeStr = childCount > 999 ? mContext.getString(R.string.ast_above_max_number)
                : String.valueOf(childCount);
        toDisplay = String.format(Locale.getDefault(), mContext.getString(R.string.ast_action_multiple_file_upload),
               sizeStr, "<b>" + getActionPathName(mainItem) + "</b>");
        return Html.fromHtml(toDisplay);
    }

    private boolean hasSubActivities(ActivityBase actBase) {
        return actBase.subActivityCount > 0;
    }

    private String getActionPathName(ActivityBase activityBase) {
        String pathName = trimPath(activityBase.relatedFile.path);
        if (pathName == null || pathName.length() == 0) {
            pathName = mTeamDrive.getName();
        } else {
            pathName = "/" + pathName; // for folder type
        }
        return pathName + ".";
    }

    private String trimPath(String in) {
        in = forwardSlashes(in);
        while (in.startsWith("/")) {
            in = in.substring(1);
        }
        return in;
    }

    private String forwardSlashes(String in) {
        return in.replaceAll("\\\\", "/");
    }

    private Path getFilePath(ActivityBase activityBase) {
        Path parent = Path.valueOf(Path.TeamDrive.teamDriveRoot(activityBase.teamDriveId),
                trimPath(activityBase.relatedFile.path));
        return Path.valueOf(parent, activityBase.relatedFile.name);
    }

}
