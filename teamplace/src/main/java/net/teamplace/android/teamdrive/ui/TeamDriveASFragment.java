package net.teamplace.android.teamdrive.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.activity.ActivityBase;
import net.teamplace.android.http.teamdrive.activity.ActivityResponse;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.utils.GraphicsUtils;

import java.io.IOException;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by jobol on 17/06/15.
 */
public class TeamDriveASFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener, TeamDriveASAdapter.ActionCallback {

    private static final String PKG = TeamDriveASFragment.class.getPackage().getName();
    private static final String KEY_TEAMDRIVE_ID = PKG + ".teamDriveId";

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private View mProgressBarContainer;
    private LinearLayoutManager mLayoutManager;
    private TeamDriveASAdapter mAdapter;
    private State mState = new State();
    private StateResolver mStateResolver = new StateResolver();

    private static class State {
        private ActivityBase[] adapterItems;
        private Parcelable adapterState;
        private String nextLink;
        private long firstRequestCode;
        private long moreRequestCode;
        private int firstVisibleItem;
        private int visibleItemCount;
        private int totalItemCount;
        private int previousTotal = 0;
        private boolean loadingOnScrollDown = true;
        private Path fileToPreview = null;
        private String versionIdToPreview = null;
        private String commentIdToPreview = null;
        private boolean progressOverlayVisible;
    }

    private class StateResolver {
        private boolean enabled = false;
        public void onEventMainThread(State state) {
            if (enabled) {
                mState = state;
            }
        }
    }

    public static TeamDriveASFragment createInstance(String teamDriveId) {
        TeamDriveASFragment instance = new TeamDriveASFragment();
        Bundle args = new Bundle();
        args.putString(KEY_TEAMDRIVE_ID, teamDriveId);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // prevent recreating state on a clean start
        mStateResolver.enabled = savedInstanceState != null;
        EventBus.getDefault().registerSticky(mStateResolver);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        TeamDrive teamDrive = new SessionManager(getActivity()).getLastSession()
                .getTeamDriveMap().get(getArguments().getString(KEY_TEAMDRIVE_ID));

        View v = inflater.inflate(R.layout.ast_fragment, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.srl_container);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_activity_list);
        mProgressBarContainer = v.findViewById(R.id.progress_bar_overlay);
        mAdapter = new TeamDriveASAdapter(getActivity(), teamDrive, this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
                R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mState.nextLink == null) {
                    return;
                }

                mState.visibleItemCount = mLayoutManager.getChildCount();
                mState.totalItemCount = mLayoutManager.getItemCount();
                mState.firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

                if (mState.loadingOnScrollDown) {
                    if (mState.totalItemCount > mState.previousTotal) {
                        mState.loadingOnScrollDown = false;
                        mState.previousTotal = mState.totalItemCount;
                    }
                }
                if (!mState.loadingOnScrollDown && (mState.totalItemCount - mState.visibleItemCount) <= mState.firstVisibleItem) {
                    mState.loadingOnScrollDown = true;
                    mState.moreRequestCode = TeamDriveService.getRandomRequestId();
                    TeamDriveService.getMoreActivities(getActivity(), mState.nextLink, mState.moreRequestCode);
                }
            }
        });
        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState == null) {
            loadData();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(BrowsingService.Contract.ACTION_RESPONSE_BROWSE);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_EXPORT_TO_PDF);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_EXPORT_TO_ZIP);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_EXTRACT_VERSION);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_COPY);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_DELETE);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_MOVE);
        filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_FETCH_FOLDER_CONTENT_IF_EXISTS);
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBrowsingServiceReceiver, filter);
        EventBus.getDefault().register(this);

        TeamDrive teamDrive = getArguments().getParcelable(KEY_TEAMDRIVE_ID);
        ActionBar ab = getActivity().getActionBar();
        ab.setTitle(teamDrive.getName());
        ab.setSubtitle(R.string.ast_title);
    }

    @Override
    public void onPause() {
        EventBus.getDefault().unregister(this);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBrowsingServiceReceiver);
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PreviewActivity.REQUEST_CODE) {
            if (data != null) {
                Activity act = getActivity();
                act.startService(data);
                setProgressOverlayVisibility(true);
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // This class uses EventBus sticky messages to persist the instance state.
        mState.adapterState = mLayoutManager.onSaveInstanceState();
        mState.adapterItems = mAdapter.getItems();
        mState.progressOverlayVisible = mProgressBarContainer.getVisibility() == View.VISIBLE;
        EventBus.getDefault().unregister(mStateResolver);
        EventBus.getDefault().postSticky(mState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            // If this is a "semi-clean" start (instance state was saved, but instance + sticky message destroyed,
            // reload:
            if (mState.adapterItems == null) {
                loadData();
                return;
            }
            mAdapter.setItems(mState.adapterItems, mState.nextLink != null && mState.nextLink.length() > 0);
            mLayoutManager.onRestoreInstanceState(mState.adapterState);
            mProgressBarContainer.setVisibility(mState.progressOverlayVisible ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onRefresh() {
        resetListCounts();
        loadData();
    }

    public void onEventMainThread(TeamDriveService.TeamDriveServiceResponse response) {

        long requestCode = response.getRequestCode();
        ActivityResponse backendResponse;
        if (requestCode == mState.firstRequestCode) {
            GraphicsUtils.setRefreshing(mSwipeRefreshLayout, false);
            try {
                backendResponse = resolveResponse(response);
                mAdapter.setItems(backendResponse.value, mState.nextLink != null && mState.nextLink.length() > 0);
            } catch (CortadoException | IOException e) {
                FeedbackToast.show(getActivity(), false, getString(R.string.ast_error_loading_failed));
            }
        } else if (requestCode == mState.moreRequestCode) {
            try {
                backendResponse = resolveResponse(response);
                mAdapter.addItems(backendResponse.value, mState.nextLink != null && mState.nextLink.length() > 0);
            } catch (RedirectException e) {
                e.printStackTrace();
                mState.moreRequestCode = TeamDriveService.getRandomRequestId();
                TeamDriveService.getMoreActivities(getActivity(), e.getRedirectLocation(), mState.moreRequestCode);
            } catch (CortadoException | IOException e) {
                FeedbackToast.show(getActivity(), false, getString(R.string.ast_error_loading_failed));
            }
        }
    }

    private ActivityResponse resolveResponse(TeamDriveService.TeamDriveServiceResponse serviceResponse)
            throws CortadoException, IOException {
        ActivityResponse backendResponse = (ActivityResponse) serviceResponse.getResponseData();
        mState.nextLink = backendResponse.nextLink;
        return backendResponse;
    }

    @Override
    public void onPreviewRequested(Path file, String versionId, String commentId) {
        setProgressOverlayVisibility(true);
        mState.fileToPreview = file;
        mState.versionIdToPreview = versionId;
        mState.commentIdToPreview = commentId;
        requestFolderContentToPreview(file);
    }

    @Override
    public void onBrowseRequested(Path path) {
        setProgressOverlayVisibility(true);
        requestCheckFolderExists(path);
    }

    private void setProgressOverlayVisibility(boolean visible) {
        mProgressBarContainer.setClickable(visible);
        mProgressBarContainer.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    private void resetListCounts() {
        mState.previousTotal = 0;
        mState.totalItemCount = 0;
        mState.firstVisibleItem = 0;
        mState.visibleItemCount = 0;
    }

    private void loadData() {
        GraphicsUtils.setRefreshing(mSwipeRefreshLayout, true);
        mState.firstRequestCode = TeamDriveService.getRandomRequestId();
        TeamDriveService.getActivitiesForTeamDrive(getActivity(),
                getArguments().getString(KEY_TEAMDRIVE_ID), mState.firstRequestCode);
    }

    private static final String EXTRA_REQUESTED_FOLDER_TO_PREVIEW = "requested to preview " + TeamDriveASFragment.class.getName();
    private static final String EXTRA_REQUESTED_FOLDER_TO_BROWSE = "requested to browse " + TeamDriveASFragment.class.getName();
    private static final String EXTRA_REQUESTED_FOLDER_PATH = "path to requeste " + TeamDriveASFragment.class.getName();

    private void requestFolderContentToPreview(Path file) {
        Intent i = BrowsingService.Contract.buildCheckRemoteFolderExists(getActivity(), file.getParentPath());
        i.putExtra(EXTRA_REQUESTED_FOLDER_TO_PREVIEW, true);
        getActivity().startService(i);
    }

    private void requestCheckFolderExists(Path folder) {
        Intent i = BrowsingService.Contract.buildCheckRemoteFolderExists(getActivity(), folder);
        i.putExtra(EXTRA_REQUESTED_FOLDER_TO_BROWSE, true);
        i.putExtra(EXTRA_REQUESTED_FOLDER_PATH, folder);
        getActivity().startService(i);
    }

    private BroadcastReceiver mBrowsingServiceReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String action = intent.getAction();

            setProgressOverlayVisibility(false);

            if (BrowsingService.Contract.ACTION_RESPONSE_FETCH_FOLDER_CONTENT_IF_EXISTS.equals(action)) {

                handleCheckFolderExistsResponse(intent);
            }
            else if (BrowsingService.Contract.ACTION_RESPONSE_EXPORT_TO_PDF.equals(action)
                    || BrowsingService.Contract.ACTION_RESPONSE_EXPORT_TO_ZIP.equals(action)
                    || BrowsingService.Contract.ACTION_RESPONSE_EXTRACT_VERSION.equals(action)) {

                onBrowseRequested(mState.fileToPreview.getParentPath());
            } else if (BrowsingService.Contract.ACTION_RESPONSE_COPY.equals(action)
                    || BrowsingService.Contract.ACTION_RESPONSE_MOVE.equals(action)
                    || BrowsingService.Contract.ACTION_RESPONSE_DELETE.equals(action)) {

            }

        }
    };

    private void handleCheckFolderExistsResponse(Intent intent) {
        Bundle requestExtras = intent.getBundleExtra(BrowsingService.Contract.RESPONSE_REQUEST_EXTRAS);
        Path path = requestExtras.getParcelable(EXTRA_REQUESTED_FOLDER_PATH);

        ArrayList<FileInfo> folderContent = intent.getParcelableArrayListExtra(BrowsingService.Contract.RESPONSE_FETCH_FOLDER_IF_EXISTS);

        if (folderContent == null) {
                FeedbackToast toast = new FeedbackToast(getActivity());
                toast.setMessage(getActivity().getResources().getString(R.string.ast_error_folder_not_found));
                toast.setDuration(Toast.LENGTH_LONG);
                toast.show();
                return;
        }

        if (requestExtras.getBoolean(EXTRA_REQUESTED_FOLDER_TO_BROWSE, false)) {

            BrowsingActivity.browseAndPush(getFragmentManager(), path);
        }
        else if  (requestExtras.getBoolean(EXTRA_REQUESTED_FOLDER_TO_PREVIEW, false)) {

            startPreview(folderContent);
        }

    }

    private void startPreview(ArrayList<FileInfo> folderContent) {
       FileInfo previewableFileInfo = null;
         for (FileInfo  info : folderContent) {
            if (info.getName().equals(mState.fileToPreview.getName())) {
                previewableFileInfo = info;
                break;
            }
        }
        if (previewableFileInfo != null) {
            ArrayList<FileInfo> singleItemList = new ArrayList<>(1); // this disables image swiping
            singleItemList.add(previewableFileInfo);
            PreviewConfiguration configuration = new PreviewConfiguration()
                    .folderContent(singleItemList)
                    .openComment(mState.commentIdToPreview).openVersion(mState.versionIdToPreview);
            Intent i = PreviewActivity.buildIntent(getActivity(), previewableFileInfo, configuration);
            startActivityForResult(i, PreviewActivity.REQUEST_CODE);
        }
        else
        {
            FeedbackToast toast = new FeedbackToast(getActivity());
            toast.setMessage(getActivity().getResources().getString(R.string.brw_error_file_not_found));
            toast.setDuration(Toast.LENGTH_LONG);
            toast.show();

        }
        setProgressOverlayVisibility(false);
    }
}