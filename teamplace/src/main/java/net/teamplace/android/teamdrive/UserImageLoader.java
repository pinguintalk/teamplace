package net.teamplace.android.teamdrive;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.process.BitmapProcessor;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;

import net.teamplace.android.application.DefaultImageDownloader;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.request.teamdrive.TeamdriveRequester;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by jobol on 23/06/15.
 */
public class UserImageLoader {

    // TODO remove and evaluate etags instead as soon as the backend implements this
    private static final int CACHE_VALIDITY = 5 * 60 * 1000;


    public static void downloadUserImage(ImageView view, User user, CacheValidator validator) {
        if (user == null || user.getImgPath() == null)
        {
            return;
        }
        int width, height;
        Drawable img = view.getDrawable();
        if (img != null) {
            width = img.getIntrinsicWidth();
            height = img.getIntrinsicHeight();
        } else {
            width = view.getWidth();
            height = view.getHeight();
        }

        StreamResolver streamResolver = new StreamResolver(width, height, user);
        DisplayImageOptions opts = new DisplayImageOptions.Builder().cacheInMemory(true).cacheOnDisk(true)
                .extraForDownloader(streamResolver)
                .postProcessor(new CropToCircleProcessor())
                .build();
        ImageLoader.getInstance().displayImage(user.getImgPath(), view, opts, new LoadingListener(user, validator));
    }


    private static class StreamResolver implements DefaultImageDownloader.NetworkStreamResolver {

        private int mImgWidth, mImgHeight;
        private User mUser;

        StreamResolver(int imgWidth, int imgHeight, User user) {
            mImgHeight = imgHeight;
            mImgWidth = imgWidth;
            mUser = user;
        }

        @Override
        public InputStream fromUri(Context context, String uri) throws IOException {
            TeamDriveDatabaseActions dbActions = TeamDriveDatabaseActions.getInstance();
            String reqEtag = dbActions.getUserImageEtag(mUser.getUserId());
            TeamdriveRequester requester = new TeamdriveRequester(context, new SessionManager(context).getLastSession(),
                    new TeamplaceBackend());
            try {
                Pair<InputStream, String> responsePair = requester.getUserImageInputStream(mUser, mImgWidth, reqEtag);
                dbActions.updateUserImageEtag(mUser.getUserId(), responsePair.second);
                return responsePair.first;
            } catch (CortadoException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    private static class LoadingListener implements ImageLoadingListener {

        private volatile boolean mClearFromCache;
        private User mUser;
        private CacheValidator mCacheValidator;

        LoadingListener(User user, CacheValidator cacheValidator) {
            mUser = user;
            mCacheValidator = cacheValidator;
        }

        @Override
        public void onLoadingStarted(String imageUri, View view) {
            File discCache = DiskCacheUtils.findInCache(imageUri, ImageLoader.getInstance().getDiscCache());
            mClearFromCache = !mCacheValidator.useCachedVersionIfAvailable(mUser) &&
                    discCache != null &&
                    discCache.exists() &&
                    discCache.lastModified() + CACHE_VALIDITY < System.currentTimeMillis();
        }

        @Override
        public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

        }

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {

            if (mClearFromCache) {
                DiskCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getDiscCache());
                downloadUserImage((ImageView) view, mUser, mCacheValidator);
                mCacheValidator.onImageCached(mUser);
            }
        }

        @Override
        public void onLoadingCancelled(String imageUri, View view) {

        }
    }

    private static class CropToCircleProcessor implements BitmapProcessor {

        @Override
        public Bitmap process(Bitmap bitmap) {
            // TODO check whether we can use a more memory efficient encoding than ARGB_8888 for the outline Bitmap.
            Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, bitmap.getWidth() / 2, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);

            return output;
        }
    }

    public interface CacheValidator {
        boolean useCachedVersionIfAvailable(User user);
        void onImageCached(User user);
    }

}
