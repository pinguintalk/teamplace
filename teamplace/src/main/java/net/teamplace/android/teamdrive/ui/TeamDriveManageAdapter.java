package net.teamplace.android.teamdrive.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.Rights;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.quota.TeamdriveQuotaModel;
import net.teamplace.android.teamdrive.UserImageLoader;
import net.teamplace.android.teamdrive.widget.QuotaView;
import net.teamplace.android.utils.CloudCentralHelper;
import net.teamplace.android.utils.TeamDriveUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

public class TeamDriveManageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder >
{

	private static final int TYPE_HEADER = 0;
	private static final int TYPE_USER = 1;
	private static final int TYPE_INVITED_USER = 2;

	private HeaderViewHolder mHeaderViewHolder;
	private final Activity mContext;
	private TeamDrive mToManage;
	private TeamdriveQuotaModel mQuota;
	private boolean mAnimateQuota = true;
	private final List<User> mUserList = new ArrayList<>();
	private final List<User> mInvitedUserList = new ArrayList<>();
	private final List<User> mDisplayableUsers = new ArrayList<>();

	private UserImageLoader.CacheValidator mCacheValidator = new UserImageLoader.CacheValidator() {

		private List<User> mLoadedUsers = new ArrayList<>();

		@Override
		public boolean useCachedVersionIfAvailable(User user) {
			return mLoadedUsers.contains(user);
		}

		@Override
		public void onImageCached(User user) {
			mLoadedUsers.add(user);
		}
	};

	TeamDriveManageAdapter(Activity ctx, TeamDrive toManage)
	{
		this.mContext = ctx;
		setTeamDrive(toManage);
	}

	public void setTeamDrive(TeamDrive toManage)
	{
		this.mToManage = toManage;
		this.mUserList.clear();
		this.mUserList.addAll(Arrays.asList(toManage.getUsers()));
		this.mInvitedUserList.clear();
		this.mInvitedUserList.addAll(Arrays.asList(toManage.getInvitedUsers()));
		Collections.sort(mUserList, COMPARATOR);
		Collections.sort(mInvitedUserList, COMPARATOR);
		mDisplayableUsers.clear();
		mDisplayableUsers.addAll(mUserList);
		mDisplayableUsers.addAll(mInvitedUserList);
	}

	//    need to override this method
	@Override
	public int getItemViewType(int position) {
		if(position==0)
			return TYPE_HEADER;
		else {
			User user = mDisplayableUsers.get(position - 1);
			if (mInvitedUserList.contains(user)) {
				return TYPE_INVITED_USER;
			}
		}
		return TYPE_USER;
	}

	public void setQuota(TeamdriveQuotaModel quota, boolean animate) {
		mQuota = quota;
		mAnimateQuota = animate;
		lazyInitQuotaView();
	}

	private void lazyInitQuotaView() {
		if (mHeaderViewHolder != null
				&& mHeaderViewHolder.quotaView != null
				&& mHeaderViewHolder.quotaView.getVisibility() == View.VISIBLE) {
			initQuotaView();
		}
	}

	private void initQuotaView() {
		QuotaView qv = mHeaderViewHolder.quotaView;
		if (mQuota == null || mToManage == null) {
			qv.setQuotaKnown(false);
			return;
		}

		if (mToManage.isQuotaUnlimited()) {
			qv.setUnlimited(true);
		} else {
			qv.setUnlimited(false);
			qv.setMaxBytes((mQuota.remainingMb + mQuota.usedMb) * 1024 * 1024);
		}
		qv.setQuotaKnown(true);
		float quotaRatio = (float) mQuota.usedPercent / 100.f;
		if (mAnimateQuota) {
			mAnimateQuota = false;
			qv.animateQuota(quotaRatio);
		} else {
			qv.setQuota(quotaRatio);
		}
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	//increasing getItemcount to 1. This will be the row of header.
	@Override
	public int getItemCount() {
		return mDisplayableUsers.size() + 1;
	}

	@Override
	public RecyclerView.ViewHolder  onCreateViewHolder(ViewGroup parent, int viewType) {

		if(viewType == TYPE_HEADER)
		{
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tmd_manage_info, parent, false);
			return  new HeaderViewHolder(v);
		}
		else if(viewType == TYPE_USER)
		{
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tmd_manage_user_item, parent, false);
			return new ItemViewHolder(v, viewType);
		}
		else if(viewType == TYPE_INVITED_USER)
		{
			View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.tmd_manage_invited_user, parent, false);
			return new ItemViewHolder(v, viewType);
		}
		throw new RuntimeException("There is no type " + viewType + " that matches " );
	}

	// Replace the contents of a view (invoked by the layout manager)
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

		if(holder instanceof HeaderViewHolder)
		{
			mHeaderViewHolder = (HeaderViewHolder) holder;
			setHeaderViewValues(mHeaderViewHolder);
		}
		else if(holder instanceof ItemViewHolder)
		{
			User user = mDisplayableUsers.get(position - 1);
			ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
			itemViewHolder.userAvatar.setTag(user);
			itemViewHolder.userAvatar.setImageResource(R.drawable.tmd_default_icon);
			UserImageLoader.downloadUserImage(itemViewHolder.userAvatar, user, mCacheValidator);
			itemViewHolder.userNameTv.setText(getDisplayName(user));
			itemViewHolder.userRoleTv.setText(user != null ? TeamDriveUtils.getMemberRoleString(mContext, user.getRole()) : "");
			itemViewHolder.userInfoTv.setText(getInfo(user, ((ItemViewHolder) holder).type == TYPE_INVITED_USER));

		}
	}



	private static String getDisplayName(User user)
	{
		if (user == null)
		{
			return "";
		}
		String displayName = user.getDisplayName();
		if (displayName == null || displayName.length() == 0)
		{
			displayName = user.getEmail();
		}
		return displayName;
	}

	private String getInfo(User user, boolean isInvitation)
	{
		if (isInvitation)
		{
			return mContext.getString(R.string.tmd_invitation);
		}
		return user != null && (user.getAboutMe() != null && user.getAboutMe().length() > 0)
				? user.getAboutMe() : showEmailIfNoDisplayName(user);
	}

	private String showEmailIfNoDisplayName(User user)
	{
		if (user == null)
		{
			return "";
		}

		String displayName = user.getDisplayName();
		if (displayName != null && (!displayName.isEmpty()) )
		{
			return user.getEmail();
		}
		return "";
	}

	private void setHeaderViewValues(HeaderViewHolder headerViewHolder)
	{

		User[] users = mToManage.getUsers();
		String memberString = (users != null)
				? TeamDriveUtils.formatMemberString(mContext, users.length)
				: "";
		headerViewHolder.tvMemberNumber.setText(memberString);

		Rights potentialRights = mToManage.getPotentialRights();
		if ((potentialRights != null && potentialRights.can(Rights.ADD_USER)))
		{
			headerViewHolder.addMemberView.setOnClickListener(mAddMemberClickListener);
		}
		else
		{
			headerViewHolder.addMemberView.setVisibility(View.GONE);
			headerViewHolder.devider.setVisibility(View.GONE);
		}

		if (potentialRights != null && potentialRights.can(Rights.UP_OR_DOWNGRADE))
		{
			headerViewHolder.btnUpgrade.setOnClickListener(mBtnUpgradeClickListener);
		}
		else
		{
			headerViewHolder.btnUpgrade.setVisibility(View.GONE);
		}

		headerViewHolder.btnManage.setOnClickListener(mBtnManageClickListener);

		lazyInitQuotaView();
	}



	final private View.OnClickListener mBtnManageClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			CloudCentralHelper.startTeamdriveManagement(mContext, mToManage.getId());
		}
	};

	final private View.OnClickListener mAddMemberClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			boolean canInvite = new SessionManager(mContext).getLastSession().getTeamDriveGlobalConfig()
					.getRights().can(Rights.ADD_USER);
			if (canInvite)
			{
				Fragment frag = TeamDriveAddMembersFragment.newInstance(mToManage.getId());
				FragmentManager fm = mContext.getFragmentManager();
				// TODO: !!! Must setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE), otherwise popping the
				// fragment won't change the UI (will work correctly after a rotation)! Why does this happen?
				fm.beginTransaction()
						.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
						.replace(R.id.container, frag)
						.addToBackStack(null)
						.commit();
				setQuota(null, true); // make sure we display a null quota when resuming

			}
			else
			{
				FeedbackToast.show(mContext, false, mContext.getString(R.string.tmd_err_user_not_activated));
			}
		}
	};

	final private View.OnClickListener mBtnUpgradeClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			Rights rights = mToManage.getRights();
			if (rights.can(Rights.UP_OR_DOWNGRADE))
			{
				CloudCentralHelper.startTeamdriveUpgrade(mContext, mToManage.getId());
			}
			else
			{
				FeedbackToast.show(mContext, false, mContext.getString(R.string.tmd_err_user_not_activated));
			}
		}
	};

	private final static Comparator<User> COMPARATOR = new Comparator<User>()
	{
		@Override
		public int compare(User lhs, User rhs)
		{
			int compare = rhs.getRole() - lhs.getRole();
			if (compare == 0)
			{
				String lhsDisplayName = getDisplayName(lhs);
				String rhsDisplayName = getDisplayName(rhs);
				compare = lhsDisplayName.toUpperCase(Locale.US).compareTo(rhsDisplayName.toUpperCase(Locale.US));
			}
			return compare;
		}
	};

	public static class HeaderViewHolder extends RecyclerView.ViewHolder {

		QuotaView quotaView;
		TextView tvMemberNumber;
		RelativeLayout addMemberView;
		View devider;
		Button btnUpgrade;
		Button btnManage;
		View rootView;

		public HeaderViewHolder(View root) {
			super(root);
			rootView = root;
			quotaView = (QuotaView) root.findViewById(R.id.quota);
			tvMemberNumber = (TextView) root.findViewById(R.id.member_no);
			addMemberView = (RelativeLayout) root.findViewById(R.id.rl_add_member);
			devider = root.findViewById(R.id.divider_memberno_addmembers);
			btnUpgrade = (Button) root.findViewById(R.id.btn_upgrade);
			btnManage = (Button) root.findViewById(R.id.btn_manage);
		}

	}

	public static class ItemViewHolder extends RecyclerView.ViewHolder {

		ImageView userAvatar;
		TextView userNameTv;
		TextView userRoleTv;
		TextView userInfoTv;
		int type;

		public ItemViewHolder(View v, int type) {
			super(v);
			this.type = type;
			userAvatar = (ImageView) v.findViewById(R.id.img_user_avatar);
			userNameTv = (TextView) v.findViewById(R.id.txt_user_name);
			userRoleTv = (TextView) v.findViewById(R.id.txt_user_role);
			userInfoTv = (TextView) v.findViewById(R.id.txt_user_info);
		}
	}

}
