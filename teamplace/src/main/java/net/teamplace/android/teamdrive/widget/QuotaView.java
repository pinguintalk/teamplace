package net.teamplace.android.teamdrive.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.cortado.android.R;

public class QuotaView extends View
{
	public static final float DEFAULT_QUOTA_PERCENT = 0.f;
	public static final float DEFAULT_QUOTA_TEXT_SIZE_SP = 36.f;
	public static final float DEFAULT_CAPTION_TEXT_SIZE_SP = 12.f;
	public static final float DEFAULT_CAPTION_MARGIN_DP = 12.f;
	public static final float DEFAULT_CIRCLE_THICKNESS_DP = 10.f;

	private static final float WARNING_QUOTA_THREASHOLD = .7f;
	private static final float LOW_QUOTA_THREASHOLD = .9f;

	private static final int NO_COLOR = -1;

	private static final long ANIMATION_DURATION = 1000; // ms
	protected static final long ANIMATION_START_DELAY = 150;

	private float mPercentage;
	private final float mDensity;
	private float mStrokeWidth; // px
	private float mCaptionMargin; // px

	private int mTextColor = NO_COLOR;

	private final Paint mDefaultPaint = new Paint();
	private final Paint mOkQuotaPaint = new Paint();
	private final Paint mWarningQuotaPaint = new Paint();
	private final Paint mLowQuotaPaint = new Paint();

	private final Paint mPercentageTextPaint = new Paint();
	private final Paint mCaptionTextPaint = new Paint();
	private final Paint mWaitTextPaint = new Paint();

	private final Rect mPercentageTextBounds = new Rect();
	private final Rect mCaptionTextBounds = new Rect();
	private final Rect mWaitTextBounds = new Rect();
	private final RectF mCircleBounds = new RectF();
	private long mMaxBytes = 10 * 1024 * 1024 * 1024L; // 10 gb

	private final String mUnlimitedLabel;
	private boolean mIsUnlimited = false;
	private boolean mIsQuotaKnown = false;

	public QuotaView(Context context)
	{
		this(context, null);
	}

	public QuotaView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public QuotaView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);

		mUnlimitedLabel = getContext().getString(R.string.qta_unlimited);
		mDensity = context.getResources().getDisplayMetrics().density;
		mDefaultPaint.setColor(Color.parseColor("#f1f0ee"));
		mOkQuotaPaint.setColor(Color.parseColor("#9db877"));
		mWarningQuotaPaint.setColor(Color.parseColor("#f2ba00"));
		mLowQuotaPaint.setColor(Color.parseColor("#cc5c33"));

		mDefaultPaint.setStyle(Paint.Style.STROKE);
		mOkQuotaPaint.setStyle(Paint.Style.STROKE);
		mWarningQuotaPaint.setStyle(Paint.Style.STROKE);
		mLowQuotaPaint.setStyle(Paint.Style.STROKE);

		mDefaultPaint.setAntiAlias(true);
		mOkQuotaPaint.setAntiAlias(true);
		mWarningQuotaPaint.setAntiAlias(true);
		mLowQuotaPaint.setAntiAlias(true);
		mPercentageTextPaint.setAntiAlias(true);
		mCaptionTextPaint.setAntiAlias(true);
		mWaitTextPaint.setAntiAlias(true);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.QuotaView);
		try
		{
			setQuota(a.getFloat(R.styleable.QuotaView_quota, DEFAULT_QUOTA_PERCENT));
			setTextColor(a.getColor(R.styleable.QuotaView_textColor, NO_COLOR));
			setQuotaTextSize(a.getDimension(R.styleable.QuotaView_quotaTextSize, DEFAULT_QUOTA_TEXT_SIZE_SP * mDensity));
			setCaptionTextSize(a.getDimension(R.styleable.QuotaView_captionTextSize, DEFAULT_CAPTION_TEXT_SIZE_SP
					* mDensity));
			setCaptionMargin(a.getDimension(R.styleable.QuotaView_captionMargin, DEFAULT_CAPTION_MARGIN_DP * mDensity));
			setWaitTextSize(a.getDimension(R.styleable.QuotaView_captionTextSize, DEFAULT_CAPTION_TEXT_SIZE_SP
					* mDensity));
			setCircleThickness(a.getDimension(R.styleable.QuotaView_circleThickness, DEFAULT_CIRCLE_THICKNESS_DP
					* mDensity));
		}
		finally
		{
			a.recycle();
		}
	}

	public void setQuotaKnown(boolean isKnown)
	{
		mIsQuotaKnown = isKnown;
		invalidate();
	}

	public void setQuota(float percent)
	{
		checkPercentOrThrow(percent);
		mPercentage = percent;
		invalidate();
	}

	public void setTextColor(int color)
	{
		mTextColor = color;
		invalidate();
	}

	public void unsetTextColor()
	{
		mTextColor = NO_COLOR;
		invalidate();
	}

	public void setQuotaTextSize(float sizePx)
	{
		mPercentageTextPaint.setTextSize(sizePx);
		invalidate();
	}

	public void setCaptionTextSize(float sizePx)
	{
		mCaptionTextPaint.setTextSize(sizePx);
		invalidate();
	}

	public void setCaptionMargin(float sizePx)
	{
		mCaptionMargin = sizePx;
		invalidate();
	}

	public void setWaitTextSize(float sizePx)
	{
		mWaitTextPaint.setTextSize(sizePx);
		invalidate();
	}

	public void setCircleThickness(float strokeWidthPx)
	{
		mStrokeWidth = strokeWidthPx;
		mDefaultPaint.setStrokeWidth(strokeWidthPx);
		mOkQuotaPaint.setStrokeWidth(strokeWidthPx);
		mWarningQuotaPaint.setStrokeWidth(strokeWidthPx);
		mLowQuotaPaint.setStrokeWidth(strokeWidthPx);
		invalidate();
	}

	public void setMaxBytes(long bytes)
	{
		mMaxBytes = bytes;
		invalidate();
	}

	public void setUnlimited(boolean isUnlimited)
	{
		mIsUnlimited = isUnlimited;
	}

	public void animateQuota(final float percent)
	{
			setQuota(0.f);
			// Nothing to animate
			if (Float.compare(percent, 0.f) <= 0)
			{
				return;
			}

			ValueAnimator anim = ValueAnimator.ofFloat(0, percent);
			anim.setStartDelay(ANIMATION_START_DELAY);
			anim.setDuration(ANIMATION_DURATION);
			anim.setInterpolator(new AccelerateDecelerateInterpolator());
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					setQuota(percent);
				}
			});
			anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
			{
				@Override
				public void onAnimationUpdate(ValueAnimator animation)
				{
					setQuota((Float) animation.getAnimatedValue());
				}
			});
			anim.start();
	}

	@SuppressLint("DefaultLocale")
	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		int h = getHeight();
		int w = getWidth();
		float hspace = h - getPaddingLeft() - getPaddingRight() - 2.f * mStrokeWidth;
		float wspace = w - getPaddingTop() - getPaddingBottom() - 2.f * mStrokeWidth;
		float side = hspace < wspace ? hspace : wspace;
		float left = (w - side) / 2.f;
		float top = (h - side) / 2.f;
		mCircleBounds.set(left, top, left + side, top + side);

		Paint p = selectPaintFromPercentage();

		if (!mIsQuotaKnown || mIsUnlimited)
		{
			canvas.drawArc(mCircleBounds, 90.f, 360.f, false, mDefaultPaint);
		}
		else
		{
			float angle = mPercentage * 360.f;
			canvas.drawArc(mCircleBounds, -90.f, angle, false, p);
			canvas.drawArc(mCircleBounds, angle - 90.f, 360.f - angle, false, mDefaultPaint);
		}

		drawText(canvas, p, h, w);
	}

	private void drawText(Canvas canvas, Paint percentagePaint, int viewHeight, int viewWidth)
	{
		int textColor = (mTextColor != NO_COLOR) ? mTextColor : percentagePaint.getColor();
		if (!mIsQuotaKnown)
		{
			drawWaitText(canvas, textColor, viewWidth, viewHeight);
			return;
		}
		mCaptionTextPaint.setColor(textColor);
		mPercentageTextPaint.setColor(textColor);

		String content;
		if (mIsUnlimited)
		{
			content = mUnlimitedLabel;
			setQuotaTextSize(30 * mDensity);
		}
		else
		{
			int percent = (int) (mPercentage * 100.f + .5f);
			content = String.format("%d%%", percent);
		}

		mPercentageTextPaint.getTextBounds(content, 0, content.length(), mPercentageTextBounds);

		Context context = getContext();
		String free = formatFileSize(context, (long) (mPercentage * mMaxBytes + .5));
		String total = formatFileSize(context, mMaxBytes);

		String captionFormatted;

		if (mIsUnlimited)
		{
			captionFormatted = String.format("%s", free);
		}
		else
		{
			captionFormatted = String.format("%s/%s", free, total);
		}

		mCaptionTextPaint.getTextBounds(captionFormatted, 0, captionFormatted.length(), mCaptionTextBounds);

		float totalTextHeight = mPercentageTextBounds.height() + mCaptionMargin + mCaptionTextBounds.height();

		canvas.drawText(content, (viewWidth - mPercentageTextBounds.width()) / 2.f, mPercentageTextBounds.height()
				+ (viewHeight - totalTextHeight) / 2.f, mPercentageTextPaint);

		canvas.drawText(captionFormatted, (viewWidth - mCaptionTextBounds.width()) / 2.f,  (viewHeight + totalTextHeight) / 2.f,
				mCaptionTextPaint);
	}

	private void drawWaitText(Canvas canvas, int textColor, int viewWidth, int viewHeight)
	{
		mWaitTextPaint.setColor(textColor);
		String waitStr = getContext().getString(R.string.qta_calculating);
		mWaitTextPaint.getTextBounds(waitStr, 0, waitStr.length(), mWaitTextBounds);
		canvas.drawText(waitStr, (viewWidth - mWaitTextBounds.width()) / 2.f, viewHeight / 2.f,
				mWaitTextPaint);
	}

	private Paint selectPaintFromPercentage()
	{
		if (mPercentage >= LOW_QUOTA_THREASHOLD)
			return mLowQuotaPaint;

		if (mPercentage >= WARNING_QUOTA_THREASHOLD)
			return mWarningQuotaPaint;

		return mOkQuotaPaint;
	}

	private void checkPercentOrThrow(float percent)
	{
		// If percent is NaN, the comparison return false and the exception is thrown
		if (!(0.f <= percent && percent <= 1.f))
			throw new IllegalArgumentException("percent must be in [0.f, 1.f], was: " + percent);
	}

	private String formatFileSize(Context context, float bytes)
	{
		String gb = context.getString(R.string.qta_gb);
		float bytesInGB = bytes / 1024 / 1024 / 1024;
		if (bytesInGB * 1024 < 100)
		{
			bytesInGB = 0.1f;
		}
		return String.format("%.1f " + gb, bytesInGB);
	}
}
