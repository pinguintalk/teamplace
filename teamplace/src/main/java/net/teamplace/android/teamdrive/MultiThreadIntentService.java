package net.teamplace.android.teamdrive;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

public abstract class MultiThreadIntentService extends Service
{
	private String name;
	private boolean redelivery;

	private volatile int executingNo = 0;

	public MultiThreadIntentService(String name)
	{
		this.name = name;
	}

	public void setIntentRedelivery(boolean enabled)
	{
		this.redelivery = enabled;
	}

	@Override
	public void onCreate()
	{
		super.onCreate();
		instantiateWorkers();
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId)
	{
		Worker worker = getWorkerForIntent(intent);
		Worker.ServiceHandler handler = worker.getHandler();
		Message msg = handler.obtainMessage();
		msg.arg1 = startId;
		msg.obj = intent;
		handler.sendMessage(msg);
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		onStart(intent, startId);
		return redelivery ? START_REDELIVER_INTENT : START_NOT_STICKY;
	}

	@Override
	public void onDestroy()
	{
		for (Worker worker : getAllWorkers())
		{
			worker.getLooper().quit();
		}
		super.onDestroy();
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		return null;
	}

	private synchronized void lazyStop(int startId)
	{
		executingNo--;
		if (executingNo == 0)
		{
			stopSelf(startId);
		}
	}

	protected abstract class Worker
	{

		private volatile Looper looper;
		private volatile ServiceHandler serviceHandler;

		public Worker(String name)
		{
			HandlerThread thread = new HandlerThread("MultiThreadIntentService worker "
					+ MultiThreadIntentService.this.name + "#" + name);
			thread.start();
			looper = thread.getLooper();
			serviceHandler = new ServiceHandler(looper);
		}

		private final class ServiceHandler extends Handler
		{

			public ServiceHandler(Looper looper)
			{
				super(looper);
			}

			@Override
			public void handleMessage(Message msg)
			{
				try
				{
					executingNo++;
					onHandleIntent((Intent) msg.obj);
				}
				finally
				{
					lazyStop(msg.arg1);
				}
			}
		}

		private ServiceHandler getHandler()
		{
			return serviceHandler;
		}

		private Looper getLooper()
		{
			return looper;
		}

		abstract void onHandleIntent(Intent intent);
	}

	protected abstract Worker getWorkerForIntent(Intent intent);

	protected abstract Worker[] getAllWorkers();

	protected abstract void instantiateWorkers();

}
