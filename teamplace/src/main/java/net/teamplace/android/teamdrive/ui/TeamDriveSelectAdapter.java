package net.teamplace.android.teamdrive.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.Payment;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.TeamDriveUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TeamDriveSelectAdapter extends ArrayAdapter<Object>
{
	private OnTeamActionsMenuClickListener mActionsListener;
	private Context context;
	public TeamDriveSelectAdapter(Context context, List<Object> objects)
	{
		super(context, 0, objects);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		View v = convertView;
		if (v == null)
		{
			LayoutInflater inflater = LayoutInflater.from(getContext());
			v = inflater.inflate(R.layout.tmd_default_item, parent, false);
			ViewHolder holder = new ViewHolder();
			holder.txtName = (TextView) v.findViewById(R.id.name);
			holder.txtUserInfo = (TextView) v.findViewById(R.id.members);
			holder.txtExpireInfo = (TextView) v.findViewById(R.id.expires_info);
			holder.btnInfo = (ImageView) v.findViewById(R.id.btn_popupmenu);
			holder.expWarningImg = (ImageView) v.findViewById(R.id.img_warning);
			v.setTag(holder);
		}

		Object item = getItem(position);
		setViewValues(v, item);
		return v;
	}

	void setOnTeamActionsMenuClickedListener(OnTeamActionsMenuClickListener listener)
	{
		mActionsListener = listener;
	}

	void sort()
	{
		sort(null);
	}

	@Override
	public void sort(Comparator<? super Object> comparator)
	{
		super.sort(COMPARATOR);
	}

	void clearIncomingInvitations()
	{
		Object item = null;
		List<IncomingInvitation> toRemove = new ArrayList<IncomingInvitation>();
		for (int i = 0; i < getCount(); i++)
		{
			item = getItem(i);
			if (item instanceof IncomingInvitation)
			{
				toRemove.add((IncomingInvitation) item);
			}
		}
		for (IncomingInvitation invitation : toRemove)
		{
			remove(invitation);
		}
	}

	void clearTeamDrives()
	{
		Object item = null;
		List<TeamDrive> toRemove = new ArrayList<TeamDrive>();
		for (int i = 0; i < getCount(); i++)
		{
			item = getItem(i);
			if (item instanceof TeamDrive)
			{
				toRemove.add((TeamDrive) item);
			}
		}
		for (TeamDrive teamDrive : toRemove)
		{
			remove(teamDrive);
		}
	}

	private void setViewValues(final View driveView, Object item)
	{
		ViewHolder holder = (ViewHolder) driveView.getTag();
		if (item instanceof TeamDrive)
		{
			driveView.setBackgroundResource(R.drawable.tmd_item_sel);
			final TeamDrive driveItem = (TeamDrive) item;
			String name = driveItem.getName();
			holder.txtName.setTextColor(getContext().getResources().getColor(R.color.brw_default_item_name_font));
			holder.txtName.setText(name);
			holder.btnInfo.setImageResource(R.drawable.tmd_info_btn);
			holder.btnInfo.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if (mActionsListener != null) {
						mActionsListener.onActionsMenuClicked(driveItem, v, driveView);
					}
				}
			});

			int count = driveItem.getUsers().length;
			String members = TeamDriveUtils.formatMemberString(getContext(), count);
			holder.txtUserInfo.setText(members);

			long now = System.currentTimeMillis();
			long paiduntil = driveItem.getPayment().getPaidUntil();
			if (now > paiduntil) {
				holder.txtExpireInfo.setText("Expired");
			} else {
				SimpleDateFormat ft = new SimpleDateFormat ("yyyy-MM-dd");
				holder.txtExpireInfo.setText("Expires on " + ft.format(new Date(paiduntil)));
			}
			setUpExpirationViews(holder.txtExpireInfo,holder.expWarningImg, driveItem);
			// TODO check if expired
			// holder.btnPopupMenu.setVisibility(View.GONE);
			// holder.btnUpgrade.setVisibility(View.VISIBLE);
		}
		else if (item instanceof IncomingInvitation)
		{
			final IncomingInvitation inviteItem = (IncomingInvitation) item;
			String name = inviteItem.getTeamDriveName();

			driveView.setBackgroundResource(R.drawable.tmd_invitation_item_sel);
			holder.txtName.setTextColor(getContext().getResources().getColor(R.color.tmd_infotextcolor));
			holder.txtName.setText(name);
			holder.txtUserInfo.setText(getContext().getText(R.string.tmd_invitation));
			holder.btnInfo.setImageResource(R.drawable.tmd_invitationbtn);
			holder.btnInfo.setClickable(false);
		}
	}

	private void setUpExpirationViews(TextView tvExpires, ImageView ivExpirationWarning, TeamDrive driveItem)
	{
		Payment payment = driveItem.getPayment();
		Long validuntil = payment != null ? payment.getPaidUntil() : null;

		if (validuntil == null)
		{
			tvExpires.setVisibility(View.INVISIBLE);
			ivExpirationWarning.setVisibility(View.GONE);
			return;
		}

		String text = null;
		int imgVisibility = View.VISIBLE;
		int textColor = context.getResources().getColor(R.color.tmd_expired_warning);

		long currentTime = System.currentTimeMillis();
		long timeToExpire = validuntil - currentTime;
		long timeFromTomorrow = validuntil - GenericUtils.tomorrowStartsAt(currentTime);
		long aDay = 1000 * 60 * 60 * 24;

		if (timeToExpire < 0)
		{
			text = context.getString(R.string.tmd_expired);
		}
		else if (timeFromTomorrow < 0)
		{
			text = context.getString(R.string.tmd_expires_today);
		}
		else if (timeFromTomorrow < aDay)
		{
			text = String.format(context.getString(R.string.tmd_expires_tomorrow));
		}
		else if (timeFromTomorrow < aDay * 2)
		{
			text = String.format(context.getString(R.string.tmd_expires_in_1_day));
		}
		else if (timeFromTomorrow < aDay * 6)
		{
			text = String.format(context.getString(R.string.tmd_expires_in_x_days), (timeFromTomorrow / aDay));
		}
		else
		{
			//text = String.format(context.getString(R.string.tmd_expires_on),
			//		DateFormat.getDateFormat(context).format(validuntil));
			//textColor = context.getResources().getColor(R.color.brw_default_item_file_info_font);
			imgVisibility = View.GONE;
		}

		tvExpires.setVisibility(View.VISIBLE);
		tvExpires.setText(text);
		tvExpires.setTextColor(textColor);
		ivExpirationWarning.setVisibility(imgVisibility);
	}
	// makes IncomingInvitations appear on top and sorts by name
	private final static Comparator<Object> COMPARATOR = new TeamDriveComparator();

	/** A overflow-like menu that represents Teamdrive actions has been clicked */
	public interface OnTeamActionsMenuClickListener
	{
		void onActionsMenuClicked(Object drive, View anchor, View driveView);
	}

	private static class ViewHolder
	{
		TextView txtName;
		TextView txtUserInfo;
		TextView txtExpireInfo;
		ImageView expWarningImg;
		ImageView btnInfo;
	}

}
