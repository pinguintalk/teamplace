package net.teamplace.android.teamdrive;

import net.teamplace.android.http.teamdrive.User;

public interface GetUserImageListener
{
	void onLoadRequested(User user, ImageResultCallback callback);
}
