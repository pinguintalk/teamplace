package net.teamplace.android.teamdrive.ui;

import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.util.Rfc822Tokenizer;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;

import com.android.ex.chips.BaseRecipientAdapter;
import com.android.ex.chips.RecipientEditTextView;
import com.cortado.android.R;

import net.teamplace.android.browsing.widget.LockedSwipeRefreshLayout;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.NoConnectivityException;
import net.teamplace.android.http.teamdrive.DriveCreation;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.UserListInvitation;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.TeamDriveUtils;
import net.teamplace.android.utils.TeamDriveUtils.InvalidEmailException;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

public class TeamDriveNewDriveFragment extends Fragment implements TextWatcher
{
	public static final String TAG = tag(TeamDriveNewDriveFragment.class);
	public static final long CREATE_TEAMDRIVE_REQUEST_CODE = 389044;

	private static final String PKG = pkg(TeamDriveNewDriveFragment.class);
	private static final String KEY_INST_IS_ENABLED = PKG + ".isEnabled";
	private static final String KEY_CAN_ADD_MEMBERS = "can add members";
	private static final int CREATE_BUTTON_INDEX = 0;

	/** Whether the create button is enabled. Sync with syncUIEditText() */
	private boolean mCanCreate;
	private boolean mIsUIEnabled = true;

	private LockedSwipeRefreshLayout mProgressBar;

	private EditText mNameTv;
	private List<View> mViews;
	private Menu mMenu;

	public static TeamDriveNewDriveFragment newInstance(boolean canAddMembers)
	{
		TeamDriveNewDriveFragment instance = new TeamDriveNewDriveFragment();
		Bundle data = new Bundle();
		data.putBoolean(KEY_CAN_ADD_MEMBERS, canAddMembers);
		instance.setArguments(data);
		return instance;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
		setUpActionBar();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.tmd_new_drive_fragment, container, false);
		setupProgressBar(root);
		setupNameTv(root);
		saveViews();
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		restoreInstanceState(savedInstanceState);

		if (!mIsUIEnabled)
		{
			disableUI();
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();
		syncCanCreateEditText();
		mNameTv.addTextChangedListener(this);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		mNameTv.removeTextChangedListener(this);
		EventBus.getDefault().unregister(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		super.onSaveInstanceState(outState);
		outState.putBoolean(KEY_INST_IS_ENABLED, mIsUIEnabled);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		inflater.inflate(R.menu.tmd_new_drive_menu, menu);
		mMenu = menu;
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// menu.getItem(CREATE_BUTTON_INDEX).setVisible(mIsUIEnabled);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() != R.id.action_create)
		{
			return false;
		}

		if (!mIsUIEnabled)
		{
			return true;
		}

		if (!mCanCreate)
		{
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_new_drive_toast_empty_title));
			return true;
		}

		createTeamDrive();
		return true;
	}

	// Event bus callback from TeamDriveService
	public void onEventMainThread(TeamDriveServiceResponse response)
	{
		if (response.getRequestCode() != CREATE_TEAMDRIVE_REQUEST_CODE)
		{
			return;
		}

		handleCreateTeamDriveResponse(response);
	}

	@Override
	public void afterTextChanged(Editable editable)
	{
		syncCanCreateEditText();
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
		// Nothing
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
		// Nothing
	}

	private void setUpActionBar()
	{
		ActionBar ab = getActivity().getActionBar();
		ab.setTitle(R.string.tmd_new_drive_ab_title);
		ab.setSubtitle(null);
	}

	private void setupProgressBar(View root)
	{
		mProgressBar = (LockedSwipeRefreshLayout) root;
		mProgressBar.setColorSchemeResources(R.color.brw_swipe_color_1, R.color.brw_swipe_color_2,
				R.color.brw_swipe_color_3, R.color.brw_swipe_color_4);
	}

	private void setupNameTv(View root)
	{
		mNameTv = (EditText) root.findViewById(R.id.et_drive_name);
		mNameTv.setSelection(mNameTv.length());
	}


	private void saveViews()
	{
		mViews = new ArrayList<View>();
		mViews.add(mNameTv);
	}

	private void restoreInstanceState(Bundle savedInstanceState)
	{
		if (savedInstanceState != null)
		{
			mIsUIEnabled = savedInstanceState.getBoolean(KEY_INST_IS_ENABLED);
		}
	}

	private boolean isDriveNameEmpty()
	{
		Editable ed = mNameTv.getText();
		return ed == null || TextUtils.isEmpty(ed);
	}

	private String getDriveName()
	{
		Editable ed = mNameTv.getText();
		return ed == null ? "" : ed.toString();
	}


	private void syncCanCreateEditText()
	{
		mCanCreate = !isDriveNameEmpty();
	}

	private void disableUI()
	{
		setUIEnabled(false);
	}

	private void enableUI()
	{
		setUIEnabled(true);
	}

	private void setUIEnabled(boolean isEnabled)
	{
		mIsUIEnabled = isEnabled;
		if(mMenu != null)
			mMenu.getItem(CREATE_BUTTON_INDEX).setVisible(mIsUIEnabled);
		getFragmentManager().invalidateOptionsMenu();
		GraphicsUtils.setRefreshing(mProgressBar, !isEnabled);
		for (View view : mViews)
		{
			view.setEnabled(isEnabled);
		}
	}

	private void createTeamDrive()
	{
		try
		{
			disableUI();
			executeCreateTeamDriveRequest();
		}
		catch (InvalidEmailException e)
		{
			Log.i(TAG, "Invalid email addresses", e);
			enableUI();
			FeedbackToast.show(getActivity(), false, getString(R.string.tmd_new_drive_toast_contacts_err));
		}
	}

	private void handleCreateTeamDriveResponse(TeamDriveServiceResponse response)
	{
		try
		{
			response.getResponseData();
			String message = String.format(getString(R.string.tmd_new_drive_toast_creation_ok), getDriveName());
			FeedbackToast.show(getActivity(), true, message);
			getFragmentManager().popBackStack();
		}
		catch (NoConnectivityException e) {
			String message = String.format(getString(R.string.tmd_err_no_internet_connection));
			FeedbackToast.show(getActivity(), false, message);
			enableUI();
		}
		catch (ConnectFailedException e)
		{
			String message = String.format(getString(R.string.ini_connect_to_server_failed));
			FeedbackToast.show(getActivity(), false, message);
			enableUI();
		}
		catch (Exception e)
		{
			String message = String.format(getString(R.string.tmd_new_drive_toast_creation_err), getDriveName());
			FeedbackToast.show(getActivity(), false, message);
			enableUI();
		}
	}

	private void executeCreateTeamDriveRequest() throws InvalidEmailException
	{
		DriveCreation drive = new DriveCreation(getDriveName(), null, null, null);

		TeamDriveService.createTeamDrive(getActivity(), drive, CREATE_TEAMDRIVE_REQUEST_CODE);
	}
}