package net.teamplace.android.teamdrive.ui;

import java.io.IOException;

import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.teamdrive.TeamDriveService.UserImageItem;
import net.teamplace.android.utils.LoggingActivity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.cortado.android.R;

import de.greenrobot.event.EventBus;

public class TeamDriveInvitationActivity extends LoggingActivity
{
	// public static final long INVITATION_HANDLING_RESPONSE_CODE = 6655424;
	public static final long REQ_ACCEPT_INVITATION = 456126846;
	public static final long REQ_REJECT_INVITATION = 2145184651;

	public static final int RESULT_LATER = 0;
	public static final int RESULT_REJECT_REQUESTED = 1;
	public static final int RESULT_ACCEPT_REQUESTED = 2;


	private ImageView imgUser;
	private long imgLastRequested;

	private State mState;
	private StateResolver mStateResolver;

	private static class State
	{
		IncomingInvitation invitation;
	}

	private class StateResolver
	{
		public void onEventMainThread(State event)
		{
			mState = event;
		}
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		EventBus.getDefault().registerSticky(mStateResolver);
		setContentView(R.layout.tmd_invitation_activity);
		setTitle("Join a Teamplace");
		String rawInfoStr = getText(R.string.tmd_join_infotext).toString();
		String info = String.format(rawInfoStr,  mState.invitation.getInvitingUserDisplayName(),
				"<b><i>" + mState.invitation.getTeamDriveName() + "</i></b>");
		String username = getDisplayName(mState.invitation);
		((TextView) findViewById(R.id.txt_invitation_info)).setText(Html.fromHtml(info));
		((TextView) findViewById(R.id.txt_inviting_user)).setText(username);
		((TextView) findViewById(R.id.txt_invitation_message)).setText(mState.invitation.getInvitationMessage());
		((ImageButton) findViewById(R.id.btn_join)).setOnClickListener(clicklistener);
		((Button) findViewById(R.id.btn_later)).setOnClickListener(clicklistener);
		((Button) findViewById(R.id.btn_reject)).setOnClickListener(clicklistener);
		imgUser = (ImageView) findViewById(R.id.img_user_avatar);
		if (mState.invitation.getInvitingUserImg() != null)
		{
			imgLastRequested = System.currentTimeMillis();

			int imgSize = getResources().getDrawable(R.drawable.tmd_default_icon).getIntrinsicHeight();
			User invitingUser = new User(mState.invitation.getInvitingUserId(), mState.invitation.getInvitingUserEmail(),
					mState.invitation.getInvitingUserDisplayName(), null, mState.invitation.getInvitingUserImg(), null);
			TeamDriveService.getUserImage(getApplicationContext(), invitingUser, imgSize, imgLastRequested,
					TeamDriveService.getRandomRequestId());
		}
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		EventBus.getDefault().unregister(mStateResolver);
		EventBus.getDefault().postSticky(mState);
		super.onSaveInstanceState(outState);
	}

	public static void startForResult(Fragment frag, IncomingInvitation invitation, int requestCode)
	{
		Intent intent = new Intent(frag.getActivity(), TeamDriveInvitationActivity.class);
		State state = new State();
		state.invitation = invitation;
		EventBus.getDefault().postSticky(state);
		frag.startActivityForResult(intent, requestCode);
	}

	public void onEventMainThread(TeamDriveServiceResponse event)
	{
		/*
		 * if (event.getRequestCode() == REQ_ACCEPT_INVITATION)
		 * {
		 * boolean error = false;
		 * try
		 * {
		 * event.getResponseData();
		 * }
		 * catch (JsonProcessingException e)
		 * {
		 * error = true;
		 * e.printStackTrace();
		 * }
		 * catch (SQLException e)
		 * {
		 * error = true;
		 * e.printStackTrace();
		 * }
		 * catch (CortadoException e)
		 * {
		 * error = true;
		 * e.printStackTrace();
		 * }
		 * catch (IOException e)
		 * {
		 * error = true;
		 * e.printStackTrace();
		 * }
		 * FeedbackToast.show(getApplicationContext(), !error, "");
		 * TeamDriveService.getInvitations(getApplicationContext(), INVITATION_HANDLING_RESPONSE_CODE);
		 * finish();
		 * }
		 * else
		 */if (event.getRequestCode() == imgLastRequested)
		{
			try
			{
				imgUser.setImageBitmap((((UserImageItem[]) event.getResponseData())[0]).getBitmap());
			}
			catch (CortadoException e)
			{
				e.printStackTrace();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
		}
	}

	private String getDisplayName(IncomingInvitation invitation)
	{
		String displayName = invitation.getInvitingUserDisplayName();
		if (displayName != null && displayName.length() == 0)
		{
			displayName = invitation.getInvitingUserEmail();
		}
		return displayName;
	}

	private final OnClickListener clicklistener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			int viewId = v.getId();
			if (viewId == R.id.btn_join)
			{
				TeamDriveService.acceptInvitation(getApplicationContext(), mState.invitation.getId(), REQ_ACCEPT_INVITATION);
				setResult(RESULT_ACCEPT_REQUESTED);
			}
			else if (viewId == R.id.btn_reject)
			{
				TeamDriveService.rejectInvitation(getApplicationContext(), mState.invitation.getId(), REQ_REJECT_INVITATION);
				setResult(RESULT_REJECT_REQUESTED);
			}
			else if (viewId == R.id.btn_later)
			{
				setResult(RESULT_LATER);
			}
			finish();
		}
	};

}
