package net.teamplace.android.action;

public interface ActionFactory
{
	Action build(int id);
}
