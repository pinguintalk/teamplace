package net.teamplace.android.action;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.path.Path;
import android.app.Activity;
import android.content.Intent;


public class PreviewDeleteAction extends FinishingAction
{
	private final Path mPath;

	public PreviewDeleteAction(Activity activity, Path path)
	{
		super(activity);
		mPath = path;
	}

	@Override
	protected Intent provideData(Activity activity)
	{
		Path[] path = new Path[] {mPath};
		return BrowsingService.Contract.buildDeleteRequestIntent(activity, path);
	}
}
