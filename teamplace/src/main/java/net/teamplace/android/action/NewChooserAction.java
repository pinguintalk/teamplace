package net.teamplace.android.action;

import android.app.DialogFragment;
import android.app.Fragment;

import net.teamplace.android.browsing.dialog.NewChooserDialogFragment;

import java.util.HashMap;


public class NewChooserAction extends DialogAction
{
	private final HashMap<String, String> mIllegalInputsForFolder;
	public NewChooserAction(Fragment target,HashMap<String, String> illegalInputs)
	{
		super(NewChooserDialogFragment.TAG, target, 0);
		mIllegalInputsForFolder = illegalInputs;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		return NewChooserDialogFragment.newInstance(mIllegalInputsForFolder);
	}
}
