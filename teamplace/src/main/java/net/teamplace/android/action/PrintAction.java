package net.teamplace.android.action;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.utils.CloudPrinterUtils;
import android.app.Activity;


public class PrintAction implements Action
{
	private final Activity mActivity;
	private Path mToPrint;

	public PrintAction(Activity activity, Path toPrint)
	{
		mActivity = activity;
		mToPrint = toPrint;
	}

	@Override
	public void execute()
	{
		// Intent i = GenericUtils.buildPrintActivityIntent(mActivity, mToPrint);
		// mActivity.startActivity(i);
		CloudPrinterUtils.print(mActivity, mToPrint);
	}
}
