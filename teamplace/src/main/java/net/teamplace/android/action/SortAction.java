package net.teamplace.android.action;

import net.teamplace.android.browsing.dialog.SortDialogFragment;
import android.app.DialogFragment;
import android.app.Fragment;


public class SortAction extends DialogAction
{
	private final int mSortCategory;
	private final int mSortOrder;

	public SortAction(Fragment target, int sortCategory, int sortOrder)
	{
		super(SortDialogFragment.TAG, target, SortDialogFragment.REQUEST_CODE);
		mSortCategory = sortCategory;
		mSortOrder = sortOrder;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		return SortDialogFragment.newInstance(mSortCategory, mSortOrder);
	}
}
