package net.teamplace.android.action;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.share.ShareActivity;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import android.content.Context;
import android.content.Intent;

import com.cortado.android.R;

public class MailAction implements Action
{
	private final Context mContext;
	private final FileInfo mFileInfo;

	public MailAction(Context context, FileInfo fileInfo)
	{
		mContext = context;
		mFileInfo = fileInfo;
	}

	@Override
	public void execute()
	{
		GlobalConfiguration globalConfig = new SessionManager(mContext).getLastSession().getTeamDriveGlobalConfig();
		boolean isActivatedUser = globalConfig.getPotentialRights().getRights() == globalConfig.getRights().getRights();
		if (isActivatedUser)
		{
			Intent i = ShareActivity.buildIntent(mContext, mFileInfo);
			mContext.startActivity(i);
		}
		else
		{
			FeedbackToast.show(mContext, false, mContext.getString(R.string.tmd_err_user_not_activated));
		}
	}
}
