package net.teamplace.android.action;

// Experimental interface for file actions
public interface Action
{
	void execute();
}
