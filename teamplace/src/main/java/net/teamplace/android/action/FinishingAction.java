package net.teamplace.android.action;

import net.teamplace.android.utils.GenericUtils;
import android.app.Activity;
import android.content.Intent;


public abstract class FinishingAction implements Action
{
	private final Activity mActivity;

	protected FinishingAction(Activity activity)
	{
		mActivity = activity;
	}

	abstract protected Intent provideData(Activity activity);

	@Override
	public void execute()
	{
		Intent data = provideData(mActivity);
		GenericUtils.finishWithData(mActivity, data);
	}
}
