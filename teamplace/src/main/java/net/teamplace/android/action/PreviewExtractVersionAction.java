package net.teamplace.android.action;

import android.app.Activity;
import android.content.Intent;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.path.Path;


public class PreviewExtractVersionAction extends FinishingAction
{
	private final Path mVersionPath;
	private final long mVersionDate;
	//private final long mVersionNumber;

	public PreviewExtractVersionAction(Activity activity, Path versionPath, long versionDate)
	{
		super(activity);
		mVersionPath = versionPath;
		mVersionDate = versionDate;
		//mVersionNumber = versionNumber;
	}

	@Override
	protected Intent provideData(Activity activity)
	{
		return BrowsingService.Contract.buildExtractVersionRequestIntent(activity, mVersionPath, mVersionDate);
	}
}
