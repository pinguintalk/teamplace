package net.teamplace.android.action;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.openin.ActivityResolver;
import net.teamplace.android.browsing.openin.ResolvingActivityDialog;
import android.app.DialogFragment;
import android.app.FragmentManager;


public class ShareAction extends DialogAction
{
	private final FileInfo mFileInfo;

	public ShareAction(FragmentManager fm, FileInfo fileInfo)
	{
		super(ResolvingActivityDialog.TAG, fm);
		mFileInfo = fileInfo;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		return ResolvingActivityDialog.newInstance(mFileInfo, ActivityResolver.ViewOrShareResolver.TYPE);
	}
}
