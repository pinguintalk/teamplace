package net.teamplace.android.action;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;

abstract public class DialogAction implements Action
{
	private static final int NO_REQUEST_CODE = -1;

	private final String mTag;
	private final Fragment mTarget;
	private final int mRequestCode;
	private final FragmentManager mFm;

	protected DialogAction(String tag, FragmentManager fm)
	{
		this(tag, null, NO_REQUEST_CODE, fm);
	}

	protected DialogAction(String tag, Fragment target, int requestCode)
	{
		this(tag, target, requestCode, null);
	}

	private DialogAction(String tag, Fragment target, int requestCode, FragmentManager fm)
	{
		checkNonNullArg(tag);
		checkCondition((target != null && requestCode != NO_REQUEST_CODE) || fm != null,
				"Provide a valid target with a valid request code or a valid FragmentManager");

		mTag = tag;
		mTarget = target;
		mRequestCode = requestCode;
		mFm = fm;
	}

	/** Builds a dialog fragment that will be shown calling {@link #execute()}. Must be non-null */
	public abstract DialogFragment buildDialogFragment();

	@Override
	public void execute()
	{
		DialogFragment dlg = buildDialogFragment();
		FragmentManager fm = mFm;
		if (mTarget != null)
		{
			fm = mTarget.getFragmentManager();
			dlg.setTargetFragment(mTarget, mRequestCode);
		}
		dlg.show(fm, mTag);
	}

	public Fragment getTarget()
	{
		return mTarget;
	}
}
