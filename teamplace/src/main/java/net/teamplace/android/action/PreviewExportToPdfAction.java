package net.teamplace.android.action;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.path.Path;
import android.app.Activity;
import android.content.Intent;


public class PreviewExportToPdfAction extends FinishingAction
{
	private final Path mPath;

	public PreviewExportToPdfAction(Activity activity, Path path)
	{
		super(activity);
		mPath = path;
	}

	@Override
	protected Intent provideData(Activity activity)
	{
		return BrowsingService.Contract.buildExportToPdfIntent(activity, mPath);
	}
}
