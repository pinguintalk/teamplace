package net.teamplace.android.action;

import java.util.ArrayList;

import net.teamplace.android.browsing.dialog.SmartfileDialogFragment;
import net.teamplace.android.browsing.path.Path;

import android.app.DialogFragment;
import android.app.Fragment;


public class SmarfileAction extends DialogAction
{
	private final ArrayList<Path> mPaths;

	public SmarfileAction(Fragment target, ArrayList<Path> paths)
	{
		super(SmartfileDialogFragment.TAG, target, SmartfileDialogFragment.REQUEST_CODE);
		mPaths = paths;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		return SmartfileDialogFragment.newInstance(mPaths);
	}
}
