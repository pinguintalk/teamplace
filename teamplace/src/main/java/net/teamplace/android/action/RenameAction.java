package net.teamplace.android.action;

import java.util.HashMap;

import net.teamplace.android.browsing.dialog.EditTextDialogFragment;

import android.app.DialogFragment;
import android.app.Fragment;

import com.cortado.android.R;

public class RenameAction extends DialogAction
{
	private final String mTitle;
	private final String mToRename;
	private final boolean mIsFolder;
	private final HashMap<String, String> mIllegalInputs;

	public RenameAction(Fragment target, String toRename, boolean isFolder, HashMap<String, String> illegalInputs)
	{
		super(EditTextDialogFragment.TAG, target, R.id.request_code_rename);
		mTitle = target.getString(R.string.brw_action_rename);
		mToRename = toRename;
		mIllegalInputs = illegalInputs;
		mIsFolder = isFolder;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		return EditTextDialogFragment.newInstance(mTitle, mToRename, mIsFolder, mIllegalInputs);
	}

}
