package net.teamplace.android.action;

import android.app.DialogFragment;
import android.app.Fragment;

import com.cortado.android.R;

import net.teamplace.android.browsing.dialog.EditTextDialogFragment;

import java.util.HashMap;

public class NewFolderAction extends DialogAction
{
	private final HashMap<String, String> mIllegalInputs;

	public NewFolderAction(Fragment target,HashMap<String, String> illegalInputs)
	{
		super(EditTextDialogFragment.TAG, target, R.id.request_code_new_folder);
		mIllegalInputs = illegalInputs;
	}

	@Override
	public DialogFragment buildDialogFragment()
	{
		Fragment target = getTarget();
		String title = target.getString(R.string.brw_action_new_folder);
		String text = target.getString(R.string.brw_new_folder_default_name);
		return EditTextDialogFragment.newInstance(title, text, true, mIllegalInputs);
	}
}
