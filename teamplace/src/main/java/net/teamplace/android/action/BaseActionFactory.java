package net.teamplace.android.action;

import com.cortado.android.R;

public class BaseActionFactory implements ActionFactory
{
	@Override
	public Action build(int id)
	{
		if (id == R.id.action_new)
			return buildNewAction();
		if (id == R.id.action_sort)
			return buildSortAction();
		if (id == R.id.action_share)
			return buildShareAction();
		if (id == R.id.action_smartfile)
			return buildSmartfileAction();
		if (id == R.id.action_mail)
			return buildMailAction();
		if (id == R.id.action_zip)
			return buildZipAction();
		if (id == R.id.action_export_to_pdf)
			return buildExportToPdfAction();
		if (id == R.id.action_rename)
			return buildRenameAction();
		if (id == R.id.action_delete)
			return buildDeleteAction();
		if (id == R.id.action_print)
			return buildPrintAction();
		if (id == R.id.action_favorite)
			return buildFavoriteAction();
		if (id == R.id.action_unfavorite)
			return buildUnfavoriteAction();
		if (id == R.id.action_extract_version)
			return buildExtractVersionAction();
		if (id == R.id.action_delete_version)
			return buildDeleteVersionAction();
		if (id == R.id.action_activity_stream)
			return buildActivityStreamAction();

		throw new IllegalArgumentException("Unrecognized id");
	}

	protected Action buildNewAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildSortAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildShareAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildSmartfileAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildMailAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildZipAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildExportToPdfAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildRenameAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildDeleteAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildPrintAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildFavoriteAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildUnfavoriteAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildExtractVersionAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildDeleteVersionAction()
	{
		throw new UnsupportedOperationException();
	}

	protected Action buildActivityStreamAction()
	{
		throw new UnsupportedOperationException();
	}
}
