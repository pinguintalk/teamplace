package net.teamplace.android.services;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;

import net.teamplace.android.backend.BackendServers;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.content.IntentBus;
import net.teamplace.android.content.IntentBusImpl;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.request.account.ProfileRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.Profile;
import net.teamplace.android.utils.Log;

import java.io.IOException;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Created by nangu on 07.08.15.
 */
public class ProfileService extends IntentService {

    private static final String TAG = tag(ProfileService.class);
    private static final String WORKER_NAME = "profile worker";

    private static final String ACTION_GET_PROFILE = "get ser profile";
    private static final String EXTRA_REQUEST_CODE = "request code";
    private static final String EXTRA_USER_ID = "user id";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param WORKER_NAME Used to name the worker thread, important only for debugging.
     */
    public ProfileService() {
        super(WORKER_NAME);
    }


    public static void getProfile(Context ctx, int requestCode)
    {
        Intent i = new Intent(ctx, ProfileService.class);
        i.setAction(ACTION_GET_PROFILE);
        i.putExtra(EXTRA_REQUEST_CODE, requestCode);
        ctx.startService(i);
    }

    public static String getProfileImageUrl(String userId)
    {
        return BackendServers.PROFILE + "/" + ServerParams.PATH_IMAGE + userId;

    }


    @Override
    protected void onHandleIntent(Intent intent) {
        Exception exc = null;
        Object result = null;

        Bundle extras = intent.getExtras();
        int reqCode = extras.getInt(EXTRA_REQUEST_CODE);

        if (intent.getAction().equals(ACTION_GET_PROFILE))
        {
            // Toast.makeText(getApplicationContext(), "request current logged user profile", Toast.LENGTH_SHORT).show();

            Session session = new SessionManager(this).getLastSession();

            ProfileRequester requester = new ProfileRequester(this, session, new TeamplaceBackend());
            try {
                result = requester.getUserProfile();
            }
            catch (Exception e)
            {
                Log.d(TAG, "Exception in export()", e);

                ExceptionManager em = ExceptionManager.getInstance();
                em.handleException(e);
            }
            finally
            {
                postEvent(reqCode, exc, result);
            }

            // sendLocalBroadcast(IntentFactory.ACTION_RESPONSE_PROFILE, extras);
        }
        else
        {
            throw new IllegalArgumentException("Request type not recognized");
        }
    }

    private void postEvent(int requestCode, Exception e, Object responseObj)
    {
        EventBus.getDefault().post(new ProfileServiceResponse(requestCode, e, responseObj));
    }

    protected void sendLocalBroadcast(String action, Bundle requestExtras)
    {
        Intent i = new Intent(action);
        i.putExtra(IntentFactory.RESPONSE_REQUEST_EXTRAS, requestExtras);

        IntentBus bus = new IntentBusImpl(this);
        bus.sendBroadcast(i);
    }

    public static class ProfileServiceResponse
    {
        public final static int NO_REQUEST_CODE = -1;

        private final Object responseData;
        private final Exception exception;

        /** Allows to identify who this response come from */
        private final int requestCode;

        public ProfileServiceResponse(int requestCode, Exception e, Object data)
        {
            this.requestCode = requestCode;
            this.responseData = data;
            this.exception = e;
        }

        public final long getRequestCode()
        {
            return this.requestCode;
        }

        public Object getResponseData() throws JsonProcessingException, CortadoException, SQLException,
                IOException
        {
            if (exception == null)
            {
                return responseData;
            }

            // Re-throw exception
            if (exception instanceof CortadoException)
            {
                throw (CortadoException) exception;
            }
            else if (exception instanceof SQLException)
            {
                throw (SQLException) exception;
            }
            else if (exception instanceof IOException)
            {
                throw (IOException) exception;
            }
            else if (exception instanceof ConnectFailedException)
            {
                throw (ConnectFailedException) exception;
            }
            else
            {
                throw new RuntimeException("Exception Type not supported: " + exception.getClass().getName(), exception);
            }
        }
    }
}
