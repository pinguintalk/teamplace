package net.teamplace.android.services;

/**
 * Created by nangu on 09.06.15.
 */
public class DownloadNotification extends TPBaseNotification
{
    private static final String TAG = "com.cortado.android.services.DownloadNotificationHelper";

    private static DownloadNotification instance;

    private DownloadNotification()
    {

    }

    public static synchronized DownloadNotification getInstance()
    {
        if (instance == null)
        {
            instance = new DownloadNotification();
        }
        return instance;
    }

}
