package net.teamplace.android.services;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.FileSystem;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.FileWasModifiedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.faulttolerant.NetworkOperationCallback;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.download.SyncFileRequester;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.GenericUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Created by nangu on 09.06.15.
 */
public class SyncFileService extends TPIntentService
{
    private static final String TAG = tag(SyncFileService.class);
    private static final String WORKER = "SyncFile service";


    public SyncFileService()
    {
        super(WORKER);
    }

    @Override
    public void onStart(Intent intent, int startId)
    {
        int requestCode = intent.getIntExtra(IntentFactory.REQUEST_CODE, -1);
         if (requestCode == IntentFactory.REQUEST_SYNC_FILE)
        {

            TimeSource timeSource = getDependencies().provideTimeSource();
            long created = timeSource.currentTimeMillis();
            synchronized (this)
            {
                Path srcPath = (Path) intent.getParcelableExtra(IntentFactory.KEY_PATH);
                Path destPath = (Path) intent.getParcelableExtra(IntentFactory.KEY_PATH_DEST);
                Path[] paths = new Path[]{srcPath};

                PathStateInfo[] pathStateInfos = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                        paths, FileJobInfo.STATE_QUEUED, TPBaseNotification.PROGRESS_PERCENT_START);
                FileJobInfo jobInfo = new FileJobInfo(created, requestCode, pathStateInfos, destPath);
                mJobQueue.put(created, jobInfo);
            }
            intent.putExtra(IntentFactory.KEY_JOB_TIMESTAMP, created);
        }
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();
        int type = extras.getInt(IntentFactory.REQUEST_CODE);

        if (type == IntentFactory.REQUEST_SYNC_FILE)
        {
            handleSyncFile(extras);
        }
        else
        {
            throw new IllegalArgumentException("Request type not recognized");
        }
    }


    private void handleSyncFile(Bundle extras) {

        long created = extras.getLong(IntentFactory.KEY_JOB_TIMESTAMP);

        mOngoingJobTimestamp.set(created);

        Path src = extras.getParcelable(IntentFactory.KEY_PATH);
        Path dest = extras.getParcelable(IntentFactory.KEY_PATH_DEST);
        String fileName = extras.getString(IntentFactory.KEY_FILE_NAME);

            String downloadFilePath = downloadFile(src,
                    (Path.Local) dest, fileName);

            EventBus bus = getDependencies().provideEventBus();

            Intent i = buildResponseIntent(
                    IntentFactory.ACTION_RESPONSE_SYNC_FILE, extras);
            sendLocalBroadcast(i);


    }

    private String downloadFilePath = null;


    private String downloadFile(final Path src, final Path.Local dest, String fileName)
    {
        final List<Pair<Path, Exception>> failed = Collections
                .synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(src);
        FileSystem fs = getDependencies().provideFileSystem();

        SyncFileRequester requester = new SyncFileRequester(getApplicationContext(), new SessionManager(
                getApplicationContext()).getLastSession(), new TeamplaceBackend(), new NetworkUpdateCallback()
        {

            @Override
            public void publishProgressInPercent(int percent)
            {

            }

            @Override
            public void onSuccess(String filePath)
            {
                downloadFilePath = filePath;

            }

            @Override
            public void onFailure(Exception e)
            {
                failed.add(new Pair<Path, Exception>(src, e));

            }
        },
                new NetworkOperationCallback()
                {
                    @Override
                    public boolean stopNetworkOperation()
                    {	// always called during download
                        if (mLastCanceled.get() > 0)
                        {
                            mLastCanceled.set(0);
                            return true;
                        }
                        return false;
                    }
                });
        try
        {
            requester.download(teamDriveId, PathUtils.remoteRequestString(src.getParentPath()), src.getName(),
                    dest.getAbsolutePathString(), fileName, null); // lastModifiedDate as file name !
        }
        catch (XCBStatusException | HttpResponseException | TeamDriveNotFoundException | ConnectFailedException
                | LogonFailedException | NotificationException | ServerLicenseException | FileWasModifiedException
                | IOException | RedirectException e1)
        {
            failed.add(new Pair<Path, Exception>(src, e1));

        }


        String externalStorageAppPath = GenericUtils.getExternalStorageAppPath().getAbsolutePathString();

        if (externalStorageAppPath.equals(dest.getAbsolutePathString()))
        {
            scanDownloadDir();
        }

        return downloadFilePath;
    }




}

