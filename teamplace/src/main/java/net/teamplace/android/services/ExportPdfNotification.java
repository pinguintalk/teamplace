package net.teamplace.android.services;

/**
 * Created by nangu on 09.06.15.
 */
public class ExportPdfNotification extends TPBaseNotification {
    private static final String TAG = "net.teamplace.android.services.ExportPdfNotification";

    private static ExportPdfNotification instance;

    private ExportPdfNotification()
    {

    }

    public static synchronized ExportPdfNotification getInstance()
    {
        if (instance == null)
        {
            instance = new ExportPdfNotification();
        }
        return instance;
    }

}