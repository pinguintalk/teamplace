package net.teamplace.android.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.sorting.SortManager;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.http.request.smartfiling.SmartFilingLocation;
import net.teamplace.android.utils.GenericUtils;

import java.util.ArrayList;
import java.util.Comparator;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Created by nangu on 09.06.15.
 */
public abstract class IntentFactory {

    private static final String PKG = pkg(IntentFactory.class);
    private static final String TAG = tag(IntentFactory.class);

    private final static Comparator<SmartFilingLocation> BEST_SMART_TARGET_FIRST_COMPARATOR = new Comparator<SmartFilingLocation>()
    {
        @Override
        public int compare(SmartFilingLocation lhs, SmartFilingLocation rhs)
        {
            boolean lorigin = lhs.isOrigin();
            boolean rorigin = rhs.isOrigin();
            if (lorigin && !rorigin)
            {
                return -1;
            }
            if (!lorigin && rorigin)
            {
                return +1;
            }

            long ldate = lhs.getAccessed();
            long rdate = rhs.getAccessed();
            if (ldate == rdate)
            {
                return 0;
            }

            return ldate > rdate ? -1 : +1;
        }
    };



    public static Intent buildBrowseRequestIntent(Context ctx, Path folder)
    {
        checkNonNullArgs(ctx, folder);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_BROWSE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, folder);
        return i;
    }

    public static Intent buildMkdirRequestIntent(Context ctx, Path folder)
    {
        checkNonNullArgs(ctx, folder);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_MKDIR);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, folder);
        return i;
    }

    public static Intent buildRenameRequestIntent(Context ctx, Path folder, String newName)
    {
        checkNonNullArgs(ctx, folder);
        checkCondition(!TextUtils.isEmpty(newName));

        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_RENAME);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, folder);
        i.putExtra(KEY_NEW_NAME, newName);
        return i;
    }

    public static Intent buildDeleteRequestIntent(Context ctx, Path[] paths)
    {
        checkNonNullArgs(ctx, paths);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_DELETE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATHS, paths);
        return i;
    }

    /** dest is the destination directory without the file name */
    public static Intent buildUploadRequestIntent(Context ctx, Path[] sources, Path dest, boolean isMove)
    {
        checkNonNullArgs(ctx, sources, dest);
        Intent i = new Intent(ctx, UploadService.class);
        if (isMove)
            i.putExtra(REQUEST_CODE, REQUEST_MOVE_LOCAL_TO_REMOTE);
        else
            i.putExtra(REQUEST_CODE, REQUEST_UPLOAD);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATHS, sources);
        i.putExtra(KEY_PATH_DEST, dest);
        return i;
    }

    /** dest is the destination directory without the file name */
    public static Intent buildDownloadRequestIntent(Context ctx, Path[] sources, Path dest,boolean isMove)
    {
        checkNonNullArgs(ctx, sources, dest);
        Intent i = new Intent(ctx, DownloadService.class);
        if (isMove)
            i.putExtra(REQUEST_CODE, REQUEST_MOVE_REMOTE_TO_LOCAL);
        else
            i.putExtra(REQUEST_CODE, REQUEST_DOWNLOAD);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATHS, sources);
        i.putExtra(KEY_PATH_DEST, dest);
        return i;
    }

    public static Intent buildGetSyncFileIntent(Context ctx, Path source, String fileName) {
        Path.Local destPath = GenericUtils.getExternalStorageThumbnailPath();

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(source);

        Intent i = new Intent(ctx, SyncFileService.class);
        i.putExtra(REQUEST_CODE, REQUEST_SYNC_FILE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, source);
        i.putExtra(KEY_PATH_DEST, destPath);
        i.putExtra(KEY_FILE_NAME, fileName);
        i.putExtra(KEY_TEAMDRIVE_ID, teamDriveId);
        return i;
    }


    // move files locally or remotely only
    public static Intent buildMoveRequestIntent(Context ctx, Path[] sources, Path dest)
    {
        checkNonNullArgs(ctx, sources, dest);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_MOVE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATHS, sources);
        i.putExtra(KEY_PATH_DEST, dest);
        return i;
    }

    public static Intent buildExportToPdfIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, ExportPdfService.class);
        i.putExtra(REQUEST_CODE, REQUEST_EXPORT_TO_PDF);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    public static Intent buildExportToZipIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, ExportPdfService.class);
        i.putExtra(REQUEST_CODE, REQUEST_EXPORT_TO_ZIP);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    public static Intent buildCheckUploadLocationsRequestIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_CHECK_UPLOAD_LOCATIONS);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    // TODO Unused - check if this can be removed
    public static Intent buildUploadPictureRequestIntent(Context ctx, ArrayList<String> imagePathList, Path dest)
    {
        checkNonNullArgs(ctx, imagePathList, dest);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_UPLOAD_PICTURE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH_DEST, dest);
        i.putExtra(KEY_PATHS, imagePathList);
        return i;
    }

    public static Intent buildAddFavoriteRequestIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_ADD_FAVORITE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    public static Intent buildDeleteFavoriteRequestIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_DELETE_FAVORITE);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    public static Intent buildEnumerateFavoriteSRequestIntent(Context ctx)
    {
        checkNonNullArg(ctx);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_ENUMERATE_FAVORITES);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        return i;
    }

    public static Intent buildSetSortRequestIntent(Context ctx, Path path, int category, int order)
    {
        checkNonNullArg(ctx);
        SortManager.checkCategory(category);
        SortManager.checkOrder(order);

        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_SET_SORT);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        i.putExtra(KEY_SORT_CATEGORY, category);
        i.putExtra(KEY_SORT_ORDER, order);
        return i;
    }

    public static Intent buildGetSortRequestIntent(Context ctx, Path path)
    {
        checkNonNullArgs(ctx, path);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_GET_SORT);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, path);
        return i;
    }

    public static Intent buildCancelUpload(Context ctx)
    {
        checkNonNullArgs(ctx);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_CANCEL_MOVE_COPY);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        return i;
    }

    public static Intent buildCancelDownload(Context ctx)
    {
        checkNonNullArgs(ctx);
        Intent i = new Intent(ctx, DownloadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_CANCEL_MOVE_COPY);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        return i;
    }

    public static Intent buildCancelCopyMoveIntent(Context ctx)
    {
        checkNonNullArgs(ctx);
        Intent i = new Intent(ctx, BrowsingService.class);
        i.putExtra(Contract.REQUEST_CODE, REQUEST_CANCEL_MOVE_COPY);
        i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
        return i;
    }

    public static Intent buildExtractVersionRequestIntent(Context ctx, Path versionPath, long versionDate)
    {
        checkNonNullArgs(ctx, versionPath);
        Intent i = new Intent(ctx, UploadService.class);
        i.putExtra(REQUEST_CODE, REQUEST_EXTRACT_VERSION);
        i.putExtra(REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATH, versionPath);
        i.putExtra(KEY_DATE, versionDate);
        return i;
    }

    public static Intent buildExportToPdfIntent(Context ctx, Path[] sources)
    {
        // dest and scr path is the same 

        checkNonNullArgs(ctx, sources, sources[0]);
        Intent i = new Intent(ctx, ExportPdfService.class);
        i.putExtra(Contract.REQUEST_CODE, REQUEST_EXPORT_TO_PDF);
        i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
        i.putExtra(KEY_PATHS, sources);
        return i;
    }

    public static Intent buildProfileRequestIntent(Context ctx, int requestId)
    {
        Intent i = new Intent(ctx, ProfileService.class);
        i.putExtra(Contract.REQUEST_CODE, REQUEST_PROFILE);
        i.putExtra(Contract.REQUEST_ID, requestId);
        return i;
    }


    public static int getDetailCopyRequestType(Path[] sources, Path dest) {

        if (sources == null || sources.length == 0)
        {
            return -1;
        }

        PathUtils.checkSameLocationOrThrow(sources);

        boolean toLocal = dest instanceof Path.Local;
        boolean fromLocal = sources[0] instanceof Path.Local;

        if (!fromLocal && !toLocal)
        {
            return REQUEST_COPY_REMOTE;
        }
        else if (fromLocal && toLocal)
        {
            return REQUEST_COPY_LOCAL;
        }
        else if (fromLocal && !toLocal)
        {
            return Contract.REQUEST_UPLOAD;
        }
        else if (!fromLocal && toLocal)
        {
            return REQUEST_DOWNLOAD;
        }
        else
            return -1;

    }

    public static int getDetailMoveRequestType(Path[] sources, Path dest) {

        if (sources == null || sources.length == 0)
        {
            return -1;
        }

        PathUtils.checkSameLocationOrThrow(sources);

        boolean toLocal = dest instanceof Path.Local;
        boolean fromLocal = sources[0] instanceof Path.Local;

        if (!fromLocal && !toLocal)
        {
            return IntentFactory.REQUEST_MOVE_REMOTE;
        }
        else if (fromLocal && toLocal)
        {
            return IntentFactory.REQUEST_MOVE_LOCAL;
        }
        else if (fromLocal && !toLocal)
        {
            return IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE;
        }
        else if (!fromLocal && toLocal)
        {
            return IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL;
        }
        else
            return -1;

    }

    /** Extracts the request id from a request or response given in form of an Intent */
    public static int extractRequestId(Intent intent)
    {
        if (intent == null)
        {
            return NO_ID;
        }

        int id = intent.getIntExtra(REQUEST_ID, NO_ID);
        if (id != NO_ID)
        {
            return id;
        }

        // This is a response, try to extract the data from RESPONSE_REQUEST_EXTRAS
        Bundle extras = intent.getBundleExtra(RESPONSE_REQUEST_EXTRAS);
        if (extras == null)
        {
            return NO_ID;
        }

        return extras.getInt(REQUEST_ID, NO_ID);
    }

    /** Extracts the parameter newName from a rename request or a response to such a request */
    public static String extractNewName(Intent intent)
    {
        if (intent == null)
        {
            return null;
        }

        String newName = intent.getStringExtra(KEY_NEW_NAME);
        if (newName != null)
        {
            return newName;
        }

        Bundle extras = intent.getBundleExtra(RESPONSE_REQUEST_EXTRAS);
        if (extras == null)
        {
            return null;
        }

        return extras.getString(KEY_NEW_NAME);
    }

    public static final String REQUEST_CODE = PKG + ".requestCode";
    public static final String REQUEST_ID = PKG + "." + "id";
    public static final int NO_ID = IdGenerator.NO_ID;

    public static final int REQUEST_BROWSE = 100;
    public static final int REQUEST_MKDIR = 101;
    public static final int REQUEST_DELETE = 102;
    public static final int REQUEST_COPY = 103;
    public static final int REQUEST_MOVE = 104;
    public static final int REQUEST_CHECK_UPLOAD_LOCATIONS = 105;
    public static final int REQUEST_RENAME = 106;
    public static final int REQUEST_EXPORT_TO_PDF = 107;
    public static final int REQUEST_EXPORT_TO_ZIP = 108;
    public static final int REQUEST_UPLOAD_PICTURE = 109; // TODO Unused - check if this can be removed
    public static final int REQUEST_ADD_FAVORITE = 110;
    public static final int REQUEST_DELETE_FAVORITE = 111;
    public static final int REQUEST_ENUMERATE_FAVORITES = 112;
    public static final int REQUEST_SET_SORT = 113;
    public static final int REQUEST_GET_SORT = 114;
    public static final int REQUEST_CANCEL_MOVE_COPY = 115;
    public static final int REQUEST_EXTRACT_VERSION = 116;
    public static final int REQUEST_UPLOAD = 117;
    public static final int REQUEST_DOWNLOAD = 118;
    public static final int REQUEST_COPY_REMOTE = 119;
    public static final int REQUEST_COPY_LOCAL = 120;
    public static final int REQUEST_MOVE_REMOTE_TO_LOCAL = 121;
    public static final int REQUEST_MOVE_LOCAL_TO_REMOTE = 122;
    public static final int REQUEST_MOVE_REMOTE = 125;
    public static final int REQUEST_MOVE_LOCAL = 126;
    public static final int REQUEST_SYNC_FILE = 127;
    public static final int REQUEST_PROFILE = 128;


    public static final String ACTION_RESPONSE_BROWSE = PKG + ".ACTION_RESPONSE_BROWSE";
    public static final String ACTION_RESPONSE_MKDIR = PKG + ".ACTION_RESPONSE_MKDIR";
    public static final String ACTION_RESPONSE_DELETE = PKG + ".ACTION_RESPONSE_DELETE";
    public static final String ACTION_RESPONSE_COPY = PKG + ".ACTION_RESPONSE_COPY";
    public static final String ACTION_RESPONSE_MOVE = PKG + ".ACTION_RESPONSE_MOVE";
    public static final String ACTION_RESPONSE_MOVE_LOCAL_TO_REMOTE = PKG + ".ACTION_RESPONSE_MOVE_LOCAL_TO_REMOTE";
    public static final String ACTION_RESPONSE_MOVE_REMOTE_TO_LOCAL = PKG + ".ACTION_RESPONSE_MOVE_REMOTE_TO_LOCAL";
    public static final String ACTION_RESPONSE_UPLOAD = PKG + ".ACTION_RESPONSE_UPLOAD";
    public static final String ACTION_RESPONSE_DOWNLOAD= PKG + ".ACTION_RESPONSE_DOWNLOAD";

    public static final String ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS = PKG
            + ".ACTION_RESPONSE_CHECK_UPLOAD_LOCATION";
    public static final String ACTION_RESPONSE_UPLOAD_PICTURE = PKG + ".ACTION_RESPONSE_UPLOAD_PICTURE";
    public static final String ACTION_RESPONSE_RENAME = PKG + ".ACTION_RESPONSE_RENAME";
    public static final String ACTION_RESPONSE_EXPORT_TO_PDF = PKG + ".ACTION_RESPONSE_EXPORT_TO_PDF";
    public static final String ACTION_RESPONSE_EXPORT_TO_ZIP = PKG + ".ACTION_RESPONSE_EXPORT_TO_ZIP";
    public static final String ACTION_RESPONSE_ADD_FAVORITE = PKG + ".ACTION_RESPONSE_ADD_FAVORITE";
    public static final String ACTION_RESPONSE_DELETE_FAVORITE = PKG + ".ACTION_RESPONSE_DELETE_FAVORITE";
    public static final String ACTION_RESPONSE_ENUMERATE_FAVORITES = PKG + ".ACTION_RESPONSE_ENUMERATE_FAVORITES";
    public static final String ACTION_RESPONSE_SET_SORT = PKG + ".ACTION_RESPONSE_SET_SORT";
    public static final String ACTION_RESPONSE_GET_SORT = PKG + ".ACTION_RESPONSE_GET_SORT";
    public static final String ACTION_RESPONSE_EXTRACT_VERSION = PKG + ".ACTION_RESPONSE_EXTRACT_VERSION";
    public static final String ACTION_RESPONSE_SYNC_FILE = PKG + ".SYNC_FILE";

    public static final String RESPONSE_REQUEST_EXTRAS = PKG + ".requestExtras";
    public static final String RESPONSE_BROWSE_FOLDER_CONTENT = PKG + ".folderContent";
    public static final String RESPONSE_FAVORITES = PKG + ".favorites";
    public static final String RESPONSE_SORT_CATEGORY = PKG + ".category";
    public static final String RESPONSE_SORT_ORDER = PKG + ".order";

    /**
     * Targets are sorted, 'best first'. A target T is better than a target S iff T was downloaded on this device
     * and S wasn't, or both T and S were downloaded on this device but T was downloaded after S, or both T and S
     * were not downloaded on this device but T was downloaded (on another device) after S.
     */
    public static final String RESPONSE_CHECK_UPLOAD_LOCATIONS_TARGETS = PKG + ".mTargets";

    public static final String WORKER = "worker";
    public static final String KEY_PATH = PKG + ".path";
    public static final String KEY_PATHS = PKG + ".paths";
    public static final String KEY_PATH_DEST = PKG + ".pathDest";
    public static final String KEY_NEW_NAME = PKG + ".newName";
    public static final String KEY_SORT_CATEGORY = PKG + ".category";
    public static final String KEY_SORT_ORDER = PKG + ".order";
    public static final String KEY_DATE = PKG + ".date";
    public static final String KEY_JOB_TIMESTAMP = PKG + ".jobId";
    public static final String KEY_TEAMDRIVE_ID = PKG + ".teamdriveid";
    public static final String KEY_LAST_MODIFIED_DATE = PKG + ".last_modified";
    public static final String KEY_FILE_NAME = PKG + ".file_name";
    public static final Path.Local.FolderFirstComparator LOCAL_PATH_FOLDER_FIRST_COMPARATOR = new Path.Local.FolderFirstComparator();



}