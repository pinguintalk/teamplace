package net.teamplace.android.services;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Pair;
import android.widget.Toast;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.services.UploadNotification;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.upload.FaultTolerantUploadManager;
import net.teamplace.android.http.request.files.ExportRequester;
import net.teamplace.android.http.request.smartfiling.SmartFilingLocation;
import net.teamplace.android.http.request.smartfiling.SmartFilingRequester;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;

/**
 * Created by nangu on 09.06.15.
 */
public class UploadService extends TPIntentService
{
    private static final String TAG = tag(UploadService.class);
    private static final int ID_PROGRESS = IdGenerator.getId();


    private final static Comparator<SmartFilingLocation> BEST_SMART_TARGET_FIRST_COMPARATOR = new Comparator<SmartFilingLocation>()
    {
        @Override
        public int compare(SmartFilingLocation lhs, SmartFilingLocation rhs)
        {
            boolean lorigin = lhs.isOrigin();
            boolean rorigin = rhs.isOrigin();
            if (lorigin && !rorigin)
            {
                return -1;
            }
            if (!lorigin && rorigin)
            {
                return +1;
            }

            long ldate = lhs.getAccessed();
            long rdate = rhs.getAccessed();
            if (ldate == rdate)
            {
                return 0;
            }

            return ldate > rdate ? -1 : +1;
        }
    };




    public UploadService()
    {
        super(WORKER);
    }

    @Override
    public void onStart(Intent intent, int startId)
    {
        int requestCode = intent.getIntExtra(IntentFactory.REQUEST_CODE, -1);
        if (requestCode == IntentFactory.REQUEST_CANCEL_MOVE_COPY)
        {
            Toast.makeText(getApplicationContext(), "Upload canceled", Toast.LENGTH_SHORT).show();

            synchronized (this)
            {
                mLastCanceled.set(System.currentTimeMillis());
                mCurrCanceledJob.set(mOngoingJobTimestamp.get());
            }

            return;
        }
        else if (requestCode == IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE
                || requestCode == IntentFactory.REQUEST_UPLOAD)
        {
            // mCancelCopyOrMoveRequested.set(false);
            Parcelable[] srcPaths = intent.getParcelableArrayExtra(IntentFactory.KEY_PATHS);
            TimeSource timeSource = getDependencies().provideTimeSource();
            long created = timeSource.currentTimeMillis();
            synchronized (this)
            {
                Path destPath = (Path) intent.getParcelableExtra(IntentFactory.KEY_PATH_DEST);
                Path[] paths = Arrays.copyOf(srcPaths, srcPaths.length, Path[].class);
                PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                        paths, FileJobInfo.STATE_QUEUED, TPBaseNotification.PROGRESS_PERCENT_START);
                FileJobInfo jobInfo = new FileJobInfo(created, requestCode, pathStateInfo, destPath);
                mJobQueue.put(created, jobInfo);
            }
            intent.putExtra(IntentFactory.KEY_JOB_TIMESTAMP, created);
        }
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();
        int type = extras.getInt(IntentFactory.REQUEST_CODE);

        if (type == IntentFactory.REQUEST_UPLOAD)
        {
            handleUpload(extras,false);
        }
        else if (type == IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE)
        {
            handleUpload(extras,true);
        }
        else if (type == IntentFactory.REQUEST_CHECK_UPLOAD_LOCATIONS)
        {
            handleCheckUploadLocations(extras);
        }

        else
        {
            throw new IllegalArgumentException("Request type not recognized");
        }
    }





    private void handleExportToPdf(Bundle extras)
    {
        Path path = extras.getParcelable(IntentFactory.KEY_PATH);
        export(path, path.getName() + "." + ExportRequester.VALUE_EXPORT_FORMAT_PDF,
                ExportRequester.VALUE_EXPORT_FORMAT_PDF);
        Intent i = buildResponseIntent(IntentFactory.ACTION_RESPONSE_EXPORT_TO_PDF, extras);
        sendLocalBroadcast(i);
    }


    private void handleCheckUploadLocations(Bundle extras)
    {
        // TODO: @giveg check this to-do, it might be obsolete. At the moment, the requester only works on the
        // workplace server
        ArrayList<Path> targets = new ArrayList<Path>();
        try
        {
            Path path = extras.getParcelable(IntentFactory.KEY_PATH);
            SmartFilingRequester requester = getDependencies().provideSmartFilingRequester(this);
            List<SmartFilingLocation> locations = requester.requestSmartFilingLocations(path.getName());
            Collections.sort(locations, BEST_SMART_TARGET_FIRST_COMPARATOR);

            for (SmartFilingLocation location : GenericUtils.emptyIfNull(locations))
            {
                String teamDriveId = location.getTeamDriveId();
                String pathLocation = PathUtils.pathLocationFromTeamDriveId(teamDriveId);
                targets.add(PathUtils.buildPathfromServerResponseString(pathLocation, location.getPath()));
            }
        }
        catch (Exception e)
        {
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
        }

        Intent i = buildResponseIntent(IntentFactory.ACTION_RESPONSE_CHECK_UPLOAD_LOCATIONS, extras);
        i.putParcelableArrayListExtra(IntentFactory.RESPONSE_CHECK_UPLOAD_LOCATIONS_TARGETS, targets);
        sendLocalBroadcast(i);
    }




    private void handleUpload(Bundle extras,boolean isMove)
    {
        long created = extras.getLong(IntentFactory.KEY_JOB_TIMESTAMP);
        if (mLastCanceled.get() < created)
        {
            mOngoingJobTimestamp.set(created);
            Parcelable[] parcelables = extras.getParcelableArray(IntentFactory.KEY_PATHS);
            // TODO: check this cast
            Path[] sources = Arrays.copyOf(parcelables, parcelables.length, Path[].class);
            if (sources == null || sources.length == 0)
            {
                return;
            }

            PathUtils.checkSameLocationOrThrow(sources);

            Path dest = extras.getParcelable(IntentFactory.KEY_PATH_DEST);

            Path.Local[] lsources = Arrays.copyOf(sources, sources.length, Path.Local[].class);

            for (final Path.Local src : lsources)
            {
                upload(src, dest, isMove);
                Intent i = buildResponseIntent(isMove
                        ? IntentFactory.ACTION_RESPONSE_MOVE_LOCAL_TO_REMOTE
                        : IntentFactory.ACTION_RESPONSE_UPLOAD, extras);
                sendLocalBroadcast(i);
            }

        }

    }



    private void upload(final Path.Local src, Path dest,boolean isMove)
    {
        final List<Pair<Path, Exception>> failed = Collections
                .synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(dest);

        final CountDownLatch done = new CountDownLatch(1);

        synchronized (this)
        {
            upgradeProgressNotificationStart(src);
        }
        final AnythingHolder<FaultTolerantUploadManager> mgrRef = new AnythingHolder<FaultTolerantUploadManager>();
        FaultTolerantUploadManager mgr = new FaultTolerantUploadManager(getApplicationContext(),
                new NetworkUpdateCallback()
                {
                    @Override
                    public void publishProgressInPercent(int percent)
                    {
                        if (mLastCanceled.get() > 0)
                        {
                            if (mgrRef.obj != null)
                            {
                                mgrRef.obj.stopUpload();
                                // after stop downloading, onFailure will called
                            }
                            // set canceled timer back 
                            mLastCanceled.set(0);
                            done.countDown();
                        }
                        else
                        {
                            synchronized (UploadService.this)
                            {
                                updateProgressNotification(percent, src);
                            }
                        }
                    }

                    @Override
                    public void onSuccess(String filePath)
                    {
                        synchronized (UploadService.this)
                        {

                            updateProgressNotificationDone(src);

                        }
                        done.countDown();
                    }

                    @Override
                    public void onFailure(Exception e)
                    {

                        if (isCurrentJobCanceled()) {
                            synchronized (this)
                            {
                                upgradeProgressNotificationCancel(src);
                            }
                        }
                        else
                        {
                            failed.add(new Pair<Path, Exception>(src, e));

                            // we got an error !
                            synchronized (this)
                            {
                                updateProgressNotificationError(src,e);
                            }
                        }
                        done.countDown();
                    }
                }, new TeamplaceBackend() );
        mgrRef.obj = mgr;
        Path.Local parent = (Path.Local) src.getParentPath();
        mgr.uploadFile(teamDriveId, parent.getAbsolutePathString(), src.getName(),
                PathUtils.remoteRequestString(dest), src.getName(), teamDriveId == null ? FORCE_NEW_NAME
                        : FORCE_NEW_VERSION);

        try
        {
            done.await();
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            Log.i(TAG, "Current thread has been interrupted during an upload", e);
            return;
        }

        if (failed.isEmpty())
        {
            if (isMove)
            {
                deleteLocal(src);
            }
        }

    }

    private void uploads(Path.Local[] sources, Path dest)
    {
        final List<Pair<Path, Exception>> failed = Collections
                .synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(dest);
        for (final Path.Local src : sources)
        {
            final CountDownLatch done = new CountDownLatch(1);

            synchronized (this)
            {
                upgradeProgressNotificationStart(src);
            }
            final AnythingHolder<FaultTolerantUploadManager> mgrRef = new AnythingHolder<FaultTolerantUploadManager>();
            FaultTolerantUploadManager mgr = new FaultTolerantUploadManager(getApplicationContext(),
                    new NetworkUpdateCallback()
                    {
                        @Override
                        public void publishProgressInPercent(int percent)
                        {
                            if (mLastCanceled.get() > 0)
                            {
                                if (mgrRef.obj != null)
                                {
                                    mgrRef.obj.stopUpload();
                                    // after stop downloading, onFailure will called
                                }
                                mLastCanceled.set(0);
                                done.countDown();
                            }
                            else
                            {
                                synchronized (UploadService.this)
                                {
                                    updateProgressNotification(percent, src);
                                }
                            }
                        }

                        @Override
                        public void onSuccess(String filePath)
                        {
                            synchronized (UploadService.this)
                            {

                                updateProgressNotificationDone(src);

                            }
                            done.countDown();
                        }

                        @Override
                        public void onFailure(Exception e)
                        {
                            failed.add(new Pair<Path, Exception>(src, e));
                            if (isCurrentJobCanceled()) {
                                synchronized (UploadService.this)
                                {
                                    upgradeProgressNotificationCancel(src);
                                }
                            }
                            else
                            {
                                // we got an error !
                                synchronized (UploadService.this)
                                {
                                    updateProgressNotificationError(src,e);
                                }
                            }
                            done.countDown();
                        }
                    }, new TeamplaceBackend());
            mgrRef.obj = mgr;
            Path.Local parent = (Path.Local) src.getParentPath();
            mgr.uploadFile(teamDriveId, parent.getAbsolutePathString(), src.getName(),
                    PathUtils.remoteRequestString(dest), src.getName(), teamDriveId == null ? FORCE_NEW_NAME
                            : FORCE_NEW_VERSION);

            try
            {
                done.await();
            }
            catch (InterruptedException e)
            {
                Thread.currentThread().interrupt();
                Log.i(TAG, "Current thread has been interrupted during an upload", e);
                return;
            }
        }

    }





    private void export(Path path, String exportedName, String exportFormat)
    {
        try
        {
            Path dest = Path.valueOf(path.getParentPath(), path.getName() + "." + exportFormat);
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
            String srcParent = PathUtils.remoteRequestString(path.getParentPath());
            String destPath = PathUtils.remoteRequestString(dest);
            ExportRequester requester = getDependencies().provideExportRequester(this);
            requester.export(teamDriveId, srcParent, path.getName(), destPath, exportFormat, FORCE_NEW_NAME);
        }
        catch (Exception e)
        {
            Log.d(TAG, "Exception in export()", e);
            int requestType = exportFormatToRequestType(exportFormat);
            postErrorFeedback(path, requestType, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
        }
    }


    private void updateProgressNotification(int progressPercent, Path... sources)
    {
        if (updateFileJobInfoArray(mOngoingJobTimestamp.get(), progressPercent, sources))
        {
            UploadNotification nh = getDependencies().provideUploadNotificationHelper();
            nh.updateProgressNotification(getApplicationContext(),
                    Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                    ID_PROGRESS, true);
        }
    }

    private void updateCancelNotification(Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, TPBaseNotification.PROGRESS_CANCEL, sources);
        UploadNotification nh = getDependencies().provideUploadNotificationHelper();
        nh.updateCancelNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(ongoingJobTimestamp);
    }

    private void updateDoneNotification(Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, TPBaseNotification.PROGRESS_PERCENT_DONE, sources);
        UploadNotification nh = getDependencies().provideUploadNotificationHelper();
        nh.updateDoneNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(ongoingJobTimestamp);
    }

    private void updateErrorNotification(List<Pair<Path, Exception>> failed)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(mOngoingJobTimestamp.get(), failed);
        UploadNotification nh = getDependencies().provideUploadNotificationHelper();
        nh.updateErrorNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(),
                        FileJobInfo[].class), ongoingJobTimestamp);
        deleteJobIfFinished(mOngoingJobTimestamp.get());
    }

    private void updateErrorNotification(Exception exception, Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(mOngoingJobTimestamp.get(), exception, sources);
        UploadNotification nh = getDependencies().provideUploadNotificationHelper();
        nh.updateErrorNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(mOngoingJobTimestamp.get());
    }



    private void updateProgressNotificationError(Local src, Exception e)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_ERROR, src);
        updateErrorNotification(e, src);
    }

    private void updateProgressNotificationDone(Local src)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_DONE, src);
        updateDoneNotification(src);
    }

    private void upgradeProgressNotificationStart(Local src)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_START, src);
    }

    private void upgradeProgressNotificationCancel(Local src)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_CANCEL, src);
        updateCancelNotification(src);
    }


    private int exportFormatToRequestType(String exportFormat)
    {
        switch (exportFormat)
        {
            case ExportRequester.FORMAT_PDF:
                return IntentFactory.REQUEST_EXPORT_TO_PDF;
            case ExportRequester.FORMAT_ZIP:
                return IntentFactory.REQUEST_EXPORT_TO_ZIP;
            default:
                return 0; // TODO: @jobol why zero?
        }
    }



    private boolean isCurrentJobCanceled()
    {
        return (mCurrCanceledJob.get() == mOngoingJobTimestamp.get());
    }


}

