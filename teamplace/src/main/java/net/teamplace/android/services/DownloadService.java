package net.teamplace.android.services;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Pair;
import android.widget.Toast;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.openin.DownloadFileDialogFragment;
import net.teamplace.android.browsing.path.FileSystem;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.services.DownloadNotification;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.FileWasModifiedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.faulttolerant.NetworkOperationCallback;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.download.DownloadRequester;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.GenericUtils;

import de.greenrobot.event.EventBus;
/**
 * Created by nangu on 09.06.15.
 */
public class DownloadService extends TPIntentService
{
    private static final String TAG = tag(DownloadService.class);
    private static final String DOWNLOAD_WORKER = "download worker";
    private static final int ID_PROGRESS = IdGenerator.getId();


    public DownloadService()
    {
        super(DOWNLOAD_WORKER);
    }

    @Override
    public void onStart(Intent intent, int startId)
    {
        int requestCode = intent.getIntExtra(IntentFactory.REQUEST_CODE, -1);
        if (requestCode == IntentFactory.REQUEST_CANCEL_MOVE_COPY)
        {
            Toast.makeText(getApplicationContext(), "Download canceled", Toast.LENGTH_SHORT).show();

            synchronized (this)
            {
                mLastCanceled.set(System.currentTimeMillis());
                mCurrCanceledJob.set(mOngoingJobTimestamp.get());
            }

            return;
        }
        else if (requestCode == IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL
                || requestCode == IntentFactory.REQUEST_DOWNLOAD)
        {
            // mCancelCopyOrMoveRequested.set(false);
            Parcelable[] srcPaths = intent.getParcelableArrayExtra(IntentFactory.KEY_PATHS);
            TimeSource timeSource = getDependencies().provideTimeSource();
            long created = timeSource.currentTimeMillis();
            synchronized (this)
            {
                Path destPath = (Path) intent.getParcelableExtra(IntentFactory.KEY_PATH_DEST);
                Path[] paths = Arrays.copyOf(srcPaths, srcPaths.length, Path[].class);
                PathStateInfo[] pathStateInfos = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                        paths, FileJobInfo.STATE_QUEUED, TPBaseNotification.PROGRESS_PERCENT_START);
                FileJobInfo jobInfo = new FileJobInfo(created, requestCode, pathStateInfos, destPath);
                mJobQueue.put(created, jobInfo);
            }
            intent.putExtra(IntentFactory.KEY_JOB_TIMESTAMP, created);
        }
        super.onStart(intent, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();
        int type = extras.getInt(IntentFactory.REQUEST_CODE);

        if (type == IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL)
        {
            handleDownload(extras, true);
        }
        else if (type == IntentFactory.REQUEST_DOWNLOAD)
        {
            handleDownload(extras, false);
        }
        else
        {
            throw new IllegalArgumentException("Request type not recognized");
        }
    }


    private void handleDownload(Bundle extras,boolean isMove) {

        long created = extras.getLong(IntentFactory.KEY_JOB_TIMESTAMP);

        mOngoingJobTimestamp.set(created);
        Parcelable[] parcelables = extras
                .getParcelableArray(IntentFactory.KEY_PATHS);
        // TODO: check this cast
        Path[] sources = Arrays.copyOf(parcelables, parcelables.length,
                Path[].class);
        if (sources == null || sources.length == 0) {
            return;
        }

        PathUtils.checkSameLocationOrThrow(sources);

        Path dest = extras.getParcelable(IntentFactory.KEY_PATH_DEST);

        for (final Path src : sources) {
            String downloadFilePath = copyOrMoveRemoteLocal(src,
                    (Path.Local) dest, isMove);

            EventBus bus = getDependencies().provideEventBus();

            if (sources.length == 1
                    && !(sources[0] instanceof Path.Local)
                    && bus.hasSubscriberForEvent(DownloadFileDialogFragment.BlockingDownloadRequest.class)) {
                DownloadFileDialogFragment.BlockingDownloadRequest downloadRequest = new DownloadFileDialogFragment.BlockingDownloadRequest(
                        sources[0], downloadFilePath);
                bus.post(downloadRequest);
            }
            Intent i = buildResponseIntent(isMove == true ?
                    IntentFactory.ACTION_RESPONSE_MOVE_REMOTE_TO_LOCAL:
                    IntentFactory.ACTION_RESPONSE_DOWNLOAD, extras);
            sendLocalBroadcast(i);
        }

    }

    private String downloadFilePath = null;


    private String copyOrMoveRemoteLocal(final Path src, final Path.Local dest, boolean isMove)
    {
        final List<Pair<Path, Exception>> failed = Collections
                .synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(src);
        FileSystem fs = getDependencies().provideFileSystem();

        synchronized (this)
        {
            upgradeProgressNotificationStart(src);
        }

        DownloadRequester requester = new DownloadRequester(getApplicationContext(), new SessionManager(
                getApplicationContext()).getLastSession(), new TeamplaceBackend(), new NetworkUpdateCallback()
        {

            @Override
            public void publishProgressInPercent(int percent)
            {
                synchronized (DownloadService.this)
                {
                    updateProgressNotification(percent, src);
                }
                EventBus bus = getDependencies().provideEventBus();
                if (bus.hasSubscriberForEvent(DownloadFileDialogFragment.ProgressEvent.class))
                {
                    if (src instanceof Path.Remote)
                    {
                        bus.post(new DownloadFileDialogFragment.ProgressEvent(src, percent));
                    }
                    else if (src instanceof Path.TeamDrive)
                    {
                        bus.post(new DownloadFileDialogFragment.ProgressEvent(src, percent));
                    }
                }
            }

            @Override
            public void onSuccess(String filePath)
            {
                downloadFilePath = filePath;
                synchronized (DownloadService.this)
                {
                    updateProgressNotificationDone(src);
                }

            }

            @Override
            public void onFailure(Exception e)
            {
                failed.add(new Pair<Path, Exception>(src, e));
                synchronized (DownloadService.this)
                {
                    updateProgressNotificationError(src, e);
                }
            }
        },
                new NetworkOperationCallback()
                {
                    @Override
                    public boolean stopNetworkOperation()
                    {	// always called during download
                        if (mLastCanceled.get() > 0)
                        {
                            synchronized (DownloadService.this)
                            {
                                upgradeProgressNotificationCancel(src);
                            }
                            mLastCanceled.set(0);
                            return true;
                        }
                        return false;
                    }
                });
        try
        {
            requester.download(teamDriveId, PathUtils.remoteRequestString(src.getParentPath()), src.getName(),
                    dest.getAbsolutePathString(), GenericUtils.createCopyFileName(fs, src.getName(), dest), null);
        }
        catch (XCBStatusException | HttpResponseException | TeamDriveNotFoundException | ConnectFailedException
                | LogonFailedException | NotificationException | ServerLicenseException | FileWasModifiedException
                | IOException | RedirectException e1)
        {
            failed.add(new Pair<Path, Exception>(src, e1));
            synchronized (DownloadService.this)
            {
                updateProgressNotificationError(src, e1);
            }
        }


        String externalStorageAppPath = GenericUtils.getExternalStorageAppPath().getAbsolutePathString();

        if (externalStorageAppPath.equals(dest.getAbsolutePathString()))
        {
            scanDownloadDir();
        }

        if (failed.isEmpty())
        {
            if (isMove)
            {
                // Does not delete check this on errors!
                deleteRemote(src);
            }
        }

        return downloadFilePath;
    }

    private String copyOrMoveRemoteLocal(final Path[] sources, final Path.Local dest, boolean delSrc)
    {
        final List<Pair<Path, Exception>> failed = Collections
                .synchronizedList(new ArrayList<Pair<Path, Exception>>(0));

        String teamDriveId = PathUtils.getTeamDriveIdOrNull(sources[0]);
        FileSystem fs = getDependencies().provideFileSystem();

        for (final Path src : sources)
        {

            synchronized (this)
            {
                upgradeProgressNotificationStart(src);
            }

            DownloadRequester requester = new DownloadRequester(getApplicationContext(), new SessionManager(
                    getApplicationContext()).getLastSession(), new TeamplaceBackend(), new NetworkUpdateCallback()
            {

                @Override
                public void publishProgressInPercent(int percent)
                {
                    synchronized (DownloadService.this)
                    {
                        updateProgressNotification(percent, src);
                    }
                    EventBus bus = getDependencies().provideEventBus();
                    if (bus.hasSubscriberForEvent(DownloadFileDialogFragment.ProgressEvent.class))
                    {
                        if (sources[0] instanceof Path.Remote)
                        {
                            bus.post(new DownloadFileDialogFragment.ProgressEvent(sources[0], percent));
                        }
                        else if (sources[0] instanceof Path.TeamDrive)
                        {
                            bus.post(new DownloadFileDialogFragment.ProgressEvent(sources[0], percent));
                        }
                    }
                }

                @Override
                public void onSuccess(String filePath)
                {
                    downloadFilePath = filePath;
                    synchronized (DownloadService.this)
                    {
                        updateProgressNotificationDone(src);
                    }

                }

                @Override
                public void onFailure(Exception e)
                {
                    failed.add(new Pair<Path, Exception>(src, e));
                    synchronized (DownloadService.this)
                    {
                        updateProgressNotificationError(src, e);
                    }
                }
            },
                    new NetworkOperationCallback()
                    {
                        @Override
                        public boolean stopNetworkOperation()
                        {	// always called during download
                            if (mLastCanceled.get() > 0)
                            {
                                synchronized (DownloadService.this)
                                {
                                    upgradeProgressNotificationCancel(src);
                                }
                                mLastCanceled.set(0);
                                return true;
                            }
                            return false;
                        }
                    });
            try
            {
                requester.download(teamDriveId, PathUtils.remoteRequestString(src.getParentPath()), src.getName(),
                        dest.getAbsolutePathString(), GenericUtils.createCopyFileName(fs, src.getName(), dest), null);
            }
            catch (XCBStatusException | HttpResponseException | TeamDriveNotFoundException | ConnectFailedException
                    | LogonFailedException | NotificationException | ServerLicenseException | FileWasModifiedException
                    | IOException | RedirectException e1)
            {
                failed.add(new Pair<Path, Exception>(src, e1));
                synchronized (DownloadService.this)
                {
                    updateProgressNotificationError(src, e1);
                }
            }
        }

        String externalStorageAppPath = GenericUtils.getExternalStorageAppPath().getAbsolutePathString();

        if (externalStorageAppPath.equals(dest.getAbsolutePathString()))
        {
            scanDownloadDir();
        }

        if (failed.isEmpty())
        {
            if (delSrc)
            {
                // Does not delete check this on errors!
                deleteRemote(sources);
            }
        }

        return downloadFilePath;
    }




    private void updateProgressNotification(int progressPercent, Path... sources)
    {

        if (updateFileJobInfoArray(mOngoingJobTimestamp.get(), progressPercent, sources))
        {
            DownloadNotification nh = getDependencies().provideDownloadNotificationHelper();
            nh.updateProgressNotification(getApplicationContext(),
                    Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                    ID_PROGRESS, true);
        }
    }

    private void updateCancelNotification(Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, TPBaseNotification.PROGRESS_CANCEL, sources);
        DownloadNotification nh = getDependencies().provideDownloadNotificationHelper();
        nh.updateCancelNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(ongoingJobTimestamp);
    }

    private void updateDoneNotification(Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, TPBaseNotification.PROGRESS_PERCENT_DONE, sources);
        DownloadNotification nh = getDependencies().provideDownloadNotificationHelper();
        nh.updateDoneNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(ongoingJobTimestamp);
    }


    private void updateErrorNotification(Exception exception, Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(mOngoingJobTimestamp.get(), exception, sources);
        DownloadNotification nh = getDependencies().provideDownloadNotificationHelper();
        nh.updateErrorNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(), FileJobInfo[].class),
                ongoingJobTimestamp);
        deleteJobIfFinished(mOngoingJobTimestamp.get());
    }



    private void updateProgressNotificationError(Path source, Exception e)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_ERROR, source);
        updateErrorNotification(e, source);
    }

    private void updateProgressNotificationDone(Path source)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_DONE, source);
        updateDoneNotification(source);
    }

    private void upgradeProgressNotificationStart(Path source)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_START, source);
    }

    private void upgradeProgressNotificationStart(Path[] sources)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_START, sources);
    }

    private void upgradeProgressNotificationCancel(Path source)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_CANCEL, source);
        updateCancelNotification(source);
    }





    private boolean isCurrentJobCanceled()
    {
        return (mCurrCanceledJob.get() == mOngoingJobTimestamp.get());
    }


}

