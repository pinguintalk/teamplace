package net.teamplace.android.services;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;

import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.request.files.ExportRequester;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.Log;

import java.util.Arrays;
import java.util.List;

import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Created by nangu on 09.06.15.
 */
public class ExportPdfService extends TPIntentService
{
    private static final String TAG = tag(ExportPdfService.class);
    private static final int ID_PROGRESS = IdGenerator.getId();

    public ExportPdfService()
    {
        super(WORKER);
    }

    @Override
    public void onStart(Intent intent, int startId)
    {

        int requestCode = intent.getIntExtra(IntentFactory.REQUEST_CODE, -1);
        if (requestCode == IntentFactory.REQUEST_EXPORT_TO_PDF)
        {

            TimeSource timeSource = getDependencies().provideTimeSource();
            long created = timeSource.currentTimeMillis();
            synchronized (this)
            {
                Path path = (Path) intent.getParcelableExtra(IntentFactory.KEY_PATH);

                Path[] paths = new Path[]{path};
                PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                        paths, FileJobInfo.STATE_QUEUED, TPBaseNotification.PROGRESS_PERCENT_START);
                FileJobInfo jobInfo = new FileJobInfo(created, requestCode, pathStateInfo, path);
                mJobQueue.put(created, jobInfo);
            }
            intent.putExtra(IntentFactory.KEY_JOB_TIMESTAMP, created);
        }
        super.onStart(intent, startId);
    }


    @Override
    protected void onHandleIntent(Intent intent)
    {
        Bundle extras = intent.getExtras();
        int type = extras.getInt(IntentFactory.REQUEST_CODE);

        if (type == IntentFactory.REQUEST_EXPORT_TO_PDF)
        {
            handleExportToPdf(extras);
        }
        else
        {
            throw new IllegalArgumentException("Request type not recognized");
        }
    }


    private void handleExportToPdf(Bundle extras)
    {

        long created = extras.getLong(IntentFactory.KEY_JOB_TIMESTAMP);
        mOngoingJobTimestamp.set(created);

        Path path = extras.getParcelable(IntentFactory.KEY_PATH);

        exportToPdf(path, path.getName() + "." + ExportRequester.VALUE_EXPORT_FORMAT_PDF,
                ExportRequester.VALUE_EXPORT_FORMAT_PDF);
        Intent i = buildResponseIntent(IntentFactory.ACTION_RESPONSE_EXPORT_TO_PDF, extras);
        sendLocalBroadcast(i);
    }

    private void handleExportToZip(Bundle extras)
    {
        Path path = extras.getParcelable(IntentFactory.KEY_PATH);
        export(path, path.getName() + "." + ExportRequester.VALUE_EXPORT_FORMAT_ZIP,
                ExportRequester.VALUE_EXPORT_FORMAT_ZIP);
        Intent i = buildResponseIntent(IntentFactory.ACTION_RESPONSE_EXPORT_TO_ZIP, extras);
        sendLocalBroadcast(i);
    }



    private void export(Path path, String exportedName, String exportFormat)
    {
        try
        {
            Path dest = Path.valueOf(path.getParentPath(), path.getName() + "." + exportFormat);
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
            String srcParent = PathUtils.remoteRequestString(path.getParentPath());
            String destPath = PathUtils.remoteRequestString(dest);

            ExportRequester requester = getDependencies().provideExportRequester(this);
            requester.export(teamDriveId, srcParent, path.getName(), destPath, exportFormat, FORCE_NEW_NAME);
        }
        catch (Exception e)
        {
            Log.d(TAG, "Exception in export()", e);
            int requestType = exportFormatToRequestType(exportFormat);
            postErrorFeedback(path, requestType, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
        }
    }

    private void exportToPdf(Path src , String exportedName, String exportFormat)
    {
        try
        {
            Path dest = Path.valueOf(src.getParentPath(), src.getName() + "." + exportFormat);
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(src);
            String srcParent = PathUtils.remoteRequestString(src.getParentPath());
            String destPath = PathUtils.remoteRequestString(dest);

            synchronized (this)
            {
                updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_START, ID_PROGRESS, src);
            }

            ExportRequester requester = getDependencies().provideExportRequester(this);
            requester.export(teamDriveId, srcParent, src.getName(), destPath, exportFormat, FORCE_NEW_NAME);

            synchronized (this)
            {
                updateProgressNotificationDone(src);
            }

        }
        catch (XCBStatusException e)
        {
            if (e.getStatusCode() == ServerParams.CB_STATUS_PROCESSING) {
                int requestType = exportFormatToRequestType(exportFormat);
                postInfoFeedback(src, requestType, e);

                synchronized (this)
                {
                    updateProgressNotificationDone(src);
                }
            }
        }
        catch (Exception e)
        {
            Log.e(TAG, "Exception in exportToPdf() e.toString()", e);

            synchronized (this)
            {
                updateProgressNotificationError(src, e);
            }

            int requestType = exportFormatToRequestType(exportFormat);
            postErrorFeedback(src, requestType, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);

        }

    }


    private void updateProgressNotification(int progressPercent,int id, Path... sources)
    {
        if (updateFileJobInfoArray(mOngoingJobTimestamp.get(), progressPercent, sources))
        {
            ExportPdfNotification nh = getDependencies().provideExportPdfNotificationHelper();
            nh.updateProgressNotification(getApplicationContext(),
                    Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(),
                            FileJobInfo[].class), id, false);
        }
    }

    private void updateDoneNotification(Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, TPBaseNotification.PROGRESS_PERCENT_DONE, sources);
        ExportPdfNotification nh = getDependencies().provideExportPdfNotificationHelper();
        nh.updateDoneNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(),
                        FileJobInfo[].class), ongoingJobTimestamp);
        deleteJobIfFinished(ongoingJobTimestamp);
    }

    private void updateErrorNotification(List<Pair<Path, Exception>> failed)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(mOngoingJobTimestamp.get(), failed);
        ExportPdfNotification nh = getDependencies().provideExportPdfNotificationHelper();
        nh.updateErrorNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(),
                        FileJobInfo[].class), ongoingJobTimestamp);
        deleteJobIfFinished(mOngoingJobTimestamp.get());
    }

    private void updateErrorNotification(Exception exception, Path... sources)
    {
        long ongoingJobTimestamp = mOngoingJobTimestamp.get();
        updateFileJobInfoArray(ongoingJobTimestamp, exception, sources);
        ExportPdfNotification nh = getDependencies().provideExportPdfNotificationHelper();
        nh.updateErrorNotification(getApplicationContext(),
                Arrays.copyOf(mJobQueue.values().toArray(), mJobQueue.size(),
                        FileJobInfo[].class), ongoingJobTimestamp);
        deleteJobIfFinished(mOngoingJobTimestamp.get());
    }

    private void updateProgressNotificationError(Path sources, Exception e)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_DONE, ID_PROGRESS, sources);
        updateErrorNotification(e, sources);
    }

    private void updateProgressNotificationDone(Path sources)
    {
        updateProgressNotification(TPBaseNotification.PROGRESS_PERCENT_DONE, ID_PROGRESS, sources);
        updateDoneNotification(sources);
    }



    private int exportFormatToRequestType(String exportFormat)
    {
        switch (exportFormat)
        {
            case ExportRequester.FORMAT_PDF:
                return IntentFactory.REQUEST_EXPORT_TO_PDF;
            case ExportRequester.FORMAT_ZIP:
                return IntentFactory.REQUEST_EXPORT_TO_ZIP;
            default:
                return 0; // TODO: @jobol why zero?
        }
    }


}
