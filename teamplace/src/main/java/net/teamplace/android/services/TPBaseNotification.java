package net.teamplace.android.services;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Pair;
import android.view.View;
import android.widget.RemoteViews;

import com.cortado.android.R;

import net.teamplace.android.services.IntentFactory;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.initialization.InitializationActivity;

import de.greenrobot.event.EventBus;



/**
 * Created by nangu on 09.06.15.
 */

public abstract class TPBaseNotification
{
    private static final String TAG = "net.teamplace.android.services.TPNotificationHelper";

    public static final int PROGRESS_PERCENT_START = 0;
    public static final int PROGRESS_PERCENT_DONE = 100;
    public static final int PROGRESS_CANCEL = -1;
    public static final int PROGRESS_ERROR = -2;

    List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
    List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
    List<Pair<FileJobInfo.PathStateInfo, Long>> finishedOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
    List<Pair<FileJobInfo.PathStateInfo, Long>> failedOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();
    List<Pair<FileJobInfo.PathStateInfo, Long>> cancelOperations = new ArrayList<Pair<FileJobInfo.PathStateInfo, Long>>();

    private final Map<Long, FileJobInfo> jobinfos = new HashMap<Long, FileJobInfo>();

    public TPBaseNotification()
    {

    }

    public synchronized void updateProgressNotification(Context ctx, FileJobInfo[] jobs,
                                                        int progressId, boolean isCancelable)
    {
        boolean multipleOngoingNoProgress = false;
        extractOperationLists(jobs);
        multipleOngoingNoProgress = ongoingOperations.size() > 1;

        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(ctx);
        builder.setOngoing(true);


        if (ongoingOperations.size() > 0)
        {
            buildSmallNotification(ctx, builder, ongoingOperations, activeOperations, multipleOngoingNoProgress);
            Notification notification = builder.build();
            buildLargeNotification(ctx, notification, ongoingOperations, activeOperations,
                    multipleOngoingNoProgress, isCancelable);
            nm.notify(TAG, progressId, notification);
        }
        else
        {
            if (activeOperations.size() < 1 && cancelOperations.size() == 0)
            {
                nm.cancel(TAG, progressId);
            }
        }
    }

    public synchronized void cancelProgressNotification(Context ctx, int  progressId)
    {
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(TAG, progressId);
    }

    public synchronized void updateDoneNotification(Context ctx, FileJobInfo[] jobs, long jobId)
    {
        extractOperationLists(jobs);

        if (finishedOperations.size() == 0)
        {
            return;
        }

        String fileName ="";
        for (Pair<PathStateInfo, Long> stateId : finishedOperations)
        {
            long id = stateId.second;
            if (jobId == id)
            {
                fileName = stateId.first.getPath().getName();
            }
        }

        if (EventBus.getDefault().hasSubscriberForEvent(FileJobInfo[].class))
        {
            EventBus.getDefault().post(jobs);
        }

        Notification.Builder builder = new Notification.Builder(ctx);
        builder.setSmallIcon(R.drawable.notification_icon);
        builder.setContentTitle(getActionTitleDoneString(ctx,jobId));
        builder.setContentText(fileName);
        builder.setTicker(ctx.getString(R.string.ntf_title_done) + " " + fileName);
        builder.setContentIntent(buildOpenIntent(ctx));
        builder.setAutoCancel(true);
        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(TAG, IdGenerator.getId(), builder.build());
    }


    public synchronized void updateCancelNotification(Context ctx, FileJobInfo[] jobs, long jobId)
    {
        extractOperationLists(jobs);

        if (finishedOperations.size() == 0)
        {
            return;
        }

        String fileName = "";
        for (Pair<PathStateInfo, Long> stateId : finishedOperations)
        {
            long id = stateId.second;
            if (jobId == id)
            {
                fileName = stateId.first.getPath().getName();
            }
        }


        Notification.Builder builder = new Notification.Builder(ctx);
        builder.setSmallIcon(R.drawable.notification_icon);
        builder.setContentTitle(getActionTitleCanceledString(ctx,jobId));
        builder.setContentText(fileName);
        builder.setContentIntent(buildOpenIntent(ctx));
        builder.setAutoCancel(true);
        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(TAG, IdGenerator.getId(), builder.build());
    }

    public static ArrayList<Pair<PathStateInfo, Long>> getStateIdList(FileJobInfo[] jobs, int action, int state)
    {
        Map<Long, Integer> idActionMap = new HashMap<>();
        for (FileJobInfo job : jobs)
        {
            idActionMap.put(job.getCreated(), job.getAction());
        }
        List<Pair<PathStateInfo, Long>> stateIdList = new ArrayList<>();
        extractOperationListForState(jobs, state, stateIdList);

        ArrayList<Pair<PathStateInfo, Long>> files = new ArrayList<>();
        for (Pair<PathStateInfo, Long> stateId : stateIdList)
        {
            long id = stateId.second;
            if (idActionMap.get(id) == action)
            {
                files.add(stateId);
            }
        }
        return files;
    }

    public static boolean isUploadJob(FileJobInfo[] jobs, Pair<PathStateInfo, Long> stateId)
    {
        for (FileJobInfo job : jobs)
        {
            if (stateId.second == job.getCreated()) {

                if (job.getAction() == IntentFactory.REQUEST_COPY) {
                    Path dest = job.getDest();
                    Path src = stateId.first.getPath();
                    if ((src instanceof Path.Local) && !(dest instanceof Path.Local)) {
                        // this is a upload job
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public synchronized void updateErrorNotification(Context ctx, FileJobInfo[] jobs,long jobId)
    {
        extractOperationLists(jobs);

        if (failedOperations.size() == 0)
        {
            return;
        }

        String fileName ="";
        for (Pair<PathStateInfo, Long> stateId : failedOperations)
        {
            long id = stateId.second;
            if (jobId == id)
            {
                fileName = stateId.first.getPath().getName();
            }
        }

        if (EventBus.getDefault().hasSubscriberForEvent(FileJobInfo[].class))
        {
            EventBus.getDefault().post(jobs);
        }

        String title = getActionTitleErrorString(ctx,jobId);
        Notification.Builder builder = new Notification.Builder(ctx);
        builder.setSmallIcon(R.drawable.notification_icon);
        builder.setContentTitle(title);
        builder.setContentText(fileName);
        Notification.BigTextStyle bigTextStyle = new Notification.BigTextStyle();
        bigTextStyle.setBigContentTitle(title);
        builder.setAutoCancel(true);
        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        builder.setStyle(bigTextStyle);
        nm.notify(TAG, IdGenerator.getId(), builder.build());

    }

    public synchronized String buildFileList(List<Pair<PathStateInfo, Long>> toExtractFrom, int action)
    {
        StringBuilder builder = null;
        for (Pair<PathStateInfo, Long> infoPair : failedOperations)
        {
            if (jobinfos.get(infoPair.second).getAction() == action)
            {
                if (builder == null)
                {
                    builder = new StringBuilder();
                }
                builder.append(infoPair.first.getPath().getName()).append(", ");
            }
        }
        return builder != null ? builder.substring(0, builder.length() - 2) : null;
    }

    private void buildErrorNotificationForAction(Context ctx, Notification.Builder builder,
                                                 Notification.BigTextStyle bigTextStyle, int action, int actionStrRes, String fileList)
    {
        String msg = String.format(ctx.getString(actionStrRes), fileList);
        builder.setContentText(msg);
        builder.setAutoCancel(true);
        NotificationManager nm = (NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE);
        bigTextStyle.bigText(msg);
        builder.setStyle(bigTextStyle);
        nm.notify(buildFailedTag(action), IdGenerator.getId(), builder.build());
    }

    protected void buildSmallNotification(Context ctx, Notification.Builder builder,
                                          List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations,
                                          List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations,
                                          boolean multipleOngoingNoProgress)
    {

        String actionString = 	getActionString(ctx, ongoingOperations.get(0).second);

        builder.setSmallIcon(R.drawable.notification_icon);
        builder.setContentTitle(actionString);
        builder.setContentText(ongoingOperations.size() == 1 ? ongoingOperations.get(0).first.getPath().getName()
                : String.format(ctx.getString(R.string.ntf_number_of_files), ongoingOperations.size()));
        builder.setContentInfo(activeOperations.size() + " " + ctx.getString(R.string.ntf_pending));

        PathStateInfo infoToEvaluate = ongoingOperations.get(0).first;
        int percent = infoToEvaluate.getProgressPercent();

        builder.setProgress(100, percent,
                percent == 0 || multipleOngoingNoProgress);

        if(jobinfos.get(ongoingOperations.get(0).second).getAction() == IntentFactory.REQUEST_UPLOAD)
        {
            builder.setPriority(Notification.PRIORITY_LOW);
        }
        else if(jobinfos.get(ongoingOperations.get(0).second).getAction() == IntentFactory.REQUEST_DOWNLOAD)
        {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }
        else {
            builder.setPriority(Notification.PRIORITY_DEFAULT);
        }

    }

    protected void buildLargeNotification(Context ctx, Notification notification,
                                          List<Pair<FileJobInfo.PathStateInfo, Long>> ongoingOperations,
                                          List<Pair<FileJobInfo.PathStateInfo, Long>> activeOperations,
                                          boolean multipleOngoingNoProgress,boolean isCancelable)

    {

        RemoteViews rv = new RemoteViews(ctx.getPackageName(), R.layout.prg_big_notification);

        rv.setTextViewText(R.id.txt_action, getActionString(ctx, ongoingOperations.get(0).second));

        PathStateInfo infoToEvaluate = ongoingOperations.get(0).first;
        int percent = infoToEvaluate.getProgressPercent();

        rv.setProgressBar(R.id.prg_progress, 100, ongoingOperations.get(0).first.getProgressPercent(),
                percent == 0 || multipleOngoingNoProgress);
        rv.setTextViewText(R.id.txt_percent, percent > 0 ? percent + "%" : "");

        rv.setTextViewText(R.id.txt_ongoing, ongoingOperations.size() == 1 ? ongoingOperations.get(0).first.getPath()
                .getName()
                : ongoingOperations.size() + " " + ctx.getString(R.string.ntf_files));

        int[] containerIds = new int[] {R.id.rl_pending0, R.id.rl_pending1, R.id.rl_pending2, R.id.rl_pending3,
                R.id.rl_pending4};
        int[] tvIds = new int[] {R.id.txt_pending0, R.id.txt_pending1, R.id.txt_pending2, R.id.txt_pending3,
                R.id.txt_pending4};
        for (int i = 0; i < tvIds.length; i++)
        {
            if (activeOperations.size() > i)
            {
                rv.setViewVisibility(containerIds[i], View.VISIBLE);
                rv.setTextViewText(tvIds[i], activeOperations.get(i).first.getPath().getName());
            }
            else
            {
                rv.setViewVisibility(containerIds[i], View.GONE);
            }
        }
        int cantBeDisplayed = activeOperations.size() - 5;
        if (cantBeDisplayed > 0)
        {
            rv.setViewVisibility(R.id.txt_pendingN, View.VISIBLE);
            rv.setTextViewText(R.id.txt_pendingN,
                    String.format(ctx.getString(R.string.ntf_more_files), cantBeDisplayed));
        }
        else
        {
            rv.setViewVisibility(R.id.txt_pendingN, View.GONE);
        }

        if (isCancelable)
            rv.setOnClickPendingIntent(R.id.btn_cancel, buildCancelIntent(ctx));
        else
            rv.setViewVisibility(R.id.btn_cancel, View.GONE);


        notification.bigContentView = rv;
    }

    protected void extractOperationLists(FileJobInfo[] jobs)
    {
        jobinfos.clear();
        for (FileJobInfo info : jobs)
        {
            jobinfos.put(info.getCreated(), info);
        }
        extractOperationListForState(jobs, FileJobInfo.STATE_ACTIVE, ongoingOperations);
        extractOperationListForState(jobs, FileJobInfo.STATE_QUEUED, activeOperations);
        extractOperationListForState(jobs, FileJobInfo.STATE_FINISHED, finishedOperations);
        extractOperationListForState(jobs, FileJobInfo.STATE_ERROR, failedOperations);
    }

    protected static void extractOperationListForState(FileJobInfo[] jobs, int state,
                                                       List<Pair<PathStateInfo, Long>> toPopulate)
    {
        toPopulate.clear();
        toPopulate.addAll(FileJobInfo.getPathStateInfoIdPairListForState(state, jobs));
    }

    public String getActionString(Context ctx, long jobId)
    {
        switch (jobinfos.get(jobId).getAction())
        {
            case IntentFactory.REQUEST_COPY:
                return ctx.getString(R.string.ntf_copying);
            case IntentFactory.REQUEST_UPLOAD:
                return ctx.getString(R.string.ntf_uploading);
            case IntentFactory.REQUEST_DOWNLOAD:
                return ctx.getString(R.string.ntf_downloading);
            case IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE:
            case IntentFactory.REQUEST_MOVE:
            case IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL:
                return ctx.getString(R.string.ntf_moving);
            case IntentFactory.REQUEST_EXPORT_TO_PDF:
                return ctx.getString(R.string.ntf_title_export_pdf);
            case IntentFactory.REQUEST_EXPORT_TO_ZIP:
                return ctx.getString(R.string.ntf_ziping);

            default:
                return "";
        }
    }

    public String getActionTitleDoneString(Context ctx, long jobId)
    {
        switch (jobinfos.get(jobId).getAction())
        {
            case IntentFactory.REQUEST_UPLOAD:
                return ctx.getString(R.string.ntf_title_upload_complete);
            case IntentFactory.REQUEST_DOWNLOAD:
                return ctx.getString(R.string.ntf_title_download_complete);
            case IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE:
            case IntentFactory.REQUEST_MOVE:
            case IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL:
                return ctx.getString(R.string.ntf_title_move_complete);
            case IntentFactory.REQUEST_EXPORT_TO_PDF:
                return ctx.getString(R.string.ntf_title_pdf_export_done);
            case IntentFactory.REQUEST_EXPORT_TO_ZIP:
                return ctx.getString(R.string.ntf_title_zip_export_done);
            default:
                return ctx.getString(R.string.ntf_title_done);
        }
    }

    public String getActionTitleCanceledString(Context ctx, long jobId)
    {
        switch (jobinfos.get(jobId).getAction())
        {
            case IntentFactory.REQUEST_UPLOAD:
                return ctx.getString(R.string.ntf_title_upload_canceled);
            case IntentFactory.REQUEST_DOWNLOAD:
                return ctx.getString(R.string.ntf_title_download_canceled);
            case IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE:
            case IntentFactory.REQUEST_MOVE:
            case IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL:
                return ctx.getString(R.string.ntf_title_move_canceled);
            default:
                return ctx.getString(R.string.ntf_title_canceled);
        }
    }

    public String getActionTitleErrorString(Context ctx, long jobId)
    {
        switch (jobinfos.get(jobId).getAction())
        {
            case IntentFactory.REQUEST_EXPORT_TO_PDF:
                return ctx.getString(R.string.ntf_title_pdf_export_failed);
            case IntentFactory.REQUEST_UPLOAD:
                return ctx.getString(R.string.ntf_title_upload_failed);
            case IntentFactory.REQUEST_DOWNLOAD:
                return ctx.getString(R.string.ntf_title_download_failed);
            case IntentFactory.REQUEST_MOVE_LOCAL_TO_REMOTE:
            case IntentFactory.REQUEST_MOVE:
            case IntentFactory.REQUEST_MOVE_REMOTE_TO_LOCAL:
                return ctx.getString(R.string.ntf_title_move_failed);
            default:
                return ctx.getString(R.string.ntf_title_error);
        }
    }

    protected PendingIntent buildCancelIntent(Context ctx)
    {

        if (jobinfos.get(ongoingOperations.get(0).second).getAction() == IntentFactory.REQUEST_UPLOAD)
        {
            return PendingIntent.getService(ctx, 0, IntentFactory.buildCancelUpload(ctx), 0);
        }
        else if (jobinfos.get(ongoingOperations.get(0).second).getAction() == IntentFactory.REQUEST_DOWNLOAD)
        {
            return PendingIntent.getService(ctx, 0, IntentFactory.buildCancelDownload(ctx), 0);
        }
        else
            return PendingIntent.getService(ctx, 0, IntentFactory.buildCancelCopyMoveIntent(ctx), 0);
    }

    protected PendingIntent buildOpenIntent(Context ctx)
    {
        Intent intent = new Intent(ctx, InitializationActivity.class);

        boolean singleDest = true;
        Iterator<Pair<PathStateInfo, Long>> iterator = finishedOperations.iterator();
        String pathStr = null;
        String nextPathStr;

        while (iterator.hasNext() && singleDest)
        {
            nextPathStr = jobinfos.get(iterator.next().second).getDest().toString();
            singleDest = pathStr == null || pathStr.equals(nextPathStr);
            pathStr = nextPathStr;
        }

        if (singleDest)
        {
            intent.setAction(InitializationActivity.ACTION_OPEN_PATH);
            Path dest = jobinfos.get(finishedOperations.get(0).second).getDest();
            intent.putExtra(InitializationActivity.EXTRA_PATH, dest);
        }
        else
        {
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.setAction(Intent.ACTION_MAIN);
        }
        return PendingIntent.getActivity(ctx, 0, intent, Intent.FLAG_ACTIVITY_CLEAR_TOP, null);
    }

    protected String buildFailedTag(int action)
    {
        return TAG + action;
    }

}

