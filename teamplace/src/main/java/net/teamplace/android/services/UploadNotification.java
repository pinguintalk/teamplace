package net.teamplace.android.services;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.util.Pair;
import android.view.View;
import android.widget.RemoteViews;

import net.teamplace.android.services.IntentFactory;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.initialization.InitializationActivity;

import de.greenrobot.event.EventBus;


/**
 * Created by nangu on 09.06.15.
 */
public class UploadNotification extends TPBaseNotification
{
    private static final String TAG = "com.cortado.android.services.UploadNotificationHelper";

    private static UploadNotification instance;


    private UploadNotification()
    {

    }

    public static synchronized UploadNotification getInstance()
    {
        if (instance == null)
        {
            instance = new UploadNotification();
        }
        return instance;
    }

}
