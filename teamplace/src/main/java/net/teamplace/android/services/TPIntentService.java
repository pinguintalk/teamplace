package net.teamplace.android.services;

/**
 * Created by nangu on 09.06.15.
 */

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.util.Pair;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.BrowsingService.Contract;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.FileJobInfo;
import net.teamplace.android.browsing.FileJobInfo.PathStateInfo;
import net.teamplace.android.browsing.path.FileSystem;
import net.teamplace.android.browsing.path.FileSystemImpl;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.Path.Local;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.content.IntentBus;
import net.teamplace.android.content.IntentBusImpl;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.file.RemoteArchive;
import net.teamplace.android.http.file.RemoteFile;
import net.teamplace.android.http.file.RemoteFolder;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.request.browse.BrowseRequester;
import net.teamplace.android.http.request.files.ExportRequester;
import net.teamplace.android.http.request.files.FileRequester;
import net.teamplace.android.http.request.smartfiling.SmartFilingRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.providers.http.BrowseRequesterProvider;
import net.teamplace.android.providers.http.ExportRequesterProvider;
import net.teamplace.android.providers.http.FileRequesterProvider;
import net.teamplace.android.providers.http.SmartFilingRequesterProvider;
import net.teamplace.android.time.SystemTimeSource;
import net.teamplace.android.time.TimeSource;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Performs network requests in the background. Inserts results in FileProvider when finished
 *
 */
public abstract class TPIntentService extends IntentService
{
    private static final String TAG = tag(TPIntentService.class);
    protected static final String WORKER = "worker";

    /** Local folder sizes are always 0, for speed reasons. In the UI they are not shown */
    private static final int DEFAULT_LOCAL_FOLDER_SIZE = 0;

    /** The depth of the tree the browse request returns */
    protected static final int BROWSE_REQUEST_DEPTH = 1;

    /** Does not force overwrite during copy or move of a file */
    protected static final int DONT_FORCE_OVERWRITE = ServerParams.OVERWRITE_DONT;

    /** Forces overwrite during copy or move of a file */
    protected static final int FORCE_OVERWRITE = ServerParams.OVERWRITE_FORCE;

    /** Forces renaming a file during copy / move, if an equally named file exists */
    protected static final int FORCE_NEW_NAME = ServerParams.OVERWRITE_NEW_NAME;

    /** Forces creating a new version during copy / move, if an equally named file exists */
    protected static final int FORCE_NEW_VERSION = ServerParams.OVERWRITE_NEW_VERSION;

    // Memory issues are not a concern here
    @SuppressLint("UseSparseArrays")
    protected
    final Map<Long, FileJobInfo> mJobQueue = new HashMap<Long, FileJobInfo>();
    // private final AtomicBoolean mCancelCopyOrMoveRequested = new AtomicBoolean(false);
    protected final AtomicLong mLastCanceled = new AtomicLong(0l);
    protected final AtomicLong mOngoingJobTimestamp = new AtomicLong(0l);
    protected final AtomicLong mCurrCanceledJob = new AtomicLong(0l);

    private Dependencies mDependencies;

    public interface Dependencies extends BrowseRequesterProvider,
            FileRequesterProvider,
            SmartFilingRequesterProvider,
            ExportRequesterProvider
    {
        TimeSource provideTimeSource();

        FileSystem provideFileSystem();

        EventBus provideEventBus();

        IntentBus provideIntentBus(Context context);

        DownloadNotification provideDownloadNotificationHelper();
        UploadNotification provideUploadNotificationHelper();
        ExportPdfNotification provideExportPdfNotificationHelper();

    }

    protected synchronized Dependencies getDependencies()
    {
        if (mDependencies == null)
        {
            mDependencies = new DependenciesImpl();
        }
        return mDependencies;
    }

    /** @hide Used by tests to inject dependencies. Don't use in production */
    public synchronized void setDependencies(Dependencies dependencies)
    {
        mDependencies = dependencies;
    }

    public TPIntentService(String workerName)
    {
        super(workerName);
    }




    protected static Intent buildResponseIntent(String action, Bundle requestExtras)
    {
        Intent i = new Intent(action);
        i.putExtra(IntentFactory.RESPONSE_REQUEST_EXTRAS, requestExtras);
        return i;
    }


    private ArrayList<FileInfo> browseLocal(Path.Local path)
    {
        FileSystem fs = getDependencies().provideFileSystem();
        Path.Local[] paths = fs.listChildren(path);
        checkNonNullArg(paths, "The file " + path.getAbsolutePathString() + " is not browsable");
        Arrays.sort(paths, IntentFactory.LOCAL_PATH_FOLDER_FIRST_COMPARATOR);
        return localPathsToFileInfos(fs, paths);
    }

    private ArrayList<FileInfo> browseRemote(Path path)
    {
        RemoteFolder folder = fetchRemoteFolder(path);
        return remoteFolderToFileInfos(path, folder);
    }


    protected boolean deleteLocal(Local[] paths)
    {
        if (paths == null || paths.length == 0)
        {
            return true;
        }

        FileSystem fs = getDependencies().provideFileSystem();
        for (Path.Local p : paths)
        {
            // Stop on first error
            if (!fs.delete(p, true))
            {
                postErrorFeedback(paths, Contract.REQUEST_DELETE, new IOException("local delete failed"));
                return false;
            }
        }
        return true;
    }

    protected boolean deleteLocal(Local path)
    {
        if (path == null || path.getLocation().isEmpty())
        {
            return true;
        }

        FileSystem fs = getDependencies().provideFileSystem();
        // Stop on first error
        if (!fs.delete(path, true))
        {
            postErrorFeedback(path, Contract.REQUEST_DELETE, new IOException("local delete failed"));
            return false;
        }

        return true;
    }

    protected void deleteRemote(Path path)
    {
        if (path == null || path.getLocation().isEmpty())
        {
            return;
        }

        try
        {
            Path parent = path.getParentPath();
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(parent);
            String parentStr = PathUtils.remoteRequestString(parent);
            String[] names = new String[]{path.getName()};
            FileRequester requester = getDependencies().provideFileRequester(this);
            requester.deleteFolderOrFile(teamDriveId, parentStr, names, FORCE_OVERWRITE);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error during delete request", e);
            postErrorFeedback(path, IntentFactory.REQUEST_DELETE, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
        }
    }

    protected void deleteRemote(Path[] paths)
    {
        if (paths == null || paths.length == 0)
        {
            return;
        }

        try
        {
            Path parent = paths[0].getParentPath();
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(parent);
            String parentStr = PathUtils.remoteRequestString(parent);
            String[] names = extractPathNames(paths);
            FileRequester requester = getDependencies().provideFileRequester(this);
            requester.deleteFolderOrFile(teamDriveId, parentStr, names, FORCE_OVERWRITE);
        }
        catch (Exception e)
        {
            Log.e(TAG, "Error during delete request", e);
            postErrorFeedback(paths, IntentFactory.REQUEST_DELETE, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
        }
    }


    protected void scanDownloadDir()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                File basePath = GenericUtils.getExternalStorageAppDir();
                File[] folderContent = basePath.listFiles(new FileFilter()
                {
                    @Override
                    public boolean accept(File pathname)
                    {
                        return pathname.isFile();
                    }
                });
                String[] filePaths = new String[folderContent.length];
                for (int i = 0; i < folderContent.length; i++)
                {
                    filePaths[i] = folderContent[i].getAbsolutePath();
                }
                MediaScannerConnection.scanFile(getApplicationContext(), filePaths, null, null);
            }
        }).start();
    }

    protected void sendLocalBroadcast(Intent i)
    {
        IntentBus bus = getDependencies().provideIntentBus(this);
        bus.sendBroadcast(i);
    }



    protected boolean updateFileJobInfoArray(long ongoingJobId, int progressState, Path... sources)
    {
        FileJobInfo currJob = mJobQueue.get(ongoingJobId);
        if (currJob != null)
        {
            int currJobState = FileJobInfo.STATE_QUEUED;

            if (progressState == TPBaseNotification.PROGRESS_CANCEL)
                currJobState = FileJobInfo.STATE_FINISHED;
            else if (progressState ==  TPBaseNotification.PROGRESS_ERROR )
                currJobState = FileJobInfo.STATE_ERROR;
            else if ( progressState < TPBaseNotification.PROGRESS_PERCENT_DONE)
                currJobState = FileJobInfo.STATE_ACTIVE;
            else
                currJobState = FileJobInfo.STATE_FINISHED;

            currJob.updatePathStateInfo(sources,currJobState,
                    progressState);
            return true;
        }
        return false;
    }

    protected void updateFileJobInfoArray(long onoingJobId, List<Pair<Path, Exception>> failed)
    {
        FileJobInfo current = mJobQueue.get(mOngoingJobTimestamp.get());
        if (current != null)
        {
            for (Pair<Path, Exception> pathPair : failed)
            {
                current.updatePathStateInfo(new Path[] {pathPair.first}, FileJobInfo.STATE_ERROR,
                        TPBaseNotification.PROGRESS_ERROR, pathPair.second);
            }
        }
    }

    protected void updateFileJobInfoArray(long onoingJobId, Exception exception, Path... sources)
    {
        FileJobInfo current = mJobQueue.get(onoingJobId);
        if (current != null)
        {
            current.updatePathStateInfo(sources, FileJobInfo.STATE_ERROR, TPBaseNotification.PROGRESS_ERROR, exception);
        }
    }


    protected synchronized void deleteJobIfFinished(long ongoingJobId)
    {

        boolean allFinished = true;
        for (FileJobInfo.PathStateInfo pathInfo : mJobQueue.get(ongoingJobId).getSourceInfos())
        {
            if (pathInfo.getProgressPercent() < TPBaseNotification.PROGRESS_PERCENT_DONE &&
                    pathInfo.getProgressPercent() >= TPBaseNotification.PROGRESS_PERCENT_START)
            {
                allFinished = false;
                break;
            }
        }
        if (allFinished)
        {
            mJobQueue.remove(ongoingJobId);
        }
    }


    private String[] extractPathNames(Path[] paths)
    {
        int len = paths.length;
        String[] names = new String[len];
        for (int i = 0; i < len; i++)
        {
            names[i] = paths[i].getName();
        }
        return names;
    }

    private RemoteFolder fetchRemoteFolder(Path folderPath)
    {
        try
        {
            Log.d(TAG, "fetchRemoteFolder, start: " + folderPath);
            String teamDriveId = PathUtils.getTeamDriveIdOrNull(folderPath);
            String remoteRequestString = PathUtils.remoteRequestString(folderPath);
            BrowseRequester requester = getDependencies().provideBrowseRequester(this);
            RemoteFolder folder = requester.doBrowseRequest(teamDriveId, remoteRequestString,
                    BROWSE_REQUEST_DEPTH, false).getBrowsedFolder();
            Log.d(TAG, "fetchRemoteFolder, done: " + folderPath);
            return folder;
        }
        catch (Exception e)
        {
            Log.e(TAG, "fetchRemoteFolder, exception: " + folderPath, e);
            postErrorFeedback(folderPath, IntentFactory.REQUEST_BROWSE, e);
            ExceptionManager em = ExceptionManager.getInstance();
            em.handleException(e);
            return null;
        }
    }

    private ArrayList<FileInfo> remoteFolderToFileInfos(Path parent, RemoteFolder folder)
    {
        if (folder == null)
        {
            return new ArrayList<FileInfo>();
        }

        RemoteFolder[] folders = folder.getFolders();
        RemoteArchive[] archives = folder.getArchives();
        RemoteFile[] files = folder.getFiles();
        int lenFolders = folders == null ? 0 : folders.length;
        int lenArchives = archives == null ? 0 : archives.length;
        int lenFiles = files == null ? 0 : files.length;
        ArrayList<FileInfo> fileInfos = new ArrayList<FileInfo>();

        for (int i = 0; i < lenFolders; i++)
        {
            RemoteFolder child = folders[i];
            fileInfos.add(new FileInfo(Path.valueOf(parent, child.getName()), child.getDate() * 1000L, child.getSize(),
                    child.isFavorite(), FileInfo.CTYPE_FOLDER));
        }

        for (int i = 0; i < lenArchives; i++)
        {
            RemoteArchive child = archives[i];
            fileInfos.add(new FileInfo(Path.valueOf(parent, child.getName()), child.getDate() * 1000L, child.getSize(),
                    child.isFavorite(), FileInfo.CTYPE_ARCHIVE));
        }

        for (int i = 0; i < lenFiles; i++)
        {
            RemoteFile child = files[i];
            String name = child.getName();
            fileInfos.add(new FileInfo(Path.valueOf(parent, name), child.getDate() * 1000L, child.getSize(), child
                    .isFavorite()));
        }
        return fileInfos;
    }

    protected ArrayList<FileInfo> localPathsToFileInfos(FileSystem fs, Path.Local[] paths)
    {
        int len = paths.length;
        ArrayList<FileInfo> fileInfos = new ArrayList<FileInfo>();
        for (int i = 0; i < len; i++)
        {
            Path.Local p = paths[i];

            int ctype = FileInfo.CTYPE_FOLDER;
            long length = DEFAULT_LOCAL_FOLDER_SIZE;
            if (!fs.isDirectory(p))
            {
                ctype = FileInfo.CTYPE_OTHER;
                length = fs.length(p);
            }
            fileInfos.add(new FileInfo(p, fs.lastModified(p), length, false, ctype));
        }
        return fileInfos;
    }


    protected void postErrorFeedback(Path path, int requestType, Exception e)
    {
        postErrorFeedback(new Path[] {path}, requestType, e);
    }

    private void postErrorFeedback(Path[] paths, int requestType, Exception e)
    {
        TimeSource timeSource = getDependencies().provideTimeSource();
        EventBus bus = getDependencies().provideEventBus();

        PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                paths, FileJobInfo.STATE_ERROR, TPBaseNotification.PROGRESS_PERCENT_DONE, e);

        long created = timeSource.currentTimeMillis();
        FileJobInfo jobInfo = new FileJobInfo(created, requestType, pathStateInfo, null);
        bus.post(new FileJobInfo[]{jobInfo});
    }

    protected void postInfoFeedback(Path path, int requestType, Exception e)
    {
        postInfoFeedback(new Path[]{path}, requestType, e);
    }

    private void postInfoFeedback(Path[] paths, int requestType, Exception e)
    {
        TimeSource timeSource = getDependencies().provideTimeSource();
        EventBus bus = getDependencies().provideEventBus();

        PathStateInfo[] pathStateInfo = FileJobInfo.buildPathStateInfoArrayWithSingleState(
                paths, FileJobInfo.STATE_FINISHED, TPBaseNotification.PROGRESS_PERCENT_DONE, e);

        long created = timeSource.currentTimeMillis();
        FileJobInfo jobInfo = new FileJobInfo(created, requestType, pathStateInfo, null);
        bus.post(new FileJobInfo[] {jobInfo});
    }



    private static class DependenciesImpl implements Dependencies
    {
        private final TimeSource mTimeSource = new SystemTimeSource();
        private final EventBus mEventBus = EventBus.getDefault();
        private final DownloadNotification mDownloadNotificationHelper = DownloadNotification.getInstance();
        private final UploadNotification mUploadNotificationHelper = UploadNotification.getInstance();
        private final ExportPdfNotification mExportPdfNotificationHelper = ExportPdfNotification.getInstance();
        private final FileSystem mFileSystem = new FileSystemImpl();
        private IntentBus mIntentBus;

        @Override
        public TimeSource provideTimeSource()
        {
            return mTimeSource;
        }

        @Override
        public EventBus provideEventBus()
        {
            return mEventBus;
        }

        @Override
        public IntentBus provideIntentBus(Context context)
        {
            if (mIntentBus == null)
            {
                mIntentBus = new IntentBusImpl(context);
            }

            return mIntentBus;
        }

        @Override
        public DownloadNotification provideDownloadNotificationHelper()
        {
            return mDownloadNotificationHelper;
        }

        @Override
        public UploadNotification provideUploadNotificationHelper()
        {
            return mUploadNotificationHelper;
        }

        @Override
        public ExportPdfNotification provideExportPdfNotificationHelper()
        {
            return mExportPdfNotificationHelper;
        }
        @Override
        public FileSystem provideFileSystem()
        {
            return mFileSystem;
        }

        @Override
        public BrowseRequester provideBrowseRequester(Context context)
        {
            Session session = new SessionManager(context).getLastSession();
            return new BrowseRequester(context, session, new TeamplaceBackend());
        }

        @Override
        public FileRequester provideFileRequester(Context context)
        {
            Session session = new SessionManager(context).getLastSession();
            return new FileRequester(context, session, new TeamplaceBackend());
        }

        @Override
        public SmartFilingRequester provideSmartFilingRequester(Context context)
        {
            Session session = new SessionManager(context).getLastSession();
            return new SmartFilingRequester(context, session, new TeamplaceBackend());
        }

        @Override
        public ExportRequester provideExportRequester(Context context)
        {
            Session session = new SessionManager(context).getLastSession();
            return new ExportRequester(context, session, new TeamplaceBackend());
        }
    }


    protected static class AnythingHolder<T>
    {
        protected T obj;
    }

    @Override
    protected void onHandleIntent(Intent arg0) {
        // TODO Auto-generated method stub

    }
}
