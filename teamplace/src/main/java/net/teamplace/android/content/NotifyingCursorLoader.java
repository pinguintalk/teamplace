package net.teamplace.android.content;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.lang.ref.WeakReference;

import net.teamplace.android.utils.Log;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;


// NOT USED
/**
 * Tiny CursorLoader that delivers callbacks on the UI thread when it effectively starts and finishes loading. The
 * listener is a WeakReference so it does not leak memory. However the user of this Loader <em>should</em> clear the
 * reference when it does not want to receive callbacks anymore. For instance, a Fragment popped from the stack but
 * running in memory could still receive callbacks and potentially modify the underlying activity; this is not good and
 * the Fragment should clear the WeakReference in onPause()
 * 
 * @author Gil Vegliach
 */
public class NotifyingCursorLoader extends CursorLoader
{
	private static final String LOG_TAG = tag(NotifyingCursorLoader.class);

	private static final int START_LOADING = 0;
	private static final int STOP_LOADING = 1;

	private volatile boolean mIsLoading = false;
	private WeakReference<OnLoadingListener> mWeakRefListener;

	public NotifyingCursorLoader(Context context)
	{
		super(context);
	}

	public NotifyingCursorLoader(Context context, Uri uri)
	{
		super(context, uri, null, null, null, null);
	}

	public NotifyingCursorLoader(Context context, Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder)
	{
		super(context, uri, projection, selection, selectionArgs, sortOrder);
	}

	public void setOnLoadingListener(WeakReference<OnLoadingListener> listener)
	{
		mWeakRefListener = listener;
	}

	public boolean isLoading()
	{
		return mIsLoading;
	}

	@Override
	protected Cursor onLoadInBackground()
	{
		mIsLoading = true;
		mHandler.sendEmptyMessage(START_LOADING);
		Cursor c = super.onLoadInBackground();
		mIsLoading = false;
		mHandler.sendEmptyMessage(STOP_LOADING);
		return c;
	}

	@SuppressLint("HandlerLeak")
	private final Handler mHandler = new Handler()
	{
		@Override
		public void handleMessage(Message msg)
		{
			switch (msg.what)
			{
				case START_LOADING:
				{
					Log.d(LOG_TAG, "onStartLoading()");
					if (mWeakRefListener == null)
						return;
					OnLoadingListener listener = mWeakRefListener.get();
					if (listener == null)
						return;

					listener.onStartLoading();
					break;
				}
				case STOP_LOADING:
				{
					Log.d(LOG_TAG, "onStopLoading()");

					if (mWeakRefListener == null)
						return;
					OnLoadingListener listener = mWeakRefListener.get();
					if (listener == null)
						return;
					listener.onStopLoading();
					break;
				}
			}
		}
	};

	public interface OnLoadingListener
	{
		void onStartLoading();

		void onStopLoading();
	}
}