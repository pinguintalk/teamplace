package net.teamplace.android.content;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;

public interface IntentBus
{
	boolean sendBroadcast(Intent intent);

	void sendBroadcastSync(Intent intent);

	void registerReceiver(BroadcastReceiver receiver, IntentFilter filter);

	void unregisterReceiver(BroadcastReceiver receiver);
}
