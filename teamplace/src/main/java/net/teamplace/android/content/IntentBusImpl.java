package net.teamplace.android.content;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;

public class IntentBusImpl implements IntentBus
{
	private final LocalBroadcastManager mManager;

	public IntentBusImpl(Context context)
	{
		checkNonNullArg(context);
		mManager = LocalBroadcastManager.getInstance(context);
	}

	@Override
	public void registerReceiver(BroadcastReceiver receiver, IntentFilter filter)
	{
		mManager.registerReceiver(receiver, filter);
	}

	@Override
	public boolean sendBroadcast(Intent intent)
	{
		return mManager.sendBroadcast(intent);
	}

	@Override
	public void sendBroadcastSync(Intent intent)
	{
		mManager.sendBroadcastSync(intent);
	}

	@Override
	public void unregisterReceiver(BroadcastReceiver receiver)
	{
		mManager.unregisterReceiver(receiver);
	}

}
