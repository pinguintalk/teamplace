package net.teamplace.android.initialization;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import com.cortado.android.R;

import net.teamplace.android.backend.BackendServers;

public class TutorialActivity extends Activity
{
	WebView wv;
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.init_tutorial);
		wv = (WebView) findViewById(R.id.wv_tutorial);
		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebViewClient(new WebViewClient()
		{
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				return false;
			}
		});
		wv.loadUrl(BackendServers.TUTORIAL);
		Button btnSkip = (Button) findViewById(R.id.btn_close);
		btnSkip.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(event.getAction() == KeyEvent.ACTION_DOWN){
			switch(keyCode)
			{
				case KeyEvent.KEYCODE_BACK:
					if(wv.canGoBack()){
						wv.goBack();
					}else{
						finish();
					}
					return true;
			}

		}
		return super.onKeyDown(keyCode, event);
	}

}
