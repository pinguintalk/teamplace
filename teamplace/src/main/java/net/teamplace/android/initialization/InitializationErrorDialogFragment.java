package net.teamplace.android.initialization;

import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;

import com.cortado.android.R;

public class InitializationErrorDialogFragment extends DialogFragment
{
	private static final String ERROR_REASON = "errorReason";

	public static DialogFragment newInstance(String errorReason)
	{
		DialogFragment fragment = new InitializationErrorDialogFragment();

		Bundle arguments = new Bundle();
		arguments.putString(ERROR_REASON, errorReason);

		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public void show(FragmentManager manager, String tag)
	{
		if (manager.findFragmentByTag(tag) == null)
		{
			super.show(manager, tag);
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		StyleableAlertDialog dialog = new StyleableAlertDialog(getActivity());

		dialog.setTitle(R.string.ntf_title_error);
		dialog.setMessage(getErrorReason());

		dialog.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.ini_close_app),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dismiss();
						getActivity().finish();
					}
				});

		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.ini_try_again),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						((InitializationActivity) getActivity()).getLoaderManager().initLoader(
								InitializationLoader.LOADER_ID, null, ((InitializationActivity) getActivity()));
					}
				});

		dialog.setCanceledOnTouchOutside(false);

		return dialog;
	}

	private String getErrorReason()
	{
		String errorReason = getArguments().getString(ERROR_REASON);

		if (errorReason == null)
		{
			return getString(R.string.error_default);
		}
		else
		{
			return errorReason;
		}
	}
}
