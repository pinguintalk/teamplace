package net.teamplace.android.initialization;

import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.util.annotations.Immutable;
import android.os.Bundle;


@Immutable
public class InitializationResult
{
	private final static String TAG = tag(InitializationResult.class);

	/* Result states */
	public static final int INIT_ERROR_OCCURED = -1;
	public static final int INIT_SESSION_LOADED = 0;
	public static final int INIT_CONFIRM_AUTHENTICATION = 1;
	public static final int INIT_ADD_NEW_ACCOUNT = 2;

	private final int result;
	private final Bundle resultBundle;
	private final Exception exception;

	public InitializationResult(int result, Bundle resultBundle)
	{
		this(result, resultBundle, null);
	}

	public InitializationResult(int result, Bundle resultBundle, Exception exception)
	{
		this.result = result;
		this.resultBundle = resultBundle;
		this.exception = exception;
	}

	public int getResult()
	{
		return result;
	}

	public Bundle getResultBundle()
	{
		return resultBundle;
	}

	public Exception getException()
	{
		return exception;
	}
}
