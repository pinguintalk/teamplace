package net.teamplace.android.initialization;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.IOException;

import net.teamplace.android.account.AuthHelper;
import net.teamplace.android.account.SignupDoneActivity;
import net.teamplace.android.application.DatabaseLocker;
import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.BasicActivity;
import android.accounts.AuthenticatorException;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.os.Bundle;

import com.cortado.android.R;
import com.crashlytics.android.Crashlytics;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class InitializationActivity extends BasicActivity implements LoaderCallbacks<InitializationResult>
{
	@SuppressWarnings("unused")
	private static final String TAG = tag(InitializationActivity.class);
	private static final String PKG = pkg(InitializationActivity.class);
	private static final int REQUEST_CODE = R.id.request_code_initialization_activity;
	private static final int REQUEST_CODE_TUTORIAL = R.id.request_code_tutorial_activity;

	private final String ACTION_SHOW_ERROR_DIALOG = "actionShowErrorDialog";
	private final String EXTRA_ERROR_REASON = "extraErrorReason";

	// Technically speaking should use app package and maybe not even be an action (since we do not have any public
	// intent filter on it)
	public static final String ACTION_OPEN_PATH = PKG + ".openPath";
	public static final String EXTRA_PATH = "path";

	private static boolean startLoader = true;
	private ErrorDialogBroadcastReceiver receiver = new ErrorDialogBroadcastReceiver();

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ini_activity);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		if (startLoader)
		{
			getLoaderManager().initLoader(InitializationLoader.LOADER_ID, getIntent().getExtras(), this);
		}

		startLoader = true;

		registerReceiver(receiver, new IntentFilter(ACTION_SHOW_ERROR_DIALOG));
	}

	@Override
	protected void onPause()
	{
		super.onPause();

		unregisterReceiver(receiver);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == REQUEST_CODE)
		{
			handleSignupDoneResult(resultCode);
		}
		else if (requestCode == REQUEST_CODE_TUTORIAL)
		{
			handleTutorialResult(resultCode);
		}
	}

	private void handleSignupDoneResult(int resultCode)
	{
		switch (resultCode)
		{
			case SignupDoneActivity.RESULT_START_TUTORIAL:
				startLoader = false;
				startActivityForResult(new Intent(this, TutorialActivity.class), REQUEST_CODE_TUTORIAL);
				break;
			case Activity.RESULT_OK:
				getLoaderManager().initLoader(InitializationLoader.LOADER_ID, null, this);
				break;
			case Activity.RESULT_CANCELED:
				finish();
				break;
			default:
				break;
		}
	}

	private void handleTutorialResult(int resultCode)
	{
		getLoaderManager().initLoader(InitializationLoader.LOADER_ID, null, this);
	}

	@Override
	public Loader<InitializationResult> onCreateLoader(int id, Bundle options)
	{
		return new InitializationLoader(this, options);
	}

	@Override
	public void onLoadFinished(Loader<InitializationResult> loader, InitializationResult result)
	{
		getLoaderManager().destroyLoader(InitializationLoader.LOADER_ID);

		if (result == null)
		{
			return;
		}

		int resultValue = result.getResult();

		if (resultValue == InitializationResult.INIT_SESSION_LOADED)
		{
			Crashlytics.getInstance().core.setUserIdentifier(new SessionManager(this).getLastSession().getCredentials().getUsername());
			Intent intent = new Intent(this, BrowsingActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

			if (ACTION_OPEN_PATH.equals(getIntent().getAction()))
			{
				Path path = (Path) getIntent().getParcelableExtra(EXTRA_PATH);
				intent.putExtra(BrowsingActivity.EXTRA_BROWSE_FROM_PATH, path);
			}

			startActivity(intent);
			finish();
		}
		else if (resultValue == InitializationResult.INIT_CONFIRM_AUTHENTICATION
				|| resultValue == InitializationResult.INIT_ADD_NEW_ACCOUNT)
		{
			// unlock Databases here, since we know it's a new login
			DatabaseLocker.get().unlock();
			startActivityForResult(AuthHelper.getIntentFromBundle(result.getResultBundle()), REQUEST_CODE);
		}
		else if (resultValue == InitializationResult.INIT_ERROR_OCCURED)
		{
			Exception cause = result.getException();

			if (cause != null)
			{
				cause.printStackTrace();
				ExceptionManager em = ExceptionManager.getInstance();
				em.handleException(cause);

				String errorReason = null;

				if (cause instanceof AuthenticatorException)
				{
					String message = cause.getMessage();

					if (message.contains("UnknownHostException"))
					{
						errorReason = getString(R.string.error_connection_failed);
					}
					else
					{
						errorReason = getString(R.string.error_default);
					}
				}
				else if (cause instanceof ConnectFailedException)
				{
					errorReason = getString(R.string.error_connection_failed);
				}
				else if (cause instanceof XCBStatusException || cause instanceof HttpResponseException
						|| cause instanceof JsonParseException || cause instanceof JsonMappingException
						|| cause instanceof IOException)
				{
					errorReason = getString(R.string.ini_server_error);
				}
				else
				{
					errorReason = getString(R.string.error_default);
				}

				Intent intent = new Intent(ACTION_SHOW_ERROR_DIALOG);
				intent.putExtra(EXTRA_ERROR_REASON, errorReason);

				sendBroadcast(intent);
			}
		}
		else
		{
			finish();
		}
	}

	@Override
	public void onLoaderReset(Loader<InitializationResult> loader)
	{
	}

	public static Intent getIntent(Context context, boolean invalidateToken)
	{
		Intent intent = new Intent(context, InitializationActivity.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
		intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		intent.putExtra(InitializationLoader.OPTION_INVALIDATE_CACHED_TOKEN, invalidateToken);
		return intent;
	}

	private class ErrorDialogBroadcastReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			DialogFragment fragment = InitializationErrorDialogFragment.newInstance(intent
					.getStringExtra(EXTRA_ERROR_REASON));

			fragment.setCancelable(false);
			fragment.show(getFragmentManager(), "iniError");
		}
	}
}
