package net.teamplace.android.initialization;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.IOException;

import net.teamplace.android.account.AccountConstants;
import net.teamplace.android.account.AuthHelper;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.request.account.AuthToken;
import net.teamplace.android.http.request.configuration.ConfigurationRequester;
import net.teamplace.android.http.session.Credentials;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.utils.ResetHelper;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.os.Bundle;

public class InitializationLoader extends AsyncTaskLoader<InitializationResult>
{
	private final static String TAG = tag(InitializationLoader.class);

	public static final int LOADER_ID = 194286;

	public static final String OPTION_INVALIDATE_CACHED_TOKEN = "optionInvalidateCachedToken";
	public static final String OPTION_RESET = "reset";

	// TODO: this is a leak. Do not ever store a context for an object that lives longer than the Context itself. Use
	// getContext() instead (it gives back a safe app context). Gil
	private final Context mContext;
	private final SessionManager mSessionManager;
	private final AccountManager mAccountManager;

	private boolean invalidateCachedToken = false;
	private boolean clearDataRequested = false;

	public InitializationLoader(Context context, Bundle options)
	{
		super(context);

		mContext = context;
		mSessionManager = new SessionManager(context);
		mAccountManager = AccountManager.get(context);

		if (options != null)
		{
			invalidateCachedToken = options.getBoolean(OPTION_INVALIDATE_CACHED_TOKEN, false);
			clearDataRequested = options.getBoolean(OPTION_RESET, false);
		}
	}

	@Override
	protected void onStartLoading()
	{
		super.onStartLoading();
		forceLoad();
	}

	@Override
	public InitializationResult loadInBackground()
	{
		if (clearDataRequested)
		{
			clearData();
		}
		InitializationResult result = initWithLastSession();

		if (result == null)
		{
			result = initWithExistingAccount();
		}

		if (result == null)
		{
			result = addNewAccount();
		}

		if (result.getResult() == InitializationResult.INIT_SESSION_LOADED)
		{
			TeamDriveService.getAllTeamDrivesSkipCacheQuery(mContext, 0);
		}
		return result;
	}

	// TODO temporary implementation to delete data on user reset. See comment in
	// ResetDialogFragment.killAndRequestResetOnRestart().
	private void clearData()
	{
		try
		{
			SessionManager sessionManager = new SessionManager(getContext());
			AccountManager accountManager = AccountManager.get(getContext());

			Session session = sessionManager.getLastSession();

			if (session != null)
			{
				Account account = session.getAccount();
				accountManager.removeAccount(account, null, null).getResult();

				ResetHelper.resetAllCaches(getContext());
			}
		}
		catch (OperationCanceledException e)
		{
			e.printStackTrace();
		}
		catch (AuthenticatorException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private InitializationResult initWithLastSession()
	{
		Session lastSession = mSessionManager.getLastSession();

		if (lastSession != null && isAccountExisting(lastSession.getAccount()))
		{
			Account account = lastSession.getAccount();
			InitializationResult result = getTokenForAccount(account);

			if (result.getResult() == InitializationResult.INIT_SESSION_LOADED)
			{
				AuthToken token = new AuthToken();
				token.setTokenType("ccs");
				token.setTokenStr(result.getResultBundle().getString(AccountManager.KEY_AUTHTOKEN, "")
						.replace("\"", ""));

				Credentials credentials = new Credentials(lastSession.getAccount().name,
						mAccountManager.getPassword(lastSession.getAccount()));

				lastSession.setToken(token);
				lastSession.setCredentials(credentials);

				mSessionManager.saveSession(lastSession);

				if (!lastSession.isConfigurationValid())
				{
					try
					{
						ConfigurationRequester requester = new ConfigurationRequester(mContext, lastSession, new TeamplaceBackend());

						lastSession.setConfiguration(requester.getConfiguration());

						mSessionManager.saveSession(lastSession);
					}
					catch (Exception exception)
					{
						ExceptionManager em = ExceptionManager.getInstance();
						em.handleException(exception);

						return new InitializationResult(InitializationResult.INIT_ERROR_OCCURED, null, exception);
					}
				}
			}

			return result;
		}

		return null;
	}

	private boolean isAccountExisting(Account account)
	{
		// TODO - check for workplace and ccs accounts
		Account[] accountArray = mAccountManager.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE);
		boolean isAccountExisting = false;

		for (Account acc : accountArray)
		{
			isAccountExisting = acc.equals(account);
		}

		// TODO remove session from cache and database

		return isAccountExisting;
	}

	private InitializationResult initWithExistingAccount()
	{
		Account[] accountArray = mAccountManager.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE);

		if (accountArray.length > 0)
		{
			Account cortadoAccount = accountArray[0];

			InitializationResult result = getTokenForAccount(cortadoAccount);

			if (result.getResult() == InitializationResult.INIT_SESSION_LOADED)
			{
				Session newSession = new Session(mContext, cortadoAccount);

				AuthToken token = new AuthToken();
				token.setTokenType("ccs");
				token.setTokenStr(result.getResultBundle().getString(AccountManager.KEY_AUTHTOKEN, "")
						.replace("\"", ""));

				Credentials credentials = new Credentials(newSession.getAccount().name,
						mAccountManager.getPassword(newSession.getAccount()));

				newSession.setToken(token);
				newSession.setCredentials(credentials);

				try
				{
					ConfigurationRequester requester = new ConfigurationRequester(mContext, newSession, new TeamplaceBackend());
					newSession.setConfiguration(requester.getConfiguration());
					mSessionManager.saveSession(newSession);
				}
				catch (Exception exception)
				{
					return new InitializationResult(InitializationResult.INIT_ERROR_OCCURED, null, exception);
				}
			}

			return result;
		}

		return null;
	}

	private InitializationResult getTokenForAccount(Account account)
	{
		if (invalidateCachedToken)
		{
			AuthHelper.invalidateCachedAuthToken(mAccountManager, account);
		}

		Bundle authBundle = AuthHelper.buildConfigBundle(1, 1);
		AccountManagerFuture<Bundle> future = mAccountManager.getAuthToken(account, AccountConstants.TOKEN_TYPE_CCS,
				authBundle, false, null, null);

		try
		{
			Bundle tokenBundle = future.getResult();
			InitializationResult initResult;

			if (tokenBundle.containsKey(AccountManager.KEY_INTENT))
			{
				initResult = new InitializationResult(InitializationResult.INIT_CONFIRM_AUTHENTICATION, tokenBundle);
			}
			else
			{
				initResult = new InitializationResult(InitializationResult.INIT_SESSION_LOADED, tokenBundle);

			}
			return initResult;
		}
		catch (OperationCanceledException | AuthenticatorException | IOException e)
		{
			e.printStackTrace();
			return new InitializationResult(InitializationResult.INIT_ERROR_OCCURED, null, e);
		}
	}

	private InitializationResult addNewAccount()
	{
		AccountManager accountManager = AccountManager.get(mContext);

		AccountManagerFuture<Bundle> future = accountManager.addAccount(AccountConstants.ACCOUNT_TYPE_WORKPLACE,
				AccountConstants.TOKEN_TYPE_CCS, null, AuthHelper.buildConfigBundle(1, 1), null, null, null);

		try
		{
			return new InitializationResult(InitializationResult.INIT_ADD_NEW_ACCOUNT, future.getResult());
		}
		catch (OperationCanceledException | AuthenticatorException | IOException e)
		{
			e.printStackTrace();
			return new InitializationResult(InitializationResult.INIT_ERROR_OCCURED, null, e);
		}
	}
}
