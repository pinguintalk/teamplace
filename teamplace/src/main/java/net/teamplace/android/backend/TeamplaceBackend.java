package net.teamplace.android.backend;

import net.teamplace.android.http.request.BackendConfiguration;

/**
 * Created by jobol on 02/06/15.
 */
public class TeamplaceBackend implements BackendConfiguration, BackendServers
{
    @Override
    public String accountManagement() {
        return ACCOUNT;
    }

    @Override
    public String workplaceServer() {
        return WORKPLACE;
    }

    @Override
    public String teamDriveApi() {
        return TEAMDRIVE_API;
    }

    @Override
    public String activityApi() {
        return ACTIVITY_API;
    }

    @Override
    public String quotaApi() {
        return QUOTA_API;
    }

    @Override
    public String registration() {
        return REGISTRATION;
    }

    @Override
    public String forgotPassword() {
        return FORGOT_PASSWORD;
    }

    @Override
    public String userProfile() {
        return PROFILE;
    }
}
