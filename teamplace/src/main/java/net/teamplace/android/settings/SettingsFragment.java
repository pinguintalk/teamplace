package net.teamplace.android.settings;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.cortado.android.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import net.teamplace.android.backend.BackendServers;
import net.teamplace.android.backend.ProductFlavor;
import net.teamplace.android.browsing.BrowsingActivity;
import net.teamplace.android.browsing.sorting.SortManager;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.Profile;
import net.teamplace.android.initialization.TutorialActivity;
import net.teamplace.android.services.ProfileService;
import net.teamplace.android.utils.ResetHelper;

import java.io.IOException;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.tag;

public class SettingsFragment extends Fragment
{
	private final String TAG = tag(SettingsFragment.class);
	private final String SETTING_PREFERENCES = SettingsFragment.class.getName() +  " SETTING_PREFERENCES";
	private final String PREF_KEY_USER_ID =  "PREF_KEY_USER_ID";

	private int mProfileResquestId;
	private TextView userDisplayName;
	private ImageView userImageView;
	private DisplayImageOptions mImageOptions;

	public static SettingsFragment newInstance()
	{
		return new SettingsFragment();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View layout = inflater.inflate(R.layout.set_fragment, container, false);


		mImageOptions = new DisplayImageOptions.Builder()
				.showImageOnFail(getResources().getDrawable(R.drawable.set_user_account)).cacheOnDisk(true).build();

		String userMail = new SessionManager(getActivity()).getLastSession().getAccount().name;
		userImageView = (ImageView) layout.findViewById(R.id.iv_user_image);

		userDisplayName = (TextView) layout.findViewById(R.id.tv_user_displayname);
		userDisplayName.setText(userMail);
		userDisplayName.setSelected(true);

		layout.findViewById(R.id.tv_setting_tutorial).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), TutorialActivity.class);
				startActivity(i);
			}
		});

		layout.findViewById(R.id.tv_setting_faq).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(getActivity(), FaqActivity.class);
				startActivity(i);
			}
		});

		layout.findViewById(R.id.tv_manage_account).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(BackendServers.ACCOUNT));
				getActivity().startActivity(i);
			}
		});

		layout.findViewById(R.id.tv_setting_reset).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				new ResetDialogFragment().show(getFragmentManager(), "reset");
			}
		});

		layout.findViewById(R.id.tv_setting_eula).setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				DialogFragment dialog = EulaAndAboutDialogFragment
						.newInstance(EulaAndAboutDialogFragment.TYPE_EULA);
				dialog.show(getFragmentManager(), "eula");
			}
		});

		layout.findViewById(R.id.tv_setting_about).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				DialogFragment dialog = EulaAndAboutDialogFragment
						.newInstance(EulaAndAboutDialogFragment.TYPE_ABOUT);
				dialog.show(getFragmentManager(), "about");
			}
		});

		layout.findViewById(R.id.tv_setting_logout).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				ResetHelper.logout(getActivity());
			}
		});

		PackageInfo pinfo = null;
		try {
			pinfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
		} catch (PackageManager.NameNotFoundException e) {
			e.printStackTrace();
		}
		String versionName = pinfo.versionName;
		String copyRightText = getResources().getString(R.string.set_copy_rights) + " " + versionName + " " + ProductFlavor.BUILD_VARIANT;
		((TextView) layout.findViewById(R.id.tv_copy_right)).setText(copyRightText);

		layout.findViewById(R.id.tv_setting_reset_sort_prefs).setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				SortManager.resetSortPreferences(getActivity());
				Toast.makeText(getActivity(), R.string.set_reset_sort_prefs_toast, Toast.LENGTH_LONG).show();
			}
		});



		return layout;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		EventBus.getDefault().register(this);
	}

	@Override
	public void onDetach()
	{
		super.onDetach();
		EventBus.getDefault().unregister(this);
	}


	@Override
	public void onResume()
	{
		super.onResume();

		SharedPreferences preferences = this.getActivity().getSharedPreferences(SETTING_PREFERENCES, 0);

		mProfileResquestId = IdGenerator.getId();
		ProfileService.getProfile(getActivity(), mProfileResquestId);

		String currUserId = preferences.getString(PREF_KEY_USER_ID, "");
		if (!TextUtils.isEmpty(currUserId)) {

			ImageLoader.getInstance().displayImage(ProfileService.getProfileImageUrl(currUserId), userImageView, mImageOptions);
		}

		syncActionBar();
	}

	private void syncActionBar()
	{
		Activity act = getActivity();
		if (!(act instanceof BrowsingActivity))
		{
			return;
		}

		BrowsingActivity browsingActivity = (BrowsingActivity) act;
		browsingActivity.setActionBarTitle(R.string.abt_settings);
		browsingActivity.setActionBarSubtitle(null);
	}

	public void onEventMainThread(ProfileService.ProfileServiceResponse event) {
		long responseCode = event.getRequestCode();
		if (responseCode == 0) return;

		if (responseCode == mProfileResquestId)
		{
			Profile profile = null;
			try {
				profile = (Profile) event.getResponseData();
			} catch (CortadoException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (profile != null) {
				String currUserId = profile.getUserId();
				SharedPreferences preferences = this.getActivity().getSharedPreferences(SETTING_PREFERENCES, 0);

				if (!preferences.getString(PREF_KEY_USER_ID,"").equals(currUserId)) {

					SharedPreferences.Editor editor = preferences.edit();
					editor.putString(PREF_KEY_USER_ID, currUserId).commit();

					ImageLoader.getInstance().displayImage(ProfileService.getProfileImageUrl(profile.getUserId()), userImageView, mImageOptions);
				}
			}
		}

	}
}
