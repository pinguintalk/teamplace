package net.teamplace.android.settings;

import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import net.teamplace.android.utils.LinereaderHelper;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;

import com.cortado.android.R;

public class EulaAndAboutDialogFragment extends DialogFragment
{
	private static final String DIALOG_TYPE = "dialogType";
	public static final int TYPE_EULA = 1;
	public static final int TYPE_ABOUT = 2;

	private WebView wvAbout;

	public static DialogFragment newInstance(int type)
	{
		if (type != TYPE_EULA && type != TYPE_ABOUT)
		{
			throw new IllegalArgumentException("Dialog type was not EULA or ABOUT");
		}

		DialogFragment fragment = new EulaAndAboutDialogFragment();

		Bundle arguments = new Bundle();
		arguments.putInt(DIALOG_TYPE, type);

		fragment.setArguments(arguments);

		return fragment;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Activity context = getActivity();
		int type = getArguments().getInt(DIALOG_TYPE);

		final StyleableAlertDialog dialog = new StyleableAlertDialog(context);
		LayoutInflater inflater = context.getLayoutInflater();

		View dialogView = inflater.inflate(R.layout.set_about_dialog, null);

		dialog.setView(dialogView);

		if (type == TYPE_EULA)
		{
			dialog.setTitle("EULA");
		}
		else
		{
			dialog.setTitle("About");
		}

		dialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getText(R.string.app_ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dismiss();
					}
				});

		wvAbout = (WebView) dialogView.findViewById(R.id.wv_about_dialog);

		new LoadAboutAsyncTask().execute(type);

		return dialog;
	}

	@Override
	public void onResume()
	{
		super.onResume();

	}

	private class LoadAboutAsyncTask extends AsyncTask<Integer, Void, String>
	{
		@Override
		protected String doInBackground(Integer... params)
		{
			int resourceToLoad;

			if (params[0] == TYPE_EULA)
			{
				resourceToLoad = R.raw.eula_workplace;
			}
			else
			{
				resourceToLoad = R.raw.thirdpartylicense;
			}

			return LinereaderHelper.load(getActivity().getResources().openRawResource(resourceToLoad));
		}

		@Override
		protected void onPostExecute(String result)
		{
			wvAbout.getSettings().setJavaScriptEnabled(false);
			wvAbout.loadDataWithBaseURL(null, result, "text/html", "utf-8", null);
		}
	}
}
