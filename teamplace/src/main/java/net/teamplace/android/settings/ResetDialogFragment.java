package net.teamplace.android.settings;

import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import net.teamplace.android.initialization.InitializationActivity;
import net.teamplace.android.initialization.InitializationLoader;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.cortado.android.R;

public class ResetDialogFragment extends DialogFragment
{
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		StyleableAlertDialog dialog = new StyleableAlertDialog(getActivity());
		dialog.setTitle(R.string.set_reset);
		dialog.setMessage(getString(R.string.set_reset_dialog_content));

		dialog.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.app_ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						// new ResetAsyncTask().execute();
						killAndRequestResetOnRestart();
					}
				});

		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.app_cancel),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						dismiss();
					}
				});

		return dialog;
	}

	// TODO (jobol): This is a hacky workaround for the following problem when deleting the databases before app exit:
	// Data might still be written after the delete, e.g. due to a delayed network request. This may either cause
	// crashes or the app restarting with unwanted database content. So this method kills the app and restarts it with a
	// flag to delete the databases first thing after start. We should implement proper database synchronization ASAP
	// and remove this hack.
	private void killAndRequestResetOnRestart()
	{
		Intent intent = new Intent(getActivity(), InitializationActivity.class);
		intent.putExtra(InitializationLoader.OPTION_RESET, true);
		intent.setAction(Intent.ACTION_DEFAULT);
		AlarmManager alMgr = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
		alMgr.set(AlarmManager.RTC, 100,
				PendingIntent.getActivity(getActivity(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));
		System.exit(0);
	}

	// private class ResetAsyncTask extends AsyncTask<Void, Void, Void>
	// {
	// private final Context context = getActivity();
	//
	// @Override
	// protected Void doInBackground(Void... params)
	// {
	// try
	// {
	// SessionManager sessionManager = new SessionManager(context);
	// AccountManager accountManager = AccountManager.get(context);
	//
	// Account account = sessionManager.getLastSession().getAccount();
	// accountManager.removeAccount(account, null, null).getResult();
	//
	// ResetHelper.resetAllCaches(context);
	// }
	// catch (OperationCanceledException e)
	// {
	// e.printStackTrace();
	// }
	// catch (AuthenticatorException e)
	// {
	// e.printStackTrace();
	// }
	// catch (IOException e)
	// {
	// e.printStackTrace();
	// }
	//
	// return null;
	// }
	//
	// @Override
	// protected void onPostExecute(Void result)
	// {
	// context.startActivity(InitializationActivity.getIntent(context, false));
	// AlarmManager alMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
	// Intent intent = new Intent(context, InitializationActivity.class);
	// intent.putExtra(InitializationLoader.OPTION_RESET, true);
	// alMgr.set(AlarmManager.RTC, 500, PendingIntent.getActivity(context, 0, intent, 0));
	// ((Activity) context).finish();
	// System.exit(0);
	// }
	// }
}
