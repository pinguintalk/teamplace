package net.teamplace.android.preview.utils;

import java.util.Iterator;
import java.util.Stack;

/**
 * Stack that guarantees uniqueness of its elements. On a push, if the element is already present, it is moved on top
 * 
 * @author Gil Vegliach
 */
public class ReorderingStack<E>
{
	private final Stack<E> mStack;

	public ReorderingStack()
	{
		mStack = new Stack<E>();
	}

	public void clear()
	{
		mStack.clear();
	}

	/**
	 * Pushes an element if not present, otherwise pull it on the top of the stack. Returns true iff the element was
	 * already present in the stack
	 * 
	 * @throws NullPointerException
	 *             if elem is null
	 */
	public boolean pushOrReorder(E elem)
	{
		if (elem == null)
			throw new NullPointerException("ReordedingStack does not support null elements");

		boolean result = removeAll(elem);
		mStack.push(elem);
		return result;
	}

	/** Pops an element out of the stack */
	public E pop()
	{
		return mStack.pop();
	}

	/** Returns whether this stack is empty or not */
	public boolean isEmpty()
	{
		return mStack.empty();
	}

	private boolean removeAll(E elem)
	{
		boolean removed = false;
		for (Iterator<E> i = mStack.iterator(); i.hasNext();)
		{
			E curr = i.next();
			if (elem.equals(curr))
			{
				i.remove();
				removed = true;
			}
		}

		return removed;
	}
}
