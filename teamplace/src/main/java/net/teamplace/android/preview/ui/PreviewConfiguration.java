package net.teamplace.android.preview.ui;

import android.os.Parcel;
import android.os.Parcelable;

import net.teamplace.android.browsing.FileInfo;

import java.util.ArrayList;

/**
 * Created by jobol on 06/07/15.
 */
public class PreviewConfiguration implements Parcelable {

    private ArrayList<FileInfo> mFolderContent;
    private String mOpenVersionId;
    private String mOpenCommentId;

    public PreviewConfiguration() {

    }

    public PreviewConfiguration folderContent(ArrayList<FileInfo> folderContent) {
        mFolderContent = folderContent;
        return this;
    }

    public PreviewConfiguration openVersion(String versionId) {
        mOpenVersionId = makeEmptyStringNull(versionId);
        return this;
    }

    public PreviewConfiguration openComment(String commentId) {
        mOpenCommentId = makeEmptyStringNull(commentId);
        return this;
    }


    public ArrayList<FileInfo> getFolderContent() {
        return mFolderContent;
    }

    public String getOpenVersionId () {
        return mOpenVersionId;
    }

    public String getOpenCommentId() {
        return mOpenCommentId;
    }

    public String makeEmptyStringNull(String in) {
        if (in != null && in.length() == 0) {
            in = null;
        }
        return in;
    }

    public PreviewConfiguration(Parcel in) {
        mFolderContent = new ArrayList<>();
        in.readTypedList(mFolderContent, FileInfo.CREATOR);
        mOpenVersionId = in.readString();
        mOpenCommentId = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(mFolderContent);
        dest.writeString(mOpenVersionId);
        dest.writeString(mOpenCommentId);
    }

    public static final Creator<PreviewConfiguration> CREATOR = new Creator<PreviewConfiguration>() {
        @Override
        public PreviewConfiguration createFromParcel(Parcel source) {
            return new PreviewConfiguration(source);
        }

        @Override
        public PreviewConfiguration[] newArray(int size) {
            return new PreviewConfiguration[size];
        }
    };
}
