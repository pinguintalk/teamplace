package net.teamplace.android.preview.widget;

/**
 * Provides a DocumentView with DocumentPages the total count of pages
 * 
 * @author Gil Vegliach
 */
public interface DocumentAdapter
{
	/** Returns how many pages the adapter knows of (they can be still loading) */
	int getCount();

	/** Returns a page given its position in the dataset */
	DocumentPage getPage(int position);
}
