package net.teamplace.android.preview.widget;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.utils.GenericUtils;

import java.util.ArrayList;
import java.util.Collections;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Bridges together version data and item views in {@link net.teamplace.android.preview.widget.PreviewFooterView
 * PreviewFooterView}
 * 
 * @author Gil Vegliach
 */
class VersionsAdapter extends BaseAdapter
{
	@SuppressWarnings("unused")
	private static final String TAG = tag(VersionsAdapter.class);

	static final int MODE_SELECTED_VERSION = 0;
	static final int MODE_ALL_VERSIONS = 1;
	private int mMode = MODE_ALL_VERSIONS;

	private final Context mContext;
	private final ArrayList<VersionItem> mItems;

	// team drive id for prewie of file in teamplace
	private  String mTeamDriveId;

	private int mSelectedPosition = -1;
	private PreviewFooterView mFooterView;

	/** Constructs a VersionsAdapter. The List items must have at least two elements (versions) */
	VersionsAdapter(Context context, ArrayList<VersionItem> items, boolean markLastModifiedAsCurrent, String teamDriveid)
	{
		checkNonNullArgs(context, items);
		checkCondition(items.size() > 1);
		mContext = context;
		Collections.sort(items, VersionItem.LAST_EDIT_COMPARATOR);
		mItems = items;
		mTeamDriveId = teamDriveid;
		//String teamdriveId = items.get(0).

		if (markLastModifiedAsCurrent)
		{
			// Ensures the first item (and only the first) is marked as current and selected
			for (VersionItem item : items)
			{
				item.setCurrent(false);
				//Log.d("VERSION", item.toString());
			}
			VersionItem item = items.get(0);
			item.setCurrent(true);
			selectCurrentInDataset();
		}
	}

	@Override
	public boolean hasStableIds()
	{
		return true;
	}

	@Override
	public int getCount()
	{
		if (mMode == MODE_SELECTED_VERSION)
		{
			return 1;
		}

		return mItems.size();
	}

	@Override
	public Object getItem(int position)
	{
		checkPositionOrThrow(position);

		// In selected view, return the selected item for all positions
		if (mMode == MODE_SELECTED_VERSION)
		{
			position = mSelectedPosition;
		}

		return mItems.get(position);
	}

	/** Ids are stable and equal to positions */
	@Override
	public long getItemId(int position)
	{
		checkPositionOrThrow(position);

		if (mMode == MODE_SELECTED_VERSION)
		{
			return mSelectedPosition;
		}

		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		final VersionItem item = (VersionItem) getItem(position);

		ViewHolder holder = new ViewHolder();

		View v = convertView;
		if (v == null)
		{
			LayoutInflater inflater = LayoutInflater.from(mContext);

			v = inflater.inflate(R.layout.prv_version_item, parent, false);

			holder.versionView = (VersionView) v.findViewById(R.id.icon);
			holder.versionLabel = (TextView) v.findViewById(R.id.version_label);
			holder.versionDate = (TextView) v.findViewById(R.id.version_date);
			holder.editorName = (TextView) v.findViewById(R.id.editor_name);

			v.setTag(holder);

		}
		else {
			holder = (ViewHolder) v.getTag();
		}

		View divider = v.findViewById(R.id.divider);

		divider.setVisibility(item.isCurrent() ? View.INVISIBLE : View.VISIBLE);

		holder.versionView.setCount(item.getVersionNumber());
		holder.versionView.setAlpha(.5f);

		if (item.isCurrent())
		{
			holder.versionLabel.setText(R.string.prv_version_item_version_label_current);
			holder.versionDate.setText("");
		} else {
			holder.versionLabel.setText(R.string.prv_version_item_version_label_other);
			String s = GenericUtils.formatDate(mContext, item.getLastEditDate());
			holder.versionDate.setText(s);
		}

		TeamDrive teamDrive = new SessionManager(mContext).getLastSession().getTeamDriveMap().get(mTeamDriveId);
		User[] users =  teamDrive.getUsers();
		for (User user: users) {
			if(user.getEmail().equals(item.getEditor())) {
				holder.editorName.setText(getDisplayName(user));
				break;
			}
		}

		if (item.isSelected()) {
			holder.versionView.setAlpha(1f);
		}

		final int selectedPos = position;
		View.OnClickListener listener = new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				mFooterView.onVersionClicked(selectedPos);
			}
		};
		v.setOnClickListener(listener);

		return v;
	}

	private static String getDisplayName(User user)
	{
		if (user == null)
		{
			return "";
		}
		String displayName = user.getDisplayName();
		if (displayName == null || displayName.length() == 0)
		{
			displayName = user.getEmail();
		}
		return displayName;
	}

	void setFooterView(PreviewFooterView v)
	{
		mFooterView = v;
	}

	void selectCurrentInDataset()
	{
		for (VersionItem item : mItems)
		{
			item.setSelected(item.isCurrent());
		}
	}

	/** Finds the selected position in the dataset or returns -1 if there is none */
	int findSelectedInDataset()
	{
		if (mItems == null || mItems.size() == 0)
		{
			return -1;
		}

		int size = mItems.size();
		for (int i = 0; i < size; i++)
		{
			if (mItems.get(i).isSelected())
			{
				return i;
			}
		}

		return -1;
	}

	/** Moves the checkbox in the dataset. The mode won't change */
	void selectInDataset(int pos)
	{
		int size = mItems.size();
		for (int i = 0; i < size; i++)
		{
			mItems.get(i).setSelected(i == pos);
		}
		notifyDataSetChanged();
	}

	/** Returns whether the current version is selected in the dataset */
	boolean isCurrentSelectedInDataset()
	{
		for (VersionItem item : mItems)
		{
			if (item.isCurrent())
			{
				return item.isSelected();
			}

			if (item.isSelected())
			{
				return item.isCurrent();
			}
		}
		return false;
	}

	/**
	 * Returns which mode the adapter is in. It can be {@link #MODE_ALL_VERSIONS} or {@link #MODE_SELECTED_VERSION}
	 */
	int getMode()
	{
		return mMode;
	}

	/**
	 * After this call, the adapter will expose only the selected version from the dataset. Call
	 * {@link #selectInDataset(int)} or {@link #selectCurrentInDataset()} before this method
	 */
	void selectedMode()
	{
		mSelectedPosition = findSelectedInDataset();
		mMode = MODE_SELECTED_VERSION;
		notifyDataSetChanged();
	}

	/**
	 * After this call, the adapter will expose the whole list of versions from the dataset
	 */
	void allVersionsMode()
	{
		mSelectedPosition = -1;
		mMode = MODE_ALL_VERSIONS;
		notifyDataSetChanged();
	}

	ArrayList<VersionItem> getAllItems()
	{
		return mItems;
	}

	private void checkPositionOrThrow(int position)
	{
		if (position < 0 || position >= mItems.size())
		{
			throw new IllegalArgumentException("Position must be between 0 and (dataset size - 1)");
		}
	}

	static class ViewHolder
	{
		VersionView versionView;
		TextView versionLabel;
		TextView versionDate;
		TextView editorName;
	}
}
