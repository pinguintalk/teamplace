package net.teamplace.android.preview.ui.controller;

import android.animation.Animator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cortado.android.R;

import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.preview.PreviewService;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.preview.widget.PreviewFooterView;
import net.teamplace.android.preview.widget.PreviewFooterView.OnVersionClickListener;
import net.teamplace.android.preview.widget.VersionItem;
import net.teamplace.android.utils.Log;

import java.util.ArrayList;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

public class BasicVersionsController implements VersionsController, OnVersionClickListener
{

	public static final String TAG = tag(BasicVersionsController.class);
	private static final String PKG = pkg(BasicVersionsController.class);

	private static final String KEY_CURR_REQUESTS = PKG + ".currRequests";
	private static final String KEY_REQUESTED_BASE_PATH = PKG + ".requestedBasePath";

	private PreviewHolderFragment mFragment;

	private View mFragmentView;
	private PreviewFooterView mFooterView;
	private int mVersionNumberOfMasterDoc;

	@Override
	public int getmVersionNumberOfMasterDoc() {
		return mVersionNumberOfMasterDoc;
	}


	/** Ids of requests in progress */
	private final ArrayList<Integer> mCurrRequests = new ArrayList<Integer>();

	private final BroadcastReceiver mResponseReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	public BasicVersionsController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	@Override
	public void setFragmentView(View v)
	{
		mFragmentView = v;
	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {

	}

	@Override
	public void onUiUpdateRequested()
	{
		syncFooterView();
		// clearVersions();
		// versionsRequest();
	}

	@Override
	public void onReloadVersionsRequested()
	{
		versionsRequest();
	}

	@Override
	public void onHideOverlaysRequested()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onShowOverlaysRequested()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public int getOverlayState()
	{
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO: @giveg this is probably the wrong place, move it
		if (savedInstanceState == null)
		{
			versionsRequest();
		}

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mFooterView = (PreviewFooterView) mFragmentView.findViewById(R.id.footer_view);
		mFooterView.setOnVersionClickListener(this);

		if (savedInstanceState != null)
		{
			ArrayList<Integer> requestIds = savedInstanceState.getIntegerArrayList(KEY_CURR_REQUESTS);
			if (requestIds != null)
			{
				mCurrRequests.addAll(requestIds);
			}
		}
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		Activity act = mFragment.getActivity();
		LocalBroadcastManager.getInstance(act).unregisterReceiver(mResponseReceiver);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		Activity act = mFragment.getActivity();
		IntentFilter filter = new IntentFilter(PreviewService.Contract.ACTION_RESPONSE_VERSIONS);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_DELETE);
		LocalBroadcastManager.getInstance(act).registerReceiver(mResponseReceiver, filter);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putIntegerArrayList(KEY_CURR_REQUESTS, mCurrRequests);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	private void versionsRequest()
	{
		Path basePath = mFragment.getFileInfoController().getMasterDocument().getPath();
		// request version only if file on teamdrive
		if (Path.isTeamDriveLocation(basePath.getLocation())) {
			Activity act = mFragment.getActivity();
			Intent i = PreviewService.Contract.buildRequestVersionsIntent(act, basePath);
			i.putExtra(KEY_REQUESTED_BASE_PATH, basePath);
			mCurrRequests.add(PreviewService.Contract.extractRequestId(i));
			act.startService(i);
		}
	}

	private void onResponseReceived(Intent intent)
	{
		Log.d(TAG, "onResponseReceived() [1], intent=" + intent);

		String action = intent.getAction();
		if (PreviewService.Contract.ACTION_RESPONSE_VERSIONS.equals(action))
		{
			Path requestedBasePath = intent.getExtras().getBundle(PreviewService.Contract.RESPONSE_REQUEST_EXTRAS)
					.getParcelable(KEY_REQUESTED_BASE_PATH);
			Path displayedBasePath = mFragment.getFileInfoController().getMasterDocument().getPath();
			if (displayedBasePath.equals(requestedBasePath))
			{
				onResponseVersions(intent);
			}
		}
		else if (BrowsingService.Contract.ACTION_RESPONSE_DELETE.equals(action))
		{
			// We have completed deleting a version
			onResponseDeleteVersion();
		}
	}

	private void onResponseVersions(Intent intent)
	{
		ArrayList<VersionItem> versions = intent
				.getParcelableArrayListExtra(PreviewService.Contract.RESPONSE_VERSIONS_LIST);
		if (versions == null) return; // in case of no internet connection
		// get version number of master document
		for(VersionItem versionItem:versions) {
			if(!versionItem.getFullName().contains(":VER_")) {
				mVersionNumberOfMasterDoc = versionItem.getVersionNumber();
				break;
			}
		}

		Path currPath = mFragment.getFileInfoController().getDisplayingPath().getParentPath();

		mFooterView.setVersionItems(versions,((Path.TeamDrive) currPath).getTeamDriveId());

		int pos = mFooterView.getPosition(mFragment.getFileInfoController().getDisplayingPath());
		if (pos >= 0)
		{
			mFooterView.selectPosition(pos);
			mFragment.getPreviewContentController().onContentChangeRequested(ContentController.REQUEST_VERSION_CHANGED);
		}
		mFragment.getActivity().invalidateOptionsMenu();
	}

	@Override
	public void onVersionClicked(VersionItem version)
	{
		Path displayingPath = mFragment.getFileInfoController().getDisplayingPath();
		FileInfo masterDocument = mFragment.getFileInfoController().getMasterDocument();
		// If we are preview this versions, nothing to do
		Path newPath = PathUtils.buildRenamedPath(displayingPath, version.getFullName());
		if (newPath.equals(displayingPath))
		{
			return;
		}
		mFragment.getFileInfoController().setDisplayingPath(newPath);
		mFragment.getFileInfoController().setDisplayingDate(newPath.equals(masterDocument)
				? masterDocument.getLastModifiedDate()
				: version.getLastEditDate());
		// rebuildAdapter();
		// mDocumentView.setAdapter(mAdapter);
		mFragment.getPreviewContentController().onContentChangeRequested(ContentController.REQUEST_VERSION_CHANGED);
		mFragment.getActivity().invalidateOptionsMenu();
	}

	private void syncFooterView()
	{
		FileInfo masterDoc = mFragment.getFileInfoController().getMasterDocument();
		mFooterView.setDocumentName(masterDoc.getName());
		mFooterView.setDocumentLastModifiedDate(masterDoc.getLastModifiedDate());
		mFooterView.setDocumentSize(masterDoc.getSize());
		mFooterView.setDocumentIcon(masterDoc.getIconResId());
	}

	@Override
	public void clearVersionUi()
	{
		mFooterView.setVersionItems(null, null);
		// rebuildAdapter();
		// mDocumentView.setAdapter(mAdapter);
		// mFragment.getPreviewContentController().onContentChangeRequested(ContentController.REQUEST_VERSION_CHANGED);
	}

	private void onResponseDeleteVersion()
	{
		// Refresh
		versionsRequest();
		Toast.makeText(mFragment.getActivity(), R.string.prv_delete_version_done, Toast.LENGTH_SHORT).show();
	}

}
