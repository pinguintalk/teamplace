package net.teamplace.android.preview;

import net.teamplace.android.preview.PreviewService.PreviewServiceBinder;
import android.content.ServiceConnection;


/**
 * Interface for notifying when PreviewService has connected and disconnected and finished loading a page and/or total
 * page count.
 * 
 * @see ServiceConnection
 * @author Gil Vegliach
 */
public interface PreviewServiceConnectionListener
{
	/** Called when a client binds to PreviewService */
	void onPreviewServiceConnected(PreviewServiceBinder service);

	/**
	 * Called both when a client unbinds from PreviewService or when PreviewService has crashed or has been killed
	 * (unlikely ServiceConnection)
	 */
	void onPreviewServiceDisconnected();

	/** Called when a load has finished (page and/or total page count) */
	void onPreviewLoadFinished(int loadedPagePosition, int totalPageNumber);
}