package net.teamplace.android.preview.widget;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

/**
 * Abstract representation of a version of a file in the UI
 * 
 * @author Gil Vegliach
 */
public class VersionItem implements Parcelable
{
	static final LastEditComparator LAST_EDIT_COMPARATOR = new LastEditComparator();

	private boolean mIsSelected;
	private boolean mIsCurrent;

	/** Filename with version, e.g. test.txt:VER_0000. It does not include the path */
	private final String mFullName;
	private final int mVersionNumber;
	private final long mSize;
	private final long mLastEditDate;
	private final String mEditor;
	private final Uri mPhotoUri;


	public VersionItem(String fullName, int mVersionNumber, long size, long lastEditDate, String editor, Uri photoUri)
	{
		this.mVersionNumber = mVersionNumber;
		mIsSelected = false;
		mIsCurrent = false;
		mFullName = fullName;
		mSize = size;
		mLastEditDate = lastEditDate;
		mEditor = editor;
		mPhotoUri = photoUri;
	}

	public VersionItem(String fullName, int mVersionNumber,long size, long lastEditDate, String editor)
	{
		this(fullName, mVersionNumber, size, lastEditDate, editor, null);
	}

	void setSelected(boolean isSelected)
	{
		mIsSelected = isSelected;
	}

	boolean isSelected()
	{
		return mIsSelected;
	}

	void setCurrent(boolean isCurrent)
	{
		mIsCurrent = isCurrent;
	}

	boolean isCurrent()
	{
		return mIsCurrent;
	}

	public String getFullName()
	{
		return mFullName;
	}

	public long getSize()
	{
		return mSize;
	}

	public long getLastEditDate()
	{
		return mLastEditDate;
	}

	public String getEditor()
	{
		return mEditor;
	}

	public Uri getPhotoUri()
	{
		return mPhotoUri;
	}

	public int getVersionNumber() {
		return mVersionNumber;
	}


	static class LastEditComparator implements Comparator<VersionItem>
	{
		@Override
		public int compare(VersionItem item1, VersionItem item2)
		{
			// Last modified first
			//return (int) (item2.mLastEditDate - item1.mLastEditDate);
			// TEP-1779
			if(item1.mLastEditDate > item2.mLastEditDate) return -1;
			if(item1.mLastEditDate == item2.mLastEditDate) return 0;
			return 1;

			//return Long.compare(item2.mLastEditDate,item1.mLastEditDate);
		}
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeInt(mIsSelected ? 1 : 0);
		dest.writeInt(mIsCurrent ? 1 : 0);
		dest.writeString(mFullName);
		dest.writeInt(mVersionNumber);
		dest.writeLong(mSize);
		dest.writeLong(mLastEditDate);
		dest.writeString(mEditor);

	}

	@Override
	public String toString()
	{
		return "VersionItem{mIsSelected=" + mIsSelected + ", mIsCurrent=" + mIsCurrent + ", mFullName=" + mFullName
				+ "versionNumber " + mVersionNumber + ", mSize=" + mSize + ", mLastEditDate=" + mLastEditDate + ", mEditor=" + mEditor + ", mPhotoUri="
				+ mPhotoUri + "}";
	}

	public static final Parcelable.Creator<VersionItem> CREATOR = new Parcelable.Creator<VersionItem>()
	{
		@Override
		public VersionItem createFromParcel(Parcel source)
		{
			boolean isSelected = source.readInt() == 1;
			boolean isCurrent = source.readInt() == 1;
			String fullName = source.readString();
			int versionNumber = source.readInt();
			long size = source.readLong();
			long lastEdit = source.readLong();
			String editor = source.readString();
			VersionItem item = new VersionItem(fullName, versionNumber, size, lastEdit, editor);
			item.setSelected(isSelected);
			item.setCurrent(isCurrent);
			return item;
		}

		@Override
		public VersionItem[] newArray(int size)
		{
			return new VersionItem[size];
		}
	};
}
