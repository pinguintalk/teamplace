package net.teamplace.android.preview.ui;

import android.content.Context;
import android.widget.RemoteViews;

import com.cortado.android.R;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.GenericUtils;

import java.util.List;

public class PreviewFragmentFactory
{
	public static PreviewHolderFragment create(Context ctx, FileInfo info, PreviewConfiguration configuration)
	{
		PreviewHolderFragment fragment;
		Path path = info.getPath();

		// video: local / remote video files
		// image: local / remote image files
		// tpm3: remote supportes files, except images
		// open in: local non images / videos, remote unsupported files
		String ext = GenericUtils.getExtension(path.getName());
		int type = FileInfo.getTypeFromExtension(ext);
		List<String> tpm3Supported = new SessionManager(ctx).getLastSession().getConfiguration()
				.getSystemConfiguration().getServerAcceptedFileExtensionsAsList();

		if (type == FileInfo.IMAGE)
		{
			fragment = ImagePreviewFragment.newInstance(info, configuration);
		}
		else if (type == FileInfo.VIDEO)
		{
			RemoteViews rv = new RemoteViews(ctx.getApplicationContext().getPackageName(),
					R.layout.prv_static_media);
			fragment = StaticContentPreviewFragment.newInstance(info, configuration, rv,
					OnStaticContentClickedHandler.VideoHandler.TYPE);
		}
		else if (type == FileInfo.AUDIO)
		{
			// because most external apps can't handle https url of mp3 file, we have to open audio file like share action

			RemoteViews rv = new RemoteViews(ctx.getApplicationContext().getPackageName(),
					R.layout.prv_static_media);
			fragment = StaticContentPreviewFragment.newInstance(info, configuration, rv,
					OnStaticContentClickedHandler.AudioHandler.TYPE);
		}
		else if (!Path.isLocalLocation(path.getLocation()) && tpm3Supported.contains(ext))
		{
			// for other supported file format, show content in preview
			fragment = Tpm3PreviewFragment.newInstance(info, configuration);
		}
		else
		{
			// other not supported formats, open in extern apps.
			RemoteViews rv = new RemoteViews(ctx.getApplicationContext().getPackageName(),
					R.layout.prv_static_not_supported);
			fragment = StaticContentPreviewFragment.newInstance(info, configuration, rv,
					OnStaticContentClickedHandler.OpenInHandler.TYPE);
		}
		return fragment;
	}
}
