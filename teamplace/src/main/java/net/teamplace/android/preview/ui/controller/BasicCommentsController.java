package net.teamplace.android.preview.ui.controller;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.DrawerLayout.DrawerListener;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.Comment;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.preview.BackPressManager;
import net.teamplace.android.preview.comment.AddCommentIntentService;
import net.teamplace.android.preview.comment.CommentIntentHelper;
import net.teamplace.android.preview.comment.CommentOperationCallback;
import net.teamplace.android.preview.comment.CommentOperationResponse;
import net.teamplace.android.preview.comment.CommentsAdapter;
import net.teamplace.android.preview.comment.DeleteCommentIntentService;
import net.teamplace.android.preview.comment.LoadCommentsIntentService;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.preview.widget.ShadowLayout;
import net.teamplace.android.teamdrive.GetUserImageListener;
import net.teamplace.android.teamdrive.ImageResultCallback;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.teamdrive.TeamDriveService.UserImageItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.emptyIfNull;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

public class BasicCommentsController implements CommentsController, DrawerListener, CommentOperationCallback,
		GetUserImageListener, BackPressManager
{

	public static final String TAG = tag(BasicCommentsController.class);
	private static final String PKG = pkg(BasicCommentsController.class);

	public static final String KEY_PATH = PKG + ".key_path";
	public static final String KEY_COMMENTS = PKG + ".key_comments";
	public static final String KEY_ALL_COMMENTS_LOADED = PKG + ".key_all_comments_loaded";
	public static final String KEY_UPDATE_IN_PROGRESS = PKG + ".key_update_in_progress";
	public static final String KEY_ADD_COMMENT_IN_PROGRESS = PKG + ".key_update_in_progress";
	public static final String KEY_COMMENTS_TO_BE_DELETED = PKG + ".key_comments_to_be_deleted";
	public static final int INITIAL_COMMENT_COUNT = 0;
	public static final int NEXT_COMMENT_COUNT = 50;
	private static final int TEAM_DRIVE_SERVICE_REQUEST_ID = 33333333;

	private PreviewHolderFragment mFragment;
	private View mFragmentView;

	private ShadowLayout mCommentsViewContainer;
	private DrawerLayout mDrawerLayout;
	private View mDrawerView;

	private Path.TeamDrive filePath;
	private TeamDrive teamdrive;
	private boolean loadFirstComments;
	private CommentsAdapter commentsAdapter;
	private ArrayList<String> commmentsToBeDeleted;
	private boolean addCommentInProgress = false;
	private final Set<String> imageDownlaodedForUsers = new HashSet<String>();
	private boolean isActivatedUser;
	private PreviewConfiguration mPreviewConfiguration;

	// Memory is not a concern here
	@SuppressLint("UseSparseArrays")
	volatile private Map<Long, ImageResultCallback> userImageCallbacks = new HashMap<Long, ImageResultCallback>();

	public BasicCommentsController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	@Override
	public void setFragmentView(View v)
	{
		mFragmentView = v;
	}

	@Override
	public void onHideOverlaysRequested()
	{
		hideCommentsView();
	}

	@Override
	public void onShowOverlaysRequested()
	{
		showCommentsView();
	}

	@Override
	public int getOverlayState()
	{
		return mDrawerLayout.isDrawerVisible(mDrawerView) ? OVERLAY_VISIBLE : OVERLAY_HIDDEN;
	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {
		mPreviewConfiguration = configuration;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{


	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		onUpdateDocument(false);
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mCommentsViewContainer = (ShadowLayout) mFragmentView.findViewById(R.id.comments_view_container);
		onCreateViewComments(savedInstanceState, mFragmentView);
		if (mPreviewConfiguration != null && mPreviewConfiguration.getOpenCommentId() != null)
		{
			expandComments();
			mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
		}
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		onPauseComments();
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		mCommentsViewContainer.showCommentCount(commentsAdapter.getCount());
		refreshComments();
		EventBus.getDefault().registerSticky(this);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		onSaveInstanceStateComments(outState);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	public void onEventMainThread(TeamDriveServiceResponse event)
	{
		if (event.getRequestCode() != TEAM_DRIVE_SERVICE_REQUEST_ID)
		{
			return;
		}
		try
		{
			UserImageItem[] responseData = (UserImageItem[]) event.getResponseData();
			if (responseData != null && responseData.length > 0)
			{
				UserImageItem imageItem = responseData[0];
				if (imageItem != null)
				{
					userImageCallbacks.get(imageItem.getRequestId()).onResult(imageItem);
				}
			}
		}
		catch (IOException | SQLException | CortadoException e)
		{
			e.printStackTrace();
		}
	}

	public void onEventMainThread(CommentOperationResponse event)
	{
		int responseCode = event.getResponseCode();
		Bundle extras = event.getExtras();

		switch (responseCode)
		{
			case LoadCommentsIntentService.RESPONSE_CODE:
			{
				showLoadedComments(event);
				break;
			}
			case AddCommentIntentService.RESPONSE_CODE:
			{
				addCommentInProgress = false;

				ImageButton addCommentImage = (ImageButton) mFragment.getView().findViewById(R.id.img_send_cmt);
				ProgressBar pbAddComment = (ProgressBar) mFragment.getView().findViewById(R.id.cmt_pb_add_comment);
				addCommentImage.setVisibility(View.VISIBLE);
				pbAddComment.setVisibility(View.INVISIBLE);

				showChangedComment(event);
				break;
			}
			case DeleteCommentIntentService.RESPONSE_CODE:
			{
				String commentId = extras.getString(CommentIntentHelper.EXTRA_COMMENT_ID);
				if (commentId != null)
				{
					commmentsToBeDeleted.remove(commentId);
				}

				showChangedComment(event);
				break;
			}
		}

		mCommentsViewContainer.showCommentCount(commentsAdapter.getCount());

		EventBus.getDefault().removeStickyEvent(event);
	}


	@Override
	public void onUpdateDocument()
	{
		onUpdateDocument(true);
	}

	private void onUpdateDocument(boolean ignoreSavedState)
	{
		loadFirstComments = ignoreSavedState;

		SessionManager manager = new SessionManager(mFragment.getActivity());
		Session session = manager.getLastSession();

		if (mFragment.getFileInfoController().getMasterDocument().getPath() instanceof Path.TeamDrive)
		{
			filePath = (Path.TeamDrive) mFragment.getFileInfoController().getMasterDocument().getPath();
			teamdrive = session.getTeamDriveMap().get(filePath.getTeamDriveId());
			GlobalConfiguration globalConfig = session.getTeamDriveGlobalConfig();
			isActivatedUser = globalConfig.getPotentialRights().getRights() == globalConfig.getRights().getRights();
		}
		refreshComments();
	}

	private void onCreateViewComments(Bundle savedInstanceState, View root)
	{
		mDrawerView = root.findViewById(R.id.rl_comment_layout);
		mDrawerLayout = (DrawerLayout) root.findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerListener(this);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mDrawerView);

		if (mFragment.getFileInfoController().getMasterDocument().getPath() instanceof Path.TeamDrive)
		{
			mCommentsViewContainer.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					expandComments();
					refreshComments();
				}
			});
		}
		else
		{
			mCommentsViewContainer.setVisibility(View.GONE);
		}

		ListView listView = (ListView) root.findViewById(R.id.lv_cmt_list);
		final EditText addCommentEditText = (EditText) root.findViewById(R.id.et_add_cmt);
		final ImageButton addCommentImage = (ImageButton) root.findViewById(R.id.img_send_cmt);

		List<Comment> comments = new ArrayList<Comment>();

		if (savedInstanceState != null && savedInstanceState.containsKey(KEY_COMMENTS))
		{
			comments = savedInstanceState.getParcelableArrayList(KEY_COMMENTS);
			loadFirstComments = false;

			ViewSwitcher switcher = (ViewSwitcher) root.findViewById(R.id.vs_load_comments);

			if (comments.size() > 0)
			{
				switcher.setDisplayedChild(1);
			}
			else
			{
				switcher.setVisibility(View.GONE);

				TextView tvNoComment = (TextView) root.findViewById(R.id.tv_no_comments);
				tvNoComment.setVisibility(View.VISIBLE);
			}
		}
		else
		{
			loadFirstComments = true;
		}

		commentsAdapter = new CommentsAdapter(mFragment.getActivity(), comments, teamdrive, this, this);
		if (savedInstanceState != null)
		{
			if (savedInstanceState.containsKey(KEY_ALL_COMMENTS_LOADED))
			{
				commentsAdapter.setAllCommentsAreLoaded(savedInstanceState.getBoolean(KEY_ALL_COMMENTS_LOADED));
			}

			if (savedInstanceState.containsKey(KEY_UPDATE_IN_PROGRESS))
			{
				commentsAdapter.setUpdateInProgress(savedInstanceState.getBoolean(KEY_UPDATE_IN_PROGRESS));
			}

			commmentsToBeDeleted = savedInstanceState.getStringArrayList(KEY_COMMENTS_TO_BE_DELETED);
		}

		if (commmentsToBeDeleted == null)
		{
			commmentsToBeDeleted = new ArrayList<String>();
		}

		listView.setAdapter(commentsAdapter);

		boolean enabled = addCommentEditText.length() > 0;
		addCommentImage.setClickable(enabled);
		addCommentImage.setEnabled(enabled);

		addCommentEditText.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				boolean enabled = s.length() > 0;
				addCommentImage.setClickable(enabled);
				addCommentImage.setEnabled(enabled);
			}
		});

		int dimension = mFragment.getActivity().getResources().getDrawable(R.drawable.add_comment_icon)
				.getIntrinsicHeight();
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dimension, dimension);

		final ProgressBar pbAddComment = (ProgressBar) root.findViewById(R.id.cmt_pb_add_comment);
		pbAddComment.setLayoutParams(params);

		addCommentImage.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{

				if (!isActivatedUser)
				{
					FeedbackToast.show(mFragment.getActivity(), false,
							mFragment.getString(R.string.tmd_err_user_not_activated));
					return;
				}

				String comment = addCommentEditText.getText().toString();
				ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();

				mFragment.getActivity().startService(
						CommentIntentHelper.getAddCommentIntent(mFragment.getActivity(), comment, commentsIds,
								filePath,
								commentsAdapter.getSince()));

				addCommentEditText.setText("");

				InputMethodManager imm = (InputMethodManager) mFragment.getActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(addCommentEditText.getWindowToken(), 0);

				addCommentInProgress = true;
				addCommentImage.setVisibility(View.INVISIBLE);
				pbAddComment.setVisibility(View.VISIBLE);
			}
		});

		if (savedInstanceState != null && savedInstanceState.getBoolean(KEY_ADD_COMMENT_IN_PROGRESS, false))
		{
			addCommentInProgress = true;
			addCommentImage.setVisibility(View.INVISIBLE);
			pbAddComment.setVisibility(View.VISIBLE);
		}
	}

	private void showCommentsView()
	{
		if (mFragment.getFileInfoController().getMasterDocument().getPath() instanceof Path.TeamDrive)
		{
			mCommentsViewContainer.setVisibility(View.VISIBLE);
			ObjectAnimator anim = ObjectAnimator.ofFloat(mCommentsViewContainer, View.ALPHA, 1.f);
			anim.start();
		}
	}

	private void hideCommentsView()
	{
		if (mFragment.getFileInfoController().getMasterDocument().getPath() instanceof Path.TeamDrive)
		{
			ObjectAnimator anim = ObjectAnimator.ofFloat(mCommentsViewContainer, View.ALPHA, 0.f);
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					mCommentsViewContainer.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
					mCommentsViewContainer.setVisibility(View.GONE);
				}
			});
			anim.start();
		}
	}

	private void expandComments()
	{
		hideCommentsView();
		mFragment.getFileInfoController().onHideOverlaysRequested();
		mDrawerLayout.openDrawer(mDrawerView);
	}

	private void refreshComments()
	{
		if (mFragment.getFileInfoController().getMasterDocument().getPath() instanceof Path.TeamDrive
				&& loadFirstComments)
		{
			commentsAdapter.getComments().clear();
			commentsAdapter.notifyDataSetChanged();
			mFragment.getActivity().startService(
					CommentIntentHelper.getLoadCommentIntent(mFragment.getActivity(), false, INITIAL_COMMENT_COUNT,
							null, filePath, 0, 0));
		}
	}

	private void onPauseComments()
	{
		EventBus.getDefault().unregister(this);
	}

	private void onSaveInstanceStateComments(Bundle outState)
	{
		outState.putParcelableArrayList(KEY_COMMENTS, (ArrayList<? extends Parcelable>) commentsAdapter.getComments());
		outState.putBoolean(KEY_ALL_COMMENTS_LOADED, commentsAdapter.areAllCommentsLoaded());
		outState.putBoolean(KEY_UPDATE_IN_PROGRESS, commentsAdapter.isUpdateInProgress());
		outState.putBoolean(KEY_ADD_COMMENT_IN_PROGRESS, addCommentInProgress);
		outState.putStringArrayList(KEY_COMMENTS_TO_BE_DELETED, commmentsToBeDeleted);
	}

	public List<Comment> handleCommentChanges(ChangedCommentResponse response, List<Comment> currentList)
	{
		List<Comment> newList = new ArrayList<Comment>(currentList);
		if (response == null)
		{
			return newList;
		}

		List<Comment> added = response.getAdded();
		newList.addAll(emptyIfNull(added));

		List<Comment> deleted = response.getDeleted();
		for (Comment delete : emptyIfNull(deleted))
		{
			newList.remove(delete);
		}

		List<Comment> modified = response.getModified();
		for (Comment modify : emptyIfNull(modified))
		{
			newList.remove(modify);
			newList.add(modify);
		}
		return newList;
	}

	private void showLoadedComments(CommentOperationResponse event)
	{
		List<Comment> newComments = event.getNewCommentList();
		if (newComments != null && newComments.size() < event.getNewCommentNumber())
		{
			commentsAdapter.setAllCommentsAreLoaded(true);
		}

		commentsAdapter.removeRefreshComment();
		List<Comment> currentList = commentsAdapter.getComments();

		if (event.isCheckChanges())
		{
			ListView list = (ListView) mFragment.getView().findViewById(R.id.lv_cmt_list);
			int oldPosition = list.getFirstVisiblePosition();
			int offset = list.getChildAt(0).getTop();

			ChangedCommentResponse changeResponse = event.getChangeCommentResponse();

			if (changeResponse != null)
			{
				List<Comment> deletedCommentsList = changeResponse.getDeleted();

				if (deletedCommentsList != null)
				{
					for (int i = 0; i < oldPosition; i++)
					{
						if (deletedCommentsList.contains(currentList.get(i)))
						{
							oldPosition--;
						}
					}
				}
			}

			int newPosition = oldPosition + newComments.size() - 1;

			List<Comment> commentList = handleCommentChanges(changeResponse, currentList);
			commentList.addAll(newComments);

			displayComments(commentList);

			list.setSelectionFromTop(newPosition, offset);

			commentsAdapter.setUpdateInProgress(false);
		}
		else
		{
			displayComments(newComments);
		}
	}

	private void showChangedComment(CommentOperationResponse event)
	{
		ChangedCommentResponse response = event.getChangeCommentResponse();
		if (response != null)
		{
			List<Comment> commentList = handleCommentChanges(response, commentsAdapter.getComments());
			displayComments(commentList);
		}
		else
		{
			Toast.makeText(mFragment.getActivity(), R.string.prv_cmt_err_loading_comments, Toast.LENGTH_LONG).show();
		}

	}

	private void displayComments(List<Comment> commentList)
	{
		ViewSwitcher switcher = (ViewSwitcher) mFragment.getView().findViewById(R.id.vs_load_comments);
		TextView tvNoComment = (TextView) mFragment.getView().findViewById(R.id.tv_no_comments);

		if (commentList != null && commentList.size() > 0)
		{
			switcher.setVisibility(View.VISIBLE);
			switcher.setDisplayedChild(1);
			tvNoComment.setVisibility(View.GONE);
		}
		else
		{
			switcher.setVisibility(View.GONE);
			tvNoComment.setVisibility(View.VISIBLE);
		}

		commentsAdapter.setComments(commentList);
		commentsAdapter.notifyDataSetChanged();
	}

	@Override
	public void onDrawerClosed(View drawerView)
	{
		showCommentsView();
		mFragment.getFileInfoController().onShowOverlaysRequested();
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mDrawerView);
	}

	@Override
	public void onDrawerOpened(View drawerView)
	{
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mDrawerView);
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset)
	{
	}

	@Override
	public void onDrawerStateChanged(int arg0)
	{
	}

	@Override
	public void deleteComment(String commentId)
	{
		commmentsToBeDeleted.add(commentId);

		ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();

		mFragment.getActivity().startService(
				CommentIntentHelper.getDeleteCommentIntent(mFragment.getActivity(), commentId, commentsIds, filePath,
						commentsAdapter.getSince()));
	}

	@Override
	public boolean willCommentBeDeleted(String commentId)
	{
		return commmentsToBeDeleted.contains(commentId);
	}

	@Override
	public void loadNextComments()
	{
		ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();
		int start = commentsAdapter.isUpdateInProgress() ? commentsAdapter.getCount() - 1 : commentsAdapter.getCount();

		mFragment.getActivity().startService(
				CommentIntentHelper.getLoadCommentIntent(mFragment.getActivity(), true, NEXT_COMMENT_COUNT,
						commentsIds,
						filePath, commentsAdapter.getSince(), start));
	}

	@Override
	public void onLoadRequested(User user, ImageResultCallback callback)
	{
		int userImageSize = mFragment.getResources().getDrawable(R.drawable.tmd_default_icon).getIntrinsicHeight();

		long requestId = TeamDriveService.getRandomRequestId();
		TeamDriveService.getUserImageFromCache(mFragment.getActivity(), user, userImageSize,
				TEAM_DRIVE_SERVICE_REQUEST_ID,
				requestId);
		if (!imageDownlaodedForUsers.contains(user.getUserId()))
		{
			TeamDriveService.getUserImage(mFragment.getActivity(), user, userImageSize, TEAM_DRIVE_SERVICE_REQUEST_ID,
					requestId);
			imageDownlaodedForUsers.add(user.getUserId());
		}

		userImageCallbacks.put(requestId, callback);
	}

	/** Returns true if back press was handles, otherwise false */
	@Override
	public boolean onBackPressed()
	{
		if (mDrawerLayout.isDrawerOpen(mDrawerView))
		{
			mDrawerLayout.closeDrawer(mDrawerView);
			return true;
		}

		return false;
	}

}
