package net.teamplace.android.preview.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cortado.android.R;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.preview.ui.controller.ActionController;
import net.teamplace.android.preview.ui.controller.BasicActionController;
import net.teamplace.android.preview.ui.controller.BasicCommentsController;
import net.teamplace.android.preview.ui.controller.BasicFileInfoController;
import net.teamplace.android.preview.ui.controller.BasicVersionsController;
import net.teamplace.android.preview.ui.controller.CommentsController;
import net.teamplace.android.preview.ui.controller.ContentController;
import net.teamplace.android.preview.ui.controller.FileInfoController;
import net.teamplace.android.preview.ui.controller.Tpm3ContentController;
import net.teamplace.android.preview.ui.controller.VersionsController;
import net.teamplace.android.utils.Log;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Container for a DocumentView and some decorations: a PageNumberView and the ActionBar. This class has the logic for
 * showing, hiding and synchronizing the decorations. The PageNumberView is owned by this class and thus hidden and
 * shown directly. Requests for hiding and showing the ActionBar are instead forwarded to PreviewActivity through a
 * listener: in this way, the Activity, which owns the ActionBar, can have the last decision on it.
 * 
 * @author Gil Vegliach
 */

public class Tpm3PreviewFragment extends PreviewHolderFragment
{
	public static final String TAG = tag(Tpm3PreviewFragment.class);
	private static final String PKG = pkg(Tpm3PreviewFragment.class);

	private static final String KEY_MASTER_DOC_INFO = PKG + ".masterDocInfo";
	private static final String KEY_PREVIEW_CONFIGURATION = PKG + ".previewConfiguration";

	public static Tpm3PreviewFragment newInstance(FileInfo info, PreviewConfiguration configuration)
	{
		Tpm3PreviewFragment fragment = new Tpm3PreviewFragment();
		Bundle args = new Bundle(1);
		args.putParcelable(KEY_MASTER_DOC_INFO, info);
		args.putParcelable(KEY_PREVIEW_CONFIGURATION, configuration);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	protected ContentController createPreviewContentController()
	{
		return new Tpm3ContentController(this);
	}

	@Override
	protected FileInfoController createFileInfoController()
	{
		return new BasicFileInfoController(this);
	}

	@Override
	protected VersionsController createVersionsController()
	{
		return new BasicVersionsController(this);
	}

	@Override
	protected CommentsController createCommentsController()
	{
		return new BasicCommentsController(this);
	}

	@Override
	protected ActionController createActionController()
	{
		return new BasicActionController(this);
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		getActionController().onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getActionController().onCreate(savedInstanceState);

		if (savedInstanceState == null)
		{
			Bundle args = getArguments();
			FileInfo doc = (FileInfo) args.getParcelable(KEY_MASTER_DOC_INFO);
			getFileInfoController().setMasterDocument(doc);
			getFileInfoController().setDisplayingPath(doc.getPath());
			getFileInfoController().setDisplayingDate(doc.getLastModifiedDate());
			PreviewConfiguration configuration = args.getParcelable(KEY_PREVIEW_CONFIGURATION);
			getFileInfoController().setPreviewConfiguration(configuration);
			getCommentsController().setPreviewConfiguration(configuration);
		}
		getFileInfoController().onCreate(savedInstanceState);
		getCommentsController().onCreate(savedInstanceState);
	}

	// TouchListener2 uses onSingleTapConfirmed() instead of performClick()
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.prv_tpm3_preview_fragment, container, false);

		getPreviewContentController().setFragmentView(root);
		getPreviewContentController().onCreateView(inflater, container, savedInstanceState);

		getFileInfoController().setFragmentView(root);
		getFileInfoController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().setFragmentView(root);
		getVersionsController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().onUiUpdateRequested();

		getActionController().setFragmentView(root);
		getActionController().onCreateView(inflater, container, savedInstanceState);

		getCommentsController().setFragmentView(root);
		getCommentsController().onCreateView(inflater, container, savedInstanceState);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getVersionsController().onActivityCreated(savedInstanceState);
	}

	@Override
	public void onStart()
	{
		super.onStart();
		getPreviewContentController().onStart();
	}

	@Override
	public void onStop()
	{
		super.onStop();
		getPreviewContentController().onStop();
	}

	@Override
	public void onResume()
	{
		super.onResume();

		getPreviewContentController().onResume();
		getVersionsController().onResume();
		getActionController().onResume();
		getCommentsController().onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getPreviewContentController().onPause();
		getVersionsController().onPause();
		getActionController().onPause();
		getCommentsController().onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		getPreviewContentController().onSaveInstanceState(outState);
		getActionController().onSaveInstanceState(outState);
		getFileInfoController().onSaveInstanceState(outState);
		getVersionsController().onSaveInstanceState(outState);
		getCommentsController().onSaveInstanceState(outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}
		getPreviewContentController().onActivityResult(requestCode, resultCode, data);
		getActionController().onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		getActionController().onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		getActionController().onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return getActionController().onOptionsItemSelected(item);
	}
}
