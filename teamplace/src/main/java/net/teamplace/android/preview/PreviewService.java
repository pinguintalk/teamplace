package net.teamplace.android.preview;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.browsing.utils.IdGenerator;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.file.FileVersion;
import net.teamplace.android.http.request.files.FileRequester;
import net.teamplace.android.http.request.preview.PreviewRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.preview.cache.BitmapPool;
import net.teamplace.android.preview.cache.DocumentsDiskCache;
import net.teamplace.android.preview.cache.DocumentsMemoryCache;
import net.teamplace.android.preview.cache.Key;
import net.teamplace.android.preview.cache.Key.Page;
import net.teamplace.android.preview.ui.TiledDocumentPage;
import net.teamplace.android.preview.utils.ReorderingStack;
import net.teamplace.android.preview.widget.VersionItem;
import net.teamplace.android.providers.http.FileRequesterProvider;
import net.teamplace.android.providers.http.PreviewRequesterProvider;
import net.teamplace.android.util.annotations.Immutable;
import net.teamplace.android.util.annotations.NonBlocking;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.LoggingService;
import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.v4.content.LocalBroadcastManager;

/**
 * Handles all background logic regarding previewing: downloading and caching pages, and versions. At the moment
 * this service is split into two main parts: the downloading and caching is done on the "slow worker"/bound part, while
 * versions requests are done on the "fast worker"/started part.
 * 
 * @author Gil Vegliach
 */
public class PreviewService extends LoggingService implements
		OnLoadPageTaskFinishedListener
{
	private static final String PKG = pkg(BrowsingService.class);
	static
	{
		TAG = tag(PreviewService.class);
	}

	public static class Contract
	{
		public static final String REQUEST_CODE = PKG + ".requestCode";
		public static final String REQUEST_ID = PKG + ".id";
		public static final int NO_ID = IdGenerator.NO_ID;

		public static final int REQUEST_VERSIONS = 100;

		public static final String ACTION_NOTIFICATION_PAGE_LOADED = PKG + ".ACTION_PAGE_LOADED";
		public static final String ACTION_NOTIFICATION_FAILED = PKG + ".ACTION_NOTIFICATION_LOADING_FAILED";
		public static final String NOTIFICATION_POSITION_LOADED_PAGE = PKG + ".NOTIFICATION_POSITION_LOADED_PAGE";
		public static final String NOTIFICATION_TOTAL_PAGE_NUMBER = PKG + "NOTIFICATION_TOTAL_PAGE_NUMBER";

		public static final String ACTION_RESPONSE_VERSIONS = PKG + ".ACTION_RESPONSE_VERSIONS";
		public static final String RESPONSE_REQUEST_EXTRAS = PKG + ".requestExtras";
		public static final String RESPONSE_VERSIONS_LIST = PKG + ".versionsList";

		public static Intent buildRequestVersionsIntent(Context ctx, Path path)
		{
			checkNonNullArgs(ctx, path);
			Intent i = new Intent(ctx, PreviewService.class);
			i.putExtra(Contract.REQUEST_CODE, REQUEST_VERSIONS);
			i.putExtra(Contract.REQUEST_ID, IdGenerator.getId());
			i.putExtra(KEY_PATH, path);
			return i;
		}

		/** Extracts the request id from a request or response given in form of an Intent */
		public static int extractRequestId(Intent intent)
		{
			if (intent == null)
			{
				return NO_ID;
			}

			int id = intent.getIntExtra(REQUEST_ID, NO_ID);
			if (id != NO_ID)
			{
				return id;
			}

			// This is a response, try to extract the data from RESPONSE_REQUEST_EXTRAS
			Bundle extras = intent.getBundleExtra(RESPONSE_REQUEST_EXTRAS);
			if (extras == null)
			{
				return NO_ID;
			}

			return extras.getInt(REQUEST_ID, NO_ID);
		}
	}

	private static final String KEY_PATH = PKG + ".path";

	private static final int MINIMUM_PAGES_IN_MEMORY = 3;
	// Taken from the code in AsyncTask
	// private static final int CPU_COUNT = Runtime.getRuntime().availableProcessors();
	// TODO: the more the tasks, the more memory is sucked up, so change this
	private static final int CORE_POOL_SIZE = 1;
	private static final float MAX_MEMORY_CACHE_MEMORY_FACTOR = .4f; // 40% of memory class
	private static final float MAX_BITMAP_POOL_MEMORY_FACTOR = .2f; // 20% of memory class
	private static final int MAX_SIZE_STORAGE = 50 * 1024 * 1024; // bytes

	private static final String CACHE_FOLDER = "preview_cache";

	/** Max time to wait when we force to download the page number triggering a print on the server */
	// The forced page number download is disable at the moment
	@SuppressWarnings("unused")
	private static final long MAX_WAIT_MS = 5000;

	/** 'What' part of a Message to be sent to the fast worker through it Handler */
	private static final int FAST_WORKER_HANDLER_MSG_WHAT = 0;

	private final static long SERVICE_SHUT_DOWN_TIMEOUT = 15 * 1000L;

	private int mMaxBytesMemoryCache;
	private final IBinder mBinder = new PreviewServiceBinder();

	/** Shutdown timer */
	final private ShutDownTimer mShutDownTimer = new ShutDownTimer(this);

	// Types vary from Task to Task
	/** Requests that are executing at any moment on the bound part of this service */
	private final HashMap<Key, AsyncTask<?, ?, ?>> mExecutingTasks = new HashMap<>();

	/** Requests that need to be executed */
	private final ReorderingStack<Key.Page> mRequestStack = new ReorderingStack<>();

	/** Bitmap pool to reduce garbage collection */
	// Volatile ensures safe publication of the reference. Working implementation but needs fine tuning on old devices.
	// Watch out for the memory limit. Disable setting mBitmapPool = null
	private volatile BitmapPool mBitmapPool;
	private volatile DocumentsDiskCache mDiskCache;
	private DocumentsMemoryCache mMemoryCache;

	/** Handles requests on the fast worker */
	private Handler mHandler;
	private Looper mLooper;

	private volatile boolean mBound;
	private volatile boolean mStarted;

	public interface Dependencies extends PreviewRequesterProvider, FileRequesterProvider
	{
	}

	private synchronized Dependencies getDependencies()
	{
		if (mDependencies == null)
		{
			mDependencies = new DependenciesImpl();
		}
		return mDependencies;
	}

	/** @hide Used by tests to inject dependencies. Don't use in production */
	public synchronized void setDependencies(Dependencies dependencies)
	{
		mDependencies = dependencies;
	}

	private static class DependenciesImpl implements Dependencies
	{
		@Override
		public FileRequester provideFileRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new FileRequester(context, session, new TeamplaceBackend());
		}

		@Override
		public PreviewRequester providePreviewRequester(Context context)
		{
			Session session = new SessionManager(context).getLastSession();
			return new PreviewRequester(context, session, new TeamplaceBackend());
		}
	}

	@Override
	public void onCreate()
	{
		ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		int memClass = am.getMemoryClass();
		int maxBytesBitmapPool = Math.round(memClass * MAX_BITMAP_POOL_MEMORY_FACTOR * 1024 * 1024);
		mMaxBytesMemoryCache = Math.round(memClass * MAX_MEMORY_CACHE_MEMORY_FACTOR * 1024 * 1024);
		Log.d(TAG, "alloc stats: memory class: " + memClass + " mb, bitmap pool: " + maxBytesBitmapPool
				+ " bytes, memory cache: " + mMaxBytesMemoryCache + " bytes");

		mBitmapPool = new BitmapPool(maxBytesBitmapPool);
		mMemoryCache = new DocumentsMemoryCache(mMaxBytesMemoryCache, mBitmapPool);
		mDiskCache = new DocumentsDiskCache(getDiskCacheDir(), MAX_SIZE_STORAGE);

		// Mimic an IntentService
		HandlerThread thread = new HandlerThread("PreviewServiceFastWorker");
		thread.start();

		mLooper = thread.getLooper();
		mHandler = new Handler(mLooper)
		{
			@Override
			public void handleMessage(Message msg)
			{
				onHandleIntent((Intent) msg.obj);

				// Keep this Service started if there are pending messages
				mStarted = hasMessages(FAST_WORKER_HANDLER_MSG_WHAT);
				setTimerIfNeeded();
			}
		};

		// Make the service persistent (started)
		Intent intent = new Intent(this, getClass());
		startService(intent);

		// Kick off a shutdown timeout: if the client registers, the timeout is stopped
		setTimerIfNeeded();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		Bundle extras = intent.getExtras();
		if (extras != null && extras.getInt(Contract.REQUEST_CODE, -1) != -1)
		{
			// Service has been started explicitly from an external component, unset the timer. The timer will be
			// set again after the task has run
			mStarted = true;
			unsetTimerIfNeeded();
			Message msg = Message.obtain(mHandler, FAST_WORKER_HANDLER_MSG_WHAT, intent);
			msg.sendToTarget();
		}

		// This service should not retry any operations if killed because it does not modify resources on the server.
		// Change this if this Service will add comments some day in the future.
		// See also bug EXTUA-259: http://jira03.thinprint.de/browse/EXTUA-259
		return Service.START_NOT_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		mBound = true;
		unsetTimerIfNeeded();
		return mBinder;
	}

	@Override
	public void onRebind(Intent intent)
	{
		mBound = true;
		unsetTimerIfNeeded();
	}

	@Override
	public boolean onUnbind(Intent intent)
	{
		mBound = false;
		setTimerIfNeeded();
		// If true, it forces onRebind() to be called next time a client binds
		return true;
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
		boolean allCanceled = cancelAllTasks();
		Log.i(TAG, "onDestroy(), allCanceled: " + allCanceled + ", inst: " + this);
		mDiskCache.close();
		mLooper.quit();
	}

	@Override
	public void onLoadPageTaskSuccess(Key.Page pkey, Result result)
	{
		mExecutingTasks.remove(pkey);
		TiledDocumentPage page = result.getPage();

		if (page != null)
		{
			mMemoryCache.put(pkey, page);
			int count = result.getPageCount();
			if (count != -1)
			{
				Key.Count ckey = Key.makeCountKey(pkey);
				mMemoryCache.putPageCount(ckey, count);
			}

			Intent i = new Intent(Contract.ACTION_NOTIFICATION_PAGE_LOADED);
			i.putExtra(Contract.NOTIFICATION_POSITION_LOADED_PAGE, pkey.getPageNumber());
			i.putExtra(Contract.NOTIFICATION_TOTAL_PAGE_NUMBER, count);
			sendLocalBroadcast(i);
		}

		processNextFromStack();
	}

	@Override
	public void onLoadPageTaskCancelled(Key.Page key)
	{
		mExecutingTasks.remove(key);
		processNextFromStack();
	}

	@Override
	public void onLoadPageTaskError(Page key)
	{
		mExecutingTasks.clear();
		mMemoryCache.clear();
		Intent i = new Intent(Contract.ACTION_NOTIFICATION_FAILED);
		sendLocalBroadcast(i);
	}

	private void onHandleIntent(Intent intent)
	{
		Bundle extras = intent.getExtras();
		int type = extras.getInt(Contract.REQUEST_CODE);
		switch (type)
		{
			case Contract.REQUEST_VERSIONS:
			{
				Path path = extras.getParcelable(KEY_PATH);
				ArrayList<VersionItem> versions = requestVersions(path);
				Intent i = new Intent(Contract.ACTION_RESPONSE_VERSIONS);
				i.putParcelableArrayListExtra(Contract.RESPONSE_VERSIONS_LIST, versions);
				i.putExtra(Contract.RESPONSE_REQUEST_EXTRAS, extras);
				sendLocalBroadcast(i);
				return;
			}
			default:
			{
				throw new IllegalArgumentException("Request type not recognized");
			}
		}
	}

	private void sendLocalBroadcast(Intent i)
	{
		LocalBroadcastManager bm = LocalBroadcastManager.getInstance(this);
		bm.sendBroadcast(i);
	}

	private void setTimerIfNeeded()
	{
		if (!mBound && !mStarted)
		{
			mShutDownTimer.set(SERVICE_SHUT_DOWN_TIMEOUT);
		}
	}

	private void unsetTimerIfNeeded()
	{
		if (mBound || mStarted)
		{
			mShutDownTimer.unset();
		}
	}

	private ArrayList<VersionItem> requestVersions(Path path)
	{
		try
		{
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			String parentFolder = PathUtils.remoteRequestString(path.getParentPath());
			FileRequester req = getDependencies().provideFileRequester(this);
			ArrayList<FileVersion> result = req.getFileVersions(teamDriveId, parentFolder, path.getName());
			return convertFileVersionsToVersionItems(result);
		}
		catch (CortadoException | IOException e)
		{
			Log.e(PreviewService.TAG, "Exception requesting versions", e);
			return null;
		}
	}

	private ArrayList<VersionItem> convertFileVersionsToVersionItems(ArrayList<FileVersion> list)
	{
		ArrayList<VersionItem> result = new ArrayList<VersionItem>(list.size());
		for (FileVersion fv : list)
		{
			result.add(new VersionItem(fv.getFullName(), fv.getVersionNumber(), fv.getSize(), fv.getDateModified(), fv.getCreatorSID()));
		}
		return result;
	}

	private void request(Key key)
	{
		if (key instanceof Key.Page)
		{
			if (!isResultLoaded(key) && mExecutingTasks.get(key) == null)
			{
				Key.Page pkey = (Key.Page) key;
				mRequestStack.pushOrReorder(pkey);
				processNextFromStack();
			}
		}
		else
		{
			throw new IllegalArgumentException("Key not recognized");
		}
	}

	private boolean isResultLoaded(Key key)
	{
		if (key instanceof Key.Page)
		{
			Key.Page pkey = (Key.Page) key;
			return mMemoryCache.isPageCached(pkey);
		}
		else
		{
			throw new IllegalArgumentException("Key not recognized");
		}
	}

	private void processNextFromStack()
	{
		// Find first unloaded request or terminate
		Key.Page pkey = null;
		while (true)
		{
			if (mRequestStack.isEmpty())
			{
				return;
			}

			pkey = mRequestStack.pop();
			if (!isResultLoaded(pkey))
			{
				break;
			}
		}
		// Postcondition: pkey contains the first unloaded request

		// If there is a free a slot for executing the pending request, then do so. Otherwise pile it up the stack
		if (mExecutingTasks.size() < CORE_POOL_SIZE)
		{
			PreviewRequesterProvider requesterProvider = getDependencies();
			int maxMemoryBytes = mMaxBytesMemoryCache / MINIMUM_PAGES_IN_MEMORY;
			LoadPageTask task = new LoadPageTask(this, pkey, mBitmapPool, mDiskCache, this, requesterProvider,
					maxMemoryBytes);
			mExecutingTasks.put(pkey, task);
			// TODO: make it serial for the moment
			// task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
			task.execute();
			Log.d(TAG, "** Requesting page: " + pkey.getPageNumber());
		}
		else
		{
			mRequestStack.pushOrReorder(pkey);
		}
	}

	private Result retrieveResult(Key key)
	{
		if (!isResultLoaded(key))
		{
			return null;
		}

		if (key instanceof Key.Page)
		{
			Key.Page pkey = (Key.Page) key;
			Key.Count ckey = Key.makeCountKey(key.getPath(), key.getDate());
			TiledDocumentPage page = mMemoryCache.get(pkey);
			int count = mMemoryCache.getPageCount(ckey);
			return new Result(page, count);
		}
		else
		{
			throw new IllegalArgumentException("Key not recognized");
		}
	}

	private boolean cancelAllTasks()
	{
		boolean result = true;
		for (AsyncTask<?, ?, ?> task : mExecutingTasks.values())
		{
			if (task != null && task.getStatus() != AsyncTask.Status.FINISHED)
			{
				result &= task.cancel(true);
			}
		}
		mExecutingTasks.clear();
		mRequestStack.clear();
		return result;
	}

	/** Obtains the internal cache directory or throw a RuntimeException */
	private File getDiskCacheDir()
	{
		File cacheDir = new File(getApplicationContext().getCacheDir(), CACHE_FOLDER);

		// Can't create directory nor cache
		if (!cacheDir.exists())
		{
			if (!cacheDir.mkdir())
			{
				throw new RuntimeException("Cannot create internal cache directory for the Preview component");
			}
		}

		return cacheDir;
	}

	@Immutable
	public static class Result
	{
		private final TiledDocumentPage mPage;
		private final int mPageCount;

		public Result(TiledDocumentPage page, int pageCount)
		{
			mPage = page;
			mPageCount = pageCount;
		}

		public TiledDocumentPage getPage()
		{
			return mPage;
		}

		public int getPageCount()
		{
			return mPageCount;
		}
	}

	public class PreviewServiceBinder extends Binder
	{
		@NonBlocking
		public boolean isResultLoaded(Key key)
		{
			return PreviewService.this.isResultLoaded(key);
		}

		@NonBlocking
		public void request(Key key)
		{
			PreviewService.this.request(key);
		}

		@NonBlocking
		public Result retrieveResult(Key key)
		{
			return PreviewService.this.retrieveResult(key);
		}
	}

	/** Handles a shut-down timer */
	private static class ShutDownTimer
	{
		private final static String SHUT_DOWN_TIMER_NAME = "ShutDownTimer";
		private final Timer mShutDownTimer = new Timer(SHUT_DOWN_TIMER_NAME, true);
		private TimerTask mCurrentTask;
		private PreviewService mService;

		private ShutDownTimer(PreviewService service)
		{
			mService = service;
		}

		private static class ShutDownTask extends TimerTask
		{
			private PreviewService mService;

			private ShutDownTask(PreviewService service)
			{
				mService = service;
			}

			@Override
			public void run()
			{
				synchronized (mService)
				{
					mService.stopSelf();
					mService = null;
				}
			}

			@Override
			public boolean cancel()
			{
				synchronized (mService)
				{
					mService = null;
				}
				return super.cancel();
			}
		}

		public void set(long timeoutMs)
		{
			mCurrentTask = new ShutDownTask(mService);
			mShutDownTimer.schedule(mCurrentTask, timeoutMs);
		}

		public boolean unset()
		{
			if (mCurrentTask != null)
			{
				boolean result = mCurrentTask.cancel();
				mCurrentTask = null;
				mShutDownTimer.purge();
				return result;
			}

			return true;
		}
	}

	private Dependencies mDependencies;
}
