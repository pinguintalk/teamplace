package net.teamplace.android.preview.ui.controller;

import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.utils.FragmentLifecycleCallbacks;
import android.view.View;


public interface PreviewController extends FragmentLifecycleCallbacks
{

	public static final int OVERLAY_VISIBLE = 0;
	public static final int OVERLAY_HIDDEN = 1;

	/**
	 * @param v
	 *            The fragment layout containing the content container
	 */
	public void setFragmentView(View v);

	/**
	 * Called when all overlays shall be hidden
	 */
	public void onHideOverlaysRequested();

	/**
	 * Called when all overlays shall be shown
	 */
	public void onShowOverlaysRequested();

	public int getOverlayState();

	public void setPreviewConfiguration(PreviewConfiguration configuration);
}
