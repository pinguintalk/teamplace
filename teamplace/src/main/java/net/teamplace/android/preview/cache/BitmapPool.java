package net.teamplace.android.preview.cache;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import net.teamplace.android.preview.ui.Geometry;
import net.teamplace.android.utils.Log;

import android.graphics.Bitmap;


/** @author Gil Vegliach */
public class BitmapPool
{
	public static final String TAG = tag(BitmapPool.class);

	private static final boolean DEBUG = true;

	private int mSize;
	private final int mCapacity;
	private final TreeMap<Geometry, LinkedList<Bitmap>> mPool = new TreeMap<Geometry, LinkedList<Bitmap>>(
			DESCENDING_SIZE_GEOMETRY);

	public BitmapPool(int capacityBytes)
	{
		mCapacity = capacityBytes;
		mSize = 0;
	}

	public Bitmap get(Geometry geom)
	{
		int size = sizeOfGeometry(geom);

		LinkedList<Bitmap> ll = mPool.get(geom);
		if (ll == null)
		{
			if (DEBUG)
				Log.d(TAG, "x get(): MISS, geom: " + geom.toString());
			return null;
		}
		mSize -= size;
		Bitmap result = ll.removeFirst();
		if (ll.size() == 0)
			mPool.remove(geom);

		if (DEBUG)
			Log.d(TAG, "o get(): HIT,  geom: " + geom.toString());

		return result;
	}

	public boolean put(Bitmap b)
	{
		if (!b.isMutable())
		{
			if (DEBUG)
				Log.d(TAG, "xx put(): can't put immutable bitmap: " + b);
			return false;
		}

		Geometry geom = Geometry.valueOf(b.getWidth(), b.getHeight());
		int size = sizeOfGeometry(geom);
		if (available() < size && !freeUp(size))
		{
			if (DEBUG)
				Log.d(TAG, "xx put(): can't free : " + size + "; avail: " + available() + "; geom: " + geom
						+ "; bitmap: " + b);
			return false;
		}

		// Post-condition: avail >= size
		LinkedList<Bitmap> ll = mPool.get(geom);
		if (ll == null)
		{
			ll = new LinkedList<Bitmap>();
			mPool.put(geom, ll);
		}
		ll.addLast(b);
		mSize += size;

		if (DEBUG)
			Log.d(TAG, "oo put(): inserted geom: " + geom.toString());

		return true;
	}

	private boolean freeUp(int bytes)
	{
		// We have the memory already
		if (available() >= bytes)
			return true;

		Set<Entry<Geometry, LinkedList<Bitmap>>> entries = mPool.entrySet();
		Iterator<Map.Entry<Geometry, LinkedList<Bitmap>>> entriesIter = entries.iterator();
		while (entriesIter.hasNext())
		{
			Map.Entry<Geometry, LinkedList<Bitmap>> entry = entriesIter.next();
			Geometry geom = entry.getKey();
			LinkedList<Bitmap> ll = entry.getValue();
			int size = sizeOfGeometry(geom);

			Iterator<Bitmap> llIter = ll.iterator();
			while (llIter.hasNext())
			{
				llIter.next();
				llIter.remove();
				mSize -= size;
				if (available() >= bytes)
					break;
			}

			if (ll.size() == 0)
				entriesIter.remove();

			if (available() >= bytes)
				return true;
		}

		return false;
	}

	private static int sizeOfGeometry(Geometry geom)
	{
		// Assumes ARGB_8888
		return geom.getWidth() * geom.getHeight() * 4;
	}

	public int capacity()
	{
		return mCapacity;
	}

	public int size()
	{
		return mSize;
	}

	public int available()
	{
		return mCapacity - mSize;
	}

	// Order not consistent with equals(), see documentation of TreeMap. The lookup on keys will use the Comparator
	// instead of equals(); notice this is different form Map's contract
	private static final Comparator<Geometry> DESCENDING_SIZE_GEOMETRY = new Comparator<Geometry>()
	{
		@Override
		public int compare(Geometry lhs, Geometry rhs)
		{
			int sizeLhs = sizeOfGeometry(lhs);
			int sizeRhs = sizeOfGeometry(rhs);
			return sizeRhs - sizeLhs;
		}
	};

}
