package net.teamplace.android.preview.dialog;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.browsing.dialog.StyleableAlertDialog;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.cortado.android.R;

/**
 * Simple dialog with a EditText that asks for a page number in some specified interval, validates user input, and
 * notifies its target fragment back
 * 
 * @author Gil Vegliach
 */
public class GoToPageDialogFragment extends DialogFragment implements TextWatcher
{
	private static final String PKG = pkg(GoToPageDialogFragment.class);
	public static final String TAG = tag(GoToPageDialogFragment.class);

	public static final int REQUEST_CODE = R.id.request_code_go_to_page;
	public static final String KEY_PAGE = PKG + ".page";

	private static final String KEY_PAGE_COUNT = PKG + ".pageCount";
	private static final String KEY_PAGE_NUMBER_TEXT = PKG + ".pageNumberText";

	private EditText mEtPageField;
	private int mPageCount = -1;

	public static GoToPageDialogFragment newInstance(int pageCount)
	{
		GoToPageDialogFragment frag = new GoToPageDialogFragment();
		Bundle args = new Bundle();
		args.putInt(KEY_PAGE_COUNT, pageCount);
		frag.setArguments(args);
		return frag;
	}

	@SuppressLint("InflateParams")
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState)
	{
		Context ctx = getActivity();
		StyleableAlertDialog dlg = new StyleableAlertDialog(ctx);
		dlg.setTitle(R.string.prv_dlg_goto_page);

		mPageCount = getArguments().getInt(KEY_PAGE_COUNT);

		LayoutInflater inflater = LayoutInflater.from(ctx);
		View v = inflater.inflate(R.layout.prv_goto_page_view, null);
		mEtPageField = (EditText) v.findViewById(R.id.page_field);

		String hintText = getString(R.string.prv_dlg_enter_page_number, mPageCount);
		mEtPageField.setHint(hintText);

		if (savedInstanceState != null)
		{
			String text = savedInstanceState.getString(KEY_PAGE_NUMBER_TEXT);
			int len = text.length();
			mEtPageField.setText(text);
			mEtPageField.setSelection(len, len);
		}

		mEtPageField.addTextChangedListener(this);

		dlg.setView(v);
		dlg.setButton(DialogInterface.BUTTON_NEGATIVE, getText(R.string.app_cancel),
				(DialogInterface.OnClickListener) null);
		dlg.setButton(DialogInterface.BUTTON_POSITIVE, getText(R.string.app_ok),
				new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
						int page = validateInput();
						if (page == -1)
							return;

						dismiss();

						Fragment target = getTargetFragment();
						if (target != null)
						{
							Intent data = new Intent();
							data.putExtra(KEY_PAGE, page);
							target.onActivityResult(REQUEST_CODE, Activity.RESULT_OK, data);
						}
					}
				});

		// Show the soft keyboard when the dialog pops up
		dlg.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		// Set appropriately the OK button depending on the input as soon as the dialog is displayed
		dlg.setOnShowListener(new DialogInterface.OnShowListener()
		{
			@Override
			public void onShow(DialogInterface dialog)
			{
				setOkButtonEnabledFromInput();
			}
		});

		return dlg;
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count)
	{
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after)
	{
	}

	@Override
	public void afterTextChanged(Editable editable)
	{
		setOkButtonEnabledFromInput();
	}

	@Override
	public void onSaveInstanceState(Bundle out)
	{
		out.putString(KEY_PAGE_NUMBER_TEXT, mEtPageField.getText().toString());
	}

	private void setOkButtonEnabledFromInput()
	{
		Dialog dlg = getDialog();
		if (dlg instanceof AlertDialog)
		{
			AlertDialog alertDlg = (AlertDialog) dlg;
			Button button = alertDlg.getButton(AlertDialog.BUTTON_POSITIVE);
			boolean isValid = validateInput() != -1;

			button.setEnabled(isValid);
		}
	}

	/**
	 * Return -1 on invalid input or page number out of bounds. Otherwise returns a number between 1 and mPageCount
	 */
	private int validateInput()
	{
		int selectedPage = -1;

		try
		{
			selectedPage = Integer.parseInt(mEtPageField.getText().toString());
		}
		catch (NumberFormatException e)
		{
			selectedPage = -1;
		}

		if (0 < selectedPage && selectedPage <= mPageCount)
			return selectedPage;

		return -1;
	}
}
