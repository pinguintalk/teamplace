package net.teamplace.android.preview.ui;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.HashMap;

import net.teamplace.android.action.Action;
import net.teamplace.android.action.ActionFactory;
import net.teamplace.android.action.BaseActionFactory;
import net.teamplace.android.action.PreviewDeleteAction;
import net.teamplace.android.action.PrintAction;
import net.teamplace.android.action.RenameAction;
import net.teamplace.android.action.ShareAction;
import net.teamplace.android.action.SmarfileAction;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.EditTextDialogFragment;
import net.teamplace.android.browsing.dialog.SmartfileDialogFragment;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.widget.PreviewFooterView;
import net.teamplace.android.utils.GenericUtils;

import android.app.Activity;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cortado.android.R;

@Deprecated
public class PreviewLocalFragment extends Fragment
{
	public static final String TAG = tag(PreviewLocalFragment.class);

	private static final String PKG = pkg(PreviewLocalFragment.class);
	private static final String KEY_DOC_INFO = PKG + ".docInfo";

	private FileInfo mFileInfo;
	private PreviewFooterView mFooterView;

	private final ActionFactory mActionFactory = new PreviewLocalActionFactory();

	public static PreviewLocalFragment newInstance(FileInfo info)
	{
		PreviewLocalFragment fragment = new PreviewLocalFragment();
		Bundle args = new Bundle(1);
		args.putParcelable(KEY_DOC_INFO, info);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		Bundle args = getArguments();
		checkNonNullArg(args, "Create fragment with newInstance()");

		mFileInfo = savedInstanceState == null
				? args.<FileInfo> getParcelable(KEY_DOC_INFO)
				: savedInstanceState.<FileInfo> getParcelable(KEY_DOC_INFO);

		checkNonNullArg(mFileInfo, "Create fragment with newInstance()");

		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View v = inflater.inflate(R.layout.prv_local_preview_fragment, container, false);
		v.setOnClickListener(mShareClickListener);

		mFooterView = (PreviewFooterView) v.findViewById(R.id.footer_view);
		syncFooterView();
		return v;
	}

	@Override
	public void onResume()
	{
		super.onResume();
		Activity act = getActivity();
		IntentFilter filter = new IntentFilter(BrowsingService.Contract.ACTION_RESPONSE_RENAME);
		LocalBroadcastManager.getInstance(act).registerReceiver(mResponseReceiver, filter);
	}

	@Override
	public void onPause()
	{
		super.onPause();
		Activity act = getActivity();
		LocalBroadcastManager.getInstance(act).unregisterReceiver(mResponseReceiver);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putParcelable(KEY_DOC_INFO, mFileInfo);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// No dynamic rights management, we are always allowed to delete and rename local files
		inflater.inflate(R.menu.prv_context_menu, menu);
		menu.removeItem(R.id.action_mail);
		menu.removeItem(R.id.action_zip);
		menu.removeItem(R.id.action_export_to_pdf);
		menu.removeItem(R.id.action_favorite);
		menu.removeItem(R.id.action_unfavorite);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		mActionFactory.build(id).execute();
		return true;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}

		if (requestCode == SmartfileDialogFragment.REQUEST_CODE)
		{
			Path target = data.getParcelableExtra(SmartfileDialogFragment.KEY_TARGET);
			Path[] sources = new Path[] {mFileInfo.getPath()};

			Intent newData;
			PreviewActivity act = (PreviewActivity) getActivity();
			int action = data.getIntExtra(SmartfileDialogFragment.KEY_ACTION, SmartfileDialogFragment.NO_ACTION);
			if (action == SmartfileDialogFragment.ACTION_COPY)
			{
				newData = BrowsingService.Contract.buildCopyRequestIntent(act, sources, target);
			}
			else if (action == SmartfileDialogFragment.ACTION_MOVE)
			{
				newData = BrowsingService.Contract.buildMoveRequestIntent(act, sources, target);
			}
			else
			{
				throw new IllegalArgumentException("Action non recognized");
			}

			GenericUtils.finishWithData(act, newData);
		}
		else if (requestCode == R.id.request_code_rename)
		{
			CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
			String newName = cs.toString();

			// Then execute background request
			Activity act = getActivity();
			Intent i = BrowsingService.Contract.buildRenameRequestIntent(getActivity(), mFileInfo.getPath(),
					newName);
			act.startService(i);
		}
	}

	private void onResponseReceived(Intent intent)
	{
		String action = intent.getAction();
		if (BrowsingService.Contract.ACTION_RESPONSE_RENAME.equals(action))
		{
			String newName = BrowsingService.Contract.extractNewName(intent);
			if (newName == null)
			{
				throw new IllegalStateException(
						"Please build requests with methods in the Contract class and do not modify them. Intent: "
								+ intent);
			}

			// Sync information on screen
			Path newPath = PathUtils.buildRenamedPath(mFileInfo.getPath(), newName);
			mFileInfo = new FileInfo(newPath, mFileInfo.getLastModifiedDate(),
					mFileInfo.getSize(),
					mFileInfo.isFavorite());

			syncFooterView();
		}
	}

	private void syncFooterView()
	{
		mFooterView.setDocumentName(mFileInfo.getName());
		mFooterView.setDocumentLastModifiedDate(mFileInfo.getLastModifiedDate());
		mFooterView.setDocumentSize(mFileInfo.getSize());
		mFooterView.setDocumentIcon(mFileInfo.getIconResId());
	}

	private final View.OnClickListener mShareClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			mActionFactory.build(R.id.action_share).execute();
		}
	};

	private final BroadcastReceiver mResponseReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	private class PreviewLocalActionFactory extends BaseActionFactory
	{
		@Override
		public Action buildShareAction()
		{
			return new ShareAction(getFragmentManager(), mFileInfo);
		}

		@Override
		public Action buildSmartfileAction()
		{
			ArrayList<Path> path = new ArrayList<Path>(1);
			path.add(mFileInfo.getPath());
			return new SmarfileAction(PreviewLocalFragment.this, path);
		}

		@Override
		public Action buildRenameAction()
		{
			return new RenameAction(PreviewLocalFragment.this, mFileInfo.getName(), false,
					new HashMap<String, String>(0));
		}

		@Override
		public Action buildDeleteAction()
		{
			return new PreviewDeleteAction(getActivity(), mFileInfo.getPath());
		}

		@Override
		public Action buildPrintAction()
		{
			return new PrintAction(getActivity(), mFileInfo.getPath());
		}
	}
}
