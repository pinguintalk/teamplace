package net.teamplace.android.preview.ui;

import net.teamplace.android.preview.ui.controller.ActionController;
import net.teamplace.android.preview.ui.controller.CommentsController;
import net.teamplace.android.preview.ui.controller.ContentController;
import net.teamplace.android.preview.ui.controller.FileInfoController;
import net.teamplace.android.preview.ui.controller.VersionsController;
import android.app.Fragment;
import android.os.Handler;


public abstract class PreviewHolderFragment extends Fragment
{
	private ContentController mPreviewContentController;
	private FileInfoController mFileInfoController;
	private VersionsController mVersionsController;
	private CommentsController mCommentsController;
	private ActionController mActionController;

	/** Handler used for animations */
	private final Handler mAnimationHandler = new Handler();

	public PreviewHolderFragment()
	{
		super();
		mPreviewContentController = createPreviewContentController();
		mFileInfoController = createFileInfoController();
		mVersionsController = createVersionsController();
		mCommentsController = createCommentsController();
		mActionController = createActionController();
	}

	public final ContentController getPreviewContentController()
	{
		return mPreviewContentController;
	}

	public final FileInfoController getFileInfoController()
	{
		return mFileInfoController;
	}

	public final VersionsController getVersionsController()
	{
		return mVersionsController;
	}

	public final CommentsController getCommentsController()
	{
		return mCommentsController;
	}

	public final ActionController getActionController()
	{
		return mActionController;
	}

	public final Handler getAnimationHandler()
	{
		return mAnimationHandler;
	}

	protected abstract ContentController createPreviewContentController();

	protected abstract FileInfoController createFileInfoController();

	protected abstract VersionsController createVersionsController();

	protected abstract CommentsController createCommentsController();

	protected abstract ActionController createActionController();

}