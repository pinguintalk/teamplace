package net.teamplace.android.preview.comment;

public interface CommentOperationCallback
{
	public void deleteComment(String commentId);

	public boolean willCommentBeDeleted(String commentId);

	public void loadNextComments();
}
