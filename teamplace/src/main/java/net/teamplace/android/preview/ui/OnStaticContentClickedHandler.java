package net.teamplace.android.preview.ui;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.openin.ActivityResolver;
import net.teamplace.android.browsing.openin.ContentHelper;
import net.teamplace.android.browsing.openin.ResolvingActivityDialog;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.request.faulttolerant.download.DownloadRequester;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.GenericUtils;

public abstract class OnStaticContentClickedHandler
{

	public static OnStaticContentClickedHandler get(int type)
	{
		switch (type)
		{
			case OpenInHandler.TYPE:
				return new OpenInHandler();
			case VideoHandler.TYPE:
				return new VideoHandler();
			case AudioHandler.TYPE:
				return new AudioHandler();
			default:
				return null;
		}
	}

	public abstract void onContentClicked(Activity context, FileInfo masterDocument, Path displayingPath,
			long displayingDate);


	public static class OpenInHandler extends OnStaticContentClickedHandler
	{
		public static final int TYPE = 0;

		@Override
		public void onContentClicked(Activity context, FileInfo masterDocument, Path displayingPath, long displayingDate)
		{
			ResolvingActivityDialog dialog = ResolvingActivityDialog.newInstance(masterDocument,
					ActivityResolver.ViewOrShareResolver.TYPE);
			dialog.show(context.getFragmentManager(), ResolvingActivityDialog.TAG);
		}
	}

	public static class VideoHandler extends OnStaticContentClickedHandler
	{
		public static final int TYPE = 1;

		@Override
		public void onContentClicked(Activity context, FileInfo masterDocument, Path displayingPath, long displayingDate)
		{
			Intent i = new Intent(Intent.ACTION_VIEW);
			String mime = GenericUtils.getMimeType(GenericUtils.getExtension(masterDocument.getName()));
			if (Path.isLocalLocation(masterDocument.getPath().getLocation()))
			{
				i.setType(mime);
				ContentHelper.setFilePathContent(i, ((Path.Local) displayingPath).getAbsolutePathString(),
						ContentHelper.STRATEGY_DATA_URI);
			}
			else
			{
				// Not downloading anything here, just building the right URL
				DownloadRequester requester = new DownloadRequester(context,
						new SessionManager(context).getLastSession(), new TeamplaceBackend(),
						null, null);
				String teamId = PathUtils.getTeamDriveIdOrNull(masterDocument.getPath());

				try
				{
					Uri uri = requester.getDownloadUriWithAccessData(teamId,
							PathUtils.remoteRequestString(displayingPath.getParentPath()), displayingPath.getName());
					i.setDataAndType(uri, mime);
				}
				catch (TeamDriveNotFoundException e)
				{
					e.printStackTrace();
				}
			}
			try
			{
				context.startActivity(i);
			}
			catch (ActivityNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}

	public static class AudioHandler extends OnStaticContentClickedHandler
	{
		public static final int TYPE = 2;

		@Override
		public void onContentClicked(Activity context, FileInfo masterDocument, Path displayingPath, long displayingDate) {

			FragmentManager fm = context.getFragmentManager();
			ResolvingActivityDialog shareDialog = ResolvingActivityDialog.newInstance(masterDocument, ActivityResolver.ViewOrShareResolver.TYPE);
			shareDialog.show(fm, "");
		}
	}

}
