package net.teamplace.android.preview;

/** Interface for a component that can handle a back press */
public interface BackPressManager
{
	/** Called when a back press should be handled. Returns true if the back press was handled, false otherwise */
	boolean onBackPressed();
}
