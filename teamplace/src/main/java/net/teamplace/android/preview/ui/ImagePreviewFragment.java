package net.teamplace.android.preview.ui;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.preview.ui.controller.ActionController;
import net.teamplace.android.preview.ui.controller.BasicActionController;
import net.teamplace.android.preview.ui.controller.BasicCommentsController;
import net.teamplace.android.preview.ui.controller.BasicFileInfoController;
import net.teamplace.android.preview.ui.controller.BasicVersionsController;
import net.teamplace.android.preview.ui.controller.CommentsController;
import net.teamplace.android.preview.ui.controller.ContentController;
import net.teamplace.android.preview.ui.controller.FileInfoController;
import net.teamplace.android.preview.ui.controller.ImageContentController;
import net.teamplace.android.preview.ui.controller.VersionsController;
import net.teamplace.android.utils.Log;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cortado.android.R;

public class ImagePreviewFragment extends PreviewHolderFragment
{

	public static final String TAG = tag(ImagePreviewFragment.class);
	private static final String PKG = pkg(ImagePreviewFragment.class);

	private static final String KEY_INITIAL_FILE = PKG + ".mInitialFile";
	private static final String KEY_CONFIGURATION = PKG + ".mConfiguration";

	public static ImagePreviewFragment newInstance(FileInfo info, PreviewConfiguration configuration)
	{
		ImagePreviewFragment instance = new ImagePreviewFragment();
		Bundle args = new Bundle();
		args.putParcelable(KEY_INITIAL_FILE, info);
		args.putParcelable(KEY_CONFIGURATION, configuration);
		instance.setArguments(args);
		return instance;
	}

	@Override
	protected ContentController createPreviewContentController()
	{
		return new ImageContentController(this);
	}

	@Override
	protected FileInfoController createFileInfoController()
	{
		return new BasicFileInfoController(this);
	}

	@Override
	protected VersionsController createVersionsController()
	{
		return new BasicVersionsController(this);
	}

	@Override
	protected CommentsController createCommentsController()
	{
		return new BasicCommentsController(this);
	}

	@Override
	protected ActionController createActionController()
	{
		return new BasicActionController(this);
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		getActionController().onAttach(activity);
	}

	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getActionController().onCreate(savedInstanceState);
		if (savedInstanceState == null)
		{
			Bundle args = getArguments();
			FileInfo masterDoc = args.getParcelable(KEY_INITIAL_FILE);
			PreviewConfiguration configuration = args.getParcelable(KEY_CONFIGURATION);
			getFileInfoController().setPreviewConfiguration(configuration);
			getCommentsController().setPreviewConfiguration(configuration);
			getFileInfoController().setMasterDocument(masterDoc);
			getFileInfoController().setDisplayingPath(masterDoc.getPath());
			getFileInfoController().setDisplayingDate(masterDoc.getLastModifiedDate());
		}
		getFileInfoController().onCreate(savedInstanceState);
		getCommentsController().onCreate(savedInstanceState);
		getPreviewContentController().onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.prv_image_preview_fragment, container, false);

		getFileInfoController().setFragmentView(root);
		getFileInfoController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().setFragmentView(root);
		getVersionsController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().onUiUpdateRequested();

		getActionController().setFragmentView(root);
		getActionController().onCreateView(inflater, container, savedInstanceState);

		getCommentsController().setFragmentView(root);
		getCommentsController().onCreateView(inflater, container, savedInstanceState);

		getPreviewContentController().setFragmentView(root);
		getPreviewContentController().onCreateView(inflater, container, savedInstanceState);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getVersionsController().onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getVersionsController().onResume();
		getActionController().onResume();
		getCommentsController().onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getVersionsController().onPause();
		getActionController().onPause();
		getCommentsController().onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		getPreviewContentController().onSaveInstanceState(outState);
		getActionController().onSaveInstanceState(outState);
		getFileInfoController().onSaveInstanceState(outState);
		getVersionsController().onSaveInstanceState(outState);
		getCommentsController().onSaveInstanceState(outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}
		getActionController().onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		getActionController().onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		getActionController().onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return getActionController().onOptionsItemSelected(item);
	}

}
