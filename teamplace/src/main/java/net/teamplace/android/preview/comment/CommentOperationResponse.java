package net.teamplace.android.preview.comment;

import java.util.List;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.Comment;
import android.os.Bundle;

public class CommentOperationResponse
{
	private final String TAG = getClass().getSimpleName();

	private int responseCode;
	private ChangedCommentResponse changeCommentResponse;
	private List<Comment> newCommentList;
	private boolean checkChanges;
	private int newCommentNumber;
	private Path.TeamDrive filePath;
	private Bundle extras;

	public CommentOperationResponse(int responseCode, ChangedCommentResponse changeCommentResponse,
			List<Comment> newCommentList, boolean checkChanges, int newCommentNumber, Path.TeamDrive filePath,
			Bundle extras)
	{
		this.responseCode = responseCode;
		this.changeCommentResponse = changeCommentResponse;
		this.newCommentList = newCommentList;
		this.checkChanges = checkChanges;
		this.newCommentNumber = newCommentNumber;
		this.filePath = filePath;
		this.extras = extras;
	}

	public int getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}

	public ChangedCommentResponse getChangeCommentResponse()
	{
		return changeCommentResponse;
	}

	public void setChangeCommentResponse(ChangedCommentResponse changeCommentResponse)
	{
		this.changeCommentResponse = changeCommentResponse;
	}

	public List<Comment> getNewCommentList()
	{
		return newCommentList;
	}

	public void setNewCommentList(List<Comment> newCommentList)
	{
		this.newCommentList = newCommentList;
	}

	public boolean isCheckChanges()
	{
		return checkChanges;
	}

	public void setCheckChanges(boolean checkChanges)
	{
		this.checkChanges = checkChanges;
	}

	public int getNewCommentNumber()
	{
		return newCommentNumber;
	}

	public void setNewCommentNumber(int newCommentNumber)
	{
		this.newCommentNumber = newCommentNumber;
	}

	public Path.TeamDrive getFilePath()
	{
		return filePath;
	}

	public void setFilePath(Path.TeamDrive filePath)
	{
		this.filePath = filePath;
	}

	public Bundle getExtras()
	{
		return extras;
	}

	public void setExtras(Bundle extras)
	{
		this.extras = extras;
	}
}
