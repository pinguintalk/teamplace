package net.teamplace.android.preview.ui.controller;

public interface VersionsController extends PreviewController
{
	public void onUiUpdateRequested();

	public void onReloadVersionsRequested();

	public void clearVersionUi();

	public int getmVersionNumberOfMasterDoc();

}
