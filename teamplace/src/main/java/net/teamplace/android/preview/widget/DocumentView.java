package net.teamplace.android.preview.widget;

import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.utils.GraphicsUtils;
import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.drawable.NinePatchDrawable;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewConfiguration;
import android.widget.OverScroller;

import com.cortado.android.R;

/**
 * Represents a document. Basically similar to ListView with zooming
 * 
 * @author Gil Vegliach
 */
public class DocumentView extends View
{
	public static final String TAG = tag(DocumentView.class);

	private static final int SHADOW_ALPHA = 130;
	private static final float SIDE_PADDING_FACTOR = .025f;
	private static final float TOP_PADDING_DP = 5.f;
	private static final float MAX_SCALE_FACTOR = 10.f;
	private static final float MAX_INITIAL_SCALE = 1.f;
	private static final int INVALID_POINTER_ID = -1;

	/** How larger is the large snap scale wrt the the small snap scale */
	private static final float SNAP_SCALE_FACTOR = 2.f;

	private int mActivePointerId;
	private float mActivePointerX;
	private float mActivePointerY;
	private float mLastTouchX;
	private float mLastTouchY;

	/** The position in the dataset of the first page visible on screen */
	private int mFirstVisiblePage;

	/**
	 * How many pages are visible on screen. The value -1 indicates that this has not been calculated yet. See {@link
	 * computeVisiblePageCount()}
	 */
	private int mVisiblePagesCount;

	private float mFitToWidthScale;
	private float mFitToPageScale;

	/** Baseline scales for drawing the pages */
	private float mBaselineScale;

	/** Orientation-almost invariant baseline scaling for top and side padding */
	private float mTopPaddingBaselineScale;

	private float mMinScale;
	private float mMaxScale;

	/** Smaller snap scale for double tap */
	private float mSmallSnapScale;

	/** Larger snap scale for double tap */
	private float mLargeSnapScale;

	/** Current scale */
	private float mScale;

	/** Top padding in px at {@link mPaddingBaselineScale} */
	private float mTopPadding;

	/**
	 * Side padding used at {@link mBaselineScale}. This will be scaled up and down by the method
	 * {@link #getScaledSidePadding() }
	 */
	private float mSidePadding;

	/** Top coordinate of the first visible page in px (before scaling) */
	private float mFirstVisiblePageTop;

	/** Left coordinate of the first visible page in px (before scaling) */
	private float mFirstVisiblePageLeft;

	/** Width of this View in px */
	private int mWidth;

	/** Height of this View in px */
	private int mHeight;

	private final OverScroller mScroller;
	private int mScrollY;
	private int mScrollX;
	private boolean mIsScrolling;
	private final int mTouchSlop;

	private final NinePatchDrawable mShadow;
	private static RectF sBorderRectF = new RectF();
	private final Paint mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

	private DocumentAdapter mAdapter;
	private OnPageChangedListener mPageChangedListener;

	/**
	 * Instance state used to restore current page number, scale and focal center. Used and cleared in
	 * {@link #onSizeChanged(int, int, int, int)}
	 */
	private SavedState mInstanceState;

	public DocumentView(Context context)
	{
		this(context, null);
	}

	public DocumentView(Context context, AttributeSet attrs)
	{
		this(context, attrs, R.attr.documentViewStyle);
	}

	public DocumentView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		mScroller = new OverScroller(context);
		mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
		setWillNotDraw(false);

		mBorderPaint.setColor(Color.RED);
		mBorderPaint.setStrokeWidth(0);
		mBorderPaint.setStyle(Style.STROKE);

		mShadow = (NinePatchDrawable) context.getResources().getDrawable(R.drawable.prv_document_view_shadow);
		mShadow.setAlpha(SHADOW_ALPHA);

		// Initialize fields
		mActivePointerId = INVALID_POINTER_ID;
		mFirstVisiblePage = 0;
		mScrollY = 0;
		mScrollX = 0;
		mIsScrolling = false;
		mVisiblePagesCount = -1;
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		// If the new measurements are the same as the old ones, just terminates
		if (w == oldw && h == oldh)
		{
			return;
		}

		// Otherwise updates members and re-compute the scales
		mWidth = w;
		mHeight = h;

		computeScalesAndPadding(mFirstVisiblePage, false);

		mFirstVisiblePageTop = getScaledTopPadding(mFitToWidthScale);
		mFirstVisiblePageLeft = mSidePadding;

		// Before calling this method mScale, mFirstVisiblePage, and the related fields must be
		// up-to-date
		updateVisiblePagesCount();

		// Restore instance state, if this is the first layout
		if (mInstanceState != null)
		{
			FocalCenterState state = mInstanceState.toFocalCenterState();
			state.cropScaleToBounds(mMinScale, mMaxScale);
			mInstanceState = null;

			restoreFocalCenterState(state);
		}
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		float topEdge = mFirstVisiblePageTop;
		for (int i = 0; i < mVisiblePagesCount; i++)
		{
			int pos = mFirstVisiblePage + i;
			DocumentPage page = mAdapter.getPage(pos);

			// The first page left coordinate is remembered in mFirstVisiblePageLeft. All other
			// pages are aligned such that the horizontal center of the first page is the same
			// as the horizontal center of all other pages.
			// ...
			// ... left_0 + (scaledWidth_0 / 2) = left_pos + (scaledWidth_pos / 2)
			// ...
			// <->
			// ...
			// ... left_pos = left_0 + (scaledWidth_0 - scaledWidth_pos) / 2
			// ...
			float leftEdge = (i == 0) ? mFirstVisiblePageLeft : mFirstVisiblePageLeft
					+ (getScaledWidth(mFirstVisiblePage) - getScaledWidth(pos)) / 2.f;

			canvas.save();
			canvas.translate(leftEdge, topEdge);
			canvas.scale(mScale, mScale, 0.f, 0.f);
			drawPage(canvas, page);
			canvas.restore();

			topEdge += getScaledTopPadding() + (page.getHeight() * mScale);
		}
	}

	@Override
	protected Parcelable onSaveInstanceState()
	{
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.copyFrom(saveFocalCenterState());
		return ss;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable state)
	{
		if (!(state instanceof SavedState))
		{
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		mInstanceState = ss;
	}

	@Override
	public void computeScroll()
	{
		super.computeScroll();

		if (mScroller.computeScrollOffset())
		{
			int prevY = mScrollY;
			int prevX = mScrollX;
			mScrollY = mScroller.getCurrY();
			mScrollX = mScroller.getCurrX();

			onDrag(mScrollX - prevX, mScrollY - prevY);
			ViewCompat.postInvalidateOnAnimation(this);
		}
	}

	/** Updates internal scale and padding values using the given page as a guideline */
	public void computeScalesAndPadding(int page, final boolean keepFocalCenter)
	{
		if (mAdapter == null || mAdapter.getCount() < page + 1)
		{
			return;
		}

		// TODO: experimental code
		// (jobol) Yes, very experimental and buggy. Hence, I deactivated the preservation of scale factor during
		// orientation change.
		// final float oldScaleFactor = mScale / mBaselineScale;
		FocalCenterState state = null;
		if (keepFocalCenter)
		{
			state = saveFocalCenterState();
		}

		mTopPadding = GraphicsUtils.dpToPx(getContext(), TOP_PADDING_DP);

		final DocumentPage b = mAdapter.getPage(page);
		final int bitmapWidth = b.getWidth();
		final int bitmapHeight = b.getHeight();
		final float availableWidth = mWidth - (2.f * SIDE_PADDING_FACTOR * mWidth);
		final float availableHeight = mHeight - (2.f * mTopPadding);

		// avoid initially scaling up images
		mFitToWidthScale = Math.min((availableWidth / bitmapWidth), MAX_INITIAL_SCALE);
		final float fitToHeightScale = availableHeight / bitmapHeight;
		final float bitmapScaledWidth = bitmapWidth * mFitToWidthScale;

		// By definition mFitToPageScale is always smaller than mFitToWidthScale
		mFitToPageScale = Math.min(fitToHeightScale, mFitToWidthScale);

		mMinScale = Math.min(mFitToPageScale, 1.f);
		mSmallSnapScale = mFitToWidthScale;
		mLargeSnapScale = mSmallSnapScale * SNAP_SCALE_FACTOR;
		mMaxScale = mMinScale * MAX_SCALE_FACTOR;

		// Base line scale >= minimum scale
		mBaselineScale = mFitToWidthScale;

		// Set current scale to the baseline scale
		mScale = mBaselineScale;

		// Between pages we draw a small padding. The padding scales up and down according to
		// the current scale. Logically speaking, the scale factor of the padding should be 1.f
		// when the view is displayed for the first time, i.e. the baseline for the padding should
		// be the baseline scale. But unfortunately this depends on the orientation as it is
		// practically the fit-to-width scale. If we were to use this scale, then when rotating the
		// device the padding would change size whilst the pages would retain their dimensions.
		// That looks weird and therefore we want the baseline to be constant between orientations.
		// Unfortunately, we cannot know the dimensions of the view in the other orientation, but
		// we can estimate the other fit-to-width scale using the current height as width. After
		// this, we take as baseline scale the minimum between the fit-to-width scale and the
		// fit-to-width scale in the other orientation: this means we will have a scaled padding no
		// smaller than mTopPadding (5dp). This baseline scale is almost constant across orientation
		// changes. The little variation is due to a change in layout of the view, e.g. because
		// the notification bar takes up some pixels
		final float otherOrientationAvailWidth = mHeight - (2.f * SIDE_PADDING_FACTOR * mHeight);
		final float otherOrientationFitToWidthScale = otherOrientationAvailWidth / (bitmapWidth);
		mTopPaddingBaselineScale = Math.min(mFitToWidthScale, otherOrientationFitToWidthScale);

		mSidePadding = (mWidth - bitmapScaledWidth) / 2.f;

		if (keepFocalCenter)
		{
			// Update the scale with the new value
			float oldScale = state.scale;
			float newScale = /* oldScaleFactor * */mBaselineScale;
			float offsetFactor = newScale / oldScale;

			// Basically scales about the focal center
			state.scale = newScale;
			state.dx *= offsetFactor;
			state.dy *= offsetFactor;
			restoreFocalCenterState(state);
		}
	}

	/** Returns the number of visible pages */
	public int getVisiblePagesCount()
	{
		return mVisiblePagesCount;
	}

	/** Returns the position of the first visible page */
	public int getFirstVisiblePage()
	{
		return mFirstVisiblePage;
	}

	/**
	 * Returns the perceived current page. This is the page that takes most space on screen. The value -1 indicates that
	 * the current page is not currently available, for instance because the dimensions have not yet been calculated
	 * (onSizeChanged) and thus neither the scales and the number of visible pages
	 */
	public int getCurrentPage()
	{
		if (mVisiblePagesCount <= 0)
		{
			return -1;
		}

		if (mVisiblePagesCount == 1)
		{
			return mFirstVisiblePage;
		}

		// We now have at least two pages
		final float scaledTopPadding = getScaledTopPadding();
		final int count = mVisiblePagesCount;

		float weights[] = new float[count];
		float totalVisibleHeight = -scaledTopPadding;

		for (int i = 0; i < count; i++)
		{
			DocumentPage page = mAdapter.getPage(mFirstVisiblePage + i);
			float h = page.getHeight() * mScale;

			totalVisibleHeight += h + scaledTopPadding;
			weights[i] = h;
		}

		// Adjust first and last images: they might be partially hidden
		if (mFirstVisiblePageTop < 0)
		{
			weights[0] += mFirstVisiblePageTop;
		}

		final float bottomEdge = totalVisibleHeight + mFirstVisiblePageTop;

		if (bottomEdge > mHeight)
		{
			weights[count - 1] -= bottomEdge - mHeight;
		}

		// Now calculate the index for the maximum weight
		int index = 0;
		float max = weights[0];

		for (int i = 0; i < count; i++)
		{
			if (weights[i] > max)
			{
				max = weights[i];
				index = i;
			}
		}

		return mFirstVisiblePage + index;
	}

	// TODO: @giveg check this
	public void setAdapter(DocumentAdapter adapter)
	{
		mAdapter = adapter;
		if (mAdapter.getCount() > 0)
		{
			mFirstVisiblePage = 0;
			computeScalesAndPadding(0, false);
			updateVisiblePagesCount();
		}
		invalidate();
	}

	public boolean isPageVisible(int position)
	{
		return mFirstVisiblePage <= position && position < (mFirstVisiblePage + mVisiblePagesCount);
	}

	public void setOnPageChangedListener(OnPageChangedListener listener)
	{
		mPageChangedListener = listener;
	}

	/**
	 * Sets the first visible page to be page, or performs adjustments if need be; no animation, no change in side
	 * position (left), no change in scale. Page is a position in the adapter, from 0 to mAdapter.getCount() - 1
	 */
	public void scrollToPage(int page)
	{
		final int count = mAdapter.getCount();

		if (page < 0 || page >= count)
		{
			throw new IndexOutOfBoundsException("page must be between 0 and Adapter.getCount() - 1");
		}

		final float scaledTopPadding = getScaledTopPadding();
		final int oldCurrPage = getCurrentPage();

		// First we try to stick the given page top edge to the top edge of the screen. If we can
		// fill down the screen with images we are done
		boolean canFill = false;
		float totalHeight = scaledTopPadding;

		for (int i = page; i < count; i++)
		{
			final float h = mAdapter.getPage(i).getHeight() * mScale;
			totalHeight += h + scaledTopPadding;

			if (totalHeight >= mHeight)
			{
				canFill = true;
				break;
			}
		}

		// If the images cannot fill down the screen, try sticking the last page to the bottom edge
		// and fill up to the top edge of the screen.
		float top = scaledTopPadding;
		if (!canFill && page > 0)
		{
			for (page--; page >= 0; page--)
			{
				float h = mAdapter.getPage(page).getHeight() * mScale;
				totalHeight += h + scaledTopPadding;

				if (totalHeight >= mHeight)
				{
					top = mHeight - totalHeight + scaledTopPadding;
					break;
				}
			}
		}

		// Now finally set the first visible page and related fields. If we could fill neither up
		// nor down, stick page to the top edge of the screen
		mFirstVisiblePage = page;
		mFirstVisiblePageTop = top;
		updateVisiblePagesCount();

		invalidate();

		int currPage = getCurrentPage();
		if (currPage != oldCurrPage && mPageChangedListener != null)
		{
			mPageChangedListener.onPageChanged(oldCurrPage, currPage);
		}
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent ev)
	{
		switch (ev.getActionMasked())
		{
			case MotionEvent.ACTION_DOWN:
			{
				mActivePointerId = ev.getPointerId(0);
				mLastTouchX = ev.getX(0);
				mLastTouchY = ev.getY(0);
				mActivePointerX = ev.getX();
				mActivePointerY = ev.getY();
				mScroller.forceFinished(true);
				mIsScrolling = false;
				break;
			}
			case MotionEvent.ACTION_MOVE:
			{
				int pointerIndex = ev.findPointerIndex(mActivePointerId);
				final float x = ev.getX(pointerIndex);
				final float y = ev.getY(pointerIndex);

				// Disables dragging while scaling, like the QuickOffice app but differently from
				// Acrobat Reader
				if (!mScaleDetector.isInProgress())
				{
					onDrag(x - mLastTouchX, y - mLastTouchY);
				}

				// Update last touch coordinates
				mLastTouchX = x;
				mLastTouchY = y;

				// Check if the user is panning
				if (!mIsScrolling)
				{
					double dst = Math.sqrt((mActivePointerX - x) * (mActivePointerX - x) + (mActivePointerY - y)
							* (mActivePointerY - y));

					if (dst > mTouchSlop)
					{
						mIsScrolling = true;
					}
				}

				break;
			}
			case MotionEvent.ACTION_UP:
			case MotionEvent.ACTION_CANCEL:
			{
				mActivePointerId = INVALID_POINTER_ID;
				break;
			}
			case MotionEvent.ACTION_POINTER_UP:
			{
				int pointerIndex = ev.getActionIndex();
				if (ev.getPointerId(pointerIndex) == mActivePointerId)
				{
					int newPointerIndex = pointerIndex == 0 ? 1 : 0;
					mActivePointerId = ev.getPointerId(newPointerIndex);
					mLastTouchX = ev.getX(newPointerIndex);
					mLastTouchY = ev.getY(newPointerIndex);
					mActivePointerX = ev.getX(newPointerIndex);
					mActivePointerY = ev.getY(newPointerIndex);
				}
				break;
			}
		}

		mGestureDetector.onTouchEvent(ev);
		mScaleDetector.onTouchEvent(ev);

		return true;
	}

	public void onDrag(float dx, float dy)
	{
		if (dy == 0 && dx == 0)
		{
			return;
		}

		if (setPositionAndCorrectEdges(mFirstVisiblePageLeft + dx, mFirstVisiblePageTop + dy, true))
		{
			invalidate();
		}
	}

	private void doScale(float px, float py, float scaleFactor)
	{
		float newScale = mScale * scaleFactor;

		// If newScale is out of bounds, correct it appropriately
		if (newScale < mMinScale)
		{
			newScale = mMinScale;
			scaleFactor = mMinScale / mScale;
		}
		else if (newScale > mMaxScale)
		{
			newScale = mMaxScale;
			scaleFactor = mMaxScale / mScale;
		}

		final float newLeft = scaleFactor * (mFirstVisiblePageLeft - px) + px;
		final float newTop = scaleFactor * (mFirstVisiblePageTop - py) + py;

		boolean shouldInvalidate = (mScale != newScale);

		// Sets scale and (left, top) position
		mScale = newScale;
		shouldInvalidate |= setPositionAndCorrectEdges(newLeft, newTop, true);

		if (shouldInvalidate)
		{
			invalidate();
		}
	}

	/** Draws a page into the given canvas, with all the decorations */
	private DocumentPage drawPage(Canvas canvas, DocumentPage page)
	{
		sBorderRectF.top = 0.f;
		sBorderRectF.left = 0.f;
		sBorderRectF.bottom = page.getHeight();
		sBorderRectF.right = page.getWidth();

		// Draws shadow. The magic number 8 has been found by trial and error, it should
		// actually be 6
		Context ctx = getContext();
		float shadowPadding = GraphicsUtils.dpToPx(ctx, 8);
		int w = Math.round(sBorderRectF.width() + 2.f * shadowPadding);
		int h = Math.round(sBorderRectF.height() + 2.f * shadowPadding);

		canvas.save();
		canvas.translate(-shadowPadding, -shadowPadding);
		mShadow.setBounds(0, 0, w, h);
		// TODO: This shadow causes a +1X overdraw on the image! It should be optimized tiling it around the rectangle
		// (which creates problems with the 9 patch stretching logic)
		mShadow.draw(canvas);
		canvas.restore();

		page.draw(canvas);
		// Uncomment to draw a border around the page
		// canvas.drawRect(sBorderRectF, mBorderPaint);

		return page;
	}

	private FocalCenterState saveFocalCenterState()
	{
		final float cx = mWidth / 2.f;
		final float cy = mHeight / 2.f;
		final float left = mFirstVisiblePageLeft;
		final float scaledTopPadding = getScaledTopPadding();
		final float dx = left - cx;

		// Dummy values
		float top = mFirstVisiblePageTop;
		float dy = 0.f;

		// Initialize first visible page and its bottom edge: this is calculated as if it were the
		// previous page, even if we start from page 0
		int page = mFirstVisiblePage;
		float bottom = mFirstVisiblePageTop - scaledTopPadding;

		for (int i = 0; i < mVisiblePagesCount; i++)
		{
			page = mFirstVisiblePage + i;

			// Top of the next page
			top = bottom + scaledTopPadding;

			// If the focal center is between two pages, choose the one above. This check is likely to
			// fail during the first iteration because the center has a large positive coordinate
			// and top then is == mFirstVisiblePageTop
			if (bottom < cy && cy < top)
			{
				dy = top - cy;
				break;
			}

			// Bottom of the next page
			bottom = top + getScaledHeight(page);

			if (top <= cy && cy <= bottom)
			{
				dy = top - cy;
				break;
			}
		}

		return new FocalCenterState(page, mScale, dx, dy);
	}

	// TODO: update docs
	/**
	 * Sets page, scale and focal center. Used during the restoration from a previous instance
	 * 
	 * @param mPage
	 *            pivot page
	 * @param scale
	 *            to be restored
	 * @param dx
	 *            offset of left coordinate of page from the focal center (dx = left - cx)
	 * @param dy
	 *            offset of top coordinate of page from the focal center (dy = top - cy)
	 */
	private void restoreFocalCenterState(FocalCenterState state)
	{
		final int page = state.page;
		final float scale = state.scale;
		final float dx = state.dx;
		final float dy = state.dy;
		final int count = mAdapter.getCount();

		if (page < 0 || page >= count)
		{
			throw new IndexOutOfBoundsException("Page must be between 0 and Adapter.getCount() - 1, was: " + page);
		}

		final int oldCurrPage = getCurrentPage();
		final float scaledTopPadding = getScaledTopPadding(scale);
		final float cy = mHeight / 2.f;

		// First if there is a gap between top and the top edge, try to fill up
		int firstPage = page;
		float topEdge = cy + dy;
		boolean canFill = (topEdge <= scaledTopPadding);

		while (firstPage > 0 && !canFill)
		{
			// Start from one page above
			firstPage--;

			final float h = mAdapter.getPage(firstPage).getHeight() * scale;
			topEdge -= (h + scaledTopPadding);

			if (topEdge <= scaledTopPadding)
			{
				canFill = true;
			}
		}

		// Now firstPage is either 0 or the correct first visible page. Set it to a neutral position
		// then shift it and correct the edges
		mScale = scale;
		mFirstVisiblePage = firstPage;
		mFirstVisiblePageTop = scaledTopPadding;
		mFirstVisiblePageLeft = getScaledSidePadding();
		updateVisiblePagesCount();

		final float leftEdge = (mWidth / 2.f) + dx;
		setPositionAndCorrectEdges(leftEdge, topEdge, false);

		invalidate();

		int currPage = getCurrentPage();
		if (currPage != oldCurrPage && mPageChangedListener != null)
		{
			mPageChangedListener.onPageChanged(oldCurrPage, currPage);
		}
	}

	/**
	 * Sets left and top coordinate of the first visible image using current mScale to detect the edges. The field
	 * mScale must be up-to-date before calling this method. The edge detection assumes that all pages are of the same
	 * size.
	 */
	private boolean setPositionAndCorrectEdges(float newLeft, float newTop, boolean fireCallbacks)
	{
		boolean shouldInvalidate = false;
		DocumentPage firstVisible = mAdapter.getPage(mFirstVisiblePage);
		float firstVisibleHeight = firstVisible.getHeight() * mScale;

		final float scaledTopPadding = getScaledTopPadding();
		final float oldLeft = mFirstVisiblePageLeft;
		final float oldTop = mFirstVisiblePageTop;
		final int oldCurrPage = getCurrentPage();
		final boolean canFillDown = canFillDown(0);

		// First page top edge detection: scrolling down is not allowed on the first page. Also, if
		// the view cannot be filled up by pages, the first page should be fixed and thus cannot
		// scroll up either
		if (mFirstVisiblePage == 0)
		{
			if (newTop > scaledTopPadding || !canFillDown)
			{
				newTop = scaledTopPadding;
			}
		}

		// Last page bottom Edge detection: if the last visible page is the last page we have, and
		// we can fill up the view with pages starting from the first one (0), then stick the bottom
		// edge to the edge of the view, correcting for the bottom padding
		final int lastVisiblePosition = mFirstVisiblePage + mVisiblePagesCount - 1;
		if (lastVisiblePosition == mAdapter.getCount() - 1 && canFillDown)
		{
			final float totalVisibleHeight = getHeightVisiblePages();
			final float bottomEdge = totalVisibleHeight + newTop;

			if (bottomEdge < mHeight - scaledTopPadding)
			{
				newTop = mHeight - scaledTopPadding - totalVisibleHeight;
			}
		}

		// Adjusts first visible page, if page is coming from bottom
		if (newTop + firstVisibleHeight < 0)
		{
			mFirstVisiblePage++;

			if (mFirstVisiblePage >= mAdapter.getCount())
			{
				throw new IllegalStateException("First page is over Adapter.getCount(). First page: "
						+ mFirstVisiblePage + "; adapter count: " + mAdapter.getCount());
			}

			shouldInvalidate = true;
			newTop += firstVisibleHeight + scaledTopPadding;

			// Updates local variable used in later computation
			firstVisible = mAdapter.getPage(mFirstVisiblePage);
			firstVisibleHeight = firstVisible.getHeight() * mScale;
		}

		// ... and if page is coming from top
		if (newTop > scaledTopPadding)
		{
			mFirstVisiblePage--;

			if (mFirstVisiblePage < 0)
			{
				throw new IllegalStateException("First page is negative. First page: " + mFirstVisiblePage);
			}

			shouldInvalidate = true;

			firstVisible = mAdapter.getPage(mFirstVisiblePage);
			firstVisibleHeight = firstVisible.getHeight() * mScale;

			newTop -= firstVisibleHeight + scaledTopPadding;
		}

		shouldInvalidate |= (newTop != oldTop);

		// Adjusts top of the first page
		mFirstVisiblePageTop = newTop;

		updateVisiblePagesCount();

		// Horizontal correction
		float firstVisibleWidth = firstVisible.getWidth() * mScale;
		float availableWidth = mWidth - firstVisibleWidth;

		if (availableWidth > 0)
		{
			// Page narrower than the view
			newLeft = availableWidth / 2.f;
		}
		else
		{
			// Page is wider than the view
			float scaledSidePadding = getScaledSidePadding();

			// Left edge snapping
			if (newLeft > scaledSidePadding)
			{
				newLeft = scaledSidePadding;
			}

			// Right edge snapping
			if (newLeft + firstVisibleWidth < mWidth - scaledSidePadding)
			{
				newLeft = mWidth - scaledSidePadding - firstVisibleWidth;
			}
		}

		shouldInvalidate |= (newLeft != oldLeft);

		mFirstVisiblePageLeft = newLeft;

		// Notify listener
		final int newCurrPage = getCurrentPage();
		if (oldCurrPage != newCurrPage && mPageChangedListener != null && fireCallbacks)
		{
			mPageChangedListener.onPageChanged(oldCurrPage, newCurrPage);
		}

		return shouldInvalidate;
	}

	/**
	 * Adjusts mVisiblePagesCount. It assumes that mScale, mFirstVisiblePage, and the related fields are up-to-date
	 */
	private void updateVisiblePagesCount()
	{
		final float scaledPadding = getScaledTopPadding();
		final int oldCount = mAdapter.getCount();
		float bottom = mFirstVisiblePageTop;
		int newCount = 0;

		while (bottom < mHeight && mFirstVisiblePage + newCount < oldCount)
		{
			DocumentPage b = mAdapter.getPage(mFirstVisiblePage + newCount);
			bottom += b.getHeight() * mScale + scaledPadding;
			newCount++;
		}

		mVisiblePagesCount = newCount;
	}

	/**
	 * Returns whether there are enough pages to fill down the screen from the given page. There is padding at the top,
	 * at the bottom and between each two pages
	 */
	private boolean canFillDown(int page)
	{
		final int count = mAdapter.getCount();
		final float scaledTopPadding = getScaledTopPadding();

		float totalHeight = scaledTopPadding;

		for (int i = page; i < count; i++)
		{
			totalHeight += getScaledHeight(i) + scaledTopPadding;

			if (totalHeight >= mHeight)
			{
				return true;
			}
		}

		return false;
	}

	private boolean canPanHorizontally()
	{
		final float w = mAdapter.getPage(mFirstVisiblePage).getWidth() * mScale;
		return w > mWidth;
	}

	/** Returns size of top padding in pixel wrt {@code scale} */
	private float getScaledTopPadding(float scale)
	{
		return mTopPadding * (scale / mTopPaddingBaselineScale);
	}

	/** Returns size of top padding in pixel wrt the current scale */
	private float getScaledTopPadding()
	{
		return getScaledTopPadding(mScale);
	}

	/** Returns size of side padding in pixel wrt the current scale */
	private float getScaledSidePadding()
	{
		// The side padding must be equal to mSidePadding when we first load the first page, i.e. at
		// scale mBaselineScale
		return mSidePadding * (mScale / mBaselineScale);
	}

	private float getScaledHeight(int page)
	{
		return mAdapter.getPage(page).getHeight() * mScale;
	}

	private float getScaledWidth(int page)
	{
		return mAdapter.getPage(page).getWidth() * mScale;
	}

	/** Calculates total height of all visible pages, padding in between included */
	private float getHeightVisiblePages()
	{
		final float scaledTopPadding = getScaledTopPadding();
		float totalVisibleHeight = -scaledTopPadding;

		for (int i = 0; i < mVisiblePagesCount; i++)
		{
			totalVisibleHeight += getScaledHeight(mFirstVisiblePage + i) + scaledTopPadding;
		}

		return totalVisibleHeight;
	}

	private final GestureDetector mGestureDetector = new GestureDetector(getContext(),
			new GestureDetector.SimpleOnGestureListener()
			{
				@Override
				public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
				{
					mScroller.forceFinished(true);
					mScrollY = 0;
					mScrollX = 0;

					int minY, maxY;
					if (velocityY > 0)
					{
						minY = 0;
						maxY = Integer.MAX_VALUE;
					}
					else
					{
						minY = Integer.MIN_VALUE;
						maxY = 0;
					}

					int minX, maxX;
					if (!canPanHorizontally())
					{
						velocityX = 0.f;
						minX = 0;
						maxX = 0;
					}
					else if (velocityX > 0)
					{
						minX = 0;
						maxX = Integer.MAX_VALUE;
					}
					else
					{
						minX = Integer.MIN_VALUE;
						maxX = 0;
					}

					mScroller.fling(0, 0, Math.round(velocityX), Math.round(velocityY), minX, maxX, minY, maxY);

					ViewCompat.postInvalidateOnAnimation(DocumentView.this);
					return true;
				}

				@Override
				public boolean onDoubleTapEvent(MotionEvent e)
				{
					// We zoom in or out only when the finger is lifted up from the second tap
					// because otherwise we could be in the middle of a quick scale gesture
					if (e.getActionMasked() == MotionEvent.ACTION_UP)
					{
						// If a quick gesture is in progress or the user is dragging the document,
						// then disable double tap
						if (mIsScrolling || mScaleDetector.isInProgress())
						{
							return true;
						}

						final int px = (int) e.getX();
						final int py = (int) e.getY();

						// If the scale is smaller than the larger of the snap scales, we zoom in to
						// the large snap scale. Otherwise we zoom out to the small snap scale
						float endScale = mScale < mLargeSnapScale ? mLargeSnapScale : mSmallSnapScale;

						ValueAnimator anim = ValueAnimator.ofFloat(mScale, endScale);
						anim.addUpdateListener(new AnimatorUpdateListener()
						{
							@Override
							public void onAnimationUpdate(ValueAnimator animation)
							{
								float updateScale = (Float) animation.getAnimatedValue();
								float updateScaleFactor = updateScale / mScale;

								// Always same pivot
								doScale(px, py, updateScaleFactor);
							}
						});
						anim.start();
					}

					return true;
				};

				@Override
				public boolean onDown(MotionEvent e)
				{
					return true;
				}
			});

	private final ScaleGestureDetector mScaleDetector = new ScaleGestureDetector(getContext(),
			new ScaleGestureDetector.SimpleOnScaleGestureListener()
			{
				@Override
				public boolean onScale(ScaleGestureDetector detector)
				{
					float scaleFactor = detector.getScaleFactor();
					if (scaleFactor == 1.f)
					{
						return true;
					}

					float px = detector.getFocusX();
					float py = detector.getFocusY();
					DocumentView.this.doScale(px, py, scaleFactor);

					return true;
				}
			});

	/**
	 * Represents the focal center, i.e. information where the user perceives the center in the View. It contains the
	 * page on which the View center falls on, this page scale, and this page left/top offset from the View center.
	 */
	private static class FocalCenterState
	{
		/**
		 * Page on which the center of the view falls on, or the page just above it if the center falls on a gap
		 */
		int page;

		/** Scale for page */
		float scale;

		/** Offset of page left coordinate from the focal center (dx = left - cx) */
		float dx;

		/** Offset of page top coordinate from the focal center (dy = top - cy) */
		float dy;

		FocalCenterState(int page, float scale, float dx, float dy)
		{
			this.page = page;
			this.scale = scale;
			this.dx = dx;
			this.dy = dx;
		}

		void cropScaleToBounds(float min, float max)
		{
			if (scale < min)
			{
				scale = min;
			}

			if (scale > max)
			{
				scale = max;
			}
		}
	}

	public static class SavedState extends BaseSavedState
	{
		int page;
		float scale;
		float dx;
		float dy;

		public SavedState(Parcelable superState)
		{
			super(superState);
		}

		public SavedState(Parcel in)
		{
			super(in);
			page = in.readInt();
			scale = in.readFloat();
			dx = in.readFloat();
			dy = in.readFloat();
		}

		// Eclipse implementation, needed for testing
		@Override
		public int hashCode()
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + Float.floatToIntBits(dx);
			result = prime * result + Float.floatToIntBits(dy);
			result = prime * result + page;
			result = prime * result + Float.floatToIntBits(scale);
			return result;
		}

		// Eclipse implementation, needed for testing
		@Override
		public boolean equals(Object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (obj == null)
			{
				return false;
			}
			if (getClass() != obj.getClass())
			{
				return false;
			}
			SavedState other = (SavedState) obj;
			if (Float.floatToIntBits(dx) != Float.floatToIntBits(other.dx))
			{
				return false;
			}
			if (Float.floatToIntBits(dy) != Float.floatToIntBits(other.dy))
			{
				return false;
			}
			if (page != other.page)
			{
				return false;
			}
			if (Float.floatToIntBits(scale) != Float.floatToIntBits(other.scale))
			{
				return false;
			}
			return true;
		}

		void copyFrom(FocalCenterState state)
		{
			page = state.page;
			scale = state.scale;
			dx = state.dx;
			dy = state.dy;
		}

		FocalCenterState toFocalCenterState()
		{
			return new FocalCenterState(page, scale, dx, dy);
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			super.writeToParcel(dest, flags);
			dest.writeInt(page);
			dest.writeFloat(scale);
			dest.writeFloat(dx);
			dest.writeFloat(dy);
		}

		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>()
		{
			@Override
			public SavedState createFromParcel(Parcel in)
			{
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size)
			{
				return new SavedState[size];
			}
		};
	}

	/* package */void notifyPageLoaded(int position)
	{
		if (position < 0)
		{
			throw new IllegalArgumentException("Position cannot be negative (" + position
					+ ") in DocumentView.notifyPageLoaded(int)");
		}

		if (mFirstVisiblePage <= position && position <= mFirstVisiblePage + mVisiblePagesCount)
		{
			refresh();
		}
	}

	/* package */void notifyPageCountChanged(int total)
	{
		// It might be that DocumentView didn't have enough pages to fill up all its space. Thanks
		// to the new value, it could be able to draw more pages now. Therefore update first
		// mVisiblePagesCount, using the (already updated) value in the adapter, and then invalidate
		// to trigger a draw
		updateVisiblePagesCount();
		invalidate();
	}

	/* package */void notifyPageScaleChanged(int position)
	{
		computeScalesAndPadding(position, true);
		invalidate();
	}

	public void refresh()
	{
		setPositionAndCorrectEdges(mFirstVisiblePageLeft, mFirstVisiblePageTop, true);
		invalidate();
	}

	/** Interface for an Adapter that wants to notify the DocumentView when the dataset changes */
	public interface ObservableAdapter
	{
		/** Notifies the DocumentView that a new image has been loaded */
		void notifyPageLoaded(int position);

		/** Notifies the DocumentView that the page count has changed */
		void notifyPageCountChanged(int total);

		/** Notifies the documentView that scales should be recalculated based on position */
		void notifyPageScaleChanged(int position);
	}

	/** Listens for changes in the current page. See {@link #getCurrentPage()} */
	public interface OnPageChangedListener
	{
		/**
		 * Called when the first visible page changed. The indexes are zero-based, i.e. wrt to the Adapter
		 */
		void onPageChanged(int oldPage, int newPage);
	}
}
