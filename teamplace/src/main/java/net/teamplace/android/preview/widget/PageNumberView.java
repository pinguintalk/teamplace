package net.teamplace.android.preview.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cortado.android.R;

/**
 * Custom view that shows the current page number and the total page number
 * 
 * @author Gil Vegliach
 */
final public class PageNumberView extends ViewGroup
{
	private int mCurrentPage = -1;
	private int mTotalPages = -1; // unknown
	private TextView mTvCurrentPage;
	private TextView mTvTotalPages;
	private LinearLayout mContainer;

	public PageNumberView(Context context, AttributeSet attrs)
	{
		this(context, attrs, R.attr.pageNumberViewStyle);
	}

	public PageNumberView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		inflate(context);
		bind();

		// Moves the background drawable to the linear layout container, if any
		Drawable bg = getBackground();
		if (bg instanceof ColorDrawable)
		{
			ColorDrawable d = (ColorDrawable) bg;

			// Hack to get the color of a ColorDrawable, since it was added in API 11. First we save
			// the old bounds and set the new ones to have a 1-pixel image
			Rect bounds = d.copyBounds();
			d.setBounds(0, 0, 1, 1);

			// Create a canvas backed up by a 1-pixel bitmap
			Bitmap bitmap = Bitmap.createBitmap(1, 1, Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);

			// Draw drawable and retrieve its color
			d.draw(canvas);
			int color = bitmap.getPixel(0, 0);

			// Release bitmap and restore drawable bounds
			bitmap.recycle();
			d.setBounds(bounds);

			// Finally set the container's background to specified color
			setBackgroundColor(color);
		}

		TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.PageNumberView, defStyle, 0);
		try
		{
			setCurrentPage(a.getInteger(R.styleable.PageNumberView_currentPage, 1));
			setPageCount(a.getInteger(R.styleable.PageNumberView_pageCount, -1));
			setBackgroundColor(a.getColor(R.styleable.PageNumberView_backgroundColor, Color.BLACK));

			int color = a.getColor(R.styleable.PageNumberView_overlayColor, Color.WHITE);
			mTvCurrentPage.setBackgroundColor(color);
			mTvTotalPages.setTextColor(color);
		}
		finally
		{
			a.recycle();
		}
	}

	private void inflate(Context context)
	{
		LayoutInflater inflater = LayoutInflater.from(context);
		inflater.inflate(R.layout.prv_page_number_view, this);
	}

	private void bind()
	{
		View v = findViewById(R.id.current_page_number);
		mTvCurrentPage = (SeeThroughTextView) v;
		mTvTotalPages = (TextView) findViewById(R.id.total_pages_number);
		mContainer = (LinearLayout) findViewById(R.id.container);

	}

	public int getCurrentPage()
	{
		return mCurrentPage;
	}

	public int getPageNumber()
	{
		return mTotalPages;
	}

	public void setCurrentPage(int page)
	{
		mCurrentPage = page;
		mTvCurrentPage.setText(String.valueOf(mCurrentPage));
		invalidate();
	}

	public void setPageCount(int total)
	{
		mTotalPages = total;

		if (mTotalPages == -1)
			mTvTotalPages.setText("-");
		else if (mTotalPages >= 0)
			mTvTotalPages.setText(String.valueOf(total));
		else
			throw new RuntimeException("Total number of pages must be >= -1");

		invalidate();
	}

	public void setPageCountUnknown()
	{
		setPageCount(-1);
	}

	/**
	 * Sets background to the given color. This internally sets the container background to the given color and the view
	 * background to the transparent color
	 */
	@Override
	public void setBackgroundColor(int color)
	{
		mContainer.setBackgroundColor(color);
		super.setBackgroundColor(0);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		View child = getChildAt(0);
		child.measure(widthMeasureSpec, heightMeasureSpec);

		int mw = child.getMeasuredWidth();
		int mh = child.getMeasuredHeight();
		setMeasuredDimension(mw, mh);
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		View child = getChildAt(0);
		int r = child.getMeasuredWidth();
		int b = child.getMeasuredHeight();
		child.layout(0, 0, r, b);
	}

	final public static class SeeThroughTextView extends TextView
	{
		Bitmap mMaskBitmap;
		Canvas mMaskCanvas;
		Paint mPaint;

		int mBackgroundColor = Color.WHITE;
		Bitmap mBackgroundBitmap;
		Canvas mBackgroundCanvas;

		public SeeThroughTextView(Context context)
		{
			super(context);
			init();
		}

		public SeeThroughTextView(Context context, AttributeSet attrs, int defStyle)
		{
			super(context, attrs, defStyle);
			init();
		}

		public SeeThroughTextView(Context context, AttributeSet attrs)
		{
			super(context, attrs);
			init();
		}

		private void init()
		{
			mPaint = new Paint();
			mPaint.setXfermode(new PorterDuffXfermode(Mode.DST_OUT));
			super.setTextColor(Color.BLACK);
			super.setBackgroundColor(Color.TRANSPARENT);
		}

		@Override
		public void setBackgroundColor(int color)
		{
			mBackgroundColor = color;
			invalidate();
		}

		@Override
		protected void onSizeChanged(int w, int h, int oldw, int oldh)
		{
			super.onSizeChanged(w, h, oldw, oldh);
			mBackgroundBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			mBackgroundCanvas = new Canvas(mBackgroundBitmap);
			mMaskBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
			mMaskCanvas = new Canvas(mMaskBitmap);
		}

		@Override
		protected void onDraw(Canvas canvas)
		{
			// To the maintainer: setting android:singleLine="true"
			// will scroll this view to a high number. Uncomment the
			// following line to debug:
			//
			// int scrollX = getScrollX();

			// Clear canvases
			mBackgroundCanvas.drawColor(mBackgroundColor);
			mMaskCanvas.drawColor(Color.BLACK, PorterDuff.Mode.CLEAR);

			// Draw mask and compose it with the background
			super.onDraw(mMaskCanvas);
			mBackgroundCanvas.drawBitmap(mMaskBitmap, 0.f, 0.f, mPaint);

			// Draw onto final canvas
			canvas.drawBitmap(mBackgroundBitmap, 0.f, 0.f, null);
		}
	}
}
