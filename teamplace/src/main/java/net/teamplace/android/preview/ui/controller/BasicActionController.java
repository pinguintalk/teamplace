package net.teamplace.android.preview.ui.controller;

import android.animation.Animator;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cortado.android.R;

import net.teamplace.android.action.Action;
import net.teamplace.android.action.ActionFactory;
import net.teamplace.android.action.BaseActionFactory;
import net.teamplace.android.action.MailAction;
import net.teamplace.android.action.PreviewDeleteAction;
import net.teamplace.android.action.PreviewExportToPdfAction;
import net.teamplace.android.action.PreviewExtractVersionAction;
import net.teamplace.android.action.PreviewZipAction;
import net.teamplace.android.action.PrintAction;
import net.teamplace.android.action.RenameAction;
import net.teamplace.android.action.ShareAction;
import net.teamplace.android.action.SmarfileAction;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.EditTextDialogFragment;
import net.teamplace.android.browsing.dialog.SmartfileDialogFragment;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.RightsHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

public class BasicActionController implements ActionController
{
	public static final String TAG = tag(BasicActionController.class);
	private static final String PKG = pkg(BasicActionController.class);

	private static final String KEY_IS_ACTION_BAR_VISIBLE = PKG + ".isActionBarVisible";
	private static final String KEY_CURR_REQUESTS = PKG + ".currRequests";
	private static final String KEY_IS_FAVORITE_REQUEST_IN_PROGRESS = PKG + ".isFavoriteRequestInProgress";

	private PreviewHolderFragment mFragment;

	private ActionBarProxy mActionBarProxy;

	/** Ids of requests in progress */
	private final ArrayList<Integer> mCurrRequests = new ArrayList<Integer>();

	/** Only one favorite request (add/remove) at a time. During this time the option menu button is disabled */
	private boolean mIsFavoriteRequestInProgress = false;

	/** Creates file actions. Don't hold on the actions too long, as you could leak */
	private final ActionFactory mActionFactory = new PreviewTp3ActionFactory();

	public BasicActionController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	private final BroadcastReceiver mResponseReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	@Override
	public void setFragmentView(View v)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {

	}

	@Override
	public void onHideOverlaysRequested()
	{
		mActionBarProxy.hide();
	}

	@Override
	public void onShowOverlaysRequested()
	{
		mActionBarProxy.show();
	}

	@Override
	public int getOverlayState()
	{
		return mActionBarProxy.isShowing() ? OVERLAY_VISIBLE : OVERLAY_HIDDEN;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == SmartfileDialogFragment.REQUEST_CODE)
		{
			Path target = data.getParcelableExtra(SmartfileDialogFragment.KEY_TARGET);
			Path[] sources = new Path[] {mFragment.getFileInfoController().getMasterDocument().getPath()};

			Intent newData;
			PreviewActivity act = (PreviewActivity) mFragment.getActivity();
			int action = data.getIntExtra(SmartfileDialogFragment.KEY_ACTION, SmartfileDialogFragment.NO_ACTION);
			if (action == SmartfileDialogFragment.ACTION_COPY)
			{
				newData = BrowsingService.Contract.buildCopyRequestIntent(act, sources, target);
			}
			else if (action == SmartfileDialogFragment.ACTION_MOVE)
			{
				newData = BrowsingService.Contract.buildMoveRequestIntent(act, sources, target);
			}
			else
			{
				throw new IllegalArgumentException("Action non recognized");
			}

			GenericUtils.finishWithData(act, newData);
		}
		else if (requestCode == R.id.request_code_rename)
		{
			CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
			String newName = cs.toString();

			// Then execute background request
			Activity act = mFragment.getActivity();
			Intent i = BrowsingService.Contract.buildRenameRequestIntent(act, mFragment.getFileInfoController()
					.getDisplayingPath(), newName);
			mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
			act.startService(i);

			// TODO: we can avoid showing a toast but we need to remember the old file name as a fall back in
			// case of error of the network request
			Toast.makeText(act, R.string.prv_rename_in_progress, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		mActionBarProxy = new ActionBarProxy(activity);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		mFragment.setHasOptionsMenu(true);
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		int menuRes = isMasterDocumentShowing()
			? R.menu.prv_context_menu
			: R.menu.prv_versions_menu;
		inflater.inflate(menuRes, menu);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		restoreState(savedInstanceState);
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		mActionFactory.build(id).execute();
		return true;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		Activity act = mFragment.getActivity();
		LocalBroadcastManager.getInstance(act).unregisterReceiver(mResponseReceiver);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// No dynamic processing to do if the versions menu is showing
		if (!isMasterDocumentShowing())
		{
			return;
		}

		List<FileInfo> fileInfo = new ArrayList<FileInfo>();
		fileInfo.add(mFragment.getFileInfoController().getMasterDocument());

		// TODO: @jobol this method potentially perform a database operation on the UI thread and should be avoided
		Session session = new SessionManager(mFragment.getActivity().getApplicationContext()).getLastSession();
		RightsHelper rightsHelper = new RightsHelper(session);
		MenuItem item = menu.findItem(R.id.action_mail);
		item.setVisible(rightsHelper.canMailAndShare(fileInfo));
		menu.findItem(R.id.action_zip).setVisible(rightsHelper.canExportToZip(fileInfo));
		menu.findItem(R.id.action_export_to_pdf).setVisible(rightsHelper.canExportToPdf(fileInfo));
		menu.findItem(R.id.action_rename).setVisible(rightsHelper.canRename(fileInfo));
		menu.findItem(R.id.action_delete).setVisible(rightsHelper.canDelete(fileInfo));

		boolean isFav = mFragment.getFileInfoController().getMasterDocument().isFavorite();
		boolean isReqInProgress = mIsFavoriteRequestInProgress;
		MenuItem favorite = menu.findItem(R.id.action_favorite);
		MenuItem unfavorite = menu.findItem(R.id.action_unfavorite);

		String pathString = mFragment.getFileInfoController()
				.getMasterDocument().getPath().toString();

		if (favorite != null)
		{
			favorite.setVisible(!isFav);
			favorite.setEnabled(!isFav && !isReqInProgress);
		}
		if (unfavorite != null)
		{
			unfavorite.setVisible(isFav);
			unfavorite.setEnabled(isFav && !isReqInProgress);
		}
		if (pathString.contains(Path.LOCAL_LOCATION + Path.LOCATION_PATH_SEPARATOR))
		{
			favorite.setVisible(false);
			unfavorite.setVisible(false);
		}
	}

	@Override
	public void onResume()
	{
		Activity act = mFragment.getActivity();
		IntentFilter filter = new IntentFilter(BrowsingService.Contract.ACTION_RESPONSE_RENAME);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_ADD_FAVORITE);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_DELETE_FAVORITE);
		LocalBroadcastManager.getInstance(act).registerReceiver(mResponseReceiver, filter);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putBoolean(KEY_IS_ACTION_BAR_VISIBLE, mActionBarProxy.isShowing());
		outState.putIntegerArrayList(KEY_CURR_REQUESTS, mCurrRequests);
		outState.putBoolean(KEY_IS_FAVORITE_REQUEST_IN_PROGRESS, mIsFavoriteRequestInProgress);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	private void addFavoriteRequest()
	{
		Activity act = mFragment.getActivity();
		Intent i = BrowsingService.Contract.buildAddFavoriteRequestIntent(act, mFragment.getFileInfoController()
				.getMasterDocument().getPath());
		mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
		act.startService(i);
	}

	private void delFavoriteRequest()
	{
		Activity act = mFragment.getActivity();
		Intent i = BrowsingService.Contract.buildDeleteFavoriteRequestIntent(act, mFragment.getFileInfoController()
				.getMasterDocument().getPath());
		mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
		act.startService(i);
	}

	private void onResponseReceived(Intent intent)
	{
		Log.d(TAG, "onResponseReceived() [1], intent=" + intent);

		// ACTION_NOTIFICATION_PAGE_LOADED is spawned by the adapter and does not store ids
		String action = intent.getAction();
		// if (Contract.ACTION_NOTIFICATION_PAGE_LOADED.equals(action))
		// {
		// onResponsePageLoaded(intent);
		// return;
		// }

		int id = BrowsingService.Contract.extractRequestId(intent);
		Log.d(TAG, "onResponseReceived() [2], action=" + action + ", id=" + id + ", mCurrRequests=" + mCurrRequests);

		// NO_ID is the same both for BrowsingService and PreviewService
		if (id == BrowsingService.Contract.NO_ID || !mCurrRequests.remove((Integer) id))
		{
			return;
		}

		if (BrowsingService.Contract.ACTION_RESPONSE_RENAME.equals(action))
		{
			onResponseRename(intent);
		}
		// else if (PreviewService.Contract.ACTION_RESPONSE_VERSIONS.equals(action))
		// {
		// onResponseVersions(intent);
		// }
		// else if (BrowsingService.Contract.ACTION_RESPONSE_DELETE.equals(action))
		// {
		// // We have completed deleting a version
		// onResponseDeleteVersion();
		// }
		else if (BrowsingService.Contract.ACTION_RESPONSE_ADD_FAVORITE.equals(action))
		{
			onResponseFavoriteStatusModified(true);
		}
		else if (BrowsingService.Contract.ACTION_RESPONSE_DELETE_FAVORITE.equals(action))
		{
			onResponseFavoriteStatusModified(false);
		}
	}

	private void onResponseRename(Intent intent)
	{
		String newName = BrowsingService.Contract.extractNewName(intent);
		if (newName == null)
		{
			throw new IllegalStateException(
					"Please build requests with methods in the Contract class and do not modify them. Intent: "
							+ intent);
		}

		// Sync information on screen
		FileInfo oldMasterDoc = mFragment.getFileInfoController().getMasterDocument();
		Path newPath = PathUtils.buildRenamedPath(oldMasterDoc.getPath(), newName);
		FileInfo newMasterDoc = new FileInfo(newPath, oldMasterDoc.getLastModifiedDate(),
				oldMasterDoc.getSize(),
				oldMasterDoc.isFavorite());
		mFragment.getFileInfoController().setMasterDocument(newMasterDoc);

		mFragment.getFileInfoController().setDisplayingPath(newPath);
		mFragment.getFileInfoController().setDisplayingDate(newMasterDoc.getLastModifiedDate());
		// syncFooterView();
		// clearVersions();

		// Starts a new versions request
		// versionsRequest();
		mFragment.getVersionsController().onUiUpdateRequested();
		//update file
		mFragment.getCommentsController().onUpdateDocument();
		Toast.makeText(mFragment.getActivity(), R.string.prv_rename_done, Toast.LENGTH_SHORT).show();
	}

	private void onResponseFavoriteStatusModified(boolean isFavorite)
	{
		mIsFavoriteRequestInProgress = false;
		mFragment.getFileInfoController().getMasterDocument().setFavorite(isFavorite);
		mFragment.getActivity().invalidateOptionsMenu();
	}

	/**
	 * Shows whether the master document or one of its other versions is on screen. The options menu depends on this
	 * value
	 */
	private boolean isMasterDocumentShowing()
	{
		Path masterPath = mFragment.getFileInfoController().getMasterDocument().getPath();
		Path showingPath = mFragment.getFileInfoController().getDisplayingPath();

		if (masterPath.equals(showingPath)) return true;

		// this section will check version number to preview from activity stream
		if (showingPath.getName().contains(":VER_")) {
			String verNr = showingPath.getName().substring(showingPath.getName().lastIndexOf("_") + 1);
			int  verNrOfMaster = mFragment.getVersionsController().getmVersionNumberOfMasterDoc();
			if (verNrOfMaster == Integer.valueOf(verNr))
				return true;
		}

		return false;
	}

	private void restoreState(Bundle savedInstanceState)
	{
		if (savedInstanceState != null)
		{
			// int count = savedInstanceState.getInt(KEY_PAGE_COUNT);
			// mAdapter.setCount(count);
			//
			// boolean isPageCountKnown = savedInstanceState.getBoolean(KEY_IS_PAGE_KNOWN);
			// mAdapter.setPageCountKnown(isPageCountKnown);

			ArrayList<Integer> requestIds = savedInstanceState.getIntegerArrayList(KEY_CURR_REQUESTS);
			if (requestIds != null)
			{
				mCurrRequests.addAll(requestIds);
			}

			mIsFavoriteRequestInProgress = savedInstanceState.getBoolean(KEY_IS_FAVORITE_REQUEST_IN_PROGRESS);
		}

		restoreDecorationsState(savedInstanceState);
	}

	private void restoreDecorationsState(Bundle savedInstanceState)
	{
		// First, initialize member variables
		if (savedInstanceState != null)
		{
			if (savedInstanceState.getBoolean(KEY_IS_ACTION_BAR_VISIBLE))
			{
				mActionBarProxy.show();
			}
			else
			{
				mActionBarProxy.hide();
			}
			// mIsPageNumberVisible = savedInstanceState.getBoolean(KEY_IS_PAGE_NUMBER_VISIBLE);
		}

		// REFACTORCOMMENT: Only using ActionBarProxy
		// Synchronize ActionBar visibility with its flag
		// if (isActionBarVisible() && !mActionBarProxy.isShowing())
		// {
		// mActionBarProxy.show();
		// }
		// else if (!isActionBarVisible() && mActionBarProxy.isShowing())
		// {
		// mActionBarProxy.hide();
		// }

		// // Synchronize PageNumber visibility with its flag
		// if (mIsPageNumberVisible)
		// {
		// if (mPageNumberViewContainer.getVisibility() != View.VISIBLE)
		// {
		// mPageNumberViewContainer.setVisibility(View.VISIBLE);
		// }
		//
		// // If the PageNumber is visible but not the action bar, then trigger a delayed fade-out
		// // animation
		// if (!mIsActionBarVisible)
		// {
		// mHandler.removeCallbacks(mPageNumberHider);
		// mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
		// }
		// }
		// else if (mPageNumberViewContainer.getVisibility() == View.VISIBLE)
		// {
		// mPageNumberViewContainer.setVisibility(View.GONE);
		// }
	}

	/** Manages the flag mIsActionBarVisible while delegating its calls to a real implementation */
	/* non-static */private class ActionBarProxy
	{
		private final Activity mActivity;

		public ActionBarProxy(Activity activity)
		{
			checkNonNullArg(activity);
			mActivity = activity;
		}

		public void hide()
		{
			// setActionBarVisible(false);
			mActivity.getActionBar().hide();
		}

		public void show()
		{
			// setActionBarVisible(true);
			mActivity.getActionBar().show();
		}

		public boolean isShowing()
		{
			return mActivity.getActionBar().isShowing();
		}

	}

	private class PreviewTp3ActionFactory extends BaseActionFactory
	{
		@Override
		public Action buildShareAction()
		{
			return new ShareAction(mFragment.getFragmentManager(), mFragment.getFileInfoController()
					.getMasterDocument());
		}

		@Override
		public Action buildSmartfileAction()
		{
			ArrayList<Path> path = new ArrayList<Path>(1);
			path.add(mFragment.getFileInfoController().getMasterDocument().getPath());
			return new SmarfileAction(mFragment, path);
		}

		@Override
		public Action buildMailAction()
		{
			return new MailAction(mFragment.getActivity(), mFragment.getFileInfoController().getMasterDocument());
		}

		@Override
		public Action buildZipAction()
		{
			return new PreviewZipAction(mFragment.getActivity(), mFragment.getFileInfoController().getMasterDocument()
					.getPath());
		}

		@Override
		public Action buildExportToPdfAction()
		{
			return new PreviewExportToPdfAction(mFragment.getActivity(), mFragment.getFileInfoController()
					.getMasterDocument().getPath());
		}

		@Override
		public Action buildFavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					addFavoriteRequest();
					mIsFavoriteRequestInProgress = true;
					mFragment.getActivity().invalidateOptionsMenu();
				}
			};
		}

		@Override
		public Action buildUnfavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					delFavoriteRequest();
					mIsFavoriteRequestInProgress = true;
					mFragment.getActivity().invalidateOptionsMenu();
				}
			};
		}

		@Override
		public Action buildRenameAction()
		{
			HashMap<String, String> illegalInputs = new HashMap<>();
			String alreadyExists = mFragment.getActivity().getString(R.string.brw_input_already_exists);
			String toName = mFragment.getFileInfoController().getMasterDocument().getName();
			for (FileInfo fileInfo : mFragment.getFileInfoController().getFolderContent())
			{
				if (!fileInfo.getName().equals(toName))
				{
					illegalInputs.put(fileInfo.getName().toLowerCase(Locale.getDefault()), alreadyExists);
				}
			}

			return new RenameAction(mFragment, toName,
					false, illegalInputs);
		}

		@Override
		public Action buildDeleteAction()
		{
			return new PreviewDeleteAction(mFragment.getActivity(), mFragment.getFileInfoController()
					.getMasterDocument().getPath());
		}

		@Override
		public Action buildExtractVersionAction()
		{
			return new PreviewExtractVersionAction(mFragment.getActivity(), mFragment.getFileInfoController().
					getDisplayingPath(),
					mFragment.getFileInfoController().getDisplayingDate());
		}

		@Override
		public Action buildDeleteVersionAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					Activity act = mFragment.getActivity();

					SessionManager sm = new SessionManager(act);

					RightsHelper rh = new RightsHelper(sm.getLastSession());

					if (!rh.canDelete(mFragment.getFileInfoController().getMasterDocument())) {
						FeedbackToast fbt = new FeedbackToast(act,false,
								act.getResources().getString(R.string.ntf_delete_failed_missing_rights));
						fbt.setTitle(act.getResources().getString(R.string.ntf_delete_title_missing_rights));
						fbt.show();
						return;
					}

					Intent i = BrowsingService.Contract.buildDeleteRequestIntent(act,
							new Path[] {mFragment.getFileInfoController().getDisplayingPath()});
					mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
					act.startService(i);

					mFragment.getFileInfoController().setDisplayingPath(
							mFragment.getFileInfoController().getMasterDocument().getPath());
					mFragment.getFileInfoController().setDisplayingDate(
							mFragment.getFileInfoController().getMasterDocument().getLastModifiedDate());
					// clearVersions();
					mFragment.getVersionsController().onUiUpdateRequested();

					Toast.makeText(act, R.string.prv_delete_version_in_progress, Toast.LENGTH_SHORT).show();
				}
			};
		}

		@Override
		public Action buildPrintAction()
		{
			Path path = mFragment.getFileInfoController().getMasterDocument().getPath();
			return new PrintAction(mFragment.getActivity(), path);
		}
	}

}
