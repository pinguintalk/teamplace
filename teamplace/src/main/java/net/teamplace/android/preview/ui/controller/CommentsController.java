package net.teamplace.android.preview.ui.controller;

public interface CommentsController extends PreviewController
{
	public void onUpdateDocument();
}
