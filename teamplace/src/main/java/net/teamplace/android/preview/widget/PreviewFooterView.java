package net.teamplace.android.preview.widget;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.ViewTreeObserver.OnPreDrawListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.cortado.android.R;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;

import java.util.ArrayList;
import java.util.List;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Footer view for the preview component. Show versions of a file.
 * 
 * @author Gil Vegliach
 */
// TODO: the code quality of this view can be improved cutting code paths in the show*() methods
// wherever pre-conditions and assumptions don't let follow those paths. This will require finding
// good points to insert the smallest number of checks and then remove dead branches. Also, consider
// the state design pattern if you run into many switches.
final public class PreviewFooterView extends LinearLayout
{
	public final static String TAG = tag(PreviewFooterView.class);

	public static final int STATE_ALL_VERSIONS = 3;
	public static final int STATE_SELECTED_VERSION = 2;
	public static final int STATE_COLLAPSED = 1;
	public static final int STATE_HIDDEN = 0;

	private static final int LIST_SCROLL_DURATION = 200;

	private int mState = STATE_COLLAPSED;
	private final ListView mVersionsList;
	private final VersionCountView mVersionsIcon;
	private final View mShadow;
	private final RelativeLayout mInfoPanel;
	private OnVersionClickListener mVersionClickListener;

	// will used only for preview file in teamdrive
	private String mTeamDriveId;

	// Non-null iff an animator is started or running. Some animations starts actually before mAnimator becomes
	// non-null, for instance on the first invalidation of some views; in that case the animator becomes non-null just
	// before drawing the first frame
	private volatile Animator mAnimator;

	// True iff an animation is scheduled or running. Touch logic is disabled during animations
	private volatile boolean mIsAnimating;

	// Used only in animateShowSelectedFromAllVersionsSelectedVisible() / animateShowAllVersionsFromSelected()
	// for saving / restoring the position of the versions list
	private int mFirstVisibleVersionPosition = -1;

	// Used only in animateShowSelectedFromAllVersionsSelectedVisible() / animateShowAllVersionsFromSelected()
	// for saving / restoring the position of the versions list
	private int mFirstVisibleVersionTop = -1;

	private final OnClickListener mInfoPanelClickListener = new OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if (mState != STATE_ALL_VERSIONS)
			{
				showAllVersions();
			}
			else
			{
				showSelectedOrCollapsed();
			}
		}
	};

	public PreviewFooterView(Context context)
	{
		this(context, null);
	}

	public PreviewFooterView(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public PreviewFooterView(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		setOrientation(LinearLayout.VERTICAL);

		// Do not let clicks go through
		setClickable(true);

		LayoutInflater inflater = LayoutInflater.from(context);
		View v = inflater.inflate(R.layout.prv_footer_view, this, true);
		mVersionsList = (ListView) v.findViewById(R.id.versions_list);
		mVersionsIcon = (VersionCountView) v.findViewById(R.id.versions_icon);
		mShadow = v.findViewById(R.id.shadow);
		mInfoPanel = (RelativeLayout) v.findViewById(R.id.info_panel);
		mInfoPanel.setOnClickListener(mInfoPanelClickListener);

		// Suppress clicks
		mInfoPanel.setEnabled(false);
	}

	public void setDocumentName(String name)
	{
		checkNonNullArg(name);
		TextView tv = (TextView) mInfoPanel.findViewById(R.id.file_name);
		tv.setText(name);
	}

	public void setDocumentLastModifiedDate(long date)
	{
		// Negative dates are supported
		TextView tv = (TextView) mInfoPanel.findViewById(R.id.file_last_modified_date);
		tv.setText(GenericUtils.formatDate(getContext(), date));
	}

	public void setDocumentSize(long size)
	{
		checkCondition(size >= 0);
		TextView tv = (TextView) mInfoPanel.findViewById(R.id.file_size);
		tv.setText(GenericUtils.formatFileSize(getContext(), size));
	}

	public void setDocumentIcon(int resid)
	{
		checkCondition(resid >= 0);
		ImageView iv = (ImageView) mInfoPanel.findViewById(R.id.file_icon);
		iv.setBackgroundResource(resid);
	}

	/**
	 * Sets version items.
	 * <p>
	 * The items here a small data structure containing only the UI state of versions of a file. This items are managed
	 * entirely by this view, including saving and restoring their state on configuration changes. Don't modify the
	 * items from external code. Synchronization of those items and the real, concrete versions of a file is up to
	 * external code.
	 * </p>
	 */
	public void setVersionItems(ArrayList<VersionItem> items, String teamDriveId)
	{
		setVersionItems(items, true, teamDriveId);
	}

	public void setOnVersionClickListener(OnVersionClickListener listener)
	{
		mVersionClickListener = listener;
	}

	/** Returns whether the PreviewFooterView is showing, even partially */
	public boolean isShowing()
	{
		return mState != STATE_HIDDEN;
	}

	public boolean isCollapsed()
	{
		return mState == STATE_COLLAPSED;
	}

	public boolean isSelectedVersion()
	{
		return mState == STATE_SELECTED_VERSION;
	}

	public boolean isSelectedVersionOrCollapsed()
	{
		return isSelectedVersion() || isCollapsed();
	}

	/**
	 * Shows the selected version or collapse this view.
	 * <p>
	 * What will be shown at the end depends on the current state of this view. The collapsed state will be shown if
	 * there are no version items or if the current version is selected. Otherwise the selected version will be shown.
	 */
	public void showSelectedOrCollapsed()
	{
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (adapter == null || adapter.isCurrentSelectedInDataset())
		{
			showCollapsed();
		}
		else
		{
			showSelected(adapter.findSelectedInDataset());
		}
	}

	/** Hides this view, if not already hidden */
	public void hide()
	{
		if (mState != STATE_HIDDEN)
		{
			animateShowHidden();
		}
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent ev)
	{
		// Intercept children touch events if animating, so we can disable them
		return mIsAnimating;
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event)
	{
		if (mIsAnimating)
		{
			// Keep on eating touch events while animating
			return true;
		}

		return super.onTouchEvent(event);
	}

	@Override
	protected Parcelable onSaveInstanceState()
	{
		// If there are animations in progress end them and triggers all listeners, so the end state will be saved
		endAnimations();

		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.state = mState;

		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (adapter != null)
		{
			ss.versionItems = adapter.getAllItems();
			ss.selectedVersion = adapter.findSelectedInDataset();
		}

		return ss;
	}

	@Override
	protected void onRestoreInstanceState(Parcelable savedState)
	{
		if (!(savedState instanceof SavedState))
		{
			super.onRestoreInstanceState(savedState);
			return;
		}

		SavedState ss = (SavedState) savedState;
		super.onRestoreInstanceState(ss.getSuperState());

		// Pass false to keep the selected position
		setVersionItems(ss.versionItems, false, mTeamDriveId);
		switch (ss.state)
		{
			case STATE_ALL_VERSIONS:
			{
				setStateAllVersions();
				break;
			}
			case STATE_SELECTED_VERSION:
			{
				setStateSelected(ss.selectedVersion);
				break;
			}
			case STATE_COLLAPSED:
			{
				setStateCollapsed();
				break;
			}
			case STATE_HIDDEN:
			{
				setHiddenState();
				break;
			}
		}
	}

	public void selectPosition(int position)
	{
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		adapter.selectInDataset(position);
	}

	public int getPosition(Path versionPath)
	{
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (adapter != null)
		{
			List<VersionItem> allItems = adapter.getAllItems();
			for (int i = 0; i < allItems.size(); i++)
			{
				if (allItems.get(i).getFullName().equals(versionPath.getName()))
				{
					return i;
				}
			}
		}
		return -1;
	}

	// Called by VersionsAdapter
	/* package */void onVersionClicked(int selectedPos)
	{
		// Eat click on an animation
		if (mIsAnimating)
		{
			return;
		}
		if (!isShowing() || isCollapsed())
		{
			return;
		}

		final VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		VersionItem item = (VersionItem) adapter.getItem(selectedPos);
		if (mVersionClickListener != null)
		{
			mVersionClickListener.onVersionClicked(item);
		}

		if (mState == STATE_SELECTED_VERSION)
		{
			// We should never be able on show the selected version if this is also the current one
			if (item.isCurrent())
			{
				showCollapsed();
			}
			else
			{
				showAllVersions();
			}
			return;
		}

		// Post-condition: mState == STATE_ALL_VERSIONS
		if (item.isCurrent())
		{
			adapter.selectInDataset(selectedPos);
			showCollapsed();
		}
		else
		{
			showSelected(selectedPos);
		}
	}

	private void showSelected(final int selectedPos)
	{
		if (!isShowing())
		{
			VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
			if (adapter == null || adapter.isCurrentSelectedInDataset())
			{
				showCollapsed();
			}
			else
			{
				animateShowSelectedFromHidden(selectedPos);
			}
		}
		else
		{
			animateShowSelectedFromAllVersions(selectedPos);
		}
	}

	private void showAllVersions()
	{
		final VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (!isShowing())
		{
			if (adapter == null || adapter.isCurrentSelectedInDataset())
			{
				showCollapsed();
			}
			else
			{
				animateShowAllVersionsFromSelected();
			}
		}
		else
		{
			if (isSelectedVersion())
			{
				animateShowAllVersionsFromSelected();
			}
			else if (isCollapsed() && adapter != null)
			{
				animateShowAllVersionsFromCollapsed();
			}
		}
	}

	private void showCollapsed()
	{
		if (!isShowing())
		{
			animateShowCollapsedFromHidden();
		}
		else if (mVersionsList.getAdapter() != null)
		{
			animateShowCollapsedFromShowing();
		}
	}

	private void setVersionItems(ArrayList<VersionItem> items, boolean markLastModifiedAsCurrent,String teamDriveId)
	{
		boolean hasVersions = items != null && items.size() > 1;
		if (hasVersions)
		{
			VersionsAdapter adapter = new VersionsAdapter(getContext(), items, markLastModifiedAsCurrent, teamDriveId);
			adapter.setFooterView(this);
			mVersionsList.setAdapter(adapter);
			mVersionsIcon.setVisibility(View.VISIBLE);
			mVersionsIcon.setCount(adapter.getCount());
			mInfoPanel.setEnabled(true);
			mTeamDriveId = teamDriveId;
		}
		else
		{
			mVersionsList.setAdapter(null);
			mVersionsIcon.setVisibility(View.GONE);
			mInfoPanel.setEnabled(false);
		}

		if (isShowing())
		{
			// Collapse the view when new items are set or items have been removed
			setStateCollapsed();
		}
	}

	private void setHiddenState()
	{
		mState = STATE_HIDDEN;
		setVisibility(View.GONE);
	}

	private void setStateCollapsed()
	{
		mState = STATE_COLLAPSED;
		setVisibility(View.VISIBLE);
		mVersionsList.setVisibility(View.GONE);
	}

	private void setStateSelected(int selectedPos)
	{
		mState = STATE_SELECTED_VERSION;
		setVisibility(View.VISIBLE);
		mVersionsList.setVisibility(View.VISIBLE);
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (adapter != null)
		{
			adapter.selectInDataset(selectedPos);
			adapter.selectedMode();
		}
	}

	private void setStateAllVersions()
	{
		mState = STATE_ALL_VERSIONS;
		setVisibility(View.VISIBLE);
		mVersionsList.setVisibility(View.VISIBLE);
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		if (adapter != null)
		{
			adapter.allVersionsMode();
		}
	}

	private boolean listIsPositionOnScreen(int position)
	{
		int count = mVersionsList.getChildCount();
		int index = position - mVersionsList.getFirstVisiblePosition();
		return 0 <= index && index < count;
	}

	/** Scrolls smoothly the list so that the selected position is visible. Returns the first visible position */
	private int listSmoothScrollToPosition(final int selectedPos, int scrollDuration)
	{
		// We would like to use ListView.smoothScrollToPosition(), but that method is
		// buggy, see for instance:
		//
		// **** https://code.google.com/p/android/issues/detail?id=36062
		//
		// Therefore we implementing our own scrolling based on ListView.smoothScrollBy().
		// This method has a weird interpolation, but otherwise it looks fine. Since also
		// OnScrollListener is a bit buggy, we just post a delayed message on ListView
		// when scrolling is over, to force displaying the right position is case
		// the scrolling hasn't done its job.
		//
		// We need to calculate the scroll offset, for that we assume:
		// 1. Three children on screen (see prv_footer_view.xml listview height)
		// 2. The heights of all children are the same

		ListView versionsList = mVersionsList;
		int firstVisiblePos = versionsList.getFirstVisiblePosition();
		int count = versionsList.getChildCount();
		int childIndex = selectedPos - firstVisiblePos;

		View firstChild = versionsList.getChildAt(0);
		int firstChildHeight = firstChild.getHeight();
		int offset = firstChild.getTop();
		final int scrollPos;
		if (childIndex >= count)
		{
			// Selected child is coming in from bottom
			scrollPos = selectedPos - 2;
			offset += (scrollPos - firstVisiblePos) * firstChildHeight;
		}
		else
		{
			// Selected child is coming in from top
			scrollPos = selectedPos;
			offset += (scrollPos - firstVisiblePos) * firstChildHeight;
		}
		versionsList.smoothScrollBy(offset, scrollDuration);

		return scrollPos;
	}

	private int getShadowHeight()
	{
		if (mShadow.getVisibility() != View.VISIBLE)
		{
			return 0;
		}

		return mShadow.getHeight();
	}

	private void endAnimations()
	{
		if (mIsAnimating)
		{
			mIsAnimating = false;
			if (mAnimator != null)
			{
				mAnimator.end();
				mAnimator = null;
			}
		}
	}

	// !!! To the maintainer:
	// This method is different from the other animation methods because the flag mIsAnimating is only set
	// and not cleared inside its body. This method must be followed by
	// animateShowSelectedFromAllVersionsSelectedVisible()
	// which only clears the flag. Breaking this invariant, will make the code misbehave in subtle ways
	private void animateShowSelectedFromAllVersions(final int selectedPos)
	{
		mIsAnimating = true;

		// If selected position is visible, just run the animation. Otherwise scroll to it first, and then run
		// the animation
		final ListView versionsList = mVersionsList;
		if (listIsPositionOnScreen(selectedPos))
		{
			// The selected child is visible, just run the animation
			animateShowSelectedFromAllVersionsSelectedVisible(selectedPos);
		}
		else
		{
			// The selected child is off screen, we need to scroll first and then animate.
			int scrollDuration = LIST_SCROLL_DURATION;
			final int scrollPos = listSmoothScrollToPosition(selectedPos, scrollDuration);
			versionsList.postDelayed(new Runnable()
			{
				@Override
				public void run()
				{
					// Forces right position to be displayed, guards against buggy
					// implementations
					versionsList.setSelection(scrollPos);
					animateShowSelectedFromAllVersionsSelectedVisible(selectedPos);
				};
			}, scrollDuration);
		}
	}

	private void animateShowAllVersionsFromSelected()
	{
		// Animation starts before invalidation and ends when everything is done
		mIsAnimating = true;

		final VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		final int selectedPos = adapter.findSelectedInDataset();
		adapter.allVersionsMode();

		mVersionsList.setVisibility(View.VISIBLE);
		mVersionsList.invalidate();

		getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener()
		{
			boolean mFirstFrame = true;

			@Override
			public boolean onPreDraw()
			{
				// The versions list is about to be redrawn. The selected item might be now off screen and we need
				// first to bring it among the displayed items. To do so,
				// 1. we scroll the list to the best position we can
				// 2. we skip the first frame to let the list update itself
				// 3. we apply the animation
				if (mFirstFrame)
				{
					int position = selectedPos;
					int top = 0;

					if (mFirstVisibleVersionPosition != -1)
					{
						// If the saved position can effectively be displayed then do so. Otherwise show the selected
						// version as the first one
						if (mFirstVisibleVersionPosition < adapter.getCount())
						{
							position = mFirstVisibleVersionPosition;
							top = mFirstVisibleVersionTop;
						}

						// Those two fields are referring to the last animateShowSelectedFromAllVersions() and are now
						// stale
						mFirstVisibleVersionPosition = -1;
						mFirstVisibleVersionTop = -1;
					}

					mVersionsList.setSelectionFromTop(position, top);
					// Just in case, the list will sync
					mVersionsList.requestLayout();
					mFirstFrame = false;
					return false;
				}

				getViewTreeObserver().removeOnPreDrawListener(this);

				int listh = mVersionsList.getHeight();
				int firstPos = mVersionsList.getFirstVisiblePosition();
				float selectedh = mVersionsList.getChildAt(selectedPos - firstPos).getHeight();

				float footerDelta = getHeight() - (selectedh + mInfoPanel.getHeight() + getShadowHeight());
				AnimatorSet set = new AnimatorSet();
				ObjectAnimator footerAnim = ObjectAnimator.ofFloat(PreviewFooterView.this, View.TRANSLATION_Y,
						footerDelta, 0.f);
				AnimatorSet.Builder builder = set.play(footerAnim);
				set.addListener(new FooterAnimatorListener()
				{
					@Override
					protected void onAnimationFinished(Animator animation)
					{
						setTranslationY(0);
						setStateAllVersions();
						mIsAnimating = false;
					}
				});

				int count = mVersionsList.getChildCount();
				for (int i = 0; i < count; i++)
				{
					final View child = mVersionsList.getChildAt(i);
					int childTop = child.getTop();
					int pos = firstPos + i;

					if (pos < selectedPos)
					{
						ObjectAnimator scale = ObjectAnimator.ofFloat(child, View.SCALE_Y, 0.f, 1.f);
						child.setPivotY(listh - child.getHeight() - childTop);
						ObjectAnimator reverseTranslation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y,
								-footerDelta, 0.f);
						builder.with(scale).with(reverseTranslation);
					}
					else if (pos == selectedPos)
					{
						float delta = listh - child.getHeight() - childTop - footerDelta;
						ObjectAnimator translation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y, delta, 0.f);
						builder.with(translation);
					}
					else
					{
						ObjectAnimator scale = ObjectAnimator.ofFloat(child, View.SCALE_Y, 0.f, 1.f);
						child.setPivotY(listh - childTop);
						ObjectAnimator reverseTranslation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y,
								-footerDelta, 0.f);
						builder.with(scale).with(reverseTranslation);
					}
					set.addListener(new FooterAnimatorListener()
					{
						@Override
						protected void onAnimationFinished(Animator animation)
						{
							child.setScaleY(1.f);
							child.setTranslationY(0.f);
						}
					});
				}
				set.start();
				mAnimator = set;
				return true;
			}
		});
	}

	private void animateShowAllVersionsFromCollapsed()
	{
		mIsAnimating = true;

		mVersionsList.setVisibility(View.VISIBLE);
		VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		adapter.allVersionsMode();

		getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener()
		{
			@Override
			public boolean onPreDraw()
			{
				getViewTreeObserver().removeOnPreDrawListener(this);

				float delta = mVersionsList.getHeight();
				mAnimator = ObjectAnimator.ofFloat(PreviewFooterView.this, View.TRANSLATION_Y, delta, 0.f);
				mAnimator.addListener(new FooterAnimatorListener()
				{
					@Override
					public void onAnimationFinished(Animator animation)
					{
						setTranslationY(0.f);
						setStateAllVersions();
						mIsAnimating = false;
					}
				});
				mAnimator.start();
				return true;
			}
		});
	}

	// !!! To the maintainer:
	// This method is different from the other animation methods because the flag mIsAnimating is only cleared
	// and not set inside its body. This method must be called only from animateShowSelectedFromAllVersions()
	// which only sets the flag. Breaking this invariant, will make the code misbehave in subtle ways
	private void animateShowSelectedFromAllVersionsSelectedVisible(final int selectedPos)
	{
		// Save first visible item position and its top
		mFirstVisibleVersionPosition = mVersionsList.getFirstVisiblePosition();
		mFirstVisibleVersionTop = mVersionsList.getChildAt(0).getTop();

		// Calculate offsets and start animation
		final VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		adapter.selectInDataset(selectedPos);

		mVersionsList.invalidate();
		mVersionsList.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener()
		{
			@Override
			public boolean onPreDraw()
			{
				mVersionsList.getViewTreeObserver().removeOnPreDrawListener(this);

				int firstPos = mVersionsList.getFirstVisiblePosition();
				int listh = mVersionsList.getHeight();
				float selectedh = mVersionsList.getChildAt(selectedPos - firstPos).getHeight();

				float footerDelta = getHeight() - (selectedh + mInfoPanel.getHeight() + getShadowHeight());
				AnimatorSet set = new AnimatorSet();
				ObjectAnimator footerAnim = ObjectAnimator.ofFloat(PreviewFooterView.this, View.TRANSLATION_Y,
						footerDelta);
				AnimatorSet.Builder builder = set.play(footerAnim);

				set.addListener(new FooterAnimatorListener()
				{
					@Override
					protected void onAnimationFinished(Animator animation)
					{
						setTranslationY(0);
						setStateSelected(selectedPos);
						mIsAnimating = false;
					}
				});

				int count = mVersionsList.getChildCount();
				for (int i = 0; i < count; i++)
				{
					final View child = mVersionsList.getChildAt(i);
					int childTop = child.getTop();
					int pos = firstPos + i;

					if (pos < selectedPos)
					{
						ObjectAnimator scale = ObjectAnimator.ofFloat(child, View.SCALE_Y, 0.f);
						child.setPivotY(listh - child.getHeight() - childTop);
						ObjectAnimator reverseTranslation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y,
								-footerDelta);
						builder.with(scale).with(reverseTranslation);
					}
					else if (pos == selectedPos)
					{
						float delta = (listh - selectedh) - childTop - footerDelta;
						ObjectAnimator translation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y, delta);
						builder.with(translation);
						Log.d(TAG, "footerDelta: " + footerDelta + ", delta: " + delta);
					}
					else
					{
						ObjectAnimator scale = ObjectAnimator.ofFloat(child, View.SCALE_Y, 0.f);
						child.setPivotY(listh - childTop);
						ObjectAnimator reverseTranslation = ObjectAnimator.ofFloat(child, View.TRANSLATION_Y,
								-footerDelta);
						builder.with(scale).with(reverseTranslation);
					}
					set.addListener(new FooterAnimatorListener()
					{
						@Override
						protected void onAnimationFinished(Animator animation)
						{
							child.setScaleY(1.f);
							child.setTranslationY(0.f);
						}
					});
				}
				set.start();
				mAnimator = set;

				return true;
			}
		});
	}

	private void animateShowSelectedFromHidden(final int selectedPos)
	{
		mIsAnimating = true;

		setVisibility(View.VISIBLE);
		mVersionsList.setVisibility(View.VISIBLE);

		final VersionsAdapter adapter = (VersionsAdapter) mVersionsList.getAdapter();
		adapter.selectInDataset(selectedPos);
		adapter.selectedMode();
		getViewTreeObserver().addOnPreDrawListener(new OnPreDrawListener()
		{
			@Override
			public boolean onPreDraw()
			{
				getViewTreeObserver().removeOnPreDrawListener(this);

				float delta = getHeight();
				mAnimator = ObjectAnimator.ofFloat(PreviewFooterView.this, View.TRANSLATION_Y, delta, 0.f);
				mAnimator.addListener(new FooterAnimatorListener()
				{
					@Override
					protected void onAnimationFinished(Animator animation)
					{
						setTranslationY(0.f);
						setStateSelected(selectedPos);
						mIsAnimating = false;
					}
				});
				mAnimator.start();
				return true;
			}
		});
	}

	// !!! To the maintainer:
	// This method is different from the other animation methods because the flag mIsAnimating is only set
	// and not cleared inside its body. This method must be followed by animateShowCollapsedFromShowingCurrentVisible()
	// which only clears the flag. Breaking this invariant, will make the code misbehave in subtle ways
	private void animateShowCollapsedFromShowing()
	{
		mIsAnimating = true;

		// If the current version is selected, we collapse the view.
		// But first, if current is not visible, we scroll to bring it on screen
		final ListView versionsList = mVersionsList;
		VersionsAdapter adapter = (VersionsAdapter) versionsList.getAdapter();
		if (adapter.isCurrentSelectedInDataset())
		{
			int currentPos = adapter.findSelectedInDataset();
			if (!listIsPositionOnScreen(currentPos))
			{
				// Scroll + animation
				int scrollDuration = LIST_SCROLL_DURATION;
				final int scrollPos = listSmoothScrollToPosition(currentPos, scrollDuration);
				versionsList.postDelayed(new Runnable()
				{
					@Override
					public void run()
					{
						versionsList.setSelection(scrollPos);
						animateShowCollapsedFromShowingCurrentVisible();
					};
				}, scrollDuration);

				// We're done, just return
				return;
			}
		}

		// Default case, just animate
		animateShowCollapsedFromShowingCurrentVisible();
	}

	// !!! To the maintainer:
	// This method is different from the other animation methods because the flag mIsAnimating is only cleared
	// and not set inside its body. This method must be called only from animateShowCollapsedFromShowing()
	// which only sets the flag. Breaking this invariant, will make the code misbehave in subtle ways
	private void animateShowCollapsedFromShowingCurrentVisible()
	{
		float deltaY = mVersionsList.getHeight();
		mAnimator = ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, deltaY);
		mAnimator.addListener(new FooterAnimatorListener()
		{
			@Override
			protected void onAnimationFinished(Animator animation)
			{
				setTranslationY(0.f);
				setStateCollapsed();
				mIsAnimating = false;
			}
		});
		mAnimator.start();
	}

	private void animateShowCollapsedFromHidden()
	{
		mIsAnimating = true;

		setVisibility(View.VISIBLE);
		mVersionsList.setVisibility(View.GONE);
		float deltaY = mInfoPanel.getHeight() + getShadowHeight();
		mAnimator = ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, deltaY, 0.f);
		mAnimator.addListener(new FooterAnimatorListener()
		{
			@Override
			protected void onAnimationFinished(Animator animation)
			{
				setTranslationY(0.f);
				setStateCollapsed();
				mIsAnimating = false;
			}
		});
		mAnimator.start();
	}

	private void animateShowHidden()
	{
		mIsAnimating = true;

		float deltaY = getHeight();
		mAnimator = ObjectAnimator.ofFloat(this, View.TRANSLATION_Y, deltaY);
		mAnimator.addListener(new FooterAnimatorListener()
		{
			@Override
			protected void onAnimationFinished(Animator animation)
			{
				setTranslationY(0.f);
				setHiddenState();
				mIsAnimating = false;
			}
		});
		mAnimator.start();
	}

	/**
	 * Wrapper for an AnimationListener that nulls out the mAnimator reference of PreviewFooterView, so that it can know
	 * the animation is over and permit garbage collection
	 */
	abstract class FooterAnimatorListener extends AnimatorListenerAdapter
	{
		abstract protected void onAnimationFinished(Animator animation);

		@Override
		public void onAnimationEnd(Animator animation)
		{
			mAnimator = null;
			onAnimationFinished(animation);
		}

		@Override
		public void onAnimationCancel(Animator animation)
		{
			// Just in case: this should never be called as we always end the animation. If for some reasons
			// ValueAnimator.cancel() will be called in the future, the contract will call onAnimationEnd() afterwards,
			// so our onAnimationFinished() callback will be triggered only once
			mAnimator = null;
		}
	}

	public interface OnVersionClickListener
	{
		void onVersionClicked(VersionItem fullName);
	}

	public static class SavedState extends BaseSavedState
	{
		int state;
		ArrayList<VersionItem> versionItems;
		int selectedVersion = -1;

		public SavedState(Parcelable superState)
		{
			super(superState);
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			super.writeToParcel(dest, flags);
			dest.writeInt(state);
			dest.writeTypedList(versionItems);
			dest.writeInt(selectedVersion);
		}

		private SavedState(Parcel in)
		{
			super(in);
			state = in.readInt();
			versionItems = in.createTypedArrayList(VersionItem.CREATOR);
			selectedVersion = in.readInt();
		}

		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>()
		{
			@Override
			public SavedState createFromParcel(Parcel in)
			{
				return new SavedState(in);
			}

			@Override
			public SavedState[] newArray(int size)
			{
				return new SavedState[size];
			}
		};
	}
}