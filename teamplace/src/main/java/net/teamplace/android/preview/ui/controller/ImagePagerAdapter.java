package net.teamplace.android.preview.ui.controller;

import it.sephiroth.android.library.imagezoom.ImageViewTouch;
import it.sephiroth.android.library.imagezoom.ImageViewTouch.OnImageViewTouchSingleTapListener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import net.teamplace.android.application.DefaultImageDownloader;
import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.preview.PreviewRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.utils.ContextUtils;
import net.teamplace.android.utils.GenericUtils;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cortado.android.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;

public class ImagePagerAdapter extends PagerAdapter
{

	private List<ItemViewInfo> mItemInfoList = Collections.synchronizedList(new ArrayList<ItemViewInfo>());
	private LayoutInflater mInflater;
	private PreviewRequester mRequester;
	private int mMaxEdge;
	private DisplayImageOptions mImageOptions;
	private GestureListener mGestureListener;

	// private OnViewTapListener mOnViewTapListener = new OnViewTapListener()
	// {
	//
	// @Override
	// public void onViewTap(View view, float x, float y)
	// {
	// if (mGestureListener != null)
	// {
	// mGestureListener.onSingleTap();
	// }
	// }
	// };

	private OnImageViewTouchSingleTapListener mOnViewTapListener = new OnImageViewTouchSingleTapListener()
	{
		@Override
		public void onSingleTapConfirmed()
		{
			if (mGestureListener != null)
			{
				mGestureListener.onSingleTap();
			}
		}
	};

	/**
	 * @param context
	 * @param fileInfos
	 *            folder content
	 * @param displayPaths
	 *            Paths different from FileInfo's path. Used for persistence only, don't modify outside the Adapter!
	 */
	public ImagePagerAdapter(Context context, List<ItemViewInfo> itemInfos)
	{
		synchronized (mItemInfoList)
		{
			mItemInfoList.addAll(itemInfos);
			extractImages();
			Collections.sort(mItemInfoList, COMPARATOR);
		}

		mInflater = LayoutInflater.from(context);
		mRequester = new PreviewRequester(context, new SessionManager(context).getLastSession(), new TeamplaceBackend());
		DisplayMetrics dm = ContextUtils.getDisplayMetrics(context);
		mMaxEdge = Math.max(dm.heightPixels, dm.widthPixels);
		mImageOptions = new DisplayImageOptions.Builder().extraForDownloader(new StreamResolver()).cacheOnDisk(true)
				.cacheInMemory(true)
				.build();
	}

	@Override
	public Object instantiateItem(ViewGroup container, int position)
	{
		View itemLayout = mInflater.inflate(R.layout.prv_image_item, container, false);
		itemLayout.setTag(position);
		updateItemView(itemLayout);
		container.addView(itemLayout);
		return itemLayout;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object)
	{
		container.removeView((View) object);
	}

	@Override
	public int getCount()
	{
		return mItemInfoList.size();
	}

	@Override
	public int getItemPosition(Object object)
	{
		return (int) ((View) object).getTag();
	}

	@Override
	public boolean isViewFromObject(View arg0, Object arg1)
	{
		return arg1.equals(arg0);
	}

	public ItemViewInfo getInfoAtPosition(int position)
	{
		return mItemInfoList.get(position);
	}

	public List<ItemViewInfo> getAllData()
	{
		return mItemInfoList;
	}

	public int indexOfFileInfo(FileInfo info)
	{
		synchronized (mItemInfoList)
		{
			for (int i = 0; i < mItemInfoList.size(); i++)
			{
				if (mItemInfoList.get(i).getBaseDocInfo().equals(info))
				{
					return i;
				}
			}
			return -1;
		}
	}

	public void updateItemVersion(View view, Path versionPath, long versionTimestamp)
	{
		int position = (int) view.getTag();
		ItemViewInfo viewInfo = getInfoAtPosition(position);
		viewInfo.mVersionPath = versionPath;
		viewInfo.mVersionTimestamp = versionTimestamp;
		updateItemView(view);
	}

	public void setGestureListener(GestureListener listener)
	{
		mGestureListener = listener;
	}

	private void updateItemView(View itemView)
	{
		final ImageViewTouch iv = (ImageViewTouch) itemView.findViewById(R.id.iv_image);
		iv.setSingleTapListener(mOnViewTapListener);

		final ProgressBar prg = (ProgressBar) itemView.findViewById(R.id.prg_progress);
		ItemViewInfo viewInfo = getInfoAtPosition((int) itemView.getTag());
		String uri = getUri(viewInfo.getVersionPath());
		final long versionTimestamp = viewInfo.getVersionTimestamp();

		if (uri != null)
		{
			ImageLoader.getInstance().displayImage(uri, iv, mImageOptions,
					new ImageLoadingListener(versionTimestamp, iv, prg, mImageOptions));
		}
	}

	private void extractImages()
	{

		Iterator<ItemViewInfo> iterator = mItemInfoList.iterator();
		FileInfo info;
		int type;
		while (iterator.hasNext())
		{
			info = iterator.next().mBaseDocInfo;
			type = FileInfo.getTypeFromExtension(GenericUtils.getExtension(info.getName()));
			if (type != FileInfo.IMAGE)
			{
				iterator.remove();
			}
		}
	}

	private String getUri(Path path)
	{
		String teamId = Path.isTeamDriveLocation(path.getLocation()) ? ((Path.TeamDrive)
				path).getTeamDriveId()
				: null;

		String uri = null;
		if (Path.isLocalLocation(path.getLocation()))
		{
			uri = "file://" + ((Path.Local) path).getAbsolutePathString();
		}
		else
		{
			try
			{
				uri = mRequester.getPreviewUri(teamId, path.getName(),
						PathUtils.remoteRequestString(path.getParentPath()), 0, mMaxEdge, mMaxEdge, 80, 0)
						.toString();
			}
			catch (TeamDriveNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		return uri;
	}

	public static class ItemViewInfo implements Parcelable
	{
		private FileInfo mBaseDocInfo;
		private Path mVersionPath;
		private long mVersionTimestamp;

		public ItemViewInfo(FileInfo baseDocInfo, Path versionPath, long versionTimestamp)
		{
			mBaseDocInfo = baseDocInfo;
			mVersionPath = versionPath;
			mVersionTimestamp = versionTimestamp;
		}

		private ItemViewInfo(Parcel in)
		{
			mBaseDocInfo = in.readParcelable(getClass().getClassLoader());
			mVersionPath = in.readParcelable(getClass().getClassLoader());
			mVersionTimestamp = in.readLong();
		}

		public static List<ItemViewInfo> fromFileInfoList(List<FileInfo> in)
		{
			List<ItemViewInfo> list = new ArrayList<>();
			for (FileInfo info : in)
			{
				list.add(new ItemViewInfo(info, info.getPath(), info.getLastModifiedDate()));
			}
			return list;
		}

		public FileInfo getBaseDocInfo()
		{
			return mBaseDocInfo;
		}

		public Path getVersionPath()
		{
			return mVersionPath;
		}

		public long getVersionTimestamp()
		{
			return mVersionTimestamp;
		}

		@Override
		public int describeContents()
		{
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags)
		{
			dest.writeParcelable(mBaseDocInfo, flags);
			dest.writeParcelable(mVersionPath, flags);
			dest.writeLong(mVersionTimestamp);
		}

		public static final Creator<ItemViewInfo> CREATOR = new Creator<ImagePagerAdapter.ItemViewInfo>()
		{

			@Override
			public ItemViewInfo createFromParcel(Parcel source)
			{
				return new ItemViewInfo(source);
			}

			@Override
			public ItemViewInfo[] newArray(int size)
			{
				return new ItemViewInfo[size];
			}
		};
	}

	private static class ImageLoadingListener extends SimpleImageLoadingListener
	{
		private boolean mDeleteCachedImg = false;

		private long mVersionTimestamp;
		private ImageView mIv;
		private ProgressBar mPrg;
		private DisplayImageOptions mImageOptions;

		private ImageLoadingListener(long versionTimestamp, ImageView iv, ProgressBar prg,
				DisplayImageOptions imageOptions)
		{
			mVersionTimestamp = versionTimestamp;
			mIv = iv;
			mPrg = prg;
			mImageOptions = imageOptions;
		}

		@Override
		public void onLoadingStarted(String imageUri, View view)
		{
			File cachedImg = DiskCacheUtils.findInCache(imageUri, ImageLoader.getInstance().getDiskCache());
			if (cachedImg != null)
			{
				long currentTime = System.currentTimeMillis();
				long cachedImgTimestamp = cachedImg.lastModified();
				// remote file is newer than cached file OR unplausible local time
				mDeleteCachedImg = mVersionTimestamp > cachedImgTimestamp || mVersionTimestamp > currentTime;
			}
			mPrg.setVisibility(View.VISIBLE);
			mIv.setVisibility(View.INVISIBLE);
			super.onLoadingStarted(imageUri, view);
		}

		@Override
		public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
		{

			if (mDeleteCachedImg)
			{
				MemoryCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getMemoryCache());
				DiskCacheUtils.removeFromCache(imageUri, ImageLoader.getInstance().getDiskCache());
				mDeleteCachedImg = false;
				ImageLoader.getInstance().displayImage(imageUri, (ImageView) view, mImageOptions, this);
				return;
			}
			mPrg.setVisibility(View.INVISIBLE);
			mIv.setVisibility(View.VISIBLE);
			super.onLoadingComplete(imageUri, view, loadedImage);
		}

		@Override
		public void onLoadingFailed(String imageUri, View view, FailReason failReason)
		{
			if (view != null) {
				Context ctx = view.getContext();
				FeedbackToast.show(ctx, false, ctx.getString(R.string.brw_error_file_not_found));
				mPrg.setVisibility(View.INVISIBLE);
			}
			super.onLoadingFailed(imageUri, view, failReason);
		}
	}

	private static class StreamResolver implements DefaultImageDownloader.NetworkStreamResolver
	{

		@Override
		public InputStream fromUri(Context context, String uri) throws IOException
		{
			Session session = new SessionManager(context).getLastSession();
			PreviewRequester requester = new PreviewRequester(context, session, new TeamplaceBackend());
			try
			{
				return requester.getImagePreviewInputStream(Uri.parse(uri));
			}
			catch (XCBStatusException | HttpResponseException | ConnectFailedException | LogonFailedException
					| NotificationException | ServerLicenseException | RedirectException e)
			{
				throw new IOException(e);
			}
		}

	}

	private static final Comparator<ItemViewInfo> COMPARATOR = new Comparator<ImagePagerAdapter.ItemViewInfo>()
	{

		@Override
		public int compare(ItemViewInfo lhs, ItemViewInfo rhs)
		{
			return getCompareStr(lhs).compareTo(getCompareStr(rhs));
		}

		private String getCompareStr(ItemViewInfo info)
		{
			return info.mBaseDocInfo.getName();
		}
	};

	public interface GestureListener
	{
		public void onSingleTap();
	}
}
