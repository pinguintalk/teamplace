package net.teamplace.android.preview.comment;

import java.util.ArrayList;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.CommentsRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import de.greenrobot.event.EventBus;

public class DeleteCommentIntentService extends IntentService
{
	private final String TAG = getClass().getSimpleName();

	public static final int RESPONSE_CODE = 3;

	public DeleteCommentIntentService()
	{
		super("DeleteCommentIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		String commentID = intent.getStringExtra(CommentIntentHelper.EXTRA_COMMENT_ID);
		Path.TeamDrive filePath = intent.getParcelableExtra(CommentIntentHelper.EXTRA_FILE_PATH);
		ArrayList<String> currentCommentIDs = intent
				.getStringArrayListExtra(CommentIntentHelper.EXTRA_CURRENT_COMMENT_IDS);
		long since = intent.getLongExtra(CommentIntentHelper.EXTRA_SINCE, -1);

		SessionManager manager = new SessionManager(this);
		Session session = manager.getLastSession();

		CommentsRequester requester = new CommentsRequester(this, session, new TeamplaceBackend());

		try
		{
			requester.deleteComment(filePath.getTeamDriveId(), commentID);
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		ChangedCommentResponse changeResponse = null;

		try
		{
			if (currentCommentIDs.size() > 0)
			{
				changeResponse = requester.getChangedComments(filePath.getTeamDriveId(),
						PathUtils.remoteRequestString(filePath.getParentPath()), filePath.getName(), since,
						currentCommentIDs);
			}
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		Bundle extras = new Bundle();
		extras.putString(CommentIntentHelper.EXTRA_COMMENT_ID, commentID);

		EventBus.getDefault().postSticky(
				new CommentOperationResponse(RESPONSE_CODE, changeResponse, null, false, 0, filePath, extras));
	}

}
