package net.teamplace.android.preview;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArgs;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.preview.ui.OldTpm3PreviewFragment;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewFragmentFactory;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.LoggingActivity;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewConfiguration;
import android.widget.Toast;

import com.cortado.android.R;

/**
 * Activity for the preview component. It handles the ActionBar and acts as a middle tier for the communication between
 * PreviewService and PreviewFragment
 * 
 * @author Gil Vegliach
 */
public class PreviewActivity extends LoggingActivity
{
	{
		TAG = tag(PreviewActivity.class);
	}

	/** Used to get back a request Intent when user clicks on a file operation */
	public static final int REQUEST_CODE = R.id.request_code_preview;

	private final static String PKG = pkg(PreviewActivity.class);
	private static final String KEY_FILE_INFO = PKG + ".fileInfo";
	private static final String KEY_CONFIGURATION = PKG + ".configuration";
	private static final int FRAGMENT_CONTAINER_ID = R.id.container;

	/**
	 * Path to the file we passed to the activity when we started the preview. It can be different
	 * from the path in PreviewFragment because of versioning. Use this if you want the base document
	 * path and not what the user is seeing on screen at the moment.
	 */
	private Path mOriginalPath;

	public static Intent buildIntent(Context context, FileInfo initialFileInfo, PreviewConfiguration configuration)
	{
		checkNonNullArgs(context, initialFileInfo);
		Intent i = new Intent(context, PreviewActivity.class);
		i.putExtra(KEY_FILE_INFO, initialFileInfo);
		i.putExtra(KEY_CONFIGURATION, configuration);
		return i;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.prv_activity);

		// Clicks on up button will be delivered to onOptionsItemSelected() with id android.R.id.home
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Bundle extras = getIntent().getExtras();
		FileInfo info = (FileInfo) extras.getParcelable(KEY_FILE_INFO);
		mOriginalPath = info.getPath();

		PreviewConfiguration configuration = extras.getParcelable(KEY_CONFIGURATION);

		FragmentManager fm = getFragmentManager();
		if (fm.findFragmentById(FRAGMENT_CONTAINER_ID) == null)
		{
			Fragment fragment = buildPreviewFragment(info, configuration);
			fm.beginTransaction().add(FRAGMENT_CONTAINER_ID, fragment, TAG).commit();
		}
		
		// hack to show overflow menu showing on the top right of actionbar 
		// need on devices with physical menu button
		try {
	        ViewConfiguration config = ViewConfiguration.get(this);
	        Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
	        if(menuKeyField != null) {
	            menuKeyField.setAccessible(true);
	            menuKeyField.setBoolean(config, false);
	        }
	    } catch (Exception ex) {
	        // Ignore
	    }
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		if (item.getItemId() == android.R.id.home)
		{
			// Up button has been clicked, just finish this activity and go back to the previous one with
			// a RESULT_OK
			GenericUtils.finishWithData(this, null);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed()
	{
		Fragment frag = getFragmentManager().findFragmentById(FRAGMENT_CONTAINER_ID);
		if (frag instanceof BackPressManager)
		{
			BackPressManager bpm = (BackPressManager) frag;
			if (bpm.onBackPressed())
			{
				return;
			}
		}

		// PreviewActivity always returns RESULT_OK on back presses
		setResult(Activity.RESULT_OK);
		super.onBackPressed();
	}

	private Fragment buildPreviewFragment(FileInfo info, PreviewConfiguration configuration)
	{
		return PreviewFragmentFactory.create(getApplicationContext(), info, configuration);
	}

	// TODO: @giveg: check this when moving comments to their own fragment
	public Path getOriginalFilePath()
	{
		return mOriginalPath;
	}

}