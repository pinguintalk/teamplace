package net.teamplace.android.preview.widget;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;

/**
 * Base abstract class that allows DocumentView to be notified when a page has been loaded and when the page count has
 * changed
 * 
 * @author Gil Vegliach
 */
public abstract class DocumentLazyBaseAdapter implements DocumentAdapter, DocumentView.ObservableAdapter
{
	private final DocumentView mDocumentView;

	public DocumentLazyBaseAdapter(DocumentView v)
	{
		checkNonNullArg(v);
		mDocumentView = v;
	}

	@Override
	public void notifyPageLoaded(int position)
	{
		if (mDocumentView != null)
			mDocumentView.notifyPageLoaded(position);
	}

	@Override
	public void notifyPageCountChanged(int total)
	{
		if (mDocumentView != null)
			mDocumentView.notifyPageCountChanged(total);
	}

	@Override
	public void notifyPageScaleChanged(int position)
	{
		if (mDocumentView != null)
			mDocumentView.notifyPageScaleChanged(position);
	}

	public DocumentView getDocumentView()
	{
		return mDocumentView;
	}
}
