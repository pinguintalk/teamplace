package net.teamplace.android.preview.widget;

import static net.teamplace.android.utils.GenericUtils.assertTrue;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.Iterator;

import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.preview.PreviewService;
import net.teamplace.android.preview.PreviewServiceConnectionListener;
import net.teamplace.android.preview.PreviewService.PreviewServiceBinder;
import net.teamplace.android.preview.cache.Key;
import net.teamplace.android.preview.ui.PlaceholderPage;
import net.teamplace.android.utils.Log;

import android.content.Context;
import android.os.Handler;


// TODO : @giveg clean this adapter. Remove DocumentLazyBaseAdapter, it is too tricky to use
/**
 * Adapter with lazy loading that shows blank pages during loading. When the total page number is unknown, this class
 * will allow only one blank page to be shown. The page requests depend on the document page and its marker date, which
 * is needed to identify its version (both in sense of teamplace-versions and normal-last-modified-date-version)
 * 
 * @author Gil Vegliach
 */
public class PreviewLazyAdapter extends DocumentLazyBaseAdapter implements
		PreviewServiceConnectionListener
{
	private static final String TAG = tag(PreviewLazyAdapter.class);
	private static final int LOADING_STATUS_UNITIALIZED = -1;
	private static final int LOADING_STATUS_HAS_NO_VISIBLE_LOADING_PAGES = 0;
	private static final int LOADING_STATUS_HAS_VISIBLE_LOADING_PAGES = 1;

	private final Context mContext;
	private final Path mDocumentPath;
	private PreviewServiceBinder mServiceBinder;
	private final PlaceholderPage mPlaceholderPage;

	/** The marking date of the document we are going to request. Used for versions and caching */
	private final long mMarkingDate;
	private boolean mIsPlaceholderGeometryFinal = false;
	private int mCount = 1;
	private boolean mIsCountKnown = false;
	private boolean mBound = false;
	private boolean mIsSyncVisibleLoadingPagesPending = false;
	private int mHasVisiblePagesLoadingStatus = LOADING_STATUS_UNITIALIZED;

	private OnPageCountKnownListener mOnPageCountKnownListener;
	private OnHasVisibleLoadingPagesStatusListener mOnHasVisibleLoadingPagesStatusListener;

	private final Handler mHandler = new Handler();
	private final ArrayList<Integer> mVisibleLoadingPages = new ArrayList<Integer>();
	private final KeyBuffer mKeyBuffer = new KeyBuffer();
	private final Runnable mSyncerVisibleLoadingPages = new Runnable()
	{
		@Override
		public void run()
		{
			syncVisibleLoadingPages();
			mIsSyncVisibleLoadingPagesPending = false;
		}
	};

	public PreviewLazyAdapter(Context ctx, DocumentView dv, Path documentPath, long markingDate)
	{
		super(dv);
		mContext = ctx;
		mPlaceholderPage = new PlaceholderPage(ctx);
		mDocumentPath = documentPath;
		mMarkingDate = markingDate;
	}

	@Override
	public int getCount()
	{
		return mCount;
	}

	@Override
	public DocumentPage getPage(int position)
	{
		if (!mBound)
			return mPlaceholderPage;

		mVisibleLoadingPages.add(position);
		postSyncVisibleLoadingPagesIfNeedBe();

		Key key = Key.makePageKey(mDocumentPath, mMarkingDate, position);
		if (!mServiceBinder.isResultLoaded(key))
		{
			mServiceBinder.request(key);
			return mPlaceholderPage;
		}

		// Service is connected and page is loaded: retrieve it and return it
		PreviewService.Result result = mServiceBinder.retrieveResult(key);
		DocumentPage page = result.getPage();
		int count = result.getPageCount();
		assertTrue(!mIsCountKnown || mCount == count,
				"Invariant broken: mIsCountKnown=" + mIsCountKnown + ", mCount=" + mCount + ", count=" + count
						+ ", position=" + position + ". Should be: 'mIsCountKnown => (mCount == count)'");

		if (!mIsCountKnown)
		{
			if (count != -1)
			{
				mCount = count;
				mIsCountKnown = true;
				if (mOnPageCountKnownListener != null)
					mOnPageCountKnownListener.onPageCountKnown(mCount);
			}
			else
			{
				mCount = Math.max(mCount, position + 2);
			}
		}

		return page;
	}

	@Override
	public void notifyPageCountChanged(int total)
	{
		// Adjust mCount and call super.notifyPageCountChanged() if necessary
		setCount(total);
		mIsCountKnown = true;
	}

	@Override
	public void notifyPageLoaded(int position)
	{
		// A page has just been loaded. If the placeholder geometry was the default one, now it can be updated
		// (DocumentView needs to update its internal scales too)
		if (!mIsPlaceholderGeometryFinal)
			recalculatePlaceholderGeometry(position);

		super.notifyPageLoaded(position);
	}

	public boolean isPageCountKnown()
	{
		return mIsCountKnown;
	}

	public void setPageCountKnown(boolean known)
	{
		boolean old = mIsCountKnown;
		mIsCountKnown = known;

		if (!old && mIsCountKnown)
			mOnPageCountKnownListener.onPageCountKnown(mCount);
	}

	public void setCount(int newCount)
	{
		int oldCount = mCount;
		mCount = newCount;

		if (oldCount != newCount && mOnPageCountKnownListener != null)
		{
			super.notifyPageCountChanged(newCount);
		}
	}

	public void setOnPageCountKnownListener(OnPageCountKnownListener listener)
	{
		mOnPageCountKnownListener = listener;
	}

	public void setOnHasVisibleLoadingPagesStatusListener(OnHasVisibleLoadingPagesStatusListener listener)
	{
		mOnHasVisibleLoadingPagesStatusListener = listener;
	}

	// Notifications from PreviewService
	@Override
	public void onPreviewServiceConnected(PreviewServiceBinder service)
	{
		Log.d(TAG, "onPreviewServiceConnected()");
		mBound = true;
		mServiceBinder = service;

		for (int pos = 0; pos < mCount; pos++)
		{
			Key key = Key.makePageKey(mDocumentPath, mMarkingDate, pos);
			if (mServiceBinder.isResultLoaded(key))
			{
				recalculatePlaceholderGeometry(pos);
				break;
			}
		}

		// Triggers onDraw()
		getDocumentView().invalidate();
	}

	@Override
	public void onPreviewServiceDisconnected()
	{
		Log.d(TAG, "onPreviewServiceDisconnected()");
		mBound = false;
		mHandler.removeCallbacks(mSyncerVisibleLoadingPages);
	}

	@Override
	public void onPreviewLoadFinished(int loadedPagePosition, int totalPageNumber)
	{
		if (totalPageNumber != -1)
			notifyPageCountChanged(totalPageNumber);

		notifyPageLoaded(loadedPagePosition);
		postSyncVisibleLoadingPagesIfNeedBe();
	}

	private void recalculatePlaceholderGeometry(int position)
	{
		DocumentPage page = getPage(position);
		mPlaceholderPage.setGeometry(mContext, page.getWidth(), page.getHeight());
		mIsPlaceholderGeometryFinal = true;
		notifyPageScaleChanged(position);
	}

	/** We post a message on the UI thread to reduce the number of times {@link #syncVisibleLoadingPages()} is called. */
	private void postSyncVisibleLoadingPagesIfNeedBe()
	{
		if (!mIsSyncVisibleLoadingPagesPending)
		{
			mHandler.post(mSyncerVisibleLoadingPages);
			mIsSyncVisibleLoadingPagesPending = true;
		}
	}

	/**
	 * Synchronizes {@link #mHasVisiblePagesLoadingStatus} and eventually notifies the relative listener. This method
	 * needs to be called after every {@link #getPage(int) } which is called on the UI thread during draw for each page
	 * drawn. Consequently, the instructions in the inner loop of this method could be executed <em>a lot</em> of times
	 * ({@link #mVisibleLoadingPages}.size() * 60 per second). The generated keys would then be garbaged collected
	 * causing a lot of stutter during drawing. For this reason, we reduce the number of method calls posting a
	 * message on {@link #mHandler} (see {@link #postSyncVisibleLoadingPagesIfNeedBe()} and we reduce garbage collection
	 * with a {@link KeyBuffer}.
	 */
	// only called when mBound is true
	private void syncVisibleLoadingPages()
	{
		Iterator<Integer> iterator = mVisibleLoadingPages.iterator();
		while (iterator.hasNext())
		{
			int position = iterator.next();
			if (!getDocumentView().isPageVisible(position))
			{
				iterator.remove();
				continue;
			}
			Key key = mKeyBuffer.getKey(mDocumentPath, mMarkingDate, position);
			if (mServiceBinder.isResultLoaded(key))
			{
				iterator.remove();
				continue;
			}
		}

		boolean hasVisibleLoadingPages = mVisibleLoadingPages.size() > 0;
		int oldHasVisiblePagesLoading = mHasVisiblePagesLoadingStatus;
		mHasVisiblePagesLoadingStatus = hasVisibleLoadingPages
				? LOADING_STATUS_HAS_VISIBLE_LOADING_PAGES
				: LOADING_STATUS_HAS_NO_VISIBLE_LOADING_PAGES;

		if (oldHasVisiblePagesLoading == LOADING_STATUS_UNITIALIZED
				|| oldHasVisiblePagesLoading != mHasVisiblePagesLoadingStatus)
		{
			if (mOnHasVisibleLoadingPagesStatusListener != null)
				mOnHasVisibleLoadingPagesStatusListener.onHasVisibleLoadingPagesStatusChanged(hasVisibleLoadingPages);
		}
	}

	public interface OnHasVisibleLoadingPagesStatusListener
	{
		void onHasVisibleLoadingPagesStatusChanged(boolean newHasVisibleLoadingPages);
	}

	public interface OnPageCountKnownListener
	{
		void onPageCountKnown(int count);
	}

	/** Avoids frequent garbage collection holding in memory up to a fixed number of keys and releasing them all at once */
	private static class KeyBuffer
	{
		private static final int KEYS_TO_KEEP_IN_MEMORY = 1000;
		private final ArrayList<Key> mBuffer = new ArrayList<Key>(KEYS_TO_KEEP_IN_MEMORY);

		Key getKey(Path document, long markingDate, int pageNumber)
		{
			Key key = Key.makePageKey(document, markingDate, pageNumber);
			if (mBuffer.size() >= KEYS_TO_KEEP_IN_MEMORY)
			{
				mBuffer.clear();
				// Try running gc now, we just release 1000 keys
				System.gc();
			}
			mBuffer.add(key);
			return key;
		}
	}
}