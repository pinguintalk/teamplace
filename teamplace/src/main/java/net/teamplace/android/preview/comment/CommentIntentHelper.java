package net.teamplace.android.preview.comment;

import java.util.ArrayList;

import net.teamplace.android.browsing.path.Path;

import android.content.Context;
import android.content.Intent;


public class CommentIntentHelper
{
	public static final String EXTRA_CHECK_CHANGES = "checkChanges";
	public static final String EXTRA_NEW_COMMENT_NUMBER = "newCommentNumber";
	public static final String EXTRA_CURRENT_COMMENT_IDS = "currentCommentIDs";
	public static final String EXTRA_FILE_PATH = "filePath";
	public static final String EXTRA_SINCE = "since";
	public static final String EXTRA_START_COMMENT = "startComment";
	public static final String EXTRA_COMMENT = "comment";
	public static final String EXTRA_COMMENT_ID = "commentID";

	public static Intent getLoadCommentIntent(Context context, boolean checkChanges, int newCommentNumber,
			ArrayList<String> currentCommentIDs, Path.TeamDrive filePath, long since, int startComment)
	{
		Intent loadCommentIntent = new Intent(context, LoadCommentsIntentService.class);

		loadCommentIntent.putExtra(EXTRA_CHECK_CHANGES, checkChanges);
		loadCommentIntent.putExtra(EXTRA_NEW_COMMENT_NUMBER, newCommentNumber);
		loadCommentIntent.putStringArrayListExtra(EXTRA_CURRENT_COMMENT_IDS, currentCommentIDs);
		loadCommentIntent.putExtra(EXTRA_FILE_PATH, filePath);
		loadCommentIntent.putExtra(EXTRA_SINCE, since);
		loadCommentIntent.putExtra(EXTRA_START_COMMENT, startComment);

		return loadCommentIntent;
	}

	public static Intent getAddCommentIntent(Context context, String comment, ArrayList<String> currentCommentIDs,
			Path.TeamDrive filePath, long since)
	{
		Intent addCommentIntent = new Intent(context, AddCommentIntentService.class);

		addCommentIntent.putExtra(EXTRA_COMMENT, comment);
		addCommentIntent.putStringArrayListExtra(EXTRA_CURRENT_COMMENT_IDS, currentCommentIDs);
		addCommentIntent.putExtra(EXTRA_FILE_PATH, filePath);
		addCommentIntent.putExtra(EXTRA_SINCE, since);

		return addCommentIntent;
	}

	public static Intent getDeleteCommentIntent(Context context, String commentID, ArrayList<String> currentCommentIDs,
			Path.TeamDrive filePath, long since)
	{
		Intent deleteCommentIntent = new Intent(context, DeleteCommentIntentService.class);

		deleteCommentIntent.putExtra(EXTRA_COMMENT_ID, commentID);
		deleteCommentIntent.putStringArrayListExtra(EXTRA_CURRENT_COMMENT_IDS, currentCommentIDs);
		deleteCommentIntent.putExtra(EXTRA_FILE_PATH, filePath);
		deleteCommentIntent.putExtra(EXTRA_SINCE, since);

		return deleteCommentIntent;
	}
}
