package net.teamplace.android.preview.ui;

import net.teamplace.android.preview.widget.DocumentPage;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.cortado.android.R;

/**
 * Placeholder page to use when loading a real DocumentPage. This class supports saving and restoring of its Geometry
 * 
 * @author Gil Vegliach
 */
public class PlaceholderPage implements DocumentPage
{
	private Geometry mGeometry;
	private Drawable mDrawable;

	public PlaceholderPage(Context context)
	{
		Drawable drawable = context.getResources().getDrawable(R.drawable.prv_screen_nail_shape);
		int w = drawable.getIntrinsicWidth();
		int h = drawable.getIntrinsicHeight();
		drawable.setBounds(0, 0, w, h);
		mGeometry = Geometry.valueOf(w, h);
		mDrawable = drawable;
	}

	@Override
	public void draw(Canvas canvas)
	{
		mDrawable.draw(canvas);
	}

	@Override
	public int getWidth()
	{
		return mGeometry.getWidth();
	}

	@Override
	public int getHeight()
	{
		return mGeometry.getHeight();
	}

	public Geometry getGeometry()
	{
		return mGeometry;
	}

	public void setGeometry(Context context, Geometry geometry)
	{
		Drawable drawable = context.getResources().getDrawable(R.drawable.prv_screen_nail_shape);
		drawable.setBounds(0, 0, geometry.getWidth(), geometry.getHeight());
		mGeometry = geometry;
		mDrawable = drawable;
	}

	public void setGeometry(Context context, int width, int height)
	{
		setGeometry(context, Geometry.valueOf(width, height));
	}
}
