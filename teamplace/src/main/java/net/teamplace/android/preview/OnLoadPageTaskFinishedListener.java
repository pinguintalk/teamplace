package net.teamplace.android.preview;

import net.teamplace.android.preview.PreviewService.Result;
import net.teamplace.android.preview.cache.Key;

public interface OnLoadPageTaskFinishedListener
{
	void onLoadPageTaskSuccess(Key.Page key, Result result);

	void onLoadPageTaskCancelled(Key.Page key);

	void onLoadPageTaskError(Key.Page key);
}