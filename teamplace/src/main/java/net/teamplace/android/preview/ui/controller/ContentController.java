package net.teamplace.android.preview.ui.controller;


public interface ContentController extends PreviewController
{
	public static final int REQUEST_VERSION_CHANGED = 1;

	/**
	 * Called when the controller is supposed to update its content
	 * 
	 * @param requestCode
	 *            type of content update
	 */
	public void onContentChangeRequested(int requestCode);
}
