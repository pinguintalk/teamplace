package net.teamplace.android.preview.cache;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.HashMap;

import net.teamplace.android.preview.ui.TiledDocumentPage;
import net.teamplace.android.util.annotations.ThreadSafe;
import net.teamplace.android.utils.Log;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;


/**
 * Wrapper for a LruCache
 * 
 * @author Gil Vegliach
 */
@ThreadSafe
public class DocumentsMemoryCache
{
	public final static String TAG = tag(DocumentsMemoryCache.class);

	private final HashMap<Key.Page, Boolean> mIsPageCached = new HashMap<Key.Page, Boolean>();
	private final HashMap<Key.Count, Integer> mPageCounts = new HashMap<Key.Count, Integer>();
	private final LruCache<Key.Page, TiledDocumentPage> mMemCache;
	private final BitmapPool mBitmapPool;

	public DocumentsMemoryCache(int maxSize, BitmapPool recycleBin)
	{
		mBitmapPool = recycleBin;
		mMemCache = new LruCache<Key.Page, TiledDocumentPage>(maxSize)
		{
			@Override
			protected int sizeOf(Key.Page key, TiledDocumentPage value)
			{
				return (value == null) ? 0 : value.getSize();
			}

			@Override
			protected void entryRemoved(boolean evicted, Key.Page key, TiledDocumentPage oldValue,
					TiledDocumentPage newValue)
			{
				Log.d(TAG, "entryRemoved(), old: " + oldValue + ", new: " + newValue);
				mIsPageCached.put(key, false);

				if (mBitmapPool != null)
				{
					Bitmap[] recycled = oldValue.recycle();
					if (recycled == null)
						return;

					// Recycle evicted bitmaps
					for (Bitmap b : recycled)
					{
						mBitmapPool.put(b);
					}
				}
			}
		};
	}

	/** Checks whether a page is cached without moving the entry to the head of the queue in case of success */
	public synchronized boolean isPageCached(Key.Page key)
	{
		Boolean isCached = mIsPageCached.get(key);
		return isCached != null && isCached.booleanValue();
	}

	/** Returns a page from the memory cache. If the result is non-null the entry is moved to the head of the queue */
	public synchronized TiledDocumentPage get(Key.Page key)
	{
		return mMemCache.get(key);
	}

	/** Stores the page into the memory cache. */
	public synchronized void put(Key.Page key, TiledDocumentPage page)
	{
		mIsPageCached.put(key, true);
		mMemCache.put(key, page);
	}

	/** Resets the state of the cache, clearing all its elements */
	public synchronized void clear()
	{
		mMemCache.evictAll();
		mIsPageCached.clear();
	}

	public synchronized int getPageCount(Key.Count key)
	{
		Integer count = mPageCounts.get(key);
		if (count == null)
			return -1;

		return count.intValue();
	}

	public synchronized void putPageCount(Key.Count key, int count)
	{
		if (count == -1)
		{
			mPageCounts.remove(key);
			return;
		}

		mPageCounts.put(key, count);
	}
}
