package net.teamplace.android.preview.ui;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.preview.ui.controller.ActionController;
import net.teamplace.android.preview.ui.controller.BasicActionController;
import net.teamplace.android.preview.ui.controller.BasicCommentsController;
import net.teamplace.android.preview.ui.controller.BasicFileInfoController;
import net.teamplace.android.preview.ui.controller.BasicVersionsController;
import net.teamplace.android.preview.ui.controller.CommentsController;
import net.teamplace.android.preview.ui.controller.ContentController;
import net.teamplace.android.preview.ui.controller.FileInfoController;
import net.teamplace.android.preview.ui.controller.StaticPreviewContentController;
import net.teamplace.android.preview.ui.controller.VersionsController;
import net.teamplace.android.utils.Log;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;

import com.cortado.android.R;

public class StaticContentPreviewFragment extends PreviewHolderFragment
{
	public static final String TAG = tag(StaticContentPreviewFragment.class);
	private static final String PKG = pkg(StaticContentPreviewFragment.class);

	private static final String KEY_CLICK_HANDLER_TYPE = PKG + ".clickHandlerType";
	private static final String KEY_MASTER_DOC_INFO = PKG + ".masterDocInfo";
	private static final String KEY_ON_CLICK_LAYOUT = ".onClickLayout";
	private static final String KEY_PREVIEW_CONFIGURATION = PKG + ".previewConfiguration";

	public static StaticContentPreviewFragment newInstance(FileInfo info, PreviewConfiguration configuration,
														   RemoteViews onClickLayout,	int clickHandlerType)
	{
		StaticContentPreviewFragment fragment = new StaticContentPreviewFragment();
		Bundle args = new Bundle();
		args.putParcelable(KEY_MASTER_DOC_INFO, info);
		args.putInt(KEY_CLICK_HANDLER_TYPE, clickHandlerType);
		args.putParcelable(KEY_ON_CLICK_LAYOUT, onClickLayout);
		args.putParcelable(KEY_PREVIEW_CONFIGURATION, configuration);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	protected ContentController createPreviewContentController()
	{
		return new StaticPreviewContentController();
	}

	@Override
	protected FileInfoController createFileInfoController()
	{
		return new BasicFileInfoController(this);
	}

	@Override
	protected VersionsController createVersionsController()
	{
		return new BasicVersionsController(this);
	}

	@Override
	protected CommentsController createCommentsController()
	{
		return new BasicCommentsController(this);
	}

	@Override
	protected ActionController createActionController()
	{
		return new BasicActionController(this);
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		getActionController().onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getActionController().onCreate(savedInstanceState);

		if (savedInstanceState == null)
		{
			Bundle args = getArguments();
			FileInfo doc = (FileInfo) args.getParcelable(KEY_MASTER_DOC_INFO);
			getFileInfoController().setMasterDocument(doc);
			getFileInfoController().setDisplayingPath(doc.getPath());
			getFileInfoController().setDisplayingDate(doc.getLastModifiedDate());
			PreviewConfiguration configuration = args.getParcelable(KEY_PREVIEW_CONFIGURATION);
			getFileInfoController().setPreviewConfiguration(configuration);
			getCommentsController().setPreviewConfiguration(configuration);
		}
		getFileInfoController().onCreate(savedInstanceState);
		getCommentsController().onCreate(savedInstanceState);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.prv_static_preview_fragment, container, false);

		RemoteViews rv = getArguments().getParcelable(KEY_ON_CLICK_LAYOUT);
		View onClickView = rv.apply(getActivity(), container);
		onClickView.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				OnStaticContentClickedHandler clickHandler = OnStaticContentClickedHandler.get(getArguments().getInt(
						KEY_CLICK_HANDLER_TYPE));
				if (clickHandler != null)
				{
					clickHandler.onContentClicked(getActivity(), getFileInfoController().getMasterDocument(),
							getFileInfoController().getDisplayingPath(), getFileInfoController().getDisplayingDate());
				}
			}
		});
		((RelativeLayout) root.findViewById(R.id.static_view_container)).addView(onClickView);

		getFileInfoController().setFragmentView(root);
		getFileInfoController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().setFragmentView(root);
		getVersionsController().onCreateView(inflater, container, savedInstanceState);

		getVersionsController().onUiUpdateRequested();

		getActionController().setFragmentView(root);
		getActionController().onCreateView(inflater, container, savedInstanceState);

		getCommentsController().setFragmentView(root);
		getCommentsController().onCreateView(inflater, container, savedInstanceState);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getVersionsController().onActivityCreated(savedInstanceState);
	}

	@Override
	public void onResume()
	{
		super.onResume();
		getVersionsController().onResume();
		getActionController().onResume();
		getCommentsController().onResume();
	}

	@Override
	public void onPause()
	{
		super.onPause();
		getVersionsController().onPause();
		getActionController().onPause();
		getCommentsController().onPause();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		getActionController().onSaveInstanceState(outState);
		getFileInfoController().onSaveInstanceState(outState);
		getVersionsController().onSaveInstanceState(outState);
		getCommentsController().onSaveInstanceState(outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}
		getActionController().onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		getActionController().onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		getActionController().onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		return getActionController().onOptionsItemSelected(item);
	}
}
