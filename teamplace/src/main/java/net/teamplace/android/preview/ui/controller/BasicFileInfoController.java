package net.teamplace.android.preview.ui.controller;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.preview.widget.PreviewFooterView;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cortado.android.R;

public class BasicFileInfoController implements FileInfoController
{
	public static final String TAG = tag(BasicFileInfoController.class);
	private static final String PKG = pkg(BasicFileInfoController.class);

	private static final String KEY_MASTER_DOC_INFO = PKG + ".masterDocInfo";
	private static final String KEY_DISPLAYING_PATH = PKG + ".displayingPath";
	private static final String KEY_DISPLAYING_DATE = PKG + ".displayingDate";
	private static final String KEY_FOLDER_CONTENT = PKG + ".mFolderContent";

	private PreviewHolderFragment mFragment;
	private View mFragmentView;
	private PreviewFooterView mFooterView;
	private PreviewConfiguration mPreviewConfiguration;

	/** Master document information without versions. This document can change if we perform a rename */
	private FileInfo mMasterDocument;

	private ArrayList<FileInfo> mFolderContent = new ArrayList<>();

	/**
	 * Path of the document that we are displaying on the screen. It can be the path of {@link #mMasterDocument} or
	 * some of its versions
	 */
	// Must be kept in sync with mDisplayingDate
	private Path mDisplayingPath;

	/**
	 * Marking date of the document that we are displaying on the screen. It can be the last modified date of
	 * {@link #mMasterDocument} or the last edit date of the version referred by {@link #mDisplayingPath}
	 */
	// Must be kept in sync with mDisplayingPath
	private long mDisplayingDate;

	public BasicFileInfoController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	@Override
	public void setFragmentView(View v)
	{
		mFragmentView = v;
	}

	@Override
	public void setDisplayingPath(Path displayingPath)
	{
		mDisplayingPath = displayingPath;
	}

	@Override
	public Path getDisplayingPath()
	{
		return mDisplayingPath;
	}

	@Override
	public void setDisplayingDate(long date)
	{
		mDisplayingDate = date;
	}

	public long getDisplayingDate()
	{
		return mDisplayingDate;
	}

	@Override
	public void setMasterDocument(FileInfo masterDocument)
	{
		mMasterDocument = masterDocument;
	}

	@Override
	public FileInfo getMasterDocument()
	{
		return mMasterDocument;
	}

	@Override
	public void setFolderContent(List<FileInfo> folderContent)
	{
		synchronized (mFolderContent)
		{
			mFolderContent.clear();
			mFolderContent.addAll(folderContent);
		}
	}

	@Override
	public List<FileInfo> getFolderContent()
	{
		synchronized (mFolderContent)
		{
			return mFolderContent;
		}
	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {
		mPreviewConfiguration = configuration;
	}

	@Override
	public void onHideOverlaysRequested()
	{
		mFooterView.hide();
	}

	@Override
	public void onShowOverlaysRequested()
	{
		mFooterView.showSelectedOrCollapsed();
	}

	@Override
	public int getOverlayState()
	{
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		if (savedInstanceState != null)
		{
			mMasterDocument = savedInstanceState.getParcelable(KEY_MASTER_DOC_INFO);
			mDisplayingPath = savedInstanceState.getParcelable(KEY_DISPLAYING_PATH);
			mDisplayingDate = savedInstanceState.getLong(KEY_DISPLAYING_DATE);
			mFolderContent = savedInstanceState.getParcelableArrayList(KEY_FOLDER_CONTENT);
		}
		else
		{
			if (mPreviewConfiguration != null)
			{
				setFolderContent(mPreviewConfiguration.getFolderContent());
				if (mPreviewConfiguration.getOpenVersionId() != null)
				{
					// use requested version
					mDisplayingPath = PathUtils.buildRenamedPath(
							mDisplayingPath, mPreviewConfiguration.getOpenVersionId());
				}
			}
		}
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mFooterView = (PreviewFooterView) mFragmentView.findViewById(R.id.footer_view);
		if (savedInstanceState != null)
		{
			mMasterDocument = savedInstanceState.getParcelable(KEY_MASTER_DOC_INFO);
			mDisplayingPath = savedInstanceState.getParcelable(KEY_DISPLAYING_PATH);
			mDisplayingDate = savedInstanceState.getLong(KEY_DISPLAYING_DATE);
			mFolderContent = savedInstanceState.getParcelableArrayList(KEY_FOLDER_CONTENT);
		}
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putParcelable(KEY_MASTER_DOC_INFO, mMasterDocument);
		outState.putParcelable(KEY_DISPLAYING_PATH, mDisplayingPath);
		outState.putLong(KEY_DISPLAYING_DATE, mDisplayingDate);
		outState.putParcelableArrayList(KEY_FOLDER_CONTENT, mFolderContent);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

}
