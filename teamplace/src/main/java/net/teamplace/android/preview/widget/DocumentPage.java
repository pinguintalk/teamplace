package net.teamplace.android.preview.widget;

import android.graphics.Canvas;

/**
 * Represents a document page in DocumentView
 * 
 * @author Gil Vegliach
 */
public interface DocumentPage
{
	/** Draws this page at the top-left corner in the canvas, i.e. position 0,0 */
	void draw(Canvas canvas);

	/** Returns width of the page */
	int getWidth();

	/** Returns height of the page */
	int getHeight();
}