package net.teamplace.android.preview.cache;

import static net.teamplace.android.utils.GenericUtils.tag;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import net.teamplace.android.preview.cache.Key.Count;
import net.teamplace.android.preview.cache.Key.Page;
import net.teamplace.android.util.annotations.Blocking;
import net.teamplace.android.util.annotations.NonBlocking;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;


/**
 * Wrapper for DiskCache with lazy loading and flushing after every write. External synchronization required
 * 
 * @author Gil Vegliach
 */
public class DocumentsDiskCache
{
	public static final String TAG = tag(DocumentsDiskCache.class);

	private final long mSize;
	private final File mDirectory;
	private DiskLruCache mDiskCache;

	@NonBlocking
	public DocumentsDiskCache(File directory, long size)
	{
		mSize = size;
		mDirectory = directory;
	}

	/**
	 * Retrieves the page for the given key. If the page is not present returns null. An IOException is thrown on IO
	 * failure
	 */
	@Blocking
	public PageData retrievePageData(Page key) throws IOException
	{
		ensureOpened();

		PageData result = null;
		DiskLruCache.Snapshot snapshot = mDiskCache.get(key.toString());

		if (snapshot != null)
		{
			InputStream is = snapshot.getInputStream(0);
			try
			{
				result = PageData.newInstance(is);
				checkNonNull(result.getData(), "Failed to retrieve " + key.toString());
			}
			finally
			{
				GenericUtils.closeQuietly(TAG, is);
				snapshot.close();
			}
		}

		return result;
	}

	/**
	 * Retrieves the page count for the given key. If the page count is not present returns -1. An IOException is thrown
	 * on IO failure
	 */
	@Blocking
	public int retrievePageCount(Count key) throws IOException
	{
		ensureOpened();

		int result = -1;
		DiskLruCache.Snapshot snapshot = mDiskCache.get(key.toString());

		if (snapshot != null)
		{
			InputStream is = snapshot.getInputStream(0);
			try
			{
				byte[] data = GenericUtils.inputStreamToByteArray(is);
				result = Integer.parseInt(new String(data));
				checkCount(result);
			}
			finally
			{
				GenericUtils.closeQuietly(TAG, is);
				snapshot.close();
			}
		}

		return result;
	}

	@Blocking
	public void storePageData(Page key, PageData value) throws IOException
	{
		checkNonNull(value, "value must be non-null");

		// No data to store, return
		if (value.getData() == null)
			return;

		ensureOpened();

		DiskLruCache.Editor editor = mDiskCache.edit(key.toString());
		checkNonNull(editor, "Another edit() in progress, synchronization error!");

		OutputStream os = editor.newOutputStream(0);
		try
		{
			os.write(value.getData());
			os.flush();
		}
		finally
		{
			GenericUtils.closeQuietly(TAG, os);
			editor.commit();
			mDiskCache.flush();
		}
	}

	@Blocking
	public void storePageCount(Count key, int pageCount) throws IOException
	{
		checkCount(pageCount);
		ensureOpened();

		DiskLruCache.Editor editor = mDiskCache.edit(key.toString());
		checkNonNull(editor, "Another edit() in progress, synchronization error!");

		OutputStream os = editor.newOutputStream(0);
		try
		{
			byte[] data = String.valueOf(pageCount).getBytes();
			os.write(data);
			os.flush();
		}
		finally
		{
			GenericUtils.closeQuietly(TAG, os);
			editor.commit();
			mDiskCache.flush();
		}
	}

	@Blocking
	public void close()
	{
		if (mDiskCache == null)
			return;

		try
		{
			// Flushing not needed, but just in case...
			mDiskCache.flush();
			mDiskCache.close();
		}
		catch (IOException e)
		{
			Log.e(TAG, "Error closing DiskCache", e);
		}
	}

	private void ensureOpened() throws IOException
	{
		if (mDiskCache == null)
			mDiskCache = DiskLruCache.open(mDirectory, 1, 1, mSize);
	}

	private void checkNonNull(Object o, String message) throws IOException
	{
		if (o == null)
			throw new IOException(message);
	}

	private void checkCount(int count)
	{
		if (count == -1)
			throw new IllegalStateException("Page count must be greated than or equal to 0 in disk cache, was: "
					+ count);
	}

	public static class PageData
	{
		private final byte[] mData;

		private PageData(byte[] data)
		{
			mData = data;
		}

		public boolean hasData()
		{
			return mData != null;
		}

		public byte[] getData()
		{
			return mData;
		}

		public static PageData newInstance(InputStream is)
		{
			byte[] data = GenericUtils.inputStreamToByteArray(is);
			return new PageData(data);
		}

		public static PageData newInstance(byte[] data)
		{
			return new PageData(data);
		}
	}
}
