package net.teamplace.android.preview.ui.controller;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.preview.ui.controller.ImagePagerAdapter.GestureListener;
import net.teamplace.android.preview.ui.controller.ImagePagerAdapter.ItemViewInfo;
import net.teamplace.android.utils.GenericUtils;

import android.animation.Animator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.cortado.android.R;

public class ImageContentController implements ContentController
{
	private static final String PKG = GenericUtils.pkg(ImageContentController.class);

	private static final String KEY_ADAPTER_DATA = PKG + "adapter data";

	private static final int OFF_SCREEN_PAGE_LIMIT = 2;

	private PreviewHolderFragment mFragment;
	private View mView;
	private ViewPager mViewPager;
	private ImagePagerAdapter mAdapter;

	public ImageContentController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	@Override
	public void setFragmentView(View v)
	{
		mView = v;
	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {

	}

	@Override
	public void onHideOverlaysRequested()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onShowOverlaysRequested()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public int getOverlayState()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{

		List<ItemViewInfo> itemViewInfos;
		if (savedInstanceState != null)
		{
			itemViewInfos = savedInstanceState.getParcelableArrayList(KEY_ADAPTER_DATA);
		}
		else
		{
			List<FileInfo> folderContent = mFragment.getFileInfoController().getFolderContent();

			// Disable swiping in FavoritesFragment
			if (isMixedList(folderContent))
			{
				folderContent.clear();
				folderContent.add(mFragment.getFileInfoController().getMasterDocument());
			}

			itemViewInfos = ImagePagerAdapter.ItemViewInfo.fromFileInfoList(folderContent);
		}

		mViewPager = (ViewPager) mView.findViewById(R.id.vp_images);
		mViewPager.setOffscreenPageLimit(OFF_SCREEN_PAGE_LIMIT);
		mAdapter = new ImagePagerAdapter(mFragment.getActivity(), itemViewInfos);
		mViewPager.setAdapter(mAdapter);
		mViewPager.setOnPageChangeListener(new OnPageChangeListener()
		{

			@Override
			public void onPageSelected(int arg0)
			{
				mFragment.getVersionsController().clearVersionUi();
				ItemViewInfo currentInfo = mAdapter.getInfoAtPosition(mViewPager.getCurrentItem());
				FileInfo baseDocInfo = currentInfo.getBaseDocInfo();
				Path versionPath = currentInfo.getVersionPath();
				long versionDate = currentInfo.getVersionTimestamp();
				mFragment.getFileInfoController().setMasterDocument(baseDocInfo);
				mFragment.getFileInfoController().setDisplayingPath(versionPath);
				mFragment.getFileInfoController().setDisplayingDate(versionDate);
				mFragment.getVersionsController().onUiUpdateRequested();
				mFragment.getVersionsController().onReloadVersionsRequested();
				mFragment.getActivity().invalidateOptionsMenu();
				mFragment.getCommentsController().onUpdateDocument();
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2)
			{
				// TODO Auto-generated method stub

			}

			@Override
			public void onPageScrollStateChanged(int arg0)
			{
				// TODO Auto-generated method stub

			}
		});
		mViewPager.setCurrentItem(mAdapter.indexOfFileInfo(mFragment.getFileInfoController().getMasterDocument()));
		mAdapter.setGestureListener(new GestureListener()
		{
			@Override
			public void onSingleTap()
			{
				ImageContentController.this.onSingleTapConfirmed();
			}
		});
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		ArrayList<ItemViewInfo> toPersist = new ArrayList<>();
		toPersist.addAll(mAdapter.getAllData());
		outState.putParcelableArrayList(KEY_ADAPTER_DATA, toPersist);
	}

	@Override
	public void onStart()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onStop()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onContentChangeRequested(int requestCode)
	{
		switch (requestCode)
		{
			case REQUEST_VERSION_CHANGED:
				// rebuildAdapter();
				// mDocumentView.setAdapter(mAdapter);
				Integer currentItemViewTag = Integer.valueOf(mViewPager.getCurrentItem());
				// mAdapter.updateItemView(mViewPager.findViewWithTag(currentItemViewTag), mFragment
				// .getFileInfoController().getDisplayingPath());
				mAdapter.updateItemVersion(mViewPager.findViewWithTag(currentItemViewTag), mFragment
						.getFileInfoController().getDisplayingPath(), mFragment.getFileInfoController()
						.getDisplayingDate());

				break;

			default:
				break;
		}
	}

	/**
	 * Checks if we're actually previewing favorites
	 * 
	 * @param infos
	 * @return
	 */
	private boolean isMixedList(List<FileInfo> infos)
	{
		String location = infos.get(0).getPath().getLocation();
		for (FileInfo info : infos)
		{
			if (!location.equals(info.getPath().getLocation()))
			{
				return true;
			}
		}
		return false;
	}

	private void onSingleTapConfirmed()
	{
		if (mFragment.getCommentsController().getOverlayState() == CommentsController.OVERLAY_HIDDEN)
		{
			if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_VISIBLE)
			{
				mFragment.getActionController().onHideOverlaysRequested();
				mFragment.getPreviewContentController().onHideOverlaysRequested();
				mFragment.getCommentsController().onHideOverlaysRequested();
				mFragment.getFileInfoController().onHideOverlaysRequested();
			}
			else
			{
				mFragment.getActionController().onShowOverlaysRequested();
				mFragment.getPreviewContentController().onShowOverlaysRequested();
				mFragment.getCommentsController().onShowOverlaysRequested();
				mFragment.getFileInfoController().onShowOverlaysRequested();
			}
		}
	}

}
