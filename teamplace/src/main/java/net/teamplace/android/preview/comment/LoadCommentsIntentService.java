package net.teamplace.android.preview.comment;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.Comment;
import net.teamplace.android.http.request.comments.CommentsRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import android.app.IntentService;
import android.content.Intent;
import de.greenrobot.event.EventBus;

public class LoadCommentsIntentService extends IntentService
{
	private final String TAG = getClass().getSimpleName();

	public static final int RESPONSE_CODE = 1;

	public LoadCommentsIntentService()
	{
		super("LoadCommentsIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		boolean checkChanges = intent.getBooleanExtra(CommentIntentHelper.EXTRA_CHECK_CHANGES, false);
		int newCommentNumber = intent.getIntExtra(CommentIntentHelper.EXTRA_NEW_COMMENT_NUMBER, 50);
		Path.TeamDrive filePath = intent.getParcelableExtra(CommentIntentHelper.EXTRA_FILE_PATH);
		int startComment = intent.getIntExtra(CommentIntentHelper.EXTRA_START_COMMENT, -1);

		SessionManager manager = new SessionManager(this);
		Session session = manager.getLastSession();

		CommentsRequester requester = new CommentsRequester(this, session, new TeamplaceBackend());

		ChangedCommentResponse changeResponse = null;
		List<Comment> newComments = null;

		if (checkChanges)
		{
			ArrayList<String> currentCommentIDs = intent
					.getStringArrayListExtra(CommentIntentHelper.EXTRA_CURRENT_COMMENT_IDS);
			long since = intent.getLongExtra(CommentIntentHelper.EXTRA_SINCE, -1);
			try
			{
				if (currentCommentIDs.size() > 0)
				{
					changeResponse = requester.getChangedComments(filePath.getTeamDriveId(),
							PathUtils.remoteRequestString(filePath.getParentPath()), filePath.getName(),
							(int) since,
							currentCommentIDs);
				}
			}
			catch (Exception e)
			{
				ExceptionManager em = ExceptionManager.getInstance();
				em.handleException(e);
			}
		}

		try
		{
			newComments = requester
					.getAllFileComments(filePath.getTeamDriveId(),
							PathUtils.remoteRequestString(filePath.getParentPath()), filePath.getName(),
							newCommentNumber, startComment);
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		EventBus.getDefault().postSticky(
				new CommentOperationResponse(RESPONSE_CODE, changeResponse, newComments, checkChanges,
						newCommentNumber, filePath, null));
	}
}
