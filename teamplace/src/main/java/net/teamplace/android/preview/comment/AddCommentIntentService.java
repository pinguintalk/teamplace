package net.teamplace.android.preview.comment;

import java.util.ArrayList;

import net.teamplace.android.backend.TeamplaceBackend;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.errorhandling.ExceptionManager;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.CommentsRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import android.app.IntentService;
import android.content.Intent;
import de.greenrobot.event.EventBus;

public class AddCommentIntentService extends IntentService
{
	private final String TAG = getClass().getSimpleName();

	public static final int RESPONSE_CODE = 2;

	public AddCommentIntentService()
	{
		super("AddCommentIntentService");
	}

	@Override
	protected void onHandleIntent(Intent intent)
	{
		String comment = intent.getStringExtra(CommentIntentHelper.EXTRA_COMMENT);
		Path.TeamDrive filePath = intent.getParcelableExtra(CommentIntentHelper.EXTRA_FILE_PATH);
		ArrayList<String> currentCommentIDs = intent
				.getStringArrayListExtra(CommentIntentHelper.EXTRA_CURRENT_COMMENT_IDS);
		long since = intent.getLongExtra(CommentIntentHelper.EXTRA_SINCE, -1);

		SessionManager manager = new SessionManager(this);
		Session session = manager.getLastSession();

		CommentsRequester requester = new CommentsRequester(this, session, new TeamplaceBackend());

		try
		{
			requester.addComment(filePath.getTeamDriveId(),
					PathUtils.remoteRequestString(filePath.getParentPath()), filePath.getName(), comment);
		}
		catch (Exception e)
		{
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		ChangedCommentResponse changeResponse = null;

		try
		{
			changeResponse = requester.getChangedComments(filePath.getTeamDriveId(),
					PathUtils.remoteRequestString(filePath.getParentPath()), filePath.getName(), since,
					currentCommentIDs);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			ExceptionManager em = ExceptionManager.getInstance();
			em.handleException(e);
		}

		EventBus.getDefault().postSticky(
				new CommentOperationResponse(RESPONSE_CODE, changeResponse, null, false, 0, filePath, null));
	}

}
