package net.teamplace.android.preview.ui;

import net.teamplace.android.preview.cache.BitmapPool;
import net.teamplace.android.preview.widget.DocumentPage;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;


/**
 * Implementation of DocumentPage that supports tiling for faster drawing
 * 
 * @author Gil Vegliach
 */
public class TiledDocumentPage implements DocumentPage
{
	private final boolean mIsTiled;
	private final Bitmap mTiles[][];
	private final RectF mBounds[][];
	private final Bitmap mBitmap;

	private final int mWidth;
	private final int mHeight;
	private final int mSize;

	private final RectF mClipBoundsF = new RectF();
	private final Rect mClipBounds = new Rect();

	private final Paint mPaint = new Paint();
	{
		mPaint.setFilterBitmap(true);
	}

	/** Creates a Tiled DocumentPage optimized for fast drawing */
	public TiledDocumentPage(Bitmap b, int tilew, int tileh, BitmapPool pool)
	{
		mWidth = b.getWidth();
		mHeight = b.getHeight();
		mSize = mWidth * mHeight * 4;

		if (tilew >= mWidth && tileh >= mHeight)
		{
			mIsTiled = false;
			mBitmap = b;
			mBounds = null;
			mTiles = null;
		}
		else
		{
			mIsTiled = true;
			mBitmap = null;

			int m = (int) Math.ceil(mHeight / ((double) tileh));
			int n = (int) Math.ceil(mWidth / ((double) tilew));
			mBounds = new RectF[m][n];
			mTiles = new Bitmap[m][n];
			initTiles(b, tilew, tileh, m, n, pool);
		}
	}

	@Override
	public void draw(Canvas canvas)
	{
		updateClipBounds(canvas);
		if (mIsTiled)
		{
			drawTiled(canvas);
		}
		else
		{
			drawNotTiled(canvas);
		}
	}

	@Override
	public int getWidth()
	{
		return mWidth;
	}

	@Override
	public int getHeight()
	{
		return mHeight;
	}

	public boolean isTiled()
	{
		return mIsTiled;
	}

	/**
	 * Size of this instance in bytes. This number does not include memory consumption for references, but this amount
	 * of memory is negligible
	 */
	public int getSize()
	{
		return mSize;
	}

	// TODO: experimental
	/**
	 * Invalidates this image and offer underlying Bitmap for recycling. This method does not call Bitmap.recycle() on
	 * those bitmaps
	 */
	public Bitmap[] recycle()
	{
		if (mIsTiled)
		{
			int n = mTiles.length;
			int m = mTiles[0].length;
			Bitmap[] result = new Bitmap[n * m];
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < m; j++)
				{
					result[i * m + j] = mTiles[i][j];
				}
			}
			return result;
		}

		return new Bitmap[] {mBitmap};
	}

	private void initTiles(Bitmap b, int tilew, int tileh, int m, int n, BitmapPool pool)
	{
		Rect src = new Rect(), dst = new Rect();
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				int l = j * tilew;
				int t = i * tileh;

				// Take pieces as large as possible, or what's left
				int w = (mWidth - l) >= tilew ? tilew : (mWidth - l);
				int h = (mHeight - t) >= tileh ? tileh : (mHeight - t);
				src.set(l, t, l + w, t + h);
				dst.set(0, 0, w, h);
				mBounds[i][j] = new RectF(src);

				// This is a mutable bitmap suitable for recycling
				Bitmap temp = null;
				if (pool != null)
				{
					temp = pool.get(Geometry.valueOf(w, h));
				}

				if (temp == null)
				{
					temp = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
					// Post condition: temp != null
				}

				mTiles[i][j] = temp;
				Canvas c = new Canvas(temp);
				c.drawBitmap(b, src, dst, null);
			}
		}
	}

	private void updateClipBounds(Canvas canvas)
	{
		canvas.getClipBounds(mClipBounds);
		mClipBoundsF.set(mClipBounds);
	}

	private void drawTiled(Canvas canvas)
	{
		for (int i = 0; i < mTiles.length; i++)
		{
			for (int j = 0; j < mTiles[0].length; j++)
			{
				if (!RectF.intersects(mClipBoundsF, mBounds[i][j]))
				{
					continue;
				}

				canvas.save();
				canvas.translate(mBounds[i][j].left, mBounds[i][j].top);
				canvas.drawBitmap(mTiles[i][j], 0, 0, mPaint);
				canvas.restore();
			}
		}
	}

	private void drawNotTiled(Canvas canvas)
	{
		canvas.drawBitmap(mBitmap, 0, 0, mPaint);
	}
}
