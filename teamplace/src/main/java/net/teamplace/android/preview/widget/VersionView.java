package net.teamplace.android.preview.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.cortado.android.R;

public class VersionView extends ImageView
{
	private final float mDensity;
	private final int mVersionBgColor;
	private final int mVersionTextColor;
	private final Paint mTextPaint;

	private int mCount = 0;
	private Drawable mVersionDrawable = null;

	public VersionView(Context context, AttributeSet attrs)
	{
		super(context, attrs);

		Resources res = context.getResources();
		mDensity = res.getDisplayMetrics().density;
		mVersionBgColor = res.getColor(R.color.prv_version_bg);
		mVersionTextColor = res.getColor(R.color.prv_version_text);

		mTextPaint = new Paint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setTextSize(18.f * mDensity);
		mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
		mTextPaint.setColor(mVersionTextColor);
	}

	public void setCount(int count)
	{
		mCount = count;

		if (count < 1)
		{
			mVersionDrawable = null;
			invalidate();
			return;
		}

		String versionLabel = String.valueOf(count);
		int w = (int) (20.f * mDensity + .5f);
		int h = w;

		Rect textBounds = new Rect();
		mTextPaint.getTextBounds(versionLabel, 0, versionLabel.length(), textBounds);

		if (w < textBounds.width())
			w = textBounds.width() + (int) (4.f * mDensity + .5f);

		GradientDrawable d = new GradientDrawable();
		d.setShape(GradientDrawable.RECTANGLE);
		d.setSize(w, h);
		d.setCornerRadius(1.5f * mDensity);
		d.setColor(mVersionBgColor);
		d.setBounds(0, 0, w, h);

		Bitmap b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		d.draw(c);
		c.drawText(versionLabel, (w - textBounds.width()) / 2.f, (h + textBounds.height()) / 2.f, mTextPaint);

		mVersionDrawable = new BitmapDrawable(getContext().getResources(), b);
		mVersionDrawable.setBounds(0, 0, w, h);
		invalidate();
	}

	public int getCount()
	{
		return mCount;
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);

		if (mVersionDrawable != null)
		{
			int w = getWidth();
			int vh = mVersionDrawable.getIntrinsicHeight();
			canvas.save();
			canvas.translate(0, (vh/2));
			mVersionDrawable.draw(canvas);
			canvas.restore();
		}
	}
}