package net.teamplace.android.preview;

import java.io.IOException;
import java.net.MalformedURLException;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.PreviewInputStream;
import net.teamplace.android.http.request.preview.PreviewRequester;
import net.teamplace.android.preview.PreviewService.Result;
import net.teamplace.android.preview.cache.BitmapPool;
import net.teamplace.android.preview.cache.DocumentsDiskCache;
import net.teamplace.android.preview.cache.DocumentsDiskCache.PageData;
import net.teamplace.android.preview.cache.Key;
import net.teamplace.android.preview.ui.Geometry;
import net.teamplace.android.preview.ui.TiledDocumentPage;
import net.teamplace.android.providers.http.PreviewRequesterProvider;
import net.teamplace.android.util.annotations.Immutable;
import net.teamplace.android.utils.ContextUtils;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.GraphicsUtils;
import net.teamplace.android.utils.Log;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Build.VERSION_CODES;
import android.os.SystemClock;
import android.util.DisplayMetrics;

public class LoadPageTask extends AsyncTask<Void, Void, Result>
{
	// TODO: this should be taken from some UI components that know about GPU
	private static final int MAX_TILE_SIZE = 512;
	private static final int MAX_ATTEMPTS = 5;
	/** How long to wait between consecutive attempts of a request */
	private static final long WAIT_BETWEEN_ATTEMPTS_MS = 300;

	// Least maximum texture size on Android supported by OpenGL. Can be higher on some devices; on those devices the
	// rendering of big textures looks much slower
	private static final int MAX_TEXTURE_SIZE_PX = 2048;

	private final Context mContext;
	private final Key.Page mPkey;
	private final BitmapPool mBitmapPool;
	private final DocumentsDiskCache mDiskCache;
	private final OnLoadPageTaskFinishedListener mListener;
	private final PreviewRequesterProvider mRequesterProvider;
	private final int mMaxMemoryBytes;

	public LoadPageTask(Context context, Key.Page pkey, BitmapPool pool, DocumentsDiskCache diskCache,
			OnLoadPageTaskFinishedListener listener, PreviewRequesterProvider requesterProvider,
			int maxMemoryBytes)
	{
		mContext = context;
		mPkey = pkey;
		mBitmapPool = pool;
		mDiskCache = diskCache;
		mListener = listener;
		mRequesterProvider = requesterProvider;
		mMaxMemoryBytes = maxMemoryBytes;
	}

	@Override
	protected Result doInBackground(Void... params)
	{
		ServerResult cached = retrieveFromDiskCache();
		ServerResult result = cached;

		// Result has not been found in the cache, ask the server
		if (!cached.hasData())
		{
			int pageNumber = mPkey.getPageNumber();
			result = downloadPageAndCount(cached, pageNumber, MAX_ATTEMPTS, WAIT_BETWEEN_ATTEMPTS_MS);
			storeIntoDiskCache(result);
		}

		// Now result contains the cached or the downloaded result, or null
		byte[] data = result.data;
		int count = result.pageCount;
		result = null;
		cached = null;

		// If there is data, decode it and turn it into a DocumentPage
		TiledDocumentPage page = null;
		if (data != null)
		{
			try
			{
				Bitmap bitmap = downsampleAndDecodeByteArray(data);

				if (bitmap != null)
				{
					checkMemorySufficient((long) bitmap.getWidth(), (long) bitmap.getHeight());
					// page = new TiledDocumentPage(bitmap, MAX_TILE_SIZE, MAX_TILE_SIZE, mBitmapPool);
					page = new TiledDocumentPage(bitmap, bitmap.getWidth(), bitmap.getHeight(), mBitmapPool);

					// If the page is tiled the large bitmap will be gc'd
					if (mBitmapPool != null && page.isTiled())
					{
						mBitmapPool.put(bitmap);
					}
				}
			}
			catch (LowMemoryException e)
			{
				e.printStackTrace();
				Runtime.getRuntime().gc();
				return null;
			}
		}
		return new Result(page, count);
	}

	@Override
	protected void onPostExecute(Result result)
	{
		if (mListener != null)
		{
			if (result != null)
			{
				mListener.onLoadPageTaskSuccess(mPkey, result);
			}
			else
			{
				mListener.onLoadPageTaskError(mPkey);
			}
		}
	}

	@Override
	protected void onCancelled(Result result)
	{
		if (mListener != null)
		{
			mListener.onLoadPageTaskCancelled(mPkey);
		}
	}

	/** Stores a ServerResult into the disk cache. Only valid values are stored */
	private void storeIntoDiskCache(ServerResult result)
	{
		synchronized (mDiskCache)
		{
			try
			{
				if (result.data != null)
				{
					mDiskCache.storePageData(mPkey, PageData.newInstance(result.data));
				}

				if (result.pageCount != -1)
				{
					mDiskCache.storePageCount(Key.makeCountKey(mPkey), result.pageCount);
				}
			}
			catch (IOException e)
			{
				throw new IllegalStateException("Disk cache IO error", e);
			}
		}
	}

	/**
	 * Retrieves a ServerResult from the disk cache. The result is as it would be downloaded from the server, i.e.
	 * data can be null and pageCount can be -1
	 */
	private ServerResult retrieveFromDiskCache()
	{
		byte[] data = null;
		int count = -1;

		synchronized (mDiskCache)
		{
			try
			{
				Key.Count keyCount = Key.makeCountKey(mPkey);
				count = mDiskCache.retrievePageCount(keyCount);

				PageData pageData = mDiskCache.retrievePageData(mPkey);
				if (pageData != null)
				{
					data = pageData.getData();
				}
			}
			catch (IOException e)
			{
				throw new IllegalStateException("Disk cache IO error", e);
			}
		}

		return new ServerResult(data, count);
	}

	// Might be handy later
	@SuppressWarnings("unused")
	private int forcePageCountDownload(long maxWaitMillis)
	{
		int pageCount = -1;

		for (long now = now(), end = now + maxWaitMillis, sleep = 100L; now < end; now = now(), sleep *= 2L)
		{
			ServerResult result = downloadPageAndCount(null, 0, 1, 0L);

			if (result.pageCount != -1)
			{
				pageCount = result.pageCount;
				break;
			}

			// If we were to sleep and to go past the ending time, skip the sleep and just return what we already
			// have
			now = now();
			if (now + sleep >= end)
			{
				break;
			}

			// Exponential back-off uninterruptible sleep
			SystemClock.sleep(sleep);
		}

		return pageCount;
	}

	private long now()
	{
		return SystemClock.elapsedRealtime();
	}

	/**
	 * Returns a pair with the loaded page as a bitmap (or null) and the total page number (or -1) and merge it with
	 * the initialValues
	 * 
	 * @throws NotificationException
	 * @throws XCBStatusException
	 * @hide
	 */
	private ServerResult downloadPageAndCount(ServerResult initialValues, int position,
			int maxAttempts, long waitBetweenAttempts)

	{
		byte[] data = null;
		int pageCount = -1;

		if (initialValues != null)
		{
			data = initialValues.data;
			pageCount = initialValues.pageCount;
		}

		int normalizedPos = position + 1;
		boolean canRetry = true;

		while (canRetry && maxAttempts-- > 0)
		{
			Path path = mPkey.getPath();
			String teamDriveId = PathUtils.getTeamDriveIdOrNull(path);
			PreviewRequester requester = mRequesterProvider.providePreviewRequester(mContext);
			PreviewInputStream in = null;
			int maxDimension = 0;
			if (FileInfo.getTypeFromExtension(GenericUtils.getExtension(path.getName())) == FileInfo.IMAGE)
			{
				DisplayMetrics dm = ContextUtils.getDisplayMetrics(mContext.getApplicationContext());
				maxDimension = Math.max(dm.widthPixels, dm.heightPixels);
			}

			try
			{
				Log.d(PreviewService.TAG, "Starting request for: " + path);
				in = (PreviewInputStream) requester.getImagePreviewInputStream(teamDriveId, path.getName(),
						PathUtils.remoteRequestString(path.getParentPath()), normalizedPos, maxDimension, maxDimension,
						80, 0);

				// Retrieve and merge values
				int tempPageCount = in.getPageCount();
				if (tempPageCount != -1)
				{
					pageCount = tempPageCount;
				}

				byte[] tempData = GenericUtils.inputStreamToByteArray(in);
				if (tempData != null)
				{
					data = tempData;
				}

				// Request successful, break the loop
				Log.d(PreviewService.TAG, "Finished request for: " + path + ", page count: " + tempPageCount);
				break;
			}
			catch (XCBStatusException e)
			{
				long xcbCode = e.getStatusCode();
				if (xcbCode == PreviewRequester.XCB_STATUS_ERR_PREV_UNFINISHED)
				{
					// If there is going to be another iteration, then sleep a bit
					if (maxAttempts > 0)
					{
						try
						{
							Thread.sleep(waitBetweenAttempts);
						}
						catch (InterruptedException ex)
						{
							Log.w(PreviewService.TAG, "Thread.interrupted() has been called during Thread.sleep()", ex);
							Thread.currentThread().interrupt();
							break;
						}
					}
				}
				else if (xcbCode == PreviewRequester.XCB_STATUS_ERR_PAGE_NUMBER_TOO_LARGE)
				{
					Log.e(PreviewService.TAG,
							"Page not available: load("
									+ position
									+ "), xcb code: "
									+ xcbCode
									+ ". For example it can be that the page number is too large or that the file does not exist");
					canRetry = false;
				}
				else
				{
					Log.e(PreviewService.TAG, "XCBStatusException during load(" + position + "), xcb code: " + xcbCode,
							e);
				}
			}
			catch (HttpResponseException e)
			{
				String msg = "Unknown HttpResponseException during load(" + position + "): ignored.";
				if (canRetry && maxAttempts > 0)
				{
					msg += " Now retrying, attempts left: " + maxAttempts;
				}

				Log.e(PreviewService.TAG, msg, e);
			}
			catch (ConnectFailedException e)
			{
				String msg = "ConnectFailedException during load(" + position
						+ "), error opening the connection or connecting: ignored.";
				if (canRetry && maxAttempts > 0)
				{
					msg += " Now retrying, attempts left: " + maxAttempts;
				}

				Log.e(PreviewService.TAG, msg, e);
			}
			catch (LogonFailedException e)
			{
				Log.e(PreviewService.TAG, "LogonFailedException during load(" + position
						+ "), unauthorized or forbidden: aborting.", e);
				canRetry = false;
			}
			catch (ServerLicenseException e)
			{
				Log.e(PreviewService.TAG, "Unknown HttpResponseException during load(" + position + "): aborting. ", e);
				canRetry = false;
			}
			catch (IOException e)
			{
				if (e instanceof MalformedURLException)
				{
					throw new IllegalStateException(
							"Malformed Url in ThumbnailRequester.getImagePreviewInputStream(). This should never happen",
							e);
				}

				String msg = "IOException during load(" + position + "): ignored.";
				if (canRetry && maxAttempts > 0)
				{
					msg += " Now retrying, attempts left: " + maxAttempts;
				}

				Log.e(PreviewService.TAG, msg, e);
			}
			catch (NotificationException e)
			{
				throw new IllegalStateException("NotificationException in load(" + mPkey.getPageNumber() + ")", e);
			}
			catch (TeamDriveNotFoundException e)
			{
				Log.d(PreviewService.TAG, "Team drive not found", e);
			}
			catch (RedirectException e)
			{
				Log.d(PreviewService.TAG, "redirect", e);
			}
			finally
			{
				GenericUtils.closeQuietly(PreviewService.TAG, in);
			}
		}

		return new ServerResult(data, pageCount);
	}

	private Bitmap downsampleAndDecodeByteArray(byte[] data)
	{
		Bitmap image;
		// Debug.startMethodTracing("decode");
		BitmapFactory.Options opts = new BitmapFactory.Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(data, 0, data.length, opts);
		checkMemorySufficient(opts.outWidth, opts.outHeight);
		int sampleSz = GraphicsUtils.calculateInSampleSize(opts.outWidth, opts.outHeight,
				LoadPageTask.MAX_TEXTURE_SIZE_PX,
				LoadPageTask.MAX_TEXTURE_SIZE_PX, mMaxMemoryBytes);
		opts.inJustDecodeBounds = false;
		opts.inSampleSize = sampleSz;
		opts.inMutable = true;

		// Before KitKat only inSampleSize == 1 was supported
		Geometry geom = Geometry.valueOf(opts.outWidth, opts.outHeight);
		Bitmap recycled = mBitmapPool != null ? mBitmapPool.get(geom) : null;
		if (VERSION.SDK_INT >= VERSION_CODES.KITKAT)
		{
			opts.inBitmap = recycled;
		}
		else
		{
			opts.inBitmap = sampleSz == 1 ? recycled : null;
		}

		try
		{
			StringBuilder sb = new StringBuilder("alloc stats: inSampleSize: " + sampleSz + ", inBitmap: ");
			if (opts.inBitmap == null)
			{
				sb.append("null, geom: " + geom.toString());
			}
			else if (opts.inBitmap.isMutable())
			{
				sb.append("mutable");
			}
			else
			{
				sb.append("immutable");
			}
			Log.i(PreviewService.TAG, sb.toString());
			checkMemorySufficient(opts.outWidth, opts.outHeight);
			image = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
			Log.i(PreviewService.TAG, "alloc stats: bitmap recycled: " + (opts.inBitmap == image));
		}
		catch (IllegalArgumentException e)
		{
			Log.w(PreviewService.TAG, "alloc stats:itmap recycling failed", e);
			opts.inBitmap = null;
			checkMemorySufficient(opts.outWidth, opts.outHeight);
			image = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
		}
		// Debug.stopMethodTracing();
		return image;
	}

	private void checkMemorySufficient(long width, long height)
	{
		long bmpSize = width * height * 4l;
		long availMemory = getAvailableMemory();
		if (availMemory / 2 < bmpSize)
		{
			throw new LowMemoryException("available memory : " + availMemory + ". Bitmap size: " + bmpSize);
		}
	}

	private long getAvailableMemory()
	{
		Runtime runtime = Runtime.getRuntime();
		return runtime.freeMemory() + (runtime.maxMemory() - runtime.totalMemory());
	}

	private static class LowMemoryException extends RuntimeException
	{
		public LowMemoryException(String message)
		{
			super(message);
		}

		private static final long serialVersionUID = 6620562022312782412L;

	}

	@Immutable
	private static class ServerResult
	{
		final byte[] data;
		final int pageCount;

		public ServerResult(byte[] data, int pageCount)
		{
			this.data = data;
			this.pageCount = pageCount;
		}

		public boolean hasData()
		{
			return data != null;
		}

		@SuppressWarnings("unused")
		public boolean hasPageCount()
		{
			return pageCount != -1;
		}
	}
}