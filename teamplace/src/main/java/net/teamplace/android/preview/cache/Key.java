package net.teamplace.android.preview.cache;

import static net.teamplace.android.utils.GenericUtils.checkCondition;
import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import net.teamplace.android.browsing.path.Path;
import android.util.Base64;


/**
 * Represents a key for entries in {@link net.teamplace.android.preview.cache.DocumentsDiskCache DocumentsDiskCache} and
 * {@link net.teamplace.android.preview.cache.DocumentsMemoryCache DocumentsMemoryCache}. This class is not used directly
 * but rather through the subclasses {@link Page Key.Page} and {@link Count Key.Count}. To create instances of these
 * classes, use the factory methods {@link #makeCountKey(String)} and {@link #makePageKey(String, int)} and pass them to
 * appropriate methods in the caches. A {@link Page Key.Page} represents a "page" tuple (path, marker-date, page) while
 * a {@link Page Key.Count} represents a "count" tuple (path, marker-date).
 * 
 * @author Gil Vegliach
 */
public abstract class Key
{
	final private String mEncoded;

	/** Decoded key used for debugging */
	final private String mDecoded;

	final private Path mPath;
	final private long mDate;

	private Key(Path path, long date, String s)
	{
		if (s == null)
			throw new NullPointerException("String key must be non-null");
		mPath = path;
		mDate = date;
		mDecoded = s;
		mEncoded = Base64.encodeToString(s.getBytes(), Base64.URL_SAFE | Base64.NO_WRAP);
	}

	/**
	 * Creates a key for storing or retrieving one particular the total page count in a document marked at a specific
	 * date
	 */
	public static Count makeCountKey(Path document, long date)
	{
		checkNonNullArg(document);
		checkCondition(date >= 0);
		return new Count(document, date,
				document + "/" + date + "/count");
	}

	/**
	 * Creates a Key.Count from a Key.Page for the same document marked at the same date.
	 */
	public static Count makeCountKey(Key.Page pkey)
	{
		return makeCountKey(pkey.getPath(), pkey.getDate());
	}

	/**
	 * Creates a key for storing or retrieving one particular page in a document marked at a specific date.
	 * The same page for the same document is still considered to be different if the marker date is different.
	 * The index pageNumber is zero-based
	 */
	public static Page makePageKey(Path document, long date, int pageNumber)
	{
		checkNonNullArg(document);
		checkCondition(date >= 0);
		checkCondition(pageNumber >= 0);
		return new Page(document, date, pageNumber,
				document + "/" + date + "/" + Integer.toString(pageNumber));
	}

	/** Returns the Path contained in this Key */
	public Path getPath()
	{
		return mPath;
	}

	/** Returns the marker date contained in this Key */
	public long getDate()
	{
		return mDate;
	}

	@Override
	public boolean equals(Object o)
	{
		if (!(o instanceof Key))
			return false;

		return mEncoded.equals(((Key) o).mEncoded);
	}

	@Override
	public int hashCode()
	{
		return mEncoded.hashCode();
	}

	@Override
	public String toString()
	{
		return mEncoded;
	}

	/** @hide */
	public String toDecodedString()
	{
		return mDecoded;
	}

	/** Key for retrieving and storing document pages */
	public static class Page extends Key
	{
		final private int mPageNumber;

		private Page(Path path, long date, int pageNumber, String s)
		{
			super(path, date, s);
			mPageNumber = pageNumber;
		}

		public int getPageNumber()
		{
			return mPageNumber;
		}
	}

	/** Key for retrieving and storing page counts */
	public static class Count extends Key
	{
		private Count(Path path, long date, String s)
		{
			super(path, date, s);
		}
	}
}