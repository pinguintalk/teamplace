package net.teamplace.android.preview.ui;

import net.teamplace.android.util.annotations.Immutable;
import android.graphics.Bitmap;


/**
 * Abstract representation of width and height of some object
 * 
 * @author Gil Vegliach
 */
@Immutable
public class Geometry
{
	private final int mWidth;
	private final int mHeight;

	/** Factory method */
	public static Geometry valueOf(int width, int height)
	{
		// At the moment a new Geometry is created each time. In the future Geometry object could be pooled
		return new Geometry(width, height);
	}

	private Geometry(int width, int height)
	{
		mWidth = width;
		mHeight = height;
	}

	public int getHeight()
	{
		return mHeight;
	}

	public int getWidth()
	{
		return mWidth;
	}

	@Override
	public String toString()
	{
		return "[w: " + mWidth + ", h: " + mHeight + "]";
	}

	/**
	 * Returns an approximation of byte taken by a bitmap with this geometry. The Bitmap can actually use less memory if
	 * it has been reconfigured
	 */
	public int getByteCount(Bitmap.Config config)
	{
		switch (config)
		{
			case ALPHA_8:
				return mWidth * mHeight;
			case ARGB_4444:
			case RGB_565:
				return mWidth * mHeight * 2;
			default:
				return mWidth * mHeight * 4;
		}
	}

	/** Same as getByteCount(Bitmap.Config.ARGB_8888) */
	public int getDefaultByteCount()
	{
		return getByteCount(Bitmap.Config.ARGB_8888);
	}
}