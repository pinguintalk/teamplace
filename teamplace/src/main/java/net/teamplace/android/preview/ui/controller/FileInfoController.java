package net.teamplace.android.preview.ui.controller;

import java.util.List;

import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.preview.ui.PreviewConfiguration;


public interface FileInfoController extends PreviewController
{
	public void setDisplayingPath(Path displayingPath);

	public Path getDisplayingPath();

	public void setMasterDocument(FileInfo masterDocument);

	public FileInfo getMasterDocument();

	public void setFolderContent(List<FileInfo> folderContent);

	public List<FileInfo> getFolderContent();

	public void setDisplayingDate(long date);

	public long getDisplayingDate();
}
