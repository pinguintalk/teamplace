package net.teamplace.android.preview.comment;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.http.request.comments.Comment;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.teamdrive.GetUserImageListener;
import net.teamplace.android.teamdrive.UserImageLoader;
import net.teamplace.android.utils.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static net.teamplace.android.utils.GenericUtils.tag;

public class CommentsAdapter extends BaseAdapter
{
	private final String TAG = tag(CommentsAdapter.class);

	private final static int LOAD_NEXT_COMMENTS_THRESHOLD = 4;

	private List<Comment> comments;
	private final CommentOperationCallback callback;
	private final GetUserImageListener imgListener;
	private Map<String, User> users;
	private long since;

	private boolean areAllCommentsLoaded = false;
	private boolean updateInProgress = false;
	private final User myUser;
	private final Comment refreshComment;

	private final int dimension;
	private final LayoutInflater inflater;

	public CommentsAdapter(Context context, List<Comment> comments, TeamDrive teamdrive,
			CommentOperationCallback callback, GetUserImageListener imgListener)
	{
		this.comments = new ArrayList<Comment>();

		setComments(comments);

		this.callback = callback;
		this.imgListener = imgListener;

		initUserMap(teamdrive);

		dimension = context.getResources().getDrawable(R.drawable.delete_comment).getIntrinsicHeight();
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		myUser = users.get(new SessionManager(context).getLastSession().getAccount().name);

		refreshComment = new Comment();
		refreshComment.setCommentId("-1");
		refreshComment.setCreated(-1);
	}


	private UserImageLoader.CacheValidator mCacheValidator = new UserImageLoader.CacheValidator() {

		private List<User> mLoadedUsers = new ArrayList<>();

		@Override
		public boolean useCachedVersionIfAvailable(User user) {
			return mLoadedUsers.contains(user);
		}

		@Override
		public void onImageCached(User user) {
			mLoadedUsers.add(user);
		}
	};

	private void initUserMap(TeamDrive teamdrive)
	{
		users = new HashMap<String, User>();

		if (teamdrive != null)
		{
			for (User user : teamdrive.getUsers())
			{
				users.put(user.getEmail(), user);
			}
		}
	}

	public void setComments(List<Comment> comments)
	{
		updateSinceTimestamp();

		if (comments != null)
		{
			this.comments = comments;

			if (this.comments != null)
			{
				Collections.sort(this.comments);
			}
		}
	}

	public void addComments(List<Comment> comments)
	{
		updateSinceTimestamp();

		if (comments != null)
		{
			this.comments.addAll(comments);

			if (this.comments != null)
			{
				Collections.sort(this.comments);
			}
		}
	}

	public ArrayList<String> getCommentsIds()
	{
		ArrayList<String> commentIds = new ArrayList<String>();

		for (Comment c : comments)
		{
			if (!c.getCommentId().equals("-1"))
			{
				commentIds.add(c.getCommentId());
			}
		}

		return commentIds;
	}

	public List<Comment> getComments()
	{
		return comments;
	}

	public long getSince()
	{
		return since;
	}

	private void updateSinceTimestamp()
	{
		since = System.currentTimeMillis() / 1000;
	}

	private void checkPosition(int position)
	{
		Log.d(TAG, "Position: " + position);

		if (position == LOAD_NEXT_COMMENTS_THRESHOLD && !areAllCommentsLoaded && !updateInProgress)
		{
			addRefreshComment();

			updateInProgress = true;
			callback.loadNextComments();
		}
	}

	public void setAllCommentsAreLoaded(boolean loaded)
	{
		areAllCommentsLoaded = loaded;
	}

	public boolean areAllCommentsLoaded()
	{
		return areAllCommentsLoaded;
	}

	public boolean isUpdateInProgress()
	{
		return updateInProgress;
	}

	public void setUpdateInProgress(boolean updateInProgress)
	{
		this.updateInProgress = updateInProgress;
	}

	public void addRefreshComment()
	{
		if (!comments.contains(refreshComment))
		{
			List<Comment> refreshList = new ArrayList<Comment>();
			refreshList.add(refreshComment);

			addComments(refreshList);
			notifyDataSetChanged();
		}
	}

	public void removeRefreshComment()
	{
		if (comments.contains(refreshComment))
		{
			comments.remove(refreshComment);
		}
	}

	@Override
	public int getCount()
	{
		if (comments != null)
		{
			return comments.size();
		}

		return 0;
	}

	@Override
	public Object getItem(int position)
	{
		return comments.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent)
	{
		checkPosition(position);

		final ViewHolder holder;

		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.cmt_list_item, parent, false);

			holder = new ViewHolder();
			holder.switcher = (ViewSwitcher) convertView.findViewById(R.id.vs_comment_item);
			holder.userImage = (ImageView) convertView.findViewById(R.id.iv_user_image);
			holder.userName = (TextView) convertView.findViewById(R.id.tv_user_name);
			holder.date = (TextView) convertView.findViewById(R.id.tv_date);
			holder.comment = (TextView) convertView.findViewById(R.id.tv_comment);
			holder.deleteImage = (ImageView) convertView.findViewById(R.id.iv_delete_cmt);
			holder.deleteProgress = (ProgressBar) convertView.findViewById(R.id.cmt_pb_delete_comment);
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
			holder.switcher.setDisplayedChild(0);
			holder.userImage.setImageResource(R.drawable.tmd_default_icon);
			holder.deleteImage.setVisibility(View.VISIBLE);
			holder.deleteImage.setClickable(true);
			holder.deleteProgress.setVisibility(View.INVISIBLE);
		}

		final Comment comment = (Comment) getItem(position);

		if (comment.equals(refreshComment))
		{
			holder.switcher.setDisplayedChild(1);
		}
		else
		{
			User user = users.get(comment.getCreator());
			holder.userImage.setTag(user);

			if (user != null)
			{
				// imgListener.onLoadRequested(user, new ImageResultCallback(holder.userImage));
				UserImageLoader.downloadUserImage(holder.userImage, user, mCacheValidator);

				String displayName = user.getDisplayName();
				if (displayName == null || displayName.length() == 0)
				{
					displayName = user.getEmail();
				}

				holder.userName.setText(displayName);
			}
			else
			{
				holder.userName.setText(comment.getCreator());
			}

			holder.date.setText(getFormattedDate(comment.getCreated()));
			holder.comment.setText(comment.getCommentBase64Decoded());

			if (user.getEmail() == myUser.getEmail())
			{
				holder.deleteImage.setOnClickListener(new OnClickListener()
				{
					@Override
					public void onClick(View v)
					{
						callback.deleteComment(comment.getCommentId());
						holder.deleteImage.setVisibility(View.INVISIBLE);
						holder.deleteImage.setClickable(false);
						holder.deleteProgress.setVisibility(View.VISIBLE);
					}
				});
			}
			else
			{
				holder.deleteImage.setVisibility(View.INVISIBLE);
				holder.deleteImage.setClickable(false);
			}

			RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dimension, dimension);
			holder.deleteProgress.setLayoutParams(params);

			if (callback.willCommentBeDeleted(comment.getCommentId()))
			{
				holder.deleteImage.setVisibility(View.INVISIBLE);
				holder.deleteImage.setClickable(false);
				holder.deleteProgress.setVisibility(View.VISIBLE);
			}
		}

		return convertView;
	}

	private String getFormattedDate(long timestamp)
	{
		// TODO: the following code does not do proper localization. Use GenericUtils.formatDate() instead!
		Date date = new Date(timestamp * 1000L);
		SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy HH:mm");

		return format.format(date);
	}

	static class ViewHolder
	{
		ViewSwitcher switcher;
		ImageView userImage;
		TextView userName;
		TextView date;
		TextView comment;
		ImageView deleteImage;
		ProgressBar deleteProgress;
	}
}
