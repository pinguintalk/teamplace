package net.teamplace.android.preview.ui;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.database.SQLException;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.cortado.android.R;

import net.teamplace.android.action.Action;
import net.teamplace.android.action.ActionFactory;
import net.teamplace.android.action.BaseActionFactory;
import net.teamplace.android.action.MailAction;
import net.teamplace.android.action.PreviewDeleteAction;
import net.teamplace.android.action.PreviewExportToPdfAction;
import net.teamplace.android.action.PreviewExtractVersionAction;
import net.teamplace.android.action.PreviewZipAction;
import net.teamplace.android.action.PrintAction;
import net.teamplace.android.action.RenameAction;
import net.teamplace.android.action.ShareAction;
import net.teamplace.android.action.SmarfileAction;
import net.teamplace.android.browsing.BrowsingService;
import net.teamplace.android.browsing.FileInfo;
import net.teamplace.android.browsing.dialog.EditTextDialogFragment;
import net.teamplace.android.browsing.dialog.SmartfileDialogFragment;
import net.teamplace.android.browsing.path.Path;
import net.teamplace.android.browsing.path.PathUtils;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.request.comments.ChangedCommentResponse;
import net.teamplace.android.http.request.comments.Comment;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.preview.BackPressManager;
import net.teamplace.android.preview.PreviewActivity;
import net.teamplace.android.preview.PreviewService;
import net.teamplace.android.preview.PreviewService.Contract;
import net.teamplace.android.preview.PreviewService.PreviewServiceBinder;
import net.teamplace.android.preview.comment.AddCommentIntentService;
import net.teamplace.android.preview.comment.CommentIntentHelper;
import net.teamplace.android.preview.comment.CommentOperationCallback;
import net.teamplace.android.preview.comment.CommentOperationResponse;
import net.teamplace.android.preview.comment.CommentsAdapter;
import net.teamplace.android.preview.comment.DeleteCommentIntentService;
import net.teamplace.android.preview.comment.LoadCommentsIntentService;
import net.teamplace.android.preview.dialog.GoToPageDialogFragment;
import net.teamplace.android.preview.widget.DocumentView;
import net.teamplace.android.preview.widget.PageNumberView;
import net.teamplace.android.preview.widget.PreviewFooterView;
import net.teamplace.android.preview.widget.PreviewLazyAdapter;
import net.teamplace.android.preview.widget.VersionItem;
import net.teamplace.android.teamdrive.GetUserImageListener;
import net.teamplace.android.teamdrive.ImageResultCallback;
import net.teamplace.android.teamdrive.TeamDriveService;
import net.teamplace.android.teamdrive.TeamDriveService.TeamDriveServiceResponse;
import net.teamplace.android.teamdrive.TeamDriveService.UserImageItem;
import net.teamplace.android.utils.GenericUtils;
import net.teamplace.android.utils.Log;
import net.teamplace.android.utils.RightsHelper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.greenrobot.event.EventBus;

import static net.teamplace.android.utils.GenericUtils.checkNonNullArg;
import static net.teamplace.android.utils.GenericUtils.emptyIfNull;
import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * @deprecated This has been extensively refactored. Replaced by {@link Tpm3PreviewFragment} Container for a
 *             DocumentView and some decorations: a PageNumberView and the ActionBar. This class has the logic for
 *             showing, hiding and synchronizing the decorations. The PageNumberView is owned by this class and thus
 *             hidden and
 *             shown directly. Requests for hiding and showing the ActionBar are instead forwarded to PreviewActivity
 *             through a
 *             listener: in this way, the Activity, which owns the ActionBar, can have the last decision on it.
 * @author Gil Vegliach
 */

@Deprecated()
public class OldTpm3PreviewFragment extends Fragment implements
		DocumentView.OnPageChangedListener,
		PreviewLazyAdapter.OnPageCountKnownListener,
		PreviewLazyAdapter.OnHasVisibleLoadingPagesStatusListener,
		PreviewFooterView.OnVersionClickListener,
		DrawerLayout.DrawerListener,
		CommentOperationCallback,
		GetUserImageListener,
		BackPressManager
{
	public static final String TAG = tag(OldTpm3PreviewFragment.class);
	private static final String PKG = pkg(OldTpm3PreviewFragment.class);

	private static final String KEY_MASTER_DOC_INFO = PKG + ".masterDocInfo";
	private static final String KEY_DISPLAYING_PATH = PKG + ".displayingPath";
	private static final String KEY_DISPLAYING_DATE = PKG + ".displayingDate";
	private static final String KEY_IS_ACTION_BAR_VISIBLE = PKG + ".isActionBarVisible";
	private static final String KEY_IS_PAGE_NUMBER_VISIBLE = PKG + ".isPageNumberVisible";
	private static final String KEY_PAGE_COUNT = PKG + ".pageCount";
	private static final String KEY_IS_PAGE_KNOWN = PKG + ".isPageCountKnown";
	private static final String KEY_CURR_REQUESTS = PKG + ".currRequests";
	private static final String KEY_IS_FAVORITE_REQUEST_IN_PROGRESS = PKG + ".isFavoriteRequestInProgress";

	private static final int PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT = 1500;
	private static final int PAGE_NUMBER_FADE_OUT_DELAY_LONG = 2500;

	/**
	 * Path of the document that we are displaying on the screen. It can be the path of {@link #mMasterDocument} or
	 * some of its versions
	 */
	// Must be kept in sync with mDisplayingDate
	private Path mDisplayingPath;

	/**
	 * Marking date of the document that we are displaying on the screen. It can be the last modified date of
	 * {@link #mMasterDocument} or the last edit date of the version referred by {@link #mDisplayingPath}
	 */
	// Must be kept in sync with mDisplayingPath
	private long mDisplayingDate;

	/** Master document information without versions. This document can change if we perform a rename */
	private FileInfo mMasterDocument;

	/** True iff this fragment's activity is bound to PreviewService */
	private boolean mBound = false;

	/** This object will route connection callbacks to {@link #mAdapter}, if available */
	private final PreviewServiceConnectionWrapper mServiceConnection = new PreviewServiceConnectionWrapper();

	/**
	 * Lazily binds pages to DocumentView and visually handles PreviewServiceConnectionListener callbacks, for
	 * instance redrawing the document when the service connects.
	 */
	private PreviewLazyAdapter mAdapter;

	private DocumentView mDocumentView;
	private View mPageNumberViewContainer;
	private View mCommentsViewContainer;
	private PageNumberView mPageNumberView;
	private PreviewFooterView mFooterView;
	private ProgressBar mProgressBar;
	private DrawerLayout mDrawerLayout;
	private View mDrawerView;

	private ActionBarProxy mActionBarProxy;

	/** Handler used for animations */
	private final Handler mHandler = new Handler();

	// TODO: @giveg this flag is probably obsolete, all the logic is in the actionbar proxy
	private boolean mIsActionBarVisible = true;
	private boolean mIsPageNumberVisible = true;

	/** Ids of requests in progress */
	private final ArrayList<Integer> mCurrRequests = new ArrayList<Integer>();

	/** Only one favorite request (add/remove) at a time. During this time the option menu button is disabled */
	private boolean mIsFavoriteRequestInProgress = false;

	/** Creates file actions. Don't hold on the actions too long, as you could leak */
	private final ActionFactory mActionFactory = new PreviewTp3ActionFactory();

	private final BroadcastReceiver mResponseReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	public static OldTpm3PreviewFragment newInstance(FileInfo info)
	{
		OldTpm3PreviewFragment fragment = new OldTpm3PreviewFragment();
		Bundle args = new Bundle(1);
		args.putParcelable(KEY_MASTER_DOC_INFO, info);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onAttach(Activity activity)
	{
		super.onAttach(activity);
		mActionBarProxy = new ActionBarProxy(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);

		if (savedInstanceState == null)
		{
			Bundle args = getArguments();
			mMasterDocument = args.getParcelable(KEY_MASTER_DOC_INFO);
			mDisplayingPath = mMasterDocument.getPath();
			mDisplayingDate = mMasterDocument.getLastModifiedDate();
		}
		else
		{
			mMasterDocument = savedInstanceState.getParcelable(KEY_MASTER_DOC_INFO);
			mDisplayingPath = savedInstanceState.getParcelable(KEY_DISPLAYING_PATH);
			mDisplayingDate = savedInstanceState.getLong(KEY_DISPLAYING_DATE);
		}

		onCreateComments();
	}

	// TouchListener2 uses onSingleTapConfirmed() instead of performClick()
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		View root = inflater.inflate(R.layout.prv_tpm3_preview_fragment, container, false);
		mDocumentView = (DocumentView) root.findViewById(R.id.document_view);

		rebuildAdapter();
		mDocumentView.setAdapter(mAdapter);

		mDocumentView.setOnTouchListener(new TouchListener2());
		mDocumentView.setOnPageChangedListener(this);

		mPageNumberViewContainer = root.findViewById(R.id.page_number_view_container);
		mPageNumberView = (PageNumberView) mPageNumberViewContainer.findViewById(R.id.page_number_view);
		mPageNumberViewContainer.setOnClickListener(mPageNumberViewOnClickListener);

		mCommentsViewContainer = root.findViewById(R.id.comments_view_container);

		mFooterView = (PreviewFooterView) root.findViewById(R.id.footer_view);
		mFooterView.setOnVersionClickListener(this);

		mProgressBar = (ProgressBar) root.findViewById(R.id.progress_bar);
		syncFooterView();

		restoreState(savedInstanceState);

		onCreateViewComments(savedInstanceState, root);
		return root;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);

		// TODO: @giveg this is probably the wrong place, move it
		if (savedInstanceState == null)
		{
			versionsRequest();
		}
	}

	@Override
	public void onStart()
	{
		super.onStart();
		Activity act = getActivity();
		Intent intent = new Intent(act, PreviewService.class);
		act.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onStop()
	{
		super.onStop();
		if (mBound)
		{
			getActivity().unbindService(mServiceConnection);
			mBound = false;

			// Normal unbinding case, notify components
			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceDisconnected();
			}
		}
	}

	@Override
	public void onResume()
	{
		super.onResume();

		// Restore PageNumber animation, if need be
		if (!mIsActionBarVisible && mIsPageNumberVisible)
		{
			mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
		}

		Activity act = getActivity();
		IntentFilter filter = new IntentFilter(BrowsingService.Contract.ACTION_RESPONSE_RENAME);
		filter.addAction(PreviewService.Contract.ACTION_RESPONSE_VERSIONS);
		filter.addAction(PreviewService.Contract.ACTION_NOTIFICATION_PAGE_LOADED);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_DELETE);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_ADD_FAVORITE);
		filter.addAction(BrowsingService.Contract.ACTION_RESPONSE_DELETE_FAVORITE);
		LocalBroadcastManager.getInstance(act).registerReceiver(mResponseReceiver, filter);

		onResumeComments();
	}

	@Override
	public void onPause()
	{
		super.onPause();

		// Remove pending animations
		mHandler.removeCallbacks(mPageNumberHider);

		Activity act = getActivity();
		LocalBroadcastManager.getInstance(act).unregisterReceiver(mResponseReceiver);

		onPauseComments();
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		Log.d(TAG, "onSaveInstanceState()");
		super.onSaveInstanceState(outState);
		outState.putBoolean(KEY_IS_ACTION_BAR_VISIBLE, mIsActionBarVisible);
		outState.putBoolean(KEY_IS_PAGE_NUMBER_VISIBLE, mIsPageNumberVisible);
		outState.putInt(KEY_PAGE_COUNT, mAdapter.getCount());
		outState.putBoolean(KEY_IS_PAGE_KNOWN, mAdapter.isPageCountKnown());
		outState.putParcelable(KEY_MASTER_DOC_INFO, mMasterDocument);
		outState.putParcelable(KEY_DISPLAYING_PATH, mDisplayingPath);
		outState.putLong(KEY_DISPLAYING_DATE, mDisplayingDate);
		outState.putIntegerArrayList(KEY_CURR_REQUESTS, mCurrRequests);
		outState.putBoolean(KEY_IS_FAVORITE_REQUEST_IN_PROGRESS, mIsFavoriteRequestInProgress);

		onSaveInstanceStateComments(outState);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (resultCode != Activity.RESULT_OK)
		{
			return;
		}

		if (requestCode == GoToPageDialogFragment.REQUEST_CODE)
		{
			int page = data.getIntExtra(GoToPageDialogFragment.KEY_PAGE, 1) - 1;
			mDocumentView.scrollToPage(page);
			mActionBarProxy.hide();
			mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_LONG);
			mFooterView.hide();
		}
		else if (requestCode == SmartfileDialogFragment.REQUEST_CODE)
		{
			Path target = data.getParcelableExtra(SmartfileDialogFragment.KEY_TARGET);
			Path[] sources = new Path[] {mMasterDocument.getPath()};

			Intent newData;
			PreviewActivity act = (PreviewActivity) getActivity();
			int action = data.getIntExtra(SmartfileDialogFragment.KEY_ACTION, SmartfileDialogFragment.NO_ACTION);
			if (action == SmartfileDialogFragment.ACTION_COPY)
			{
				newData = BrowsingService.Contract.buildCopyRequestIntent(act, sources, target);
			}
			else if (action == SmartfileDialogFragment.ACTION_MOVE)
			{
				newData = BrowsingService.Contract.buildMoveRequestIntent(act, sources, target);
			}
			else
			{
				throw new IllegalArgumentException("Action non recognized");
			}

			GenericUtils.finishWithData(act, newData);
		}
		else if (requestCode == R.id.request_code_rename)
		{
			CharSequence cs = data.getCharSequenceExtra(EditTextDialogFragment.KEY_TEXT);
			String newName = cs.toString();

			// Then execute background request
			Activity act = getActivity();
			Intent i = BrowsingService.Contract.buildRenameRequestIntent(act, mDisplayingPath, newName);
			mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
			act.startService(i);

			// TODO: we can avoid showing a toast but we need to remember the old file name as a fall back in
			// case of error of the network request
			Toast.makeText(act, R.string.prv_rename_in_progress, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		int menuRes = isMasterDocumentShowing()
				? R.menu.prv_context_menu
				: R.menu.prv_versions_menu;
		inflater.inflate(menuRes, menu);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// No dynamic processing to do if the versions menu is showing
		if (!isMasterDocumentShowing())
		{
			return;
		}

		List<FileInfo> fileInfo = new ArrayList<FileInfo>();
		fileInfo.add(mMasterDocument);

		// TODO: @jobol this method potentially perform a database operation on the UI thread and should be avoided
		Session session = new SessionManager(getActivity().getApplicationContext()).getLastSession();
		RightsHelper rightsHelper = new RightsHelper(session);
		MenuItem item = menu.findItem(R.id.action_mail);
		item.setVisible(rightsHelper.canMailAndShare(fileInfo));
		menu.findItem(R.id.action_zip).setVisible(rightsHelper.canExportToZip(fileInfo));
		menu.findItem(R.id.action_export_to_pdf).setVisible(rightsHelper.canExportToPdf(fileInfo));
		menu.findItem(R.id.action_rename).setVisible(rightsHelper.canRename(fileInfo));
		menu.findItem(R.id.action_delete).setVisible(rightsHelper.canDelete(fileInfo));

		boolean isFav = mMasterDocument.isFavorite();
		boolean isReqInProgress = mIsFavoriteRequestInProgress;
		MenuItem favorite = menu.findItem(R.id.action_favorite);
		MenuItem unfavorite = menu.findItem(R.id.action_unfavorite);
		favorite.setVisible(!isFav);
		favorite.setEnabled(!isFav && !isReqInProgress);
		unfavorite.setVisible(isFav);
		unfavorite.setEnabled(isFav && !isReqInProgress);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		int id = item.getItemId();
		mActionFactory.build(id).execute();
		return true;
	}

	@Override
	public void onPageChanged(int oldPage, int newPage)
	{
		mPageNumberView.setCurrentPage(newPage + 1);
	}

	@Override
	public void onPageCountKnown(int count)
	{
		mPageNumberView.setPageCount(count);
	}

	@Override
	public void onVersionClicked(VersionItem version)
	{
		// If we are preview this versions, nothing to do
		Path newPath = PathUtils.buildRenamedPath(mDisplayingPath, version.getFullName());
		if (newPath.equals(mDisplayingPath))
		{
			return;
		}

		mDisplayingPath = newPath;
		mDisplayingDate = newPath.equals(mMasterDocument)
				? mMasterDocument.getLastModifiedDate()
				: version.getLastEditDate();

		rebuildAdapter();
		mDocumentView.setAdapter(mAdapter);

		getActivity().invalidateOptionsMenu();
	}

	/** Returns true if back press was handles, otherwise false */
	@Override
	public boolean onBackPressed()
	{
		if (mDrawerLayout.isDrawerOpen(mDrawerView))
		{
			mDrawerLayout.closeDrawer(mDrawerView);
			return true;
		}

		return false;
	}

	@Override
	public void onDrawerClosed(View drawerView)
	{
		mPageNumberShower.run();
		showCommentsView();
		mFooterView.showSelectedOrCollapsed();

		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mDrawerView);
	}

	@Override
	public void onDrawerOpened(View drawerView)
	{
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mDrawerView);
	}

	@Override
	public void onDrawerSlide(View drawerView, float slideOffset)
	{
	}

	@Override
	public void onDrawerStateChanged(int arg0)
	{
	}

	@Override
	public void onHasVisibleLoadingPagesStatusChanged(boolean newHasVisibleLoadingPages)
	{
		mProgressBar.setVisibility(newHasVisibleLoadingPages ? View.VISIBLE : View.GONE);
	}

	private void onResponseReceived(Intent intent)
	{
		Log.d(TAG, "onResponseReceived() [1], intent=" + intent);

		// ACTION_NOTIFICATION_PAGE_LOADED is spawned by the adapter and does not store ids
		String action = intent.getAction();
		if (Contract.ACTION_NOTIFICATION_PAGE_LOADED.equals(action))
		{
			onResponsePageLoaded(intent);
			return;
		}

		int id = BrowsingService.Contract.extractRequestId(intent);
		Log.d(TAG, "onResponseReceived() [2], action=" + action + ", id=" + id + ", mCurrRequests=" + mCurrRequests);

		// NO_ID is the same both for BrowsingService and PreviewService
		if (id == BrowsingService.Contract.NO_ID || !mCurrRequests.remove((Integer) id))
		{
			return;
		}

		if (BrowsingService.Contract.ACTION_RESPONSE_RENAME.equals(action))
		{
			onResponseRename(intent);
		}
		else if (PreviewService.Contract.ACTION_RESPONSE_VERSIONS.equals(action))
		{
			onResponseVersions(intent);
		}
		else if (BrowsingService.Contract.ACTION_RESPONSE_DELETE.equals(action))
		{
			// We have completed deleting a version
			onResponseDeleteVersion();
		}
		else if (BrowsingService.Contract.ACTION_RESPONSE_ADD_FAVORITE.equals(action))
		{
			onResponseFavoriteStatusModified(true);
		}
		else if (BrowsingService.Contract.ACTION_RESPONSE_DELETE_FAVORITE.equals(action))
		{
			onResponseFavoriteStatusModified(false);
		}
	}

	private void onResponsePageLoaded(Intent intent)
	{
		if (mAdapter == null)
		{
			return;
		}

		int pageCount = intent.getIntExtra(Contract.NOTIFICATION_TOTAL_PAGE_NUMBER, -1);
		int pagePos = intent.getIntExtra(Contract.NOTIFICATION_POSITION_LOADED_PAGE, -1);
		mAdapter.onPreviewLoadFinished(pagePos, pageCount);
	}

	private void onResponseRename(Intent intent)
	{
		String newName = BrowsingService.Contract.extractNewName(intent);
		if (newName == null)
		{
			throw new IllegalStateException(
					"Please build requests with methods in the Contract class and do not modify them. Intent: "
							+ intent);
		}

		// Sync information on screen
		Path newPath = PathUtils.buildRenamedPath(mMasterDocument.getPath(), newName);
		mMasterDocument = new FileInfo(newPath, mMasterDocument.getLastModifiedDate(),
				mMasterDocument.getSize(),
				mMasterDocument.isFavorite());

		mDisplayingPath = newPath;
		mDisplayingDate = mMasterDocument.getLastModifiedDate();
		syncFooterView();
		clearVersions();

		// Starts a new versions request
		versionsRequest();

		Toast.makeText(getActivity(), R.string.prv_rename_done, Toast.LENGTH_SHORT).show();
	}

	private void onResponseVersions(Intent intent)
	{
		ArrayList<VersionItem> versions = intent
				.getParcelableArrayListExtra(PreviewService.Contract.RESPONSE_VERSIONS_LIST);
		mFooterView.setVersionItems(versions, null);
	}

	private void onResponseDeleteVersion()
	{
		// Refresh
		versionsRequest();
		Toast.makeText(getActivity(), R.string.prv_delete_version_done, Toast.LENGTH_SHORT).show();
	}

	private void onResponseFavoriteStatusModified(boolean isFavorite)
	{
		mIsFavoriteRequestInProgress = false;
		mMasterDocument.setFavorite(isFavorite);
		getActivity().invalidateOptionsMenu();
	}

	private void onSingleTapConfirmed(MotionEvent e)
	{
		if (!mDrawerLayout.isDrawerVisible(mDrawerView))
		{
			// If the footer is visible, with the first tap we collapse it
			if (!mFooterView.isSelectedVersionOrCollapsed() && mFooterView.isShowing())
			{
				mHandler.removeCallbacks(mPageNumberHider);
				mFooterView.showSelectedOrCollapsed();
				return;
			}

			if (mActionBarProxy.isShowing())
			{
				mActionBarProxy.hide();
				mPageNumberHider.run();
				hideCommentsView();
				mFooterView.hide();
			}
			else
			{
				mHandler.removeCallbacks(mPageNumberHider);
				mActionBarProxy.show();
				mPageNumberShower.run();
				showCommentsView();
				mFooterView.showSelectedOrCollapsed();
			}
		}
	}

	private void onScrollStarted()
	{
		if (!mActionBarProxy.isShowing())
		{
			mHandler.removeCallbacks(mPageNumberHider);
			mPageNumberShower.run();
		}
	}

	private void onScrollFinished()
	{
		if (!mActionBarProxy.isShowing())
		{
			mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
		}
	}

	/**
	 * Shows whether the master document or one of its other versions is on screen. The options menu depends on this
	 * value
	 */
	private boolean isMasterDocumentShowing()
	{
		return mMasterDocument.getPath().equals(mDisplayingPath);
	}

	private void rebuildAdapter()
	{
		if (mBound && mAdapter != null)
		{
			mAdapter.onPreviewServiceDisconnected();
		}

		mAdapter = new PreviewLazyAdapter(getActivity(), mDocumentView, mDisplayingPath, mDisplayingDate);
		mAdapter.setOnPageCountKnownListener(this);
		mAdapter.setOnHasVisibleLoadingPagesStatusListener(this);

		if (mBound)
		{
			mAdapter.onPreviewServiceConnected(mServiceConnection.previewService);
		}
	}

	private void clearVersions()
	{
		mFooterView.setVersionItems(null,null);
		rebuildAdapter();
		mDocumentView.setAdapter(mAdapter);
	}

	private void versionsRequest()
	{
		Activity act = getActivity();
		Intent i = PreviewService.Contract.buildRequestVersionsIntent(act, mMasterDocument.getPath());
		mCurrRequests.add(PreviewService.Contract.extractRequestId(i));
		act.startService(i);
	}

	private void addFavoriteRequest()
	{
		Activity act = getActivity();
		Intent i = BrowsingService.Contract.buildAddFavoriteRequestIntent(act, mMasterDocument.getPath());
		mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
		act.startService(i);
	}

	private void delFavoriteRequest()
	{
		Activity act = getActivity();
		Intent i = BrowsingService.Contract.buildDeleteFavoriteRequestIntent(act, mMasterDocument.getPath());
		mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
		act.startService(i);
	}

	private void syncFooterView()
	{
		mFooterView.setDocumentName(mMasterDocument.getName());
		mFooterView.setDocumentLastModifiedDate(mMasterDocument.getLastModifiedDate());
		mFooterView.setDocumentSize(mMasterDocument.getSize());
		mFooterView.setDocumentIcon(mMasterDocument.getIconResId());
	}

	private void restoreState(Bundle savedInstanceState)
	{
		if (savedInstanceState != null)
		{
			int count = savedInstanceState.getInt(KEY_PAGE_COUNT);
			mAdapter.setCount(count);

			boolean isPageCountKnown = savedInstanceState.getBoolean(KEY_IS_PAGE_KNOWN);
			mAdapter.setPageCountKnown(isPageCountKnown);

			ArrayList<Integer> requestIds = savedInstanceState.getIntegerArrayList(KEY_CURR_REQUESTS);
			if (requestIds != null)
			{
				mCurrRequests.addAll(requestIds);
			}

			mIsFavoriteRequestInProgress = savedInstanceState.getBoolean(KEY_IS_FAVORITE_REQUEST_IN_PROGRESS);
		}

		restoreDecorationsState(savedInstanceState);
	}

	private void restoreDecorationsState(Bundle savedInstanceState)
	{
		// First, initialize member variables
		if (savedInstanceState != null)
		{
			mIsActionBarVisible = savedInstanceState.getBoolean(KEY_IS_ACTION_BAR_VISIBLE);
			mIsPageNumberVisible = savedInstanceState.getBoolean(KEY_IS_PAGE_NUMBER_VISIBLE);
		}

		// Synchronize ActionBar visibility with its flag
		if (mIsActionBarVisible && !mActionBarProxy.isShowing())
		{
			mActionBarProxy.show();
		}
		else if (!mIsActionBarVisible && mActionBarProxy.isShowing())
		{
			mActionBarProxy.hide();
		}

		// Synchronize PageNumber visibility with its flag
		if (mIsPageNumberVisible)
		{
			if (mPageNumberViewContainer.getVisibility() != View.VISIBLE)
			{
				mPageNumberViewContainer.setVisibility(View.VISIBLE);
			}

			// If the PageNumber is visible but not the action bar, then trigger a delayed fade-out
			// animation
			if (!mIsActionBarVisible)
			{
				mHandler.removeCallbacks(mPageNumberHider);
				mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
			}
		}
		else if (mPageNumberViewContainer.getVisibility() == View.VISIBLE)
		{
			mPageNumberViewContainer.setVisibility(View.GONE);
		}
	}

	private void showCommentsView()
	{
		if (mMasterDocument.getPath() instanceof Path.TeamDrive)
		{
			mCommentsViewContainer.setVisibility(View.VISIBLE);
			ObjectAnimator anim = ObjectAnimator.ofFloat(mCommentsViewContainer, View.ALPHA, 1.f);
			anim.start();
		}
	}

	private void hideCommentsView()
	{
		if (mMasterDocument.getPath() instanceof Path.TeamDrive)
		{
			ObjectAnimator anim = ObjectAnimator.ofFloat(mCommentsViewContainer, View.ALPHA, 0.f);
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationEnd(Animator animation)
				{
					mCommentsViewContainer.setVisibility(View.GONE);
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
					mCommentsViewContainer.setVisibility(View.GONE);
				}
			});
			anim.start();
		}
	}

	/** Runnable that hides PageNumberView with an animation */
	private final Runnable mPageNumberHider = new Runnable()
	{
		boolean mIsAnimating = false;

		@Override
		public void run()
		{
			// If page number is not displayed or we are animating
			if (mPageNumberViewContainer.getVisibility() == View.GONE || mIsAnimating)
			{
				return;
			}

			mIsPageNumberVisible = false;

			ObjectAnimator anim = ObjectAnimator.ofFloat(mPageNumberViewContainer, View.ALPHA, 0.f);
			anim.addListener(new AnimatorListener()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					mIsAnimating = true;
				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					mPageNumberViewContainer.setVisibility(View.GONE);
					mIsAnimating = false;
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
				}
			});
			anim.start();
		};
	};

	/** Runnable that shows PageNumberView with an animation */
	private final Runnable mPageNumberShower = new Runnable()
	{
		boolean mIsAnimating = false;

		@Override
		public void run()
		{
			// If the page number is shown or already animating, nothing to do
			if (mPageNumberViewContainer.getVisibility() == View.VISIBLE || mIsAnimating)
			{
				return;
			}

			mIsPageNumberVisible = true;

			// Otherwise display it with a fade-in animation
			mPageNumberViewContainer.setVisibility(View.VISIBLE);

			ObjectAnimator anim = ObjectAnimator.ofFloat(mPageNumberViewContainer, View.ALPHA, 1.f);
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					mIsAnimating = true;
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					mIsAnimating = false;
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
					mIsAnimating = false;
				}
			});
			anim.start();
		};
	};

	private final View.OnClickListener mPageNumberViewOnClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			mHandler.removeCallbacks(mPageNumberHider);
			mIsActionBarVisible = true;
			mActionBarProxy.show();

			int count = mAdapter.getCount();
			DialogFragment dialog = GoToPageDialogFragment.newInstance(count);
			dialog.setTargetFragment(OldTpm3PreviewFragment.this, GoToPageDialogFragment.REQUEST_CODE);
			dialog.show(getActivity().getFragmentManager(), GoToPageDialogFragment.TAG);
		}
	};

	// TouchListener2 uses onSingleTapConfirmed() instead of performClick()
	@SuppressLint("ClickableViewAccessibility")
	/* non-static */class TouchListener2 implements View.OnTouchListener
	{
		boolean mIsScrolling = false;

		private final GestureDetector mDoubleTapDetector = new GestureDetector(getActivity(),
				new SimpleOnGestureListener()
				{
					@Override
					public boolean onSingleTapConfirmed(MotionEvent e)
					{
						OldTpm3PreviewFragment.this.onSingleTapConfirmed(e);
						return true;
					};

					@Override
					public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
					{
						if (!mIsScrolling)
						{
							mIsScrolling = true;
							onScrollStarted();
						}
						return false;
					};
				});

		@Override
		public boolean onTouch(View v, MotionEvent event)
		{
			if (mIsScrolling)
			{
				int action = event.getActionMasked();
				if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP)
				{
					mIsScrolling = false;
					onScrollFinished();
				}

			}
			return mDoubleTapDetector.onTouchEvent(event);
		}
	}

	/** Handles touches events, showing and hiding decorations */
	class TouchListener implements View.OnTouchListener
	{
		// This is a diagram of the state machine of the touch events: we want to invert the
		// visibility of the action bar and the page number view on a 'click' and set a timer for
		// hiding the page number view on a 'scroll end'. Also we want to ignore the second tap of a
		// double tap (not shown). When an action up or cancel occurs, the field mIsScrolling helps
		// distinguish between the 'click' and the 'scroll' state
		//
		// .................o start ..........
		// .................|.................
		// ........... down |.................
		// .................|.................
		// .................*.................
		// ................/.\................
		// ........... up /...\ move .........
		// ............../.... \..............
		// ...... click * .. -- * scroll .....
		// ................ |___| ............
		// ........... move ....|.. up .......
		// .....................|.............
		// .....................* scroll end .

		private static final int INVALID_POINTER_ID = -1;
		private float mActivePointerX;
		private float mActivePointerY;
		private int mActivePointerId;

		/** Whether we are panning or scaling the DocumentView */
		private boolean mIsScrolling;

		/** Threshold to distinguish a scroll from a tap */
		private final int mTouchSlop;

		/** Returns false on all events except a double tap, where it also resets the scroll flag */
		private final GestureDetector mDoubleTapDetector = new GestureDetector(getActivity(),
				new SimpleOnGestureListener()
				{
					/** Returns true on the last event of a double tap, otherwise false */
					@Override
					public boolean onDoubleTapEvent(MotionEvent e)
					{
						int action = e.getActionMasked();
						if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL)
						{
							return true;
						}

						return false;
					}
				});

		public TouchListener()
		{
			mActivePointerId = INVALID_POINTER_ID;
			mIsScrolling = false;
			mTouchSlop = ViewConfiguration.get(getActivity()).getScaledTouchSlop();
		}

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event)
		{
			// Touches cancel fade-out animation
			mHandler.removeCallbacks(mPageNumberHider);

			// Handles a double tap: we ignore the second tap, cause we treat it as a single tap
			// as far as the action bar and PageNumberView are concerned. But if scrolling was on,
			// then the action bar is already slipping out, so fade out also PageNumberView
			if (mDoubleTapDetector.onTouchEvent(event))
			{
				if (mIsScrolling)
				{
					mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
					mIsScrolling = false;
				}

				return false;
			}

			switch (event.getActionMasked())
			{
				case MotionEvent.ACTION_DOWN:
				{
					mActivePointerId = event.getPointerId(0);
					mActivePointerX = event.getX();
					mActivePointerY = event.getY();
					mIsScrolling = false;

					break;
				}

				case MotionEvent.ACTION_MOVE:
				{
					int index = event.findPointerIndex(mActivePointerId);
					float x = event.getX(index);
					float y = event.getY(index);

					double dst = Math.sqrt((mActivePointerX - x) * (mActivePointerX - x) + (mActivePointerY - y)
							* (mActivePointerY - y));

					// When scrolling starts, hide the action bar and show the PageNumberView
					if (dst > mTouchSlop && !mIsScrolling)
					{
						mActionBarProxy.hide();
						mPageNumberShower.run();
						mIsScrolling = true;
					}

					break;
				}

				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					// The finger has been lifted. If it had been a tap, then invert the visibility
					// of the action bar and the page number view
					if (!mIsScrolling)
					{
						if (mActionBarProxy.isShowing())
						{
							mActionBarProxy.hide();
							mPageNumberHider.run();
						}
						else
						{
							mActionBarProxy.show();
							mPageNumberShower.run();
						}

					}
					// Otherwise, it was a scroll: the action bar is hidden and we set a timer
					// for hiding the page number view
					else
					{
						mHandler.postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
					}

					mActivePointerId = INVALID_POINTER_ID;
					mIsScrolling = false;

					break;
				}

				case MotionEvent.ACTION_POINTER_UP:
				{
					// A secondary pointer has been lifted: adjust relative active pointer fields
					int index = event.getActionIndex();
					if (event.getPointerId(index) == mActivePointerId)
					{
						int newIndex = (index == 0 ? 1 : 0);
						mActivePointerId = event.getPointerId(newIndex);
						mActivePointerX = event.getX(newIndex);
						mActivePointerY = event.getY(newIndex);
					}

					break;
				}
			}

			return false;
		}
	}

	/** Manages the flag mIsActionBarVisible while delegating its calls to a real implementation */
	/* non-static */private class ActionBarProxy
	{
		private final Activity mActivity;

		public ActionBarProxy(Activity activity)
		{
			checkNonNullArg(activity);
			mActivity = activity;
		}

		public void hide()
		{
			mIsActionBarVisible = false;
			mActivity.getActionBar().hide();
		}

		public void show()
		{
			mIsActionBarVisible = true;
			mActivity.getActionBar().show();
		}

		public boolean isShowing()
		{
			return mActivity.getActionBar().isShowing();
		}

	}

	/**
	 * Routes 'callbacks to a ServiceConnection' to a PreviewServiceConnection
	 */
	private class PreviewServiceConnectionWrapper implements ServiceConnection
	{
		PreviewServiceBinder previewService;

		@Override
		public void onServiceConnected(ComponentName className, IBinder service)
		{
			Log.d(TAG, "onServiceConnected()");

			previewService = (PreviewServiceBinder) service;
			mBound = true;

			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceConnected(previewService);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name)
		{
			Log.d(TAG, "onServiceDisconnected()");

			previewService = null;
			mBound = false;

			// Service has crashed or has been killed, notify components
			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceDisconnected();
			}
		}
	}

	private class PreviewTp3ActionFactory extends BaseActionFactory
	{
		@Override
		public Action buildShareAction()
		{
			return new ShareAction(getFragmentManager(), mMasterDocument);
		}

		@Override
		public Action buildSmartfileAction()
		{
			ArrayList<Path> path = new ArrayList<Path>(1);
			path.add(mMasterDocument.getPath());
			return new SmarfileAction(OldTpm3PreviewFragment.this, path);
		}

		@Override
		public Action buildMailAction()
		{
			return new MailAction(getActivity(), mMasterDocument);
		}

		@Override
		public Action buildZipAction()
		{
			return new PreviewZipAction(getActivity(), mMasterDocument.getPath());
		}

		@Override
		public Action buildExportToPdfAction()
		{
			return new PreviewExportToPdfAction(getActivity(), mMasterDocument.getPath());
		}

		@Override
		public Action buildFavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					addFavoriteRequest();
					mIsFavoriteRequestInProgress = true;
					getActivity().invalidateOptionsMenu();
				}
			};
		}

		@Override
		public Action buildUnfavoriteAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					delFavoriteRequest();
					mIsFavoriteRequestInProgress = true;
					getActivity().invalidateOptionsMenu();
				}
			};
		}

		@Override
		public Action buildRenameAction()
		{
			return new RenameAction(OldTpm3PreviewFragment.this, mMasterDocument.getName(), false,
					new HashMap<String, String>(0));
		}

		@Override
		public Action buildDeleteAction()
		{
			return new PreviewDeleteAction(getActivity(), mMasterDocument.getPath());
		}

		@Override
		public Action buildExtractVersionAction()
		{
			return new PreviewExtractVersionAction(getActivity(), mDisplayingPath, mDisplayingDate);
		}

		@Override
		public Action buildDeleteVersionAction()
		{
			return new Action()
			{
				@Override
				public void execute()
				{
					Activity act = getActivity();
					Intent i = BrowsingService.Contract.buildDeleteRequestIntent(act, new Path[] {mDisplayingPath});
					mCurrRequests.add(BrowsingService.Contract.extractRequestId(i));
					act.startService(i);

					mDisplayingPath = mMasterDocument.getPath();
					mDisplayingDate = mMasterDocument.getLastModifiedDate();
					clearVersions();

					Toast.makeText(act, R.string.prv_delete_version_in_progress, Toast.LENGTH_SHORT).show();
				}
			};
		}

		@Override
		public Action buildPrintAction()
		{
			return new PrintAction(getActivity(), mDisplayingPath);
		}
	}

	/* --- Comments --- */
	public static final String KEY_PATH = PKG + ".key_path";
	public static final String KEY_COMMENTS = PKG + ".key_comments";
	public static final String KEY_ALL_COMMENTS_LOADED = PKG + ".key_all_comments_loaded";
	public static final String KEY_UPDATE_IN_PROGRESS = PKG + ".key_update_in_progress";
	public static final String KEY_ADD_COMMENT_IN_PROGRESS = PKG + ".key_update_in_progress";
	public static final String KEY_COMMENTS_TO_BE_DELETED = PKG + ".key_comments_to_be_deleted";
	public static final int INITIAL_COMMENT_COUNT = 15;
	public static final int NEXT_COMMENT_COUNT = 50;
	private static final int TEAM_DRIVE_SERVICE_REQUEST_ID = 33333333;

	private Path.TeamDrive filePath;
	private CommentsAdapter commentsAdapter;
	private TeamDrive teamdrive;
	// Memory is not a concern here
	@SuppressLint("UseSparseArrays")
	volatile private Map<Long, ImageResultCallback> userImageCallbacks = new HashMap<Long, ImageResultCallback>();
	private final Set<String> imageDownlaodedForUsers = new HashSet<String>();
	private boolean loadFirstComments;
	private boolean addCommentInProgress = false;
	private ArrayList<String> commmentsToBeDeleted;

	private void onCreateComments()
	{
		SessionManager manager = new SessionManager(getActivity());
		Session session = manager.getLastSession();

		if (mMasterDocument.getPath() instanceof Path.TeamDrive)
		{
			filePath = (Path.TeamDrive) mMasterDocument.getPath();
			teamdrive = session.getTeamDriveMap().get(filePath.getTeamDriveId());
		}
	}

	private void onCreateViewComments(Bundle savedInstanceState, View root)
	{
		mDrawerView = root.findViewById(R.id.rl_comment_layout);
		mDrawerLayout = (DrawerLayout) root.findViewById(R.id.drawer_layout);
		mDrawerLayout.setDrawerListener(this);
		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, mDrawerView);

		if (mMasterDocument.getPath() instanceof Path.TeamDrive)
		{
			mCommentsViewContainer.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					mPageNumberHider.run();
					hideCommentsView();
					mFooterView.hide();

					mDrawerLayout.openDrawer(mDrawerView);
				}
			});
		}
		else
		{
			mCommentsViewContainer.setVisibility(View.GONE);
		}

		ListView listView = (ListView) root.findViewById(R.id.lv_cmt_list);
		final EditText addCommentEditText = (EditText) root.findViewById(R.id.et_add_cmt);
		final ImageButton addCommentImage = (ImageButton) root.findViewById(R.id.img_send_cmt);

		List<Comment> comments = new ArrayList<Comment>();

		if (savedInstanceState != null && savedInstanceState.containsKey(KEY_COMMENTS))
		{
			comments = savedInstanceState.getParcelableArrayList(KEY_COMMENTS);
			loadFirstComments = false;

			ViewSwitcher switcher = (ViewSwitcher) root.findViewById(R.id.vs_load_comments);

			if (comments.size() > 0)
			{
				switcher.setDisplayedChild(1);
			}
			else
			{
				switcher.setVisibility(View.GONE);

				TextView tvNoComment = (TextView) root.findViewById(R.id.tv_no_comments);
				tvNoComment.setVisibility(View.VISIBLE);
			}
		}
		else
		{
			loadFirstComments = true;
		}

		commentsAdapter = new CommentsAdapter(getActivity(), comments, teamdrive, this, this);
		if (savedInstanceState != null)
		{
			if (savedInstanceState.containsKey(KEY_ALL_COMMENTS_LOADED))
			{
				commentsAdapter.setAllCommentsAreLoaded(savedInstanceState.getBoolean(KEY_ALL_COMMENTS_LOADED));
			}

			if (savedInstanceState.containsKey(KEY_UPDATE_IN_PROGRESS))
			{
				commentsAdapter.setUpdateInProgress(savedInstanceState.getBoolean(KEY_UPDATE_IN_PROGRESS));
			}

			commmentsToBeDeleted = savedInstanceState.getStringArrayList(KEY_COMMENTS_TO_BE_DELETED);
		}

		if (commmentsToBeDeleted == null)
		{
			commmentsToBeDeleted = new ArrayList<String>();
		}

		listView.setAdapter(commentsAdapter);

		boolean enabled = addCommentEditText.length() > 0;
		addCommentImage.setClickable(enabled);
		addCommentImage.setEnabled(enabled);

		addCommentEditText.addTextChangedListener(new TextWatcher()
		{
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count)
			{
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after)
			{
			}

			@Override
			public void afterTextChanged(Editable s)
			{
				boolean enabled = s.length() > 0;
				addCommentImage.setClickable(enabled);
				addCommentImage.setEnabled(enabled);
			}
		});

		int dimension = getActivity().getResources().getDrawable(R.drawable.add_comment_icon).getIntrinsicHeight();
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(dimension, dimension);

		final ProgressBar pbAddComment = (ProgressBar) root.findViewById(R.id.cmt_pb_add_comment);
		pbAddComment.setLayoutParams(params);

		addCommentImage.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				String comment = addCommentEditText.getText().toString();
				ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();

				getActivity().startService(
						CommentIntentHelper.getAddCommentIntent(getActivity(), comment, commentsIds, filePath,
								commentsAdapter.getSince()));

				addCommentEditText.setText("");

				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
						Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(addCommentEditText.getWindowToken(), 0);

				addCommentInProgress = true;
				addCommentImage.setVisibility(View.INVISIBLE);
				pbAddComment.setVisibility(View.VISIBLE);
			}
		});

		if (savedInstanceState != null && savedInstanceState.getBoolean(KEY_ADD_COMMENT_IN_PROGRESS, false))
		{
			addCommentInProgress = true;
			addCommentImage.setVisibility(View.INVISIBLE);
			pbAddComment.setVisibility(View.VISIBLE);
		}
	}

	private void onResumeComments()
	{
		if (mMasterDocument.getPath() instanceof Path.TeamDrive && loadFirstComments)
		{
			getActivity().startService(
					CommentIntentHelper.getLoadCommentIntent(getActivity(), false, INITIAL_COMMENT_COUNT, null,
							filePath, 0, 0));
		}

		EventBus.getDefault().registerSticky(this);
	}

	private void onPauseComments()
	{
		EventBus.getDefault().unregister(this);
	}

	private void onSaveInstanceStateComments(Bundle outState)
	{
		outState.putParcelableArrayList(KEY_COMMENTS, (ArrayList<? extends Parcelable>) commentsAdapter.getComments());
		outState.putBoolean(KEY_ALL_COMMENTS_LOADED, commentsAdapter.areAllCommentsLoaded());
		outState.putBoolean(KEY_UPDATE_IN_PROGRESS, commentsAdapter.isUpdateInProgress());
		outState.putBoolean(KEY_ADD_COMMENT_IN_PROGRESS, addCommentInProgress);
		outState.putStringArrayList(KEY_COMMENTS_TO_BE_DELETED, commmentsToBeDeleted);
	}

	/* --- Comments --- */
	@Override
	public void onLoadRequested(User user, ImageResultCallback callback)
	{
		int userImageSize = getResources().getDrawable(R.drawable.tmd_default_icon).getIntrinsicHeight();

		long requestId = TeamDriveService.getRandomRequestId();
		TeamDriveService.getUserImageFromCache(getActivity(), user, userImageSize, TEAM_DRIVE_SERVICE_REQUEST_ID,
				requestId);
		if (!imageDownlaodedForUsers.contains(user.getUserId()))
		{
			TeamDriveService.getUserImage(getActivity(), user, userImageSize, TEAM_DRIVE_SERVICE_REQUEST_ID, requestId);
			imageDownlaodedForUsers.add(user.getUserId());
		}

		userImageCallbacks.put(requestId, callback);
	}

	public void onEventMainThread(TeamDriveServiceResponse event)
	{
		if (event.getRequestCode() != TEAM_DRIVE_SERVICE_REQUEST_ID)
		{
			return;
		}
		try
		{
			UserImageItem[] responseData = (UserImageItem[]) event.getResponseData();
			if (responseData != null && responseData.length > 0)
			{
				UserImageItem imageItem = responseData[0];
				if (imageItem != null)
				{
					userImageCallbacks.get(imageItem.getRequestId()).onResult(imageItem);
				}
			}
		}
		catch (IOException | SQLException | CortadoException e)
		{
			e.printStackTrace();
		}
	}

	public List<Comment> handleCommentChanges(ChangedCommentResponse response, List<Comment> currentList)
	{
		List<Comment> newList = new ArrayList<Comment>(currentList);
		if (response == null)
		{
			return newList;
		}

		List<Comment> added = response.getAdded();
		newList.addAll(emptyIfNull(added));

		List<Comment> deleted = response.getDeleted();
		for (Comment delete : emptyIfNull(deleted))
		{
			newList.remove(delete);
		}

		List<Comment> modified = response.getModified();
		for (Comment modify : emptyIfNull(modified))
		{
			newList.remove(modify);
			newList.add(modify);
		}
		return newList;
	}

	@Override
	public void deleteComment(String commentId)
	{
		commmentsToBeDeleted.add(commentId);

		ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();

		getActivity().startService(
				CommentIntentHelper.getDeleteCommentIntent(getActivity(), commentId, commentsIds, filePath,
						commentsAdapter.getSince()));
	}

	@Override
	public boolean willCommentBeDeleted(String commentId)
	{
		return commmentsToBeDeleted.contains(commentId);
	}

	@Override
	public void loadNextComments()
	{
		ArrayList<String> commentsIds = commentsAdapter.getCommentsIds();
		int start = commentsAdapter.isUpdateInProgress() ? commentsAdapter.getCount() - 1 : commentsAdapter.getCount();

		getActivity().startService(
				CommentIntentHelper.getLoadCommentIntent(getActivity(), true, NEXT_COMMENT_COUNT, commentsIds,
						filePath, commentsAdapter.getSince(), start));
	}

	public void onEventMainThread(CommentOperationResponse event)
	{
		int responseCode = event.getResponseCode();
		Bundle extras = event.getExtras();

		switch (responseCode)
		{
			case LoadCommentsIntentService.RESPONSE_CODE:
			{
				showLoadedComments(event);
				break;
			}
			case AddCommentIntentService.RESPONSE_CODE:
			{
				addCommentInProgress = false;

				ImageButton addCommentImage = (ImageButton) getView().findViewById(R.id.img_send_cmt);
				ProgressBar pbAddComment = (ProgressBar) getView().findViewById(R.id.cmt_pb_add_comment);
				addCommentImage.setVisibility(View.VISIBLE);
				pbAddComment.setVisibility(View.INVISIBLE);

				showChangedComment(event);
				break;
			}
			case DeleteCommentIntentService.RESPONSE_CODE:
			{
				String commentId = extras.getString(CommentIntentHelper.EXTRA_COMMENT_ID);
				if (commentId != null)
				{
					commmentsToBeDeleted.remove(commentId);
				}

				showChangedComment(event);
				break;
			}
		}

		EventBus.getDefault().removeStickyEvent(event);
	}

	private void showLoadedComments(CommentOperationResponse event)
	{
		List<Comment> newComments = event.getNewCommentList();
		if (newComments != null && newComments.size() < event.getNewCommentNumber())
		{
			commentsAdapter.setAllCommentsAreLoaded(true);
		}

		commentsAdapter.removeRefreshComment();
		List<Comment> currentList = commentsAdapter.getComments();

		if (event.isCheckChanges())
		{
			ListView list = (ListView) getView().findViewById(R.id.lv_cmt_list);
			int oldPosition = list.getFirstVisiblePosition();
			int offset = list.getChildAt(0).getTop();

			ChangedCommentResponse changeResponse = event.getChangeCommentResponse();

			if (changeResponse != null)
			{
				List<Comment> deletedCommentsList = changeResponse.getDeleted();

				if (deletedCommentsList != null)
				{
					for (int i = 0; i < oldPosition; i++)
					{
						if (deletedCommentsList.contains(currentList.get(i)))
						{
							oldPosition--;
						}
					}
				}
			}

			int newPosition = oldPosition + newComments.size() - 1;

			List<Comment> commentList = handleCommentChanges(changeResponse, currentList);
			commentList.addAll(newComments);

			displayComments(commentList);

			list.setSelectionFromTop(newPosition, offset);

			commentsAdapter.setUpdateInProgress(false);
		}
		else
		{
			displayComments(newComments);
		}
	}

	private void showChangedComment(CommentOperationResponse event)
	{
		ChangedCommentResponse response = event.getChangeCommentResponse();
		if (response != null)
		{
			List<Comment> commentList = handleCommentChanges(response, commentsAdapter.getComments());
			displayComments(commentList);
		}
		else
		{
			Toast.makeText(getActivity(), R.string.prv_cmt_err_loading_comments, Toast.LENGTH_LONG).show();
		}
	}

	private void displayComments(List<Comment> commentList)
	{
		ViewSwitcher switcher = (ViewSwitcher) getView().findViewById(R.id.vs_load_comments);
		TextView tvNoComment = (TextView) getView().findViewById(R.id.tv_no_comments);

		if (commentList != null && commentList.size() > 0)
		{
			switcher.setVisibility(View.VISIBLE);
			switcher.setDisplayedChild(1);
			tvNoComment.setVisibility(View.GONE);
		}
		else
		{
			switcher.setVisibility(View.GONE);
			tvNoComment.setVisibility(View.VISIBLE);
		}

		commentsAdapter.setComments(commentList);
		commentsAdapter.notifyDataSetChanged();
	}
}
