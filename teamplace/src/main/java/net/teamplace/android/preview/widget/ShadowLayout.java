package net.teamplace.android.preview.widget;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.cortado.android.R;

import static net.teamplace.android.utils.GenericUtils.tag;

/**
 * Layout that draws a cast shadow outside its bounds. The shadow size is set to 10 dp
 * 
 * @author Gil Vegliach
 */
public class ShadowLayout extends FrameLayout
{
	private static final String TAG = tag(ShadowLayout.class);

	private float mDensity;
	private int mCommentCountBgColor;
	private int mCommentCountColor;
	private Paint mTextPaint;
	private int mCount = 0;
	private Drawable mCommentCountDrawable = null;
	private View mCountView = null;
	public ShadowLayout(Context context)
	{
		this(context, null);

	}

	public ShadowLayout(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public ShadowLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);
		setWillNotDraw(false);

		Resources res = context.getResources();
		mDensity  = res.getDisplayMetrics().density;
		mCommentCountBgColor = res.getColor(R.color.prv_version_bg);
		mCommentCountColor = res.getColor(R.color.prv_version_text);

		mTextPaint = new Paint();
		mTextPaint.setAntiAlias(true);
		mTextPaint.setTextSize(12.f * mDensity);
		mTextPaint.setTypeface(Typeface.DEFAULT_BOLD);
		mTextPaint.setColor(mCommentCountColor);

	}

	public int getCount()
	{
		return mCount;
	}

	public void setCount(int count) {
		this.mCount = count;
	}


	public void showCommentCount(int count) {
		this.mCount = count;
		if (mCountView != null)
			this.removeView(mCountView);
		mCountView = null;

		if (mCount < 1) {
			mCommentCountDrawable = null;
			invalidate();
			return;
		}

		String commmentsCountLabel = String.valueOf(mCount);
		if(mCount > 999) commmentsCountLabel = "999+";
		int h = (int) (15.f * mDensity + .5f);
		int w = h;


		Rect textBounds = new Rect();
		mTextPaint.getTextBounds(commmentsCountLabel, 0, commmentsCountLabel.length(), textBounds);
		mTextPaint.setAlpha(255);

		if (w < textBounds.width())
			w = textBounds.width() + (int) (4.f * mDensity + .5f);

		GradientDrawable d = new GradientDrawable();
		d.setShape(GradientDrawable.RECTANGLE);
		d.setSize(w, h);
		d.setCornerRadius(1.5f * mDensity);
		d.setColor(mCommentCountBgColor);
		d.setBounds(0, 0, w, h);

		Bitmap b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
		Canvas c = new Canvas(b);
		d.draw(c);
		c.drawText(commmentsCountLabel, (w - textBounds.width()) / 2.f, (h + textBounds.height()) / 2.f, mTextPaint);

		mCommentCountDrawable = new BitmapDrawable(getContext().getResources(), b);
		mCommentCountDrawable.setBounds(0, 0, w, h);
		mCommentCountDrawable.setAlpha(255);

		mCountView = new ImageView(this.getContext());
		LayoutParams params = this.generateDefaultLayoutParams();
		params.height = h;
		params.width = w;
		params.gravity = Gravity.RIGHT;
		params.topMargin = 2;
		params.rightMargin = 2;

		mCountView.setBackground(mCommentCountDrawable);
		this.addView(mCountView, params);
		bringChildToFront(mCountView);
	}


	/**
	 * @param bitmap The source bitmap.
	 * @param opacity a value between 0 (completely transparent) and 255 (completely
	 * opaque).
	 * @return The opacity-adjusted bitmap.  If the source bitmap is mutable it will be
	 * adjusted and returned, otherwise a new bitmap is created.
	 */
	/*
	private Bitmap adjustOpacity(Bitmap bitmap, int opacity)
	{
		Bitmap mutableBitmap = bitmap.isMutable()
				? bitmap
				: bitmap.copy(Bitmap.Config.ARGB_8888, true);
		Canvas canvas = new Canvas(mutableBitmap);
		int colour = (opacity & 0xFF) << 24;
		canvas.drawColor(colour, PorterDuff.Mode.DST_IN);
		return mutableBitmap;
	}


	@Override
	public void setAlpha(float alpha)
	{
		super.setAlpha(alpha);
		getChildAt(0).setAlpha(alpha);
	}

	*/
}
