package net.teamplace.android.preview.ui.controller;

import static net.teamplace.android.utils.GenericUtils.pkg;
import static net.teamplace.android.utils.GenericUtils.tag;
import net.teamplace.android.errorhandling.FeedbackToast;
import net.teamplace.android.preview.PreviewService;
import net.teamplace.android.preview.PreviewService.Contract;
import net.teamplace.android.preview.PreviewService.PreviewServiceBinder;
import net.teamplace.android.preview.dialog.GoToPageDialogFragment;
import net.teamplace.android.preview.ui.PreviewConfiguration;
import net.teamplace.android.preview.ui.PreviewHolderFragment;
import net.teamplace.android.preview.widget.DocumentView;
import net.teamplace.android.preview.widget.PageNumberView;
import net.teamplace.android.preview.widget.PreviewLazyAdapter;
import net.teamplace.android.preview.widget.PreviewLazyAdapter.OnHasVisibleLoadingPagesStatusListener;
import net.teamplace.android.preview.widget.PreviewLazyAdapter.OnPageCountKnownListener;
import net.teamplace.android.utils.Log;
import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.AttributeSet;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.cortado.android.R;

public class Tpm3ContentController implements ContentController, DocumentView.OnPageChangedListener,
		OnPageCountKnownListener, OnHasVisibleLoadingPagesStatusListener
{

	public static final String TAG = tag(Tpm3ContentController.class);
	private static final String PKG = pkg(Tpm3ContentController.class);

	private static final String KEY_IS_PAGE_NUMBER_VISIBLE = PKG + ".isPageNumberVisible";
	private static final String KEY_PAGE_COUNT = PKG + ".pageCount";
	private static final String KEY_IS_PAGE_KNOWN = PKG + ".isPageCountKnown";

	private static final int PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT = 1500;
	private static final int PAGE_NUMBER_FADE_OUT_DELAY_LONG = 2500;

	private PreviewHolderFragment mFragment;
	private View mFragmentView;
	private ProgressBar mProgressBar;

	/** True iff this fragment's activity is bound to PreviewService */
	private boolean mBound = false;

	/** This object will route connection callbacks to {@link #mAdapter}, if available */
	private final PreviewServiceConnectionWrapper mServiceConnection = new PreviewServiceConnectionWrapper();

	/**
	 * Lazily binds pages to DocumentView and visually handles PreviewServiceConnectionListener callbacks, for
	 * instance redrawing the document when the service connects.
	 */
	private PreviewLazyAdapter mAdapter;

	private DocumentView mDocumentView;
	private View mPageNumberViewContainer;
	private PageNumberView mPageNumberView;

	private boolean mIsPageNumberVisible = true;

	private final BroadcastReceiver mResponseReceiver = new BroadcastReceiver()
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			onResponseReceived(intent);
		}
	};

	public Tpm3ContentController(PreviewHolderFragment fragment)
	{
		mFragment = fragment;
	}

	@Override
	public void setFragmentView(View v)
	{
		mFragmentView = v;
	}

	@Override
	public void setPreviewConfiguration(PreviewConfiguration configuration) {

	}

	@Override
	public void onHideOverlaysRequested()
	{
		// REFACTORCOMMENT: Not using delayed animations for now.
		// mFragment.getAnimationHandler().postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_LONG);
		mPageNumberHider.run();
	}

	@Override
	public void onShowOverlaysRequested()
	{
		mPageNumberShower.run();
	}

	@Override
	public int getOverlayState()
	{
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public void onContentChangeRequested(int requestCode)
	{
		switch (requestCode)
		{
			case REQUEST_VERSION_CHANGED:
				rebuildAdapter();
				mDocumentView.setAdapter(mAdapter);
				break;

			default:
				break;
		}
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == GoToPageDialogFragment.REQUEST_CODE)
		{
			int page = data.getIntExtra(GoToPageDialogFragment.KEY_PAGE, 1) - 1;
			mDocumentView.scrollToPage(page);
			// mFragment.hideOverlays();
		}
	}

	@Override
	public void onAttach(Activity activity)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfigurationChanged(Configuration newConfig)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim)
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
	{
		// TODO Auto-generated method stub

	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		mDocumentView = (DocumentView) mFragmentView.findViewById(R.id.document_view);
		rebuildAdapter();
		mDocumentView.setAdapter(mAdapter);
		mDocumentView.setOnTouchListener(new TouchListener2());
		mDocumentView.setOnPageChangedListener(this);
		mPageNumberViewContainer = mFragmentView.findViewById(R.id.page_number_view_container);
		mPageNumberView = (PageNumberView) mPageNumberViewContainer.findViewById(R.id.page_number_view);
		mPageNumberViewContainer.setOnClickListener(mPageNumberViewOnClickListener);
		mProgressBar = (ProgressBar) mFragmentView.findViewById(R.id.progress_bar);

		// REFACTORCOMMENT: taken from restoreInstanceState()
		if (savedInstanceState != null)
		{
			int count = savedInstanceState.getInt(KEY_PAGE_COUNT);
			mAdapter.setCount(count);
			boolean isPageCountKnown = savedInstanceState.getBoolean(KEY_IS_PAGE_KNOWN);
			mAdapter.setPageCountKnown(isPageCountKnown);

			// REFACTORCOMMENT: taken from restoreDecorationsState()
			mIsPageNumberVisible = savedInstanceState.getBoolean(KEY_IS_PAGE_NUMBER_VISIBLE);
		}

		// REFACTORCOMMENT: taken from restoreDecorationsState()
		// Synchronize PageNumber visibility with its flag
		if (mIsPageNumberVisible)
		{
			if (mPageNumberViewContainer.getVisibility() != View.VISIBLE)
			{
				mPageNumberViewContainer.setVisibility(View.VISIBLE);
			}

			// If the PageNumber is visible but not the action bar, then trigger a delayed fade-out
			// animation
			if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_HIDDEN)
			{
				// REFACTORCOMMENT: Not using delayed animations for now.
				// mFragment.getAnimationHandler().removeCallbacks(mPageNumberHider);
				// mFragment.getAnimationHandler().postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
				onHideOverlaysRequested();
			}
		}
		else if (mPageNumberViewContainer.getVisibility() == View.VISIBLE)
		{
			mPageNumberViewContainer.setVisibility(View.GONE);
		}
		return null;
	}

	@Override
	public void onDestroy()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyOptionsMenu()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDestroyView()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onDetach()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onHiddenChanged(boolean hidden)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onInflate(Activity activity, AttributeSet attrs, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onLowMemory()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item)
	{
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onOptionsMenuClosed(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPause()
	{
		mFragment.getAnimationHandler().removeCallbacks(mPageNumberHider);
		LocalBroadcastManager.getInstance(mFragment.getActivity()).unregisterReceiver(mResponseReceiver);
	}

	@Override
	public void onPrepareOptionsMenu(Menu menu)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onResume()
	{
		// Restore PageNumber animation, if need be
		if ((mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_HIDDEN)
				&& mIsPageNumberVisible)
		{
			// REFACTORCOMMENT: Not using delayed animations for now.
			// mFragment.getAnimationHandler().postDelayed(mPageNumberHider, PAGE_NUMBER_FADE_OUT_DELAY_DEFAULT);
			onHideOverlaysRequested();
		}
		Activity act = mFragment.getActivity();
		IntentFilter filter = new IntentFilter(PreviewService.Contract.ACTION_NOTIFICATION_PAGE_LOADED);
		filter.addAction(PreviewService.Contract.ACTION_NOTIFICATION_FAILED);
		LocalBroadcastManager.getInstance(act).registerReceiver(mResponseReceiver, filter);
	}

	@Override
	public void onSaveInstanceState(Bundle outState)
	{
		outState.putBoolean(KEY_IS_PAGE_NUMBER_VISIBLE, mIsPageNumberVisible);
		outState.putInt(KEY_PAGE_COUNT, mAdapter.getCount());
		outState.putBoolean(KEY_IS_PAGE_KNOWN, mAdapter.isPageCountKnown());
	}

	@Override
	public void onStart()
	{
		Activity act = mFragment.getActivity();
		Intent intent = new Intent(act, PreviewService.class);
		act.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onStop()
	{
		if (mBound)
		{
			mFragment.getActivity().unbindService(mServiceConnection);
			mBound = false;

			// Normal unbinding case, notify components
			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceDisconnected();
			}
		}
	}

	@Override
	public void onTrimMemory(int level)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onViewStateRestored(Bundle savedInstanceState)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void onPageChanged(int oldPage, int newPage)
	{
		mPageNumberView.setCurrentPage(newPage + 1);
	}

	private void rebuildAdapter()
	{
		if (mBound && mAdapter != null)
		{
			mAdapter.onPreviewServiceDisconnected();
		}

		mAdapter = new PreviewLazyAdapter(mFragment.getActivity(), mDocumentView, mFragment.getFileInfoController()
				.getDisplayingPath(), mFragment.getFileInfoController().getDisplayingDate());
		mAdapter.setOnPageCountKnownListener(this);
		mAdapter.setOnHasVisibleLoadingPagesStatusListener(this);

		if (mBound)
		{
			mAdapter.onPreviewServiceConnected(mServiceConnection.previewService);
		}
	}

	private void onResponseReceived(Intent intent)
	{
		Log.d(TAG, "onResponseReceived() [1], intent=" + intent);

		// ACTION_NOTIFICATION_PAGE_LOADED is spawned by the adapter and does not store ids
		String action = intent.getAction();

		if (Contract.ACTION_NOTIFICATION_PAGE_LOADED.equals(action))
		{
			onResponsePageLoaded(intent);
			return;
		}
		else if (Contract.ACTION_NOTIFICATION_FAILED.equals(action))
		{
			FeedbackToast.show(mFragment.getActivity().getApplicationContext(), false,
					mFragment.getString(R.string.prv_err_out_of_memory));
			mFragment.getActivity().finish();
		}
	}

	private void onResponsePageLoaded(Intent intent)
	{
		if (mAdapter == null)
		{
			return;
		}

		int pageCount = intent.getIntExtra(Contract.NOTIFICATION_TOTAL_PAGE_NUMBER, -1);
		int pagePos = intent.getIntExtra(Contract.NOTIFICATION_POSITION_LOADED_PAGE, -1);
		mAdapter.onPreviewLoadFinished(pagePos, pageCount);
	}

	private void onSingleTapConfirmed(MotionEvent e)
	{
		if (mFragment.getCommentsController().getOverlayState() == CommentsController.OVERLAY_HIDDEN)
		{
			if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_VISIBLE)
			{
				mFragment.getActionController().onHideOverlaysRequested();
				mFragment.getPreviewContentController().onHideOverlaysRequested();
				mFragment.getCommentsController().onHideOverlaysRequested();
				mFragment.getFileInfoController().onHideOverlaysRequested();
			}
			else
			{
				mFragment.getActionController().onShowOverlaysRequested();
				mFragment.getPreviewContentController().onShowOverlaysRequested();
				mFragment.getCommentsController().onShowOverlaysRequested();
				mFragment.getFileInfoController().onShowOverlaysRequested();
			}
		}
	}

	private void onScrollStarted()
	{
		if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_HIDDEN)
		{
			mFragment.getPreviewContentController().onHideOverlaysRequested();
		}
	}

	private void onScrollFinished()
	{
		if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_HIDDEN)
		{
			mFragment.getPreviewContentController().onHideOverlaysRequested();
		}
	}

	@Override
	public void onPageCountKnown(int count)
	{
		mPageNumberView.setPageCount(count);
	}

	@Override
	public void onHasVisibleLoadingPagesStatusChanged(boolean newHasVisibleLoadingPages)
	{
		mProgressBar.setVisibility(newHasVisibleLoadingPages ? View.VISIBLE : View.GONE);
	}

	private final View.OnClickListener mPageNumberViewOnClickListener = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			// REFACTORCOMMENT: Not using delayed animations for now
			// mHandler.removeCallbacks(mPageNumberHider);
			mFragment.getActionController().onShowOverlaysRequested();

			int count = mAdapter.getCount();
			DialogFragment dialog = GoToPageDialogFragment.newInstance(count);
			dialog.setTargetFragment(mFragment, GoToPageDialogFragment.REQUEST_CODE);
			dialog.show(mFragment.getActivity().getFragmentManager(), GoToPageDialogFragment.TAG);
		}
	};

	/** Runnable that hides PageNumberView with an animation */
	private final Runnable mPageNumberHider = new Runnable()
	{
		boolean mIsAnimating = false;

		@Override
		public void run()
		{
			// If page number is not displayed or we are animating
			if (mPageNumberViewContainer.getVisibility() == View.GONE || mIsAnimating)
			{
				return;
			}

			mIsPageNumberVisible = false;

			ObjectAnimator anim = ObjectAnimator.ofFloat(mPageNumberViewContainer, View.ALPHA, 0.f);
			anim.addListener(new AnimatorListener()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					mIsAnimating = true;
				}

				@Override
				public void onAnimationRepeat(Animator animation)
				{
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					mPageNumberViewContainer.setVisibility(View.GONE);
					mIsAnimating = false;
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
				}
			});
			anim.start();
		};
	};

	/** Runnable that shows PageNumberView with an animation */
	private final Runnable mPageNumberShower = new Runnable()
	{
		boolean mIsAnimating = false;

		@Override
		public void run()
		{
			// If the page number is shown or already animating, nothing to do
			if (mPageNumberViewContainer.getVisibility() == View.VISIBLE || mIsAnimating)
			{
				return;
			}

			mIsPageNumberVisible = true;

			// Otherwise display it with a fade-in animation
			mPageNumberViewContainer.setVisibility(View.VISIBLE);

			ObjectAnimator anim = ObjectAnimator.ofFloat(mPageNumberViewContainer, View.ALPHA, 1.f);
			anim.addListener(new AnimatorListenerAdapter()
			{
				@Override
				public void onAnimationStart(Animator animation)
				{
					mIsAnimating = true;
				}

				@Override
				public void onAnimationEnd(Animator animation)
				{
					mIsAnimating = false;
				}

				@Override
				public void onAnimationCancel(Animator animation)
				{
					mIsAnimating = false;
				}
			});
			anim.start();
		};
	};

	/**
	 * Routes 'callbacks to a ServiceConnection' to a PreviewServiceConnection
	 */
	private class PreviewServiceConnectionWrapper implements ServiceConnection
	{
		PreviewServiceBinder previewService;

		@Override
		public void onServiceConnected(ComponentName className, IBinder service)
		{
			Log.d(TAG, "onServiceConnected()");

			previewService = (PreviewServiceBinder) service;
			mBound = true;

			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceConnected(previewService);
			}
		}

		@Override
		public void onServiceDisconnected(ComponentName name)
		{
			Log.d(TAG, "onServiceDisconnected()");

			previewService = null;
			mBound = false;

			// Service has crashed or has been killed, notify components
			if (mAdapter != null)
			{
				mAdapter.onPreviewServiceDisconnected();
			}
		}
	}

	// TouchListener2 uses onSingleTapConfirmed() instead of performClick()
	@SuppressLint("ClickableViewAccessibility")
	/* non-static */class TouchListener2 implements View.OnTouchListener
	{
		boolean mIsScrolling = false;

		private final GestureDetector mDoubleTapDetector = new GestureDetector(mFragment.getActivity(),
				new SimpleOnGestureListener()
				{
					@Override
					public boolean onSingleTapConfirmed(MotionEvent e)
					{
						Tpm3ContentController.this.onSingleTapConfirmed(e);
						return true;
					};

					@Override
					public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY)
					{
						if (!mIsScrolling)
						{
							mIsScrolling = true;
							onScrollStarted();
						}
						return false;
					};
				});

		@Override
		public boolean onTouch(View v, MotionEvent event)
		{
			if (mIsScrolling)
			{
				int action = event.getActionMasked();
				if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP)
				{
					mIsScrolling = false;
					onScrollFinished();
				}

			}
			return mDoubleTapDetector.onTouchEvent(event);
		}
	}

	/** Handles touches events, showing and hiding decorations */
	class TouchListener implements View.OnTouchListener
	{
		// This is a diagram of the state machine of the touch events: we want to invert the
		// visibility of the action bar and the page number view on a 'click' and set a timer for
		// hiding the page number view on a 'scroll end'. Also we want to ignore the second tap of a
		// double tap (not shown). When an action up or cancel occurs, the field mIsScrolling helps
		// distinguish between the 'click' and the 'scroll' state
		//
		// .................o start ..........
		// .................|.................
		// ........... down |.................
		// .................|.................
		// .................*.................
		// ................/.\................
		// ........... up /...\ move .........
		// ............../.... \..............
		// ...... click * .. -- * scroll .....
		// ................ |___| ............
		// ........... move ....|.. up .......
		// .....................|.............
		// .....................* scroll end .

		private static final int INVALID_POINTER_ID = -1;
		private float mActivePointerX;
		private float mActivePointerY;
		private int mActivePointerId;

		/** Whether we are panning or scaling the DocumentView */
		private boolean mIsScrolling;

		/** Threshold to distinguish a scroll from a tap */
		private final int mTouchSlop;

		/** Returns false on all events except a double tap, where it also resets the scroll flag */
		private final GestureDetector mDoubleTapDetector = new GestureDetector(mFragment.getActivity(),
				new SimpleOnGestureListener()
				{
					/** Returns true on the last event of a double tap, otherwise false */
					@Override
					public boolean onDoubleTapEvent(MotionEvent e)
					{
						int action = e.getActionMasked();
						if (action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_CANCEL)
						{
							return true;
						}

						return false;
					}
				});

		public TouchListener()
		{
			mActivePointerId = INVALID_POINTER_ID;
			mIsScrolling = false;
			mTouchSlop = ViewConfiguration.get(mFragment.getActivity()).getScaledTouchSlop();
		}

		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouch(View v, MotionEvent event)
		{

			// Handles a double tap: we ignore the second tap, cause we treat it as a single tap
			// as far as the action bar and PageNumberView are concerned. But if scrolling was on,
			// then the action bar is already slipping out, so fade out also PageNumberView
			if (mDoubleTapDetector.onTouchEvent(event))
			{
				if (mIsScrolling)
				{
					mFragment.getPreviewContentController().onHideOverlaysRequested();
					mIsScrolling = false;
				}

				return false;
			}

			switch (event.getActionMasked())
			{
				case MotionEvent.ACTION_DOWN:
				{
					mActivePointerId = event.getPointerId(0);
					mActivePointerX = event.getX();
					mActivePointerY = event.getY();
					mIsScrolling = false;

					break;
				}

				case MotionEvent.ACTION_MOVE:
				{
					int index = event.findPointerIndex(mActivePointerId);
					float x = event.getX(index);
					float y = event.getY(index);

					double dst = Math.sqrt((mActivePointerX - x) * (mActivePointerX - x) + (mActivePointerY - y)
							* (mActivePointerY - y));

					// When scrolling starts, hide the action bar and show the PageNumberView
					if (dst > mTouchSlop && !mIsScrolling)
					{
						mFragment.getActionController().onHideOverlaysRequested();
						mFragment.getPreviewContentController().onShowOverlaysRequested();
						mIsScrolling = true;
					}

					break;
				}

				case MotionEvent.ACTION_UP:
				case MotionEvent.ACTION_CANCEL:
				{
					// The finger has been lifted. If it had been a tap, then invert the visibility
					// of the action bar and the page number view
					if (!mIsScrolling)
					{
						if (mFragment.getActionController().getOverlayState() == ActionController.OVERLAY_VISIBLE)
						{
							mFragment.getActionController().onHideOverlaysRequested();
							mFragment.getPreviewContentController().onHideOverlaysRequested();
						}
						else
						{
							mFragment.getActionController().onShowOverlaysRequested();
							mFragment.getPreviewContentController().onShowOverlaysRequested();
						}

					}
					// Otherwise, it was a scroll: the action bar is hidden and we set a timer
					// for hiding the page number view
					else
					{
						mFragment.getPreviewContentController().onHideOverlaysRequested();
					}

					mActivePointerId = INVALID_POINTER_ID;
					mIsScrolling = false;

					break;
				}

				case MotionEvent.ACTION_POINTER_UP:
				{
					// A secondary pointer has been lifted: adjust relative active pointer fields
					int index = event.getActionIndex();
					if (event.getPointerId(index) == mActivePointerId)
					{
						int newIndex = (index == 0 ? 1 : 0);
						mActivePointerId = event.getPointerId(newIndex);
						mActivePointerX = event.getX(newIndex);
						mActivePointerY = event.getY(newIndex);
					}

					break;
				}
			}

			return false;
		}
	}

}
