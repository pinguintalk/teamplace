package net.teamplace.android.preview.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

import com.cortado.android.R;

public class MaxHeightFrameLayout extends FrameLayout
{
	private int mMaxHeight;

	public MaxHeightFrameLayout(Context context)
	{
		super(context);
	}

	public MaxHeightFrameLayout(Context context, AttributeSet attrs)
	{
		this(context, attrs, 0);
	}

	public MaxHeightFrameLayout(Context context, AttributeSet attrs, int defStyle)
	{
		super(context, attrs, defStyle);

		TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.MaxHeightFrameLayout,
				defStyle, 0);
		mMaxHeight = a.getDimensionPixelSize(R.styleable.MaxHeightFrameLayout_maxHeight, -1);
		a.recycle();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		if (mMaxHeight != -1)
		{
			int height = View.MeasureSpec.getSize(heightMeasureSpec);
			if (height > mMaxHeight)
			{
				heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(mMaxHeight, View.MeasureSpec.AT_MOST);
			}
		}
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	public void setMaxHeight(int maxHeight)
	{
		mMaxHeight = maxHeight;
		requestLayout();
	}

	public int getMaxHeight()
	{
		return mMaxHeight;
	}
}
