package net.teamplace.android.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the methods as non-blocking. This means the method is asynchronous and quick to execute, e.g it can be called
 * on the UI Thread. A lack of this annotation does not state that the methods is blocking, rather it states only that
 * such information is not declared.
 * 
 * @see Blocking
 * @author Gil Vegliach
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface NonBlocking
{
}
