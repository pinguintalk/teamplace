package net.teamplace.android.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks a member variable as guarded by the specified instance in synchronized code
 * 
 * @author Gil Vegliach
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.FIELD})
public @interface GuardedBy
{
	String value();
}
