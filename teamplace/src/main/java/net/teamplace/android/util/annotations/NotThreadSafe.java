package net.teamplace.android.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the class as not-thread-safe. If the class is to be used in a multi-threaded environment, then external
 * synchronization must be used
 * 
 * @see ThreadSafe
 * @author Gil Vegliach
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface NotThreadSafe
{
}
