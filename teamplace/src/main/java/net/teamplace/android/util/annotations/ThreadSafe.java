package net.teamplace.android.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the class as thread-safe. This means that the class will behave as expected for all possible interleaving of
 * threads
 * 
 * @see NotThreadSafe
 * @author Gil Vegliach
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.TYPE})
public @interface ThreadSafe
{
}
