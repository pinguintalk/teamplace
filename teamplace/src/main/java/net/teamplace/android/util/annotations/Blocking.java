package net.teamplace.android.util.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Marks the methods as blocking. This means the method is synchronous and could potentially take long time to complete.
 * A lack of this annotation does not state that the methods is non-blocking, it states only that such information is
 * not declared.
 * 
 * @see NonBlocking
 * @author Gil Vegliach
 */
@Retention(RetentionPolicy.SOURCE)
@Target({ElementType.METHOD, ElementType.CONSTRUCTOR})
public @interface Blocking
{
}
