package net.teamplace.android.util.concurrent;

import android.os.AsyncTask;

/**
 * AsyncTask that will just sleep serially for the amounts of time passed in {@link #execute(Long...)}
 * 
 * @author Gil Vegliach
 */
public class SleepyAsyncTask extends AsyncTask<Long, Void, Void>
{
	@Override
	protected Void doInBackground(Long... params)
	{
		for (Long time : params)
		{
			try
			{
				Thread.sleep(time);
			}
			catch (InterruptedException e)
			{
				Thread.currentThread().interrupt();
				break;
			}
		}

		return null;
	}
}
