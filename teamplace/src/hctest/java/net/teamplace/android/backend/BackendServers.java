package net.teamplace.android.backend;

/**
 * Created by jobol on 02/06/15.
 *
 */
public interface BackendServers
{
    public static final String ACCOUNT = "https://reg.test.hc.thinprint.de:444/cloudcentral";
    public static final String WORKPLACE = "https://reg.test.hc.thinprint.de:442";
    public static final String TEAMDRIVE_API = "https://reg.test.hc.thinprint.de:444/teamdrive/v2";
    public static final String ACTIVITY_API = "https://reg.test.hc.thinprint.de:444/activity/v1";
    public static final String QUOTA_API =  "https://reg.test.hc.thinprint.de:444/quota/v2";
    public static final String REGISTRATION = "https://reg.test.hc.thinprint.de";
    public static final String FORGOT_PASSWORD = null; // TODO
    public static final String TUTORIAL = null; // TODO
    public static final String FAQ = "https://www.teamplace.net/en-us/Pricing#faq";
    public static final String PROFILE = "https://reg.test.hc.thinprint.de:444/profile/v1";
}
