package net.teamplace.android.backend;

/**
 * Created by jobol on 02/06/15.
 *
 */
public interface BackendServers
{
    public static final String ACCOUNT = "https://manage.hcdev.vm";
    public static final String WORKPLACE = "https://connect.hcdev.vm";
    public static final String TEAMDRIVE_API = "https://api.hcdev.vm/teamdrive/v2";
    public static final String ACTIVITY_API = "https://api.hcdev.vm/activity/v1";
    public static final String QUOTA_API = "https://api.hcdev.vm/quota/v2";
    public static final String REGISTRATION = "https://account.hcdev.vm";
    public static final String FORGOT_PASSWORD = null; // TODO
    public static final String TUTORIAL = null; // TODO
    public static final String FAQ = "https://www.teamplace.net/en-us/Pricing#faq";
    public static final String PROFILE = "https://api.hcdev.vm/profile/v1";
}
