package com.forgottensystems.multiimagechooserV2;

import java.util.ArrayList;

import android.app.Activity;
import android.app.LoaderManager;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class MultiImageChooserActivity extends Activity implements OnItemClickListener,
		LoaderCallbacks<Cursor>
{
	public static final String TAG = MultiImageChooserActivity.class.getSimpleName();

	public static final String EXTRA_IMAGE_PATH_LIST = "MULTIPLEFILENAMES";
	public static final String EXTRA_TOTAL_FILES_COUNT = "TOTALFILES";

	private static final int DEFAULT_COLUMN_WIDTH = 120;
	private final static int SELECTED_COLOR = 0xff9db877;

	private Cursor imageCursor;
	private int imageIdColumnIndex;
	private int imagePathColumnIndex;
	private int coloumWidth;
	private GridView gridView;
	private ImageAdapter adapter;

	private ArrayList<String> imagePaths = new ArrayList<String>();
	private final ImageFetcher fetcher = new ImageFetcher();
	private boolean shouldRequestThumb = true;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.multiselectorgrid);

		restoreSavedInstanceState(savedInstanceState);

		coloumWidth = DEFAULT_COLUMN_WIDTH;

		Display display = getWindowManager().getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);

		int width = size.x;
		int testColWidth = width / 4;
		if (testColWidth > coloumWidth)
		{
			coloumWidth = width / 5;
		}

		gridView = (GridView) findViewById(R.id.gridview);
		gridView.setColumnWidth(coloumWidth);
		gridView.setOnItemClickListener(this);
		gridView.setOnScrollListener(new OnScrollListener()
		{
			private int lastFirstItem = 0;
			private long timestamp = System.currentTimeMillis();

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState)
			{
				if (scrollState == SCROLL_STATE_IDLE)
				{
					shouldRequestThumb = true;
					adapter.notifyDataSetChanged();
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
			{
				float dt = System.currentTimeMillis() - timestamp;
				if (firstVisibleItem != lastFirstItem)
				{
					double speed = 1 / dt * 1000;
					lastFirstItem = firstVisibleItem;
					timestamp = System.currentTimeMillis();
					shouldRequestThumb = speed < visibleItemCount;
				}
			}
		});

		adapter = new ImageAdapter(this);
		gridView.setAdapter(adapter);

		LoaderManager.enableDebugLogging(false);
		getLoaderManager().initLoader(0, null, this);

		setupHeader();
		updateAcceptButton();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		outState.putStringArrayList(EXTRA_IMAGE_PATH_LIST, imagePaths);
		super.onSaveInstanceState(outState);
	}

	private void restoreSavedInstanceState(Bundle savedInstanceState)
	{
		if (savedInstanceState != null && savedInstanceState.containsKey(EXTRA_IMAGE_PATH_LIST))
		{
			imagePaths = savedInstanceState.getStringArrayList(EXTRA_IMAGE_PATH_LIST);
		}
	}

	private void setupHeader()
	{
		findViewById(R.id.actionbar_done).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				selectClicked();
			}
		});

		findViewById(R.id.actionbar_discard).setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	public boolean isChecked(String filePath)
	{
		return imagePaths.contains(filePath);
	}

	public void cancelClicked(View ignored)
	{
		setResult(RESULT_CANCELED);
		finish();
	}

	public void selectClicked()
	{
		Intent data = new Intent();
		if (imagePaths.isEmpty())
		{
			setResult(RESULT_CANCELED);
		}
		else
		{
			Bundle result = new Bundle();
			result.putStringArrayList(EXTRA_IMAGE_PATH_LIST, new ArrayList<String>(imagePaths));

			if (imageCursor != null)
			{
				result.putInt(EXTRA_TOTAL_FILES_COUNT, imageCursor.getCount());
			}

			data.putExtras(result);
			setResult(RESULT_OK, data);
		}
		finish();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position, long id)
	{
		String name = getImageName(position);

		if (name == null)
		{
			return;
		}

		boolean isChecked = !isChecked(name);
		if (isChecked)
		{
			if (imagePaths.add(name))
			{
				view.setBackgroundColor(SELECTED_COLOR);
			}
		}
		else
		{
			if (imagePaths.remove(name))
			{
				view.setBackgroundColor(Color.TRANSPARENT);
			}
		}

		updateAcceptButton();
	}

	private String getImageName(int position)
	{
		imageCursor.moveToPosition(position);
		try
		{
			return imageCursor.getString(imagePathColumnIndex);
		}
		catch (Exception e)
		{
			return null;
		}
	}

	private void updateAcceptButton()
	{
		TextView doneTv = (TextView) findViewById(R.id.actionbar_done_textview);
		boolean enabled = imagePaths.size() != 0;
		doneTv.setEnabled(enabled);
		findViewById(R.id.actionbar_done).setEnabled(enabled);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int cursorID, Bundle arg1)
	{
		// only query for images on the built-in storage, not SD card
		String extStoragePrefix = Environment.getExternalStorageDirectory().getAbsolutePath() + "%";
		return new CursorLoader(MultiImageChooserActivity.this, MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
				new String[] {MediaStore.Images.Media._ID, MediaStore.Images.Media.DATA},
				MediaStore.Images.Media.DATA + " LIKE ?", new String[] {extStoragePrefix},
				MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC");
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor)
	{
		if (cursor == null)
		{
			return;
		}

		imageCursor = cursor;
		imageIdColumnIndex = imageCursor.getColumnIndex(MediaStore.Images.Media._ID);
		imagePathColumnIndex = imageCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

		adapter.notifyDataSetChanged();
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		imageCursor = null;
	}

	public class ImageAdapter extends BaseAdapter
	{
		private final Bitmap mPlaceHolderBitmap;

		public ImageAdapter(Context c)
		{
			Bitmap tmpHolderBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.loading_icon);
			mPlaceHolderBitmap = Bitmap.createScaledBitmap(tmpHolderBitmap, coloumWidth, coloumWidth, false);
			if (tmpHolderBitmap != mPlaceHolderBitmap)
			{
				tmpHolderBitmap.recycle();
				tmpHolderBitmap = null;
			}
		}

		@Override
		public int getCount()
		{
			return imageCursor == null ? 0 : imageCursor.getCount();
		}

		@Override
		public Object getItem(int position)
		{
			return position;
		}

		@Override
		public long getItemId(int position)
		{
			return position;
		}

		@Override
		public View getView(int pos, View convertView, ViewGroup parent)
		{
			if (convertView == null)
			{
				convertView = new ImageView(MultiImageChooserActivity.this);
			}

			ImageView imageView = (ImageView) convertView;
			imageView.setImageBitmap(null);

			final int position = pos;
			if (!imageCursor.moveToPosition(position))
			{
				return imageView;
			}

			if (imageIdColumnIndex == -1)
			{
				return imageView;
			}

			final int id = imageCursor.getInt(imageIdColumnIndex);
			if (isChecked(imageCursor.getString(imagePathColumnIndex)))
			{
				imageView.setBackgroundColor(SELECTED_COLOR);
			}
			else
			{
				imageView.setBackgroundColor(Color.TRANSPARENT);
			}

			if (shouldRequestThumb)
			{
				fetcher.fetch(Integer.valueOf(id), imageView, coloumWidth);
			}

			return imageView;
		}
	}
}