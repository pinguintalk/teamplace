package net.teamplace.android.account;

public interface AccountConstants
{
	public static final String TOKEN_TYPE_CCS = "ccs";
	public static final String TOKEN_TYPE_FACEBOOK = "facebook";

	public static final String ACCOUNT_TYPE_WORKPLACE = "com.cortado.account";
	public static final String ACCOUNT_TYPE_CCS = "com.cortado.corporate.account";

	public static final String KEY_TOKEN_VALIDITY = "token is vaid until";
	public static final String KEY_EXCEPTION = "exception";

	public static final String CONFIG_BUNDLE_EXTRA_APP_VERSION_CODE = "app version code";
	public static final String CONFIG_BUNDLE_EXTRA_REQUIRED_AUTH_LIBRARY_VERSION = "minimum auth library version";

	public static final int NOTIFICATION_ID_LOGIN_NEEDED = 648546454;

}
