package net.teamplace.android.account;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.SignUpEvent;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.request.account.RegistrationRequester;
import net.teamplace.android.http.request.account.RegistrationResult;
import net.teamplace.android.http.request.account.TokenRequester;
import net.teamplace.android.http.util.GenericUtils;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Pattern;

public class SignupActivity extends Activity
{
	private final String TAG = getClass().getSimpleName();

	private final String KEY_ERROR_TEXT = "keyErrorText";
	private final String KEY_SWITCHER_STATE = "keySwitcherState";

	private final int PW_MIN_LENGTH = 5;
	private final int PW_MAX_LENGTH = 28;

	public static final int REQ_SIGNUP = 1;

	private String accountType;
	private String tokenType;

	private EditText etEmail, etPassword;
	private TextView tvBackToLogin, tvError;
	private Button btnSubmit, btnTellMeMore;
	private ViewSwitcher switcher;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_registration);

		accountType = getIntent().getStringExtra(LoginActivity.EXTRA_ACC_TYPE);
		tokenType = getIntent().getStringExtra(LoginActivity.EXTRA_TOKEN_TYPE);
		etEmail = (EditText) findViewById(R.id.et_email);
		etPassword = (EditText) findViewById(R.id.et_password);
		tvBackToLogin = (TextView) findViewById(R.id.tv_login);
		btnSubmit = (Button) findViewById(R.id.btn_register);
		switcher = (ViewSwitcher) findViewById(R.id.vs_register_button);

		btnSubmit.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				signup();
			}
		});

		tvBackToLogin.setOnClickListener(new OnClickListener()
		{

			@Override
			public void onClick(View v)
			{
				setResult(RESULT_CANCELED);
				finish();
			}
		});

		tvError = (TextView) findViewById(R.id.tv_error);

		btnTellMeMore = (Button) findViewById(R.id.btn_tell_me_more);
		btnTellMeMore.requestFocus();
		btnTellMeMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (LoginActivity.isConnectedToInternet(SignupActivity.this)) {
					Intent i = new Intent(SignupActivity.this, TellMeMoreActivity.class);
					startActivity(i);
				} else {
					Toast.makeText(SignupActivity.this,
									getResources().getString(R.string.error_connection_failed), Toast.LENGTH_LONG).show();
					}
			}
		});

		if (savedInstanceState != null)
		{
			if (savedInstanceState.containsKey(KEY_ERROR_TEXT))
			{
				tvError.setText(savedInstanceState.getString(KEY_ERROR_TEXT));
				tvError.setVisibility(View.VISIBLE);
			}

			if (savedInstanceState.containsKey(KEY_SWITCHER_STATE))
			{
				switcher.setDisplayedChild(savedInstanceState.getInt(KEY_SWITCHER_STATE, 0));
			}
		}

		getLoaderManager().initLoader(123456, null, mCallbacks);
	}

	private static final String MAIL_REGEX = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	private boolean isValidEmail(String strEmail)
	{
		return Pattern.compile(MAIL_REGEX).matcher(strEmail).matches();
	}

	@Override
	public void onBackPressed()
	{
		setResult(RESULT_CANCELED);
		super.onBackPressed();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		if (tvError.getText().length() > 0)
		{
			outState.putString(KEY_ERROR_TEXT, tvError.getText().toString());
		}

		outState.putInt(KEY_SWITCHER_STATE, switcher.getDisplayedChild());

		super.onSaveInstanceState(outState);
	}

	private void signup()
	{
		tvError.setVisibility(View.GONE);

		String password = etPassword.getText().toString();

		if (!GenericUtils.isValidEmail(etEmail.getText().toString()))
		{
			tvError.setVisibility(View.VISIBLE);
			tvError.setText(R.string.login_enter_email);
		}
		else if (password == null || password.length() < PW_MIN_LENGTH || password.length() > PW_MAX_LENGTH)
		{
			tvError.setVisibility(View.VISIBLE);
			tvError.setText(String
					.format(getString(R.string.registration_enter_password), PW_MIN_LENGTH, PW_MAX_LENGTH));
		}
		else
		{
			// check spaces in password
			if (password.matches(".*\\s+.*"))
			{
				tvError.setVisibility(View.VISIBLE);
				tvError.setText(R.string.registration_spaces_not_allow_in_password);
				return;
			}
			switcher.setDisplayedChild(1);
			Loader<Intent> l = getLoaderManager().restartLoader(123456, null, mCallbacks);
			l.forceLoad();
		}
	}



	private LoaderCallbacks<Intent> mCallbacks = new LoaderCallbacks<Intent>()
	{

		@Override
		public void onLoaderReset(Loader<Intent> arg0)
		{
		}

		@Override
		public void onLoadFinished(Loader<Intent> arg0, Intent arg1)
		{

			if (arg1.getStringExtra(SignupLoader.KEY_ERROR) == null)
			{
				setResult(RESULT_OK, arg1);
				finish();
			}
			else
			{
				if (switcher != null)
				{
					switcher.setDisplayedChild(0);
				}

				String error = arg1.getStringExtra(SignupLoader.KEY_ERROR);

				if (error == null)
				{
					error = getString(R.string.error_default);
				}

				if (tvError != null)
				{
					tvError.setText(error);
					tvError.setVisibility(View.VISIBLE);
				}
			}
		}

		@Override
		public Loader<Intent> onCreateLoader(int id, Bundle args)
		{
			return new SignupLoader(getApplicationContext(), etEmail.getText().toString(), etPassword.getText()
					.toString(), tokenType, accountType);
		}
	};

	private static class SignupLoader extends AsyncTaskLoader<Intent>
	{
		static final String KEY_PASSWORD = "password_key";
		static final String KEY_ERROR = "error_key";

		private String mUser, mPassword, mAccountType;

		public SignupLoader(Context ctx, String user, String password, String tokenType, String accountType)
		{
			super(ctx);
			mUser = user;
			mPassword = password;
			mAccountType = accountType;
		}

		@Override
		public Intent loadInBackground()
		{
			RegistrationRequester requester = new RegistrationRequester(getContext(), null, AccountLibrary.configuration());
			TokenRequester tokenRequester = new TokenRequester(getContext(), null, AccountLibrary.configuration());

			Bundle data = new Bundle();
			boolean success = false;
			try
			{
				RegistrationResult result = requester.registerAccount(mUser, mPassword);

				if (result.isSuccess())
				{
					success = true;
					String token = tokenRequester.requestToken(mUser, mPassword, AccountConstants.ACCOUNT_TYPE_CCS)
							.getTokenStr();

					if (deleteExistingAccounts())
					{
						data.putString(AccountManager.KEY_ACCOUNT_NAME, mUser);
						data.putString(KEY_PASSWORD, mPassword);
						data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType);
						data.putString(AccountManager.KEY_AUTHTOKEN, token);
					}
					else
					{
						data.putString(KEY_ERROR, getContext().getString(R.string.error_delete_account));
					}
				}
				else
				{
					int code = result.getResponseCode();

					switch (code)
					{
						case 406:
							data.putString(KEY_ERROR, getContext().getString(R.string.error_invalid_email_address));
							break;

						case 409:
							data.putString(KEY_ERROR, getContext().getString(R.string.error_email_in_use));
							break;

						default:
							data.putString(KEY_ERROR,
									String.format(getContext().getString(R.string.error_code_occurred), code));
							break;
					}
				}
			}
			catch (CortadoException e)
			{
				e.printStackTrace();

				if (e instanceof ConnectFailedException)
				{
					data.putString(KEY_ERROR, getContext().getString(R.string.error_connection_failed));
				}
				else
				{
					data.putString(KEY_ERROR, getContext().getString(R.string.error_default));
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
				data.putString(KEY_ERROR, getContext().getString(R.string.error_default));
			}

			AccountLibrary.answers().logSignUp(new SignUpEvent().putSuccess(success));

			Intent intent = new Intent();
			intent.putExtras(data);
			return intent;
		}

		private boolean deleteExistingAccounts()
		{
			AccountManager am = AccountManager.get(getContext());
			Account[] existingAccounts = am.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE);
			final CountDownLatch latch = new CountDownLatch(existingAccounts.length);
			final AtomicInteger failed = new AtomicInteger(existingAccounts.length);

			for (Account account : existingAccounts)
			{
				am.removeAccount(account, new AccountManagerCallback<Boolean>()
				{
					@Override
					public void run(AccountManagerFuture<Boolean> future)
					{
						try
						{
							if (future.getResult())
							{
								failed.decrementAndGet();
							}
						}
						catch (OperationCanceledException e)
						{
							e.printStackTrace();
						}
						catch (AuthenticatorException e)
						{
							e.printStackTrace();
						}
						catch (IOException e)
						{
							e.printStackTrace();
						}
						latch.countDown();
					}
				}, null);
			}

			try
			{
				latch.await();
				return failed.get() == 0;
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
				return false;
			}
		}
	}
}
