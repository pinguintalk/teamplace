package net.teamplace.android.account;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SignupDoneActivity extends Activity
{
	private final String TAG = getClass().getSimpleName();

	public static final int REQUEST_CODE = 421986;

	public static final int RESULT_START_TUTORIAL = 13516513;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_tutorial);

		TextView tvGoToTeamplace = (TextView) findViewById(R.id.tv_teamplace);
		tvGoToTeamplace.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setResult(RESULT_OK);
				finish();
			}
		});

		Button btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				setResult(RESULT_START_TUTORIAL);
				finish();
			}
		});
	}

	@Override
	public void onBackPressed()
	{
		setResult(RESULT_OK);
		super.onBackPressed();
	}
}
