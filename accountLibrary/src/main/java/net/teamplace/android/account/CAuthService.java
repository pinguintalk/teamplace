package net.teamplace.android.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class CAuthService extends Service
{

	@Override
	public void onCreate()
	{
		super.onCreate();
		System.out.println("# CAuthService onCreate");
	}

	@Override
	public IBinder onBind(Intent intent)
	{
		System.out.println("# CAuthService onBind");
		CAccountAuthenticator authenticator = new CAccountAuthenticator(this);
		return authenticator.getIBinder();
	}

}
