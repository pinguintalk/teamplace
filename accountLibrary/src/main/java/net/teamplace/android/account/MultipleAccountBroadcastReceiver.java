package net.teamplace.android.account;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MultipleAccountBroadcastReceiver extends BroadcastReceiver
{
	public static final String ACTION_NO_MUTIPLE_ACCOUNTS = "net.teamplace.android.account.ACTION_NO_MUTIPLE_ACCOUNTS";

	@Override
	public void onReceive(Context context, Intent intent)
	{
		String action = intent.getAction();

		if (action.equals(ACTION_NO_MUTIPLE_ACCOUNTS))
		{
			Toast.makeText(context, R.string.no_mutiple_accounts, Toast.LENGTH_SHORT).show();
		}
	}
}
