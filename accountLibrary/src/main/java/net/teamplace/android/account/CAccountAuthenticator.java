package net.teamplace.android.account;

import java.io.IOException;

import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.request.account.TokenRequester;
import android.accounts.AbstractAccountAuthenticator;
import android.accounts.Account;
import android.accounts.AccountAuthenticatorResponse;
import android.accounts.AccountManager;
import android.accounts.NetworkErrorException;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

public class CAccountAuthenticator extends AbstractAccountAuthenticator implements AccountConstants
{
	private final Context context;

	private static final int ERROR_CODE_MULTIPLE_ACCOUNTS = 1;
	private static final int ERROR_CODE_EXCEPTION = 2;

	public CAccountAuthenticator(Context context)
	{
		super(context);
		this.context = context.getApplicationContext();
	}

	@Override
	public Bundle addAccount(AccountAuthenticatorResponse response, String accountType, String authTokenType,
			String[] requiredFeatures, Bundle options) throws NetworkErrorException
	{
		System.out.println("# addAccount");
		final Bundle result = new Bundle();

		if (doesAccountAlreadyExist())
		{
			result.putInt(AccountManager.KEY_ERROR_CODE, ERROR_CODE_MULTIPLE_ACCOUNTS);
			result.putString(AccountManager.KEY_ERROR_MESSAGE, context.getString(R.string.no_mutiple_accounts));

			Intent intent = new Intent(MultipleAccountBroadcastReceiver.ACTION_NO_MUTIPLE_ACCOUNTS);
			context.sendBroadcast(intent);

			return result;
		}

		final Intent intent = new Intent(context, LoginActivity.class);

		if (authTokenType == null) // if started from settings
		{
			intent.putExtra(LoginActivity.EXTRA_STARTED_BY_SETTINGS, true);

			authTokenType = TOKEN_TYPE_CCS;
		}

		intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
		intent.putExtra(LoginActivity.EXTRA_ACC_TYPE, accountType);
		intent.putExtra(LoginActivity.EXTRA_TOKEN_TYPE, authTokenType);

		result.putParcelable(AccountManager.KEY_INTENT, intent);

		return result;
	}

	@Override
	public Bundle getAuthToken(AccountAuthenticatorResponse response, Account account, String strTokenType,
			Bundle options) throws NetworkErrorException
	{
		Bundle result = new Bundle();

		TokenRequester requester = new TokenRequester(context, null, AccountLibrary.configuration());
		AccountManager am = AccountManager.get(context);

		try
		{
			String tokenStr = requester.requestToken(account.name, am.getPassword(account), strTokenType).getTokenStr();

			if (TextUtils.isEmpty(tokenStr))
			{
				return addAccount(response, account.type, strTokenType, null, null);
			}
			else
			{
				result.putString(AccountManager.KEY_ACCOUNT_NAME, account.name);
				result.putString(AccountManager.KEY_ACCOUNT_TYPE, account.type);
				result.putString(AccountManager.KEY_AUTHTOKEN, tokenStr);
			}
		}
		catch (LogonFailedException e)
		{
			final Intent intent = new Intent(context, LoginActivity.class);
			intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response);
			intent.putExtra(LoginActivity.EXTRA_ACC_TYPE, AccountConstants.ACCOUNT_TYPE_WORKPLACE);
			intent.putExtra(LoginActivity.EXTRA_TOKEN_TYPE, strTokenType);
			intent.putExtra(LoginActivity.EXTRA_LOGIN_EMAIL, account.name);

			result.putParcelable(AccountManager.KEY_INTENT, intent);
		}
		catch (CortadoException e)
		{
			e.printStackTrace();
			result.putInt(AccountManager.KEY_ERROR_CODE, ERROR_CODE_EXCEPTION);
			result.putString(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
		}
		catch (IOException e)
		{
			e.printStackTrace();
			result.putInt(AccountManager.KEY_ERROR_CODE, ERROR_CODE_EXCEPTION);
			result.putString(AccountManager.KEY_ERROR_MESSAGE, e.getMessage());
		}
		return result;
	}

	@Override
	public Bundle confirmCredentials(AccountAuthenticatorResponse arg0, Account arg1, Bundle arg2)
			throws NetworkErrorException
	{
		return null;
	}

	@Override
	public Bundle editProperties(AccountAuthenticatorResponse arg0, String arg1)
	{
		return null;
	}

	@Override
	public String getAuthTokenLabel(String arg0)
	{
		return "AuthTokenLabel";
	}

	@Override
	public Bundle hasFeatures(AccountAuthenticatorResponse arg0, Account arg1, String[] arg2)
			throws NetworkErrorException
	{
		return null;
	}

	@Override
	public Bundle updateCredentials(AccountAuthenticatorResponse arg0, Account arg1, String arg2, Bundle arg3)
			throws NetworkErrorException
	{
		return null;
	}

	private boolean doesAccountAlreadyExist()
	{
		AccountManager manager = AccountManager.get(context);

		Account[] accounts = manager.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE);

		return accounts.length > 0;
	}
}
