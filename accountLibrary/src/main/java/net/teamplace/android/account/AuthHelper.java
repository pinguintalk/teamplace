package net.teamplace.android.account;

import java.io.IOException;

import net.teamplace.android.http.exception.CortadoException;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;

public class AuthHelper implements AccountConstants
{
	public static Bundle buildConfigBundle(int versionCode, int minAuthLibVersion)
	{
		Bundle b = new Bundle();
		b.putInt(CONFIG_BUNDLE_EXTRA_APP_VERSION_CODE, versionCode);
		b.putInt(CONFIG_BUNDLE_EXTRA_REQUIRED_AUTH_LIBRARY_VERSION, minAuthLibVersion);
		return b;
	}

	public static void invalidateCachedAuthToken(AccountManager manager, Account account)
	{
		String cachedAuthToken = manager.peekAuthToken(account, TOKEN_TYPE_CCS);
		manager.invalidateAuthToken(ACCOUNT_TYPE_WORKPLACE, cachedAuthToken);
	}

	public static String getTokenFromBundle(Bundle bundle) throws CortadoException, IOException
	{
		if (bundle.getSerializable(KEY_EXCEPTION) != null)
		{
			Exception ex = (Exception) bundle.getSerializable(KEY_EXCEPTION);
			if (ex instanceof CortadoException)
			{
				throw (CortadoException) ex;
			}
			else if (ex instanceof IOException)
			{
				throw (IOException) ex;
			}
		}
		return bundle.getString(AccountManager.KEY_AUTHTOKEN);
	}

	public static Intent getIntentFromBundle(Bundle bundle)
	{
		return (Intent) bundle.getParcelable(AccountManager.KEY_INTENT);
	}

}
