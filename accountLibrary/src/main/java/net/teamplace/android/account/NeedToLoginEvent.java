package net.teamplace.android.account;

import android.content.Intent;

public class NeedToLoginEvent
{
	private Intent loginIntent;

	public NeedToLoginEvent(Intent intent)
	{
		this.loginIntent = intent;
	}

	public Intent getIntent()
	{
		return loginIntent;
	}
}
