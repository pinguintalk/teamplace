package net.teamplace.android.account;

import com.crashlytics.android.answers.Answers;

import net.teamplace.android.http.request.BackendConfiguration;

/**
 * Created by jobol on 02/06/15.
 */
public final class AccountLibrary {

    private static BackendConfiguration backendConfiguration;
    private static Answers answersImpl;

    public static synchronized void init(BackendConfiguration configuration, Answers answers)
    {
        if (backendConfiguration != null)
        {
            throw new IllegalStateException("already initialized");
        }
        backendConfiguration = configuration;
        answersImpl = answers;
    }

    static synchronized BackendConfiguration configuration()
    {
        if (backendConfiguration == null)
        {
            throw new IllegalStateException("not initialized");
        }
        return backendConfiguration;
    }

    static synchronized Answers answers()
    {
        if (answersImpl == null)
        {
            throw new IllegalStateException("not initialized");
        }
        return answersImpl;
    }

    private AccountLibrary()
    {
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return null;
    }
}
