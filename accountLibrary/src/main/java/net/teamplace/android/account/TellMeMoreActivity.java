package net.teamplace.android.account;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

public class TellMeMoreActivity extends Activity
{

	public static final String INTRO_LINK = "http://www.teamplace.net/webview/intro";

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.tell_me_more);
		WebView wv = (WebView) findViewById(R.id.wv_tell_me_more);
		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebViewClient(new WebViewClient()
		{
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url)
			{
				return false;
			}
		});
		wv.loadUrl(INTRO_LINK);
		Button btnSkip = (Button) findViewById(R.id.btn_close);
		btnSkip.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				finish();
			}
		});
	}

	@Override
	public void onBackPressed()
	{
	}

}
