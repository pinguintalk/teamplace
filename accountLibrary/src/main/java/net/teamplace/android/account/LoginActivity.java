package net.teamplace.android.account;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.app.Activity;
import android.app.LoaderManager.LoaderCallbacks;
import android.app.NotificationManager;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.Intent;
import android.content.Loader;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;

import net.teamplace.android.http.database.util.DatabaseResetHelper;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.request.account.AuthToken;
import net.teamplace.android.http.request.account.TokenRequester;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.util.GenericUtils;

import java.io.IOException;

import de.greenrobot.event.EventBus;

public class LoginActivity extends AccountAuthenticatorActivity implements AccountConstants
{
	private final String TAG = getClass().getSimpleName();

	public static final String EXTRA_ACC_TYPE = "account mTokenType";
	public static final String EXTRA_TOKEN_TYPE = "token mTokenType";
	public static final String EXTRA_STARTED_BY_SETTINGS = "started by setings";
	public static final String EXTRA_LOGIN_EMAIL = "login_email";

	private final String KEY_ERROR_TEXT = "keyErrorText";
	private final String KEY_SWITCHER_STATE = "keySwitcherState";

	private String mAccType;
	private String mTokenType;

	private EditText etUser, etPassword;
	private TextView txtNewUser, txtError, tvForgotPassword;
	private Button btnLogin, btnTellMeMore;
	private ViewSwitcher switcher;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_login);

		final Intent intent = getIntent();

		if (!intent.getBooleanExtra(EXTRA_STARTED_BY_SETTINGS, false))
		{
			NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.cancel(NOTIFICATION_ID_LOGIN_NEEDED);
			EventBus.getDefault().removeStickyEvent(NeedToLoginEvent.class);
		}

		mAccType = intent.getStringExtra(EXTRA_ACC_TYPE);
		mTokenType = intent.getStringExtra(EXTRA_TOKEN_TYPE);
		etUser = (EditText) findViewById(R.id.et_email);
		etUser.requestFocus();
		hideKeyboard(this,etUser);
		if (intent.hasExtra(EXTRA_LOGIN_EMAIL))
		{
			String loginEmail = intent.getStringExtra(EXTRA_LOGIN_EMAIL);

			if (loginEmail != null && loginEmail.length() > 0)
			{
				etUser.setText(loginEmail);
			}
		}

		etPassword = (EditText) findViewById(R.id.et_password);

		txtNewUser = (TextView) findViewById(R.id.tv_register);
		txtNewUser.setEnabled(true);
		txtNewUser.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View arg0)
			{
				Intent i = new Intent(LoginActivity.this, SignupActivity.class);
				i.putExtras(intent);
				startActivityForResult(i, SignupActivity.REQ_SIGNUP);
			}
		});

		txtError = (TextView) findViewById(R.id.tv_error);

		btnLogin = (Button) findViewById(R.id.btn_login);
		btnLogin.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				login();
			}
		});

		switcher = (ViewSwitcher) findViewById(R.id.vs_login_button);

		if (savedInstanceState != null)
		{
			if (savedInstanceState.containsKey(KEY_ERROR_TEXT))
			{
				txtError.setText(savedInstanceState.getString(KEY_ERROR_TEXT));
				txtError.setVisibility(View.VISIBLE);
			}

			if (savedInstanceState.containsKey(KEY_SWITCHER_STATE))
			{
				switcher.setDisplayedChild(savedInstanceState.getInt(KEY_SWITCHER_STATE, 0));
			}
		}

		tvForgotPassword = (TextView) findViewById(R.id.tv_forgot_password);
		tvForgotPassword.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				openCloudCentralForgotPassword();
			}
		});

		btnTellMeMore = (Button) findViewById(R.id.btn_tell_me_more);
		btnTellMeMore.requestFocus();
		btnTellMeMore.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				if (isConnectedToInternet(LoginActivity.this)) {
					Intent i = new Intent(LoginActivity.this, TellMeMoreActivity.class);
					startActivity(i);
				} else {
					Toast.makeText(LoginActivity.this,
							getResources().getString(R.string.error_connection_failed), Toast.LENGTH_LONG).show();
				}
			}
		});

		getLoaderManager().initLoader(123456, null, mCallbacks);
	}

	public static void hideKeyboard(Context context, View view)
	{
		InputMethodManager inputMethodManager = (InputMethodManager) context
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	private void openCloudCentralForgotPassword()
	{
		Uri.Builder builder = Uri.parse(AccountLibrary.configuration().forgotPassword()).buildUpon();

		if (tvForgotPassword.getText().length() > 0)
		{
			builder.appendQueryParameter("user", etUser.getText().toString());
		}

		Intent i = new Intent(Intent.ACTION_VIEW, builder.build());
		startActivity(i);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == SignupActivity.REQ_SIGNUP)
		{
			if (resultCode == RESULT_OK)
			{
				addAccount(data, false);

				startActivityForResult(new Intent(LoginActivity.this, SignupDoneActivity.class),
						SignupDoneActivity.REQUEST_CODE);
			}
		}
		else if (requestCode == SignupDoneActivity.REQUEST_CODE)
		{
			setResult(resultCode);
			finish();
		}

		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState)
	{
		if (txtError.getText().length() > 0)
		{
			outState.putString(KEY_ERROR_TEXT, txtError.getText().toString());
		}

		outState.putInt(KEY_SWITCHER_STATE, switcher.getDisplayedChild());

		super.onSaveInstanceState(outState);
	}

	private void login()
	{
		txtError.setVisibility(View.GONE);

		if (!GenericUtils.isValidEmail(etUser.getText().toString()))
		{
			txtError.setVisibility(View.VISIBLE);
			txtError.setText(R.string.login_enter_email);
		}
		else if (TextUtils.isEmpty(etPassword.getText().toString()))
		{
			txtError.setVisibility(View.VISIBLE);
			txtError.setText(R.string.login_enter_password);
		}
		else
		{
			txtNewUser.setEnabled(false);
			switcher.setDisplayedChild(1);
			Loader<Intent> l = getLoaderManager().restartLoader(123456, null, mCallbacks);
			l.forceLoad();
		}
	}

	private void addAccount(Intent accountData, boolean finishActivity)
	{
		AccountManager accountManager = AccountManager.get(getApplicationContext());

		Account account = new Account(accountData.getStringExtra(AccountManager.KEY_ACCOUNT_NAME),
				accountData.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE));

		boolean success = accountManager.addAccountExplicitly(account,
				accountData.getStringExtra(LoginLoader.KEY_PASSWORD), null);

		if (!success)
		{
			accountManager.setPassword(account, accountData.getStringExtra(LoginLoader.KEY_PASSWORD));
		}

		accountManager.setAuthToken(account, TOKEN_TYPE_CCS, accountData.getStringExtra(AccountManager.KEY_AUTHTOKEN));

		setAccountAuthenticatorResult(accountData.getExtras());
		setResult(RESULT_OK, accountData);

		if (finishActivity)
		{
			finish();
		}
	}

	public static boolean isConnectedToInternet(Context context){
		ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null)
		{
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED)
					{
						return true;
					}

		}
		return false;
	}


	private LoaderCallbacks<Intent> mCallbacks = new LoaderCallbacks<Intent>()
	{

		@Override
		public void onLoaderReset(Loader<Intent> arg0)
		{
		}

		@Override
		public void onLoadFinished(Loader<Intent> arg0, Intent arg1)
		{
			if (arg1.getStringExtra(LoginLoader.KEY_ERROR) == null)
			{
				addAccount(arg1, true);
			}
			else
			{
				if (switcher != null)
				{
					switcher.setDisplayedChild(0);
				}

				if (txtError != null)
				{
					txtError.setText(arg1.getStringExtra(LoginLoader.KEY_ERROR));
					txtError.setVisibility(View.VISIBLE);
				}
			}

			txtNewUser.setEnabled(true);
		}

		@Override
		public Loader<Intent> onCreateLoader(int id, Bundle args)
		{
			return new LoginLoader(getApplicationContext(), etUser.getText().toString(), etPassword.getText()
					.toString(), mTokenType, mAccType);
		}
	};

	private static class LoginLoader extends AsyncTaskLoader<Intent>
	{
		static final String KEY_PASSWORD = "mPassword key";
		static final String KEY_ERROR = "error key";
		private String mUser, mPassword, mTokenType, mAccountType;

		public LoginLoader(Context ctx, String user, String password, String tokenType, String accountType)
		{
			super(ctx);
			mUser = user;
			mPassword = password;
			mTokenType = tokenType;
			mAccountType = accountType;
		}

		@Override
		public Intent loadInBackground()
		{

			TokenRequester requester = new TokenRequester(getContext(), null, AccountLibrary.configuration());

			Bundle data = new Bundle();
			try
			{
				AuthToken token = requester.requestToken(mUser, mPassword, mTokenType);
				if (token != null && !TextUtils.isEmpty(token.getTokenStr()))
				{
					data.putString(AccountManager.KEY_ACCOUNT_NAME, mUser);
					data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType);
					data.putString(AccountManager.KEY_AUTHTOKEN, token.getTokenStr());
					data.putString(KEY_PASSWORD, mPassword);
				}
				else
				{
					data.putString(KEY_ERROR, getContext().getString(R.string.error_default));
				}
			}
			catch (CortadoException e)
			{
				e.printStackTrace();

				if (e instanceof LogonFailedException)
				{
					data.putString(KEY_ERROR, getContext().getString(R.string.error_invalid_credentials));
				}
				else if (e instanceof ConnectFailedException)
				{
					data.putString(KEY_ERROR, getContext().getString(R.string.error_connection_failed));
				}
			}
			catch (IOException e)
			{
				e.printStackTrace();
				data.putString(KEY_ERROR, getContext().getString(R.string.error_default));
			}

			removeOldAccountIfNecessary();

			boolean success = data.getString(AccountManager.KEY_AUTHTOKEN) != null;
			AccountLibrary.answers().logLogin(new LoginEvent().putSuccess(success));

			Intent intent = new Intent();
			intent.putExtras(data);
			return intent;
		}

		private void removeOldAccountIfNecessary()
		{
			AccountManager accountManager = AccountManager.get(getContext());
			Account[] cortadoAccounts = accountManager.getAccountsByType(AccountConstants.ACCOUNT_TYPE_WORKPLACE);

			if (cortadoAccounts.length > 0 && !cortadoAccounts[0].name.equals(mUser))
			{
				try
				{
					AccountManagerFuture<Boolean> future = accountManager.removeAccount(cortadoAccounts[0], null, null);
					future.getResult();
					new SessionManager(getContext()).killSession();
					DatabaseResetHelper.killAllDatabases(getContext());
				}
				catch (OperationCanceledException e)
				{
					e.printStackTrace();
				}
				catch (AuthenticatorException e)
				{
					e.printStackTrace();
				}
				catch (IOException e)
				{
					e.printStackTrace();
				}
			}
		}
	}
}
