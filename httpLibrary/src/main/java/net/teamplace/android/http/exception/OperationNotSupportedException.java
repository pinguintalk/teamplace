package net.teamplace.android.http.exception;

public class OperationNotSupportedException extends CortadoException
{
	private static final long serialVersionUID = -8384353696340876953L;

	public OperationNotSupportedException(String strError)
	{
		super(strError);
	}

}
