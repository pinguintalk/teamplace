package net.teamplace.android.http.exception;

public class NoConnectivityException extends CortadoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2798359657321594662L;

	public NoConnectivityException()
	{
		super("No network connection");
	}

	public NoConnectivityException(String strErr)
	{
		super(strErr);
	}

	public NoConnectivityException(Exception e)
	{
		super(e);
	}
}
