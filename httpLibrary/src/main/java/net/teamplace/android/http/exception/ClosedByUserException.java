package net.teamplace.android.http.exception;

import java.io.IOException;

public class ClosedByUserException extends IOException
{
	private static final long serialVersionUID = 7270809705734583547L;

	public ClosedByUserException()
	{
	}

	public ClosedByUserException(String detailMessage)
	{
		super(detailMessage);
	}
}
