package net.teamplace.android.http.exception;

public class LogonFailedException extends CortadoException
{
	private static final long serialVersionUID = -6999628809081436116L;

	public LogonFailedException()
	{
		super("could not logon");
	}

	public LogonFailedException(String strErr)
	{
		super(strErr);
	}
}
