package net.teamplace.android.http.database.util;

import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jobol on 12/06/15.
 */
public abstract class SQLiteStatementExecutor<T>  {

    private AutoIncrementSQLiteStatement mStatement;
    private List<String> columns = new ArrayList<>();
    private Binder mBinder;

    public SQLiteStatementExecutor(SQLiteDatabase database, String table)
    {
        mBinder = new NotInitializedBinder();
        map(null);
        mBinder = new InitializedBinder();

        // TODO make statement creation more generic
        mStatement = AutoIncrementSQLiteStatement.compileInsertOrReplace(database, table,
                columns.toArray(new String[columns.size()]));
    }

    protected abstract <A extends T> void map(A obj);

    public void executeStatement(T toMap) {
        map(toMap);
        mStatement.execute();
    }

    public void bindString(String column, String value) {
        mBinder.bindString(column, value);
    }

    public void bindLong(String column, long value) {
        mBinder.bindLong(column, value);
    }

    public void bindDouble(String column, double value) {
        mBinder.bindDouble(column, value);
    }

    private interface Binder {
        void bindString(String column, String value);
        void bindLong(String column, long value);
        void bindDouble(String column, double value);
    }

    private class InitializedBinder implements Binder {

        @Override
        public void bindString(String column, String value) {
            mStatement.bindString(value);
        }

        @Override
        public void bindLong(String column, long value) {
            mStatement.bindLong(value);
        }

        @Override
        public void bindDouble(String column, double value) {
            mStatement.bindDouble(value);
        }
    }

    private class NotInitializedBinder implements Binder {

        @Override
        public void bindString(String column, String value) {
            columns.add(column);
        }

        @Override
        public void bindLong(String column, long value) {
            columns.add(column);
        }

        @Override
        public void bindDouble(String column, double value) {
            columns.add(column);
        }
    }
}
