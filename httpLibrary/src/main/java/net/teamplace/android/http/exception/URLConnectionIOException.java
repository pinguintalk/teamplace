package net.teamplace.android.http.exception;

public class URLConnectionIOException extends CortadoException
{

	public URLConnectionIOException(String strError)
	{
		super(strError);
	}

	/**
     * 
     */
	private static final long serialVersionUID = -5424745706737460538L;

}
