package net.teamplace.android.http.teamdrive.database;

import android.database.sqlite.SQLiteDatabase;

public class TableDriveEtags
{
	public static final String TABLE = "table_teamdrive_etags";

	public static final String COLUMN_TEAMDRIVE_ID = "ref_etag_teamdrive_id";
	public static final String COLUMN_ETAG = "etag";

	public static final String ID_ETAG_ALL_DRIVES = "etag for all drives";

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " ("
			+ COLUMN_TEAMDRIVE_ID + " REFERENCES " + TableTeamDrives.TABLE + "(" + TableTeamDrives.COLUMN_ID + "), "
			+ COLUMN_ETAG + " TEXT, PRIMARY KEY (" + COLUMN_TEAMDRIVE_ID + "));";

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableDriveEtags.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}
}