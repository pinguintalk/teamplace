package net.teamplace.android.http.request.files;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.util.FilePathHelper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class MoveParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_TEAM)
	private String teamDest;

	@JsonProperty(PARAM_TEAMSRC)
	private String teamSrc;

	@JsonProperty(PARAM_FOLDER)
	private String parentFolder;

	@JsonProperty(PARAM_MOVE_SRC)
	private String moveFileOrFolder;

	@JsonProperty(PARAM_DEST_FOLDER)
	private String moveDestinationFolder;

	@JsonProperty(PARAM_MOVE_DST)
	private String destinationFileName;

	@JsonProperty(PARAM_FORCE_OVERWRITE)
	private int forceOverwrite;

	public MoveParameter(String teamDest, String teamSrc, String parentFolder, String moveFileOrFolder,
			String moveDestinationFolder, String destinationFileName, int forceOverwrite)
	{
		this.teamDest = teamDest;
		this.teamSrc = teamSrc;
		this.parentFolder = FilePathHelper.buildWindowsPath(parentFolder, false, false);
		this.moveFileOrFolder = moveFileOrFolder;
		this.moveDestinationFolder = FilePathHelper.buildWindowsPath(moveDestinationFolder, false, false);
		this.destinationFileName = destinationFileName;
		this.forceOverwrite = forceOverwrite;
	}
}
