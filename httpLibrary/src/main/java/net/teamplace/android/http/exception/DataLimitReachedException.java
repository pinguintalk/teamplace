package net.teamplace.android.http.exception;

public class DataLimitReachedException extends CortadoException
{
	private static final long serialVersionUID = 4745587178824631377L;

	public DataLimitReachedException(String strMsg)
	{
		super(strMsg);
	}
}
