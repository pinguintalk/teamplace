package net.teamplace.android.http.teamdrive;

import android.os.Parcel;
import android.os.Parcelable;

public class CcsRights extends CheckableRights
{
	public static final int DOWNLOAD = 1;
	public static final int DOWNLOAD_AS_PDF_ONLY = 2;
	public static final int PREVIEW = 4;
	public static final int PRINT = 8;
	public static final int MAIL_AND_SHARE = 16;
	public static final int EXPORT = 32;
	// public static final int EXPORT_TO_ZIP = 64;
	public static final int COPY = 128;
	// public static final int CUT = 256;
	public static final int DELETE = 512;
	public static final int RENAME = 1024;
	public static final int UPLOAD = 2048;
	// public static final int CREATE_FILE = 4096;
	public static final int CREATE_FOLDER = 8192;

	public CcsRights(int rights)
	{
		super(rights);
	}

}
