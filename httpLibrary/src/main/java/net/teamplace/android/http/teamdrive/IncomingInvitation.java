package net.teamplace.android.http.teamdrive;

import java.io.IOException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import android.os.Parcel;
import android.os.Parcelable;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class IncomingInvitation
{

	@JsonProperty("Id")
	private String id;

	@JsonProperty("Email")
	private String email;

	@JsonProperty("TeamdriveId")
	private String teamDriveId;

	@JsonProperty("TeamdriveName")
	private String teamDriveName;

	@JsonProperty("TeamdriveNote")
	private String teamDriveNote;

	@JsonProperty("InvitingUserId")
	private String invitingUserId;

	@JsonProperty("InvitingUserEmail")
	private String invitingUserEmail;

	@JsonProperty("InvitingUserDisplayname")
	private String invitingUserDisplayname;

	@JsonProperty("InvitingUserImg")
	private String invitingUserImg;

	@JsonProperty("InvitationMessage")
	private String invitationMessage;

	@JsonProperty("Role")
	private Integer role;

	public IncomingInvitation()
	{
	}

	public IncomingInvitation(String id, String email, String teamDriveId, String teamDriveName, String teamDriveNote,
			String invitingUserId, String invitingUserEmail, String invitingUserDisplayName, String invitingUserImg,
			String invitationMessage, Integer role)
	{
		this.id = id;
		this.email = email;
		this.teamDriveId = teamDriveId;
		this.teamDriveName = teamDriveName;
		this.teamDriveNote = teamDriveNote;
		this.invitingUserId = invitingUserId;
		this.invitingUserEmail = invitingUserEmail;
		this.invitingUserDisplayname = invitingUserDisplayName;
		this.invitingUserImg = invitingUserImg;
		this.invitationMessage = invitationMessage;
		this.role = role;
	}

	public static IncomingInvitation createFromJson(String json) throws JsonParseException, JsonMappingException,
			IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, IncomingInvitation.class);
	}

	public static IncomingInvitation[] createArrayFromJson(String json) throws JsonParseException,
			JsonMappingException, IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, IncomingInvitation[].class);
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getTeamDriveId()
	{
		return teamDriveId;
	}

	public void setTeamDriveId(String teamDriveId)
	{
		this.teamDriveId = teamDriveId;
	}

	public String getTeamDriveName()
	{
		return teamDriveName;
	}

	public void setTeamDriveName(String teamDriveName)
	{
		this.teamDriveName = teamDriveName;
	}

	public String getTeamDriveNote()
	{
		return teamDriveNote;
	}

	public void setTeamDriveNote(String teamDriveNote)
	{
		this.teamDriveNote = teamDriveNote;
	}

	public String getInvitingUserId()
	{
		return invitingUserId;
	}

	public void setInvitingUserId(String invitingUserId)
	{
		this.invitingUserId = invitingUserId;
	}

	public String getInvitingUserEmail()
	{
		return invitingUserEmail;
	}

	public void setInvitingUserEmail(String invitingUserEmail)
	{
		this.invitingUserEmail = invitingUserEmail;
	}

	public String getInvitingUserDisplayName()
	{
		return invitingUserDisplayname;
	}

	public void setInvitingUserDisplayName(String invitingUserDisplayName)
	{
		this.invitingUserDisplayname = invitingUserDisplayName;
	}

	public String getInvitingUserImg()
	{
		return invitingUserImg;
	}

	public void setInvitingUserImg(String invitingUserImg)
	{
		this.invitingUserImg = invitingUserImg;
	}

	public String getInvitationMessage()
	{
		return invitationMessage;
	}

	public void setInvitationMessage(String invitationMessage)
	{
		this.invitationMessage = invitationMessage;
	}

	public Integer getRole()
	{
		return role;
	}

	public void setRole(Integer role)
	{
		this.role = role;
	}

}
