package net.teamplace.android.http.teamdrive;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

/**
 * Created by jobol on 06/08/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class DriveCreation {

    @JsonProperty("Name")
    private String name;

    @JsonProperty("Note")
    private String note;

    @JsonProperty("ImgData")
    private byte[] imgData;

    @JsonProperty("Invitations")
    private UserListInvitation invitations;

    public DriveCreation(String name, String note, byte[] imgData, UserListInvitation invitations) {
        this.name = name;
        this.note = note;
        this.imgData = imgData;
        this.invitations = invitations;
    }

    public static String createJsonObject(DriveCreation... in) throws JsonProcessingException
    {
        return JsonUtils.getDefaultMapper().writeValueAsString(in.length > 1 ? in : in[0]);
    }

    public static DriveCreation createFromJson(String json) throws JsonParseException, JsonMappingException, IOException {
        return JsonUtils.getDefaultMapper().readValue(json, DriveCreation.class);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public byte[] getImgData() {
        return imgData;
    }

    public void setImgData(byte[] imgData) {
        this.imgData = imgData;
    }

    public UserListInvitation getInvitations() {
        return invitations;
    }

    public void setInvitations(UserListInvitation invitations) {
        this.invitations = invitations;
    }
}
