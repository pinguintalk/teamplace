package net.teamplace.android.http.request.account;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonInfoRequestBuilder
{

	private static final String KEY_ID = "id";
	private static final String VALUE_ID = "CloudPrinterInfo";
	private static final String KEY_USERNAME = "username";
	private static final String KEY_PASSWORD = "password";

	public static byte[] createInfoRequest(String user, String password)
	{
		JSONObject json = new JSONObject();
		try
		{
			json.put(KEY_ID, VALUE_ID);
			json.put(KEY_USERNAME, user);
			json.put(KEY_PASSWORD, password);
		}
		catch (JSONException e)
		{
		}
		try
		{
			return json.toString().getBytes("utf-8");
		}
		catch (UnsupportedEncodingException e)
		{
			return json.toString().getBytes();
		}
	}
}
