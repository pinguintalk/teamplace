package net.teamplace.android.http.file;

import net.teamplace.android.http.annotation.JacksonDataModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class RemoteArchive extends RemoteFolder
{
	public RemoteArchive()
	{
	}
}
