package net.teamplace.android.http.request;

/**
 * According to http://c3po:88/hosting-center/tppsrv-requests/allgemein/
 * 
 * @author jobol
 */
public interface ServerParams
{

	/*
	 * ############ PATHS #############
	 */

	/**
	 * browse, file and shell requests
	 */
	public static final String GUI_BROWSE_FILE_SHELL = "gui_1/";

	/**
	 * upload requests
	 */
	public static final String GUI_UPLOAD = "gui_2";

	/**
	 * setup and management related requests
	 */
	public static final String GUI_SETUP = "gui_3/";

	/**
	 * bookmark requests
	 */
	public static final String GUI_BOOKMARKS = "gui_11/";

	/**
	 * registration
	 */
	public static final String REG = "reg";

	/**
	 * hello request
	 */
	public static final String HELLO_2 = "hello_2/";


	/**
	 * thumbnail, file and shell requests
	 */
	public static final String GUI_THUMBNAIL_FILE_SHELL = "gui_1/getthumbnail";

	/**
	 * Path for POST uploads
	 */
	public static final String PATH_SEND = "cgi-bin/send/";

	public static final String PASSWORD_ENCODING = "iso-8859-1";

	public static final String PATH_PRINT_UPLOAD = ".print";
	public static final String PRINT_PROPERTIES_FILE = ".properties";

	public static final String PATH_TOKEN = "user/token";

	public static final String PATH_REGISTRATION = "user/register";

	public static final String PATH_RESET_PASSWORD = "user/password/request";

	public static final String PATH_INFO_REQUEST = "account/requestinfo";

	public static final String PATH_CHANGEPASSWORD = "changepassword";

	public static final String SHARE = "share/";


	/* Profile Path */
	public static final String PATH_IMAGE = "image/";

	public static final String PATH_EMAIL = "Email/";


	public static final String PATH_TEAMDRIVE_USER = "user";
	public static final String PATH_TEAMDRIVE_INVITATION = "invitation";
	public static final String PATH_TEAMDRIVE_ACCEPT = "accept";
	public static final String PATH_TEAMDRIVE_REJECT = "reject";
	public static final String PATH_TEAMDRIVE_CONFIGURATION = "configuration";
	public static final String PATH_TEAMDRIVE_CREATE = "create";
	public static final String PATH_TEAMDRIVE_CREATE_INVITE = "createinvite";

	public static final String PATH_TRANSACTION_ID = "gui_3/transaction/";

	public static final int TIMEOUT = 60000;// TODO ?!  timeout for pdf export is over 30 ms
	public static final int TIMEOUT_CONF = 60000;// TODO ?! <_<

	/*
	 * ############ REQUEST HEADERS ###########
	 */

	/**
	 * Authentication type
	 */
	public static final String HEADER_AUTHORIZATION = "Authorization";
	/**
	 * Prefix for basic auth value
	 */
	public static final String AUTHENTICATION_BASIC = "Basic ";

	public static final String HEADER_X_TOKEN = "X-Token";

	public static final String HEADER_X_TOKEN_VALID_UNTIL = "X-TokenValidUntil";

	public static final String HEADER_X_TOKENTYPE = "X-TokenType";

	public static final String TOKENTYPE_CORTADO = "cortado";

	public static final String HEADER_X_EMAIL = "X-Email";

	public static final String HEADER_X_CBSTATUS = "X-CBStatus";

	public final static String HEADER_CORTADO_NOTIFY = "X-Notify";

	public final static String HEADER_CORTADO_NOTIFY_REASON = "X-LockReason";

	public static final String HEADER_X_PASSWORD = "X-Password";

	/**
	 * Major and Minor version of the Cortado application
	 */
	public static final String HEADER_X_CB_CLIENT_INFO = "X-CBClientInfo";

	/**
	 * Value: The deviceID as used in registration for identification of the device (IMEI, BlackBerry PIN)....
	 */
	public static final String HEADER_X_DEVICE_ID = "X-DeviceID";

	/**
	 * User agent. For value, see http://c3po:88/hosting-center/clients/client-typ-identifikation-mobilecb
	 * -client/?searchterm=header
	 */
	public static final String HEADER_USER_AGENT = "User-Agent";

	// /**
	// * Prefix defining the application name in the user agent string
	// */
	// public static final String USER_AGENT_PREFIX_APP_NAME = "TPCPClient";

	/**
	 * Content-Type heasder
	 */
	public static final String HEADER_CONTENT_TYPE = "Content-Type";

	/**
	 * Multipart content type, append boundary
	 */
	public static final String PREFIX_MULTIPART_WITH_BOUNDARY = "multipart/form-data; boundary=";

	/**
	 * Content-Encoding header
	 */
	public static final String HEADER_CONTENT_ENCODING = "Content-Encoding";

	/**
	 * UTF-8 encoding
	 */
	public static final String ENCODING_UTF8 = "utf-8";

	/**
	 * accepted response MIME types
	 */
	public static final String HEADER_ACCEPT = "Accept";

	public static final String MIME_JSON = "application/json";

	/**
	 * Content length header
	 */
	public static final String HEADER_CONTENT_LENGTH = "Content-Length";

	public static final String HEADER_CONNECTION = "Connection";
	public static final String CONNECTION_CLOSE = "close";

	public static final String HEADER_IF_NONE_MATCH = "If-None-Match";

	public static final String HEADER_ETAG = "Etag";

	public static final String HEADER_RANGE = "Range";

	public static final String HEADER_TRANSACTION_ID = "X-TID";
	public static final String HEADER_FRAGMENT_SIZE = "X-FragSize";

	/*
	 * ############# REQUEST PARAMETERS ################
	 */
	public static final String PARAM_RESPONSE_SCHEMA = "txtschema";
	public static final String RESPONSE_SCHEMA_PLAIN = "1";
	public static final String RESPONSE_SCHEMA_XML = "2";
	public static final String PARAM_FOLDER = "fldr";
	public static final String PARAM_FILE = "file";
	public static final String PARAM_FSTAT = "fstat";

	public static final String PARAM_BOOKMARK = "bookmark";
	public static final String PARAM_BOOKMARK_DELETE = "delete";

	public static final String PARAM_ENUMERATE_COMMENTS = "enumcmmnt";
	public static final String PARAM_COMMENT_ID = "delcmmntid";
	public static final String PARAM_DEST_FOLDER = "fldrdest";
	public static final String PARAM_DOWNLOAD_FILE = "fdn";
	public static final String PARAM_DEST_FILE = "fcpdst";
	public static final String PARAM_DELETE_FILE = "fdel";
	public static final String PARAM_VERSION_FILE = "vrs";
	public static final String PARAM_USER = "user";
	public static final String PARAM_PASSWORD = "password";
	public static final String PARAM_DEPTH = "depth";
	public static final String PARAM_HANDLE_ARCHIVE = "xzip";
	public static final String PARAM_SINCE = "since";
	public static final String HANDLE_ARCHIVE_AS_FILE = "0";
	public static final String HANDLE_ARCHIVE_DONT_ANALYZE = "1";
	public static final String HANDLE_ARCHIVE_ANALYZE = "2";
	public static final String PARAM_REG_MAIL = "MAIL";
	public static final String PARAM_TEAM = "team";
	public static final String PARAM_TEAMSRC = "teamsrc";
	public static final String PARAM_SHELL = "shell";
	public static final String PARAM_EXPORT_FORMAT = "export";
	public static final String PARAM_DEST = "dest";

	public static final String VALUE_EXPORT = "export";
	public static final String VALUE_EXPORT_FORMAT_PDF = "pdf";
	public static final String VALUE_EXPORT_FORMAT_ZIP = "zip";

	public static final String PARAM_CHECK = "check";
	public static final String PARAM_CHECK_TEAM = "team";
	public static final String PARAM_CHECK_FILE = "file";
	public static final String PARAM_CHECK_FOLDER = "fldr";
	public static final String PARAM_CHECK_ID = "id";
	public static final String VALUE_CHECK_LOCATION = "location";

	public static final String VALUE_CHECK_CONFLICT = "conflict";

	public static final String PARAM_PREVIEW_FILE = "fprev";// path to the file to be previewed
															// obligatorisch
	public static final String PARAM_PREVIEW_FOLDER = "fldr";// folder path to the local file to be
																// previewed (relative to user's root)
																// optional
	public static final String PARAM_PREVIEW_PAGENO = "fqppno";// full quality preview page number
																// optional
	public static final String PARAM_PREVIEW_XRES = "xres";// preview picture width in pixels
															// optional
	public static final String PARAM_PREVIEW_YRES = "yres";// preview picture height in pixels
															// optional
	public static final String PARAM_PREVIEW_FLAGS = "flags";// flag mask - controls processing of
																// request (currently only for TPM3
																// preview request) 1 - return as soon
																// as possible (don't wait for
																// response data) 2 - ignore base
																// file's last write time (use preview
																// from cache) optional
	public static final String PARAM_PREVIEW_QLTY = "qlty";
	public static final String PARAM_TOKEN_TYPE = "tokentype";
	public static final String PARAM_RUN_AS = "runas";
	public static final String PARAM_RUN_AS_X = "runasx";
	public static final String PARAM_LOGON_NAME = "logonname";
	public static final String PARAM_LOGON_SERVER = "logonserver";

	public static final String PARAM_TEAMDRIVE_FIELDS_FILTER = "fields";
	public static final String PARAM_IMAGE_SIZE = "s";

	/** Why had this been removed??? @author Gil */
	public static final String PARAM_PREVIEW_TPM = "tpm"; // tpm version, required

	public static final String PARAM_TRANSACTION_ID = "tid"; // tpm version, required

	public static final String PARAM_TOKEN = "token";
	public static final String PARAM_TYPE = "type";
	public static final String PARAM_DEVICEID = "deviceid";

	public static final int OVERWRITE_DONT = 0;
	public static final int OVERWRITE_FORCE = 1;
	public static final int OVERWRITE_NEW_NAME = 2;
	public static final int OVERWRITE_NEW_VERSION = 3;

	/*
	 * ########## REQUEST METHODS ##########
	 */
	public static final String HTTP_GET = "GET";
	public static final String HTTP_POST = "POST";
	public static final String HTTP_DELETE = "DELETE";

	/*
	 * WP/CC SPECIFIC ADDITIONS:
	 */
	public static final String HEADER_X_FINDID = "X-FindId";
	public static final String HEADER_X_FINDSTATUS = "X-FindStatus";
	public static final String HEADER_X_FINDTIME = "X-FindTime";

	public static final String PARAM_FIND = "find";
	public static final String PARAM_FIND_ID = "findid";
	public static final String PARAM_START = "start";
	public static final String PARAM_TIMEOUT = "timeout";
	public static final String PARAM_FORCE_OVERWRITE = "fovr";
	public static final String PARAM_CANCEL = "cancel";

	public static final String PARAM_SEND_TO = "sendto";
	public static final String PARAM_PUBLIC = "everyone";
	public static final String PARAM_HTML = "html";
	public static final String PARAM_USER_TEXT = "usertext";
	public static final String PARAM_VALID_UNTIL = "validuntil";
	public static final String PARAM_SUBJECT = "subject";
	public static final String PARAM_BODY = "body";

	/*
	 * JSON params
	 */

	public static final String JSON_CMD = "cmd";
	public static final String JSON_PARAMS = "params";

	public static final String CMD_SHARE = "share";
	public static final String CMD_DELETE = "delete";
	public static final String CMD_COPY = "copy";
	public static final String CMD_MOVE = "move";
	public static final String CMD_CREATE = "create";
	public static final String CMD_ADD_COMMENT = "addcmmnt";
	public static final String CMD_MODIFY_COMMENT = "modcmmnt";
	public static final String CMD_CHANGED_COMMENT = "cmmntchngd";

	public static final String PARAM_MOVE_SRC = "fmvsrc";
	public static final String PARAM_MOVE_DST = "fmvdst";
	public static final String PARAM_COPY_SRC = "fcpsrc";
	public static final String PARAM_COPY_DST = "fcpdst";
	public static final String PARAM_CREATE = "fcr";
	public static final String PARAM_COMMENT = "cmmnt";
	public static final String PARAM_COMMENT_IDS = "commentids";
	public static final String PARAM_MODIFIED = "modified";
	public static final String PARAM_NOT_MODIFIED = "notmodified";
	public static final String PARAM_ADDED = "added";
	public static final String PARAM_DELETED = "deleted";

	/*
	 * Preview Response Headers
	 */
	public static final String RESPONSE_PREVIEW_XRES = "X-xRes";// preview picture width in pixels
																// optional
	public static final String RESPONSE_PREVIEW_YRES = "X-yRes";// preview picture height in pixels
																// optional
	public static final String RESPONSE_PREVIEW_MAXXRES = "X-xResMax";// preview picture max width
																		// in pixels optional
	public static final String RESPONSE_PREVIEW_MAXYRES = "X-yResMax";// preview picture max height
																		// in pixels optional
	public static final String RESPONSE_PREVIEW_PAGECOUNT = "X-PagesCount";// pagecount

	public static final String PATH_TOKEN_WORKPLACE = "user/token";


	/* CBStatus Codes */
	public static final int CB_STATUS_PROCESSING = 129;  // process is still running

}
