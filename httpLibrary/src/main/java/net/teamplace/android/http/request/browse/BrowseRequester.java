package net.teamplace.android.http.request.browse;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.teamplace.android.http.bookmark.Json;
import net.teamplace.android.http.bookmark.Json.AddBookmark;
import net.teamplace.android.http.bookmark.Json.DeleteBookmark;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.file.BrowseResponse;
import net.teamplace.android.http.json.JsonUtils;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import java.io.IOException;
import java.util.ArrayList;

/***
 * This class handles the browse and bookmark requests.
 * 
 * @author jamic
 */
public class BrowseRequester extends BasicRequester
{
	private static final String TAG = BrowseRequester.class.getSimpleName();

	public BrowseRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public BrowseResponse doBrowseRequest(String teamDriveId, String baseFolder, boolean inspectArchives)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException,
			TeamDriveNotFoundException, RedirectException {
		return doBrowseRequest(teamDriveId, baseFolder, 1, inspectArchives);
	}

	public BrowseResponse doBrowseRequest(String teamDriveId, String baseFolder, int depth, boolean inspectArchives)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException,
			TeamDriveNotFoundException, RedirectException {
		Log.d(TAG, "Execute browse request");

		Uri uri = getBrowseUri(teamDriveId, baseFolder, depth, inspectArchives);
		String json = executeStringGETRequest(uri, getMimeJsonHeader(), true, true);
		return BrowseResponse.createBrowseResponseFromJSON(json);
	}

	public void addBookmarkRequest(String teamDriveId, String path) throws ConnectFailedException, XCBStatusException,
			HttpResponseException, LogonFailedException, NotificationException, ServerLicenseException,
			TeamDriveNotFoundException, IOException, RedirectException {
		addBookmarkRequest(teamDriveId, path, null);
	}

	public void addBookmarkRequest(String teamDriveId, String path, Integer sortingPosition)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException, ServerLicenseException, TeamDriveNotFoundException, IOException, RedirectException {
		Log.d(TAG, "Execute add bookmark request");

		String server = getServerForTeamDriveId(teamDriveId);
		Uri uri = getBookmarkUri(server);

		AddBookmark.Bookmark bookmark = new AddBookmark.Bookmark(path, teamDriveId, sortingPosition);
		ArrayList<AddBookmark.Bookmark> payload = new ArrayList<AddBookmark.Bookmark>();
		payload.add(bookmark);
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		byte[] data = mapper.writeValueAsBytes(payload);

		doJsonUploadPOST(uri, data, true, true);
	}

	public void deleteBookmarkRequest(String teamDriveId, String path) throws ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException, TeamDriveNotFoundException, IOException, RedirectException {
		Log.d(TAG, "Execute delete bookmark request");

		String server = getServerForTeamDriveId(teamDriveId);
		Uri uri = getBookmarkDeleteUri(server);

		DeleteBookmark.Bookmark bookmark = new DeleteBookmark.Bookmark(path, teamDriveId);
		ArrayList<DeleteBookmark.Bookmark> payload = new ArrayList<DeleteBookmark.Bookmark>();
		payload.add(bookmark);
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		byte[] data = mapper.writeValueAsBytes(payload);

		doJsonUploadPOST(uri, data, true, true);
	}

	public ArrayList<Json.EnumerateBookmarks.Bookmark> enumerateBookmarksRequest(String teamDriveId)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException, ServerLicenseException, TeamDriveNotFoundException, JsonParseException,
			JsonMappingException, IOException, RedirectException {
		Log.d(TAG, "Execute enumerate bookmarks request");

		String server = getServerForTeamDriveId(teamDriveId);
		Uri uri = getBookmarkUri(server);
		String json = executeStringGETRequest(uri, getMimeJsonHeader(), true, true);

		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		JavaType arrayListType = mapper.getTypeFactory().constructCollectionType(ArrayList.class,
				Json.EnumerateBookmarks.Bookmark.class);
		return mapper.readValue(json, arrayListType);
	}


	public boolean isFileOrFolderExists(String teamDriveId, String parentFolder, String filename)
			throws TeamDriveNotFoundException,  XCBStatusException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {

		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();
		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_FSTAT, filename);
		builder.appendQueryParameter(PARAM_FOLDER, parentFolder);
		if(teamDriveId != null && !TextUtils.isEmpty(teamDriveId)) {
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		Uri uri = builder.build();

		String jsonResult = "";
		try {
			jsonResult = executeStringGETRequest(uri, getMimeJsonHeader(), false, false);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		}

		return TextUtils.isEmpty(jsonResult)?false:true;
	}


	private Uri getBrowseUri(String teamDriveId, String baseFolder, int depth, boolean inspectArchives)
			throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();
		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_RESPONSE_SCHEMA, RESPONSE_SCHEMA_XML);
		builder.appendQueryParameter(PARAM_FOLDER, baseFolder);
		builder.appendQueryParameter(PARAM_DEPTH, String.valueOf(depth));
		builder.appendQueryParameter(PARAM_HANDLE_ARCHIVE, inspectArchives ? HANDLE_ARCHIVE_ANALYZE
				: HANDLE_ARCHIVE_DONT_ANALYZE);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		return builder.build();
	}

	private Uri getBookmarkUri(String server)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();
		builder.appendEncodedPath(GUI_BOOKMARKS);
		builder.appendEncodedPath(PARAM_BOOKMARK);
		return builder.build();
	}

	private Uri getBookmarkDeleteUri(String server)
	{
		Uri base = getBookmarkUri(server);
		Uri.Builder builder = base.buildUpon();
		builder.appendEncodedPath(PARAM_BOOKMARK_DELETE);
		return builder.build();
	}
}
