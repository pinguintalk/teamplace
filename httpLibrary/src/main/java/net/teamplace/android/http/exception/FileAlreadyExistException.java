package net.teamplace.android.http.exception;

public class FileAlreadyExistException extends AlreadyExistException
{
	/**
     * 
     */
	private static final long serialVersionUID = -7441320134306360811L;

	public FileAlreadyExistException()
	{
		super("File ");
	}

	public FileAlreadyExistException(String strFileName)
	{
		super(strFileName);
	}

}
