package net.teamplace.android.http.session.configuration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.util.GenericUtils;

import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ShellObject
{
	private final String TAG = getClass().getSimpleName();

	public static final String TABLE_NAME_SHELL_TYPE = "table_name_shell_type";
	public static final String COL_SHELL_TYPE = "shell_type";
	public static final String COL_CONFIGURATION_ID = "configuration_id";

	public static final String TABLE_NAME_MAPPING = "table_name_mapping";
	public static final String COL_SHELL_TYPE_ID = "shell_type_id";
	public static final String COL_ORIGINAL_FILETYPE = "original_filetype";
	public static final String COL_EXPORTED_FILETYPE = "exported_filetype";

	public static final String CREATE_TABLE_SHELL = "create table " + TABLE_NAME_SHELL_TYPE + "("
			+ SessionSQLiteHelper.COLUMN_ID + " integer primary key autoincrement, " + COL_CONFIGURATION_ID
			+ " integer, " + COL_SHELL_TYPE + " text);";

	public static final String CREATE_TABLE_MAPPING = "create table " + TABLE_NAME_MAPPING + "("
			+ SessionSQLiteHelper.COLUMN_ID + " integer primary key autoincrement, " + COL_SHELL_TYPE_ID + " integer, "
			+ COL_ORIGINAL_FILETYPE + " text, " + COL_EXPORTED_FILETYPE + " text);";

	private final String DIVIDER = ":";

	private long id;
	private long configurationId;

	@JsonProperty("Type")
	private String type;

	@JsonProperty("Mapping")
	private Map<String, List<String>> mapping = new HashMap<String, List<String>>();

	public ShellObject()
	{
	}

	public ShellObject(String type, Cursor mappingCursor)
	{
		this.type = type;
		this.mapping = generateMappingFromCursor(mappingCursor);
	}

	private Map<String, List<String>> generateMappingFromCursor(Cursor mappingCursor)
	{
		Map<String, List<String>> extensionMap = new HashMap<String, List<String>>();

		if (mappingCursor.moveToFirst())
		{
			String originalFiletype;
			List<String> exportedFiletype;

			do
			{
				originalFiletype = mappingCursor.getString(mappingCursor.getColumnIndex(COL_ORIGINAL_FILETYPE));
				exportedFiletype = GenericUtils.splitStringByDivider(mappingCursor.getString(mappingCursor
						.getColumnIndex(COL_EXPORTED_FILETYPE)), DIVIDER);

				extensionMap.put(originalFiletype, exportedFiletype);
			}
			while (mappingCursor.moveToNext());
		}

		return extensionMap;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public void setConfigurationId(long configurationId)
	{
		this.configurationId = configurationId;
	}

	public String getType()
	{
		return type;
	}

	public Map<String, List<String>> getMapping()
	{
		return mapping;
	}

	@JsonProperty("Mapping")
	private void setMapping(Map<String, Object> mapping)
	{
		String exportedFiletype;

		for (String originalFiletype : mapping.keySet())
		{
			exportedFiletype = (String) mapping.get(originalFiletype);
			this.mapping.put(originalFiletype, GenericUtils.splitStringByDivider(exportedFiletype, DIVIDER));
		}
	}

	public ContentValues getTypeAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_CONFIGURATION_ID, configurationId);
		values.put(COL_SHELL_TYPE, type);

		return values;
	}

	public List<ContentValues> getMappingAsContentValues()
	{
		List<ContentValues> valuesArray = new ArrayList<ContentValues>();
		ContentValues currentValue;

		List<String> destinationExtensions = new ArrayList<String>();
		StringBuilder extensionBuilder = new StringBuilder();

		for (String key : mapping.keySet())
		{
			currentValue = new ContentValues();
			extensionBuilder.setLength(0);

			destinationExtensions = mapping.get(key);

			for (int i = 0; i < destinationExtensions.size(); i++)
			{
				extensionBuilder.append(destinationExtensions.get(i));

				if ((i + 1) < destinationExtensions.size())
				{
					extensionBuilder.append(DIVIDER);
				}
			}

			currentValue.put(COL_SHELL_TYPE_ID, id);
			currentValue.put(COL_ORIGINAL_FILETYPE, key);
			currentValue.put(COL_EXPORTED_FILETYPE, extensionBuilder.toString());

			valuesArray.add(currentValue);
		}

		return valuesArray;
	}
}
