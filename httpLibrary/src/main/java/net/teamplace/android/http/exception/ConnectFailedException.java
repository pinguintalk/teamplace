package net.teamplace.android.http.exception;

public class ConnectFailedException extends CortadoException
{
	private static final long serialVersionUID = 621978723285279940L;

	public ConnectFailedException()
	{
		super("Connect to server failed");
	}

	public ConnectFailedException(String strErr)
	{
		super(strErr);
	}

	public ConnectFailedException(Exception e)
	{
		super(e);
	}
}
