package net.teamplace.android.http.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;

public class TodoToastMaker
{

	private TodoToastMaker()
	{
	}

	public static void showTodoToast(final Context ctx, final String message)
	{
		if (ctx instanceof Activity)
		{
			((Activity) ctx).runOnUiThread(new Runnable()
			{
				@Override
				public void run()
				{
					Toast t = Toast.makeText(ctx, "\n  " + message + "  \n", Toast.LENGTH_SHORT);
					t.setGravity(Gravity.CENTER, 0, 0);
					TextView v = (TextView) t.getView().findViewById(android.R.id.message);
					v.setBackgroundColor(Color.WHITE);
					v.setTextColor(Color.RED);
					t.show();
				}
			});
		}
	}
}
