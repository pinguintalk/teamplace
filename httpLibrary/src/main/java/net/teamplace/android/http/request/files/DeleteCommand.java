package net.teamplace.android.http.request.files;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JacksonDataModel
public class DeleteCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private DeleteParameter[] deleteParameter;

	public DeleteCommand(String team, String parentFolder, String[] folderOrFile, int forceDeleteFolder)
	{
		setCommand(CMD_DELETE);

		List<DeleteParameter> list = new ArrayList<DeleteParameter>();

		for (String deleteFileOrFolder : folderOrFile)
		{
			list.add(new DeleteParameter(team, parentFolder, deleteFileOrFolder, forceDeleteFolder));
		}

		deleteParameter = list.toArray(new DeleteParameter[list.size()]);
	}
}
