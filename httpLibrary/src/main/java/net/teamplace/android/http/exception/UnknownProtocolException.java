package net.teamplace.android.http.exception;

public class UnknownProtocolException extends CortadoException
{

	public UnknownProtocolException(String strError)
	{
		super(strError);
	}

	/**
     * 
     */
	private static final long serialVersionUID = 1L;

}
