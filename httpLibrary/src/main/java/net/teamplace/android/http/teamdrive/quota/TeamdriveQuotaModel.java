package net.teamplace.android.http.teamdrive.quota;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import java.io.IOException;

/**
 * Created by jobol on 06/08/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class TeamdriveQuotaModel extends QuotaModel {

    @JsonProperty("IsSuccess")
    public Boolean isSuccess;

    @JsonProperty("TeamdriveId")
    public String teamDriveId;

    public TeamdriveQuotaModel() {
        super();
    }

    public TeamdriveQuotaModel(Boolean isSuccess, String teamDriveId, Integer limit, Integer remainingMb, Integer remainingPercent,
                               Integer usedMb, Integer usedPercent) {
        super(limit, remainingMb, remainingPercent, usedMb, usedPercent);
        this.isSuccess = isSuccess;
        this.teamDriveId = teamDriveId;
    }

    public static TeamdriveQuotaModel[] arrayFromJson(String json) throws JsonParseException, JsonMappingException,
            IOException {
        return JsonUtils.getDefaultMapper().readValue(json, TeamdriveQuotaModel[].class);
    }
}
