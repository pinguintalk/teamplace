package net.teamplace.android.http.exception;

public class PreviewNotFinishedException extends CortadoException
{

	/**
     * 
     */
	private static final long serialVersionUID = -7875764171047339918L;

	public PreviewNotFinishedException(String msg)
	{
		super(msg);
	}

}
