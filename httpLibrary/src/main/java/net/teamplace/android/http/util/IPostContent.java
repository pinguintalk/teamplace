package net.teamplace.android.http.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.Map;

public interface IPostContent
{

	public Map<String, String> getHeaders();

	public void setHeaders(HttpURLConnection conn);

	public InputStream getPayload();

	public int getSize();

	public boolean isValid();

	public String getType();

	public String getB64Metadata();

}
