package net.teamplace.android.http.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import net.teamplace.android.http.request.ICloseAble;


public class HeaderInputStream extends InputStream
{
	private final ICloseAble closeAble;
	private final InputStream inputStream;
	private final Map<String, List<String>> responseHeaders;

	public HeaderInputStream(ICloseAble closeAble, InputStream inputStream, Map<String, List<String>> responseHeaders)
	{
		super();
		this.closeAble = closeAble;
		this.inputStream = inputStream;
		this.responseHeaders = responseHeaders;

	}

	public Map<String, List<String>> getResponseHeaders()
	{
		return responseHeaders;
	}

	@Override
	public int read() throws IOException
	{
		return this.inputStream.read();
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		this.inputStream.close();
		this.closeAble.close();
	}

}
