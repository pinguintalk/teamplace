package net.teamplace.android.http.session;

import android.content.ContentResolver;
import android.content.Context;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

public class DeviceID
{
	private static final String TAG = DeviceID.class.getSimpleName();

	private static final String INVALID_ANDROID_ID = "9774d56d682e549c";

	private static String mDeviceID;

	public static String getDeviceID(Context context)
	{
		if (mDeviceID == null)
		{
			mDeviceID = fetchUniqueDeviceId(context);
		}

		return mDeviceID;
	}

	private final static String fetchUniqueDeviceId(final Context context)
	{
		Log.d(TAG, "Fetch unique device id");

		String deviceID = null;

		final ContentResolver revolver = context.getContentResolver();

		if (revolver != null)
		{
			deviceID = android.provider.Settings.System.getString(revolver, Settings.Secure.ANDROID_ID);
		}

		if (isInvalidId(deviceID)) // when android id invalid (2.2 bug, invalid id)
		{
			final Object telman = context.getSystemService(Context.TELEPHONY_SERVICE);
			if (telman instanceof TelephonyManager)
			{
				deviceID = ((TelephonyManager) telman).getDeviceId();
			}
		}

		if (isInvalidId(deviceID)) // when deviceId is invalid (no phone)
		{
			final Object wman = context.getSystemService(Context.WIFI_SERVICE);
			if (wman instanceof WifiManager)
			{
				final String strMac = ((WifiManager) wman).getConnectionInfo().getMacAddress();
				if (!isInvalidId(strMac)) // when wifiMac valid
				{
					deviceID = strMac.replace(":", "");
				}
			}
		}

		if (isInvalidId(deviceID))
		{
			deviceID = "0000000000000000000000"; // when nothing valid?
		}

		deviceID = "WMD501D" + deviceID.toUpperCase();

		return deviceID;
	}

	private final static boolean isInvalidId(final String strId)
	{
		if (strId == null || strId.trim().length() == 0 || strId.equalsIgnoreCase(INVALID_ANDROID_ID)
				|| strId.replace('0', ' ').trim().length() == 0)
		{
			return true;
		}

		return false;
	}
}