package net.teamplace.android.http.util;

import java.security.GeneralSecurityException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

public class DebugHelper
{
	private DebugHelper()
	{
	}

	public static boolean isDebugVersion(Context ctx)
	{
		Context app = ctx.getApplicationContext();
		try
		{
			PackageInfo info = app.getPackageManager().getPackageInfo(app.getPackageName(),
					PackageManager.GET_CONFIGURATIONS);
			int flags = info.applicationInfo.flags;
			return (flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0;
		}
		catch (NameNotFoundException e)
		{
			e.printStackTrace();
		}
		return true;
	}

	public static void ignoreSslCerts(Context ctx)
	{
		TodoToastMaker.showTodoToast(ctx, "IGNORING SSL CERTIFICATES!");
		TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager()
		{
			public java.security.cert.X509Certificate[] getAcceptedIssuers()
			{
				return null;
			}

			public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType)
			{
			}

			public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)
			{
			}
		}};

		// Install the all-trusting trust manager
		try
		{
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			// Trust all host names
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier()
			{
				@Override
				public boolean verify(String hostname, SSLSession session)
				{
					return true;
				}
			});
		}
		catch (GeneralSecurityException e)
		{
		}
	}
}
