package net.teamplace.android.http.teamdrive.activity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.NamedType;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import java.io.IOException;

/**
 * Created by jobol on 19/06/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ActivityResponse {
    @JsonProperty("value")
    public ActivityBase[] value;

    @JsonProperty("@odata.nextLink")
    public String nextLink;

    public static ActivityResponse createFromJson(String json) throws JsonParseException, JsonMappingException,
            IOException
    {
        return JsonUtils.getDefaultMapper().readValue(json, ActivityResponse.class);
    }
}
