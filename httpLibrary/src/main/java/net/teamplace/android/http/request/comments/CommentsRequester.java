package net.teamplace.android.http.request.comments;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import java.io.IOException;
import java.util.List;

public class CommentsRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public CommentsRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public List<Comment> getAllFileComments(String teamDriveId, String folder, String file, int commentCount,
			int commentOffset)
			throws TeamDriveNotFoundException, JsonParseException, JsonMappingException, XCBStatusException,
			HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		Uri uri = getAllFileCommentsUri(getServerForTeamDriveId(teamDriveId), folder, file, teamDriveId, commentCount,
				commentOffset);

		FileInfo fileInfo = FileInfo.createFileInfoFromJSON(executeStringGETRequest(uri, getMimeJsonHeader(), true,
				true));

		return fileInfo.getComments();
	}

	public List<Comment> addComment(String teamDriveId, String folder, String file, String comment)
			throws TeamDriveNotFoundException,
			XCBStatusException, HttpResponseException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException,
			IOException, RedirectException {
		byte[] data = new AddCommentCommand(folder, file, teamDriveId, comment).getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return Comment.createCommentArrayListFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	public void deleteComment(String teamDriveId, String commentId) throws TeamDriveNotFoundException,
			XCBStatusException, HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		Uri uri = getDeleteCommentUri(getServerForTeamDriveId(teamDriveId), commentId);

		executeFireAndForgetGetRequest(uri, null, true, true);
	}

	public List<Comment> modifyComment(String teamDriveId, String commentId, String comment)
			throws TeamDriveNotFoundException, XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		byte[] data = new ModifyCommentCommand(commentId, comment).getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return Comment.createCommentArrayListFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	public ChangedCommentResponse getChangedComments(String teamDriveId, String folder, String file, long since,
			List<String> commentIds)
			throws TeamDriveNotFoundException, XCBStatusException, HttpResponseException, ConnectFailedException,
			LogonFailedException,
			NotificationException, ServerLicenseException, IOException, RedirectException {
		byte[] data = new ChangedCommentCommand(teamDriveId, folder, file, since, commentIds).getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return ChangedCommentResponse.createChangedCommentResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	private Uri getAllFileCommentsUri(String server, String folder, String file, String teamDriveId, int commentCount,
			int commentOffset)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();

		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_FSTAT, file);
		builder.appendQueryParameter(PARAM_FOLDER, folder);
		builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		builder.appendQueryParameter(PARAM_ENUMERATE_COMMENTS, Integer.toString(commentCount));
		builder.appendQueryParameter(PARAM_START, Integer.toString(commentOffset));

		return builder.build();
	}

	private Uri getDeleteCommentUri(String server, String commentId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();

		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_COMMENT_ID, commentId);

		return builder.build();
	}

}
