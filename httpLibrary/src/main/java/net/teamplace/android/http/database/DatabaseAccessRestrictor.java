package net.teamplace.android.http.database;

/**
 * Created by jobol on 09/06/15.
 */
public interface DatabaseAccessRestrictor {
    public boolean isLocked();
}
