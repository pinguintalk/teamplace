package net.teamplace.android.http.request.teamdrive;

import android.content.Context;
import android.net.Uri;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.teamdrive.activity.ActivityResponse;

import java.io.IOException;
import java.util.Locale;

/**
 * Created by jobol on 16/06/15.
 */
public class ActivityRequester extends BasicRequester {

    private static final String PATH_ACTIVITY = "Activities";
    private static final String PARAM_COUNT = "$count";
    private static final String PARAM_EXPAND = "$expand";
    private static final String PARAM_FILTER = "$filter";
    private static final String PARAM_ORDERBY = "$orderby";

    private static final String VALUE_RELATED_FILE = "RelatedFile";
    private static final String VALUE_TIMESTAMP_DESC ="TimeStamp desc";
    private static final String VALUE_FILTER_TEMPLATE_NO_SUBACTIVITIES = "ParentActivityId eq null and TeamdriveId eq %s";
    private static final String VALUE_FILTER_TEMPLATE_ALL = "TeamdriveId eq %s";

    public ActivityRequester(Context context, Session session, BackendConfiguration backendConfiguration) {
        super(context, session, backendConfiguration);
    }

//    https:/api.teamplace.net/activity/v1/Activities?%24count=true&%24expand=RelatedFile&%24filter=ParentActivityId%20eq%20null%20and%20TeamdriveId%20eq%205e7266b3-0cb6-4a6f-bb62-e549e4b89264&%24orderby=TimeStamp%20desc
    public ActivityResponse getActivities(String driveId) throws ServerLicenseException, ConnectFailedException,
            IOException, LogonFailedException, HttpResponseException, NotificationException, XCBStatusException, RedirectException {
        return executeRequest(getActivityUri(driveId));
    }

    public ActivityResponse getMoreActivities(String nextLink) throws HttpResponseException, ServerLicenseException,
            XCBStatusException, IOException, ConnectFailedException, LogonFailedException, NotificationException, RedirectException {
        return executeRequest(Uri.parse(nextLink));
    }

    private ActivityResponse executeRequest(Uri uri) throws ServerLicenseException, ConnectFailedException, IOException,
            LogonFailedException, HttpResponseException, NotificationException, XCBStatusException, RedirectException {
        return ActivityResponse.createFromJson(executeStringGETRequest(uri, null, true, true));
    }

    private Uri getActivityUri(String driveId) {
        Uri.Builder builder = Uri.parse(backendConfiguration.activityApi()).buildUpon();
        builder.appendEncodedPath(PATH_ACTIVITY)
                .appendQueryParameter(PARAM_COUNT, String.valueOf(true))
                .appendQueryParameter(PARAM_EXPAND, VALUE_RELATED_FILE)
                .appendQueryParameter(PARAM_FILTER, String.format(Locale.US, VALUE_FILTER_TEMPLATE_NO_SUBACTIVITIES, driveId))
                .appendQueryParameter(PARAM_ORDERBY, VALUE_TIMESTAMP_DESC);
        return builder.build();
    }
}

