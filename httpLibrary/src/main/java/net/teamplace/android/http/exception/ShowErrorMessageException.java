package net.teamplace.android.http.exception;

public class ShowErrorMessageException extends CortadoException
{
	private static final long serialVersionUID = -8923998682571739684L;

	public ShowErrorMessageException(String strError)
	{
		super(strError);
	}
}
