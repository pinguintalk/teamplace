package net.teamplace.android.http.request.preview;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.request.PreviewInputStream;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.util.CloseableConnection;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class PreviewRequester extends BasicRequester
{
	public static final int XCB_STATUS_ERR_PAGE_NUMBER_TOO_LARGE = 2;
	public static final int XCB_STATUS_ERR_PREV_UNFINISHED = 880;

	private static final String TAG = PreviewRequester.class.getSimpleName();

	public PreviewRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public InputStream getImagePreviewInputStream(String teamDriveId, String file, String folder, int page, int xRes,
			int yRes, int qlty, int flags)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException,
			ServerLicenseException, IOException, TeamDriveNotFoundException, RedirectException {
		// HttpURLConnection conn = null;
		// PreviewInputStream previewInputStream = null;
		// try
		// {
		// List<NameValuePair> properties = new LinkedList<NameValuePair>();
		// properties.add(new BasicNameValuePair(HEADER_ACCEPT, ""));
		// Uri uri = getPreviewUri(teamDriveId, file, folder, page, xRes, yRes, qlty, flags);
		// conn = connectAndExecuteGETRequest(uri, properties, true, true);
		//
		// int resp_xRes = conn.getHeaderFieldInt(RESPONSE_PREVIEW_XRES, -1);
		// int resp_yRes = conn.getHeaderFieldInt(RESPONSE_PREVIEW_YRES, -1);
		// int resp_xResMax = conn.getHeaderFieldInt(RESPONSE_PREVIEW_MAXXRES, -1);
		// int resp_yResMax = conn.getHeaderFieldInt(RESPONSE_PREVIEW_MAXYRES, -1);
		// int resp_pageCount = conn.getHeaderFieldInt(RESPONSE_PREVIEW_PAGECOUNT, -1);
		//
		// previewInputStream = new PreviewInputStream(new CloseableConnection(conn), conn.getInputStream(),
		// conn.getContentType(), resp_pageCount,
		// resp_xRes, resp_yRes, resp_xResMax, resp_yResMax);
		// }
		// finally
		// {
		// if (previewInputStream == null)
		// {
		// disconnect(conn);
		// }
		// }
		// return previewInputStream;
		Uri uri = getPreviewUri(teamDriveId, file, folder, page, xRes, yRes, qlty, flags);
		return getImagePreviewInputStream(uri);
	}

	public InputStream getImagePreviewInputStream(Uri uri)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		PreviewInputStream previewInputStream = null;
		try
		{
			List<NameValuePair> properties = new LinkedList<NameValuePair>();
			properties.add(new BasicNameValuePair(HEADER_ACCEPT, ""));
			conn = connectAndExecuteGETRequest(uri, properties, true, true);

			int resp_xRes = conn.getHeaderFieldInt(RESPONSE_PREVIEW_XRES, -1);
			int resp_yRes = conn.getHeaderFieldInt(RESPONSE_PREVIEW_YRES, -1);
			int resp_xResMax = conn.getHeaderFieldInt(RESPONSE_PREVIEW_MAXXRES, -1);
			int resp_yResMax = conn.getHeaderFieldInt(RESPONSE_PREVIEW_MAXYRES, -1);
			int resp_pageCount = conn.getHeaderFieldInt(RESPONSE_PREVIEW_PAGECOUNT, -1);

			previewInputStream = new PreviewInputStream(new CloseableConnection(conn), conn.getInputStream(),
					conn.getContentType(), resp_pageCount,
					resp_xRes, resp_yRes, resp_xResMax, resp_yResMax);
		}
		finally
		{
			if (previewInputStream == null)
			{
				disconnect(conn);
			}
		}
		return previewInputStream;
	}

	public Uri getPreviewUri(String teamDriveId, String file, String folder, int page, int xRes, int yRes, int qlty,
			int flags)
			throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();
		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_PREVIEW_FILE, file);
		builder.appendQueryParameter(PARAM_PREVIEW_FOLDER, folder);
		builder.appendQueryParameter(PARAM_PREVIEW_PAGENO, Integer.toString(page));
		builder.appendQueryParameter(PARAM_PREVIEW_XRES, Integer.toString(xRes));
		builder.appendQueryParameter(PARAM_PREVIEW_YRES, Integer.toString(yRes));
		builder.appendQueryParameter(PARAM_PREVIEW_FLAGS, Integer.toString(flags));
		builder.appendQueryParameter(PARAM_PREVIEW_QLTY, Integer.toString(qlty));

		/** tpm=3 is required in a preview request, from empirical tests @author Gil Vegliach */
		builder.appendQueryParameter(PARAM_PREVIEW_TPM, String.valueOf(3));

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}
		return builder.build();
	}
}