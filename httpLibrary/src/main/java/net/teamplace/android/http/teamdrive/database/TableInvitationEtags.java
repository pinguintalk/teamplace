package net.teamplace.android.http.teamdrive.database;

import android.database.sqlite.SQLiteDatabase;

public class TableInvitationEtags
{
	public static final String TABLE = "table_invitation_etags";

	public static final String COLUMN_INVITATION_ID = "ref_etag_invitation_id";
	public static final String COLUMN_ETAG = "etag";

	public static final String ID_ETAG_ALL_INVITATIONS = "etag for all invitations";

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " ("
			+ COLUMN_INVITATION_ID + " REFERENCES " + TableInvitations.TABLE + "(" + TableInvitations.COLUMN_ID + "), "
			+ COLUMN_ETAG + " TEXT, PRIMARY KEY ("
			+ COLUMN_INVITATION_ID + "));";

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableInvitationEtags.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}
}
