package net.teamplace.android.http.request.share;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.util.FilePathHelper;

import android.util.Base64;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties({"password", "validuntil"})
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class ShareParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_TEAM)
	private String team;

	@JsonProperty(PARAM_CHECK_FILE)
	private String filePath;

	@JsonProperty(PARAM_SEND_TO)
	private String recipients;

	@JsonProperty(PARAM_PREVIEW_FILE)
	private boolean preview;

	@JsonProperty(PARAM_HTML)
	private boolean html;

	@JsonProperty(PARAM_PUBLIC)
	private boolean isPublic;

	@JsonProperty(PARAM_PASSWORD)
	private String password;

	@JsonProperty(PARAM_USER_TEXT)
	private String userText;

	@JsonProperty(PARAM_SUBJECT)
	private String subject;

	@JsonProperty(PARAM_VALID_UNTIL)
	private String validUntil;

	public ShareParameter(String teamDriveId, String filePath, String recipients, boolean preview, boolean html,
			boolean isPublic, String password, String userText, String subject, long validUntil)
	{
		this.team = teamDriveId;
		this.filePath = FilePathHelper.buildWindowsPath(filePath, false, false);
		this.recipients = recipients;
		this.preview = preview;
		this.html = html;
		this.isPublic = isPublic;
		this.password = password;
		this.userText = Base64.encodeToString(userText.getBytes(), Base64.NO_WRAP);
		this.subject = Base64.encodeToString(subject.getBytes(), Base64.NO_WRAP);
		this.validUntil = getFormattedValidUntilString(validUntil);
	}

	@JsonIgnore
	private String getFormattedValidUntilString(long validUntil)
	{
		String format = "yyyy-MM-dd HH:mm:ss.SSS";

		SimpleDateFormat formatter = new SimpleDateFormat(format);
		formatter.setTimeZone(TimeZone.getTimeZone("UTC"));

		return formatter.format(new Date(validUntil));
	}
}
