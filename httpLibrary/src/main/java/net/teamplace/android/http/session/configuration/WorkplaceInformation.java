package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class WorkplaceInformation
{
	private static final String COL_DISK_QUOTA_FREE = "diskQuotaFree";
	private static final String COL_DISK_QUOTA_USED = "diskQuotaUsed";
	private static final String COL_DISK_QUOTA_LIMIT = "diskQuotaLimit";
	private static final String COL_HELP = "help";
	private static final String COL_CONF = "conf";
	private static final String COL_UPDT = "updt";
	private static final String COL_AD = "ad";
	private static final String COL_NEWS = "news";
	private static final String COL_CONNNECTED_PRINTER_SVC = "connnectedPrinterSVC";
	private static final String COL_TOKEN_HOST = "tokenHost";
	private static final String COL_C_CENTRAL_HOST = "CCentralHost";

	public static final String TABLE_PART = COL_DISK_QUOTA_FREE + " integer, " + COL_DISK_QUOTA_USED + " integer, "
			+ COL_DISK_QUOTA_LIMIT + " integer, " + COL_HELP + " text, " + COL_CONF + " text, " + COL_UPDT + " text, "
			+ COL_AD + " text, " + COL_NEWS + " text, " + COL_CONNNECTED_PRINTER_SVC + " text, " + COL_TOKEN_HOST
			+ " text, " + COL_C_CENTRAL_HOST + " text";

	@JsonProperty("DiskQuotaFree")
	private long diskQuotaFree = -1;

	@JsonProperty("DiskQuotaUsed")
	private long diskQuotaUsed = -1;

	@JsonProperty("DiskQuotaLimit")
	private long diskQuotaLimit = -1;

	@JsonProperty("Help")
	private String helpUrl;

	@JsonProperty("CONF")
	private String configurationSiteUrl;

	@JsonProperty("UPDT")
	private String updateSiteUrl;

	@JsonProperty("AD")
	private String adServerUrl;

	@JsonProperty("NEWS")
	private String newsSiteUrl;

	@JsonProperty("CONNECTEDPRINTERSVC")
	private String connectedPrinterDirectoryUrl;

	@JsonProperty("TOKENHOST")
	private String tokenHostUrl;

	@JsonProperty("CCENTRALHOST")
	private String cloudCentralHostUrl;

	public WorkplaceInformation()
	{
	}

	public WorkplaceInformation(Cursor cursor)
	{
		if (!cursor.isNull(cursor.getColumnIndex(COL_DISK_QUOTA_FREE)))
		{
			diskQuotaFree = cursor.getLong(cursor.getColumnIndex(COL_DISK_QUOTA_FREE));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_DISK_QUOTA_USED)))
		{
			diskQuotaUsed = cursor.getLong(cursor.getColumnIndex(COL_DISK_QUOTA_USED));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_DISK_QUOTA_LIMIT)))
		{
			diskQuotaLimit = cursor.getLong(cursor.getColumnIndex(COL_DISK_QUOTA_LIMIT));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_HELP)))
		{
			helpUrl = cursor.getString(cursor.getColumnIndex(COL_HELP));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_CONF)))
		{
			configurationSiteUrl = cursor.getString(cursor.getColumnIndex(COL_CONF));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_UPDT)))
		{
			updateSiteUrl = cursor.getString(cursor.getColumnIndex(COL_UPDT));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_AD)))
		{
			adServerUrl = cursor.getString(cursor.getColumnIndex(COL_AD));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_NEWS)))
		{
			newsSiteUrl = cursor.getString(cursor.getColumnIndex(COL_NEWS));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_CONNNECTED_PRINTER_SVC)))
		{
			connectedPrinterDirectoryUrl = cursor.getString(cursor.getColumnIndex(COL_CONNNECTED_PRINTER_SVC));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_TOKEN_HOST)))
		{
			tokenHostUrl = cursor.getString(cursor.getColumnIndex(COL_TOKEN_HOST));
		}

		if (!cursor.isNull(cursor.getColumnIndex(COL_C_CENTRAL_HOST)))
		{
			cloudCentralHostUrl = cursor.getString(cursor.getColumnIndex(COL_C_CENTRAL_HOST));
		}
	}

	public long getDiskQuotaFree()
	{
		return diskQuotaFree;
	}

	public void setDiskQuotaFree(long diskQuotaFree)
	{
		this.diskQuotaFree = diskQuotaFree;
	}

	public long getDiskQuotaUsed()
	{
		return diskQuotaUsed;
	}

	public void setDiskQuotaUsed(long diskQuotaUsed)
	{
		this.diskQuotaUsed = diskQuotaUsed;
	}

	public long getDiskQuotaLimit()
	{
		return diskQuotaLimit;
	}

	public void setDiskQuotaLimit(long diskQuotaLimit)
	{
		this.diskQuotaLimit = diskQuotaLimit;
	}

	public String getHelpUrl()
	{
		return helpUrl;
	}

	public void setHelpUrl(String helpUrl)
	{
		this.helpUrl = helpUrl;
	}

	public String getConfigurationSiteUrl()
	{
		return configurationSiteUrl;
	}

	public void setConfigurationSiteUrl(String configurationSiteUrl)
	{
		this.configurationSiteUrl = configurationSiteUrl;
	}

	public String getUpdateSiteUrl()
	{
		return updateSiteUrl;
	}

	public void setUpdateSiteUrl(String updateSiteUrl)
	{
		this.updateSiteUrl = updateSiteUrl;
	}

	public String getAdServerUrl()
	{
		return adServerUrl;
	}

	public void setAdServerUrl(String adServerUrl)
	{
		this.adServerUrl = adServerUrl;
	}

	public String getNewsSiteUrl()
	{
		return newsSiteUrl;
	}

	public void setNewsSiteUrl(String newsSiteUrl)
	{
		this.newsSiteUrl = newsSiteUrl;
	}

	public String getConnectedPrinterDirectoryUrl()
	{
		return connectedPrinterDirectoryUrl;
	}

	public void setConnectedPrinterDirectoryUrl(String connectedPrinterDirectoryUrl)
	{
		this.connectedPrinterDirectoryUrl = connectedPrinterDirectoryUrl;
	}

	public String getTokenHostUrl()
	{
		return tokenHostUrl;
	}

	public void setTokenHostUrl(String tokenHostUrl)
	{
		this.tokenHostUrl = tokenHostUrl;
	}

	public String getCloudCentralHostUrl()
	{
		return cloudCentralHostUrl;
	}

	public void setCloudCentralHostUrl(String cloudCentralHostUrl)
	{
		this.cloudCentralHostUrl = cloudCentralHostUrl;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_DISK_QUOTA_FREE, diskQuotaFree);
		values.put(COL_DISK_QUOTA_USED, diskQuotaUsed);
		values.put(COL_DISK_QUOTA_LIMIT, diskQuotaLimit);
		values.put(COL_HELP, helpUrl);
		values.put(COL_CONF, configurationSiteUrl);
		values.put(COL_UPDT, updateSiteUrl);
		values.put(COL_AD, adServerUrl);
		values.put(COL_NEWS, newsSiteUrl);
		values.put(COL_CONNNECTED_PRINTER_SVC, connectedPrinterDirectoryUrl);
		values.put(COL_TOKEN_HOST, tokenHostUrl);
		values.put(COL_C_CENTRAL_HOST, cloudCentralHostUrl);

		return values;
	}
}
