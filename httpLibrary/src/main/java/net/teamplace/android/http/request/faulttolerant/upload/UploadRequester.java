package net.teamplace.android.http.request.faulttolerant.upload;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;

import net.teamplace.android.http.exception.ClosedByUserException;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.multipart.MultipartFile;
import net.teamplace.android.http.multipart.MultipartInputStream;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.request.ConnectionErrorHandler;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.download.UpDownloadSQLiteHelper;
import net.teamplace.android.http.session.Session;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class UploadRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public UploadRequester(Context context, Session session, BackendConfiguration backendConfiguration, NetworkUpdateCallback networkUpdateCallback)
	{
		super(context, session, backendConfiguration);
	}

	public void upload(String teamDriveId, MultipartFile file, UploadStatus status) throws ConnectFailedException,
			XCBStatusException,
			HttpResponseException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException,
			IOException, TeamDriveNotFoundException, RedirectException {
		HttpURLConnection connection = null;
		MultipartInputStream is = null;
		OutputStream os = null;
		TimeoutTimerTask ttt = null;

		try
		{
			configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration);

			while (file.hasNextChunk())
			{
				is = file.getNextChunk();

				Uri uri = getPrintUploadUri(getServerForTeamDriveId(teamDriveId), teamDriveId);

				int contentLength = (int) is.length();
				connection = connectAndExecutePOSTRequest(uri, getMultipartHeader(contentLength), contentLength, false);

				ttt = new TimeoutTimerTask(connection);
				new Timer().scheduleAtFixedRate(ttt, TIMEOUT, TIMEOUT);

				try
				{
					os = connection.getOutputStream();
					writeToOutputStream(is, os, ttt);

					ConnectionErrorHandler.handleConnectionStatus(connection);
					showResponseHeader(connection);

					tokenManager.updateTokenFromResponse(context, session, connection);

					configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration, connection);
				}
				catch (IOException e)
				{
					try
					{
						is.forceClose();
					}
					catch (IOException e1)
					{
						e1.printStackTrace();
					}

					if (e instanceof ClosedByUserException)
					{
						throw (ClosedByUserException) e;
					}
					else
					{
						throw new ConnectFailedException(e);
					}
				}
			}
		}
		finally
		{
			if (file.hasNextChunk())
			{
				Log.d(TAG, "File was not completely uploaded");
				insertOrUpdateUploadStatus(status);
			}
			else
			{
				Log.d(TAG, "Delete upload status from db");
				deleteUploadStatus(status);
			}

			if (ttt != null)
			{
				ttt.cancel();
			}

			closeStream(is);
			closeStream(os);
			disconnect(connection);
		}
	}

	public String getTransactionId(String teamDriveId, String folder, String fileName, int forceOverwrite)
			throws TeamDriveNotFoundException, XCBStatusException, HttpResponseException, JsonParseException,
			JsonMappingException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		Uri uri = getGetTransactionUri(getServerForTeamDriveId(teamDriveId), folder, fileName, forceOverwrite,
				teamDriveId);

		return executeSingleHeaderFielGETRequest(uri, null, true, true, HEADER_TRANSACTION_ID);
	}

	public long getAlreadyUploadedBytes(String teamDriveId, String transactionId) throws TeamDriveNotFoundException,
			XCBStatusException,
			HttpResponseException, JsonParseException, JsonMappingException, ConnectFailedException,
			LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		Uri uri = getAlreadyUploadedBytesUri(getServerForTeamDriveId(teamDriveId), transactionId);

		String bytesCount = "0";

		try
		{
			bytesCount = executeSingleHeaderFielGETRequest(uri, null, true, true, HEADER_FRAGMENT_SIZE);
		}
		catch (XCBStatusException e)
		{
			if (e.getStatusCode() == 2)
			{
				return 0;
			}
			else
			{
				throw e;
			}
		}

		return Long.parseLong(bytesCount);
	}

	private List<NameValuePair> getMultipartHeader(int contentLength)
	{
		List<NameValuePair> properties = new LinkedList<NameValuePair>();

		properties.add(new BasicNameValuePair(HEADER_CONTENT_TYPE, PREFIX_MULTIPART_WITH_BOUNDARY
				+ MultipartFile.BOUNDARY_HEADER));
		properties.add(new BasicNameValuePair(HEADER_CONTENT_ENCODING, ENCODING_UTF8));
		properties.add(new BasicNameValuePair(HEADER_CONTENT_LENGTH, Integer.toString(contentLength)));

		return properties;
	}

	private Uri getPrintUploadUri(String server, String teamDriveId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon().appendEncodedPath(PATH_SEND);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		return builder.build();
	}

	private Uri getGetTransactionUri(String server, String folder, String fileName, int forceOverwrite,
			String teamDriveId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();

		builder.appendEncodedPath(PATH_TRANSACTION_ID);
		builder.appendQueryParameter(PARAM_DEST_FOLDER, folder);
		builder.appendQueryParameter(PARAM_DEST_FILE, fileName);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		if (forceOverwrite == OVERWRITE_FORCE || forceOverwrite == OVERWRITE_NEW_NAME
				|| forceOverwrite == OVERWRITE_NEW_VERSION)
		{
			builder.appendQueryParameter(PARAM_FORCE_OVERWRITE, Integer.toString(forceOverwrite));
		}

		return builder.build();
	}

	private Uri getAlreadyUploadedBytesUri(String server, String transactionId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();

		builder.appendEncodedPath(PATH_TRANSACTION_ID);
		builder.appendQueryParameter(PARAM_TRANSACTION_ID, transactionId);

		return builder.build();
	}

	private void insertOrUpdateUploadStatus(UploadStatus status)
	{
		UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
		SQLiteDatabase database = databaseHelper.getWritableDatabase();

		Cursor cursor = database.query(UploadStatus.TABLE_NAME, null, UploadStatus.COL_TRANSACTION_ID + "= ?",
				new String[] {status.getTransactionId()}, null, null, null);

		try
		{
			if (cursor.moveToFirst())
			{
				database.update(UploadStatus.TABLE_NAME, status.getAsContentValues(), UploadStatus.COL_TRANSACTION_ID
						+ "= ?",
						new String[] {status.getTransactionId()});
			}
			else
			{
				database.insert(UploadStatus.TABLE_NAME, null, status.getAsContentValues());
			}
		}
		finally
		{
			cursor.close();
			database.close();
		}
	}

	private void deleteUploadStatus(UploadStatus status)
	{
		UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
		SQLiteDatabase database = databaseHelper.getWritableDatabase();

		database.delete(UploadStatus.TABLE_NAME, UploadStatus.COL_TRANSACTION_ID + "= ?",
				new String[] {status.getTransactionId()});
		database.close();
	}
}
