package net.teamplace.android.http.session.database;

import android.accounts.AccountManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.util.Pair;

import net.teamplace.android.http.database.DatabaseAccessRestrictor;
import net.teamplace.android.http.database.DatabaseActions;
import net.teamplace.android.http.database.DatabaseLockedException;
import net.teamplace.android.http.session.Credentials;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.configuration.Configuration;
import net.teamplace.android.http.session.configuration.FolderType;
import net.teamplace.android.http.session.configuration.ShellObject;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Created by jobol on 10/06/15.
 */
public class SessionDatabaseActions extends DatabaseActions {

    private static SessionDatabaseActions instance;

    private Context mContext;

    private SessionDatabaseActions(Context context, DatabaseAccessRestrictor restrictor) {
        super(new SessionSQLiteHelper(context), restrictor);
        mContext = context;
    }

    public static synchronized SessionDatabaseActions getInstance() {
        if (instance == null) {
            throw new IllegalStateException("not initialized");
        }
        return instance;
    }

    public static synchronized void init(Context context, DatabaseAccessRestrictor restrictor) {
        if (instance != null) {
            throw new IllegalStateException("already initialized");
        }
        instance = new SessionDatabaseActions(context, restrictor);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public synchronized long insert(final String table, final ContentValues values)
            throws DatabaseLockedException {
        final AtomicLong id = new AtomicLong(-1l);
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() {
                id.set(mDb.insert(table, null, values));
            }
        });
        return id.get();
    }

    public synchronized int update(final String table, final ContentValues values,
                             final String whereClause, final String[] whereArgs) throws DatabaseLockedException {
        final AtomicInteger rows = new AtomicInteger(0);
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() {
                rows.set(mDb.update(table, values, whereClause, whereArgs));
            }
        });
        return rows.get();
    }

    public synchronized long delete(final String table, final String whereClause, final String[] whereArgs) throws DatabaseLockedException {
        final AtomicLong rows = new AtomicLong(0);
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() {
                rows.set(mDb.delete(table, whereClause, whereArgs));
            }
        });
        return rows.get();
    }

    public synchronized void deleteAllRowsInDatabase() {
        try {
            mDb.beginTransaction();
            mDb.delete(Session.TABLE_NAME, null, null);
            mDb.delete(Configuration.TABLE_NAME, null, null);
            mDb.delete(ShellObject.TABLE_NAME_SHELL_TYPE, null, null);
            mDb.delete(ShellObject.TABLE_NAME_MAPPING, null, null);
            mDb.delete(FolderType.TABLE_NAME, null, null);
            mDb.setTransactionSuccessful();
        } finally {
            mDb.endTransaction();
        }
    }

    public synchronized boolean killSessionFromDatabase() throws DatabaseLockedException {
        final AtomicBoolean success = new AtomicBoolean(false);
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() {
                Cursor cursor = mDb.query(Session.TABLE_NAME, null, null, null, null, null, null);

                if (cursor.moveToFirst()) {
                    int i = 0;
                    int position = 0;
                    long timestamp = 0;
                    long cursorTimestamp;

                    do {
                        cursorTimestamp = cursor.getLong(cursor.getColumnIndex(Session.COL_TIMESTAP));

                        if (cursorTimestamp > timestamp) {
                            timestamp = cursorTimestamp;
                            position = i;
                        }
                        i++;
                    }
                    while (cursor.moveToNext());

                    if (cursor.moveToPosition(position)) {
                        int iSessionId = cursor.getInt(cursor.getColumnIndex(SessionSQLiteHelper.COLUMN_ID));
                        int iConfId = cursor.getInt(cursor.getColumnIndex(Session.COL_CONFIGURATION_ID));

                        int iSuccess = mDb.delete(Session.TABLE_NAME, SessionSQLiteHelper.COLUMN_ID + "= ?",
                                new String[]

                                        {Integer.toString(iSessionId)});

                        iSuccess = mDb.delete(Configuration.TABLE_NAME, SessionSQLiteHelper.COLUMN_ID + "= ?",
                                new String[]
                                        {Integer.toString(iConfId)});
                        cursor.close();
                        success.set(true);
                        return;
                    }

                }
                cursor.close();
            }
        });
        return false;
    }

    public synchronized Configuration loadConfiguration(final long configurationID) throws DatabaseLockedException {
        final AtomicReference<Configuration> configRef = new AtomicReference<>();
        executeLockable(new DatabaseAction() {
            @Override
            public void execute() {
                Cursor cursor = mDb.query(Configuration.TABLE_NAME, null, SessionSQLiteHelper.COLUMN_ID + "= ?",
                        new String[]
                                {Long.toString(configurationID)}, null, null, null);
                Configuration configuration = null;
                if (cursor.moveToFirst()) {
                    configuration = new Configuration(cursor);
                    configuration.setShell(loadShellObjectFromDatabase(configurationID));
                    configuration.setFolders(loadFolderTypesFromDatabase(configurationID));
                }
                cursor.close();
                configRef.set(configuration);
            }
        });

        return configRef.get();
    }

    public synchronized Session getSessionFromDatabase() throws DatabaseLockedException {
        Session session = loadSessionFromDatabase();

        if (session == null)
        {
            return null;
        }

        Configuration configuration = loadConfiguration(session.getConfigurationID());

        if (configuration == null)
        {
            return null;
        }

        session.setConfiguration(configuration);
        session.updateConfigurationID();
        TeamDriveDatabaseActions dbActions = TeamDriveDatabaseActions.getInstance();
        session.setTeamDrives(dbActions.getAllTeamDrives());
        Pair<GlobalConfiguration, String> pair = dbActions.getGlobalConfigurationAndEtag();
        session.setTeamDriveGlobalConfig(pair != null ? pair.first : null);

        return session;
    }

    public synchronized void saveSessionInDatabase(final Session session) throws DatabaseLockedException {
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() throws DatabaseLockedException {
                session.updateTimestamp();
                session.updateConfigurationID();

                long sessionId = session.getID();

                ContentValues sessionContentValues = session.getAsContentValues();

                if (sessionId == -1)
                {
                    session.setID(insert(Session.TABLE_NAME, sessionContentValues));
                }
                else
                {
                    update(Session.TABLE_NAME, sessionContentValues,
                            SessionSQLiteHelper.COLUMN_ID + "= ?",
                            new String[]{Long.toString(sessionId)});
                }
            }
        });
    }

    public void saveConfigurationInDatabase(final Configuration configuration) throws DatabaseLockedException {
        executeInTransaction(new DatabaseAction() {
            @Override
            public void execute() throws DatabaseLockedException {
                if (configuration != null)
                {
                    long configurationId = configuration.getId();
                    ContentValues configurationValues = configuration.getAsContentValues();

                    if (configurationId == -1)
                    {
                        configuration
                                .setId(insert(Configuration.TABLE_NAME, configurationValues));

                        saveShellObjectsInDatabase(configuration.getId(), configuration.getShell());
                        saveFolderTypesInDatabase(configurationId, configuration.getFolders());
                    }
                    else
                    {
                        update(Configuration.TABLE_NAME, configurationValues,
                                SessionSQLiteHelper.COLUMN_ID + "= ?",
                                new String[]{Long.toString(configurationId)});

                        updateShellObjectsInDatabase(configurationId, configuration.getShell());
                        updateFolderTypesInDatabase(configurationId, configuration.getFolders());
                    }
                }
            }
        });

    }

    private Session loadSessionFromDatabase()
    {
        Cursor cursor = mDb.query(Session.TABLE_NAME, null, null, null, null, null, null);

        if (cursor.moveToFirst())
        {
            int i = 0;
            int position = 0;
            long timestamp = 0;
            long cursorTimestamp;

            do
            {
                cursorTimestamp = cursor.getLong(cursor.getColumnIndex(Session.COL_TIMESTAP));

                if (cursorTimestamp > timestamp)
                {
                    timestamp = cursorTimestamp;
                    position = i;
                }

                i++;
            }
            while (cursor.moveToNext());

            if (cursor.moveToPosition(position))
            {
                Session session = new Session(mContext, cursor);
                cursor.close();

                Credentials credentials = new Credentials(session.getAccount().name, AccountManager.get(mContext)
                        .getPassword(session.getAccount()));
                session.setCredentials(credentials);

                return session;
            }
        }

        return null;
    }

    private ShellObject[] loadShellObjectFromDatabase(long configurationID)
    {
        List<ShellObject> shellObjectList = new ArrayList<ShellObject>();

        Cursor shellTypeCursor = mDb.query(ShellObject.TABLE_NAME_SHELL_TYPE, null,
                ShellObject.COL_CONFIGURATION_ID
                        + "= ?", new String[] {Long.toString(configurationID)}, null, null, null);
        if (shellTypeCursor.moveToFirst())
        {
            int shellId;
            String shellType;
            Cursor mappingCursor;
            do
            {
                shellId = shellTypeCursor.getInt(shellTypeCursor.getColumnIndex(SessionSQLiteHelper.COLUMN_ID));
                shellType = shellTypeCursor.getString(shellTypeCursor.getColumnIndex(ShellObject.COL_SHELL_TYPE));

                mappingCursor = mDb.query(ShellObject.TABLE_NAME_MAPPING, null, ShellObject.COL_SHELL_TYPE_ID
                        + "= ?", new String[] {Integer.toString(shellId)}, null, null, null);

                shellObjectList.add(new ShellObject(shellType, mappingCursor));

                mappingCursor.close();
            }
            while (shellTypeCursor.moveToNext());
        }
        return shellObjectList.toArray(new ShellObject[shellObjectList.size()]);
    }

    private FolderType[] loadFolderTypesFromDatabase(long configurationID)
    {
        List<FolderType> list = new ArrayList<FolderType>();

        Cursor cursor = mDb.query(FolderType.TABLE_NAME, null, FolderType.COL_CONFIGURATION_ID + "= ?",
                new String[] {Long.toString(configurationID)}, null, null, null);

        if (cursor.moveToFirst())
        {
            do
            {
                list.add(new FolderType(cursor));
            }
            while (cursor.moveToNext());
        }

        return list.toArray(new FolderType[list.size()]);
    }

    private void saveShellObjectsInDatabase(long configurationId, ShellObject[] shellObjects) throws DatabaseLockedException {
        for (ShellObject object : shellObjects)
        {
            object.setConfigurationId(configurationId);
            object.setId(insert(ShellObject.TABLE_NAME_SHELL_TYPE,
                    object.getTypeAsContentValues()));

            for (ContentValues value : object.getMappingAsContentValues())
            {
                insert(ShellObject.TABLE_NAME_MAPPING, value);
            }
        }
    }

    private void updateShellObjectsInDatabase(long configurationId, ShellObject[] shellObjects) throws DatabaseLockedException {
        // SessionSQLiteHelper databaseHelper = new SessionSQLiteHelper(mContext);
        // SQLiteDatabase database = databaseHelper.getWritableDatabase();

        Cursor cursor = mDb.query(ShellObject.TABLE_NAME_SHELL_TYPE, null, ShellObject.COL_CONFIGURATION_ID
                        + "= ?", new String[]{Long.toString(configurationId)}, null, null,
                null);

        if (cursor.moveToFirst())
        {
            int shellId;

            do
            {
                shellId = cursor.getInt(cursor.getColumnIndex(SessionSQLiteHelper.COLUMN_ID));

                delete(ShellObject.TABLE_NAME_MAPPING, ShellObject.COL_SHELL_TYPE_ID
                        + "= ?", new String[]{Integer.toString(shellId)});

                delete(ShellObject.TABLE_NAME_SHELL_TYPE,
                        SessionSQLiteHelper.COLUMN_ID
                                + "= ?", new String[]{Integer.toString(shellId)});
            }
            while (cursor.moveToNext());
        }

        cursor.close();
        // database.close();

        saveShellObjectsInDatabase(configurationId, shellObjects);
    }

    private void saveFolderTypesInDatabase(long configurationId, FolderType[] folderTypes) throws DatabaseLockedException {
        for (FolderType folder : folderTypes)
        {
            folder.setConfigurationId(configurationId);
            insert(FolderType.TABLE_NAME, folder.getAsContentValues());
        }
    }

    private void updateFolderTypesInDatabase(long configurationId, FolderType[] folderTypes) throws DatabaseLockedException {
        delete(FolderType.TABLE_NAME, FolderType.COL_CONFIGURATION_ID + "= ?",
                new String[]{Long.toString(configurationId)});

        saveFolderTypesInDatabase(configurationId, folderTypes);
    }
}
