package net.teamplace.android.http.teamdrive;

import java.io.IOException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class User
{
	public static final int ROLE_READ_ONLY_MEMBER = 0;
	public static final int ROLE_MEMBER = 20;
	public static final int ROLE_MEMBER_INVITE = 30;
	public static final int ROLE_OWNER = 100;
	public static final int ROLE_ADMIN = 60;

	@JsonProperty("UserId")
	private String userId;

	@JsonProperty("Email")
	private String email;

	@JsonProperty("Displayname")
	private String displayName;

	@JsonProperty("AboutMe")
	private String aboutMe;

	@JsonProperty("ImgPath")
	private String imgPath;

	@JsonProperty("Role")
	private Integer role;

	public User()
	{
	}

	public User(String userId, String email, String displayName, String aboutMe, String imgPath,
			Integer role)
	{
		this.userId = userId;
		this.email = email;
		this.displayName = displayName;
		this.aboutMe = aboutMe;
		this.imgPath = imgPath;
		this.role = role;
	}

	public User(String email, Integer role)
	{
		this(null, email, null, null, null, role);
	}

	public static User createFromJson(String json) throws JsonParseException, JsonMappingException, IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, User.class);
	}

	public static User[] createArrayFromJson(String json) throws JsonParseException, JsonMappingException, IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, User[].class);
	}

	public static String createJsonObject(User... in) throws JsonProcessingException
	{
		return JsonUtils.getDefaultMapper().writeValueAsString(in.length > 1 ? in : in[0]);
	}

	public static String createJsonArray(User[] in) throws JsonProcessingException
	{
		return JsonUtils.getDefaultMapper().writeValueAsString(in);
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getAboutMe()
	{
		return aboutMe;
	}

	public void setAboutMe(String aboutMe)
	{
		this.aboutMe = aboutMe;
	}

	public String getImgPath()
	{
		return imgPath;
	}

	public void setImgPath(String imgPath)
	{
		this.imgPath = imgPath;
	}

	public Integer getRole()
	{
		return role;
	}

	public void setRole(Integer role)
	{
		this.role = role;
	}
}