package net.teamplace.android.http.session;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.Log;

public class UserAgent
{
	private static final String TAG = UserAgent.class.getSimpleName();

	public static final String DEVICE_NAME_REGEX = "[^a-zA-Z0-9\\/]";
	public static final String DEVICE_VERSION_REGEX = "[^a-zA-Z0-9\\.]";

	private static String mUserAgent;

	public static String getUserAgent(Context context)
	{
		if (mUserAgent == null)
		{
			mUserAgent = generateUserAgent(context);
		}

		return mUserAgent;
	}

	private static String generateUserAgent(Context context)
	{
		Log.d(TAG, "Generate UserAgent");

		StringBuilder userAgentBuilder = new StringBuilder();

		userAgentBuilder.append(getProductPrefix(context) + "/");
		userAgentBuilder.append(getApplicationVersion(context) + " (");
		userAgentBuilder.append(getManufacturer() + "_");
		userAgentBuilder.append(getDeviceName() + "_");
		userAgentBuilder.append(getOSVersion() + ", ");
		userAgentBuilder.append(getCountry());
		userAgentBuilder.append(getMCC(context) + ")");

		mUserAgent = userAgentBuilder.toString();

		return mUserAgent;
	}

	private static String getProductPrefix(Context context)
	{
		String packageName = context.getPackageName();

		if (packageName.equals("com.cortado.android.workplace") || packageName.equals("com.cortado.android.corporate"))
		{
			return "CBMClient";
		}

		return "CBMClient";
	}

	private static String getApplicationVersion(Context context)
	{
		try
		{
			return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
		}
		catch (NameNotFoundException e)
		{
			return "1.0.0.0";
		}
	}

	private static String getManufacturer()
	{
		return "Android";
	}

	private static String getDeviceName()
	{
		return Build.MODEL.trim().replaceAll(DEVICE_NAME_REGEX, "");
	}

	private static String getOSVersion()
	{
		return Build.VERSION.RELEASE.trim().replaceAll(DEVICE_VERSION_REGEX, "");
	}

	private static String getCountry()
	{
		String language = System.getProperty("user.language") != null ? System.getProperty("user.language") : "en";

		if (language.equals("ja")) // map "ja" to "jp" to match ressourceBase
		{
			language = "jp";
		}

		return language;
	}

	private static String getMCC(Context context)
	{
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

		String networkOperator = telephonyManager.getNetworkOperator();

		if (networkOperator != null && networkOperator.length() > 0)
		{
			return "/" + networkOperator.substring(0, 3);
		}

		return "";
	}
}