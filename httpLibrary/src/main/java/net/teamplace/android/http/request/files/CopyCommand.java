package net.teamplace.android.http.request.files;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JacksonDataModel
public class CopyCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private CopyParameter[] copyParameter;

	public CopyCommand(String teamDest, String teamSrc, String parentFolder, String[] copyFileOrFolder,
			String copyDestinationFolder, String destinationFileName, int forceOverwrite)
	{
		setCommand(CMD_COPY);

		List<CopyParameter> list = new ArrayList<CopyParameter>();

		for (String file : copyFileOrFolder)
		{
			list.add(new CopyParameter(teamDest, teamSrc, parentFolder, file, copyDestinationFolder, destinationFileName,
					forceOverwrite));
		}

		this.copyParameter = list.toArray(new CopyParameter[list.size()]);
	}
}
