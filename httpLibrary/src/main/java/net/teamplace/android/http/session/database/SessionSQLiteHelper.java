package net.teamplace.android.http.session.database;

import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.configuration.Configuration;
import net.teamplace.android.http.session.configuration.FolderType;
import net.teamplace.android.http.session.configuration.ShellObject;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SessionSQLiteHelper extends SQLiteOpenHelper
{
	private static final String DATABASE_NAME = "session.db";
	private static final int CURRENT_DATABASE_VERSION = 1;

	public static final String COLUMN_ID = "_id";

	SessionSQLiteHelper(Context context)
	{
		super(context, DATABASE_NAME, null, CURRENT_DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database)
	{
		database.execSQL(Session.CREATE_TABLE);
		database.execSQL(Configuration.CREATE_TABLE);
		database.execSQL(ShellObject.CREATE_TABLE_SHELL);
		database.execSQL(ShellObject.CREATE_TABLE_MAPPING);
		database.execSQL(FolderType.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
	}
}
