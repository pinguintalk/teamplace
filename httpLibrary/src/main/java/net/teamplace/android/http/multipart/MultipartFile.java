package net.teamplace.android.http.multipart;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import net.teamplace.android.http.request.faulttolerant.ProgressCallback;


/**
 * Wrapper class for HTTP uploads via multipart. Generates the content body for HTTP request.
 * 
 * @author jamic
 */
public abstract class MultipartFile
{
	protected static final String NEWLINE = "\r\n";
	protected static final String ENCODING = "utf-8";

	protected static final String CONTENT_TYPE = "Content-Type";
	protected static final String CONTENT_ENCODING = "Content-Encoding";
	protected static final String CONTENT_LENGTH = "Content-Length";

	protected static final String BOUNDARY = "----------------CEE79FBC5322EF9F";
	public static final String BOUNDARY_HEADER = "--------------CEE79FBC5322EF9F";
	protected static final String BOUND_STR = "----------------CEE79FBC5322EF9F\r\n";
	protected static String FINAL_BOUND_STR = NEWLINE + "----------------CEE79FBC5322EF9F--" + NEWLINE + NEWLINE;

	protected final String PARAM_GUI = "gui";
	protected final String PARAM_APPEND = "append";
	protected final String PARAM_DESTFILE = "fcpdst";
	protected final String PARAM_DESTPATH = "fldrdest";
	protected final String PARAM_FORCE_OVERWRITE = "fovr";
	protected final String PARAM_TRANSACTION_ID = "tid";

	public static int CHUNK_SIZE = 1024 * 1024 * 5; // 5 MB
	// public static int CHUNK_SIZE = 1024 * 512; // 500 KB
	// public static int CHUNK_SIZE = 1024 * 128; // 125 KB

	protected boolean isFirstChunk = true;

	protected String fileName;
	protected long fileSize;

	protected ChunkLimitedInputStream chunkedInputStream;

	private byte[] preContent;

	protected MultipartFile(InputStream fileInputStream, String fileName, long fileSize, long skipBytes)
			throws IOException
	{
		chunkedInputStream = new ChunkLimitedInputStream(fileInputStream, CHUNK_SIZE, (int) fileSize, skipBytes);
		this.fileName = fileName;
		this.fileSize = fileSize;
	}

	protected MultipartFile(File file, long skipBytes) throws IOException
	{
		this(new FileInputStream(file), file.getName(), file.length(), skipBytes);
	}

	public abstract String getFileNameForUpload();

	public void setCallback(ProgressCallback callback)
	{
		chunkedInputStream.setCallback(callback);
	}

	protected abstract void buildPreContent(StringBuilder strBuilder);

	public static byte[] getFinalBoundaryAsBytes()
	{
		return getStringAsBytes(FINAL_BOUND_STR, ENCODING);
	}

	public void closeFileAndCancelActions()
	{
		try
		{
			chunkedInputStream.closeInnerStream(true);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Add a new Comtent-Disposition Part for the given Key-Value-Pair to the request body.
	 */
	protected void addContentDispositionPart(String key, String value, StringBuilder strBuilder)
	{
		strBuilder.append(BOUND_STR);
		strBuilder.append("Content-Disposition: form-data; name=\"" + key + "\"\r\n");
		strBuilder.append(NEWLINE);
		strBuilder.append(value);
		strBuilder.append(NEWLINE);
	}

	private void addContentDispositionPartFilename(StringBuilder strBuilder)
	{
		strBuilder.append(BOUND_STR);
		strBuilder.append(("Content-Disposition: form-data; name=\"ATTFILE\"\r\n"));
		// strBuilder.append(("Content-Disposition: form-data; name=\"ATTFILE\"; filename=\"" + getFileNameForUpload() +
		// "\"\r\n"));
		strBuilder.append(NEWLINE);
	}

	protected void addAppendMode(StringBuilder strBuilder)
	{
		if (isFirstChunk)
		{
			if (chunkedInputStream.isLastChunk())
			{
				return;
			}
			else
			{
				isFirstChunk = false;
			}
		}

		if (!chunkedInputStream.isLastChunk())
		{
			addContentDispositionPart(PARAM_APPEND, "1", strBuilder);
		}
		else
		{
			addContentDispositionPart(PARAM_APPEND, "end", strBuilder);
		}
	}

	public static byte[] getStringAsBytes(String string, String encoding)
	{
		try
		{
			return string.getBytes(encoding);
		}
		catch (UnsupportedEncodingException e)
		{
			return string.getBytes();
		}
	}

	public String getFileName()
	{
		return fileName;
	}

	public long getRawFileSize()
	{
		return fileSize;
	}

	public boolean hasNextChunk()
	{
		return chunkedInputStream.hasNextChunk();
	}

	public MultipartInputStream getNextChunk()
	{
		StringBuilder strBuilder = new StringBuilder();

		buildPreContent(strBuilder);
		addContentDispositionPartFilename(strBuilder);

		String content = strBuilder.toString();

		System.out.println(content);

		preContent = getStringAsBytes(content, ENCODING);

		return new MultipartInputStream(preContent, chunkedInputStream);
	}
}