package net.teamplace.android.http.session.configuration;

import java.io.IOException;
import java.net.HttpURLConnection;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.request.account.TokenRequester;
import net.teamplace.android.http.request.configuration.ConfigurationRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;

import android.content.Context;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * <b>Class Overview</b>
 * <p>
 * This class handles the configuration management during the application runtime. Except the configuration (
 * {@link ConfigurationRequester}) and token ({@link TokenRequester}) request every request use this class to update the
 * configuration if needed.
 * 
 * @author jamic
 */
public class ConfigurationManager implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	private static ConfigurationManager instance;

	public static ConfigurationManager getInstance()
	{
		if (instance == null)
		{
			instance = new ConfigurationManager();
		}

		return instance;
	}

	/***
	 * This method checks if the current configuration is valid or not. If the configuration is invalid a new one will
	 * be requested and saved in current session.
	 * 
	 * @param context
	 * @param session
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws IOException
	 */
	synchronized public void updateConfigurationIfNeeded(Context context, Session session, BackendConfiguration backendConfiguration) throws XCBStatusException,
			HttpResponseException,
			JsonParseException, JsonMappingException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException,
			IOException, RedirectException {
		Log.d(TAG, "Check configuration validity");

		Configuration configuration = session.getConfiguration();

		if (!configuration.isValid())
		{
			Log.d(TAG, "Configuration invalid");

			SessionManager sessionManager = new SessionManager(context);
			ConfigurationRequester requester = new ConfigurationRequester(context, session, backendConfiguration);

			session.setConfiguration(requester.getConfiguration());

			sessionManager.saveSession(session);
		}
		else
		{
			Log.d(TAG, "Configuration is valid");
		}

		Log.d(TAG, "Done");
	}

	synchronized public void updateConfigurationIfNeeded(Context context, Session session, BackendConfiguration backendConfiguration, HttpURLConnection connection)
			throws XCBStatusException,
			HttpResponseException, JsonParseException, JsonMappingException, ConnectFailedException,
			LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		if (connection == null)
		{
			return;
		}

		String notify = connection.getHeaderField(HEADER_CORTADO_NOTIFY);

		if (notify != null && notify.length() > 0 && notify.equals("8"))
		{
			SessionManager sessionManager = new SessionManager(context);
			ConfigurationRequester requester = new ConfigurationRequester(context, session, backendConfiguration);

			session.setConfiguration(requester.getConfiguration());

			sessionManager.saveSession(session);
		}
	}
}
