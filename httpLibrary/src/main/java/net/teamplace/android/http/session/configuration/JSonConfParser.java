package net.teamplace.android.http.session.configuration;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

public class JSonConfParser
{

	private static final String VALIDITY = "VALIDITY";
	private static final String HOST = "HOST";
	private static final String HOSTEX = "HOSTEX";
	private static final String SYSTEM = "System";
	private static final String FILEEXT = "FILEEXT";

	public static Configuration parseJSonToConf(String jsonIn)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(jsonIn);
			JSONObject system = jsonObject.getJSONObject(SYSTEM);
			String hostex = system.getString(HOSTEX);
			String host = system.getString(HOST);
			String validity = system.getString(VALIDITY);
			String fileExt = system.getString(FILEEXT);
			int valid = Integer.parseInt(validity);
			Date date = new Date();
			long time = date.getTime();
			time += (valid * 1000);
			Date validUntil = new Date(time);
			return null;// TODO create constructor new Configuration(hostex, host, validUntil, fileExt);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			System.out.println(e);
			return null;
		}
	}

}
