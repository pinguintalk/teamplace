package net.teamplace.android.http.request.account;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/**
 * Handles the requests around Cortado accounts
 * 
 * @author Jamic
 */
public class AccountRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public AccountRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	/**
	 * Registers an Cortado account on given server with given email
	 * 
	 * @param email
	 *            The users email address
	 * @return {@link RegisterResult}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public RegisterResult registerAccount(String email) throws ConnectFailedException,
			HttpResponseException, LogonFailedException,
			XCBStatusException, NotificationException, ServerLicenseException, JsonParseException,
			JsonMappingException, IOException, RedirectException {
		byte[] data = JSonRegisterRequestBuilder.createRegisterRequest(email);

		Uri uri = getRegisterAccountUri(backendConfiguration.registration());

		return (RegisterResult) ResponseResult.parseResponse(doJsonUploadPOST(uri, data, false, false),
				ResponseResult.TYPE_REGISTER);
	}

	/**
	 * Resets the user password
	 * @param user
	 *            The users email address
	 * @return {@link PasswordResetResult}
	 */
	public PasswordResetResult resetPassword(String user) throws ConnectFailedException,
			HttpResponseException, LogonFailedException,
			IOException, XCBStatusException, NotificationException, ServerLicenseException, RedirectException {
		byte[] data = JsonPasswordResetRequestBuilder.createPasswordResetRequest(user);

		Uri uri = getResetPasswordUri(backendConfiguration.registration());

		return (PasswordResetResult) ResponseResult.parseResponse(doJsonUploadPOST(uri, data, false, false),
				ResponseResult.TYPE_RESET_PW);
	}

	/**
	 * Requests information about the user
	 * 
	 * @param user
	 *            The users email address
	 * @param password
	 *            The user password
	 * @return {@link InfoRequestResult}
	 */
	public InfoRequestResult requestInfo(String user, String password) throws HttpResponseException,
			LogonFailedException,
			IOException, ConnectFailedException, XCBStatusException, NotificationException, ServerLicenseException, RedirectException {
		byte[] data = JsonInfoRequestBuilder.createInfoRequest(user, password);

		Uri uri = getInfoUri(backendConfiguration.registration());

		return (InfoRequestResult) ResponseResult.parseResponse(doJsonUploadPOST(uri, data, true, true),
				ResponseResult.TYPE_INFO);
	}

	/**
	 * Changes the current user password
	 * 
	 * @param user
	 *            The users email address
	 * @param password
	 *            The current user password
	 * @param newPassword
	 *            The new user password
	 */
	public void changePassword(String user, String password, String newPassword)
			throws ConnectFailedException, XCBStatusException,
			HttpResponseException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		List<NameValuePair> properties = new LinkedList<NameValuePair>();
		properties.add(new BasicNameValuePair(HEADER_X_PASSWORD, newPassword));

		Uri uri = getChangePasswordUri(backendConfiguration.registration());

		executeFireAndForgetGetRequest(uri, properties, true, true);
	}

	private Uri getRegisterAccountUri(String server)
	{
		return Uri.parse(server).buildUpon().appendEncodedPath(PATH_REGISTRATION).build();
	}

	private Uri getResetPasswordUri(String server)
	{
		return Uri.parse(server).buildUpon().appendEncodedPath(PATH_RESET_PASSWORD).build();
	}

	private Uri getChangePasswordUri(String server)
	{
		return Uri.parse(server).buildUpon().appendEncodedPath(PATH_CHANGEPASSWORD).build();
	}

	private Uri getInfoUri(String server)
	{
		return Uri.parse(server).buildUpon().appendEncodedPath(PATH_INFO_REQUEST).build();
	}
}
