package net.teamplace.android.http.request.faulttolerant.upload;

import net.teamplace.android.http.request.faulttolerant.download.UpDownloadSQLiteHelper;
import android.content.ContentValues;
import android.database.Cursor;


public class UploadStatus
{
	private final String TAG = getClass().getSimpleName();

	public static final String TABLE_NAME = "upload_table";

	public static final String COL_TRANSACTION_ID = "transactionId";
	public static final String COL_REMOTE_FILE_PATH = "remoteFilePath";
	public static final String COL_REMOTE_FILE_NAME = "remoteFileName";
	public static final String COL_LOCAL_FILE_PATH = "localFilePath";
	public static final String COL_LOCAL_FILE_NAME = "localFileName";
	public static final String COL_LAST_MODIFY_TIMESTAMP = "lastModifyTimestamp";

	public static String CREATE_TABLE = "create table " + TABLE_NAME + "(" + UpDownloadSQLiteHelper.COLUMN_ID
			+ " integer primary key autoincrement, " + COL_TRANSACTION_ID + " text, " + COL_REMOTE_FILE_PATH
			+ " text not null, "
			+ COL_REMOTE_FILE_NAME + " text not null, " + COL_LOCAL_FILE_PATH + " text not null, "
			+ COL_LOCAL_FILE_NAME + " text not null, "
			+ COL_LAST_MODIFY_TIMESTAMP + " integer);";

	private String transactionId;
	private String remoteFilePath;
	private String remoteFileName;
	private String localFilePath;
	private String localFileName;
	private long lastModifyTimestamp;

	public UploadStatus()
	{
	}

	public UploadStatus(String transactionId, String remoteFilePath, String remoteFileName, String localFilePath,
			String localFileName,
			long lastModifyTimestamp)
	{
		this.transactionId = transactionId;
		this.remoteFilePath = remoteFilePath;
		this.remoteFileName = remoteFileName;
		this.localFilePath = localFilePath;
		this.localFileName = localFileName;
		this.lastModifyTimestamp = lastModifyTimestamp;
	}

	public UploadStatus(Cursor cursor)
	{
		transactionId = cursor.getString(cursor.getColumnIndex(COL_TRANSACTION_ID));
		remoteFilePath = cursor.getString(cursor.getColumnIndex(COL_REMOTE_FILE_PATH));
		remoteFileName = cursor.getString(cursor.getColumnIndex(COL_REMOTE_FILE_NAME));
		localFilePath = cursor.getString(cursor.getColumnIndex(COL_LOCAL_FILE_PATH));
		localFileName = cursor.getString(cursor.getColumnIndex(COL_LOCAL_FILE_NAME));
		lastModifyTimestamp = cursor.getLong(cursor.getColumnIndex(COL_LAST_MODIFY_TIMESTAMP));
	}

	public String getTransactionId()
	{
		return transactionId;
	}

	public void setTransactionId(String transactionId)
	{
		this.transactionId = transactionId;
	}

	public String getRemoteFilePath()
	{
		return remoteFilePath;
	}

	public void setRemoteFilePath(String filePath)
	{
		this.remoteFilePath = filePath;
	}

	public String getRemoteFileName()
	{
		return remoteFileName;
	}

	public void setRemoteFileName(String fileName)
	{
		this.remoteFileName = fileName;
	}

	public String getLocalFilePath()
	{
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath)
	{
		this.localFilePath = localFilePath;
	}

	public String getLocalFileName()
	{
		return localFileName;
	}

	public void setLocalFileName(String localFileName)
	{
		this.localFileName = localFileName;
	}

	public long getLastModifyTimestamp()
	{
		return lastModifyTimestamp;
	}

	public void setLastModifyTimestamp(long lastModifyTimestamp)
	{
		this.lastModifyTimestamp = lastModifyTimestamp;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_TRANSACTION_ID, transactionId);
		values.put(COL_REMOTE_FILE_PATH, remoteFilePath);
		values.put(COL_REMOTE_FILE_NAME, remoteFileName);
		values.put(COL_LOCAL_FILE_PATH, localFilePath);
		values.put(COL_LOCAL_FILE_NAME, localFileName);
		values.put(COL_LAST_MODIFY_TIMESTAMP, lastModifyTimestamp);

		return values;
	}
}
