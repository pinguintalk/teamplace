package net.teamplace.android.http.request.share;

import java.io.IOException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.ArrayType;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ShareResponse
{
	private final String TAG = getClass().getSimpleName();

	public static final int STATUS_SUCCESS = 0;

	@JsonProperty("status")
	private int status;

	@JsonProperty("recipient")
	private String recipient;

	@JsonProperty("shareid")
	private String shareId;

	@JsonProperty("sharelink")
	private String shareLink;

	@JsonProperty("sharelinkmac")
	private String shareLinkMac;

	@JsonProperty("sharelinkccs")
	private String shareLinkCCS;

	public ShareResponse()
	{
	}

	public int getStatus()
	{
		return status;
	}

	public String getRecipient()
	{
		return recipient;
	}

	public String getShareId()
	{
		return shareId;
	}

	public String getShareLink()
	{
		return shareLink;
	}

	public String getShareLinkMac()
	{
		return shareLinkMac;
	}

	public String getShareLinkCCS()
	{
		return shareLinkCCS;
	}

	@JsonIgnore
	public static ShareResponse[] createShareResponseFromJSON(String json) throws JsonParseException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();

		ArrayType arrayType = mapper.getTypeFactory().constructArrayType(ShareResponse.class);

		return mapper.readValue(json, arrayType);
	}
}
