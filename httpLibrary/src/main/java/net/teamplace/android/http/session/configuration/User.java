package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class User
{
	private static final String COL_PASSWORD_EXPIRATION_DATE = "password_expiration_date";
	private static final String COL_LOGGED_USER_COUNT = "logged_user_count";
	private static final String COL_TPUID = "tpuid";

	protected static final String TABLE_PART = COL_PASSWORD_EXPIRATION_DATE + " text, " + COL_LOGGED_USER_COUNT
			+ " integer, " + COL_TPUID + " text, ";

	@JsonProperty("PWEXPIRATIONDATE")
	private long mPasswordExpirationDate;

	@JsonProperty("$LoggedUserCount")
	private int mLoggedUserCount;

	@JsonProperty("TPUID")
	private String mUID;

	public User()
	{
	}

	public User(Cursor cursor)
	{
		mPasswordExpirationDate = cursor.getLong(cursor.getColumnIndex(COL_PASSWORD_EXPIRATION_DATE));
		mLoggedUserCount = cursor.getInt(cursor.getColumnIndex(COL_LOGGED_USER_COUNT));
		mUID = cursor.getString(cursor.getColumnIndex(COL_TPUID));
	}

	public long getPasswordExpirationDate()
	{
		return mPasswordExpirationDate;
	}

	public void setPasswordExpirationDate(long passwordExpirationDate)
	{
		mPasswordExpirationDate = passwordExpirationDate;
	}

	public int getLoggedUserCount()
	{
		return mLoggedUserCount;
	}

	public void setLoggedUserCount(int loggedUserCount)
	{
		mLoggedUserCount = loggedUserCount;
	}

	public String getUID()
	{
		return mUID;
	}

	public void setUID(String uid)
	{
		mUID = uid;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_PASSWORD_EXPIRATION_DATE, mPasswordExpirationDate);
		values.put(COL_LOGGED_USER_COUNT, mLoggedUserCount);
		values.put(COL_TPUID, mUID);

		return values;
	}
}
