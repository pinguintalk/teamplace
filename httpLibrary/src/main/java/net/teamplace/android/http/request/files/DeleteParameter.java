package net.teamplace.android.http.request.files;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.util.FilePathHelper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class DeleteParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_TEAM)
	private String team;

	@JsonProperty(PARAM_FOLDER)
	private String parentFolder;

	@JsonProperty(PARAM_DELETE_FILE)
	private String fileOrFolder;

	@JsonProperty(PARAM_FORCE_OVERWRITE)
	private int forceDeletion;

	public DeleteParameter(String team, String parentFolder, String fileOrFolder, int forceDeleteFolder)
	{
		this.team = team;
		this.parentFolder = FilePathHelper.buildWindowsPath(parentFolder, false, false);
		this.fileOrFolder = fileOrFolder;
		this.forceDeletion = forceDeleteFolder;
	}
}
