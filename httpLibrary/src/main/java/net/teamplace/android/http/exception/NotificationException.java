package net.teamplace.android.http.exception;

public class NotificationException extends CortadoException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2331027462790728834L;

	private final long _notification;
	private final long _reason;

	public NotificationException(long notification, long reason, String message)
	{
		super(message);
		_notification = notification;
		_reason = reason;
	}

	public long getNotification()
	{
		return _notification;
	}

	public long getReason()
	{
		return _reason;
	}

}
