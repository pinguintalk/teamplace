package net.teamplace.android.http.request.faulttolerant;

public interface NetworkOperationCallback
{
	public boolean stopNetworkOperation();
}
