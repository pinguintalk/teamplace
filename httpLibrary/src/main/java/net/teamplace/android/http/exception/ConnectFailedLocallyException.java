package net.teamplace.android.http.exception;

public class ConnectFailedLocallyException extends ConnectFailedException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -414052198004223643L;

	public ConnectFailedLocallyException(String strErr)
	{
		super(strErr);
	}

	public ConnectFailedLocallyException()
	{
		super("Connection failes locally");
	}

}
