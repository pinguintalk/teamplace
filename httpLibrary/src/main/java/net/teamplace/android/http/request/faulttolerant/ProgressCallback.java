package net.teamplace.android.http.request.faulttolerant;

public interface ProgressCallback
{
	public void publishProgressInPercent(int percent);
}
