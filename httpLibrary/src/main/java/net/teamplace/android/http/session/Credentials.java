package net.teamplace.android.http.session;

public class Credentials
{
	private String mUsername;
	private String mPassword;

	public Credentials()
	{
	}

	public Credentials(String username, String password)
	{
		mUsername = username;
		mPassword = password;
	}

	public String getUsername()
	{
		return mUsername;
	}

	public void setUsername(String username)
	{
		mUsername = username;
	}

	public String getPassword()
	{
		return mPassword;
	}

	public void setPassword(String password)
	{
		mPassword = password;
	}
}
