package net.teamplace.android.http.file;

import net.teamplace.android.http.annotation.JacksonDataModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class RemoteFile
{
	protected static final int FLAG_VERSION = 8;
	protected static final int FLAG_SMART_FILE = 16;
	protected static final int FLAG_IS_FAVORITE = 32;

	protected final String NAME = "name";
	protected final String SIZE = "size";
	protected final String DATE = "date";
	protected final String FLAGS = "flags";
	protected final String UNCOMPRESSED_SIZE = "uncsize";

	@JsonProperty(NAME)
	protected String name;

	@JsonProperty(SIZE)
	protected long size;

	@JsonProperty(DATE)
	protected long date;

	@JsonProperty(FLAGS)
	protected int flags;

	@JsonProperty(UNCOMPRESSED_SIZE)
	private long uncompressedSize;

	public RemoteFile()
	{
	}

	public String getName()
	{
		return name;
	}

	public long getSize()
	{
		return size;
	}

	public long getDate()
	{
		return date;
	}

	public long getUncompressedSize()
	{
		return uncompressedSize;
	}

	public boolean hasVersions()
	{
		return (flags & FLAG_VERSION) == FLAG_VERSION;
	}

	public boolean isSmartFile()
	{
		return (flags & FLAG_SMART_FILE) == FLAG_SMART_FILE;
	}

	public boolean isFavorite()
	{
		return (flags & FLAG_IS_FAVORITE) == FLAG_IS_FAVORITE;
	}
}
