package net.teamplace.android.http.teamdrive;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

/**
 * Created by jobol on 06/08/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class UserListInvitation {

    @JsonProperty("Message")
    private String message;

    @JsonProperty("Users")
    private User[] users;

    public UserListInvitation(String message, User[] users) {
        this.message = message;
        this. users = users;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User[] getUsers() {
        return users;
    }

    public void setUsers(User[] users) {
        this.users = users;
    }

    public static String createJsonObject(UserListInvitation... in) throws JsonProcessingException
    {
        return JsonUtils.getDefaultMapper().writeValueAsString(in.length > 1 ? in : in[0]);
    }
}
