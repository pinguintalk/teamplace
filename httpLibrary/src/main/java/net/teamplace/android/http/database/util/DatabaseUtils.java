package net.teamplace.android.http.database.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.database.sqlite.SQLiteDatabase;

public class DatabaseUtils
{
	public static void modifyColumnSet(SQLiteDatabase db, String table, String[] newColumns,
			String[] newColumnDescriptions, String[] addedColumns)
	{
		String tempTable = table + "_temp";

		List<String> descriptionsList = new ArrayList<String>(Arrays.asList(newColumnDescriptions));
		List<String> toCreateList = new ArrayList<String>(Arrays.asList(newColumns));

		String[] addedColumnDesriptions = new String[addedColumns.length];

		int indexToRemove;
		for (int i = 0; i < addedColumns.length; i++)
		{
			indexToRemove = toCreateList.indexOf(addedColumns[i]);
			addedColumnDesriptions[i] = descriptionsList.get(indexToRemove);
			descriptionsList.remove(indexToRemove);
			toCreateList.remove(indexToRemove);
		}

		String[] toCreate = toCreateList.toArray(new String[toCreateList.size()]);
		String[] toCreateDescriptions = descriptionsList.toArray(new String[descriptionsList.size()]);

		StringBuilder selectBuilder = new StringBuilder();
		for (String column : toCreateList)
		{
			selectBuilder.append(column).append(", ");
		}
		String select = selectBuilder.substring(0, selectBuilder.length() - 2).toString();

		// create temporary table
		db.execSQL("CREATE TEMPORARY TABLE " + tempTable + "("
				+ buildCreationColumnList(toCreate, toCreateDescriptions) + ");");
		// copy columns from old table
		db.execSQL("INSERT INTO " + tempTable + " SELECT "
				+ select + " FROM " + table + ";");
		// drop old table
		db.execSQL("DROP TABLE " + table + ";");
		// create new table
		db.execSQL("CREATE TABLE " + table + "("
				+ buildCreationColumnList(toCreate, toCreateDescriptions) + ");");
		// copy from temporary table
		db.execSQL("INSERT INTO " + table + " SELECT "
				+ select + " FROM " + tempTable + ";");
		// drop temporary table
		db.execSQL("DROP TABLE " + tempTable + ";");

		for (int i = 0; i < addedColumns.length; i++)
		{
			db.execSQL("ALTER TABLE " + table + " ADD COLUMN " + addedColumns[i] + " " + addedColumnDesriptions[i]
					+ ";");
		}

	}

	public static String buildCreationColumnList(String[] columnNames, String[] descriptions)
	{
		if (columnNames.length != descriptions.length)
		{
			throw new IllegalArgumentException("columnNames and descriptions must be same length.");
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < columnNames.length; i++)
		{
			sb.append(columnNames[i])
					.append(" ")
					.append(descriptions[i])
					.append(", ");
		}
		return sb.substring(0, sb.length() - 2).toString();
	}
}
