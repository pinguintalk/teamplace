package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ServerInformation
{
	private static final String COL_SERVER_VERSION = "server_version";

	protected static final String TABLE_PART = ServerInformation.COL_SERVER_VERSION + " text, ";

	@JsonProperty("VERSION")
	private String mVersion;

	public ServerInformation()
	{
	}

	public ServerInformation(Cursor cursor)
	{
		mVersion = cursor.getString(cursor.getColumnIndex(COL_SERVER_VERSION));
	}

	public String getVersion()
	{
		return mVersion;
	}

	public void setVersion(String version)
	{
		mVersion = version;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_SERVER_VERSION, mVersion);

		return values;
	}
}
