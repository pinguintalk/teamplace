package net.teamplace.android.http.exception;

public class XCBStatusException extends ShowErrorMessageException
{
	public static final long ERROR_NOT_SUPPORTED = 50;
	public static final long ERR_SERVER_FILE_NOT_FOUND = 801;
	public static final long ERR_SERVER_DIRECTORY_NOT_FOUND = 802;
	public static final long ERR_SERVER_ERROR_IN_REQUEST = 803;
	public static final long ERR_SERVER_PRINTER_NOT_FOUND = 804;
	public static final long ERR_SERVER_WRONG_FILE_NAME = 805;
	public static final long ERR_SERVER_WRONG_PRINTER_NAME = 806;
	public static final long ERR_SERVER_REGISTRY_KEY_NOT_SET = 807;
	public static final long ERR_SERVER_FILE_LOCKED = 808;
	public static final long ERR_SERVER_FILEISEMPTY = 809;
	public static final long ERR_INCORRECT_DOC_TYPE = 850;
	public static final long ERR_CANNOT_RECOGNIZE_DOCUMENT = 851;
	public static final long ERR_CREATING_PROCESS = 852;
	public static final long ERR_READING_REGISTRY = 853;
	public static final long ERR_FETCH_FILE_UNKNOWN_PROTOCOL = 854;
	public static final long ERR_FETCH_FILE_FILE_NOT_FOUND = 855;
	public static final long ERR_CONFIG_ENTRY_NOT_FOUND = 856;
	public static final long ERR_NO_PRINT_INFO_FOR_DOCUMENT = 857;
	public static final long ERR_PRINTER_TIMEOUT_EXPIRED = 858;
	public static final long ERR_STARTPROCESS_RESPONSE_TIMEOUT_EXPIRED = 859;
	public static final long ERR_CANTREADSTARTPROCESSRESPONSE = 860;
	public static final long ERR_EMPTYEVENTNAME = 861;
	public static final long ERR_MULTIUSERDATA_NOT_INITIALISED = 862;
	public static final long ERR_MULTIUSERDATA_USERSUNDEFINED = 863;
	public static final long ERR_NOFREECONTEXT_AVAILABLE = 864;
	public static final long ERR_CONF_NO_DEFAULT_PRINTER_SPECIFIED = 865;
	public static final long ERR_FAXSEND_WRONGFAXNUMBER = 866;
	public static final long ERR_FAXSEND_EXCEPTIONININIT = 867;
	public static final long ERR_FAXSEND_NOTALLOWED = 868;
	public static final long ERR_FAXSEND_ILLEGALNUMBER = 869;
	public static final long ERR_PROGRAM_VERSION = 870;
	public static final long ERR_INVALID_HOTSPOTCODE = 871;
	public static final long ERR_REQ_HEADER_NOTCOMPLETE = 872;
	public static final long ERR_PARSING_REQ_HEADER = 873;
	public static final long ERR_PARSING_REQ_TEXT = 874;
	public static final long ERR_INVALID_VERB_PARAMETERS = 875;
	public static final long ERR_INVALID_PARAMETER_COMBINATION = 876;
	public static final long ERR_PRINTFUNC_HANDLED_EXTERNALLY = 877;
	public static final long ERR_CONFREQ_HANDLED_EXTERNALLY = 878;
	public static final long ERR_VERB_NOTSUPPORTED = 879;
	public static final long ERR_PREV_UNFINISHED = 880;
	public static final long ERR_FEAT_NOT_ALLOWED = 881;
	public static final long ERR_SENDMAIL_SOCKET = 889;
	public static final long ERR_SENDMAIL_ARGUMENT = 890;
	public static final long ERR_SENDMAIL_SERVERCONNECTION = 891;
	public static final long ERR_SENDMAIL_SENDMESSFAILED = 892;
	public static final long ERR_SENDMAIL_SENDMESSFAILED_UNKNOWN = 893;
	public static final long ERR_SENDMAIL_SENDMESSFAILED_NONLOCALMAIL = 894;
	public static final long ERR_DISK_QUOTA_EXCEEDED = 1295;
	public static final long ERR_SHARE_SENDMESSFAILED = 1627;
	public static final long ERR_URL_NOT_FOUND = 12007;

	private long m_lErrorCode;

	public XCBStatusException(String strError, long lErrorCode)
	{
		super(strError + " " + lErrorCode);
		m_lErrorCode = lErrorCode;
	}

	public long getStatusCode()
	{
		return m_lErrorCode;
	}

	/**
     * 
     */
	private static final long serialVersionUID = -2055801545795456011L;

}
