package net.teamplace.android.http.exception;

public class FileNotFoundException extends ConnectFailedException
{

	private static final long serialVersionUID = -7677633494983283885L;

	public FileNotFoundException()
	{
		super();
	}

	public FileNotFoundException(String str)
	{
		super(str);
	}
}