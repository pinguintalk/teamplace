package net.teamplace.android.http.exception;

public class ConnectionLocallyForbiddenException extends ConnectionForbiddenException
{
	private static final long serialVersionUID = -1665643511832041611L;

	public ConnectionLocallyForbiddenException(String strErr)
	{
		super(strErr);
	}
}
