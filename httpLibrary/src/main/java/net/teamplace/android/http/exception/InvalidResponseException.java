package net.teamplace.android.http.exception;

public class InvalidResponseException extends CortadoException
{
	private static final long serialVersionUID = 7773391989624372837L;

	public InvalidResponseException()
	{
		super("invalid response");
	}

	public InvalidResponseException(int iResponseCode)
	{
		super("invalid response " + iResponseCode);
	}

	public InvalidResponseException(String strErr)
	{
		super(strErr);
	}
}
