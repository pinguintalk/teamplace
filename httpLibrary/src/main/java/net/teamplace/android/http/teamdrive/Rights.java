package net.teamplace.android.http.teamdrive;

import android.os.Parcel;
import android.os.Parcelable;

public class Rights extends CheckableRights
{
	public static final int RETRIEVE_ALL_TEAMDRIVES = 1;
	public static final int RETRIEVE_TEAMDRIVE = 2;
	public static final int CREATE_TEAMDRIVE = 4;
	public static final int UPDATE_TEAMDRIVE = 8;
	public static final int DELETE_TEAMDRIVE = 16;
	public static final int ADD_USER = 32;
	public static final int DELETE_USER = 64;
	public static final int CHANGE_MEMBER_ROLE = 128;
	public static final int UP_OR_DOWNGRADE = 65536;

	public Rights(int rights)
	{
		super(rights);
	}

}
