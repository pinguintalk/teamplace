package net.teamplace.android.http.request.search;

public interface FileSearchResultListener
{
	/**
	 * @param result
	 *            the returned result
	 * @return whether the search shall be continued
	 */
	public void onNewResult(FileSearchResult result, boolean isLastResult);

	/**
	 * Invoked before each server request.
	 * 
	 * @return whether to execute the next request.
	 */
	public boolean continueSearch(String searchId);
}
