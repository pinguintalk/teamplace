package net.teamplace.android.http.request;

public interface ICloseAble
{

	public void close();

}
