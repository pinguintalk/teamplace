package net.teamplace.android.http.exception;

public class OperationFailedException extends CortadoException
{
	private static final long serialVersionUID = -8384353696340876953L;

	public OperationFailedException(String strError)
	{
		super(strError);
	}

}
