package net.teamplace.android.http.exception;

public class InvalidServerCredentials extends ConnectFailedException
{
	private static final long serialVersionUID = 1026793431668597099L;

	public InvalidServerCredentials(String strErr)
	{
		super(strErr);
	}
}
