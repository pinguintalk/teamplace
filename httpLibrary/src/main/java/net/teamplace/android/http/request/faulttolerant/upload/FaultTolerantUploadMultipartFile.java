package net.teamplace.android.http.request.faulttolerant.upload;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import net.teamplace.android.http.multipart.MultipartFile;


public class FaultTolerantUploadMultipartFile extends MultipartFile
{
	private final String TAG = getClass().getSimpleName();

	private String transactionId;
	private String fileName;
	private long lastModified;

	public FaultTolerantUploadMultipartFile(InputStream fileInputStream, String fileName, long fileSize,
			String transactionId, long skipBytes)
			throws IOException
	{
		super(fileInputStream, fileName, fileSize, skipBytes);

		this.transactionId = transactionId;
		this.fileName = fileName;
	}

	public FaultTolerantUploadMultipartFile(File file, String transactionId, long skipBytes) throws IOException
	{
		super(file, skipBytes);

		this.transactionId = transactionId;
		this.fileName = file.getName();
		this.lastModified = file.lastModified();
	}

	@Override
	public String getFileNameForUpload()
	{
		return fileName;
	}

	@Override
	protected void buildPreContent(StringBuilder strBuilder)
	{
		addContentDispositionPart(PARAM_GUI, "1", strBuilder);
		addContentDispositionPart(PARAM_TRANSACTION_ID, transactionId, strBuilder);
		addAppendMode(strBuilder);
	}

	public long getLastModified()
	{
		return lastModified;
	}
}
