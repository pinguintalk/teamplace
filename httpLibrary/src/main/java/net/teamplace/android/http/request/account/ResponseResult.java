package net.teamplace.android.http.request.account;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

public class ResponseResult implements Parcelable
{
	public static final String TYPE_REGISTER = "register";
	public static final String TYPE_RESET_PW = "reset";
	public static final String TYPE_INFO = "info";

	private static final String KEY_RESULT = "Result";
	private static final String KEY_SUCCESS = "IsSuccess";
	private static final String KEY_MESSAGE_LIST = "MessageList";

	protected String mMessage;
	protected int mResultCode = -1;
	protected boolean mSuccess;

	public ResponseResult(String message, int resultCode, boolean success)
	{
		mMessage = message;
		mResultCode = resultCode;
		mSuccess = success;
	}

	public ResponseResult(Parcel in)
	{
		mMessage = in.readString();
		mResultCode = in.readInt();
		mSuccess = in.readInt() == 1;
	}

	public String getMessage()
	{
		return mMessage;
	}

	public int getResultCode()
	{
		return mResultCode;
	}

	public boolean isSuccess()
	{
		return mSuccess;
	}

	@Override
	public String toString()
	{
		return mMessage;
	}

	public static ResponseResult parseResponse(String jsonIn, String requestType)
	{
		try
		{
			JSONObject jsonObject = new JSONObject(jsonIn);
			Boolean success = jsonObject.getBoolean(KEY_SUCCESS);
			int result = jsonObject.getInt(KEY_RESULT);

			String message = null;
			try
			{
				message = jsonObject.getString(KEY_MESSAGE_LIST);
			}
			catch (JSONException e)
			{
				message = "";
			}

			if (requestType.equals(TYPE_REGISTER))
			{
				return new RegisterResult(message, result, success);
			}
			else if (requestType.equals(TYPE_RESET_PW))
			{
				return new PasswordResetResult(message, result, success);
			}
			else
			{
				return new InfoRequestResult(message, result, success);
			}
		}
		catch (JSONException e)
		{
			System.out.println(e);
			return null;
		}
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(mMessage);
		dest.writeInt(mResultCode);
		dest.writeInt(mSuccess ? 1 : 0);
	}

	public static final Parcelable.Creator<ResponseResult> CREATOR = new Parcelable.Creator<ResponseResult>()
	{
		public ResponseResult createFromParcel(Parcel in)
		{
			return new ResponseResult(in);
		}

		public ResponseResult[] newArray(int size)
		{
			return new ResponseResult[size];
		}
	};

}
