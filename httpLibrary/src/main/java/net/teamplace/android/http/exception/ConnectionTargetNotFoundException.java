package net.teamplace.android.http.exception;

public class ConnectionTargetNotFoundException extends CortadoException
{

	public ConnectionTargetNotFoundException(String strError)
	{
		super(strError);
	}

	/**
     * 
     */
	private static final long serialVersionUID = -2598983862254635357L;

}
