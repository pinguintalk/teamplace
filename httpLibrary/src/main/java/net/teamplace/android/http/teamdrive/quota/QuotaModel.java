package net.teamplace.android.http.teamdrive.quota;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.teamplace.android.http.annotation.JacksonDataModel;

/**
 * Created by jobol on 06/08/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public abstract class QuotaModel {

    @JsonProperty("Limit")
    public Integer limit;

    @JsonProperty("RemainingMb")
    public Integer remainingMb;

    @JsonProperty("RemainingPercent")
    public Integer remainingPercent;

    @JsonProperty("UsedMb")
    public Integer usedMb;

    @JsonProperty("UsedPercent")
    public Integer usedPercent;

    public QuotaModel() {
    }

    public QuotaModel(Integer limit, Integer remainingMb, Integer remainingPercent, Integer usedMb, Integer usedPercent) {
        this.limit = limit;
        this.remainingMb = remainingMb;
        this.remainingPercent = remainingPercent;
        this.usedMb = usedMb;
        this.usedPercent = usedPercent;
    }

}
