package net.teamplace.android.http.exception;

public class NoFreeSpaceException extends DataLimitReachedException
{
	/**
     * 
     */
	private static final long serialVersionUID = 7347963369830676352L;

	public NoFreeSpaceException(String strMsg)
	{
		super(strMsg);
	}

}
