package net.teamplace.android.http.teamdrive.database;

import android.database.sqlite.SQLiteDatabase;

public class TableInvitations
{

	public static final String TABLE = "table_invitations";

	public static final String COLUMN_ID = "invitation_id";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_TEAMDRIVE_ID = "teamdrive_id";
	public static final String COLUMN_TEAMDRIVE_NAME = "teamdrive_name";
	public static final String COLUMN_TEAMDRIVE_NOTE = "teamdrive_note";
	public static final String COLUMN_INVITING_USER_ID = "inviting_user_id";
	public static final String COLUMN_INVITING_USER_EMAIL = "inviting_user_email";
	public static final String COLUMN_INVITING_USER_DISPLAY_NAME = "inviting_user_display_name";
	public static final String COLUMN_INVITATION_MESSAGE = "invitation_mesaage";
	public static final String COLUMN_ROLE = "role";

	public static final String ID_ETAG_ALL_DRIVES = "etag for all drives";

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " ("
			+ COLUMN_ID + " TEXT PRIMARY KEY, "
			+ COLUMN_EMAIL + " TEXT, "
			+ COLUMN_TEAMDRIVE_ID + " TEXT, "
			+ COLUMN_TEAMDRIVE_NAME + " TEXT, "
			+ COLUMN_TEAMDRIVE_NOTE + " TEXT, "
			+ COLUMN_INVITING_USER_ID + " TEXT, "
			+ COLUMN_INVITING_USER_EMAIL + " TEXT, "
			+ COLUMN_INVITING_USER_DISPLAY_NAME + " TEXT, "
			+ COLUMN_INVITATION_MESSAGE + " TEXT, "
			+ COLUMN_ROLE + " INTEGER);";

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableInvitations.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}
}
