package net.teamplace.android.http.request;

import net.teamplace.android.http.exception.AccountNotFoundException;
import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;

import java.io.IOException;
import java.net.HttpURLConnection;


public class ConnectionErrorHandler implements ServerParams
{
	private static final String TAG = ConnectionErrorHandler.class.getSimpleName();

	public ConnectionErrorHandler()
	{
	}

	public static void handleConnectionStatus(HttpURLConnection connection) throws XCBStatusException,
			HttpResponseException, NotificationException, ServerLicenseException, LogonFailedException,
			ConnectFailedException, RedirectException {
		int statusCode = getHTTPResponseCode(connection);
		if (statusCode == HttpURLConnection.HTTP_MOVED_PERM || statusCode == HttpURLConnection.HTTP_MOVED_TEMP)
		{
			throw new RedirectException(connection.getHeaderField("Location"));
		}
		handleConnectionStatus(statusCode, getXCBStatusCode(connection),
				getNotificationCode(connection), getLockReason(connection));
	}

	private static int getHTTPResponseCode(HttpURLConnection connection) throws ConnectFailedException
	{
		int respCode = -1;
		try
		{
			respCode = connection.getResponseCode();
		}
		catch (IOException e)
		{
			String msg = e.getMessage();
			if (msg != null && msg.contains("Received authentication challenge is null"))
			{
				respCode = 401; // http://stackoverflow.com/questions/12931791/java-io-ioexception-received-authentication-challenge-is-null-in-ics-4-0-3
			}
			else
			{
				throw new ConnectFailedException(e);
			}
		}
		if (respCode == -1)
		{
			throw new ConnectFailedException("http response" + respCode);
		}
		return respCode;
	}

	private static Integer getXCBStatusCode(HttpURLConnection connection)
	{
		int xcb = -1;
		String xcbHeader = connection.getHeaderField(HEADER_X_CBSTATUS);

		if (xcbHeader != null)
		{
			try
			{
				xcb = Integer.decode(xcbHeader);
			}
			catch (NumberFormatException e)
			{
				e.printStackTrace();
			}
		}
		return xcb;
	}

	private static long getNotificationCode(HttpURLConnection connection)
	{
		if (connection == null)
		{
			return 0L;
		}

		String headerNotification = connection.getHeaderField(HEADER_CORTADO_NOTIFY);// notify header bitmask (1 sel
																						// wipe, 2 lock)

		if (headerNotification != null && headerNotification.length() > 0)
		{
			try
			{
				return Long.parseLong(headerNotification);
			}
			catch (NumberFormatException e)
			{
			}
		}
		return 0L;
	}

	private static long getLockReason(HttpURLConnection connection)
	{
		if (connection == null)
		{
			return 0L;
		}

		String headerNotification = connection.getHeaderField(HEADER_CORTADO_NOTIFY_REASON);

		if (headerNotification != null && headerNotification.length() > 0)
		{
			try
			{
				return Long.parseLong(headerNotification);
			}
			catch (NumberFormatException e)
			{
			}
		}
		return 0L;
	}

	private static void handleConnectionStatus(int responseCode, int xcbStatusCode, long notify, long notifyReason)
			throws NotificationException, XCBStatusException, ServerLicenseException, HttpResponseException,
			LogonFailedException, ConnectFailedException
	{
		if (notify > 0 && notify != 8)
		{
			throw new NotificationException(notify, notifyReason, "Notification");
		}

		if (responseCode == HttpURLConnection.HTTP_OK)
		{
			// xcbStatusCode == -1, if no X-CB header field was found
			if (xcbStatusCode == -1 || xcbStatusCode == 0)
			{
				return;
			}
			/*else if (xcbStatusCode == CB_STATUS_PROCESSING) {
				// server is still processing (e.g. to export large pdf)
				return;
			}*/

			throw new XCBStatusException("error while connecting", xcbStatusCode);
		}
		else if (responseCode == HttpURLConnection.HTTP_UNAVAILABLE)
		{
			if (xcbStatusCode == 2)
			{
				throw new ServerLicenseException();
			}
		}
		else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND)
		{
			throw new AccountNotFoundException("");
		}

		throwHTTPException(responseCode);
	}

	private static void throwHTTPException(int statusCode) throws LogonFailedException, HttpResponseException,
			ConnectFailedException
	{
		if (statusCode == -1)
		{
			throw new ConnectFailedException("HTTP Status could not be read");
		}
		else if (200 <= statusCode && statusCode < 300)
		{
			// Successful, just return
			return;
		}
		else if (statusCode == 401)
		{
			throw new LogonFailedException("Invalid logon information");
		}
		else
		{
			throw new HttpResponseException(statusCode);
		}
	}
}
