package net.teamplace.android.http.request.share;

import java.io.IOException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import android.content.Context;
import android.net.Uri;


public class ShareRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public ShareRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	/**
	 * @param filePath
	 *            - path to the shared file
	 * @param recipients
	 *            - email addresses which have to be divided by a ";"
	 * @param preview
	 *            - show a preview on landing page
	 * @param isPublic
	 *            - share file via public link
	 * @param userText
	 *            - personal message for the recipients
	 * @return {@link ShareResponse}
	 * @throws TeamDriveNotFoundException
	 * @throws IOException
	 */
	public ShareResponse[] shareFileViaJSON(String teamDriveId, String filePath, String recipients, boolean preview,
			boolean html, boolean isPublic, String password, String userText, String subject, long validUntil)
			throws XCBStatusException, HttpResponseException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException, TeamDriveNotFoundException, IOException, RedirectException {
		byte[] data = new ShareCommand(teamDriveId, filePath, recipients, preview, html, isPublic, password, userText,
				subject, validUntil).getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return ShareResponse.createShareResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}
	//
	// public ShareResponse[] getShareLinViaJSON(String teamDriveId, String filePath, String recipients, boolean preview, boolean html, boolean isPublic, String password, String userText,
	// String subject, long validUntil) throws XCBStatusException, HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
	// TeamDriveNotFoundException, IOException
	// {
	// byte[] data = new ShareLinkCommand(filePath, recipients, preview, html, isPublic, password, userText, subject, validUntil).getAsJSONByteArray();
	//
	// Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));
	//
	// return ShareResponse.createShareResponseFromJSON(doJSONUpload(uri, data, true));
	// }

	public void shareAsMailAttachment(String teamDriveId, String filePath, String sendTo, String subject,
			String userText) throws TeamDriveNotFoundException, XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		Uri uri = getShareAsMailAttachmentUri(teamDriveId, filePath, sendTo, subject, userText);

		executeFireAndForgetGetRequest(uri, null, true, true);
	}

	private Uri getShareAsMailAttachmentUri(String teamDriveId, String filePath, String sendTo, String subject,
			String userText) throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_SEND_TO, sendTo);
		builder.appendQueryParameter(PARAM_COPY_SRC, filePath);
		builder.appendQueryParameter(PARAM_SUBJECT, subject);
		builder.appendQueryParameter(PARAM_BODY, userText);

		return builder.build();
	}
}
