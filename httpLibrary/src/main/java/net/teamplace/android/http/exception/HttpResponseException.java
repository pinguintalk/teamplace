package net.teamplace.android.http.exception;

public class HttpResponseException extends InvalidResponseException
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6306172201988799795L;
	private final int _response;

	public HttpResponseException(int iResponseCode)
	{
		super("invalid response " + iResponseCode);
		_response = iResponseCode;
	}

	public HttpResponseException(String strErr, int iResponse)
	{
		super(strErr);
		_response = iResponse;
	}

	public int getResponseCode()
	{
		return _response;
	}
}
