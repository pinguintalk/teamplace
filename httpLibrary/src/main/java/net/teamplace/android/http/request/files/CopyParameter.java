package net.teamplace.android.http.request.files;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.util.FilePathHelper;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class CopyParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_TEAM)
	private String teamDest;

	@JsonProperty(PARAM_TEAMSRC)
	private String teamSrc;

	@JsonProperty(PARAM_FOLDER)
	private String parentFolder;

	@JsonProperty(PARAM_COPY_SRC)
	private String copyFileOrFolder;

	@JsonProperty(PARAM_DEST_FOLDER)
	private String copyDestinationFolder;

	@JsonProperty(PARAM_COPY_DST)
	private String destinationFileName;

	@JsonProperty(PARAM_FORCE_OVERWRITE)
	private int forceOverwrite;

	public CopyParameter(String teamDest, String teamSrc, String parentFolder, String copyFileOrFolder,
			String copyDestinationFolder, String destinationFileName, int forceOverwrite)
	{
		this.teamDest = teamDest;
		this.teamSrc = teamSrc;
		this.parentFolder = FilePathHelper.buildWindowsPath(parentFolder, false, false);
		this.copyFileOrFolder = copyFileOrFolder;
		this.copyDestinationFolder = FilePathHelper.buildWindowsPath(copyDestinationFolder, false, false);
		this.destinationFileName = destinationFileName;
		this.forceOverwrite = forceOverwrite;
	}
}
