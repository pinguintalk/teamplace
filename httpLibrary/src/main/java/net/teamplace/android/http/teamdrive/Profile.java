package net.teamplace.android.http.teamdrive;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import java.io.IOException;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class Profile
{

	@JsonProperty("Id")
	private String userId;

	@JsonProperty("Email")
	private String email;

	@JsonProperty("Displayname")
	private String displayName;

	@JsonProperty("AboutMe")
	private String aboutMe;


	public Profile()
	{
	}

	public Profile(String userId, String email, String displayName, String aboutMe)
	{
		this.userId = userId;
		this.email = email;
		this.displayName = displayName;
		this.aboutMe = aboutMe;

	}

	public static Profile createFromJson(String json) throws JsonParseException, JsonMappingException, IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, Profile.class);
	}

	public static String createJsonObject(Profile... in) throws JsonProcessingException
	{
		return JsonUtils.getDefaultMapper().writeValueAsString(in.length > 1 ? in : in[0]);
	}

	public String getUserId()
	{
		return userId;
	}

	public void setUserId(String userId)
	{
		this.userId = userId;
	}

	public String getEmail()
	{
		return email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}

	public String getAboutMe()
	{
		return aboutMe;
	}

	public void setAboutMe(String aboutMe)
	{
		this.aboutMe = aboutMe;
	}

}