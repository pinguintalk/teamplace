package net.teamplace.android.http.exception;

public class AlreadyExistException extends CortadoException
{
	/**
     * 
     */
	private static final long serialVersionUID = -7441320134306360811L;

	public AlreadyExistException()
	{
		super("already exists");
	}

	public AlreadyExistException(String strError)
	{
		super(strError);
	}

}
