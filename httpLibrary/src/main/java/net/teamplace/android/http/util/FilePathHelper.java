package net.teamplace.android.http.util;

public class FilePathHelper
{
	private static final String TAG = FilePathHelper.class.getSimpleName();

	public static String buildWindowsPath(String in, boolean removeTrailing, boolean removeLeading)
	{
		String result = in.replace('/', '\\');
		result = result.replace("\\\\", "\\");
		if (result.endsWith("\\") && removeTrailing)
		{
			result = result.substring(0, result.length() - 1);
		}
		if (result.startsWith("\\") && removeLeading)
		{
			result = result.substring(1);
		}
		// replace :
		result = result.replaceAll(":", "_");

		return result;
	}

	public static String buildJavaPath(String in)
	{
		String result = in.replace('\\', '/');
		// result = result.replace("_", ":");

		return result;
	}
}
