package net.teamplace.android.http.request;

import java.io.IOException;
import java.io.InputStream;

public class PreviewInputStream extends InputStream
{
	private final ICloseAble closeAble;
	private final InputStream inputStream;
	private String contentType = "";

	public PreviewInputStream(ICloseAble closeAble, InputStream inputStream, String contentType, int pageCount,
			int width, int height, int maxWidth, int maxHeight)
	{
		super();
		this.closeAble = closeAble;
		this.inputStream = inputStream;
		this.contentType = contentType;
		this.pageCount = pageCount;
		this.width = width;
		this.height = height;
		this.maxWidth = maxWidth;
		this.maxHeight = maxHeight;
	}

	private int pageCount = -1;
	private int width = -1;
	private int height = -1;
	private int maxWidth = -1;
	private int maxHeight = -1;

	public String get_contentType()
	{
		return contentType;
	}

	public int getPageCount()
	{
		return pageCount;
	}

	public int getWidth()
	{
		return width;
	}

	public int getHeight()
	{
		return height;
	}

	public int getMaxWidth()
	{
		return maxWidth;
	}

	public int getMaxHeight()
	{
		return maxHeight;
	}

	@Override
	public int read() throws IOException
	{
		return this.inputStream.read();
	}

	@Override
	public void close() throws IOException
	{
		super.close();
		this.inputStream.close();
		this.closeAble.close();
	}

}
