package net.teamplace.android.http.request.share;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;
import net.teamplace.android.http.json.JsonUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JacksonDataModel
public class ShareCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private ShareParameter shareParameter;

	public ShareCommand(String teamDriveId, String filePath, String recipients, boolean preview, boolean html,
			boolean isPublic, String password, String userText, String subject, long validUntil)
	{
		setCommand(CMD_SHARE);

		this.shareParameter = new ShareParameter(teamDriveId, filePath, recipients, preview, html, isPublic, password,
				userText, subject, validUntil);
	}

	@JsonIgnore
	public String getAsJSONString() throws JsonProcessingException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		return mapper.writeValueAsString(this);
	}

	@JsonIgnore
	public byte[] getAsJSONByteArray() throws JsonProcessingException
	{
		return getAsJSONString().getBytes();
	}
}
