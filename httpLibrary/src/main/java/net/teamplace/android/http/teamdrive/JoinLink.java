package net.teamplace.android.http.teamdrive;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import java.io.IOException;

/**
 * Created by jobol on 14/08/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class JoinLink {

    @JsonProperty("Link")
    public String link;

    @JsonProperty("Code")
    public String code;

    public JoinLink() {
    }

    public static JoinLink createFromJson(String json) throws IOException {
        return JsonUtils.getDefaultMapper().readValue(json, JoinLink.class);
    }

    public static JoinLink[] arrayFromJson(String json) throws IOException {
        return JsonUtils.getDefaultMapper().readValue(json, JoinLink[].class);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class Create {

        @JsonProperty("Role")
        public Integer role;

        public Create() {}

        public Create(Integer role) {
            this.role = role;
        }

        public static String createJsonObject(Create... create) throws JsonProcessingException {
            if (create == null || create.length == 0) {
                return null;
            }
            return JsonUtils.getDefaultMapper().writeValueAsString(create.length > 1 ? create : create[0]);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class Delete {

        @JsonProperty("Code")
        public String code;

        public Delete() {}

        public Delete(String code) {
            this.code = code;
        }

        public static String createJsonObject(Delete... delete) throws JsonProcessingException {
            if (delete == null || delete.length == 0) {
                return null;
            }
            return JsonUtils.getDefaultMapper().writeValueAsString(delete.length > 1 ? delete : delete[0]);
        }


    }
}
