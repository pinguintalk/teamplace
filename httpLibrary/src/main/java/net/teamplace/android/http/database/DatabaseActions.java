package net.teamplace.android.http.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by jobol on 10/06/15.
 */
public abstract class DatabaseActions {

    protected SQLiteDatabase mDb;
    protected SQLiteOpenHelper mOpenHelper;
    protected DatabaseAccessRestrictor mRestrictor;


    public interface DatabaseAction {
        void execute() throws DatabaseLockedException;
    }

    protected DatabaseActions(SQLiteOpenHelper helper, DatabaseAccessRestrictor restrictor) {
        mRestrictor = restrictor;
        mOpenHelper = helper;
        mDb = mOpenHelper.getWritableDatabase();
    }

    protected void executeInTransaction(DatabaseAction action) throws DatabaseLockedException {
        if (mRestrictor.isLocked()) {
            throw new DatabaseLockedException();
        }
        try {
            mDb.beginTransaction();
            action.execute();
            mDb.setTransactionSuccessful();
        } finally {
            mDb.endTransaction();
        }
    }

    protected void executeLockable(DatabaseAction action) throws DatabaseLockedException {
        if (mRestrictor.isLocked()) {
            throw new DatabaseLockedException();
        }
        action.execute();
    }
}
