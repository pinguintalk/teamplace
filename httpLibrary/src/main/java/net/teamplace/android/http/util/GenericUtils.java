package net.teamplace.android.http.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import android.text.TextUtils;

public class GenericUtils
{
	public GenericUtils()
	{
	}

	public static <T> Set<T> emptyIfNull(Set<T> set)
	{
		return set == null ? Collections.<T> emptySet() : set;
	}

	public static <T> List<T> emptyIfNull(List<T> list)
	{
		return list == null ? Collections.<T> emptyList() : list;
	}

	/**
	 * Check whether the given email address is valid. Use an internal Pattern. Taken from SO,
	 * http://stackoverflow.com/a/7882950/913286 .
	 * 
	 * @param target
	 *            CharSequence containing the email address to verify
	 * @return
	 */
	public static boolean isValidEmail(CharSequence target)
	{
		return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
	}

	public static List<String> splitStringByDivider(String toSplit, String divider)
	{
		List<String> list = new ArrayList<String>();

		for (String splitter : toSplit.split(Pattern.quote(divider)))
		{
			list.add(splitter);
		}

		return list;
	}
}
