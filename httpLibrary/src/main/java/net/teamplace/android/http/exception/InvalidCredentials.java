package net.teamplace.android.http.exception;

public class InvalidCredentials extends ConnectFailedException
{
	private static final long serialVersionUID = 1026793431668597099L;

	public InvalidCredentials()
	{
		this("invalid credentials");
	}

	public InvalidCredentials(String strErr)
	{
		super(strErr);
	}
}
