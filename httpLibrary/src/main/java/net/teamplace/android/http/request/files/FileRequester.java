package net.teamplace.android.http.request.files;

import java.io.IOException;
import java.util.ArrayList;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.file.FileVersion;
import net.teamplace.android.http.file.JsonFileVersionParser;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class FileRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public FileRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public ArrayList<FileVersion> getFileVersions(String teamDriveId, String parentFolder, String file)
			throws IOException, ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException,
			TeamDriveNotFoundException, RedirectException {
		Uri uri = getFileVersionUri(teamDriveId, parentFolder, file);

		return JsonFileVersionParser.parseVersionJson(executeStringGETRequest(uri, getMimeJsonHeader(), true, true),
				file, parentFolder);
	}

	/**
	 * @param parentFolder
	 *            - name of the source folder
	 * @param folderOrFile
	 *            - name of the file to delete
	 * @param forceDeleteFolder
	 *            - delete folder if not empty
	 * @return List of {@link FileOperationResult}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws TeamDriveNotFoundException
	 */
	public FileOperationResponse deleteFolderOrFile(String teamDriveId, String parentFolder, String[] folderOrFile,
			int forceDeleteFolder)
			throws XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, JsonParseException,
			JsonMappingException, IOException, TeamDriveNotFoundException, RedirectException {
		Log.d(TAG, "Execute delete file or folder request");

		DeleteCommand command = new DeleteCommand(teamDriveId, parentFolder, folderOrFile, forceDeleteFolder);

		byte[] data = command.getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return FileOperationResponse.createFileOperationResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	/**
	 * @param srcFolder
	 *            - name of the source folder
	 * @param srcFile
	 *            - name of the file to copy
	 * @param dstFolder
	 *            - name of the destination folder to move
	 * @param dstFile
	 *            - name of the copied file, null to use the name of original file
	 * @param forceOverwrite
	 * @return List of {@link FileOperationResult}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws TeamDriveNotFoundException
	 */
	public FileOperationResponse copyFile(String teamDest, String teamSrc, String parentFolder,
			String[] filesOrFoldersToCopy,
			String copyDestinationFolder, String destinationFileName, int forceOverwrite) throws XCBStatusException,
			HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException,
			IOException, TeamDriveNotFoundException, RedirectException {
		Log.d(TAG, "Execute copy file or folder request");

		CopyCommand command = new CopyCommand(teamDest, teamSrc, parentFolder, filesOrFoldersToCopy,
				copyDestinationFolder,
				destinationFileName, forceOverwrite);

		byte[] data = command.getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDest));

		return FileOperationResponse.createFileOperationResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	/**
	 * @param srcFolder
	 *            - name of the source folder
	 * @param srcFile
	 *            - name of the file to move
	 * @param dstFolder
	 *            - name of the destination folder to move * @param dstFile - name of the moved file, null to use the
	 *            name of original file
	 * @param forceOverwrite
	 * @return List of {@link FileOperationResult}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws TeamDriveNotFoundException
	 */
	public FileOperationResponse moveFile(String teamDest, String teamSrc, String parentFolder,
			String[] filesOrFoldersToMove,
			String moveDestinationFolder, String destinationFileName, int forceOverwrite) throws XCBStatusException,
			HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException,
			IOException, TeamDriveNotFoundException, RedirectException {
		Log.d(TAG, "Execute move file or folder request");

		MoveCommand command = new MoveCommand(teamDest, teamSrc, parentFolder, filesOrFoldersToMove,
				moveDestinationFolder,
				destinationFileName, forceOverwrite);

		byte[] data = command.getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDest));

		return FileOperationResponse.createFileOperationResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	/**
	 * @param parentFolder
	 *            - name of the source folder
	 * @param newFolder
	 *            - name of the new file
	 * @param forceOverwrite
	 * @return List of {@link FileOperationResult}
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws TeamDriveNotFoundException
	 */
	public FileOperationResponse createFolder(String teamDriveId, String parentFolder, String createFileOrFolder,
			int forceOverwrite)
			throws XCBStatusException, HttpResponseException, ConnectFailedException, LogonFailedException,
			NotificationException,
			ServerLicenseException, JsonParseException, JsonMappingException, IOException, TeamDriveNotFoundException, RedirectException {
		Log.d(TAG, "Execute create file or folder request");

		CreateCommand command = new CreateCommand(teamDriveId, parentFolder, createFileOrFolder, forceOverwrite);

		byte[] data = command.getAsJSONByteArray();

		Uri uri = getCommandUri(getServerForTeamDriveId(teamDriveId));

		return FileOperationResponse.createFileOperationResponseFromJSON(doJsonUploadPOST(uri, data, true, true));
	}

	private Uri getFileVersionUri(String teamDriveId, String parentFolder, String file)
			throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();
		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_FOLDER, parentFolder.replaceAll("/", "\\\\"));
		builder.appendQueryParameter(PARAM_VERSION_FILE, file);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		return builder.build();
	}
}
