package net.teamplace.android.http.request.comments;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import android.util.Base64;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class ModifyCommentParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_CHECK_ID)
	private String commentId;

	@JsonProperty(PARAM_COMMENT)
	private String comment;

	public ModifyCommentParameter(String commentId, String comment)
	{
		this.commentId = commentId;
		this.comment = Base64.encodeToString(comment.getBytes(), Base64.NO_WRAP);
	}

	public String getCommentId()
	{
		return commentId;
	}

	public void setCommentId(String commentId)
	{
		this.commentId = commentId;
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

}
