package net.teamplace.android.http.exception;

public class EmptyResponseException extends InvalidResponseException
{
	private static final long serialVersionUID = 7724083739214645655L;

	public EmptyResponseException()
	{

	}

	public EmptyResponseException(String strErr)
	{
		super(strErr);
	}
}
