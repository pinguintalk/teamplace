package net.teamplace.android.http.file;

import net.teamplace.android.http.annotation.JacksonDataModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class RemoteFolder extends RemoteFile
{
	public static final int TYPE_STANDARD = 0;
	public static final int TYPE_SHARE_POINT = 1;
	public static final int TYPE_ZIP = 2;

	private final int FLAG_ARCHIVE = 1;
	private final int FLAG_ANALYSIS_NOT_COMPLETED = 2;
	private final int FLAG_DRIVE = 4;

	protected final String SUB = "sub";
	protected final String FILE_COUNT = "files";
	protected final String TYPE = "type";

	private final String ARCHIVES = "archive";
	private final String FILES = "file";
	public static final String FOLDER = "folder";

	@JsonProperty(SUB)
	protected int sub;

	@JsonProperty(FILE_COUNT)
	protected int fileCount;

	@JsonProperty(TYPE)
	private int type;

	@JsonProperty(ARCHIVES)
	private RemoteArchive[] archives;

	@JsonProperty(FILES)
	private RemoteFile[] files;

	@JsonProperty(FOLDER)
	private RemoteFolder[] folders;

	public RemoteFolder()
	{
	}

	public int getSub()
	{
		return sub;
	}

	public int getFileCount()
	{
		return fileCount;
	}

	public int getType()
	{
		return type;
	}

	public RemoteArchive[] getArchives()
	{
		return archives;
	}

	public RemoteFile[] getFiles()
	{
		return files;
	}

	public RemoteFolder[] getFolders()
	{
		return folders;
	}

	public boolean isArchive()
	{
		return (flags & FLAG_ARCHIVE) == FLAG_ARCHIVE;
	}

	public boolean isAnalysisCompleted()
	{
		return !((flags & FLAG_ANALYSIS_NOT_COMPLETED) == FLAG_ANALYSIS_NOT_COMPLETED);
	}

	public boolean isDrive()
	{
		return (flags & FLAG_DRIVE) == FLAG_DRIVE;
	}
}