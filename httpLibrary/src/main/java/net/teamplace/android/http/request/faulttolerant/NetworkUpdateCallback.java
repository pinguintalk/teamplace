package net.teamplace.android.http.request.faulttolerant;

public interface NetworkUpdateCallback extends ProgressCallback
{
	public void onSuccess(String filePath);

	public void onFailure(Exception e);
}
