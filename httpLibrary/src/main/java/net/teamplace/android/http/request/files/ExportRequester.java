package net.teamplace.android.http.request.files;

import java.io.IOException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class ExportRequester extends BasicRequester
{

	public static final String FORMAT_PDF = VALUE_EXPORT_FORMAT_PDF;
	public static final String FORMAT_ZIP = VALUE_EXPORT_FORMAT_ZIP;

	public ExportRequester(Context ctx, Session session, BackendConfiguration backendConfiguration)
	{
		super(ctx, session, backendConfiguration);
	}

	public void export(String teamDriveId, String srcParentPath, String srcName, String destPath, String exportFormat,
			int forceOverwrite)
			throws XCBStatusException,
			HttpResponseException, JsonParseException, JsonMappingException, ConnectFailedException,
			LogonFailedException, NotificationException,
			ServerLicenseException, TeamDriveNotFoundException, IOException, RedirectException {
		executeStringGETRequest(
				getExportUri(teamDriveId, exportFormat, srcParentPath, srcName, destPath, forceOverwrite),
				getMimeJsonHeader(), true, true);
	}

	private Uri getExportUri(String teamDriveId, String exportFormat, String srcPath, String srcFile, String dest,
			int forceOverwrite)
			throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();

		builder.appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendEncodedPath(PARAM_SHELL + "/");
		builder.appendQueryParameter(PARAM_SHELL, VALUE_EXPORT);
		builder.appendQueryParameter(PARAM_EXPORT_FORMAT, exportFormat);
		builder.appendQueryParameter(PARAM_FOLDER, srcPath);
		builder.appendQueryParameter(PARAM_FILE, srcFile);
		builder.appendQueryParameter(PARAM_FORCE_OVERWRITE, String.valueOf(forceOverwrite));
		builder.appendQueryParameter(PARAM_DEST, dest);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		return builder.build();
	}

}
