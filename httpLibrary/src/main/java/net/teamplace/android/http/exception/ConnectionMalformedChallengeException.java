package net.teamplace.android.http.exception;

public class ConnectionMalformedChallengeException extends
		IllegalLogonDataException
{
	public ConnectionMalformedChallengeException(String strMsg)
	{
		super(strMsg);
	}

	/**
     * 
     */
	private static final long serialVersionUID = -6255426007843586319L;

}
