package net.teamplace.android.http.request.account;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

public class JSonRegisterRequestBuilder
{

	private static final String KEY_EMAIL = "Email";
	private static final String KEY_APPLICATIONNAME = "ApplicationName";

	private static final String VALUE_APPLICATIONNAME = "CloudPrinterApp";

	public static byte[] createRegisterRequest(String email)
	{
		JSONObject register = new JSONObject();
		try
		{
			register.put(KEY_APPLICATIONNAME, VALUE_APPLICATIONNAME);
			register.put(KEY_EMAIL, email);

		}
		catch (JSONException e)
		{
		}
		try
		{
			return register.toString().getBytes("utf-8");
		}
		catch (UnsupportedEncodingException e)
		{
			return register.toString().getBytes();
		}

	}

}
