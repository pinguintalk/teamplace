package net.teamplace.android.http.request;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.account.AuthToken;
import net.teamplace.android.http.session.DeviceID;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.UserAgent;
import net.teamplace.android.http.session.configuration.ConfigurationManager;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.util.CloseableConnection;
import net.teamplace.android.http.util.DebugHelper;
import net.teamplace.android.http.util.HeaderInputStream;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.Uri;
import android.util.Base64;
import android.util.Log;
import android.util.Pair;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * Collects and provides several basic methods which can be used by any requester to build single requests.
 * 
 * @author jamic, joorg
 */
public class BasicRequester implements ServerParams
{
	private final String TAG = getClass().getSimpleName();
	private static final int BUFFER_SIZE = 4096;
	private static final int MAX_RETRIES = 1;

	protected final List<NameValuePair> commonHeaders;
	protected final Context context;
	protected ConfigurationManager configurationManager;
	protected TokenManager tokenManager;

	protected Session session;
	protected BackendConfiguration backendConfiguration;

	// Used by show*Header methods
	private static final Object sLogLock = new Object();

	protected boolean isDebugVersion;

	protected BasicRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		this.session = session;
		this.backendConfiguration = backendConfiguration;
		this.commonHeaders = getBasicHeaders(context);
		this.context = context.getApplicationContext();
		this.configurationManager = ConfigurationManager.getInstance();
		this.tokenManager = TokenManager.getInstance();

		this.isDebugVersion = DebugHelper.isDebugVersion(context);
		if (isDebugVersion)
		{
			Log.d(TAG, "debug version, ignoring ssl certs");
			DebugHelper.ignoreSslCerts(context.getApplicationContext());
		}
	}

	protected HttpURLConnection connectAndExecuteGETRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException, JsonParseException, JsonMappingException, IOException, RedirectException {

		if (handleInvalidConfiguration)
		{
			configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration);
		}

		HttpURLConnection connection = null;

		boolean success;
		boolean doRetry;
		int retries = 0;

		do
		{
			success = true;
			doRetry = false;

			try
			{
				if (isDebugVersion)
				{
					Log.d(TAG, "Uri: " + uri.toString());
				}

				connection = openConnection(uri, HTTP_GET);

				if (session.getToken() != null)
				{
					setTokenAuthParams(connection, session.getToken());
				}

				setTimeout(connection, TIMEOUT_CONF);
				setHeader(connection, requestProperties);
				showRequestHeader(connection);

				try
				{
					connection.connect();
				}
				catch (IOException e)
				{
					throw new ConnectFailedException(e);
				}

				showResponseHeader(connection);
				ConnectionErrorHandler.handleConnectionStatus(connection);

				tokenManager.updateTokenFromResponse(context, session, connection);

				if (handleNotifyUpdateConfiguration)
				{
					configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration, connection);
				}
			}
			catch (LogonFailedException e)
			{
				if (retries < MAX_RETRIES)
				{
					doRetry = tokenManager.updateTokenFromAccountManager(context, session, tokenManager.isUpdating());
				}

				Log.e(TAG, "LogonFailedException, doRetry: " + doRetry, e);
				success = false;
				retries++;
				disconnect(connection);
			}
		}
		while (!success && doRetry && retries <= MAX_RETRIES);

		if (!success)
		{
			throw new LogonFailedException("Could not request new token from server");
		}

		return connection;
	}

	protected String executeStringGETRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException, JsonParseException, JsonMappingException, IOException, RedirectException {
		HttpURLConnection conn = null;
		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
			return getStringFromResponse(conn);
		}
		finally
		{
			disconnect(conn);
		}
	}

	protected Pair<String, String> executeStringGetRequestEvaluateEtag(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, JsonParseException, JsonMappingException, IOException, RedirectException {
		HttpURLConnection conn = null;

		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
			String body = getStringFromResponse(conn);
			String etag = conn.getHeaderField(HEADER_ETAG);
			return new Pair<String, String>(body, etag);
		}
		finally
		{
			disconnect(conn);
		}
	}

	protected void executeFireAndForgetGetRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
		}
		finally
		{
			disconnect(conn);
		}
	}

	protected HeaderInputStream executeInputStreamGetRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
			return new HeaderInputStream(new CloseableConnection(conn), conn.getInputStream(), conn.getHeaderFields());
		}
		finally
		{
			// do not disconnect because the inputStream would be closed
		}
	}

	protected Map<String, List<String>> executeHeaderGETRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration) throws XCBStatusException,
			HttpResponseException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
			return conn.getHeaderFields();
		}
		finally
		{
			disconnect(conn);
		}
	}

	protected String executeSingleHeaderFielGETRequest(Uri uri, List<NameValuePair> requestProperties,
			boolean handleInvalidConfiguration, boolean handleNotifyUpdateConfiguration, String headerFielName)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		try
		{
			conn = connectAndExecuteGETRequest(uri, requestProperties, handleInvalidConfiguration,
					handleNotifyUpdateConfiguration);
			return conn.getHeaderField(headerFielName);
		}
		finally
		{
			disconnect(conn);
		}
	}

	protected HttpURLConnection connectAndExecutePOSTRequest(Uri uri, List<NameValuePair> requestProperties,
			int contentLength, boolean handleConfiguration) throws ConnectFailedException, XCBStatusException,
			HttpResponseException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException, IOException, RedirectException {
		if (isDebugVersion)
		{
			Log.d(TAG, "Uri: " + uri.toString());
		}

		if (handleConfiguration)
		{
			configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration);
		}

		HttpURLConnection connection = openConnection(uri, HTTP_POST);

		if (session.getToken() != null)
		{
			setTokenAuthParams(connection, session.getToken());
		}

		setTimeout(connection, TIMEOUT);
		setHeader(connection, requestProperties);
		connection.setDoOutput(true);
		connection.setFixedLengthStreamingMode(contentLength);
		showRequestHeader(connection);

		return connection;
	}

	protected void setHeader(HttpURLConnection connection, List<NameValuePair> customHeader)
	{
		if (customHeader != null)
		{
			for (NameValuePair header : customHeader)
			{
				connection.setRequestProperty(header.getName(), header.getValue());
			}
		}

		setCommonHeaders(connection);
	}

	/**
	 * Sets the request headers that are generally necessary to execute a request to the connection.
	 * 
	 * @param conn
	 */
	private void setCommonHeaders(HttpURLConnection conn)
	{
		for (NameValuePair header : commonHeaders)
		{
			conn.setRequestProperty(header.getName(), header.getValue());
		}
	}

	private List<NameValuePair> getBasicHeaders(Context context)
	{
		List<NameValuePair> headers = new LinkedList<NameValuePair>();

		headers.add(new BasicNameValuePair(HEADER_X_DEVICE_ID, DeviceID.getDeviceID(context)));
		headers.add(new BasicNameValuePair(HEADER_USER_AGENT, UserAgent.getUserAgent(context)));

		try
		{
			headers.add(new BasicNameValuePair(HEADER_X_CB_CLIENT_INFO, context.getPackageManager().getPackageInfo(
					context.getPackageName(), 0).versionName));
		}
		catch (NameNotFoundException e)
		{
			headers.add(new BasicNameValuePair(HEADER_X_CB_CLIENT_INFO, "0"));
		}

		return headers;
	}

	final protected void showRequestHeader(HttpURLConnection connection)
	{
		if (!isDebugVersion)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		String line = "--------------------------------------------------\n";
		sb.append(line);
		sb.append("# Request Header: \n");

		if (connection == null)
		{
			sb.append("[W]: connection is null\n");
		}
		else
		{
			Map<String, List<String>> requestHeader = connection.getRequestProperties();
			if (requestHeader == null)
			{
				sb.append("[W]: requestHeader is null\n");
			}
			else
			{
				for (String headerKey : requestHeader.keySet())
				{
					List<String> values = requestHeader.get(headerKey);
					for (String value : values)
					{
						sb.append(headerKey + " = " + value);
						sb.append("\n");
					}
				}
			}
		}
		sb.append(line);
		String output = sb.toString();

		// Make the output on logcat atomic
		synchronized (sLogLock)
		{
			Log.d(TAG, output);
		}
	}

	final protected void showResponseHeader(HttpURLConnection connection)
	{
		if (!isDebugVersion)
		{
			return;
		}
		StringBuilder sb = new StringBuilder();
		String line = "--------------------------------------------------\n";
		sb.append(line);
		sb.append("# Response Header: \n");

		if (connection == null)
		{
			sb.append("[W]: connection is null\n");
		}
		else
		{
			Map<String, List<String>> responseHeader = connection.getHeaderFields();
			if (responseHeader == null)
			{
				sb.append("[W]: responseHeader is null\n");
			}
			else
			{
				for (String headerKey : responseHeader.keySet())
				{
					List<String> values = responseHeader.get(headerKey);
					for (String value : values)
					{
						sb.append(headerKey + " = " + value);
						sb.append("\n");
					}
				}
			}
		}
		sb.append(line);
		String output = sb.toString();

		// Make the output on logcat atomic
		synchronized (sLogLock)
		{
			Log.w(TAG, output);
		}
	}

	protected HttpURLConnection openConnection(Uri uri, String requestMethod) throws MalformedURLException,
			ConnectFailedException
	{
		try
		{
			HttpURLConnection conn = (HttpURLConnection) new URL(uri.toString()).openConnection();

			if (requestMethod != null)
			{
				conn.setRequestMethod(requestMethod);
			}

			return conn;
		}
		catch (IOException e)
		{
			throw new ConnectFailedException(e);
		}
	}

	protected void doUpload(HttpURLConnection conn, TimeoutTimerTask ttt, byte[] data) throws ConnectFailedException
	{
		new Timer().schedule(ttt, TIMEOUT, TIMEOUT);

		InputStream is = new ByteArrayInputStream(data);
		OutputStream os = null;

		try
		{
			os = conn.getOutputStream();
			writeToOutputStream(is, os, ttt);
		}
		catch (IOException e)
		{
			conn.disconnect();
			throw new ConnectFailedException(e);
		}
		finally
		{
			try
			{
				if (os != null)
				{
					os.close();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			try
			{
				if (is != null)
				{
					is.close();
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}

	protected String doJsonUploadPOST(Uri server, byte[] data, boolean handleConfiguration,
			boolean handleNotifyUpdateConfiguration) throws XCBStatusException, HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException, IOException, RedirectException {
		HttpURLConnection connection = null;
		TimeoutTimerTask ttt = null;

		try
		{
			boolean success;
			boolean doRetry;
			int retries = 0;

			do
			{
				success = true;
				doRetry = false;

				try
				{
					if (isDebugVersion)
					{
						Log.d(TAG, new String(data));
					}

					int contentLength = data.length;

					connection = connectAndExecutePOSTRequest(server, getJSONHeader(contentLength), contentLength,
							handleConfiguration);

					ttt = new TimeoutTimerTask(connection);
					doUpload(connection, ttt, data);

					showResponseHeader(connection);
					ConnectionErrorHandler.handleConnectionStatus(connection);

					tokenManager.updateTokenFromResponse(context, session, connection);

					if (handleNotifyUpdateConfiguration)
					{
						configurationManager.updateConfigurationIfNeeded(context, session, backendConfiguration, connection);
					}
				}
				catch (LogonFailedException e)
				{
					if (ttt != null)
					{
						ttt.cancel();
					}

					if (retries < MAX_RETRIES)
					{
						doRetry = tokenManager.updateTokenFromAccountManager(context, session,
								tokenManager.isUpdating());
					}

					Log.e(TAG, "LogonFailedException, doRetry: " + doRetry, e);

					success = false;
					retries++;
					disconnect(connection);
				}
			}
			while (!success && doRetry && retries <= MAX_RETRIES);

			if (!success)
			{
				throw new LogonFailedException("Could not request new token from server");
			}

			return getStringFromResponse(connection);
		}
		finally
		{
			if (ttt != null)
			{
				ttt.cancel();
			}
			disconnect(connection);
		}
	}

	/**
	 * Adds basic authorization headers to the connection.
	 * 
	 * @param conn
	 * @param user
	 * @param password
	 */
	protected void setBasicAuthParams(HttpURLConnection conn, String user, String password)
	{
		conn.setRequestProperty(HEADER_AUTHORIZATION, AUTHENTICATION_BASIC + getBase64Creds(user, password));
	}

	/**
	 * Adds the headers necessary for token authentication to the connection.
	 * 
	 * @param conn
	 * @param user
	 * @param token
	 */
	protected void setTokenAuthParams(HttpURLConnection conn, AuthToken token)
	{
		conn.setRequestProperty(HEADER_X_TOKENTYPE, token.getTokenType());
		conn.setRequestProperty(HEADER_X_TOKEN, token.getTokenStr());
	}

	protected void setTimeout(HttpURLConnection conn, int timeout)
	{
		conn.setReadTimeout(timeout);
		conn.setConnectTimeout(timeout);
	}

	/**
	 * Prints the connection parameters (complete URL, request method and all header fields) to the console.
	 * 
	 * @param conn
	 */
	// private void logConn(HttpURLConnection conn)
	// {
	// // Log.d(TAG, "Requesting " + conn.getURL().toString() + " (" + conn.getRequestMethod() + ")");
	//
	// Map<String, List<String>> headers = conn.getRequestProperties();
	// for (Iterator<Entry<String, List<String>>> iterator = headers.entrySet().iterator(); iterator.hasNext();)
	// {
	// Entry<String, List<String>> entry = iterator.next();
	// for (Iterator<String> iterator2 = entry.getValue().iterator(); iterator2.hasNext();)
	// {
	// String value = iterator2.next();
	// if (trustAllCertificates || !(entry.getKey().contains("HEADER_AUTHORIZATION") ||
	// value.contains(AUTHENTICATION_BASIC)))
	// {
	// // Log.d(TAG, "Header \"" + entry.getKey() + "\" = \"" + value + "\"");
	// }
	// }
	// }
	// }

	protected void writeToOutputStream(InputStream is, OutputStream os, TimeoutTimerTask ttt) throws IOException
	{
		byte[] buffer = new byte[BUFFER_SIZE];
		int bytesRead;

		while ((bytesRead = is.read(buffer)) != -1)
		{
			if (ttt != null)
			{
				ttt.update();
			}
			os.write(buffer, 0, bytesRead);
			os.flush();
		}
	}

	/**
	 * @param user
	 * @param password
	 * @return "user:password" as Base64 encoded String
	 */
	private String getBase64Creds(String user, String password)
	{
		String base64 = null;
		try
		{
			base64 = Base64.encodeToString((user + ":" + password).getBytes(PASSWORD_ENCODING), Base64.NO_WRAP);
		}
		catch (UnsupportedEncodingException e)
		{
		}

		return base64;
	}

	/**
	 * @param conn
	 * @return the response body as String
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws HttpResponseException
	 * @throws IOException
	 * @throws UnreachableException
	 */
	protected String getStringFromResponse(HttpURLConnection conn) throws ConnectFailedException,
			HttpResponseException, LogonFailedException
	{
		InputStream is = null;
		try
		{
			is = conn.getInputStream();
			StringBuilder strBuilder = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line;

			while ((line = reader.readLine()) != null)
			{
				strBuilder.append(line);
			}

			is.close();

			String responseString = strBuilder.toString();

			if (isDebugVersion)
			{
				Log.d(TAG, responseString);
			}

			return responseString;
		}
		catch (IOException e)
		{
			throw new ConnectFailedException(e);
		}
		finally
		{
			if (is != null)
			{
				try
				{
					is.close();
				}
				catch (IOException ex)
				{
				}
			}
		}
	}

	protected class TimeoutTimerTask extends TimerTask
	{

		private long lastUpdate = 0l;
		private HttpURLConnection conn;

		public TimeoutTimerTask(HttpURLConnection conn)
		{
			this.conn = conn;
		}

		synchronized void update()
		{
			lastUpdate = System.currentTimeMillis();
		}

		@Override
		public synchronized boolean cancel()
		{
			conn = null;
			return super.cancel();
		}

		@Override
		public synchronized void run()
		{
			if ((lastUpdate + TIMEOUT) < System.currentTimeMillis())
			{
				// System.out.println("No transfer activity since " + TIMEOUT + "ms, killing connection.");
				try
				{
					if (conn != null)
					{
						conn.disconnect();
					}
				}
				catch (Exception e)
				{
				}
				cancel();
			}

		}
	}

	protected void disconnect(HttpURLConnection conn)
	{
		if (conn != null)
		{
			try
			{
				conn.disconnect();
			}
			catch (Exception e)
			{
			}
		}
	}

	protected void closeStream(Closeable closeable)
	{
		try
		{
			if (closeable != null)
			{
				closeable.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	protected int asInt(HttpURLConnection conn, String param, int defaultValue)
	{
		String val = conn.getRequestProperty(param);
		if (val == null)
		{
			return defaultValue;
		}
		try
		{
			return Integer.parseInt(val);
		}
		catch (NumberFormatException e)
		{
			return defaultValue;
		}

	}

	protected String getFirstHeaderValue(Map<String, List<String>> headers, String key)
	{
		if (headers == null || key == null)
		{
			return null;
		}
		List<String> values = headers.get(key);
		if (values == null || values.size() == 0)
		{
			return null;
		}
		return values.get(0);
	}

	protected List<NameValuePair> getMimeJsonHeader()
	{
		List<NameValuePair> properties = new LinkedList<NameValuePair>();

		properties.add(new BasicNameValuePair(HEADER_ACCEPT, MIME_JSON));

		return properties;
	}

	protected List<NameValuePair> getJSONHeader(int contentLength)
	{
		List<NameValuePair> properties = new LinkedList<NameValuePair>();

		properties.add(new BasicNameValuePair(HEADER_ACCEPT, MIME_JSON));
		properties.add(new BasicNameValuePair(HEADER_CONTENT_TYPE, MIME_JSON));
		properties.add(new BasicNameValuePair(HEADER_CONTENT_ENCODING, ENCODING_UTF8));
		properties.add(new BasicNameValuePair(HEADER_CONTENT_LENGTH, String.valueOf(contentLength)));

		return properties;
	}

	protected Uri getCommandUri(String server)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();
		builder.appendEncodedPath("cmd/");

		return builder.build();
	}

	/**
	 * If teamDriveId is null this method returns the default server string from configuration. Otherwise it matches the
	 * IDs saved in session to get the TeamDrive server string.
	 * 
	 * @param teamDriveId
	 * @return The server string for teamDriveID (or default server string if teamDriveID was null), or null if no
	 *         TeamDrive was found.
	 * @throws TeamDriveNotFoundException
	 */
	protected String getServerForTeamDriveId(String teamDriveId) throws TeamDriveNotFoundException
	{
		if (teamDriveId == null)
		{
			return session.getConfiguration().getSystemConfiguration().getHost();
		}
		else
		{
			Map<String, TeamDrive> drivesInSession = session.getTeamDriveMap();
			if (drivesInSession != null)
			{
				TeamDrive drive = drivesInSession.get(teamDriveId);
				if (drive != null)
				{
					return drive.getFileServiceUrl();
				}
			}
			throw new TeamDriveNotFoundException("TeamDrive with ID: " + teamDriveId + " not found");
		}
	}
}
