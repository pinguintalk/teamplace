package net.teamplace.android.http.exception;

public class LogonInformationNeeded extends LogonFailedException
{
	private static final long serialVersionUID = -8651849643595196159L;

	private final String _daServer;
	private final String _daUser;
	private boolean _fUserFixed = false;
	private boolean _fPWSaveAllowed = true;

	public LogonInformationNeeded(String strServer, String strUser)
	{
		_daServer = strServer;
		_daUser = strUser;
	}

	public final boolean isUserFixed()
	{
		return _fUserFixed;
	}

	public final void setUserNameAsFixed(boolean f)
	{
		_fUserFixed = f;
	}

	public final boolean isPwSaveAllowed()
	{
		return _fPWSaveAllowed;
	}

	public final void setPwSaveAllow(boolean f)
	{
		_fPWSaveAllowed = f;
	}

	public String getUser()
	{
		return _daUser == null ? "" : _daUser;
	}

	public String getServer()
	{
		return _daServer;
	}
}
