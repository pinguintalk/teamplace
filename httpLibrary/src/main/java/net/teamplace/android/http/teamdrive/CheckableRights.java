package net.teamplace.android.http.teamdrive;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public abstract class CheckableRights
{
	int rights;

	public CheckableRights(int rights)
	{
		this.rights = rights;
	}

	public boolean can(int... toCheck)
	{
		int mask = 0;

		for (int flag : toCheck)
		{
			mask |= flag;
		}
		return (rights & mask) == mask;
	}

	public boolean canAny(int... toCheck)
	{
		int mask = 0;

		for (int flag : toCheck)
		{
			mask |= flag;
		}
		return (rights & mask) > 0;
	}

	public int getRights()
	{
		return rights;
	}

	public static class Serializer extends JsonSerializer<CheckableRights>
	{
		@Override
		public void serialize(CheckableRights arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException,
				JsonProcessingException
		{
			arg1.writeNumber(arg0.rights);
		}
	}
}
