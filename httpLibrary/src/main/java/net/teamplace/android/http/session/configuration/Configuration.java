package net.teamplace.android.http.session.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.json.JsonUtils;

import org.apache.commons.io.IOUtils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class Configuration
{
	public static final String TABLE_NAME = "table_configuration";

	private static final String COL_VALID_TO = "validTo";

	public static final String CREATE_TABLE = "create table " + TABLE_NAME + "(" + SessionSQLiteHelper.COLUMN_ID
			+ " integer primary key autoincrement, " + ServerInformation.TABLE_PART + ConfigurationSchema.TABLE_PART
			+ User.TABLE_PART + UserRights.TABLE_PART + DriversHash.TABLE_PART + SystemConfiguration.TABLE_PART + ", "
			+ WorkplaceInformation.TABLE_PART + ", " + COL_VALID_TO + " integer);";

	private long id = -1;

	@JsonProperty("Server")
	private ServerInformation serverInformation;

	@JsonProperty("Schema")
	private ConfigurationSchema schema;

	@JsonProperty("User")
	private User user;

	@JsonProperty("GUI")
	private UserRights userRights;

	@JsonProperty("Drivers")
	private DriversHash driversHash;

	@JsonProperty("System")
	private SystemConfiguration systemConfiguration;

	@JsonProperty("Workplace")
	private WorkplaceInformation workplaceInformation;

	@JsonProperty("Folders")
	private FolderType[] folders;

	@JsonProperty("Shell")
	private ShellObject[] shell;

	private long validTo = -1;

	public Configuration()
	{
	}

	public Configuration(Cursor cursor)
	{
		serverInformation = new ServerInformation(cursor);
		schema = new ConfigurationSchema(cursor);
		user = new User(cursor);
		userRights = new UserRights(cursor);
		driversHash = new DriversHash(cursor);
		systemConfiguration = new SystemConfiguration(cursor);
		workplaceInformation = new WorkplaceInformation(cursor);
		validTo = cursor.getLong(cursor.getColumnIndex(COL_VALID_TO));
	}

	public long getId()
	{
		return id;
	}

	public void setId(long id)
	{
		this.id = id;
	}

	public ServerInformation getServerInformation()
	{
		return serverInformation;
	}

	public ConfigurationSchema getSchema()
	{
		return schema;
	}

	public User getUser()
	{
		return user;
	}

	public UserRights getUserRights()
	{
		return userRights;
	}

	public DriversHash getDriversHash()
	{
		return driversHash;
	}

	public SystemConfiguration getSystemConfiguration()
	{
		return systemConfiguration;
	}

	public WorkplaceInformation getWorkplaceInformation()
	{
		return workplaceInformation;
	}

	public FolderType[] getFolders()
	{
		return folders;
	}

	@JsonIgnore
	public void setFolders(FolderType[] folders)
	{
		this.folders = folders;
	}

	public ShellObject[] getShell()
	{
		return shell;
	}

	@JsonIgnore
	public void setShell(ShellObject[] shell)
	{
		this.shell = shell;
	}

	@SuppressLint("UseSparseArrays")
	public Map<Integer, FolderType> getFoldersAsMap()
	{
		Map<Integer, FolderType> map = new HashMap<Integer, FolderType>();

		for (FolderType folder : folders)
		{
			map.put(folder.getId(), folder);
		}

		return map;
	}

	public ShellObject getShellObjectByType(String type)
	{
		for (ShellObject object : shell)
		{
			if (object.getType().equals(type))
			{
				return object;
			}
		}

		return null;
	}

	public void updateValidTo()
	{
		validTo = (System.currentTimeMillis() / 1000L) + systemConfiguration.getValidity();
	}

	public boolean isValid()
	{
		return ((System.currentTimeMillis() / 1000L) < validTo);
	}

	public static Configuration createConfigurationFromJSON(String jsonString) throws JsonParseException,
			JsonMappingException, IOException
	{
		return createConfigurationFromJSON(jsonString.getBytes());
	}

	public static Configuration createConfigurationFromJSON(InputStream is) throws JsonParseException,
			JsonMappingException, IOException
	{
		return createConfigurationFromJSON(IOUtils.toByteArray(is));
	}

	public static Configuration createConfigurationFromJSON(byte[] jsonBytes) throws JsonParseException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		Configuration config = mapper.readValue(jsonBytes, Configuration.class);

		config.updateValidTo();

		return config;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.putAll(serverInformation.getAsContentValues());
		values.putAll(schema.getAsContentValues());
		values.putAll(user.getAsContentValues());
		values.putAll(userRights.getAsContentValues());
		values.putAll(driversHash.getAsContentValues());
		values.putAll(systemConfiguration.getAsContentValues());
		values.putAll(workplaceInformation.getAsContentValues());
		values.put(COL_VALID_TO, validTo);

		return values;
	}
}
