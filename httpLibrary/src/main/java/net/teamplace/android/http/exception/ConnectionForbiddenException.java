package net.teamplace.android.http.exception;

public class ConnectionForbiddenException extends ConnectFailedException
{
	private static final long serialVersionUID = 7462007412339213971L;

	public ConnectionForbiddenException(String strErr)
	{
		super(strErr);
	}

	public ConnectionForbiddenException()
	{
		super("Access denied");
	}
}
