package net.teamplace.android.http.request.smartfiling;

import java.io.Serializable;

public class FileVersion implements Serializable, Comparable<FileVersion>
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4864834499578810780L;

	private String rootFileName;
	private String parentFolderPath;
	private long size;
	private String fullName;
	private String creatorSID;
	private int versionNumber;
	private long dateModified;
	private String streamName;

	public FileVersion(String rootFileName, String parentFolderPath, long size, String fullName, String creatorSID,
			int versionNumber, long dateModified, String streamName)
	{
		super();
		this.rootFileName = rootFileName;
		this.parentFolderPath = parentFolderPath;
		this.size = size;
		this.fullName = fullName;
		this.creatorSID = creatorSID;
		this.versionNumber = versionNumber;
		this.dateModified = dateModified;
		this.streamName = streamName;
	}

	public String getRootFileName()
	{
		return rootFileName;
	}

	public String getParentFolderPath()
	{
		return parentFolderPath;
	}

	public long getSize()
	{
		return size;
	}

	public String getFullName()
	{
		return fullName;
	}

	public String getCreatorSID()
	{
		return creatorSID;
	}

	public int getVersionNumber()
	{
		return versionNumber;
	}

	public long getDateModified()
	{
		return dateModified * 1000;
	}

	public String getStreamName()
	{
		return streamName;
	}

	@Override
	public int compareTo(FileVersion another)
	{
		return Long.valueOf(another.getDateModified()).compareTo(Long.valueOf(getDateModified()));
	}

}
