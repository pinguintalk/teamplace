package net.teamplace.android.http.exception;

public class PreviewPageOutOfRangeException extends CortadoException
{

	/**
     * 
     */
	private static final long serialVersionUID = -7875764171047339918L;
	private int _pageCount = -1;

	public PreviewPageOutOfRangeException(String msg, int pages)
	{
		super(msg);
		_pageCount = pages;
	}

	public int getPageCount()
	{
		return _pageCount;
	}

}
