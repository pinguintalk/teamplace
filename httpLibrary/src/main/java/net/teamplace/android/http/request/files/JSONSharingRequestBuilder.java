package net.teamplace.android.http.request.files;

import java.io.UnsupportedEncodingException;

import net.teamplace.android.http.request.ServerParams;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.util.Base64;
import android.util.Log;


public class JSONSharingRequestBuilder implements ServerParams
{
	private static final String CMD = "cmd";
	private static final String CMD_SHARE = "share";
	private static final String PARAMS = "params";

	@SuppressLint("LongLogTag")
	public static byte[] createSharingJSON(String filePath, String recipients, boolean preview, boolean everyone,
			String userText)
	{
		try
		{
			JSONObject root = new JSONObject();
			JSONObject params = new JSONObject();

			root.put(CMD, CMD_SHARE);

			Log.d("JSONSharingRequestBuilder", "Check");

			params.put(PARAM_CHECK_TEAM, buildWindowsPath(filePath, true, true));
			// params.put(PARAM_CHECK_FILE, filePath.replaceAll("/", "\\\\"));
			params.put(PARAM_SEND_TO, recipients);
			params.put(PARAM_PREVIEW_FILE, preview);
			params.put(PARAM_PUBLIC, everyone);
			params.put(PARAM_USER_TEXT, new String(Base64.encode(userText.getBytes(), Base64.NO_WRAP)));

			root.put(PARAMS, params);

			try
			{
				return root.toString().getBytes("utf-8");
			}
			catch (UnsupportedEncodingException e)
			{
				return root.toString().getBytes();
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}

	private static String buildWindowsPath(String in, boolean removetrailing, boolean removeLeading)
	{
		String result = in.replace('/', '\\');
		result = result.replace("\\\\", "\\");
		if (result.endsWith("\\") && removetrailing)
		{
			result = result.substring(0, result.length() - 1);
		}
		if (result.startsWith("\\") && removeLeading)
		{
			result = result.substring(1);
		}
		// replace :
		result = result.replaceAll(":", "_");

		return result;
	}

	public static byte[] createSharingJSON(String filePath, String recipients, boolean preview, boolean html,
			boolean everyone, String password,
			String userText, String validUntil)
	{
		try
		{
			JSONObject root = new JSONObject();
			JSONObject params = new JSONObject();

			root.put(CMD, CMD_SHARE);

			params.put(PARAM_CHECK_TEAM, filePath);
			params.put(PARAM_SEND_TO, recipients);
			params.put(PARAM_PREVIEW_FILE, preview);
			params.put(PARAM_HTML, html);
			params.put(PARAM_PUBLIC, everyone);
			params.put(PARAM_PASSWORD, password);
			params.put(PARAM_USER_TEXT, userText);
			params.put(PARAM_VALID_UNTIL, validUntil);

			root.put(PARAMS, params);

			try
			{
				return root.toString().getBytes("utf-8");
			}
			catch (UnsupportedEncodingException e)
			{
				return root.toString().getBytes();
			}
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}

		return null;
	}
}
