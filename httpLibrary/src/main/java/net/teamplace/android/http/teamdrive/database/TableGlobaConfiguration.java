package net.teamplace.android.http.teamdrive.database;

import net.teamplace.android.http.database.util.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;


public class TableGlobaConfiguration
{

	public static final String TABLE = "table_global_configuration";

	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_MAX_FREE_DRIVES = "max_free_drives";
	public static final String COLUMN_POTENTIAL_RIGHTS = "potential_rights";
	public static final String COLUMN_RIGHTS = "rights";
	public static final String COLUMN_MAX_USERS_FREE_TEAMDRIVE = "max_users_free_teamdrive";
	public static final String COLUMN_MAX_USERS_PREMIUM_TEAMDRIVE = "max_users_premium_teamdrive";
	public static final String COLUMN_DAYS_BEFORE_FREE_TEAMDRIVES_EXPIRE = "days_before_free_teamdrives_expire";
	public static final String COLUMN_ETAG = "etag";

	public static final int SINGLE_ROW_ID = 0;

	private static final String[] ALL_COLUMNS = new String[] {COLUMN_ID, COLUMN_MAX_FREE_DRIVES,
			COLUMN_POTENTIAL_RIGHTS,
			COLUMN_RIGHTS, COLUMN_MAX_USERS_FREE_TEAMDRIVE, COLUMN_MAX_USERS_PREMIUM_TEAMDRIVE,
			COLUMN_DAYS_BEFORE_FREE_TEAMDRIVES_EXPIRE, COLUMN_ETAG};
	private static final String[] CREATION_INFO = new String[] {"INTEGER PRIMARY KEY", "INTEGER", "INTEGER",
			"INTEGER", "INTEGER", "INTEGER",
			"INTEGER", "INTEGER"};

	private static final String CREATE_TABLE;

	static
	{
		CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ TABLE
				+ " ("
				+ DatabaseUtils.buildCreationColumnList(ALL_COLUMNS, CREATION_INFO)
				+ ");";
	}

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 1:
				DatabaseUtils.modifyColumnSet(db, TABLE, ALL_COLUMNS, CREATION_INFO,
						new String[]{COLUMN_MAX_FREE_DRIVES});
				db.execSQL("UPDATE " + TABLE + " SET " + COLUMN_ETAG + " = NULL;");
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableGlobaConfiguration.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}
}
