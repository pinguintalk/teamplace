package net.teamplace.android.http.request.account;

import android.content.Context;
import android.database.SQLException;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.CortadoException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.configuration.Configuration;
import net.teamplace.android.http.teamdrive.Profile;

import java.io.IOException;

/***
 * <b>Class Overview</b>
 * <p>
 * This class is used to get a client configuration from default Cortado Workplace App Server
 * 
 * @author jamic
 */
public class ProfileRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public ProfileRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	/***
	 * Requests a profile of current logged user.
	 * 
	 * @return {@link Configuration}
	 * @throws ConnectFailedException
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public Profile getUserProfile() throws ConnectFailedException, XCBStatusException, HttpResponseException,
			LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException, RedirectException {
		return Profile.createFromJson(executeStringGETRequest(getProfileUri(), getMimeJsonHeader(),
				false, false));
	}

	private Uri getProfileUri()
	{
		Uri.Builder builder = Uri.parse(backendConfiguration.userProfile()).buildUpon();

		return builder.build();

	}


}
