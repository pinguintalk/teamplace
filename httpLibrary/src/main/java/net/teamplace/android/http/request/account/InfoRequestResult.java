package net.teamplace.android.http.request.account;

import android.os.Parcel;

public class InfoRequestResult extends ResponseResult
{
	public static final int RESULT_SUCCESS = 1;
	public static final int RESULT_ERR_UNKNOWN_REQUEST = 0;
	public static final int RESULT_ERR = 9;

	public InfoRequestResult(String message, int resultCode, Boolean success)
	{
		super(message, resultCode, success);
	}

	public InfoRequestResult(Parcel in)
	{
		super(in);
	}
}
