package net.teamplace.android.http.request.account;

public class RegistrationResult
{
	private final String TAG = getClass().getSimpleName();

	private boolean success;
	private int responseCode;

	public RegistrationResult(boolean success, int responseCode)
	{
		this.success = success;
		this.responseCode = responseCode;
	}

	public boolean isSuccess()
	{
		return success;
	}

	public void setSuccess(boolean success)
	{
		this.success = success;
	}

	public int getResponseCode()
	{
		return responseCode;
	}

	public void setResponseCode(int responseCode)
	{
		this.responseCode = responseCode;
	}
}
