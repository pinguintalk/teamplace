package net.teamplace.android.http.teamdrive.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Pair;

import net.teamplace.android.http.database.DatabaseAccessRestrictor;
import net.teamplace.android.http.database.DatabaseActions;
import net.teamplace.android.http.database.DatabaseLockedException;
import net.teamplace.android.http.database.util.AutoIncrementSQLiteStatement;
import net.teamplace.android.http.teamdrive.CcsRights;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.Payment;
import net.teamplace.android.http.teamdrive.Rights;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;


public class TeamDriveDatabaseActions extends DatabaseActions
{

	private static TeamDriveDatabaseActions instance;

	private TeamDriveDatabaseActions(Context context, DatabaseAccessRestrictor restrictor)
	{
		super(new TeamDriveSQLiteOpenHelper(context), restrictor);
	}

	public static synchronized TeamDriveDatabaseActions getInstance() {
		if (instance == null) {
			throw new IllegalStateException("not initialized");
		}
		return instance;
	}

	public static synchronized void init(Context context, DatabaseAccessRestrictor restrictor) {
		if (instance != null) {
			throw new IllegalStateException("already initialized");
		}
		instance = new TeamDriveDatabaseActions(context, restrictor);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		throw new CloneNotSupportedException();
	}

	public synchronized void clearAllTables()
	{
		try {
			mDb.beginTransaction();
			String[] allTableNames = new String[]{
					TableDriveEtags.TABLE,
					TableGlobaConfiguration.TABLE,
					TableInvitationEtags.TABLE,
					TableInvitations.TABLE,
					TableTeamDrives.TABLE,
					TableTeamDriveUsers.TABLE,
					TableUsers.TABLE
			};
			for (String tableName : allTableNames) {
				mDb.delete(tableName, null, null);
			}
			mDb.setTransactionSuccessful();
		} finally {
			mDb.endTransaction();
		}

	}

	public synchronized void insertOrUpdateTeamDrives(final TeamDrive... teamDrives) throws DatabaseLockedException {
		executeInTransaction(new DatabaseAction() {
			@Override
			public void execute() {
				basicInsertOrUpdateTeamDrives(teamDrives);
			}
		});
	}

	public synchronized void overwriteTeamDriveDb(final TeamDrive... teamDrives) throws DatabaseLockedException {
		executeInTransaction(new DatabaseAction() {
			@Override
			public void execute() {
				String[] teamDriveTableNames = new String[]
						{
								TableDriveEtags.TABLE,
								TableTeamDriveUsers.TABLE,
								TableUsers.TABLE,
								TableTeamDrives.TABLE
						};
				for (String tableName : teamDriveTableNames) {
					mDb.delete(tableName, null, null);
				}

				basicInsertOrUpdateTeamDrives(teamDrives);
			}
		});
	}

	public synchronized TeamDrive[] getAllTeamDrives()
	{
		Cursor teamDriveCursor = mDb.query(TableTeamDrives.TABLE, null, null, null, null, null, null);
		if (teamDriveCursor.getCount() > 0)
		{
			TeamDrive[] drives = getTeamDrivesFromCursor(teamDriveCursor);
			teamDriveCursor.close();

			for (TeamDrive drive : drives)
			{
				drive.setUsers(getUsersForTeamDrive(drive.getId()));
				drive.setInvitedUsers(getInvitedUsersForTeamDrive(drive.getId()));
			}
			return drives;
		}
		return new TeamDrive[0];
	}

	public synchronized User[] getUsersForTeamDrive(String driveId)
	{
		return getUsersForTeamDrive(driveId, TableUsers.TYPE_USER);
	}

	public synchronized User[] getInvitedUsersForTeamDrive(String driveId)
	{
		return getUsersForTeamDrive(driveId, TableUsers.TYPE_INVITED);
	}

	public synchronized void updateUserImageEtag(String userId, String etag)
	{
		ContentValues cv = new ContentValues();
		cv.put(TableUsers.COLUMN_USER_IMG_ETAG, etag);
		mDb.updateWithOnConflict(TableUsers.TABLE, cv, TableUsers.COLUMN_ID + " = ?", new String[]{userId},
				SQLiteDatabase.CONFLICT_IGNORE);
	}

	public synchronized String getUserImageEtag(String userId)
	{
		String etag = null;
		Cursor etagCursor = mDb.query(TableUsers.TABLE, new String[]{TableUsers.COLUMN_USER_IMG_ETAG},
				TableUsers.COLUMN_ID + " = ?", new String[]{userId}, null, null, null);
		if (etagCursor.moveToFirst())
		{
			etag = etagCursor.getString(etagCursor.getColumnIndexOrThrow(TableUsers.COLUMN_USER_IMG_ETAG));
		}
		return etag;
	}

	public synchronized String getEtagForTeamDrive(String driveId)
	{
		String etag = null;
		Cursor etagCursor = mDb.query(TableDriveEtags.TABLE, null, TableDriveEtags.COLUMN_TEAMDRIVE_ID + " = ?",
				new String[] {driveId}, null, null, null);
		if (etagCursor.moveToFirst())
		{
			etag = etagCursor.getString(etagCursor.getColumnIndexOrThrow(TableDriveEtags.COLUMN_ETAG));
		}
		etagCursor.close();
		return etag;
	}

	public synchronized String getEtagForAllTeamDrives()
	{
		return getEtagForTeamDrive(TableDriveEtags.ID_ETAG_ALL_DRIVES);
	}

	public synchronized void insertTeamDriveEtag(String driveId, String etag)
	{
		ContentValues values = new ContentValues();
		values.put(TableDriveEtags.COLUMN_TEAMDRIVE_ID, driveId);
		values.put(TableDriveEtags.COLUMN_ETAG, etag);
		mDb.insertWithOnConflict(TableDriveEtags.TABLE, null, values, SQLiteDatabase.CONFLICT_REPLACE);
	}

	public synchronized void insertAllTeamDrivesEtag(String etag)
	{
		insertTeamDriveEtag(TableDriveEtags.ID_ETAG_ALL_DRIVES, etag);
	}

	public synchronized void overwriteInvitationsTable(final IncomingInvitation... invitations) throws DatabaseLockedException {
		executeInTransaction(new DatabaseAction() {
			@Override
			public void execute() {
				mDb.delete(TableInvitations.TABLE, null, null);
				basicInsertOrUpdateInvitations(invitations);
			}
		});
	}

	public synchronized IncomingInvitation[] getInvitations()
	{
		Cursor cursor = mDb.query(true, TableInvitations.TABLE, null, null, null, null, null, null, null, null);
		IncomingInvitation[] invitations = null;
		if (cursor != null && cursor.getCount() > 0)
		{
			invitations = new IncomingInvitation[cursor.getCount()];
			int i = 0;
			while (cursor.moveToNext())
			{
				invitations[i] = new IncomingInvitation(
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_ID)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_EMAIL)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_TEAMDRIVE_ID)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_TEAMDRIVE_NAME)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_TEAMDRIVE_NOTE)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_INVITING_USER_ID)),
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_INVITING_USER_EMAIL)),
						cursor.getString(cursor
								.getColumnIndexOrThrow(TableInvitations.COLUMN_INVITING_USER_DISPLAY_NAME)),
						null,
						cursor.getString(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_INVITATION_MESSAGE)),
						cursor.getInt(cursor.getColumnIndexOrThrow(TableInvitations.COLUMN_ROLE)));
				i++;
			}
		}
		cursor.close();
		return invitations;
	}

	public synchronized void insertOrUpdateInvitationEtag(String invitationId, String etag)
	{
		ContentValues cv = new ContentValues();
		cv.put(TableInvitationEtags.COLUMN_INVITATION_ID, invitationId);
		cv.put(TableInvitationEtags.COLUMN_ETAG, etag);
		mDb.insertWithOnConflict(TableInvitationEtags.TABLE, null, cv, SQLiteDatabase.CONFLICT_REPLACE);
	}

	public synchronized String getEtagForAllInvitations()
	{
		return getEtagForInvitation(TableInvitationEtags.ID_ETAG_ALL_INVITATIONS);
	}

	public synchronized String getEtagForInvitation(String invitationId)
	{
		String etag = null;
		Cursor c = mDb.query(true, TableInvitationEtags.TABLE, null, null, null, null, null, null, null, null);
		if (c.moveToFirst())
		{
			etag = c.getString(c.getColumnIndexOrThrow(TableInvitationEtags.COLUMN_ETAG));
		}
		c.close();
		return etag;
	}

	public synchronized void insertOrUpdateGlobalConfiguration(final GlobalConfiguration config, String etag)
	{
		AutoIncrementSQLiteStatement statement = AutoIncrementSQLiteStatement.compileInsertOrReplace(mDb,
				TableGlobaConfiguration.TABLE,
				new String[]{TableGlobaConfiguration.COLUMN_ID,
					TableGlobaConfiguration.COLUMN_MAX_FREE_DRIVES,
					TableGlobaConfiguration.COLUMN_POTENTIAL_RIGHTS,
					TableGlobaConfiguration.COLUMN_RIGHTS,
					TableGlobaConfiguration.COLUMN_MAX_USERS_FREE_TEAMDRIVE,
					TableGlobaConfiguration.COLUMN_MAX_USERS_PREMIUM_TEAMDRIVE,
					TableGlobaConfiguration.COLUMN_DAYS_BEFORE_FREE_TEAMDRIVES_EXPIRE,
					TableGlobaConfiguration.COLUMN_ETAG});

		statement.bindLong(TableGlobaConfiguration.SINGLE_ROW_ID);
		statement.bindLong(config.getMaxFreeDrives());
		statement.bindLong(config.getPotentialRights().getRights());
		statement.bindLong(config.getRights().getRights());
		statement.bindLong(config.getMaxUsersFreeTeamDrive());
		statement.bindLong(config.getMaxUsersPremiumTeamDrive());
		statement.bindLong(config.getDaysBeforeFreeTeamDrivesExpire());
		statement.bindString(etag);
		statement.execute();
	}

	public synchronized Pair<GlobalConfiguration, String> getGlobalConfigurationAndEtag()
	{
		Cursor c = mDb.query(true, TableGlobaConfiguration.TABLE, null, null, null, null, null, null, null, null);
		Pair<GlobalConfiguration, String> result = null;
		if (c.moveToFirst())
		{
			GlobalConfiguration config = new GlobalConfiguration(
					c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_MAX_FREE_DRIVES)),
					new Rights(c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_POTENTIAL_RIGHTS))),
					new Rights(c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_RIGHTS))),
					c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_MAX_USERS_FREE_TEAMDRIVE)),
					c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_MAX_USERS_PREMIUM_TEAMDRIVE)),
					c.getInt(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_DAYS_BEFORE_FREE_TEAMDRIVES_EXPIRE)));
			String etag = c.getString(c.getColumnIndexOrThrow(TableGlobaConfiguration.COLUMN_ETAG));
			result = new Pair<GlobalConfiguration, String>(config, etag);
		}
		c.close();
		return result;
	}

	private void basicInsertOrUpdateInvitations(IncomingInvitation... invitations)
	{
		AutoIncrementSQLiteStatement invitationStatement = AutoIncrementSQLiteStatement.compileInsertOrReplace(mDb,
				TableInvitations.TABLE,
				new String[]{TableInvitations.COLUMN_ID,
						TableInvitations.COLUMN_EMAIL,
						TableInvitations.COLUMN_TEAMDRIVE_ID,
						TableInvitations.COLUMN_TEAMDRIVE_NAME,
						TableInvitations.COLUMN_TEAMDRIVE_NOTE,
						TableInvitations.COLUMN_INVITING_USER_ID,
						TableInvitations.COLUMN_INVITING_USER_EMAIL,
						TableInvitations.COLUMN_INVITING_USER_DISPLAY_NAME,
						TableInvitations.COLUMN_INVITATION_MESSAGE,
						TableInvitations.COLUMN_ROLE});
		for (IncomingInvitation invitation : invitations)
		{
			invitationStatement.bindString(invitation.getId());
			invitationStatement.bindString(invitation.getEmail());
			invitationStatement.bindString(invitation.getTeamDriveId());
			invitationStatement.bindString(invitation.getTeamDriveName());
			invitationStatement.bindString(invitation.getTeamDriveNote());
			invitationStatement.bindString(invitation.getInvitingUserId());
			invitationStatement.bindString(invitation.getInvitingUserEmail());
			invitationStatement.bindString(invitation.getInvitingUserDisplayName());
			invitationStatement.bindString(invitation.getInvitationMessage());
			invitationStatement.bindLong(invitation.getRole());
			invitationStatement.execute();
		}
	}

	private void basicInsertOrUpdateTeamDrives(TeamDrive... teamDrives)
	{
		AutoIncrementSQLiteStatement driveStatement = AutoIncrementSQLiteStatement.compileInsertOrReplace(mDb,
				TableTeamDrives.TABLE,
				new String[]{
						TableTeamDrives.COLUMN_ID,
						TableTeamDrives.COLUMN_NAME,
						TableTeamDrives.COLUMN_NOTE,
						TableTeamDrives.COLUMN_IMGPATH,
						TableTeamDrives.COLUMN_FILE_SERVICE_URL,
						TableTeamDrives.COLUMN_API_BASE_URL,
						TableTeamDrives.COLUMN_IS_QUOTA_UNLIMITED,
						TableTeamDrives.COLUMN_PAYMENT_PAIDUNTIL,
						TableTeamDrives.COLUMN_PAYMENT_DEMO_PERIOD_UNTIL,
						TableTeamDrives.COLUMN_PAYMENT_ISFREE,
						TableTeamDrives.COLUMN_RIGHTS,
						TableTeamDrives.COLUMN_POTENTIAL_RIGHTS,
						TableTeamDrives.COLUMN_ROLE,
						TableTeamDrives.COLUMN_CCSRIGHTS
				});

		AutoIncrementSQLiteStatement userStatement = AutoIncrementSQLiteStatement.compileInsertOrReplace(mDb,
				TableUsers.TABLE,
				new String[]{
					TableUsers.COLUMN_ID,
					TableUsers.COLUMN_EMAIL,
					TableUsers.COLUMN_DISPLAYNAME,
					TableUsers.COLUMN_ABOUT_ME,
					TableUsers.COLUMN_IMGPATH,
					TableUsers.COLUMN_USER_TYPE
				});

		AutoIncrementSQLiteStatement driveUserStatement = AutoIncrementSQLiteStatement.compileInsertOrReplace(mDb,
				TableTeamDriveUsers.TABLE,
				new String[]{
					TableTeamDriveUsers.COLUMN_TEAMDRIVE_ID,
					TableTeamDriveUsers.COLUMN_USER_ID,
 					TableTeamDriveUsers.COLUMN_ROLE
				});

		for (TeamDrive teamDrive : teamDrives)
		{
			insertOrUpdateTeamDrive(teamDrive, driveStatement, userStatement, driveUserStatement);
		}
	}

	private void insertOrUpdateTeamDrive(TeamDrive teamDrive, AutoIncrementSQLiteStatement driveStatement,
										 AutoIncrementSQLiteStatement userStatement,
										 AutoIncrementSQLiteStatement driveUserStatement)
	{
		Payment payment = teamDrive.getPayment();

		driveStatement.bindString(teamDrive.getId());
		driveStatement.bindString(teamDrive.getName());
		driveStatement.bindString(teamDrive.getNote());
		driveStatement.bindString(teamDrive.getImgPath());
		driveStatement.bindString(teamDrive.getFileServiceUrl());
		driveStatement.bindString(teamDrive.getApiBaseUrl());
		driveStatement.bindDouble(teamDrive.isQuotaUnlimited() ? 1 : 0);
		driveStatement.bindLong(payment != null ? payment.getPaidUntil() : null);
		driveStatement.bindLong(payment != null ? payment.getDemoPeriodUntil() : null);
		driveStatement.bindLong(payment != null ? (payment.isFree() ? 1 : 0) : null);
		driveStatement.bindLong(teamDrive.getRights() != null ? teamDrive.getRights().getRights() : -1);
		driveStatement.bindLong(teamDrive.getPotentialRights() != null ? teamDrive.getPotentialRights().getRights()
				: -1);
		driveStatement.bindLong(teamDrive.getRole());
		driveStatement.bindLong(teamDrive.getCcsRights() != null ? teamDrive.getCcsRights().getRights() : -1);
		driveStatement.execute();

		for (User user : teamDrive.getUsers())
		{
			insertOrUpdateUser(user, user.getUserId(), TableUsers.TYPE_USER, user.getRole(),
					teamDrive.getId(), userStatement, driveUserStatement);
		}
		if (teamDrive.getInvitedUsers() != null)
		{
			String invitedUserInternalId = null;
			for (User user : teamDrive.getInvitedUsers())
			{
				// invited users have no ID so we generate one:
				invitedUserInternalId = TableUsers.INVITED_USER_ID_PREFIX + user.getEmail();
				insertOrUpdateUser(user, invitedUserInternalId, TableUsers.TYPE_INVITED,
						TableTeamDriveUsers.ROLE_INVITATION, teamDrive.getId(), userStatement,
						driveUserStatement);
			}
		}
	}

	private void insertOrUpdateUser(User user, String userId, int type, long role, String teamDriveId,
									AutoIncrementSQLiteStatement userStatement,
									AutoIncrementSQLiteStatement driveUserStatement)
	{
		userStatement.bindString(userId);
		userStatement.bindString(user.getEmail());
		userStatement.bindString(user.getDisplayName());
		userStatement.bindString(user.getAboutMe());
		userStatement.bindString(user.getImgPath());
		userStatement.bindLong(type);
		userStatement.execute();

		driveUserStatement.bindString(teamDriveId);
		driveUserStatement.bindString(userId);
		driveUserStatement.bindLong(role);
		driveUserStatement.execute();
	}

	private TeamDrive[] getTeamDrivesFromCursor(Cursor teamDriveCursor)
	{
		TeamDrive[] drives = new TeamDrive[teamDriveCursor.getCount()];
		int i = 0;
		while (teamDriveCursor.moveToNext())
		{
			drives[i] = getTeamDriveFromCursor(teamDriveCursor);
			i++;
		}
		return drives;
	}

	private TeamDrive getTeamDriveFromCursorWithoutUsers(Cursor teamDriveCursor)
	{
		return new TeamDrive(
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_ID)),
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_NAME)),
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_NOTE)),
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_FILE_SERVICE_URL)),
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_API_BASE_URL)),
				teamDriveCursor.getLong(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_IS_QUOTA_UNLIMITED)) == 1,
				teamDriveCursor.getString(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_IMGPATH)),
				new Payment(
						teamDriveCursor.getLong(teamDriveCursor
								.getColumnIndexOrThrow(TableTeamDrives.COLUMN_PAYMENT_PAIDUNTIL)),
						teamDriveCursor.getLong(teamDriveCursor
								.getColumnIndexOrThrow(TableTeamDrives.COLUMN_PAYMENT_DEMO_PERIOD_UNTIL)),
						teamDriveCursor.getInt(teamDriveCursor
								.getColumnIndexOrThrow(TableTeamDrives.COLUMN_PAYMENT_ISFREE)) == 1),
				null,
				null,
				teamDriveCursor.getInt(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_ROLE)),
				new Rights(teamDriveCursor.getInt(teamDriveCursor.getColumnIndexOrThrow(TableTeamDrives.COLUMN_RIGHTS))),
				new Rights(teamDriveCursor.getInt(teamDriveCursor
						.getColumnIndexOrThrow(TableTeamDrives.COLUMN_POTENTIAL_RIGHTS))),
				new CcsRights(teamDriveCursor.getInt(teamDriveCursor
						.getColumnIndexOrThrow(TableTeamDrives.COLUMN_CCSRIGHTS))));
	}

	private TeamDrive getTeamDriveFromCursor(Cursor teamDriveCursor)
	{
		TeamDrive td = getTeamDriveFromCursorWithoutUsers(teamDriveCursor);
		td.setUsers(getUsersForTeamDrive(td.getId()));
		td.setInvitedUsers(getInvitedUsersForTeamDrive(td.getId()));
		return td;
	}

	private User[] getUsersForTeamDrive(String driveId, int userType)
	{
		String statement = "SELECT "
				+ TableUsers.COLUMN_ID + ", "
				+ TableUsers.COLUMN_EMAIL + ", "
				+ TableUsers.COLUMN_DISPLAYNAME + ", "
				+ TableUsers.COLUMN_ABOUT_ME + ", "
				+ TableUsers.COLUMN_IMGPATH + ", "
				+ TableUsers.COLUMN_USER_TYPE + ", "
				+ TableTeamDriveUsers.COLUMN_ROLE
				+ " FROM " + TableUsers.TABLE + ", " + TableTeamDriveUsers.TABLE
				+ " WHERE "
				+ TableUsers.COLUMN_ID + " = " + TableTeamDriveUsers.COLUMN_USER_ID
				+ " AND " + TableUsers.COLUMN_USER_TYPE + " = " + userType
				+ " AND " + TableTeamDriveUsers.COLUMN_TEAMDRIVE_ID + " = ?;";
		Cursor userCursor = mDb.rawQuery(statement, new String[] {driveId});
		User[] users = new User[userCursor.getCount()];
		int j = 0;
		while (userCursor.moveToNext())
		{
			users[j] = new User(userCursor.getString(userCursor.getColumnIndexOrThrow(TableUsers.COLUMN_ID)),
					userCursor.getString(userCursor.getColumnIndexOrThrow(TableUsers.COLUMN_EMAIL)),
					userCursor.getString(userCursor.getColumnIndexOrThrow(TableUsers.COLUMN_DISPLAYNAME)),
					userCursor.getString(userCursor.getColumnIndexOrThrow(TableUsers.COLUMN_ABOUT_ME)),
					userCursor.getString(userCursor.getColumnIndexOrThrow(TableUsers.COLUMN_IMGPATH)),
					userCursor.getInt(userCursor.getColumnIndexOrThrow(TableTeamDriveUsers.COLUMN_ROLE)));
			j++;
		}
		userCursor.close();
		return users;
	}
}
