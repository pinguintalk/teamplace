package net.teamplace.android.http.exception;

public class UnsupportedRequestException extends OperationNotSupportedException
{
	public UnsupportedRequestException(String strError)
	{
		super(strError);
	}

	/**
     * 
     */
	private static final long serialVersionUID = 8553358128512851870L;

}
