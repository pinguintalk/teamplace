package net.teamplace.android.http.request.files;

import net.teamplace.android.http.annotation.JacksonDataModel;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class FileOperationResult
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty("result")
	private int result;

	@JsonProperty("item")
	private String item;

	@JsonProperty("x-result")
	private String xResult;

	public FileOperationResult()
	{
	}

	public int getResult()
	{
		return result;
	}

	public String getItem()
	{
		return item;
	}

	public String getxResult()
	{
		return xResult;
	}
}
