package net.teamplace.android.http.request.files;

import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JacksonDataModel
public class MoveCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private MoveParameter[] copyParameter;

	public MoveCommand(String teamDest, String teamSrc, String parentFolder, String[] moveFileOrFolder,
			String moveDestinationFolder, String destinationFileName, int forceOverwrite)
	{
		setCommand(CMD_MOVE);

		List<MoveParameter> list = new ArrayList<MoveParameter>();

		for (String file : moveFileOrFolder)
		{
			list.add(new MoveParameter(teamDest, teamSrc, parentFolder, file, moveDestinationFolder, destinationFileName,
					forceOverwrite));
		}

		this.copyParameter = list.toArray(new MoveParameter[list.size()]);
	}
}
