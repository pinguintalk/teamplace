package net.teamplace.android.http.util;

import java.net.HttpURLConnection;

import net.teamplace.android.http.request.ICloseAble;


public class CloseableConnection implements ICloseAble
{
	private final HttpURLConnection conn;

	public CloseableConnection(HttpURLConnection conn)
	{
		this.conn = conn;
	}

	@Override
	public void close()
	{
		if (conn != null)
		{
			try
			{
				conn.disconnect();
			}
			catch (Exception e)
			{
			}
		}
	}

}
