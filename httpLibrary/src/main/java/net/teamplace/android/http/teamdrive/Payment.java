package net.teamplace.android.http.teamdrive;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import net.teamplace.android.http.annotation.JacksonDataModel;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class Payment
{

	@JsonProperty("PaidUntil")
	@JsonSerialize(using = DateSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	@JsonDeserialize(using = DateDeSerializer.class)
	private Long paidUntil;

	@JsonProperty("DemoPeriodUntil")
	@JsonSerialize(using = DateSerializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	@JsonDeserialize(using = DateDeSerializer.class)
	private Long demoPeriodUntil;


	@JsonProperty("IsFree")
	private boolean isFree;

	public Payment()
	{
	}

	public Payment(Long paidUntil, Long demoPeriodUntil, boolean isFree)
	{
		this.paidUntil = paidUntil;
		this.demoPeriodUntil = demoPeriodUntil;
		this.isFree = isFree;
	}

	public static class DateSerializer extends JsonSerializer<Long>
	{
		@Override
		public void serialize(Long arg0, JsonGenerator arg1, SerializerProvider arg2) throws IOException,
				JsonProcessingException
		{
			arg1.writeString(String.valueOf(arg0));
		}
	}

	public static class DateDeSerializer extends JsonDeserializer<Long>
	{

		private static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
		static
		{
			DATETIME_FORMAT.setTimeZone(TimeZone.getTimeZone("GMT"));
		}

		@Override
		public Long deserialize(JsonParser arg0, DeserializationContext arg1) throws IOException,
				JsonProcessingException
		{
			try
			{
				return DATETIME_FORMAT.parse(arg0.getText()).getTime();
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
			return null;
		}

	}


	public Long getPaidUntil()
	{
		return paidUntil;
	}

	public void setPaidUntil(Long paidUntil)
	{
		this.paidUntil = paidUntil;
	}

	public Long getDemoPeriodUntil()
	{
		return demoPeriodUntil;
	}

	public void setDemoPeriodUntil(Long demoPeriodUntil)
	{
		this.demoPeriodUntil = demoPeriodUntil;
	}

	public boolean isFree()
	{
		return isFree;
	}

	public void setFree(boolean isFree)
	{
		this.isFree = isFree;
	}
}