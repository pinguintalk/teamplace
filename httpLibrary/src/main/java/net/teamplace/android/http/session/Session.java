package net.teamplace.android.http.session;

import java.util.HashMap;
import java.util.Map;

import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.request.account.AuthToken;
import net.teamplace.android.http.session.configuration.Configuration;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.TeamDrive;

import android.accounts.Account;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;


public class Session
{
	public static final String TABLE_NAME = "table_session";

	public static final String COL_TIMESTAP = "timestamp";
	private static final String COL_ACCOUNT_TYPE = "account_type";
	private static final String COL_ACCOUNT_NAME = "account_name";
	public static final String COL_CONFIGURATION_ID = "configuration_id";

	public static String CREATE_TABLE = "create table " + TABLE_NAME + "(" + SessionSQLiteHelper.COLUMN_ID
			+ " integer primary key autoincrement, " + COL_TIMESTAP + " integer, " + COL_ACCOUNT_TYPE
			+ " text not null, " + COL_ACCOUNT_NAME + " text not null, " + AuthToken.COL_AUTHTOKEN_STRING
			+ " text not null, " + AuthToken.COL_AUTHTOKEN_TYPE + " text not null, " + COL_CONFIGURATION_ID
			+ " integer not null);";

	private long id = -1;
	private long timestamp;
	private Account account;
	private Credentials credentials;
	private AuthToken token;
	private long configurationId = -1;
	private Configuration configuration;
	private final String deviceId;
	private final String userAgent;
	private Map<String, TeamDrive> teamDriveMap;
	private GlobalConfiguration teamDriveGlobalConfig;

	public Session(Context context, Account account)
	{
		updateTimestamp();
		this.account = account;
		deviceId = DeviceID.getDeviceID(context);
		userAgent = UserAgent.getUserAgent(context);
	}

	public Session(Context context, Cursor cursor)
	{
		id = cursor.getLong(cursor.getColumnIndex(SessionSQLiteHelper.COLUMN_ID));
		timestamp = cursor.getLong(cursor.getColumnIndex(COL_TIMESTAP));

		String accountType = cursor.getString(cursor.getColumnIndex(COL_ACCOUNT_TYPE));
		String accountName = cursor.getString(cursor.getColumnIndex(COL_ACCOUNT_NAME));

		account = new Account(accountName, accountType);

		token = new AuthToken(cursor);

		configurationId = cursor.getLong(cursor.getColumnIndex(COL_CONFIGURATION_ID));

		deviceId = DeviceID.getDeviceID(context);
		userAgent = UserAgent.getUserAgent(context);
	}

	public long getID()
	{
		return id;
	}

	public void setID(long id)
	{
		this.id = id;
	}

	public long getTimestamp()
	{
		return timestamp;
	}

	public void setTimestamp(long timestamp)
	{
		this.timestamp = timestamp;
	}

	public Account getAccount()
	{
		return account;
	}

	public void setAccount(Account account)
	{
		this.account = account;
	}

	public Credentials getCredentials()
	{
		return credentials;
	}

	public void setCredentials(Credentials credentials)
	{
		this.credentials = credentials;
	}

	public AuthToken getToken()
	{
		return token;
	}

	public void setToken(AuthToken token)
	{
		this.token = token;
	}

	public long getConfigurationID()
	{
		return configurationId;
	}

	public void setConfigurationID(long configurationId)
	{
		this.configurationId = configurationId;
	}

	public void updateConfigurationID()
	{
		if (configuration != null)
		{
			configurationId = configuration.getId();
		}
	}

	public Configuration getConfiguration()
	{
		return configuration;
	}

	public void setConfiguration(Configuration configuration)
	{
		this.configuration = configuration;
	}

	public String getDeviceID()
	{
		return deviceId;
	}

	public String getUserAgent()
	{
		return userAgent;
	}

	public void updateTimestamp()
	{
		timestamp = System.currentTimeMillis() / 1000L;
	}

	public boolean isConfigurationValid()
	{
		if (configuration != null)
		{
			return configuration.isValid();
		}

		return false;
	}

	public Map<String, TeamDrive> getTeamDriveMap()
	{
		return teamDriveMap;
	}

	public void setTeamDriveMap(Map<String, TeamDrive> teamDriveMap)
	{
		this.teamDriveMap = teamDriveMap;
	}

	public void setTeamDrives(TeamDrive[] teamDriveArr)
	{
		if (teamDriveMap == null)
		{
			teamDriveMap = new HashMap<String, TeamDrive>();
		}
		teamDriveMap.clear();

		teamDriveArr = teamDriveArr == null ? new TeamDrive[0] : teamDriveArr;
		for (TeamDrive drive : teamDriveArr)
		{
			teamDriveMap.put(drive.getId(), drive);
		}
	}

	public GlobalConfiguration getTeamDriveGlobalConfig()
	{
		return teamDriveGlobalConfig;
	}

	public void setTeamDriveGlobalConfig(GlobalConfiguration config)
	{
		this.teamDriveGlobalConfig = config;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_TIMESTAP, timestamp);

		if (account != null)
		{
			values.put(COL_ACCOUNT_TYPE, account.type);
			values.put(COL_ACCOUNT_NAME, account.name);
		}

		if (token != null)
		{
			values.putAll(token.getAsContentValues());
		}

		values.put(COL_CONFIGURATION_ID, configurationId);

		return values;
	}

}
