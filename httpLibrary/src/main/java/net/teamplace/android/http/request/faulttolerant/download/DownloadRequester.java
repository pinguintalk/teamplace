package net.teamplace.android.http.request.faulttolerant.download;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.FileWasModifiedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.request.faulttolerant.NetworkOperationCallback;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.session.DeviceID;
import net.teamplace.android.http.session.Session;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.util.Log;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class DownloadRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	private NetworkUpdateCallback networkUpdateCallback;
	private NetworkOperationCallback networkOperationCallback;

	private long totalFileLength;
	private File localTmpFile;
	private int oldProgressValue;

	public DownloadRequester(Context context, Session session, BackendConfiguration backendConfiguration, NetworkUpdateCallback networkUpdateCallback,
			NetworkOperationCallback networkOperationCallback)
	{
		super(context, session, backendConfiguration);

		this.networkUpdateCallback = networkUpdateCallback;
		this.networkOperationCallback = networkOperationCallback;
	}

	public String download(String teamDriveId, String remotePath, String remoteFileName, String localPath,
			String localFileName, DownloadStatus status)
			throws TeamDriveNotFoundException, ConnectFailedException, XCBStatusException, HttpResponseException,
			LogonFailedException, NotificationException, ServerLicenseException, FileWasModifiedException,
			JsonParseException, JsonMappingException, IOException, RedirectException {
		Uri uri = getDownloadUri(getServerForTeamDriveId(teamDriveId), remotePath, remoteFileName, teamDriveId);

		HttpURLConnection connection = null;
		BufferedInputStream bufferedInputStream = null;
		FileOutputStream fileOutputStream = null;
		String downloadFilePath = null;

		try
		{
			if (status != null)
			{
				localTmpFile = new File(localPath, status.getTmpFileName());
			}

			connection = connectAndExecuteGETRequest(uri, getRangeHeader(status), true, true);

			long remoteTimestamp = checkFileLastModifyTimestamp(connection, status);

			if (status == null)
			{
				long currentTime = System.currentTimeMillis() / 1000L;

				status = new DownloadStatus(remotePath, remoteFileName, localPath, localFileName, remoteTimestamp,
						currentTime,
						generateTmpFileName(currentTime));

				localTmpFile = new File(localPath, status.getTmpFileName());
			}

			totalFileLength = getTotalFileLength(connection);

			bufferedInputStream = new BufferedInputStream(connection.getInputStream());
			fileOutputStream = new FileOutputStream(localTmpFile, true);

			byte[] buffer = new byte[4096];
			int byteRead = -1;
			oldProgressValue = -1;

			Log.d(TAG, "Download " + localTmpFile.getName());

			while ((byteRead = bufferedInputStream.read(buffer)) != -1
					&& !networkOperationCallback.stopNetworkOperation())
			{
				fileOutputStream.write(buffer, 0, byteRead);
				publishProgressInPercent(status);
			}
		}
		finally
		{
			if (!isFileDownloadedCompletely(connection, status))
			{
				insertOrUpdateDownlaodStatus(status);
			}
			else
			{
				if (status != null)
				{
					downloadFilePath = status.getLocalFilePath() + File.separator + status.getLocalFileName();
					removeDownloadStatus(status);
					networkUpdateCallback.onSuccess(downloadFilePath);
				}
			}

			disconnect(connection);
			closeStream(bufferedInputStream);
			closeStream(fileOutputStream);
		}

		return downloadFilePath;
	}

	public InputStream getDownloadInputStream(Uri from)
			throws TeamDriveNotFoundException, XCBStatusException, HttpResponseException, JsonParseException,
			JsonMappingException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		HttpURLConnection connection = connectAndExecuteGETRequest(from, null, true, true);
		return connection.getInputStream();
	}

	public Uri getDownloadUriWithAccessData(String teamDriveId, String remotePath, String remoteFileName)
			throws TeamDriveNotFoundException
	{
		Uri uri = getDownloadUri(getServerForTeamDriveId(teamDriveId), remotePath, remoteFileName, teamDriveId)
				.buildUpon()
				.appendQueryParameter(PARAM_TOKEN, session.getToken().getTokenStr())
				.appendQueryParameter(PARAM_TYPE, session.getToken().getTokenType())
				.appendQueryParameter(PARAM_DEVICEID, DeviceID.getDeviceID(context))
				.build();
		return uri;
	}

	private Uri getDownloadUri(String server, String remotePath, String remoteFileName, String teamDriveId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();
		builder.appendEncodedPath("gui_1/");
		builder.appendQueryParameter("fldr", remotePath);
		builder.appendQueryParameter("fdn", remoteFileName);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}

		return builder.build();
	}

	private List<NameValuePair> getRangeHeader(DownloadStatus status)
	{
		List<NameValuePair> properties = new LinkedList<NameValuePair>();

		if (status != null)
		{
			properties.add(new BasicNameValuePair(HEADER_RANGE, "bytes=" + getByteCountOfFile(status) + "-"));
		}
		else
		{
			properties.add(new BasicNameValuePair(HEADER_RANGE, "bytes=0-"));
		}

		Log.d(TAG, properties.get(0).getName() + " " + properties.get(0).getValue());

		return properties;
	}

	private long checkFileLastModifyTimestamp(HttpURLConnection connection, DownloadStatus status)
			throws FileWasModifiedException
	{
		long remoteLastModifyTimestamp = Long.parseLong(connection.getHeaderField("X-Filetime"));

		if (status != null)
		{
			Log.d(TAG, "Remote: " + remoteLastModifyTimestamp + " Local: " + status.getLastModifyTimestamp());
		}

		if (status != null && remoteLastModifyTimestamp > status.getLastModifyTimestamp())
		{
			throw new FileWasModifiedException();
		}

		return remoteLastModifyTimestamp;
	}

	private String generateTmpFileName(long currentTime)
	{
		return ".tmp_" + Long.toString(currentTime) + ".tmp";
	}

	private long getTotalFileLength(HttpURLConnection connection)
	{
		String contentRange = connection.getHeaderField("Content-Range");
		return Long.parseLong(contentRange.substring(contentRange.indexOf("/") + 1));
	}

	private void publishProgressInPercent(DownloadStatus status)
	{
		if (networkUpdateCallback != null)
		{
			int newProgressValue = (int) ((getByteCountOfFile(status) * 100) / totalFileLength);

			if (newProgressValue > oldProgressValue)
			{
				oldProgressValue = newProgressValue;
				networkUpdateCallback.publishProgressInPercent(oldProgressValue);
			}
		}
	}

	private long getByteCountOfFile(DownloadStatus status)
	{
		if (status != null)
		{
			return localTmpFile.length();
		}

		return 0;
	}

	private boolean isFileDownloadedCompletely(HttpURLConnection connection, DownloadStatus status)
	{
		Log.d(TAG, "Total: " + totalFileLength + " Real: " + getByteCountOfFile(status));

		if (totalFileLength == getByteCountOfFile(status))
		{
			renameFile(status);
			return true;
		}

		return false;
	}

	private void renameFile(DownloadStatus status)
	{
		if (status != null)
		{
			File fileToRename = new File(status.getLocalFilePath(), status.getTmpFileName());
			File newFile = new File(status.getLocalFilePath(), status.getLocalFileName());

			int fileNameCounter = 0;

			while (newFile.exists())
			{
				fileNameCounter++;

				String fileName = status.getLocalFileName();
				String name = fileName.substring(0, fileName.lastIndexOf("."));
				String extension = fileName.substring(fileName.lastIndexOf("."));

				newFile = new File(status.getLocalFilePath(), name + "(" + Integer.toString(fileNameCounter) + ")"
						+ extension);
			}

			Log.d(TAG, "Old file name: " + fileToRename.getName());
			Log.d(TAG, "New file name: " + newFile.getName());

			status.setLocalFileName(newFile.getName());

			fileToRename.renameTo(newFile);
		}
	}

	private void insertOrUpdateDownlaodStatus(DownloadStatus status)
	{
		if (status != null)
		{
			UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
			SQLiteDatabase database = databaseHelper.getWritableDatabase();

			Cursor cursor = database.query(DownloadStatus.TABLE_NAME, null, DownloadStatus.COL_REMOTE_FILE_PATH
					+ "= ? AND "
					+ DownloadStatus.COL_REMOTE_FILE_NAME + "= ?",
					new String[] {status.getRemoteFilePath(), status.getRemoteFileName()}, null, null,
					null);

			try
			{
				if (cursor.moveToFirst())
				{
					database.update(DownloadStatus.TABLE_NAME, status.getAsContentValues(),
							DownloadStatus.COL_REMOTE_FILE_PATH + "= ? AND "
									+ DownloadStatus.COL_REMOTE_FILE_NAME + "= ?",
							new String[] {status.getRemoteFilePath(), status.getRemoteFileName()});
				}
				else
				{
					database.insert(DownloadStatus.TABLE_NAME, null, status.getAsContentValues());
				}
			}
			finally
			{
				cursor.close();
				database.close();
			}
		}
	}

	private void removeDownloadStatus(DownloadStatus status)
	{
		if (status != null)
		{
			UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
			SQLiteDatabase database = databaseHelper.getWritableDatabase();

			database.delete(DownloadStatus.TABLE_NAME,
					DownloadStatus.COL_REMOTE_FILE_PATH + "= ? AND " + DownloadStatus.COL_REMOTE_FILE_NAME + "= ?",
					new String[] {status.getRemoteFilePath(), status.getRemoteFileName()});

			database.close();
		}
	}
}
