package net.teamplace.android.http.request.smartfiling;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

@JacksonDataModel
public class SmartFilingLocation
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty("id")
	private String id;

	@JsonProperty("path")
	private String path;

	@JsonProperty("origin")
	private boolean origin;

	@JsonProperty("team")
	private String teamDriveId;

	@JsonProperty("accessed")
	private long accessed;

	public SmartFilingLocation()
	{
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getPath()
	{
		return path;
	}

	public void setPath(String path)
	{
		this.path = path;
	}

	public boolean isOrigin()
	{
		return origin;
	}

	public void setOrigin(boolean origin)
	{
		this.origin = origin;
	}

	public String getTeamDriveId()
	{
		return teamDriveId;
	}

	public void setTeamDriveId(String teamDriveId)
	{
		this.teamDriveId = teamDriveId;
	}

	public long getAccessed()
	{
		return accessed;
	}

	public void setAccessed(long accessed)
	{
		this.accessed = accessed;
	}

	@JsonIgnore
	public static List<SmartFilingLocation> createSmartFilingTargetArrayListFromJSON(String json)
			throws JsonParseException, JsonMappingException,
			IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();

		CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(ArrayList.class,
				SmartFilingLocation.class);

		return mapper.readValue(json, collectionType);
	}
}
