package net.teamplace.android.http.exception;

public class CortadoException extends Exception
{
	public CortadoException(String strError)
	{
		super(strError);
	}

	public CortadoException(Exception e)
	{
		super(e);
	}

	/**
     * 
     */
	private static final long serialVersionUID = -5706637475097300349L;

}
