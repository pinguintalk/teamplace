package net.teamplace.android.http.request.comments;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;
import net.teamplace.android.http.util.FilePathHelper;
import android.util.Base64;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class AddCommentParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_COMMENT)
	private String comment;

	@JsonProperty(PARAM_FOLDER)
	private String folder;

	@JsonProperty(PARAM_FILE)
	private String file;

	@JsonProperty(PARAM_TEAM)
	private String teamDriveId;

	public AddCommentParameter(String folder, String file, String teamDriveId, String comment)
	{
		this.folder = FilePathHelper.buildWindowsPath(folder, false, false);
		this.file = file;
		this.teamDriveId = teamDriveId;
		this.comment = Base64.encodeToString(comment.getBytes(), Base64.NO_WRAP);
	}

	public String getComment()
	{
		return comment;
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	public String getFolder()
	{
		return folder;
	}

	public void setFolder(String folder)
	{
		this.folder = folder;
	}

	public String getFile()
	{
		return file;
	}

	public void setFile(String file)
	{
		this.file = file;
	}

	public String getTeamDriveId()
	{
		return teamDriveId;
	}

	public void setTeamDriveId(String teamDriveId)
	{
		this.teamDriveId = teamDriveId;
	}
}
