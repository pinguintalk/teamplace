package net.teamplace.android.http.request.comments;

import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class ChangedCommentParameter implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_FILE)
	private String file;

	@JsonProperty(PARAM_FOLDER)
	private String folder;

	@JsonProperty(PARAM_TEAM)
	private String teamDriveId;

	@JsonProperty(PARAM_SINCE)
	private long since;

	@JsonProperty(PARAM_COMMENT_IDS)
	private List<String> commentIds;

	public ChangedCommentParameter(String teamDriveId, String folder, String file, long since, List<String> commentIds)
	{
		this.teamDriveId = teamDriveId;
		this.folder = folder;
		this.file = file;
		this.since = since;
		this.commentIds = commentIds;
	}

	public String getFile()
	{
		return file;
	}

	public void setFile(String file)
	{
		this.file = file;
	}

	public String getFolder()
	{
		return folder;
	}

	public void setFolder(String folder)
	{
		this.folder = folder;
	}

	public String getTeamDriveId()
	{
		return teamDriveId;
	}

	public void setTeamDriveId(String teamDriveId)
	{
		this.teamDriveId = teamDriveId;
	}

	public long getSince()
	{
		return since;
	}

	public void setSince(long since)
	{
		this.since = since;
	}

	public List<String> getCommentIds()
	{
		return commentIds;
	}

	public void setCommentIds(List<String> commentIds)
	{
		this.commentIds = commentIds;
	}
}
