package net.teamplace.android.http.request.faulttolerant.download;

import android.content.ContentValues;
import android.database.Cursor;

public class DownloadStatus
{
	private final String TAG = getClass().getSimpleName();

	public static final String TABLE_NAME = "download_table";

	public static final String COL_REMOTE_FILE_PATH = "remoteFilePath";
	public static final String COL_REMOTE_FILE_NAME = "remoteFileName";
	public static final String COL_LOCAL_FILE_PATH = "localFilePath";
	public static final String COL_LOCAL_FILE_NAME = "localFileName";
	public static final String COL_LAST_MODIFY_TIMESTAMP = "lastModifyTimestamp";
	public static final String COL_LAST_DOWNLOAD_ATTEMPT = "lastDownloadAttempt";
	public static final String COL_TMP_FILE_NAME = "tmpFileName";
	public static final String COL_FORCE_DELETE = "forceDelete";

	public static String CREATE_TABLE = "create table " + TABLE_NAME + "(" + UpDownloadSQLiteHelper.COLUMN_ID
			+ " integer primary key autoincrement, " + COL_REMOTE_FILE_PATH + " text not null, " + COL_REMOTE_FILE_NAME
			+ " text not null, "
			+ COL_LOCAL_FILE_PATH + " text not null, " + COL_LOCAL_FILE_NAME + " text not null, "
			+ COL_LAST_MODIFY_TIMESTAMP + " integer, "
			+ COL_LAST_DOWNLOAD_ATTEMPT + " integer, " + COL_TMP_FILE_NAME + " text not null, " + COL_FORCE_DELETE
			+ " integer);";

	private String remoteFilePath;
	private String remoteFileName;
	private String localFilePath;
	private String localFileName;
	private long lastModifyTimestamp;
	private long lastDownloadAttempt;
	private String tmpFileName;
	private boolean forceDelete;

	public DownloadStatus()
	{
	}

	public DownloadStatus(String remoteFilePath, String remoteFileName, String localFilePath, String localFileName,
			long lastModifyTimestamp,
			long lastDownloadAttempt, String tmpFileName)
	{
		this.remoteFilePath = remoteFilePath;
		this.remoteFileName = remoteFileName;
		this.localFilePath = localFilePath;
		this.localFileName = localFileName;
		this.lastModifyTimestamp = lastModifyTimestamp;
		this.lastDownloadAttempt = lastDownloadAttempt;
		this.tmpFileName = tmpFileName;
		this.forceDelete = false;
	}

	public DownloadStatus(Cursor cursor)
	{
		remoteFilePath = cursor.getString(cursor.getColumnIndex(COL_REMOTE_FILE_PATH));
		remoteFileName = cursor.getString(cursor.getColumnIndex(COL_REMOTE_FILE_NAME));
		localFilePath = cursor.getString(cursor.getColumnIndex(COL_LOCAL_FILE_PATH));
		localFileName = cursor.getString(cursor.getColumnIndex(COL_LOCAL_FILE_NAME));
		lastModifyTimestamp = cursor.getLong(cursor.getColumnIndex(COL_LAST_MODIFY_TIMESTAMP));
		lastDownloadAttempt = cursor.getLong(cursor.getColumnIndex(COL_LAST_DOWNLOAD_ATTEMPT));
		tmpFileName = cursor.getString(cursor.getColumnIndex(COL_TMP_FILE_NAME));
		forceDelete = cursor.getInt(cursor.getColumnIndex(COL_FORCE_DELETE)) == 1 ? true : false;
	}

	public String getRemoteFilePath()
	{
		return remoteFilePath;
	}

	public void setRemoteFilePath(String filePath)
	{
		this.remoteFilePath = filePath;
	}

	public String getRemoteFileName()
	{
		return remoteFileName;
	}

	public void setRemoteFileName(String fileName)
	{
		this.remoteFileName = fileName;
	}

	public String getLocalFilePath()
	{
		return localFilePath;
	}

	public void setLocalFilePath(String localFilePath)
	{
		this.localFilePath = localFilePath;
	}

	public String getLocalFileName()
	{
		return localFileName;
	}

	public void setLocalFileName(String localFileName)
	{
		this.localFileName = localFileName;
	}

	public long getLastModifyTimestamp()
	{
		return lastModifyTimestamp;
	}

	public void setLastModifyTimestamp(long lastModifyTimestamp)
	{
		this.lastModifyTimestamp = lastModifyTimestamp;
	}

	public long getLastDownloadAttempt()
	{
		return lastDownloadAttempt;
	}

	public void setLastDownloadAttempt(long lastDownloadAttempt)
	{
		this.lastDownloadAttempt = lastDownloadAttempt;
	}

	public String getTmpFileName()
	{
		return tmpFileName;
	}

	public void setTmpFileName(String tmpFileName)
	{
		this.tmpFileName = tmpFileName;
	}

	public boolean isForceDelete()
	{
		return forceDelete;
	}

	public void setForceDelete(boolean forceDelete)
	{
		this.forceDelete = forceDelete;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_REMOTE_FILE_PATH, remoteFilePath);
		values.put(COL_REMOTE_FILE_NAME, remoteFileName);
		values.put(COL_LOCAL_FILE_PATH, localFilePath);
		values.put(COL_LOCAL_FILE_NAME, localFileName);
		values.put(COL_LAST_MODIFY_TIMESTAMP, lastModifyTimestamp);
		values.put(COL_LAST_DOWNLOAD_ATTEMPT, lastDownloadAttempt);
		values.put(COL_TMP_FILE_NAME, tmpFileName);
		values.put(COL_FORCE_DELETE, forceDelete ? 1 : 0);

		return values;
	}
}
