package net.teamplace.android.http.exception;

public class ServerLicenseException extends CortadoException
{
	private static final long serialVersionUID = 1L;

	public ServerLicenseException()
	{
		super("No Server License");
	}

	public ServerLicenseException(String strErr)
	{
		super(strErr);
	}
}
