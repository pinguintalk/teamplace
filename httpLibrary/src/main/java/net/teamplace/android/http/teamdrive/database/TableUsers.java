package net.teamplace.android.http.teamdrive.database;

import android.database.sqlite.SQLiteDatabase;

public class TableUsers
{

	public static final String TABLE = "table_users";

	public static final String COLUMN_ID = "user_id";
	public static final String COLUMN_EMAIL = "email";
	public static final String COLUMN_DISPLAYNAME = "displayname";
	public static final String COLUMN_ABOUT_ME = "aboutme";
	public static final String COLUMN_IMGPATH = "imgpath";
	public static final String COLUMN_USER_IMG_ETAG = "user_img_etag";
	public static final String COLUMN_USER_TYPE = "user_type";

	public static final int TYPE_USER = 0;
	public static final int TYPE_INVITED = 1;

	public static final String INVITED_USER_ID_PREFIX = "invitation:";

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " (" +
			COLUMN_ID + " TEXT PRIMARY KEY, "
			+ COLUMN_EMAIL + " TEXT NOT NULL, "
			+ COLUMN_DISPLAYNAME + " TEXT, "
			+ COLUMN_ABOUT_ME + " TEXT, "
			+ COLUMN_IMGPATH + " TEXT, "
			+ COLUMN_USER_IMG_ETAG + " TEXT, "
			+ COLUMN_USER_TYPE + " INTEGER);";

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableUsers.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}
}
