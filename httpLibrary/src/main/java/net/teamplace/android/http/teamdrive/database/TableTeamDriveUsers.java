package net.teamplace.android.http.teamdrive.database;

import android.database.sqlite.SQLiteDatabase;

public class TableTeamDriveUsers
{

	public static final String TABLE = "table_teamdrive_users";

	public static final String COLUMN_USER_ID = "ref_user_id";
	public static final String COLUMN_TEAMDRIVE_ID = "ref_teamdrive_id";
	public static final String COLUMN_ROLE = "role";

	public static final long ROLE_INVITATION = Long.MIN_VALUE;

	public static final String CREATE_TABLE = "CREATE TABLE " + TABLE + " ("
			+ COLUMN_USER_ID + " REFERENCES " + TableUsers.TABLE + "(" + TableUsers.COLUMN_ID + "), "
			+ COLUMN_TEAMDRIVE_ID + " REFERENCES " + TableTeamDrives.TABLE + "(" + TableTeamDrives.COLUMN_ID + "), "
			+ COLUMN_ROLE + " INTEGER NOT NULL, PRIMARY KEY (" + COLUMN_USER_ID + ", " + COLUMN_TEAMDRIVE_ID + "));";

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableTeamDriveUsers.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}

}
