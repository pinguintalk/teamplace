package net.teamplace.android.http.database.util;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class AutoIncrementSQLiteStatement
{

	private SQLiteStatement innerStatement;
	private int pos = 1;

	private AutoIncrementSQLiteStatement(SQLiteStatement statement)
	{
		this.innerStatement = statement;
	}

	public static AutoIncrementSQLiteStatement compile(SQLiteDatabase db, String sql)
	{
		return new AutoIncrementSQLiteStatement(db.compileStatement(sql));
	}

	public static AutoIncrementSQLiteStatement compileInsertOrReplace(SQLiteDatabase db, String table, String[] columns)
	{
		StringBuilder sb = new StringBuilder("INSERT OR REPLACE INTO ")
				.append(table)
				.append("(");
		StringBuilder sbPlaceholders = new StringBuilder();
		boolean isLast = false;
		for (int i = 0; i < columns.length; i++)
		{
			isLast = i == columns.length - 1;
			sbPlaceholders.append(!isLast ? "?, " : "?)");
			sb.append(columns[i])
					.append(!isLast ? ", " : ") VALUES (" + sbPlaceholders.toString());
		}
		return compile(db, sb.toString());
	}

	public void bindString(String value)
	{
		if (value != null)
		{
			innerStatement.bindString(pos, value);
		}
		else
		{
			innerStatement.bindNull(pos);
		}
		pos++;
	}

	public void bindDouble(double value)
	{
		innerStatement.bindDouble(pos, value);
		pos++;
	}

	public void bindLong(long value)
	{
		innerStatement.bindDouble(pos, value);
		pos++;
	}

	public void execute()
	{
		innerStatement.execute();
		pos = 1;
	}

}
