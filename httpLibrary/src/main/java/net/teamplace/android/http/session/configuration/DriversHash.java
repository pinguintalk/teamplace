package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class DriversHash
{
	private static final String COL_PRINTER_DYNAMIC = "printer_dynamic";
	private static final String COL_PRINTER_STATIC = "printer_static";
	private static final String COL_PRINTER_FULL = "printer_full";

	protected static final String TABLE_PART = COL_PRINTER_DYNAMIC + " text, " + COL_PRINTER_STATIC + " text, "
			+ COL_PRINTER_FULL + " text, ";

	@JsonProperty("PrinterDynamic")
	private String mDynamicHash;

	@JsonProperty("PrinterStatic")
	private String mStaticHash;

	@JsonProperty("PrinterFull")
	private String mFullHash;

	public DriversHash()
	{
	}

	public DriversHash(Cursor cursor)
	{
		mDynamicHash = cursor.getString(cursor.getColumnIndex(COL_PRINTER_DYNAMIC));
		mStaticHash = cursor.getString(cursor.getColumnIndex(COL_PRINTER_STATIC));
		mFullHash = cursor.getString(cursor.getColumnIndex(COL_PRINTER_FULL));
	}

	public String getDynamicHash()
	{
		return mDynamicHash;
	}

	public void setDynamicHash(String dynamicHash)
	{
		mDynamicHash = dynamicHash;
	}

	public String getStaticHash()
	{
		return mStaticHash;
	}

	public void setStaticHash(String staticHash)
	{
		mStaticHash = staticHash;
	}

	public String getFullHash()
	{
		return mFullHash;
	}

	public void setFullHash(String fullHash)
	{
		mFullHash = fullHash;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_PRINTER_DYNAMIC, mDynamicHash);
		values.put(COL_PRINTER_STATIC, mStaticHash);
		values.put(COL_PRINTER_FULL, mFullHash);

		return values;
	}
}
