package net.teamplace.android.http.request.account;

import java.io.IOException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;

@JacksonDataModel
public class AuthToken
{
	private final String TAG = getClass().getSimpleName();

	public static final String COL_AUTHTOKEN_STRING = "authTokenString";
	public static final String COL_AUTHTOKEN_TYPE = "authTokenType";

	@JsonProperty("Token")
	private String tokenStr;

	@JsonProperty("TokenType")
	private String tokenType;

	@JsonProperty("ValidTo")
	private String validTo;

	@JsonProperty("ValidToEx")
	private long validToEx;

	public AuthToken()
	{
	}

	public AuthToken(String tokenStr, String tokenType)
	{
		this.tokenStr = tokenStr;
		this.tokenType = tokenType;
	}

	public AuthToken(Cursor cursor)
	{
		tokenStr = cursor.getString(cursor.getColumnIndex(COL_AUTHTOKEN_STRING));
		tokenType = cursor.getString(cursor.getColumnIndex(COL_AUTHTOKEN_TYPE));
	}

	public void setTokenStr(String tokenStr)
	{
		this.tokenStr = tokenStr;
	}

	public String getTokenStr()
	{
		return tokenStr;
	}

	public void setValidTo(String validTo)
	{
		this.validTo = validTo;
	}

	public String getValidTo()
	{
		return validTo;
	}

	public void setValidToEx(long validToEx)
	{
		this.validToEx = validToEx;
	}

	public long getValidToEx()
	{
		return validToEx;
	}

	public void setTokenType(String tokenType)
	{
		this.tokenType = tokenType;
	}

	public String getTokenType()
	{
		return tokenType;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_AUTHTOKEN_STRING, tokenStr);
		values.put(COL_AUTHTOKEN_TYPE, tokenType);

		return values;
	}

	public static AuthToken createAuthTokenFromJSON(String json) throws IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		return mapper.readValue(json, AuthToken.class);
	}
}
