package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ConfigurationSchema
{
	private static final String COL_SCHEMA_VERSION = "schema_version";

	protected static final String TABLE_PART = COL_SCHEMA_VERSION + " text, ";

	@JsonProperty("Version")
	private String mVersion;

	public ConfigurationSchema()
	{
	}

	public ConfigurationSchema(Cursor cursor)
	{
		mVersion = cursor.getString(cursor.getColumnIndex(COL_SCHEMA_VERSION));
	}

	public String getVersion()
	{
		return mVersion;
	}

	public void setVersion(String version)
	{
		mVersion = version;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_SCHEMA_VERSION, mVersion);

		return values;
	}
}