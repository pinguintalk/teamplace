package net.teamplace.android.http.exception;

public class IllegalLogonDataException extends ConnectFailedException
{
	private static final long serialVersionUID = -5821938540583869140L;

	public IllegalLogonDataException()
	{
		this("illegal logon data");
	}

	public IllegalLogonDataException(String strErr)
	{
		super(strErr);
	}
} // public class InvalidLogonData extends ConnectFailedException