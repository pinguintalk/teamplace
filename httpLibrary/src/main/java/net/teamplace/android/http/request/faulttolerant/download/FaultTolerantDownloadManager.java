package net.teamplace.android.http.request.faulttolerant.download;

import net.teamplace.android.http.exception.FileWasModifiedException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.faulttolerant.NetworkOperationCallback;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class FaultTolerantDownloadManager implements NetworkOperationCallback
{
	private final String TAG = getClass().getSimpleName();

	private Context context;
	private DownloadRequester requester;

	private volatile boolean stopDownload = false;
	private NetworkUpdateCallback networkUpdateCallback;

	public FaultTolerantDownloadManager(Context context, NetworkUpdateCallback networkUpdateCallback, BackendConfiguration backendConfiguration)
	{
		this.context = context.getApplicationContext();
		this.networkUpdateCallback = networkUpdateCallback;

		Session session = new SessionManager(this.context).getLastSession();
		requester = new DownloadRequester(context, session, backendConfiguration, this.networkUpdateCallback, this);
	}

	public void downloadFile(final String teamDriveId, final String remotePath, final String remoteFileName,
			final String localPath, final String localFileName)
	{
		stopDownload = false;

		Thread downloadThread = new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				boolean error = false;

				DownloadStatus status = getDownloadStatus(remotePath, remoteFileName, localPath, localFileName);
				String downloadFilePath = null;

				try
				{
					try
					{
						downloadFilePath = requester.download(teamDriveId, remotePath, remoteFileName, localPath,
								localFileName, status);
					}
					catch (FileWasModifiedException e)
					{
						downloadFilePath = requester.download(teamDriveId, remotePath, remoteFileName, localPath,
								localFileName, null);
					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					error = true;
					networkUpdateCallback.onFailure(e);
				}

				if (!error)
				{
					networkUpdateCallback.onSuccess(downloadFilePath);
				}
			}
		});

		downloadThread.start();
	}

	private DownloadStatus getDownloadStatus(String remotePath, String remoteFileName, String localPath,
			String localFileName)
	{
		UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
		SQLiteDatabase database = databaseHelper.getReadableDatabase();

		Cursor cursor = database.query(DownloadStatus.TABLE_NAME, null, DownloadStatus.COL_REMOTE_FILE_PATH
				+ "= ? AND "
				+ DownloadStatus.COL_REMOTE_FILE_NAME + "= ? AND " + DownloadStatus.COL_LOCAL_FILE_PATH + "= ? AND "
				+ DownloadStatus.COL_LOCAL_FILE_NAME + "= ?", new String[] {remotePath, remoteFileName, localPath,
				localFileName}, null, null, null);

		try
		{
			if (cursor.moveToFirst())
			{
				return new DownloadStatus(cursor);
			}

			return null;
		}
		finally
		{
			cursor.close();
			database.close();
		}
	}

	public void stopDownload()
	{
		Log.d(TAG, "Stop download");
		stopDownload = true;
	}

	@Override
	public boolean stopNetworkOperation()
	{
		return stopDownload;
	}
}
