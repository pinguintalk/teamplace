package net.teamplace.android.http.exception;

public class TypeNotSupportedException extends OperationNotSupportedException
{

	/**
     * 
     */
	private static final long serialVersionUID = -5682141911883658656L;

	public TypeNotSupportedException(String strError)
	{
		super(strError);
	}

}
