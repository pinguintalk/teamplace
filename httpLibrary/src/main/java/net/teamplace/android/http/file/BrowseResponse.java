package net.teamplace.android.http.file;

import java.io.IOException;
import java.io.InputStream;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class BrowseResponse
{
	@JsonProperty(RemoteFolder.FOLDER)
	private RemoteFolder folder;

	public BrowseResponse()
	{
	}

	public RemoteFolder getBrowsedFolder()
	{
		return folder;
	}

	public static BrowseResponse createBrowseResponseFromJSON(String jsonString) throws JsonParseException,
			JsonMappingException, IOException
	{
		return createBrowseResponseFromJSON(jsonString.getBytes());
	}

	public static BrowseResponse createBrowseResponseFromJSON(InputStream jsonIs) throws JsonParseException,
			JsonMappingException, IOException
	{
		return createBrowseResponseFromJSON(IOUtils.toByteArray(jsonIs));
	}

	public static BrowseResponse createBrowseResponseFromJSON(byte[] jsonBytes) throws JsonParseException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		BrowseResponse response = mapper.readValue(jsonBytes, BrowseResponse.class);

		return response;
	}
}
