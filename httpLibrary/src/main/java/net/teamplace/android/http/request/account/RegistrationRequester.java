package net.teamplace.android.http.request.account;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.request.ConnectionErrorHandler;
import net.teamplace.android.http.session.Session;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class RegistrationRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	private final byte[] regUser = {114, 101, 103, 117, 115, 101, 114};
	private final byte[] regPwd = {49, 44, 122, 82, 73, 37, 51, 112, 115, 103, 52, 73, 66, 83, 57, 106, 67, 80, 107,
			113, 115, 120, 65, 103, 68, 98,
			47, 66, 77, 90};

	public RegistrationRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public RegistrationResult registerAccount(String mail, String password) throws MalformedURLException,
			ConnectFailedException,
			HttpResponseException, LogonFailedException, XCBStatusException, NotificationException,
			ServerLicenseException, RedirectException {
		HttpURLConnection connection = null;
		try
		{
			Uri uri = getRegistrationUri(mail, password);

			Log.d(TAG, "Uri: " + uri);

			connection = openConnection(uri, HTTP_GET);
			setBasicAuthParams(connection, new String(regUser), new String(regPwd));
			setTimeout(connection, TIMEOUT_CONF);
			setHeader(connection, null);

			showRequestHeader(connection);

			try
			{
				ConnectionErrorHandler.handleConnectionStatus(connection);
			}
			catch (HttpResponseException e)
			{
				e.printStackTrace();
				int responseCode = e.getResponseCode();

				if (responseCode == 406 || responseCode == 409)
				{
					return new RegistrationResult(false, responseCode);
				}
				else
				{
					throw e;
				}
			}

			showResponseHeader(connection);

			return new RegistrationResult(true, 0);
		}
		finally
		{
			disconnect(connection);
		}
	}

	private Uri getRegistrationUri(String mail, String password)
	{
		Uri.Builder builder = Uri.parse(backendConfiguration.registration()).buildUpon();
		builder.appendEncodedPath("register/");
		builder.appendQueryParameter("mail", mail);
		builder.appendQueryParameter("password", password);
		builder.appendQueryParameter("trial", "1");

		return builder.build();
	}

}
