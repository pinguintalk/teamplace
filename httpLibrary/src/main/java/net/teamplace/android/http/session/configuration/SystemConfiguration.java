package net.teamplace.android.http.session.configuration;

import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.util.GenericUtils;

import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class SystemConfiguration
{
	private static final String COL_DEBUG_LEVEL = "debug_level";
	private static final String COL_BANDWIDTH = "bandwidth";
	private static final String COL_PACKETSIZE = "packetsize";
	private static final String COL_CONNECT = "connect";
	private static final String COL_CONNECT_EX = "connect_ex";
	private static final String COL_KEEP_ALIVE_CIRCLE = "keep_alive_circle";
	private static final String COL_TIMEOUT_OPEN_PRINTER = "timeout_open_printer";
	private static final String COL_TIMEOUT_PRINT = "timeout_print";
	private static final String COL_ACCEPTED_FILE_EXT = "accepted_file_ext";
	private static final String COL_HOST = "host";
	private static final String COL_HOST_EX = "host_ex";
	private static final String COL_RECONNECT = "reconnect";
	private static final String COL_VALIDITY = "validity";

	protected static final String TABLE_PART = COL_DEBUG_LEVEL + " integer, " + COL_BANDWIDTH + " integer, "
			+ COL_PACKETSIZE + " integer, " + COL_CONNECT + " text, " + COL_CONNECT_EX + " text, "
			+ COL_KEEP_ALIVE_CIRCLE + " integer, " + COL_TIMEOUT_OPEN_PRINTER + " integer, " + COL_TIMEOUT_PRINT
			+ " integer, " + COL_ACCEPTED_FILE_EXT + " text, " + COL_HOST + " text, " + COL_HOST_EX + " text, "
			+ COL_RECONNECT + " integer, " + COL_VALIDITY + " integer";

	private static final String DIVIDER = ";";

	@JsonProperty("DBG")
	private int mDebugLevel;

	@JsonProperty("BW")
	private int mBandWidth;

	@JsonProperty("PACKETSIZE")
	private int mPacketSize;

	@JsonProperty("CONNECT")
	private String mConnectURL;

	@JsonProperty("CONNECTEX")
	private String mConnectExURL;

	@JsonProperty("TESTIV")
	private int mKeepAliveCircle;

	@JsonProperty("TOOPENPRINTER")
	private int mTimeoutOpenPrinter;

	@JsonProperty("TOOPRINT")
	private int mTimeoutPrint;

	@JsonProperty("FILEEXT")
	private String mServerAcceptedFileExtensions;

	@JsonProperty("HOST")
	private String mHost;

	@JsonProperty("HOSTEX")
	private String mHostEx;

	@JsonProperty("RECONNECT")
	private int mReconnect;

	@JsonProperty("VALIDITY")
	private int mValidity;

	public SystemConfiguration()
	{
	}

	public SystemConfiguration(Cursor cursor)
	{
		mDebugLevel = cursor.getInt(cursor.getColumnIndex(COL_DEBUG_LEVEL));
		mBandWidth = cursor.getInt(cursor.getColumnIndex(COL_BANDWIDTH));
		mPacketSize = cursor.getInt(cursor.getColumnIndex(COL_PACKETSIZE));
		mConnectURL = cursor.getString(cursor.getColumnIndex(COL_CONNECT));
		mConnectExURL = cursor.getString(cursor.getColumnIndex(COL_CONNECT_EX));
		mKeepAliveCircle = cursor.getInt(cursor.getColumnIndex(COL_KEEP_ALIVE_CIRCLE));
		mTimeoutOpenPrinter = cursor.getInt(cursor.getColumnIndex(COL_TIMEOUT_OPEN_PRINTER));
		mTimeoutPrint = cursor.getInt(cursor.getColumnIndex(COL_TIMEOUT_PRINT));
		mServerAcceptedFileExtensions = cursor.getString(cursor.getColumnIndex(COL_ACCEPTED_FILE_EXT));
		mHost = cursor.getString(cursor.getColumnIndex(COL_HOST));
		mHostEx = cursor.getString(cursor.getColumnIndex(COL_HOST_EX));
		mReconnect = cursor.getInt(cursor.getColumnIndex(COL_RECONNECT));
		mValidity = cursor.getInt(cursor.getColumnIndex(COL_VALIDITY));
	}

	public int getDebugLevel()
	{
		return mDebugLevel;
	}

	public void setDebugLevel(int debugLevel)
	{
		mDebugLevel = debugLevel;
	}

	public int getBandWidth()
	{
		return mBandWidth;
	}

	public void setBandWidth(int bandWidth)
	{
		mBandWidth = bandWidth;
	}

	public int getPacketSize()
	{
		return mPacketSize;
	}

	public void setPacketSize(int packetSize)
	{
		mPacketSize = packetSize;
	}

	public String getConnectURL()
	{
		return mConnectURL;
	}

	public void setConnectURL(String connectURL)
	{
		mConnectURL = connectURL;
	}

	public String getConnectExURL()
	{
		return mConnectExURL;
	}

	public void setConnectExURL(String connectExURL)
	{
		mConnectExURL = connectExURL;
	}

	public int getKeepAliveCircle()
	{
		return mKeepAliveCircle;
	}

	public void setKeepAliveCircle(int keepAliveCircle)
	{
		mKeepAliveCircle = keepAliveCircle;
	}

	public int getTimeoutOpenPrinter()
	{
		return mTimeoutOpenPrinter;
	}

	public void setTimeoutOpenPrinter(int timeoutOpenPrinter)
	{
		mTimeoutOpenPrinter = timeoutOpenPrinter;
	}

	public int getTimeoutPrint()
	{
		return mTimeoutPrint;
	}

	public void setTimeoutPrint(int timeoutPrint)
	{
		mTimeoutPrint = timeoutPrint;
	}

	public String getServerAcceptedFileExtensionsAsString()
	{
		return mServerAcceptedFileExtensions;
	}

	public void setServerAcceptedFileExtensionsAsString(String serverAcceptedFileExtensionsString)
	{
		mServerAcceptedFileExtensions = serverAcceptedFileExtensionsString;
	}

	public List<String> getServerAcceptedFileExtensionsAsList()
	{
		return GenericUtils.splitStringByDivider(mServerAcceptedFileExtensions, DIVIDER);
	}

	public String getHost()
	{
		return mHost;
	}

	public void setHost(String host)
	{
		mHost = host;
	}

	public String getHostEx()
	{
		return mHostEx;
	}

	public void setHostEx(String hostEx)
	{
		mHostEx = hostEx;
	}

	public int getReconnect()
	{
		return mReconnect;
	}

	public void setReconnect(int reconnect)
	{
		mReconnect = reconnect;
	}

	public int getValidity()
	{
		return mValidity;
	}

	public void setValidity(int validity)
	{
		mValidity = validity;
	}

	public boolean isExtensionAcceptedByServer(String extension)
	{
		return getServerAcceptedFileExtensionsAsList().contains(extension) ? true : false;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_DEBUG_LEVEL, mDebugLevel);
		values.put(COL_BANDWIDTH, mBandWidth);
		values.put(COL_PACKETSIZE, mPacketSize);
		values.put(COL_CONNECT, mConnectURL);
		values.put(COL_CONNECT_EX, mConnectExURL);
		values.put(COL_KEEP_ALIVE_CIRCLE, mKeepAliveCircle);
		values.put(COL_TIMEOUT_OPEN_PRINTER, mTimeoutOpenPrinter);
		values.put(COL_TIMEOUT_PRINT, mTimeoutPrint);
		values.put(COL_ACCEPTED_FILE_EXT, mServerAcceptedFileExtensions);
		values.put(COL_HOST, mHost);
		values.put(COL_HOST_EX, mHostEx);
		values.put(COL_RECONNECT, mReconnect);
		values.put(COL_VALIDITY, mValidity);

		return values;
	}
}