package net.teamplace.android.http.teamdrive;

import java.io.IOException;
import java.util.Arrays;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class TeamDrive
{

	@JsonProperty("Id")
	private String id;

	@JsonProperty("Name")
	private String name;

	@JsonProperty("Note")
	private String note;

	@JsonProperty("FileServiceUrl")
	private String fileServiceUrl;

	@JsonProperty("ApiBaseUrl")
	private String apiBaseUrl;

	@JsonProperty("IsQuotaUnlimited")
	private Boolean isQuotaUnlimited;

	@JsonProperty("ImgPath")
	private String imgPath;

	@JsonProperty("Payment")
	private Payment payment;

	@JsonProperty("Users")
	private User[] users;

	@JsonProperty("InvitedUsers")
	private User[] invitedUsers;

	@JsonProperty("Role")
	private Integer role;

	@JsonProperty("Rights")
	@JsonSerialize(using = Rights.Serializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private Rights rights;

	@JsonProperty("PotentialRights")
	@JsonSerialize(using = Rights.Serializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private Rights potentialRights;

	@JsonProperty("CcsRights")
	@JsonSerialize(using = CcsRights.Serializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private CcsRights ccsRights;

	public TeamDrive()
	{
	}

	public TeamDrive(String id, String name, String note, String fileServiceUrl, String apiBaseUrl,
			Boolean isQuotaUnlimited, String imgPath, Payment payment, User[] users, User[] invitedUsers, Integer role,
			Rights rights, Rights potentialRights, CcsRights ccsRights)
	{
		super();
		this.id = id;
		this.name = name;
		this.note = note;
		this.fileServiceUrl = fileServiceUrl;
		this.apiBaseUrl = apiBaseUrl;
		this.isQuotaUnlimited = isQuotaUnlimited;
		this.imgPath = imgPath;
		this.payment = payment;
		this.users = users;
		this.invitedUsers = invitedUsers;
		this.role = role;
		this.rights = rights;
		this.potentialRights = potentialRights;
		this.ccsRights = ccsRights;
	}

	public static TeamDrive createFromJson(String json) throws JsonParseException, JsonMappingException, IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, TeamDrive.class);
	}

	public static TeamDrive[] createArrayFromJson(String json) throws JsonParseException, JsonMappingException,
			IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, TeamDrive[].class);
	}

	public static String createJsonObject(TeamDrive... in) throws JsonProcessingException
	{
		return JsonUtils.getDefaultMapper().writeValueAsString(in.length > 1 ? in : in[0]);
	}

	public String getId()
	{
		return id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}

	public String getFileServiceUrl()
	{
		return fileServiceUrl;
	}

	public void setFileServiceUrl(String fileServiceUrl)
	{
		this.fileServiceUrl = fileServiceUrl;
	}

	public String getApiBaseUrl()
	{
		return apiBaseUrl;
	}

	public void setApiBaseUrl(String apiBaseUrl)
	{
		this.apiBaseUrl = apiBaseUrl;
	}

	public Boolean isQuotaUnlimited()
	{
		return isQuotaUnlimited;
	}

	public void setQuotaUnlimited(Boolean isQuotaUnlimited)
	{
		this.isQuotaUnlimited = isQuotaUnlimited;
	}

	public String getImgPath()
	{
		return imgPath;
	}

	public void setImgPath(String imgPath)
	{
		this.imgPath = imgPath;
	}

	public Payment getPayment()
	{
		return payment;
	}

	public void setPayment(Payment payment)
	{
		this.payment = payment;
	}

	public User[] getUsers()
	{
		return users;
	}

	public void setUsers(User[] users)
	{
		this.users = users;
	}

	public User[] getInvitedUsers()
	{
		return invitedUsers;
	}

	public void setInvitedUsers(User[] invitedUsers)
	{
		this.invitedUsers = invitedUsers;
	}

	public Integer getRole()
	{
		return role;
	}

	public void setRole(Integer role)
	{
		this.role = role;
	}

	public Rights getRights()
	{
		return rights;
	}

	public void setRights(Rights rights)
	{
		this.rights = rights;
	}

	public Rights getPotentialRights()
	{
		return potentialRights;
	}

	public void setPotentialRights(Rights potentialRights)
	{
		this.potentialRights = potentialRights;
	}

	public CcsRights getCcsRights()
	{
		return ccsRights;
	}

	public void setCcsRights(CcsRights ccsRights)
	{
		this.ccsRights = ccsRights;
	}
}
