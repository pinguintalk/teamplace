package net.teamplace.android.http.search;

import java.util.List;

public class FileSearchResult
{

	public static final String STATUS_RUNNING = "running";
	public static final String STATUS_FINISHED = "finished";
	public static final String STATUS_CANCELLED = "cancel";

	private List<Item> items;
	private String findId;
	private String findStatus;
	private long findTime;

	public FileSearchResult()
	{
	}

	public String getFindId()
	{
		return findId;
	}

	public void setFindId(String findId)
	{
		this.findId = findId;
	}

	public String getFindStatus()
	{
		return findStatus;
	}

	public void setFindStatus(String findStatus)
	{
		this.findStatus = findStatus;
	}

	public List<Item> getItems()
	{
		return items;
	}

	public void setItems(List<Item> items)
	{
		this.items = items;
	}

	public long getFindTime()
	{
		return findTime;
	}

	public void setFindTime(long findTime)
	{
		this.findTime = findTime;
	}

	public static class Item
	{
		private long size = -1l;
		private int flags = 0;
		private long date = -1l;
		private String name = null;
		private String excerpt = null;
		private String baseFolder = null;

		public Item()
		{
		}

		public Item(long size, int flags, long date, String name, String excerpt, String baseFolder)
		{
			this.size = size;
			this.flags = flags;
			this.date = date;
			this.name = name;
			this.excerpt = excerpt;
			this.baseFolder = baseFolder;
		}

		public long getSize()
		{
			return size;
		}

		public void setSize(long size)
		{
			this.size = size;
		}

		public int getFlags()
		{
			return flags;
		}

		public void setFlags(int flags)
		{
			this.flags = flags;
		}

		public long getDate()
		{
			return date;
		}

		public void setDate(long date)
		{
			this.date = date;
		}

		public String getName()
		{
			return name;
		}

		public void setName(String name)
		{
			this.name = name;
		}

		public String getExcerpt()
		{
			return excerpt;
		}

		public void setExcerpt(String excerpt)
		{
			this.excerpt = excerpt;
		}

		public String getBaseFolder()
		{
			return baseFolder;
		}

		public void setBaseFolder(String baseFolder)
		{
			this.baseFolder = baseFolder;
		}

	}
}
