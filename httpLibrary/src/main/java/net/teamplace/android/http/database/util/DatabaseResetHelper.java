package net.teamplace.android.http.database.util;

import net.teamplace.android.http.database.DatabaseLockedException;
import net.teamplace.android.http.session.database.SessionDatabaseActions;
import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.request.faulttolerant.download.UpDownloadSQLiteHelper;
import net.teamplace.android.http.teamdrive.database.TeamDriveDatabaseActions;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteOpenHelper;


public final class DatabaseResetHelper
{

	private DatabaseResetHelper()
	{
	}

	public static void killAllDatabases(Context context)
	{
		SQLiteOpenHelper[] helpers = new SQLiteOpenHelper[] {
				new UpDownloadSQLiteHelper(context),
//				SessionSQLiteHelper.getInstance(context),
//				TeamDr|ΩiveSQLiteOpenHelper.getInstance(context)
 	};

		for (SQLiteOpenHelper helper : helpers)
		{
			context.deleteDatabase(helper.getDatabaseName());
		}
		TeamDriveDatabaseActions.getInstance().clearAllTables();
		SessionDatabaseActions.getInstance().deleteAllRowsInDatabase();

		Intent deleteSortDatabase = new Intent("com.cortado.android.ACTION_DELETE_SORT_DATABASE");
		context.sendBroadcast(deleteSortDatabase);
	}
}
