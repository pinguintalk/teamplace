package net.teamplace.android.http.exception;

public class FileWasModifiedException extends Exception
{
	private static final long serialVersionUID = 802840607087564131L;

	public FileWasModifiedException()
	{
	}

	public FileWasModifiedException(String detailMessage)
	{
		super(detailMessage);
	}

	public FileWasModifiedException(Throwable throwable)
	{
		super(throwable);
	}

	public FileWasModifiedException(String detailMessage, Throwable throwable)
	{
		super(detailMessage, throwable);
	}

}
