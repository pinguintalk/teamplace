package net.teamplace.android.http.request.comments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Base64;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class Comment implements Comparable<Comment>, Parcelable
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty("creator")
	private String creator;

	@JsonProperty("created")
	private int created;

	@JsonProperty("modified")
	private int modified;

	@JsonProperty("commentid")
	private String commentId;

	@JsonProperty("fileid")
	private String fileId;

	@JsonProperty("comment")
	private String comment;

	public Comment()
	{
	}

	public Comment(Parcel parcel)
	{
		creator = parcel.readString();
		created = parcel.readInt();
		modified = parcel.readInt();
		commentId = parcel.readString();
		fileId = parcel.readString();
		comment = parcel.readString();
	}

	public String getCreator()
	{
		return creator;
	}

	public void setCreator(String creator)
	{
		this.creator = creator;
	}

	public int getCreated()
	{
		return created;
	}

	public void setCreated(int created)
	{
		this.created = created;
	}

	public int getModified()
	{
		return modified;
	}

	public void setModified(int modified)
	{
		this.modified = modified;
	}

	public String getCommentId()
	{
		return commentId;
	}

	public void setCommentId(String commentId)
	{
		this.commentId = commentId;
	}

	public String getFileId()
	{
		return fileId;
	}

	public void setFileId(String fileId)
	{
		this.fileId = fileId;
	}

	public String getComment()
	{
		return comment;
	}

	public String getCommentBase64Decoded()
	{
		return new String(Base64.decode(comment.getBytes(), Base64.NO_WRAP));
	}

	public void setComment(String comment)
	{
		this.comment = comment;
	}

	@Override
	public int compareTo(Comment c)
	{
		return ((Integer) this.getCreated()).compareTo((Integer) c.getCreated());
	}

	@Override
	public boolean equals(Object o)
	{
		return this.commentId.equals(((Comment) o).commentId);
	}

	@JsonIgnore
	public static List<Comment> createCommentArrayListFromJSON(String json) throws JsonParseException,
			JsonMappingException, IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();

		CollectionType collectionType = mapper.getTypeFactory().constructCollectionType(ArrayList.class, Comment.class);

		return mapper.readValue(json, collectionType);
	}

	@Override
	public int describeContents()
	{
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags)
	{
		dest.writeString(creator);
		dest.writeInt(created);
		dest.writeInt(modified);
		dest.writeString(commentId);
		dest.writeString(fileId);
		dest.writeString(comment);
	}

	public static final Parcelable.Creator<Comment> CREATOR = new Parcelable.Creator<Comment>()
	{
		@Override
		public Comment createFromParcel(Parcel source)
		{
			return new Comment(source);
		}

		@Override
		public Comment[] newArray(int size)
		{
			return new Comment[size];
		}
	};

}