package net.teamplace.android.http.request.faulttolerant.download;

import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.request.faulttolerant.upload.UploadStatus;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class UpDownloadSQLiteHelper extends SQLiteOpenHelper
{
	private static final String DATABASE_NAME = "updownload.db";
	private static final int CURRENT_DATABASE_VERSION = 1;

	public static final String COLUMN_ID = "_id";

	public UpDownloadSQLiteHelper(Context context)
	{
		super(context, DATABASE_NAME, null, CURRENT_DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database)
	{
		database.execSQL(DownloadStatus.CREATE_TABLE);
		database.execSQL(UploadStatus.CREATE_TABLE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion)
	{
	}

//	public static int update(Context context, String table, ContentValues values, String whereClause, String[] whereArgs)
//	{
//		SessionSQLiteHelper dbHelper = SessionSQLiteHelper.getInstance(context);
//		SQLiteDatabase database = null;
//		try
//		{
//			database = dbHelper.getWritableDatabase();
//			database.beginTransaction();
//			int rows = database.update(table, values, whereClause, whereArgs);
//			database.setTransactionSuccessful();
//			return rows;
//		}
//		finally
//		{
//			if (database != null)
//			{
//				if (database.inTransaction())
//				{
//					database.endTransaction();
//				}
//				database.close();
//			}
//		}
//
//	}
//
//	public static long delete(Context context, String table, String whereClause, String[] whereArgs)
//	{
//		SessionSQLiteHelper dbHelper = SessionSQLiteHelper.getInstance(context);
//		SQLiteDatabase database = null;
//		try
//		{
//			database = dbHelper.getWritableDatabase();
//			database.beginTransaction();
//			long rows = database.delete(table, whereClause, whereArgs);
//			database.setTransactionSuccessful();
//			return rows;
//		}
//		finally
//		{
//			if (database != null)
//			{
//				if (database.inTransaction())
//				{
//					database.endTransaction();
//				}
//				database.close();
//			}
//		}
//	}

}
