package net.teamplace.android.http.request.comments;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class FileInfo
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty("filecomments")
	private List<Comment> comments;

	public FileInfo()
	{
	}

	public List<Comment> getComments()
	{
		return comments;
	}

	public void setComments(List<Comment> comments)
	{
		this.comments = comments;
	}

	public static FileInfo createFileInfoFromJSON(String jsonString) throws JsonParseException, JsonMappingException,
			IOException
	{
		return createFileInfoFromJSON(jsonString.getBytes());
	}

	public static FileInfo createFileInfoFromJSON(InputStream jsonInputStream) throws JsonParseException,
			JsonMappingException, IOException
	{
		return createFileInfoFromJSON(IOUtils.toByteArray(jsonInputStream));
	}

	public static FileInfo createFileInfoFromJSON(byte[] jsonBytes) throws JsonParseException, IOException
	{
		FileInfo fileInfo;

		try
		{
			ObjectMapper mapper = JsonUtils.getDefaultMapper();
			fileInfo = mapper.readValue(jsonBytes, FileInfo.class);
		}
		catch (JsonMappingException e)
		{
			fileInfo = new FileInfo();
			fileInfo.setComments(new ArrayList<Comment>());
		}

		return fileInfo;
	}
}
