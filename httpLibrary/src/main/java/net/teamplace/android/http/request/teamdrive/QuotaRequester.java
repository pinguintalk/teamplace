package net.teamplace.android.http.request.teamdrive;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.json.JsonUtils;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.teamdrive.quota.AccountQuotaModel;
import net.teamplace.android.http.teamdrive.quota.TeamdriveQuotaModel;

import java.io.IOException;
import java.nio.charset.Charset;

/**
 * Created by jobol on 06/08/15.
 */
public class QuotaRequester extends BasicRequester {

    private static final String PATH_TEAMDRIVE = "teamdrive";

    public QuotaRequester(Context context, Session session, BackendConfiguration backendConfiguration) {
        super(context, session, backendConfiguration);
    }

    public TeamdriveQuotaModel[] getTeamDriveQuota(String... teamDriveId) throws IOException, HttpResponseException,
            XCBStatusException, ConnectFailedException, ServerLicenseException, LogonFailedException, RedirectException, NotificationException {
        String reqBody = JsonUtils.getDefaultMapper().writeValueAsString(teamDriveId);
        Uri uri = getQuotaUri().buildUpon().appendEncodedPath(PATH_TEAMDRIVE).build();
        String responseBody = doJsonUploadPOST(uri, reqBody.getBytes(Charset.forName("utf-8")), true, true);
        return TeamdriveQuotaModel.arrayFromJson(responseBody);
    }

    public AccountQuotaModel getAccountQuota() throws ServerLicenseException, ConnectFailedException, IOException,
            LogonFailedException, HttpResponseException, RedirectException, NotificationException, XCBStatusException {
        String responseBody = executeStringGETRequest(getQuotaUri(), null, true, true);
        return AccountQuotaModel.fromJson(responseBody);
    }

    private Uri getQuotaUri() {
        return Uri.parse(backendConfiguration.quotaApi());
    }
}
