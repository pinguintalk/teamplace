package net.teamplace.android.http.multipart;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Wraps the InputStream for a HTTP Multipart Upload. HTTPURLConnection or HTTPClient use a single InpuStream object.
 * Because we have several Stream to Upload, we need this Wrapper class, which handle
 * several Streams as one single Stream.
 * 
 * @author jamic
 */
public class MultipartInputStream extends InputStream
{

	private final double THRESHOLD = 0.05;

	private ByteArrayInputStream preContentStream = null;
	private ChunkLimitedInputStream contentStream = null;
	private ByteArrayInputStream finalStream = null;
	private long contentLength = 0;

	private boolean isPreContent = true;
	private boolean isEnding = false;

	private boolean closed = false;

	public MultipartInputStream(byte[] preContent, ChunkLimitedInputStream chunkedStream)
	{
		preContentStream = new ByteArrayInputStream(preContent);
		contentStream = chunkedStream;
		contentStream.openChunk();
		finalStream = new ByteArrayInputStream(MultipartFile.getFinalBoundaryAsBytes());

		contentLength += preContent.length;

		if (chunkedStream.isLastChunk())
		{
			contentLength += chunkedStream.getLastChunkSize();
		}
		else
		{
			contentLength += MultipartFile.CHUNK_SIZE;
		}

		contentLength += MultipartFile.getFinalBoundaryAsBytes().length;
	}

	@Override
	public int read(byte[] buffer) throws IOException
	{
		if (closed)
		{
			throw new IOException("Stream Closed");
		}
		int l = 0;

		if (isPreContent)
		{
			l = preContentStream.read(buffer);

			if (l != -1)
			{
				return l;
			}
			else
			{
				preContentStream.close();
				isPreContent = false;
			}
		}

		if (!isPreContent && !isEnding)
		{
			l = contentStream.read(buffer);

			if (l != -1)
			{
				return l;
			}
			else
			{
				contentStream.close();
				isEnding = true;
			}
		}

		if (isEnding)
		{

			l = finalStream.read(buffer);
			// System.out.println("isEnding, buffer=" + new String(buffer,0,l-1));
			if (l == -1)
			{
				finalStream.close();
				return l;
			}
			else
			{
				return l;
			}
		}
		else
		{
			return 0;
		}
	}

	@Override
	public void close() throws IOException
	{
		closed = true;
		try
		{
			preContentStream.close();
		}
		catch (Exception e)
		{
		}
		try
		{
			contentStream.close();
		}
		catch (Exception e)
		{
		}
		try
		{
			finalStream.close();
		}
		catch (Exception e)
		{
		}
		super.close();
	}

	public void forceClose() throws IOException
	{
		contentStream.closeInnerStream(false);
	}

	public boolean isClosed()
	{
		return closed;
	}

	public long length()
	{
		return contentLength;
	}

	@Override
	public int read() throws IOException
	{
		if (closed)
		{
			throw new IOException("Stream Closed");
		}
		int l = 0;

		if (isPreContent)
		{
			l = preContentStream.read();

			if (l != -1)
			{
				return l;
			}
			else
			{
				preContentStream.close();
				isPreContent = false;
			}
		}

		if (!isPreContent && !isEnding)
		{
			l = contentStream.read();

			if (l != -1)
			{
				return l;
			}
			else
			{
				contentStream.close();
				isEnding = true;
			}
		}

		if (isEnding)
		{
			l = finalStream.read();

			if (l == -1)
			{
				finalStream.close();
				return l;
			}
			else
			{
				return l;
			}
		}
		else
		{
			return 0;
		}
	}
}
