package net.teamplace.android.http.request.configuration;

import java.io.IOException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.configuration.Configuration;
import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

/***
 * <b>Class Overview</b>
 * <p>
 * This class is used to get a client configuration from default Cortado Workplace App Server
 * 
 * @author jamic
 */
public class ConfigurationRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public ConfigurationRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	/***
	 * Requests a configuration for the given account in session.
	 * 
	 * @return {@link Configuration}
	 * @throws ConnectFailedException
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public Configuration getConfiguration() throws ConnectFailedException, XCBStatusException, HttpResponseException,
			LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException, RedirectException {
		return Configuration.createConfigurationFromJSON(executeStringGETRequest(getConfUri(), getMimeJsonHeader(),
				false, false));
	}

	private Uri getConfUri()
	{
		Uri.Builder builder = Uri.parse(backendConfiguration.workplaceServer()).buildUpon();
		builder.appendEncodedPath(GUI_SETUP);
		builder.appendQueryParameter(PARAM_RESPONSE_SCHEMA, RESPONSE_SCHEMA_XML);

		return builder.build();
	}
}
