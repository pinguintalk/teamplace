package net.teamplace.android.http.teamdrive.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class TeamDriveSQLiteOpenHelper extends SQLiteOpenHelper
{

	public static final String DB_NAME = "teamdrives.db";
	public static final int DB_VERSION = 3;

	public static final String COLUMN_ID = "_id";

	private int inUse = 0;

	private Object lockMe = new Object();

	TeamDriveSQLiteOpenHelper(Context context)
	{
		super(context, DB_NAME, null, DB_VERSION);
	}

	public void lazyClose()
	{
		synchronized (lockMe)
		{
			if (inUse < 2)
			{
				close();
			}
			inUse--;
		}
	}

	@Override
	public void onCreate(SQLiteDatabase db)
	{
		TableTeamDrives.create(db);
		TableUsers.create(db);
		TableTeamDriveUsers.create(db);
		TableDriveEtags.create(db);
		TableInvitations.create(db);
		TableInvitationEtags.create(db);
		TableGlobaConfiguration.create(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
	{
		while (oldVersion < newVersion) {
			TableTeamDrives.upgrade(db, oldVersion, newVersion);
			TableUsers.upgrade(db, oldVersion, newVersion);
			TableTeamDriveUsers.upgrade(db, oldVersion, newVersion);
			TableDriveEtags.upgrade(db, oldVersion, newVersion);
			TableInvitations.upgrade(db, oldVersion, newVersion);
			TableInvitationEtags.upgrade(db, oldVersion, newVersion);
			TableGlobaConfiguration.upgrade(db, oldVersion, newVersion);
			oldVersion++;
		}
	}

	@Override
	public SQLiteDatabase getWritableDatabase()
	{
		synchronized (lockMe)
		{
			inUse++;
		}
		return super.getWritableDatabase();
	}

	@Override
	public SQLiteDatabase getReadableDatabase()
	{
		synchronized (lockMe)
		{
			inUse++;
		}
		return super.getReadableDatabase();
	}

}
