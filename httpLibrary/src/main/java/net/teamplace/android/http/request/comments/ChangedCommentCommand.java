package net.teamplace.android.http.request.comments;

import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;
import net.teamplace.android.http.json.JsonUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonPropertyOrder(alphabetic = true)
@JsonInclude(Include.NON_NULL)
@JacksonDataModel
public class ChangedCommentCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private ChangedCommentParameter params;

	public ChangedCommentCommand(String teamDriveId, String folder, String file, long since, List<String> commentIds)
	{
		setCommand(CMD_CHANGED_COMMENT);

		params = new ChangedCommentParameter(teamDriveId, folder, file, since, commentIds);
	}

	@JsonIgnore
	public String getAsJSONString() throws JsonProcessingException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		return mapper.writeValueAsString(this);
	}

	@JsonIgnore
	public byte[] getAsJSONByteArray() throws JsonProcessingException
	{
		return getAsJSONString().getBytes();
	}
}
