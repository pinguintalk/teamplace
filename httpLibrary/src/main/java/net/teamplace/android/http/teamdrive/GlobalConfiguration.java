package net.teamplace.android.http.teamdrive;

import java.io.IOException;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class GlobalConfiguration
{

	@JsonProperty("MaxFreeDrives")
	private int maxFreeDrives;

	@JsonProperty("PotentialRights")
	@JsonSerialize(using = Rights.Serializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private Rights potentialRights;

	@JsonProperty("Rights")
	@JsonSerialize(using = Rights.Serializer.class, include = JsonSerialize.Inclusion.NON_NULL)
	private Rights rights;

	@JsonProperty("MaxUsersFreeTeamdrive")
	private int maxUsersFreeTeamDrive;

	@JsonProperty("MaxUsersPremiumTeamDrive")
	private int maxUsersPremiumTeamDrive;

	@JsonProperty("DaysBeforeFreeTeamDrivesExpire")
	private int daysBeforeFreeTeamDrivesExpire;

	public GlobalConfiguration()
	{
	}

	public GlobalConfiguration(int maxFreeDrives, Rights potentialRights, Rights rights, int maxUsersFreeTeamDrive,
			int maxUsersPremiumTeamDrive, int daysBeforeFreeTeamDrivesExpire)
	{
		this.maxFreeDrives = maxFreeDrives;
		this.potentialRights = potentialRights;
		this.rights = rights;
		this.maxUsersFreeTeamDrive = maxUsersFreeTeamDrive;
		this.maxUsersPremiumTeamDrive = maxUsersPremiumTeamDrive;
		this.daysBeforeFreeTeamDrivesExpire = daysBeforeFreeTeamDrivesExpire;
	}

	public static GlobalConfiguration createFromJson(String json) throws JsonParseException, JsonMappingException,
			IOException
	{
		return JsonUtils.getDefaultMapper().readValue(json, GlobalConfiguration.class);
	}

	public int getMaxFreeDrives()
	{
		return maxFreeDrives;
	}

	public void setMaxFreeDrives(int maxFreeDrives)
	{
		this.maxFreeDrives = maxFreeDrives;
	}

	public Rights getPotentialRights()
	{
		return potentialRights;
	}

	public void setPotentialRights(Rights potentialRights)
	{
		this.potentialRights = potentialRights;
	}

	public Rights getRights()
	{
		return rights;
	}

	public void setRights(Rights rights)
	{
		this.rights = rights;
	}

	public int getMaxUsersFreeTeamDrive()
	{
		return maxUsersFreeTeamDrive;
	}

	public void setMaxUsersFreeTeamDrive(int maxUsersFreeTeamDrive)
	{
		this.maxUsersFreeTeamDrive = maxUsersFreeTeamDrive;
	}

	public int getMaxUsersPremiumTeamDrive()
	{
		return maxUsersPremiumTeamDrive;
	}

	public void setMaxUsersPremiumTeamDrive(int maxUsersPremiumTeamDrive)
	{
		this.maxUsersPremiumTeamDrive = maxUsersPremiumTeamDrive;
	}

	public int getDaysBeforeFreeTeamDrivesExpire()
	{
		return daysBeforeFreeTeamDrivesExpire;
	}

	public void setDaysBeforeFreeTeamDrivesExpire(int daysBeforeFreeTeamDrivesExpire)
	{
		this.daysBeforeFreeTeamDrivesExpire = daysBeforeFreeTeamDrivesExpire;
	}

}
