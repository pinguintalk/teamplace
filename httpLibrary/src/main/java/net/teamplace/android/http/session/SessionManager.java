package net.teamplace.android.http.session;

import android.accounts.Account;
import android.content.Context;
import android.util.Log;

import net.teamplace.android.http.database.DatabaseLockedException;
import net.teamplace.android.http.session.database.SessionDatabaseActions;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.TeamDrive;

import java.util.HashMap;
import java.util.Map;


public class SessionManager
{
	private final String TAG = getClass().getSimpleName();

	private static Map<Account, Session> sessionCache;

	private final Context mContext;

	private SessionDatabaseActions mDatabaseActions;

	public SessionManager(Context context)
	{
		mContext = context;
		mDatabaseActions = SessionDatabaseActions.getInstance();
		if (sessionCache == null)
		{
			sessionCache = new HashMap<Account, Session>();
		}
	}

	public void killSession()
	{
		if (sessionCache != null)
		{
			sessionCache.clear();
			Log.i(TAG, "SessionCache killed");
		}
		killSessionFromDatabase();
	}

	public void clearAllSessions()
	{
		if (sessionCache != null)
		{
			sessionCache.clear();
		}
		clearAllSessionsFromDatabase();
	}

	private void clearAllSessionsFromDatabase()
	{
		try {
			mDatabaseActions.delete(Session.TABLE_NAME, null, null);
		} catch (DatabaseLockedException e) {
			e.printStackTrace();
		}
	}

	private boolean killSessionFromDatabase()
	{
		try {
			return mDatabaseActions.killSessionFromDatabase();
		} catch (DatabaseLockedException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Returns a Session object for a given Account or null if no Session was found
	 * 
	 * @return
	 */
	public Session getSessionForAccount(Account account)
	{
		if (sessionCache != null)
		{
			sessionCache.get(account);
		}

		// TODO search in database if nothing in cache

		return null;
	}

	/**
	 * Returns the last used Session or null if no Session was found
	 * 
	 * @return
	 */
	public Session getLastSession()
	{
		Session lastSession = getSessionFromCache();

		if (lastSession != null)
		{
			Log.d(TAG, "Session was loaded from cache");
			return lastSession;
		}

		try {
			lastSession = mDatabaseActions.getSessionFromDatabase();
		} catch (DatabaseLockedException e) {
			e.printStackTrace();
		}

		if (lastSession != null)
		{
			Log.d(TAG, "Session was loaded from database");
			return lastSession;
		}

		return null;
	}

	// When updating TeamDrives, the TeamDrive DB is already updated from TeamDriveService, so
	// updating the cached Session here is sufficient.
	public void updateCachedSessionWithTeamDrives(TeamDrive[] drives, GlobalConfiguration globalConfig)
	{
		Session inCache = getSessionFromCache();
		if (inCache != null)
		{
			inCache.setTeamDrives(drives);
			inCache.setTeamDriveGlobalConfig(globalConfig);
			sessionCache.put(inCache.getAccount(), inCache);
		}
	}

	public void updateTeamDriveInCachedSession(TeamDrive drive)
	{
		Session inCache = getSessionFromCache();
		if (inCache != null)
		{
			inCache.getTeamDriveMap().put(drive.getId(), drive);
		}
	}

	private Session getSessionFromCache()
	{
		Log.d(TAG, "Try to load session from cache");

		if (sessionCache != null)
		{
			long sessionTimestamp = 0;
			Session currentSession = null;
			Account latestAccount = null;

			for (Account account : sessionCache.keySet())
			{
				currentSession = sessionCache.get(account);

				if (currentSession.getTimestamp() > sessionTimestamp)
				{
					sessionTimestamp = currentSession.getTimestamp();
					latestAccount = account;
				}
			}

			if (latestAccount != null)
			{
				return sessionCache.get(latestAccount);
			}
		}

		return null;
	}


	public void saveSession(Session session) {
		sessionCache.put(session.getAccount(), session);
		try {
			mDatabaseActions.saveSessionInDatabase(session);
			mDatabaseActions.saveConfigurationInDatabase(session.getConfiguration());
		} catch (DatabaseLockedException e) {
			e.printStackTrace();
		}

	}


}
