package net.teamplace.android.http.exception;

public class RegistrationNeededException extends IllegalLogonDataException
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2418425965884809556L;

	public RegistrationNeededException()
	{
		this("need to register first");
	}

	public RegistrationNeededException(String strError)
	{
		super(strError);
	}

}
