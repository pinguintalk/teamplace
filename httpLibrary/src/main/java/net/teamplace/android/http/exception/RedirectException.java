package net.teamplace.android.http.exception;

/**
 * Created by jobol on 22/06/15.
 */
public class RedirectException extends CortadoException {

    private String redirectLocation;

    public RedirectException(String redirectLocation) {
        super("redirect to: " + redirectLocation);
        this.redirectLocation = redirectLocation;
    }

    public String getRedirectLocation() {
        return redirectLocation;
    }
}
