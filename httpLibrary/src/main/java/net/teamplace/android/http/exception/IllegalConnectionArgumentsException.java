package net.teamplace.android.http.exception;

public class IllegalConnectionArgumentsException extends ConnectFailedException
{

	public IllegalConnectionArgumentsException(String strMg)
	{
		super(strMg);
	}

	/**
     * 
     */
	private static final long serialVersionUID = 7077131262381317918L;

}
