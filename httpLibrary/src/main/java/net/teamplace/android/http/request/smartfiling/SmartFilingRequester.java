package net.teamplace.android.http.request.smartfiling;

import java.io.IOException;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SmartFilingRequester extends BasicRequester
{

	public SmartFilingRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public List<SmartFilingLocation> requestSmartFilingLocations(String fileName) throws ConnectFailedException,
			XCBStatusException,
			HttpResponseException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException,
			IOException, TeamDriveNotFoundException, RedirectException {
		Uri uri = getLocationUri(getServerForTeamDriveId(null), fileName);

		String result = executeStringGETRequest(uri, getMimeJsonHeader(), true, true);

		return SmartFilingLocation.createSmartFilingTargetArrayListFromJSON(result);
	}

	private Uri getLocationUri(String server, String fileName)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon();
		builder.appendEncodedPath(GUI_SETUP);
		builder.appendQueryParameter(PARAM_CHECK, VALUE_CHECK_LOCATION);
		builder.appendQueryParameter(PARAM_FILE, fileName);

		return builder.build();
	}
}
