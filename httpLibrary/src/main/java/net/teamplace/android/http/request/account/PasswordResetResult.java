package net.teamplace.android.http.request.account;

import android.os.Parcel;

public class PasswordResetResult extends ResponseResult
{
	public static final int RESULT_NOT_REGISTERED = 0;
	public static final int RESULT_SUCCESS = 1;
	public static final int RESULT_EMAIL_ERROR = 8;

	public PasswordResetResult(String message, int resultCode, Boolean success)
	{
		super(message, resultCode, success);
	}

	public PasswordResetResult(Parcel in)
	{
		super(in);
	}
}
