package net.teamplace.android.http.exception;

public class AccountNotFoundException extends FileNotFoundException
{

	/**
     * 
     */
	private static final long serialVersionUID = -2262769888954726580L;

	public AccountNotFoundException(String strAccount)
	{
		super(strAccount + " not found");
	}
}
