package net.teamplace.android.http.request.search;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonFileSearchResultParser
{
	private static final String SIZE = "size";
	private static final String FLAGS = "flags";
	private static final String DATE = "date";
	private static final String NAME = "name";
	private static final String EXCERPT = "excerpt";

	public static List<FileSearchResult.Item> parseJsonToFileSearchResult(String jsonIn)
	{
		List<FileSearchResult.Item> results = new ArrayList<FileSearchResult.Item>();
		try
		{
			JSONArray resultArr = new JSONArray(jsonIn);
			for (int i = 0; i < resultArr.length(); i++)
			{
				JSONObject jsonObj = (JSONObject) resultArr.get(i);
				FileSearchResult.Item fsr = new FileSearchResult.Item();
				fsr.setDate(jsonObj.getLong(DATE));
				fsr.setExcerpt(jsonObj.getString(EXCERPT));
				fsr.setFlags(jsonObj.getInt(FLAGS));
				fsr.setName(jsonObj.getString(NAME));
				fsr.setSize(jsonObj.getLong(SIZE));
				results.add(fsr);
			}
			return results;
		}
		catch (JSONException e)
		{
			e.printStackTrace();
			return null;
		}

	}

}
