package net.teamplace.android.http.multipart;

import java.io.IOException;
import java.io.InputStream;

import net.teamplace.android.http.exception.ClosedByUserException;
import net.teamplace.android.http.request.faulttolerant.ProgressCallback;

import android.util.Log;


public class ChunkLimitedInputStream extends InputStream
{
	private final String TAG = getClass().getSimpleName();

	private final double THRESHOLD = 0.01;

	private final InputStream innerStream;
	private final int chunkSize;
	private final long totalSize;
	private int chunkRead;
	private long totalRead;
	private boolean chunkClosed;
	private ProgressCallback mCallback;

	private boolean mClosedByUser;

	/*---- Progress ----*/

	private long memory = 0;
	private long thres = 0;

	public ChunkLimitedInputStream(InputStream innerStream, int chunkSize, int totalSize) throws IOException
	{
		this(innerStream, chunkSize, totalSize, 0);
	}

	public ChunkLimitedInputStream(InputStream innerStream, int chunkSize, int totalSize, long skipBytes)
			throws IOException
	{
		super();
		this.innerStream = innerStream;
		this.chunkSize = chunkSize;
		this.totalSize = totalSize;
		this.totalRead = skipBytes;

		this.innerStream.skip(skipBytes);

		thres = (long) (totalSize * THRESHOLD);
	}

	public void setCallback(ProgressCallback pCallback)
	{
		mCallback = pCallback;
	}

	private void calculateAndPublishProgress()
	{
		if (totalRead > (memory + thres) || totalRead == totalSize)
		{
			int percent = (int) ((totalRead * 100) / totalSize);

			if (mCallback != null)
			{
				mCallback.publishProgressInPercent(percent);
			}

			memory = totalRead;
		}
	}

	@Override
	public int read(byte[] buffer) throws IOException
	{
		if (chunkClosed)
		{
			return -1;
		}

		if (mClosedByUser)
		{
			throw new ClosedByUserException("Stream was closed by user");
		}

		int readSize = 0;
		if (buffer.length < chunkSize - chunkRead)
		{
			readSize = buffer.length;
		}
		else
		{
			readSize = chunkSize - chunkRead;
		}
		int count = innerStream.read(buffer, 0, readSize);
		if (count != -1)
		{
			chunkRead += count;
			totalRead += count;
		}

		calculateAndPublishProgress();

		if (chunkRead >= chunkSize || totalRead >= totalSize)
		{
			chunkClosed = true;
		}
		return count;
	}

	@Override
	public int read() throws IOException
	{
		if (chunkClosed)
		{
			return -1;
		}

		if (mClosedByUser)
		{
			throw new ClosedByUserException("Stream was closed by user");
		}

		int i = innerStream.read();

		if (i != -1)
		{
			// chunkRead += i;
			// totalRead += i;
			chunkRead++;
			totalRead++;
		}

		calculateAndPublishProgress();

		if (chunkRead >= chunkSize || totalRead >= totalSize)
		{
			chunkClosed = true;
		}
		return i;
	}

	@Override
	public void close() throws IOException
	{
		if (totalRead == totalSize)
		{
			closeInnerStream(false);
		}
		return;
	}

	public void openChunk()
	{
		chunkRead = 0;
		chunkClosed = false;
	}

	public void closeInnerStream(boolean closedByUser) throws IOException
	{
		Log.d(TAG, "Total read: " + totalRead);

		mClosedByUser = closedByUser;

		innerStream.close();
	}

	public boolean hasNextChunk()
	{
		return totalRead < totalSize;
	}

	public boolean isLastChunk()
	{
		return (totalSize - totalRead) <= chunkSize;

	}

	public long getLastChunkSize()
	{
		if (isLastChunk())
		{
			return (int) totalSize - totalRead;
		}
		else
		{
			return -1;
		}
	}

	@Override
	public int available() throws IOException
	{
		if (chunkClosed)
		{
			return 0;
		}
		if (innerStream.available() > (chunkSize - chunkRead))
		{
			return chunkSize - chunkRead;
		}
		return innerStream.available();
	}

}
