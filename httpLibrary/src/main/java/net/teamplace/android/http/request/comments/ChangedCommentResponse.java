package net.teamplace.android.http.request.comments;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;
import net.teamplace.android.http.request.ServerParams;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class ChangedCommentResponse implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(PARAM_MODIFIED)
	private List<Comment> modified;

	@JsonProperty(PARAM_ADDED)
	private List<Comment> added;

	@JsonProperty(PARAM_NOT_MODIFIED)
	private List<Comment> notModified;

	@JsonProperty(PARAM_DELETED)
	private List<Comment> deleted;

	public ChangedCommentResponse()
	{
	}

	public List<Comment> getModified()
	{
		return modified;
	}

	public void setModified(List<Comment> modified)
	{
		this.modified = modified;
	}

	public List<Comment> getAdded()
	{
		return added;
	}

	public void setAdded(List<Comment> added)
	{
		this.added = added;
	}

	public List<Comment> getNotModified()
	{
		return notModified;
	}

	public void setNotModified(List<Comment> notModified)
	{
		this.notModified = notModified;
	}

	public List<Comment> getDeleted()
	{
		return deleted;
	}

	public void setDeleted(List<Comment> deleted)
	{
		this.deleted = deleted;
	}

	public static ChangedCommentResponse createChangedCommentResponseFromJSON(String jsonString)
			throws JsonParseException, JsonMappingException,
			IOException
	{
		return createChangedCommentResponseFromJSON(jsonString.getBytes());
	}

	public static ChangedCommentResponse createChangedCommentResponseFromJSON(InputStream jsonInputStream)
			throws JsonParseException,
			JsonMappingException, IOException
	{
		return createChangedCommentResponseFromJSON(IOUtils.toByteArray(jsonInputStream));
	}

	public static ChangedCommentResponse createChangedCommentResponseFromJSON(byte[] jsonBytes)
			throws JsonParseException, IOException
	{
		ChangedCommentResponse changedCommentResponse;

		try
		{
			ObjectMapper mapper = JsonUtils.getDefaultMapper();
			changedCommentResponse = mapper.readValue(jsonBytes, ChangedCommentResponse.class);
		}
		catch (JsonMappingException e)
		{
			changedCommentResponse = new ChangedCommentResponse();
		}

		return changedCommentResponse;
	}
}
