package net.teamplace.android.http.json;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.request.ServerParams;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class Command implements ServerParams
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_CMD)
	private String command;

	public Command()
	{
	}

	public String getCommand()
	{
		return command;
	}

	public void setCommand(String command)
	{
		this.command = command;
	}

	@JsonIgnore
	public String getAsJSONString() throws JsonProcessingException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		return mapper.writeValueAsString(this);
	}

	@JsonIgnore
	public byte[] getAsJSONByteArray() throws JsonProcessingException
	{
		return getAsJSONString().getBytes();
	}
}
