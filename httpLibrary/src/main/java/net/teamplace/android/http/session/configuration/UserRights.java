package net.teamplace.android.http.session.configuration;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class UserRights
{
	private final String HEX_PREFIX = "0x";

	public final static long BITMASK_SHOW_REMOTEDIR = 0x00001; // show remote files
	public final static long BITMASK_SHOW_PRINTERSEARCH = 0x00002; // enable to search for printers (Corporate only)
	public final static long BITMASK_SHOW_PRINTERSEARCHEX = 0x00004; // enable to search for printers with properties
																		// (Corporate only)
	public final static long BITMASK_SHOW_PRINTBYDRIVER = 0x00008; // enable print by driver
	public final static long BITMASK_SHOW_ONETIMEPW = 0x00010; // use one time passwords
	public final static long BITMASK_SHOW_WIFIPRINT = 0x00020; // enable wifi based printing
	public final static long BITMASK_SHOW_GPS = 0x00040; // transmit location data
	public final static long BITMASK_SHOW_ENTERPRISESEARCH = 0x00080; // show enterprise search
	public final static long BITMASK_SHOW_FORCEPDFDOWNLOAD = 0x00100; // force download as PDF
	public final static long BITMASK_SHOW_ALLOWPWSAVE = 0x00200; // allow to save password locally
	public final static long BITMASK_SHOW_CHANGEDOMAINPASSWORD = 0x00400; // show change domain password
	public final static long BITMASK_SHOW_REPLICANTS = 0x10000; // enable replication
	public final static long BITMASK_SHOW_THUMBS = 0x20000; // show thumbnails
	public final static long BITMASK_SHOW_HTTPPRINT = 0x40000; // enable HTTP printing
	public final static long BITMASK_SHOW_TESTPAGE = 0x80000; // allow test page printing
	public final static long BITMASK_SHOW_CONNECTED_PRINTERS = 0x0100000; // show Connected Printers on map
	public final static long BITMASK_SHOW_MS_EXCHANGE = 0x0200000; // enable MS Exchange
	public final static long BITMASK_SHOW_SECUREBROWSE = 0x0400000; // allow Intranet/Secure Browsing
	public final static long BITMASK_SHOW_OFFLINEPW = 0x0800000; // force offline password
	public final static long BITMASK_SHOW_SHARE = 0x1000000; // allow (private) sharing
	public final static long BITMASK_SHOW_PUBLICSHARE = 0x2000000; // allow public sharing
	public final static long BITMASK_SHOW_COMMENTS = 0x4000000; // allow public sharing

	public final static long BITMASK_DENY_SENDBYMAIL = 0x00001; // send as email not allowed
	public final static long BITMASK_DENY_DOWNLOAD = 0x00002; // download not allowed (remote files)
	public final static long BITMASK_DENY_MIMEDOWNLOAD = 0x00004; // mime-download not allowed (remote files)
																	// (relevant to BlackBerry Preview)
	public final static long BITMASK_DENY_UPLOAD = 0x00008; // upload not allowed (remote files)
	public final static long BITMASK_DENY_PREVIEW = 0x00010; // TPM preview not allowed
	public final static long BITMASK_DENY_PRINTING = 0x00020; // print not allowed
	public final static long BITMASK_DENY_WEBDAV = 0x00040; // WebDAV operations not allowed (client side WebDAV only)
	public final static long BITMASK_DENY_DELETE = 0x00080; // delete not allowed (remote files)
	public final static long BITMASK_DENY_EXPORT = 0x00100; // export of files not allowed (remote files)
	public final static long BITMASK_DENY_DISALLOWPWSAVE = 0x00200; // disallow to save password locally
	public final static long BITMASK_DENY_DIRECTPRINTING = 0x10000; // DirectPrint not allowed
	public final static long BITMASK_DENY_INTERNETBROWSING = 0x20000; // Internet browsing not allowed
	public final static long BITMASK_DENY_AUTO_UP_DOWNLOAD = 0x40000; // deny Auto Up-/Download

	public final static long BITMASK_NOTIFY_SELECTIVEWIPE = 0x00001; // selective wipe
	public final static long BITMASK_NOTIFY_LOCK = 0x00002; // lock client
	public final static long BITMASK_NOTIFY_ASKPW = 0x00004; // ??? lock Active Sync ???

	private static final String COL_SHOW_FLAGS = "show_flags";
	private static final String COL_DENY_FLAGS = "deny_flags";
	private static final String COL_SHOW_RESTRICTED_FLAGS = "show_restricted_flags";
	private static final String COL_PRINT_PARAMS = "print_params";

	protected static final String TABLE_PART = COL_SHOW_FLAGS + " text, " + COL_DENY_FLAGS + " text, "
			+ COL_SHOW_RESTRICTED_FLAGS + " text, "
			+ COL_PRINT_PARAMS + " text, ";

	@JsonProperty("SHOW")
	private long showFlags;

	@JsonProperty("DENY")
	private long denyFlags;

	@JsonProperty("SHOWRST")
	private long showRestrictedFlags;

	@JsonProperty("PPARAMS")
	private long printParamFlags;

	public UserRights()
	{
	}

	public UserRights(Cursor cursor)
	{
		showFlags = cursor.getInt(cursor.getColumnIndex(COL_SHOW_FLAGS));
		denyFlags = cursor.getInt(cursor.getColumnIndex(COL_DENY_FLAGS));
		showRestrictedFlags = cursor.getInt(cursor.getColumnIndex(COL_SHOW_RESTRICTED_FLAGS));
		printParamFlags = cursor.getInt(cursor.getColumnIndex(COL_PRINT_PARAMS));
	}

	public long getShowFlags()
	{
		return showFlags;
	}

	public void setShowFlags(long showFlags)
	{
		this.showFlags = showFlags;
	}

	public long getDenyFlags()
	{
		return denyFlags;
	}

	public void setDenyFlags(long denyFlags)
	{
		this.denyFlags = denyFlags;
	}

	public long getShowRestrictedFlags()
	{
		return showRestrictedFlags;
	}

	public void setShowRestrictedFlags(long showRestrictedFlags)
	{
		this.showRestrictedFlags = showRestrictedFlags;
	}

	public long getPrintParamFlags()
	{
		return printParamFlags;
	}

	public void setPrintParamFlags(long printParamFlags)
	{
		this.printParamFlags = printParamFlags;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_SHOW_FLAGS, showFlags);
		values.put(COL_DENY_FLAGS, denyFlags);
		values.put(COL_SHOW_RESTRICTED_FLAGS, showRestrictedFlags);
		values.put(COL_PRINT_PARAMS, printParamFlags);

		return values;
	}

	// ------------------- check SHOW flags -------------------

	public boolean isRemoteDirectoryEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_REMOTEDIR, checkRestrictedShowFlag);
	}

	public boolean isPrinterSearchEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_PRINTERSEARCH, checkRestrictedShowFlag);
	}

	public boolean isAdvancedPrinterSearchEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_PRINTERSEARCHEX, checkRestrictedShowFlag);
	}

	public boolean isPrintByDriverEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_PRINTBYDRIVER, checkRestrictedShowFlag);
	}

	public boolean isOneTimePasswordEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_ONETIMEPW, checkRestrictedShowFlag);
	}

	public boolean isPrintByWifiEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_WIFIPRINT, checkRestrictedShowFlag);
	}

	public boolean isTransmitLocationDataEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_GPS, checkRestrictedShowFlag);
	}

	public boolean isEnterpriseSearchEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_ENTERPRISESEARCH, checkRestrictedShowFlag);
	}

	public boolean isForcePDFDownloadEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_FORCEPDFDOWNLOAD, checkRestrictedShowFlag);
	}

	public boolean isSavePasswordLocallyEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_ALLOWPWSAVE, checkRestrictedShowFlag);
	}

	public boolean isChangeDomainPasswordEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_CHANGEDOMAINPASSWORD, checkRestrictedShowFlag);
	}

	public boolean isAutoUpDownloadEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_REPLICANTS, checkRestrictedShowFlag);
	}

	public boolean isShowThumbnailsEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_THUMBS, checkRestrictedShowFlag);
	}

	public boolean isPrintByHTTPEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_HTTPPRINT, checkRestrictedShowFlag);
	}

	public boolean isPrintTestPageEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_TESTPAGE, checkRestrictedShowFlag);
	}

	public boolean isConnectedPrintersEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_CONNECTED_PRINTERS, checkRestrictedShowFlag);
	}

	public boolean isMSExchnageEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_MS_EXCHANGE, checkRestrictedShowFlag);
	}

	public boolean isSecureBrowserEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_SECUREBROWSE, checkRestrictedShowFlag);
	}

	public boolean isOfflinePasswordEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_OFFLINEPW, checkRestrictedShowFlag);
	}

	public boolean isPrivateSharingEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_SHARE, checkRestrictedShowFlag);
	}

	public boolean isPublicSharingEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_PUBLICSHARE, checkRestrictedShowFlag);
	}

	public boolean isCommentsEnabled(boolean checkRestrictedShowFlag)
	{
		return checkShowFlag(BITMASK_SHOW_COMMENTS, checkRestrictedShowFlag);
	}

	// ------------------- check DENY flags -------------------

	public boolean isSendByMailAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_SENDBYMAIL);
	}

	public boolean isDownloadAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_DOWNLOAD);
	}

	public boolean isMIMEDownloadAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_MIMEDOWNLOAD);
	}

	public boolean isUploadAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_UPLOAD);
	}

	public boolean isPreviewAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_PREVIEW);
	}

	public boolean isPrintingAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_PRINTING);
	}

	public boolean isWebDAVAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_WEBDAV);
	}

	public boolean isDeleteRemoteFilesAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_DELETE);
	}

	public boolean isExportRemoteFilesAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_EXPORT);
	}

	public boolean isSavePasswordLocallyAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_DISALLOWPWSAVE);
	}

	public boolean isDirectPrintingAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_DIRECTPRINTING);
	}

	public boolean isBrowsingAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_INTERNETBROWSING);
	}

	public boolean isAutoUpDownloadAllowed()
	{
		return !checkDenyFlag(BITMASK_DENY_AUTO_UP_DOWNLOAD);
	}

	public int convertHexStringToInteger(String hexString)
	{
		String strNumber;

		int iPos = hexString.indexOf(HEX_PREFIX);
		if ((iPos >= 0) && (iPos + HEX_PREFIX.length() < hexString.length()))
		{
			strNumber = hexString.substring(iPos + HEX_PREFIX.length());
		}
		else
		{
			strNumber = hexString;
		}

		try
		{
			return Integer.parseInt(strNumber, 16);
		}
		catch (NumberFormatException exc)
		{
			exc.printStackTrace();
		}

		return 0;
	}

	/**
	 * Checks if the given SHOW bit is set or not
	 * 
	 * @return true if bit is set, false otherwise
	 */
	private boolean checkShowFlag(long checkBitmask, boolean checkRestrictedShowFlag)
	{
		long checkedFlags = showFlags;

		if (checkRestrictedShowFlag)
		{
			checkedFlags = showFlags | showRestrictedFlags;
		}

		return (checkedFlags & checkBitmask) != 0 ? true : false;
	}

	/**
	 * Checks if the given DENY bit is set or not
	 * 
	 * @return true if bit is set, false otherwise
	 */
	private boolean checkDenyFlag(long checkBitmask)
	{
		return (denyFlags & checkBitmask) == checkBitmask ? true : false;
	}
}