package net.teamplace.android.http.request.search;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class SearchRequester extends BasicRequester
{

	public SearchRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public void search(String teamDriveId, String searchStr, String baseFolder, int timeoutMillis,
			boolean forceNewSearch, int millisbetweenRequests,
			FileSearchResultListener listener) throws ConnectFailedException, XCBStatusException,
			HttpResponseException, LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException,
			TeamDriveNotFoundException, RedirectException {
		search(teamDriveId, searchStr, baseFolder, timeoutMillis, forceNewSearch, millisbetweenRequests, listener, null);
	}

	private void search(String teamDriveId, String searchStr, String baseFolder, int timeoutMillis,
			boolean forceNewSearch,
			int millisbetweenRequests, FileSearchResultListener listener, FileSearchResult previous)
			throws ConnectFailedException,
			XCBStatusException, HttpResponseException, LogonFailedException, NotificationException,
			ServerLicenseException, JsonParseException,
			JsonMappingException, IOException, TeamDriveNotFoundException, RedirectException {
		if (listener != null)
		{
			if (previous == null
					|| (listener.continueSearch(previous.getFindId()) && previous.getFindStatus().equals(
							FileSearchResult.STATUS_RUNNING)))
			{
				FileSearchResult result = doSearchRequest(teamDriveId, searchStr, baseFolder, timeoutMillis,
						((previous == null) ? forceNewSearch
								: false), (previous == null) ? null : previous.getFindId(),
						(previous != null) ? previous.getItems().size() : 0);

				listener.onNewResult(result, !result.getFindStatus().equals(FileSearchResult.STATUS_RUNNING));

				try
				{
					Thread.sleep(millisbetweenRequests);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
				}
				search(teamDriveId, searchStr, baseFolder, timeoutMillis,
						((previous == null) ? forceNewSearch : false), millisbetweenRequests,
						listener, result);
			}
			else
			{
				cancelSearch(teamDriveId, previous.getFindId());
			}
		}
	}

	public boolean cancelSearch(String teamDriveId, String searchId) throws ConnectFailedException, XCBStatusException,
			HttpResponseException,
			LogonFailedException, NotificationException, ServerLicenseException, JsonParseException,
			JsonMappingException, IOException,
			TeamDriveNotFoundException, RedirectException {
		HttpURLConnection conn = null;

		try
		{
			Uri uri = getCancelSearchUri(getServerForTeamDriveId(teamDriveId), searchId);

			conn = connectAndExecuteGETRequest(uri, null, true, true);

			return FileSearchResult.STATUS_CANCELLED.equals(conn.getHeaderField(HEADER_X_FINDSTATUS));
		}
		finally
		{
			if (conn != null)
			{
				disconnect(conn);
			}
		}
	}

	private FileSearchResult doSearchRequest(String teamDriveId, String searchStr, String baseFolder,
			int timeoutMillis, boolean forceOverwrite,
			String findId, int start) throws ConnectFailedException, XCBStatusException, HttpResponseException,
			LogonFailedException,
			NotificationException, ServerLicenseException, JsonParseException, JsonMappingException, IOException,
			TeamDriveNotFoundException, RedirectException {
		HttpURLConnection conn = null;

		try
		{
			if (searchStr == null && findId == null)
			{
				throw new IllegalArgumentException("Must at least specify find ID or search String!");
			}
			if (findId != null && forceOverwrite)
			{
				throw new IllegalArgumentException("Cannot overwrite results for continued search request!");
			}

			Uri uri = getSearchUri(getServerForTeamDriveId(teamDriveId), searchStr, baseFolder, timeoutMillis,
					forceOverwrite, findId, start);

			conn = connectAndExecuteGETRequest(uri, null, true, true);

			FileSearchResult fsr = new FileSearchResult();
			List<FileSearchResult.Item> result = JsonFileSearchResultParser
					.parseJsonToFileSearchResult(getStringFromResponse(conn));
			fsr.setItems(result);
			fsr.setFindId(conn.getHeaderField(HEADER_X_FINDID));
			fsr.setFindStatus(conn.getHeaderField(HEADER_X_FINDSTATUS));

			if (conn.getHeaderField(HEADER_X_FINDTIME) != null)
			{
				fsr.setFindTime(Long.parseLong(conn.getHeaderField(HEADER_X_FINDTIME)));
			}

			return fsr;
		}
		finally
		{
			if (conn != null)
			{
				disconnect(conn);
			}
		}
	}

	private Uri getSearchUri(String server, String searchStr, String baseFolder, int timeoutMillis,
			boolean forceOverwrite, String findId, int start)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon().appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		if (searchStr != null)
		{
			builder.appendQueryParameter(PARAM_FIND, searchStr);
		}
		if (baseFolder != null)
		{
			builder.appendQueryParameter(PARAM_FOLDER, baseFolder.replaceAll("/", "\\\\"));
		}
		if (timeoutMillis > 0)
		{
			builder.appendQueryParameter(PARAM_TIMEOUT, String.valueOf(timeoutMillis));
		}
		if (findId != null)
		{
			builder.appendQueryParameter(PARAM_FIND_ID, findId);
		}
		if (start > 0)
		{
			builder.appendQueryParameter(PARAM_START, String.valueOf(start));
		}
		builder.appendQueryParameter(PARAM_FORCE_OVERWRITE, forceOverwrite ? "1" : "0");

		return builder.build();
	}

	private Uri getCancelSearchUri(String server, String searchId)
	{
		Uri.Builder builder = Uri.parse(server).buildUpon().appendEncodedPath(GUI_BROWSE_FILE_SHELL);
		builder.appendQueryParameter(PARAM_FIND_ID, searchId);
		builder.appendQueryParameter(PARAM_CANCEL, "1");

		return builder.build();
	}
}
