package net.teamplace.android.http.request.faulttolerant.upload;

import java.io.File;

import net.teamplace.android.http.multipart.MultipartFile;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.faulttolerant.NetworkUpdateCallback;
import net.teamplace.android.http.request.faulttolerant.download.UpDownloadSQLiteHelper;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class FaultTolerantUploadManager
{
	private final String TAG = getClass().getSimpleName();

	private Context context;
	private UploadRequester requester;
	private MultipartFile multiPartFile;

	private NetworkUpdateCallback networkUpdateCallback;

	public FaultTolerantUploadManager(Context context, NetworkUpdateCallback networkUpdateCallback, BackendConfiguration backendConfiguration)
	{
		this.context = context.getApplicationContext();
		this.networkUpdateCallback = networkUpdateCallback;

		Session session = new SessionManager(this.context).getLastSession();
		requester = new UploadRequester(context, session, backendConfiguration, this.networkUpdateCallback);
	}

	public void uploadFile(final String teamDriveId, final String localPath, final String localFileName,
			final String remotePath,
			final String remoteFileName, final int forceOverwrite)
	{
		// Thread uploadThread = new Thread(new Runnable()
		// {
		// @Override
		// public void run()
		// {
		boolean error = false;

		try
		{
			String transactionId;
			long skipBytes = 0;

			File uploadFile = new File(localPath, localFileName);
			UploadStatus status = getUploadStatus(remotePath, remoteFileName, localPath, localFileName);

			if (status != null)
			//{
				// currently we don't support resume uploading
				deleteUploadStatus(status.getTransactionId());
			
				/*
				if (fileWasModified(uploadFile, status))
				{
					deleteUploadStatus(status.getTransactionId());
					transactionId = requester.getTransactionId(teamDriveId, remotePath, remoteFileName,
							forceOverwrite);
					status = new UploadStatus(transactionId, remotePath, remoteFileName, localPath,
							localFileName, uploadFile.lastModified());
				}
				else
				{
					skipBytes = requester.getAlreadyUploadedBytes(teamDriveId, status.getTransactionId());
					Log.d(TAG, "Already uploaded bytes: " + skipBytes);
				}
				
			}
			else*/
				
			{
				transactionId = requester.getTransactionId(teamDriveId, remotePath, remoteFileName,
						forceOverwrite);
				status = new UploadStatus(transactionId, remotePath, remoteFileName, localPath, localFileName,
						uploadFile.lastModified());
			}

			Log.d(TAG, status.getTransactionId());

			multiPartFile = new FaultTolerantUploadMultipartFile(uploadFile, status.getTransactionId(),
					skipBytes);
			multiPartFile.setCallback(networkUpdateCallback);

			requester.upload(teamDriveId, multiPartFile, status);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			error = true;
			networkUpdateCallback.onFailure(e);
		}

		if (!error)
		{
			networkUpdateCallback.onSuccess(null);
		}
		// }
		// });

		// uploadThread.start();
	}

	private UploadStatus getUploadStatus(String remotePath, String remoteFileName, String localPath,
			String localFileName)
	{
		UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
		SQLiteDatabase database = databaseHelper.getReadableDatabase();

		Cursor cursor = database.query(UploadStatus.TABLE_NAME, null, UploadStatus.COL_REMOTE_FILE_PATH + "= ? AND "
				+ UploadStatus.COL_REMOTE_FILE_NAME + "= ? AND " + UploadStatus.COL_LOCAL_FILE_PATH + "= ? AND "
				+ UploadStatus.COL_LOCAL_FILE_NAME
				+ "= ?", new String[] {remotePath, remoteFileName, localPath, localFileName}, null, null, null);

		try
		{
			if (cursor.moveToFirst())
			{
				return new UploadStatus(cursor);
			}

			return null;
		}
		finally
		{
			cursor.close();
			database.close();
		}
	}

	private boolean fileWasModified(File file, UploadStatus status)
	{
		Log.d(TAG,
				"File last modified: " + file.lastModified() + " | Last modified timestamp: "
						+ status.getLastModifyTimestamp());

		return (file.lastModified() > status.getLastModifyTimestamp()) ? true : false;
	}

	private void deleteUploadStatus(String transactionId)
	{
		UpDownloadSQLiteHelper databaseHelper = new UpDownloadSQLiteHelper(context);
		SQLiteDatabase database = databaseHelper.getReadableDatabase();

		int rows = database.delete(UploadStatus.TABLE_NAME, UploadStatus.COL_TRANSACTION_ID + "= ?",
				new String[] {transactionId});

		Log.d(TAG, rows + " deleted");

		database.close();
	}

	public void stopUpload()
	{
		Log.d(TAG, "Stop upload");

		if (multiPartFile != null)
		{
			multiPartFile.closeFileAndCancelActions();
		}
	}
}
