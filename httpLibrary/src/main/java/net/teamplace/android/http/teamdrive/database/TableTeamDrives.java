package net.teamplace.android.http.teamdrive.database;

import net.teamplace.android.http.database.util.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

public class TableTeamDrives
{

	public static final String TABLE = "table_teamdrives";

	public static final String COLUMN_ID = "teamdrive_id";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_NOTE = "note";
	public static final String COLUMN_IMGPATH = "imgpath";
	public static final String COLUMN_FILE_SERVICE_URL = "file_service_url";
	public static final String COLUMN_API_BASE_URL = "api_base_url";
	public static final String COLUMN_IS_QUOTA_UNLIMITED = "is_quota_unlimited";
	public static final String COLUMN_PAYMENT_PAIDUNTIL = "payment_paiduntil";
	public static final String COLUMN_PAYMENT_DEMO_PERIOD_UNTIL = "payment_demo_period_until";
	public static final String COLUMN_PAYMENT_ISFREE = "payment_is_free";
	public static final String COLUMN_ROLE = "role";
	public static final String COLUMN_RIGHTS = "rights";
	public static final String COLUMN_POTENTIAL_RIGHTS = "potential_rights";
	public static final String COLUMN_CCSRIGHTS = "ccsrights";

	private static final String[] ALL_COLUMNS = new String[] {COLUMN_ID, COLUMN_NAME, COLUMN_NOTE, COLUMN_IMGPATH,
			COLUMN_FILE_SERVICE_URL, COLUMN_API_BASE_URL, COLUMN_IS_QUOTA_UNLIMITED, COLUMN_PAYMENT_PAIDUNTIL, COLUMN_PAYMENT_DEMO_PERIOD_UNTIL,
			COLUMN_PAYMENT_ISFREE, COLUMN_ROLE, COLUMN_RIGHTS, COLUMN_POTENTIAL_RIGHTS, COLUMN_CCSRIGHTS};
	private static final String[] CREATION_INFO = new String[] {"TEXT PRIMARY KEY", "TEXT", "TEXT", "TEXT", "TEXT", "TEXT",
			"INTEGER DEFAULT 0", "INTEGER", "INTEGER", "INTEGER DEFAULT 0", "INTEGER", "INTEGER", "INTEGER", "INTEGER"};

	public static final String CREATE_TABLE;

	static
	{
		CREATE_TABLE = "CREATE TABLE IF NOT EXISTS "
				+ TABLE
				+ " ("
				+ DatabaseUtils.buildCreationColumnList(ALL_COLUMNS, CREATION_INFO)
				+ ");";
	}

	public static void create(SQLiteDatabase db)
	{
		db.execSQL(CREATE_TABLE);
	}

	public static void upgrade(SQLiteDatabase db, int oldVer, int curVer)
	{
		switch (oldVer)
		{
			case 1:
				DatabaseUtils.modifyColumnSet(db, TABLE, ALL_COLUMNS, CREATION_INFO,
						new String[]{COLUMN_PAYMENT_ISFREE});
				db.execSQL("UPDATE " + TableDriveEtags.TABLE + " SET " + TableDriveEtags.COLUMN_ETAG + " = NULL;");
				break;
			case 2:
				// TeamDrive API change to v2, cached v1 TeamDrive data might be incompatible
				db.execSQL("DROP TABLE " + TableTeamDrives.TABLE + ";");
				create(db);
				break;
			default:
				break;
		}
	}

}
