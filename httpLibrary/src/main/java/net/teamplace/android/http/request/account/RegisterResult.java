package net.teamplace.android.http.request.account;

import android.os.Parcel;

public class RegisterResult extends ResponseResult
{
	public static final int KNOWN_USER_REGISTERED = 0;
	public static final int SUCCESS = 1;
	public static final int DUPLICATE_USER = 2;
	public static final int MAIL_ERROR = 8;
	public static final int ERROR = 9;

	public RegisterResult(String message, int resultCode, boolean success)
	{
		super(message, resultCode, success);
	}

	public RegisterResult(Parcel in)
	{
		super(in);
	}
}
