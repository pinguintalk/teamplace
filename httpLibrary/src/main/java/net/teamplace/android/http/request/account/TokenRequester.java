package net.teamplace.android.http.request.account;

import java.io.IOException;
import java.net.HttpURLConnection;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.request.ConnectionErrorHandler;
import net.teamplace.android.http.request.TokenManager;
import net.teamplace.android.http.session.Session;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class TokenRequester extends BasicRequester
{
	private final String TAG = getClass().getSimpleName();

	public TokenRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public AuthToken requestToken(String user, String password, String tokenType) throws ConnectFailedException,
			HttpResponseException,
			LogonFailedException, XCBStatusException, NotificationException, ServerLicenseException, IOException, RedirectException {
		HttpURLConnection connection = null;
		try
		{
			Uri uri = getTokenUri();

			Log.d(TAG, "Uri: " + uri);

			connection = openConnection(uri, HTTP_GET);

			setBasicAuthParams(connection, user, password);
			setHeader(connection, null);
			setTimeout(connection, TIMEOUT_CONF);
			showRequestHeader(connection);

			ConnectionErrorHandler.handleConnectionStatus(connection);

			showResponseHeader(connection);

			String token = TokenManager.getTokenFromResponse(connection);

			return new AuthToken(token, tokenType);
		}
		finally
		{
			disconnect(connection);
		}
	}

	public Uri getTokenUri()
	{
		Uri.Builder builder = Uri.parse(backendConfiguration.workplaceServer()).buildUpon();
		builder.appendEncodedPath(HELLO_2);

		return builder.build();
	}

}
