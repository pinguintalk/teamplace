package net.teamplace.android.http.exception;

public class NotReadyException extends ConnectionLocallyForbiddenException
{

	/**
     * 
     */
	private static final long serialVersionUID = 4737017675883860396L;

	public NotReadyException()
	{
		super("Not ready");
	}

	public NotReadyException(String strErr)
	{
		super(strErr);
	}

}
