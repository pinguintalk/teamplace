package net.teamplace.android.http.exception;

public class NonEmptyResponseException extends InvalidResponseException
{
	private static final long serialVersionUID = 7724083739214645655L;

	public NonEmptyResponseException()
	{

	}

	public NonEmptyResponseException(String strErr)
	{
		super(strErr);
	}
}
