package net.teamplace.android.http.teamdrive.activity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import net.teamplace.android.http.annotation.JacksonDataModel;

/**
 * Created by jobol on 16/06/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class FileInfo {

    @JsonProperty("Id")
    public String id;

    @JsonProperty("Name")
    public String name;

    @JsonProperty("Path")
    public String path;
}
