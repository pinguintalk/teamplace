package net.teamplace.android.http.request;

/**
 * Created by jobol on 02/06/15.
 *
 * Makes backend URLs available to modules.
 */
public interface BackendConfiguration {
    public String accountManagement();
    public String workplaceServer();
    public String teamDriveApi();
    public String activityApi();
    public String quotaApi();
    public String registration();
    public String forgotPassword();
    public String userProfile();
}
