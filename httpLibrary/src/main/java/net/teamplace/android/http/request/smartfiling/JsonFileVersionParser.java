package net.teamplace.android.http.request.smartfiling;

import java.util.ArrayList;
import java.util.Collections;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonFileVersionParser
{

	private static final String SIZE = "size";
	private static final String FULLNAME = "fullname";
	private static final String CREATOR = "creator";
	private static final String VERSION = "version";
	private static final String DATE = "date";
	private static final String NAME = "name";

	public static ArrayList<FileVersion> parseVersionJson(String jsonIn, String parentFile, String parentFolder)
	{
		ArrayList<FileVersion> versions = new ArrayList<FileVersion>();
		try
		{
			JSONArray jsonVersions = new JSONArray(jsonIn);
			for (int i = 0; i < jsonVersions.length(); i++)
			{
				JSONObject jsonVersion = (JSONObject) jsonVersions.get(i);
				long size = jsonVersion.getLong(SIZE);
				String fullname = jsonVersion.getString(FULLNAME);
				String creator = jsonVersion.getString(CREATOR);
				int version = jsonVersion.getInt(VERSION);
				long date = jsonVersion.getLong(DATE);
				String name = jsonVersion.getString(NAME);
				FileVersion fileVersion = new FileVersion(parentFile, parentFolder, size, fullname, creator, version,
						date, name);
				versions.add(fileVersion);
			}

		}
		catch (JSONException e)
		{
			System.out.println(e);
		}
		Collections.sort(versions);
		return versions;
	}

}
