package net.teamplace.android.http.exception;

public class TeamDriveNotFoundException extends CortadoException
{
	private static final long serialVersionUID = -793995702190473358L;

	public TeamDriveNotFoundException(Exception e)
	{
		super(e);
	}

	public TeamDriveNotFoundException(String strError)
	{
		super(strError);
	}
}
