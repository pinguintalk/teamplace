package net.teamplace.android.http.request.files;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.Command;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder(alphabetic = true)
@JacksonDataModel
public class CreateCommand extends Command
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty(JSON_PARAMS)
	private CreateParameter createParameter;

	public CreateCommand(String team, String parentFolder, String createFileOrFolder, int forceOverwrite)
	{
		setCommand(CMD_CREATE);

		this.createParameter = new CreateParameter(team, parentFolder, createFileOrFolder, forceOverwrite);
	}
}
