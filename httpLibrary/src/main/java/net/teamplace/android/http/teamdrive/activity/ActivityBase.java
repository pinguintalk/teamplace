package net.teamplace.android.http.teamdrive.activity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import net.teamplace.android.http.annotation.JacksonDataModel;

/**
 * Created by jobol on 16/06/15.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "TypeId")
@JsonSubTypes( {
        @JsonSubTypes.Type(value = ActivityBase.FileUploaded.class, name = "1"),
        @JsonSubTypes.Type(value = ActivityBase.CommentCreated.class, name = "2"),
        @JsonSubTypes.Type(value = ActivityBase.FileVersionCreated.class, name = "3"),
        @JsonSubTypes.Type(value = ActivityBase.TeamDriveCreated.class, name = "4"),
        @JsonSubTypes.Type(value = ActivityBase.UserJoined.class, name = "5")
})
@JacksonDataModel
public abstract class ActivityBase {

    @JsonProperty("Id")
    public String id;

    @JsonProperty("ParentActivityId")
    public String parentActivityId;

    @JsonProperty("UserId")
    public String userId;

    @JsonProperty("TeamdriveId")
    public String teamDriveId;

    @JsonProperty("RelatedFile")
    public FileInfo relatedFile;

    @JsonProperty("TimeStamp")
    public long timeStamp;

    @JsonProperty("SubActivityCount")
    public int subActivityCount;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class CommentCreated extends ActivityBase {

        @JsonProperty("CommentId")
        public String commentId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class FileVersionCreated extends ActivityBase {

        @JsonProperty("FileVersion")
        public int fileVersion;

        @JsonProperty("FileVersionId")
        public String fileVersionId;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class FileUploaded extends ActivityBase {

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class UserJoined extends ActivityBase {

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    @JacksonDataModel
    public static class TeamDriveCreated extends ActivityBase {

    }

}
