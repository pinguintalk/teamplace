package net.teamplace.android.http.request.teamdrive;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.teamdrive.DriveCreation;
import net.teamplace.android.http.teamdrive.GlobalConfiguration;
import net.teamplace.android.http.teamdrive.IncomingInvitation;
import net.teamplace.android.http.teamdrive.TeamDrive;
import net.teamplace.android.http.teamdrive.User;
import net.teamplace.android.http.teamdrive.UserListInvitation;
import net.teamplace.android.http.util.HeaderInputStream;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.net.Uri;
import android.util.Pair;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public class TeamdriveRequester extends BasicRequester
{


	public TeamdriveRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	/**
	 * Gets a specific TeamDrive
	 * 
	 * @param token
	 * @param driveId
	 * @param etag
	 * @param requestFilters
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 */
	public Pair<TeamDrive, String> getTeamDrive(String driveId, String etag, String... requestFilters)
			throws JsonParseException,
			JsonMappingException, IOException, XCBStatusException, HttpResponseException, ConnectFailedException,
			LogonFailedException,
			NotificationException, ServerLicenseException, RedirectException {
		Pair<String, String> response = getResponseStringPair(getTeamDriveUri(driveId, requestFilters), etag);
		return new Pair<TeamDrive, String>(TeamDrive.createFromJson(response.first), response.second);
	}

	/**
	 * Gets all TeamDrives
	 * 
	 * @param etag
	 * @param requestFilters
	 * @param token
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws MalformedURLException
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws IOException
	 */
	public Pair<TeamDrive[], String> getTeamDrives(String etag, String... requestFilters) throws JsonParseException,
			JsonMappingException,
			XCBStatusException, HttpResponseException, MalformedURLException, ConnectFailedException,
			LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		Pair<String, String> response = getResponseStringPair(getTeamDriveUri(null, requestFilters), etag);
		return new Pair<TeamDrive[], String>(TeamDrive.createArrayFromJson(response.first), response.second);
	}

	/**
	 * Creates a TeamDrive.
	 * 
	 * @param token
	 * @param name
	 * @param note
	 * @param imgData
	 * @param users
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws MalformedURLException
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws UnsupportedEncodingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public TeamDrive createTeamDrive(DriveCreation toCreate) throws JsonParseException, JsonMappingException,
			XCBStatusException, HttpResponseException,
			MalformedURLException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException,
			UnsupportedEncodingException, JsonProcessingException, IOException, RedirectException {
		return TeamDrive.createFromJson(doJsonUploadPOST(getCreateUri(toCreate.getInvitations() != null),
				DriveCreation.createJsonObject(toCreate).getBytes(ENCODING_UTF8), true, true));
	}

	/**
	 * Add user(s) to a TeamDrive
	 * 
	 * @param token
	 * @param driveId
	 * @param users
	 * @throws XCBStatusException
	 * @throws HttpResponseException
	 * @throws ConnectFailedException
	 * @throws LogonFailedException
	 * @throws NotificationException
	 * @throws ServerLicenseException
	 * @throws IOException
	 */
	public void addUsersToTeamDrive(String driveId, User... users) throws XCBStatusException, HttpResponseException,
			ConnectFailedException,
			LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		doJsonUploadPOST(getUserUri(driveId), User.createJsonArray(users).getBytes(ENCODING_UTF8), true, true); // make
																												// sure
																												// we
																												// always
																												// use
																												// an
																												// array,
																												// since
																												// the
																												// server
																												// doesn't
																												// accept
																												// single
																												// objects
	}

	public void inviteUsersToTeamDrive(String driveId, UserListInvitation invitations) throws XCBStatusException,
			HttpResponseException,
			JsonParseException, JsonMappingException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException,
			UnsupportedEncodingException, JsonProcessingException, IOException, RedirectException {
		doJsonUploadPOST(getInvitationUriBuilder().appendEncodedPath(driveId).build(), UserListInvitation
				.createJsonObject(invitations).getBytes(ENCODING_UTF8), true, true);
	}

	public void acceptInvitation(String invitationId) throws XCBStatusException, HttpResponseException,
			JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		doJsonUploadPOST(
				getInvitationUriBuilder().appendEncodedPath(invitationId).appendEncodedPath(PATH_TEAMDRIVE_ACCEPT)
						.build(), new byte[] {},
				true, true);
	}

	public void rejectInvitation(String invitationId) throws XCBStatusException, HttpResponseException,
			JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		doJsonUploadPOST(
				getInvitationUriBuilder().appendEncodedPath(invitationId).appendEncodedPath(PATH_TEAMDRIVE_REJECT)
						.build(), new byte[] {},
				true, true);
	}

	public Pair<IncomingInvitation[], String> getInvitations(String etag) throws XCBStatusException,
			HttpResponseException, JsonParseException,
			JsonMappingException, ConnectFailedException, LogonFailedException, NotificationException,
			ServerLicenseException, IOException, RedirectException {
		Pair<String, String> response = getResponseStringPair(getInvitationUriBuilder().build(), etag);
		return new Pair<IncomingInvitation[], String>(IncomingInvitation.createArrayFromJson(response.first),
				response.second);
	}

	public Pair<GlobalConfiguration, String> getGlobalConfiguration(String etag) throws JsonParseException,
			JsonMappingException, IOException,
			XCBStatusException, HttpResponseException, ConnectFailedException, LogonFailedException,
			NotificationException, ServerLicenseException, RedirectException {
		Pair<String, String> response = getResponseStringPair(
				getBasicTeamDriveUriBuilder().appendEncodedPath(PATH_TEAMDRIVE_CONFIGURATION).build(),
				etag);
		return new Pair<GlobalConfiguration, String>(GlobalConfiguration.createFromJson(response.first),
				response.second);
	}

	public Pair<Boolean, String> getUserImage(User user, File out, Integer imgSize, String etag)
			throws XCBStatusException, HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		InputStream is = null;
		OutputStream os = null;
		try
		{
			os = new FileOutputStream(out);
			Pair<InputStream, String> isPair = getUserImageInputStream(user, imgSize, etag);
			is = isPair.first;
			writeToOutputStream(is, os, null);
			return new Pair<>(Boolean.valueOf(out.exists()), isPair.second);
		}
		finally
		{
			closeStream(os);
			closeStream(is);
		}
	}

	public Pair<InputStream, String> getUserImageInputStream(User user, Integer imgSize, String etag) throws HttpResponseException,
			XCBStatusException, IOException, ConnectFailedException, ServerLicenseException, LogonFailedException, RedirectException,
			NotificationException {
		Uri uri = Uri.parse(user.getImgPath());
		if (imgSize != null)
		{
			uri = uri.buildUpon().appendQueryParameter(PARAM_IMAGE_SIZE, String.valueOf(imgSize)).build();
		}
		List<NameValuePair> headers = commonHeaders;
		if (etag != null)
		{
			headers = new ArrayList<NameValuePair>(commonHeaders);
			headers.add(new BasicNameValuePair(HEADER_IF_NONE_MATCH, etag));
		}
		HeaderInputStream is = executeInputStreamGetRequest(uri, headers, true, true);
		List<String> etagList = is.getResponseHeaders().get(HEADER_ETAG);
		String responseEtag = etagList != null && etagList.size() > 0 ? etagList.get(0) : null;
		return new Pair<InputStream, String>(is, responseEtag);
	}

	private Pair<String, String> getResponseStringPair(Uri uri, String etag) throws XCBStatusException,
			HttpResponseException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException,
			JsonParseException, JsonMappingException,
			IOException, RedirectException {
		List<NameValuePair> requestProperties = null;
		if (etag != null)
		{
			requestProperties = new ArrayList<NameValuePair>();
			requestProperties.add(new BasicNameValuePair(HEADER_IF_NONE_MATCH, etag));
		}
		return executeStringGetRequestEvaluateEtag(uri, requestProperties, true, true);
	}

	private Uri getUserUri(String driveId)
	{
		return getBasicTeamDriveUriBuilder().appendEncodedPath(driveId).appendEncodedPath(PATH_TEAMDRIVE_USER).build();
	}

	private Uri.Builder getInvitationUriBuilder()
	{
		return getBasicTeamDriveUriBuilder().appendEncodedPath(PATH_TEAMDRIVE_INVITATION);
	}

	private Uri getTeamDriveUri(String driveId, String... requestFilters)
	{
		Uri.Builder builder = getBasicTeamDriveUriBuilder();
		if (driveId != null)
		{
			builder.appendEncodedPath(driveId);
		}
		if (requestFilters != null && requestFilters.length > 0)
		{
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < requestFilters.length; i++)
			{
				sb.append(requestFilters[i]).append(i < (requestFilters.length - 1) ? "," : "");
			}
			builder.appendQueryParameter(PARAM_TEAMDRIVE_FIELDS_FILTER, sb.toString());
		}
		return builder.build();
	}

	private Uri getCreateUri(boolean includeInvitations)
	{
		return getBasicTeamDriveUriBuilder().appendEncodedPath(
				includeInvitations ? PATH_TEAMDRIVE_CREATE_INVITE : PATH_TEAMDRIVE_CREATE).build();
	}

	private Uri.Builder getBasicTeamDriveUriBuilder()
	{
		return Uri.parse(backendConfiguration.teamDriveApi()).buildUpon();
	}

	public static class RequestFilters
	{
		public static final String ID = "id";
		public static final String NAME = "name";
		public static final String NOTE = "note";
		public static final String IMGPATH = "imgpath";
		public static final String IMGDATA = "imgdata";
		public static final String FILESERVICEURL = "fileserviceurl";
		public static final String APIBASEURL = "apibaseurl";
		public static final String ISQUOTAUNLIMITED = "isquotaunlimited";
		public static final String PAYMENT = "payment";
		public static final String USERS = "users";
		public static final String INVITED_USERS = "invitedusers";
		public static final String ROLE = "role";
		public static final String RIGHTS = "rights";
		public static final String POTENTIAL_RIGHTS = "potentialrights";
		public static final String CCSRIGHTS = "ccsrights";
		public static final String[] ALL_BUT_IMGDATA = new String[] {ID, NAME, NOTE, IMGPATH, FILESERVICEURL,
				APIBASEURL, ISQUOTAUNLIMITED, PAYMENT, USERS,
				INVITED_USERS, ROLE, RIGHTS,
				POTENTIAL_RIGHTS, CCSRIGHTS};

		private RequestFilters()
		{
		}
	}

}
