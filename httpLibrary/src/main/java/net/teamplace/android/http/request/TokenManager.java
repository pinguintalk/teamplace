package net.teamplace.android.http.request;

import java.io.IOException;
import java.net.HttpURLConnection;

import net.teamplace.android.http.request.account.AuthToken;
import net.teamplace.android.http.session.Session;
import net.teamplace.android.http.session.SessionManager;
import net.teamplace.android.http.util.DebugHelper;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorException;
import android.accounts.OperationCanceledException;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;


/***
 * Handles the token management during the application runtime. It saves token strings from response headers and
 * requests new tokens from {@link AccountManager}, if the token stored in session is
 * invalid.
 * 
 * @author jamic
 */
public class TokenManager implements ServerParams
{
	private final String OPTION_APP_VERSION_CODE = "app version code";
	private final String OPTION_MINIMUM_AUTH_LIBRARY_VERSION = "minimum auth library version";
	private final String TOKENTYPE_CCS = "ccs";
	private final String ACCOUNT_TYPE_WORKPLACE = "com.cortado.account";

	private static final String TAG = TokenManager.class.getSimpleName();
	private static TokenManager sInstance;
	private volatile boolean mUpdating = false;

	public static TokenManager getInstance()
	{
		if (sInstance == null)
		{
			sInstance = new TokenManager();
		}
		return sInstance;
	}

	/**
	 * Tries to read the "X-Token" header field from HTTP response and saves it in current session
	 */
	synchronized public void updateTokenFromResponse(Context context, Session session, HttpURLConnection connection)
	{
		boolean isDebugVersion = DebugHelper.isDebugVersion(context);
		log("Check response for new token", isDebugVersion);

		String newToken = getTokenFromResponse(connection);
		if (newToken == null || newToken.isEmpty())
		{
			log("No new token found", isDebugVersion);
			return;
		}

		log("New token found: " + newToken, isDebugVersion);
		AuthToken token = session.getToken();
		if (token != null)
		{
			token.setTokenStr(newToken);
		}
		else
		{
			session.setToken(new AuthToken(newToken, TOKENTYPE_CCS));
		}

		SessionManager sessionManager = new SessionManager(context);
		sessionManager.saveSession(session);
	}

	public static String getTokenFromResponse(HttpURLConnection connection)
	{
		return connection.getHeaderField(HEADER_X_TOKEN);
	}

	/***
	 * Requests a new token from {@link AccountManager} and saves it in current session. During an token update the
	 * method {@link #isUpdating()} returns true. In case that two or more threads try to
	 * update the token with this method, you should always call {@link #isUpdating()} before, so you can set skipUpdate
	 * in {@link #updateTokenFromAccountManager(Context, Session, boolean)} to true.
	 * 
	 * @param context
	 * @param session
	 * @param skipUpdate
	 * @return true if token update was successful, false otherwise
	 */
	synchronized public boolean updateTokenFromAccountManager(Context context, Session session, boolean skipUpdate)
	{
		boolean isDebugVersion = DebugHelper.isDebugVersion(context);
		if (skipUpdate)
		{
			log("Skip token update", isDebugVersion);
			return true;
		}

		log("Start token update", isDebugVersion);

		mUpdating = true;

		AccountManager manager = AccountManager.get(context);

		invalidateCachedAuthToken(manager, session.getAccount());

		Bundle authBundle = buildConfigBundle(1, 1);
		AccountManagerFuture<Bundle> future = manager.getAuthToken(session.getAccount(), TOKENTYPE_CCS, authBundle,
				true, null, null);

		try
		{
			Bundle tokenBundle = future.getResult();
			if (tokenBundle.containsKey(AccountManager.KEY_INTENT))
			{
				return false;
			}

			String newToken = tokenBundle.getString(AccountManager.KEY_AUTHTOKEN);

			log("New token: " + newToken, isDebugVersion);

			AuthToken token = session.getToken();
			if (token != null)
			{
				token.setTokenStr(newToken);
			}
			else
			{
				session.setToken(new AuthToken(newToken, TOKENTYPE_CCS));
			}
			SessionManager sessionManager = new SessionManager(context);
			sessionManager.saveSession(session);

			return true;
		}
		catch (OperationCanceledException e)
		{
			e.printStackTrace();
		}
		catch (AuthenticatorException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		finally
		{
			mUpdating = false;
		}

		return false;
	}

	/**
	 * Returns true if token update is in progress, false otherwise.
	 */
	public boolean isUpdating()
	{
		return mUpdating;
	}

	private Bundle buildConfigBundle(int versionCode, int minAuthLibVersion)
	{
		Bundle bundle = new Bundle();
		bundle.putInt(OPTION_APP_VERSION_CODE, versionCode);
		bundle.putInt(OPTION_MINIMUM_AUTH_LIBRARY_VERSION, minAuthLibVersion);
		return bundle;
	}

	private void invalidateCachedAuthToken(AccountManager manager, Account account)
	{
		String cachedAuthToken = manager.peekAuthToken(account, TOKENTYPE_CCS);
		manager.invalidateAuthToken(ACCOUNT_TYPE_WORKPLACE, cachedAuthToken);
	}

	private void log(String message, boolean isDebugVersion)
	{
		if (isDebugVersion)
		{
			Log.d(TAG, message);
		}
	}
}
