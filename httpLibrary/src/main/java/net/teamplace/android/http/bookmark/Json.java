package net.teamplace.android.http.bookmark;

import net.teamplace.android.http.annotation.JacksonDataModel;
import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents json objects for bookmark requests. Each static inner class is one request and each request contains
 * inner classes related to its json objects.
 */
public class Json
{
	// One class per request. The 'inner-inner' classes represent json objects
	public static class AddBookmark
	{
		@JacksonDataModel
		@JsonInclude(Include.NON_NULL)
		public static class Bookmark
		{
			final private String mName;
			final private String mTeam;
			final private Integer mSortingPosition;

			@JsonCreator
			public Bookmark(@JsonProperty("name") String name, /**/
					@JsonProperty("team") String team, /**/
					@JsonProperty("sortingPos") Integer sortingPosition)
			{
				mName = name;
				mTeam = team;
				mSortingPosition = sortingPosition;
			}

			public String getName()
			{
				return mName;
			}

			public String getTeam()
			{
				return mTeam;
			}

			public Integer getSortingPos()
			{
				return mSortingPosition;
			}
		}
	}

	public static class DeleteBookmark
	{
		@JacksonDataModel
		@JsonInclude(Include.NON_NULL)
		public static class Bookmark
		{
			final private String mName;
			final private String mTeam;

			@JsonCreator
			public Bookmark(@JsonProperty("name") String name, /**/
					@JsonProperty("team") String team)
			{
				mName = name;
				mTeam = team;
			}

			public String getName()
			{
				return mName;
			}

			public String getTeam()
			{
				return mTeam;
			}
		}
	}

	public static class EnumerateBookmarks
	{
		@JacksonDataModel
		@JsonInclude(Include.NON_NULL)
		@JsonIgnoreProperties(ignoreUnknown = true)
		public static class Bookmark implements Parcelable
		{
			final private String mName;
			final private String mTeam;
			final private int mSortingPosition;
			final private long mAddedDate;
			final private int mFlags;
			final private long mDate;
			final private long mSize;
			final private Integer mType;

			@JsonCreator
			public Bookmark(@JsonProperty("name") String name, /**/
					@JsonProperty("team") String team, /**/
					@JsonProperty("sortingpos") int sortingPosition, /**/
					@JsonProperty("added") long addedDate, /**/
					@JsonProperty("flags") int flags, /**/
					@JsonProperty("date") long date, /**/
					@JsonProperty("size") long size, /**/
					@JsonProperty("type") Integer type)
			{
				mName = name;
				mTeam = team;
				mSortingPosition = sortingPosition;
				mAddedDate = addedDate;
				mFlags = flags;
				mDate = date;
				mSize = size;
				mType = type;
			}

			public String getName()
			{
				return mName;
			}

			public String getTeam()
			{
				return mTeam;
			}

			public int getSortingPosition()
			{
				return mSortingPosition;
			}

			public long getAddedDate()
			{
				return mAddedDate;
			}

			public int getFlags()
			{
				return mFlags;
			}

			public long getDate()
			{
				return mDate;
			}

			public long getSize()
			{
				return mSize;
			}

			public Integer getType()
			{
				return mType;
			}

			@Override
			public int describeContents()
			{
				return 0;
			}

			@Override
			public void writeToParcel(Parcel dest, int flags)
			{
				dest.writeString(mName);
				dest.writeString(mTeam);
				dest.writeInt(mSortingPosition);
				dest.writeLong(mAddedDate);
				dest.writeInt(mFlags);
				dest.writeLong(mDate);
				dest.writeLong(mSize);
				dest.writeInt(mType == null ? -1 : mType);
			}

			public static final Parcelable.Creator<Bookmark> CREATOR = new Parcelable.Creator<Bookmark>()
			{
				@Override
				public Bookmark createFromParcel(Parcel source)
				{
					String name = source.readString();
					String team = source.readString();
					int sortingPosition = source.readInt();
					long addedDate = source.readLong();
					int flags = source.readInt();
					long date = source.readLong();
					long size = source.readLong();
					Integer type = source.readInt();
					type = type == -1 ? null : type;
					return new Bookmark(name, team, sortingPosition, addedDate, flags, date, size, type);
				}

				@Override
				public Bookmark[] newArray(int size)
				{
					return new Bookmark[size];
				}
			};
		}
	}
}
