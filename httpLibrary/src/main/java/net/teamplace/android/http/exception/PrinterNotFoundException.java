package net.teamplace.android.http.exception;

public class PrinterNotFoundException extends FileNotFoundException
{

	/**
     * 
     */
	private static final long serialVersionUID = -2262769888954726581L;

	public PrinterNotFoundException(String strPrinter)
	{
		super(strPrinter + " not found");
	}
}
