package net.teamplace.android.http.multipart;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MultipartFileDefaultUpload extends MultipartFile
{
	private static final String regExReplace = "\\[|\\]|\\\\|\\*|\\?|\\<|\\>|\\/|\\||\"|\\:";

	private final Date fileUploadDate;
	private final String mFilePath;

	public MultipartFileDefaultUpload(InputStream pFileInputStream, String pFileName, String filePath, long pFileSize,
			Date date, long skipBytes)
			throws IOException
	{
		super(pFileInputStream, pFileName, pFileSize, skipBytes);
		fileUploadDate = date;
		mFilePath = filePath;
	}

	@Override
	public String getFileNameForUpload()
	{
		return getFilePrefix() + getNameWithReplacedChars();
	}

	private String getNameWithReplacedChars()
	{
		if (fileName == null || fileName.length() < 1)
		{
			return "";
		}
		return fileName.replaceAll(regExReplace, "_");
	}

	private String getFilePrefix()
	{
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.setTimeZone(Calendar.getInstance().getTimeZone());
		// sdf.applyPattern("dd.MMM.yyyy-HH.mm.ss.SSS_");
		sdf.applyPattern("yyyyMMdd-HH.mm.ss-");
		return sdf.format(fileUploadDate);
	}

	@Override
	protected void buildPreContent(StringBuilder strBuilder)
	{
		addContentDispositionPart(PARAM_GUI, "1", strBuilder);
		addAppendMode(strBuilder);
		addContentDispositionPart(PARAM_DESTFILE, getFileNameForUpload(), strBuilder);
		addContentDispositionPart(PARAM_DESTPATH, getFilePathForUpload(), strBuilder);
	}

	private String getFilePathForUpload()
	{
		return mFilePath.replaceAll("/", "\\\\");
	}
}
