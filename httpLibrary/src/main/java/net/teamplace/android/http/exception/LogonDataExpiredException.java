package net.teamplace.android.http.exception;

public class LogonDataExpiredException extends IllegalLogonDataException
{

	public LogonDataExpiredException(String strAccount)
	{
		super(strAccount + " expired");
	}

	/**
     * 
     */
	private static final long serialVersionUID = -7696059226570153374L;

}
