package net.teamplace.android.http.exception;

public class MalformedURLException extends CortadoException
{
	public MalformedURLException(String strError)
	{
		super(strError);
	}

	/**
     * 
     */
	private static final long serialVersionUID = 1446353057025175011L;

}
