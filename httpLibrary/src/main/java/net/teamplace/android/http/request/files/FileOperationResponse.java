package net.teamplace.android.http.request.files;

import java.io.IOException;
import java.io.InputStream;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.json.JsonUtils;

import org.apache.commons.io.IOUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@JacksonDataModel
public class FileOperationResponse
{
	private final String TAG = getClass().getSimpleName();

	@JsonProperty("results")
	private FileOperationResult[] results;

	public FileOperationResponse()
	{
	}

	public FileOperationResult[] getResults()
	{
		return results;
	}

	public static FileOperationResponse createFileOperationResponseFromJSON(String jsonString)
			throws JsonParseException, JsonMappingException,
			IOException
	{
		return createFileOperationResponseFromJSON(jsonString.getBytes());
	}

	public static FileOperationResponse createFileOperationResponseFromJSON(InputStream jsonIs)
			throws JsonParseException, JsonMappingException,
			IOException
	{
		return createFileOperationResponseFromJSON(IOUtils.toByteArray(jsonIs));
	}

	public static FileOperationResponse createFileOperationResponseFromJSON(byte[] jsonBytes)
			throws JsonParseException, JsonMappingException,
			IOException
	{
		ObjectMapper mapper = JsonUtils.getDefaultMapper();
		FileOperationResponse response = mapper.readValue(jsonBytes, FileOperationResponse.class);

		return response;
	}
}
