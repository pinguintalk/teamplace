package net.teamplace.android.http.request.account;

import java.io.UnsupportedEncodingException;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonPasswordResetRequestBuilder
{

	private static final String KEY_EMAIL = "Email";

	public static byte[] createPasswordResetRequest(String email)
	{
		JSONObject register = new JSONObject();
		try
		{
			register.put(KEY_EMAIL, email);

		}
		catch (JSONException e)
		{
		}
		try
		{
			return register.toString().getBytes("utf-8");
		}
		catch (UnsupportedEncodingException e)
		{
			return register.toString().getBytes();
		}
	}
}
