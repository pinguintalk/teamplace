package net.teamplace.android.http.request.thumbnail;

import android.content.Context;
import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import net.teamplace.android.http.exception.ConnectFailedException;
import net.teamplace.android.http.exception.HttpResponseException;
import net.teamplace.android.http.exception.LogonFailedException;
import net.teamplace.android.http.exception.NotificationException;
import net.teamplace.android.http.exception.RedirectException;
import net.teamplace.android.http.exception.ServerLicenseException;
import net.teamplace.android.http.exception.TeamDriveNotFoundException;
import net.teamplace.android.http.exception.XCBStatusException;
import net.teamplace.android.http.request.BackendConfiguration;
import net.teamplace.android.http.request.BasicRequester;
import net.teamplace.android.http.session.Session;

import org.apache.http.NameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.util.LinkedList;
import java.util.List;

public class ThumbnailRequester extends BasicRequester
{
	private static final String TAG = ThumbnailRequester.class.getSimpleName();

	public ThumbnailRequester(Context context, Session session, BackendConfiguration backendConfiguration)
	{
		super(context, session, backendConfiguration);
	}

	public InputStream getInputStream(String teamDriveId, String file, String folder)
			throws ConnectFailedException, XCBStatusException, HttpResponseException, LogonFailedException,
			NotificationException,
			ServerLicenseException, IOException, TeamDriveNotFoundException, RedirectException {
		Uri uri = getThumbnailUri(teamDriveId, file, folder);
		return getInputStream(uri);
	}

	public InputStream getInputStream(Uri uri)
			throws XCBStatusException, HttpResponseException, JsonParseException, JsonMappingException,
			ConnectFailedException, LogonFailedException, NotificationException, ServerLicenseException, IOException, RedirectException {
		HttpURLConnection conn = null;
		InputStream in = null;
		try
		{
			List<NameValuePair> properties = new LinkedList<NameValuePair>();
			conn = connectAndExecuteGETRequest(uri, properties, true, true);
			in = conn.getInputStream();
		}
		finally
		{
			if (in == null)
			{
				disconnect(conn);
			}
		}
		return in;
	}

	public Uri getThumbnailUri(String teamDriveId, String folder, String file)
			throws TeamDriveNotFoundException
	{
		Uri.Builder builder = Uri.parse(getServerForTeamDriveId(teamDriveId)).buildUpon();
		builder.appendEncodedPath(GUI_THUMBNAIL_FILE_SHELL);
		builder.appendQueryParameter(PARAM_FOLDER, folder);
		builder.appendQueryParameter(PARAM_FILE, file);
		builder.appendQueryParameter(PARAM_RESPONSE_SCHEMA, RESPONSE_SCHEMA_PLAIN);

		if (teamDriveId != null)
		{
			builder.appendQueryParameter(PARAM_TEAM, teamDriveId);
		}
		return builder.build();
	}
}