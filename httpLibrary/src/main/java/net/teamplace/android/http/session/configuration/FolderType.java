package net.teamplace.android.http.session.configuration;

import java.util.List;

import net.teamplace.android.http.annotation.JacksonDataModel;
import net.teamplace.android.http.session.database.SessionSQLiteHelper;
import net.teamplace.android.http.util.GenericUtils;

import android.content.ContentValues;
import android.database.Cursor;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
@JacksonDataModel
public class FolderType
{
	public static final String TABLE_NAME = "table_name_folder_type";

	public static final String COL_CONFIGURATION_ID = "configuration_id";

	private static final String COL_FOLDER_ID = "folder_id";
	private static final String COL_EXPORT = "export";
	private static final String COL_OP = "op";

	public static final String CREATE_TABLE = "create table " + TABLE_NAME + "("
			+ SessionSQLiteHelper.COLUMN_ID + " integer primary key autoincrement, " + COL_CONFIGURATION_ID
			+ " integer, " + COL_EXPORT + " text, " + COL_FOLDER_ID + " integer, " + COL_OP + " integer);";

	private final String DIVIDER = ":";

	private long configurationId;

	@JsonProperty("id")
	private int id;

	@JsonProperty("export")
	private String export;

	@JsonProperty("op")
	private int op;

	public FolderType()
	{
	}

	public FolderType(Cursor cursor)
	{
		export = cursor.getString(cursor.getColumnIndex(COL_EXPORT));
		id = cursor.getInt(cursor.getColumnIndex(COL_FOLDER_ID));
		op = cursor.getInt(cursor.getColumnIndex(COL_OP));
	}

	public void setConfigurationId(long configurationId)
	{
		this.configurationId = configurationId;
	}

	public int getId()
	{
		return id;
	}

	public String getExport()
	{
		return export;
	}

	public List<String> getExportAsList()
	{
		return GenericUtils.splitStringByDivider(export, DIVIDER);
	}

	public int getOP()
	{
		return op;
	}

	public ContentValues getAsContentValues()
	{
		ContentValues values = new ContentValues();

		values.put(COL_CONFIGURATION_ID, configurationId);
		values.put(COL_EXPORT, export);
		values.put(COL_FOLDER_ID, id);
		values.put(COL_OP, op);

		return values;
	}
}
