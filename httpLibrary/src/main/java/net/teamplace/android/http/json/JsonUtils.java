package net.teamplace.android.http.json;

import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonUtils
{
	private static final String TAG = JsonUtils.class.getSimpleName();

	private static ObjectMapper defaultMapper;

	public synchronized static ObjectMapper getDefaultMapper()
	{
		if (defaultMapper == null)
		{
			defaultMapper = new ObjectMapper();
		}

		return defaultMapper;
	}
}
